package jehc.djshi.web.config;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.interceptor.NameMatchTransactionAttributeSource;
import org.springframework.transaction.interceptor.TransactionInterceptor;
/**
 * @Desc 面向切面AOP事物配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Aspect
@Configuration
public class TransactionConfig {

    //配置通配符
    private static final String AOP_POINTCUT_EXPRESSION = "execution(* jehc.djshi.*.service.impl.*.*(..)))";

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Bean
    public TransactionInterceptor txAdvice() {
        DefaultTransactionAttribute txAttr_REQUIRED = new DefaultTransactionAttribute();
        txAttr_REQUIRED.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        DefaultTransactionAttribute txAttr_REQUIRED_READONLY = new DefaultTransactionAttribute();
        txAttr_REQUIRED_READONLY.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        txAttr_REQUIRED_READONLY.setReadOnly(true);

        //配置方法拦截
        NameMatchTransactionAttributeSource source = new NameMatchTransactionAttributeSource();
        source.addTransactionalMethod("*Poulators", txAttr_REQUIRED);
        source.addTransactionalMethod("recover*", txAttr_REQUIRED);
        source.addTransactionalMethod("add*", txAttr_REQUIRED);
        source.addTransactionalMethod("globalForce*", txAttr_REQUIRED);
        source.addTransactionalMethod("insert*", txAttr_REQUIRED);
        source.addTransactionalMethod("save*", txAttr_REQUIRED);
        source.addTransactionalMethod("put*", txAttr_REQUIRED);
        source.addTransactionalMethod("del*", txAttr_REQUIRED);
        source.addTransactionalMethod("sync*", txAttr_REQUIRED);
        source.addTransactionalMethod("import*", txAttr_REQUIRED);
        source.addTransactionalMethod("delete*", txAttr_REQUIRED);
        source.addTransactionalMethod("update*", txAttr_REQUIRED);
        source.addTransactionalMethod("reply*", txAttr_REQUIRED);
        source.addTransactionalMethod("operate*", txAttr_REQUIRED);
        source.addTransactionalMethod("edit*", txAttr_REQUIRED);
        source.addTransactionalMethod("operate*", txAttr_REQUIRED);
        source.addTransactionalMethod("start*", txAttr_REQUIRED);
        source.addTransactionalMethod("submit*", txAttr_REQUIRED);
        source.addTransactionalMethod("exec*", txAttr_REQUIRED);
        source.addTransactionalMethod("complete*", txAttr_REQUIRED);
        source.addTransactionalMethod("agreement*", txAttr_REQUIRED);
        source.addTransactionalMethod("persistence*", txAttr_REQUIRED);
        source.addTransactionalMethod("sign*", txAttr_REQUIRED);
        source.addTransactionalMethod("execute*", txAttr_REQUIRED);
        source.addTransactionalMethod("callBack*", txAttr_REQUIRED);
        source.addTransactionalMethod("reject*", txAttr_REQUIRED);
        source.addTransactionalMethod("set*", txAttr_REQUIRED);
        source.addTransactionalMethod("delegate*", txAttr_REQUIRED);
        source.addTransactionalMethod("transfer*", txAttr_REQUIRED);
        source.addTransactionalMethod("waiver*", txAttr_REQUIRED);
        source.addTransactionalMethod("termination*", txAttr_REQUIRED);
        source.addTransactionalMethod("approve*", txAttr_REQUIRED);
        source.addTransactionalMethod("setOwner*", txAttr_REQUIRED);
        source.addTransactionalMethod("get*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("query*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("find*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("list*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("count*", txAttr_REQUIRED_READONLY);
        return new TransactionInterceptor(transactionManager, source);
    }

    @Bean
    public Advisor txAdviceAdvisor() {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression(AOP_POINTCUT_EXPRESSION);
        return new DefaultPointcutAdvisor(pointcut, txAdvice());
    }

//    @Bean
//    public SpringProcessEngineConfiguration springProcessEngineConfiguration(){
//
//    }
}
