package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.LcApply;
import java.util.List;
import java.util.Map;
/**
 * @Desc 流程申请
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcApplyDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcApply> getLcApplyListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param lc_apply_id
	* @return
	*/
	LcApply getLcApplyById(String lc_apply_id);
	/**
	* 添加
	* @param lcApply
	* @return
	*/
	int addLcApply(LcApply lcApply);
	/**
	* 修改
	* @param lcApply
	* @return
	*/
	int updateLcApply(LcApply lcApply);
	/**
	* 删除
	* @param condition
	* @return
	*/
	int delLcApply(Map<String, Object> condition);
	/**
	 * 根据实例编号查找集合
	 * @param condition
	 * @return
	 */
	List<LcApply> getLcApplyList(Map<String, Object> condition);
}
