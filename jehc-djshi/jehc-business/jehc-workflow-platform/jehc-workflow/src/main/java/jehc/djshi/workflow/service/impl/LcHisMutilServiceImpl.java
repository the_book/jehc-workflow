package jehc.djshi.workflow.service.impl;

import jehc.djshi.workflow.dao.LcHisMutilDao;
import jehc.djshi.workflow.model.LcHisMutil;
import jehc.djshi.workflow.service.LcHisMutilService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * @Desc 会签节点历史人员
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Service
public class LcHisMutilServiceImpl implements LcHisMutilService {
    @Autowired
    LcHisMutilDao lcHisMutilDao;

    /**
     * 查询列表
     * @param lcHisMutil
     * @return
     */
    public List<LcHisMutil> getLcHisMutilListByCondition(LcHisMutil lcHisMutil){
        return lcHisMutilDao.getLcHisMutilListByCondition(lcHisMutil);
    }


    /**
     * 新增
     * @param lcHisMutil
     * @return
     */
    public int addLcHisMutil(LcHisMutil lcHisMutil){
        return lcHisMutilDao.addLcHisMutil(lcHisMutil);
    }



    /**
     * 批量新增
     * @param lcHisMutils
     * @return
     */
    public int addBatchLcHisMutil(List<LcHisMutil> lcHisMutils){
        return lcHisMutilDao.addBatchLcHisMutil(lcHisMutils);
    }


    /**
     * 删除
     * @param condition
     * @return
     */
    public int delLcHisMutil(Map<String,Object> condition){
        return lcHisMutilDao.delLcHisMutil(condition);
    }

    /**
     * 获取最大批次
     * @param lcHisMutil
     * @return
     */
    public Integer getMaxBatch(LcHisMutil lcHisMutil){
        return lcHisMutilDao.getMaxBatch(lcHisMutil);
    }
}
