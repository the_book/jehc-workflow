package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcNodeCandidate;
import jehc.djshi.workflow.service.LcNodeCandidateService;

/**
* @Desc 节点办理人 
* @Author 邓纯杰
* @CreateTime 2022-04-03 20:12:39
*/
@Api(value = "节点办理人", description = "节点办理人",tags = "节点办理人")
@RestController
@RequestMapping("/lcNodeCandidate")
public class LcNodeCandidateController extends BaseAction{
	@Autowired
	private LcNodeCandidateService lcNodeCandidateService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcNodeCandidate>> getLcNodeCandidateListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcNodeCandidate> lcNodeCandidateList = lcNodeCandidateService.getLcNodeCandidateListByCondition(condition);
		PageInfo<LcNodeCandidate> page = new PageInfo<LcNodeCandidate>(lcNodeCandidateList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param lc_node_candidate_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{lc_node_candidate_id}")
	public BaseResult<LcNodeCandidate> getLcNodeCandidateById(@PathVariable("lc_node_candidate_id")String lc_node_candidate_id){
		LcNodeCandidate lcNodeCandidate = lcNodeCandidateService.getLcNodeCandidateById(lc_node_candidate_id);
		return outDataStr(lcNodeCandidate);
	}
	/**
	* 添加
	* @param lcNodeCandidate 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcNodeCandidate(@RequestBody LcNodeCandidate lcNodeCandidate){
		int i = 0;
		if(null != lcNodeCandidate){
			lcNodeCandidate.setLc_node_candidate_id(toUUID());
			i=lcNodeCandidateService.addLcNodeCandidate(lcNodeCandidate);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcNodeCandidate 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcNodeCandidate(@RequestBody LcNodeCandidate lcNodeCandidate){
		int i = 0;
		if(null != lcNodeCandidate){
			i=lcNodeCandidateService.updateLcNodeCandidate(lcNodeCandidate);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param lc_node_candidate_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcNodeCandidate(String lc_node_candidate_id){
		int i = 0;
		if(!StringUtil.isEmpty(lc_node_candidate_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_node_candidate_id",lc_node_candidate_id.split(","));
			i=lcNodeCandidateService.delLcNodeCandidate(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
