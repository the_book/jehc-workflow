package jehc.djshi.workflow.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcNodeBtn;

/**
* @Desc 节点按钮（关系表） 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:37:18
*/
public interface LcNodeBtnDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcNodeBtn> getLcNodeBtnListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param node_btn_id 
	* @return
	*/
	LcNodeBtn getLcNodeBtnById(String node_btn_id);
	/**
	* 添加
	* @param lcNodeBtn 
	* @return
	*/
	int addLcNodeBtn(LcNodeBtn lcNodeBtn);
	/**
	* 修改
	* @param lcNodeBtn 
	* @return
	*/
	int updateLcNodeBtn(LcNodeBtn lcNodeBtn);
	/**
	* 修改（根据动态条件）
	* @param lcNodeBtn 
	* @return
	*/
	int updateLcNodeBtnBySelective(LcNodeBtn lcNodeBtn);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcNodeBtn(Map<String, Object> condition);

	/**
	 * 根据属性id删除
	 * @param lc_node_attribute_id
	 * @return
	 */
	int delLcNodeBtnByAttributeId(String lc_node_attribute_id);
}
