package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcNodeBtnService;
import jehc.djshi.workflow.dao.LcNodeBtnDao;
import jehc.djshi.workflow.model.LcNodeBtn;

/**
* @Desc 节点按钮（关系表） 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:37:18
*/
@Service("lcNodeBtnService")
public class LcNodeBtnServiceImpl extends BaseService implements LcNodeBtnService{
	@Autowired
	private LcNodeBtnDao lcNodeBtnDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcNodeBtn> getLcNodeBtnListByCondition(Map<String,Object> condition){
		try{
			return lcNodeBtnDao.getLcNodeBtnListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param node_btn_id 
	* @return
	*/
	public LcNodeBtn getLcNodeBtnById(String node_btn_id){
		try{
			LcNodeBtn lcNodeBtn = lcNodeBtnDao.getLcNodeBtnById(node_btn_id);
			return lcNodeBtn;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcNodeBtn 
	* @return
	*/
	public int addLcNodeBtn(LcNodeBtn lcNodeBtn){
		int i = 0;
		try {
			i = lcNodeBtnDao.addLcNodeBtn(lcNodeBtn);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcNodeBtn 
	* @return
	*/
	public int updateLcNodeBtn(LcNodeBtn lcNodeBtn){
		int i = 0;
		try {
			i = lcNodeBtnDao.updateLcNodeBtn(lcNodeBtn);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcNodeBtn 
	* @return
	*/
	public int updateLcNodeBtnBySelective(LcNodeBtn lcNodeBtn){
		int i = 0;
		try {
			i = lcNodeBtnDao.updateLcNodeBtnBySelective(lcNodeBtn);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcNodeBtn(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcNodeBtnDao.delLcNodeBtn(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
