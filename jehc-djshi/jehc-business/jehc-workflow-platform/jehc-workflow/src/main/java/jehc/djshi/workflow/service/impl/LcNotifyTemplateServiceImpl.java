package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcNotifyTemplateService;
import jehc.djshi.workflow.dao.LcNotifyTemplateDao;
import jehc.djshi.workflow.model.LcNotifyTemplate;

/**
* @Desc 流程通知模板 
* @Author 邓纯杰
* @CreateTime 2022-11-30 11:29:02
*/
@Service("lcNotifyTemplateService")
public class LcNotifyTemplateServiceImpl extends BaseService implements LcNotifyTemplateService{
	@Autowired
	private LcNotifyTemplateDao lcNotifyTemplateDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcNotifyTemplate> getLcNotifyTemplateListByCondition(Map<String,Object> condition){
		try{
			return lcNotifyTemplateDao.getLcNotifyTemplateListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LcNotifyTemplate getLcNotifyTemplateById(String id){
		try{
			LcNotifyTemplate lcNotifyTemplate = lcNotifyTemplateDao.getLcNotifyTemplateById(id);
			return lcNotifyTemplate;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcNotifyTemplate 
	* @return
	*/
	public int addLcNotifyTemplate(LcNotifyTemplate lcNotifyTemplate){
		int i = 0;
		try {
			i = lcNotifyTemplateDao.addLcNotifyTemplate(lcNotifyTemplate);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcNotifyTemplate 
	* @return
	*/
	public int updateLcNotifyTemplate(LcNotifyTemplate lcNotifyTemplate){
		int i = 0;
		try {
			i = lcNotifyTemplateDao.updateLcNotifyTemplate(lcNotifyTemplate);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcNotifyTemplate 
	* @return
	*/
	public int updateLcNotifyTemplateBySelective(LcNotifyTemplate lcNotifyTemplate){
		int i = 0;
		try {
			i = lcNotifyTemplateDao.updateLcNotifyTemplateBySelective(lcNotifyTemplate);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcNotifyTemplate(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcNotifyTemplateDao.delLcNotifyTemplate(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
