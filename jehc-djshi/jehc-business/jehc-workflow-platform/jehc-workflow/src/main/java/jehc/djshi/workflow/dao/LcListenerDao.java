package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.LcListener;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程监听器
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcListenerDao {

    /**
     *
     * @param condition
     * @return
     */
    List<LcListener> getLcListenerListByCondition(Map<String, Object> condition);

    /**
     *
     * @param lc_listener_id
     * @return
     */
    LcListener getLcListenerById(String lc_listener_id);

    /**
     * 添加
     * @param lcListener
     * @return
     */
    int addLcListener(LcListener lcListener);

    /**
     * 修改
     * @param lcListener
     * @return
     */
    int updateLcListener(LcListener lcListener);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delLcListener(Map<String, Object> condition);
}
