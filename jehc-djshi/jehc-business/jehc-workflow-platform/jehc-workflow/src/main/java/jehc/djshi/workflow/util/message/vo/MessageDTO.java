package jehc.djshi.workflow.util.message.vo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.task.IdentityLink;

import java.util.List;

/**
 * @Desc 消息中心实体对象DTO
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Data
public class MessageDTO {
    String taskId;//任务id

    String nodeId;//节点编号（流程设计器定义id）

    String assigne;//任务经办人

    String businessKey;//业务Key

    String owner;//任务所属人

    List<String> userId;//任务候选人集合

    List<String> groupId;//任务候选人组集合

    List<IdentityLink> candidators;//候选人，候选组等对象集合

    /**
     *
     */
    public MessageDTO(){

    }

    /**
     *
     */
    public MessageDTO(String taskId,String nodeId,String assigne,String businessKey,String owner,List<IdentityLink> candidators){
        this.taskId = taskId;
        this.nodeId = nodeId;
        this.assigne = assigne;
        this.businessKey = businessKey;
        this.owner = owner;
        this.candidators = candidators;
    }

}
