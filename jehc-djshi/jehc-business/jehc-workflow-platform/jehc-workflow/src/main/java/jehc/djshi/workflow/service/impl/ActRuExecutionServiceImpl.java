package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.dao.ActRuExecutionDao;
import jehc.djshi.workflow.dao.ActRuIdentitylinkDao;
import jehc.djshi.workflow.dao.ActRuTaskDao;
import jehc.djshi.workflow.dao.ActRunVariableDao;
import jehc.djshi.workflow.model.ActRuExecution;
import jehc.djshi.workflow.service.ActRuExecutionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 运行实例
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class ActRuExecutionServiceImpl extends BaseService implements ActRuExecutionService {

    @Autowired
    private ActRuExecutionDao actRuExecutionDao;

    @Autowired
    private ActRuTaskDao actRuTaskDao;

    @Autowired
    ActRunVariableDao actRunVariableDao;

    @Autowired
    private ActRuIdentitylinkDao actRuIdentitylinkDao;

    /**
     * 查询运行实例集合
     * @param condition
     * @return
     */
    public List<ActRuExecution> getActRuExecutionListByCondition(Map<String,Object> condition){
        return actRuExecutionDao.getActRuExecutionListByCondition(condition);
    }

    /**
     * 查询单个运行实例
     * @param id
     * @return
     */
    public ActRuExecution getActRuExecutionById(String id){
        return actRuExecutionDao.getActRuExecutionById(id);
    }

    /**
     * 删除运行实例
     * @param id
     * @return
     */
    public BaseResult delActRuExecution(String id, String procInstId){
        try {
            actRuIdentitylinkDao.delActRuIdentitylinkByProcInstId(id);//删除运行时流程人员
            actRuTaskDao.delActRuTaskByExecutionId(id);//删除运行任务
            actRunVariableDao.delActRunVariableByExecutionId(id);//删除运行变量
            actRuExecutionDao.delActRuExecution(id);//删除运行实例
        }catch (Exception e){
            log.error("删除运行时实例及其关联失败,{}",e);
            throw new ExceptionUtil("删除运行时实例及其关联失败");
        }
        return BaseResult.success();
    }
}
