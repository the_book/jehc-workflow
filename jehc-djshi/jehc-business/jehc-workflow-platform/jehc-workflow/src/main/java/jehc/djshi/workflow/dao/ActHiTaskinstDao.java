package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.ActHiTaskinst;

import java.util.Map;

/**
 * @Desc 历史任务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface ActHiTaskinstDao {

    /**
     * 根据流程定义统计流程任务运行平均时间
     * @param proc_def_id
     * @return
     */
    String getActHiTaskInstAvgTime(String proc_def_id);

    /**
     * 更新
     * @param actHiTaskinst
     * @return
     */
    int updateActHiTaskInst(ActHiTaskinst actHiTaskinst);

    /**
     * 更新该任务为未完成
     * @param condition
     * @return
     */
    int updateActHiTaskInstEndTime(Map<String,Object> condition);
}
