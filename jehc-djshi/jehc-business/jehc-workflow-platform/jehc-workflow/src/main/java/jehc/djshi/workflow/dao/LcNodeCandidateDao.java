package jehc.djshi.workflow.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcNodeCandidate;

/**
* @Desc 节点办理人 
* @Author 邓纯杰
* @CreateTime 2022-04-03 20:12:39
*/
public interface LcNodeCandidateDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcNodeCandidate> getLcNodeCandidateListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param lc_node_candidate_id 
	* @return
	*/
	LcNodeCandidate getLcNodeCandidateById(String lc_node_candidate_id);
	/**
	* 添加
	* @param lcNodeCandidate 
	* @return
	*/
	int addLcNodeCandidate(LcNodeCandidate lcNodeCandidate);
	/**
	* 修改
	* @param lcNodeCandidate 
	* @return
	*/
	int updateLcNodeCandidate(LcNodeCandidate lcNodeCandidate);
	/**
	* 修改（根据动态条件）
	* @param lcNodeCandidate 
	* @return
	*/
	int updateLcNodeCandidateBySelective(LcNodeCandidate lcNodeCandidate);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcNodeCandidate(Map<String, Object> condition);

	/**
	 * 根据属性id删除
	 * @param lc_node_attribute_id
	 * @return
	 */
	int delLcNodeCandidateByAttributeId(String lc_node_attribute_id);
}
