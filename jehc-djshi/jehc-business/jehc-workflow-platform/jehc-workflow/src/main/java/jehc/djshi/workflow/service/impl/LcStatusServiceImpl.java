package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.workflow.dao.LcStatusDao;
import jehc.djshi.workflow.model.LcStatus;
import jehc.djshi.workflow.service.LcStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * @Desc 流程状态
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class LcStatusServiceImpl extends BaseService implements LcStatusService {
	@Autowired
	private LcStatusDao lcStatusDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcStatus> getLcStatusListByCondition(Map<String,Object> condition){
		try{
			return lcStatusDao.getLcStatusListByCondition(condition);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param lc_status_id 
	* @return
	*/
	public LcStatus getLcStatusById(String lc_status_id){
		try{
			return lcStatusDao.getLcStatusById(lc_status_id);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcStatus
	* @return
	*/
	public int addLcStatus(LcStatus lcStatus){
		int i = 0;
		try {
			lcStatus.setCreate_id(getXtUid());
			lcStatus.setCreate_time(getDate());
			i = lcStatusDao.addLcStatus(lcStatus);
		} catch (Exception e) {
			i = 0;
			throw new RuntimeException(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcStatus
	* @return
	*/
	public int updateLcStatus(LcStatus lcStatus){
		int i = 0;
		try {
			lcStatus.setUpdate_id(getXtUid());
			lcStatus.setUpdate_time(getDate());
			i = lcStatusDao.updateLcStatus(lcStatus);
		} catch (Exception e) {
			i = 0;
			throw new RuntimeException(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcStatus(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcStatusDao.delLcStatus(condition);
		} catch (Exception e) {
			i = 0;
			throw new RuntimeException(e.getMessage(),e.getCause());
		}
		return i;
	}
}
