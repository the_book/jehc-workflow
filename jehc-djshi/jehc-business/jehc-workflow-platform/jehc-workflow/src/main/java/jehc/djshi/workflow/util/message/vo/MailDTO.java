package jehc.djshi.workflow.util.message.vo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @Desc 工作流邮件DTO
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Data
public class MailDTO {

    private String from;//发送者

    private List<String> tos;//接收者

    private String subject;//主题

    private String html;//发送内容
}
