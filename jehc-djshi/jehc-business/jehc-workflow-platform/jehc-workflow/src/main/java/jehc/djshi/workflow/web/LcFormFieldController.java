package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.workflow.model.LcForm;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcFormField;
import jehc.djshi.workflow.service.LcFormFieldService;

/**
* @Desc 工作流表单字段 
* @Author 邓纯杰
* @CreateTime 2022-04-04 19:17:12
*/
@Api(value = "工作流表单字段", description = "工作流表单字段", tags = "工作流表单字段")
@RestController
@RequestMapping("/lcFormField")
public class LcFormFieldController extends BaseAction{
	@Autowired
	private LcFormFieldService lcFormFieldService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcFormField>> getLcFormFieldListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcFormField> lcFormFieldList = lcFormFieldService.getLcFormFieldListByCondition(condition);
		for(LcFormField lcFormField:lcFormFieldList){
			if(!StringUtil.isEmpty(lcFormField.getCreate_id())){
				OauthAccountEntity createBy = getAccount(lcFormField.getCreate_id());
				if(null != createBy){
					lcFormField.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(lcFormField.getUpdate_id())){
				OauthAccountEntity modifieBy = getAccount(lcFormField.getUpdate_id());
				if(null != modifieBy){
					lcFormField.setModifiedBy(modifieBy.getName());
				}
			}
		}
		PageInfo<LcFormField> page = new PageInfo<LcFormField>(lcFormFieldList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param lc_form_field_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{lc_form_field_id}")
	public BaseResult<LcFormField> getLcFormFieldById(@PathVariable("lc_form_field_id")String lc_form_field_id){
		LcFormField lcFormField = lcFormFieldService.getLcFormFieldById(lc_form_field_id);
		OauthAccountEntity createBy = getAccount(lcFormField.getCreate_id());
		if(null != createBy){
			lcFormField.setCreateBy(createBy.getName());
		}
		if(!StringUtil.isEmpty(lcFormField.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(lcFormField.getUpdate_id());
			if(null != modifiedBy){
				lcFormField.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(lcFormField);
	}
	/**
	* 添加
	* @param lcFormField 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcFormField(@RequestBody LcFormField lcFormField){
		int i = 0;
		if(null != lcFormField){
			lcFormField.setCreate_id(getXtUid());
			lcFormField.setCreate_time(getDate());
			lcFormField.setLc_form_field_id(toUUID());
			i=lcFormFieldService.addLcFormField(lcFormField);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcFormField 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcFormField(@RequestBody LcFormField lcFormField){
		int i = 0;
		if(null != lcFormField){
			lcFormField.setUpdate_id(getXtUid());
			lcFormField.setUpdate_time(getDate());
			i=lcFormFieldService.updateLcFormField(lcFormField);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param lc_form_field_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcFormField(String lc_form_field_id){
		int i = 0;
		if(!StringUtil.isEmpty(lc_form_field_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_form_field_id",lc_form_field_id.split(","));
			i=lcFormFieldService.delLcFormField(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询集合
	 * @param lc_form_field_id
	 */
	@ApiOperation(value="查询集合", notes="查询集合")
	@NeedLoginUnAuth
	@GetMapping(value="/find")
	public BaseResult<List<LcFormField>> find(String lc_form_field_id){
		if(!StringUtil.isEmpty(lc_form_field_id)){
			Map<String, Object> condition = new HashMap<>();
			condition.put("lc_form_field_id",lc_form_field_id.split(","));
			List<LcFormField> lcFormFieldList = lcFormFieldService.getLcFormFieldListByCondition(condition);
			return new BaseResult(lcFormFieldList);
		}
		return new BaseResult();
	}

}
