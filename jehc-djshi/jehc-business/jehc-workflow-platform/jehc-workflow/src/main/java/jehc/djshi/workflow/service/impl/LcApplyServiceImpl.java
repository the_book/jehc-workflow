package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.workflow.dao.LcApplyDao;
import jehc.djshi.workflow.model.LcApply;
import jehc.djshi.workflow.service.LcApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程申请
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class LcApplyServiceImpl extends BaseService implements LcApplyService {
	@Autowired
	private LcApplyDao lcApplyDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcApply> getLcApplyListByCondition(Map<String,Object> condition){
		try{
			return lcApplyDao.getLcApplyListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param lc_apply_id 
	* @return
	*/
	public LcApply getLcApplyById(String lc_apply_id){
		try{
			return lcApplyDao.getLcApplyById(lc_apply_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcApply
	* @return
	*/
	public int addLcApply(LcApply lcApply){
		int i = 0;
		try {
			lcApply.setCreate_time(getDate());
			i = lcApplyDao.addLcApply(lcApply);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcApply
	* @return
	*/
	public int updateLcApply(LcApply lcApply){
		int i = 0;
		try {
			lcApply.setUpdate_time(getDate());
			i = lcApplyDao.updateLcApply(lcApply);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcApply(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcApplyDao.delLcApply(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 根据实例编号查找集合
	 * @param condition
	 * @return
	 */
	public List getLcApplyList(Map<String,Object> condition){
		return lcApplyDao.getLcApplyList(condition);
	}
}
