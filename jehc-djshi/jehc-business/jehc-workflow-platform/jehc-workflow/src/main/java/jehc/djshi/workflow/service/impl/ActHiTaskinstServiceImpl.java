package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.workflow.dao.ActHiTaskinstDao;
import jehc.djshi.workflow.model.ActHiTaskinst;
import jehc.djshi.workflow.service.ActHiTaskinstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * @Desc 历史任务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class ActHiTaskinstServiceImpl extends BaseService implements ActHiTaskinstService {

    @Autowired
    private ActHiTaskinstDao actHiTaskinstDao;

    /**
     * 根据流程定义统计流程任务运行平均时间
     * @param proc_def_id
     * @return
     */
    public String getActHiTaskInstAvgTime(String proc_def_id) {
        return actHiTaskinstDao.getActHiTaskInstAvgTime(proc_def_id);
    }

    /**
     * 更新
     * @param actHiTaskinst
     * @return
     */
    public int updateActHiTaskInst(ActHiTaskinst actHiTaskinst){
        try {
            return actHiTaskinstDao.updateActHiTaskInst(actHiTaskinst);
        }catch (Exception e){
            throw new ExceptionUtil("更新历史任务异常：{}",e);
        }
    }
}
