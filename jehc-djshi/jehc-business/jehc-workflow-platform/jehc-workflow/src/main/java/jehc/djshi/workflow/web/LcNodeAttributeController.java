package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.workflow.model.LcNodeBtn;
import jehc.djshi.workflow.param.NodeAttributeParam;
import jehc.djshi.workflow.service.LcNodeBtnService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcNodeAttribute;
import jehc.djshi.workflow.service.LcNodeAttributeService;

/**
* @Desc 节点属性 
* @Author 邓纯杰
* @CreateTime 2022-03-27 22:05:30
*/
@Api(value = "节点属性",description = "节点属性", tags = "节点属性")
@RestController
@RequestMapping("/lcNodeAttribute")
public class LcNodeAttributeController extends BaseAction {
	@Autowired
	private LcNodeAttributeService lcNodeAttributeService;
	@Autowired
	private LcNodeBtnService lcNodeBtnService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcNodeAttribute>> getLcNodeAttributeListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcNodeAttribute> lcNodeAttributeList = lcNodeAttributeService.getLcNodeAttributeListByCondition(condition);
		PageInfo<LcNodeAttribute> page = new PageInfo<LcNodeAttribute>(lcNodeAttributeList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	* 查询单条记录
	* @param lc_node_attribute_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{lc_node_attribute_id}")
	public BaseResult<LcNodeAttribute> getLcNodeAttributeById(@PathVariable("lc_node_attribute_id")String lc_node_attribute_id){
		LcNodeAttribute lcNodeAttribute = lcNodeAttributeService.getLcNodeAttributeById(lc_node_attribute_id);
		if(null != lcNodeAttribute){
			Map<String,Object> condition = new HashMap<>();
			condition.put("lc_node_attribute_id",lcNodeAttribute.getLc_node_attribute_id());
			List<LcNodeBtn> lcNodeBtns = lcNodeBtnService.getLcNodeBtnListByCondition(condition);
			lcNodeAttribute.setLcNodeBtns(lcNodeBtns);
		}
		return outDataStr(lcNodeAttribute);
	}
//	/**
//	* 添加
//	* @param lcNodeAttribute
//	*/
//	@ApiOperation(value="添加", notes="添加")
//	@PostMapping(value="/add")
//	public BaseResult addLcNodeAttribute(@RequestBody LcNodeAttribute lcNodeAttribute){
//		int i = 0;
//		if(null != lcNodeAttribute){
//			lcNodeAttribute.setLc_node_attribute_id(toUUID());
//			i=lcNodeAttributeService.addLcNodeAttribute(lcNodeAttribute);
//		}
//		if(i>0){
//			return outAudStr(true);
//		}else{
//			return outAudStr(false);
//		}
//	}
//	/**
//	* 修改
//	* @param lcNodeAttribute
//	*/
//	@ApiOperation(value="修改", notes="修改")
//	@PutMapping(value="/update")
//	public BaseResult updateLcNodeAttribute(@RequestBody LcNodeAttribute lcNodeAttribute){
//		int i = 0;
//		if(null != lcNodeAttribute){
//			i=lcNodeAttributeService.updateLcNodeAttribute(lcNodeAttribute);
//		}
//		if(i>0){
//			return outAudStr(true);
//		}else{
//			return outAudStr(false);
//		}
//	}

	/**
	 * 保存
	 * @param lcNodeAttribute
	 */
	@ApiOperation(value="保存", notes="保存")
	@PostMapping(value="/save")
	public BaseResult saveLcNodeAttribute(@RequestBody LcNodeAttribute lcNodeAttribute){
		int i = 0;
		if(null != lcNodeAttribute){
			i=lcNodeAttributeService.saveLcNodeAttribute(lcNodeAttribute);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param lc_node_attribute_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcNodeAttribute(String lc_node_attribute_id){
		int i = 0;
		if(!StringUtil.isEmpty(lc_node_attribute_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_node_attribute_id",lc_node_attribute_id.split(","));
			i=lcNodeAttributeService.delLcNodeAttribute(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}


	/**
	 *
	 * @param nodeAttributeParam
	 * @return
	 */
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/attribute")
	public BaseResult<LcNodeAttribute> getLcNodeAttribute(NodeAttributeParam nodeAttributeParam){
		if(StringUtil.isEmpty(nodeAttributeParam.getNodeId())){
			throw new ExceptionUtil("节点编号为空");
		}
		if(StringUtil.isEmpty(nodeAttributeParam.getHid_()) && StringUtil.isEmpty(nodeAttributeParam.getMounts_id())){
			throw new ExceptionUtil("挂载编号和历史版本号为空");
		}
		LcNodeAttribute lcNodeAttribute = lcNodeAttributeService.getLcNodeAttribute(nodeAttributeParam);
		return outDataStr(lcNodeAttribute);
	}

}
