package jehc.djshi.workflow.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcFieldHtml;

/**
* @Desc 字段html类型 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:38:14
*/
public interface LcFieldHtmlService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcFieldHtml> getLcFieldHtmlListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param lc_field_html_id 
	* @return
	*/
	LcFieldHtml getLcFieldHtmlById(String lc_field_html_id);
	/**
	* 添加
	* @param lcFieldHtml 
	* @return
	*/
	int addLcFieldHtml(LcFieldHtml lcFieldHtml);
	/**
	* 修改
	* @param lcFieldHtml 
	* @return
	*/
	int updateLcFieldHtml(LcFieldHtml lcFieldHtml);
	/**
	* 修改（根据动态条件）
	* @param lcFieldHtml 
	* @return
	*/
	int updateLcFieldHtmlBySelective(LcFieldHtml lcFieldHtml);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcFieldHtml(Map<String, Object> condition);
}
