package jehc.djshi.workflow.web;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.sys.dao.XtUserinfoDao;
import jehc.djshi.sys.model.XtUserinfo;
import jehc.djshi.workflow.model.LcDeploymentHis;
import jehc.djshi.workflow.param.LcReceiveNParam;
import jehc.djshi.workflow.param.LcTaskParam;
import jehc.djshi.workflow.service.LcDeploymentHisService;
import jehc.djshi.workflow.service.LcTaskService;
import jehc.djshi.workflow.vo.*;
import jehc.djshi.workflow.param.LcReceiveParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.util.ActivitiUtil;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Desc 流程任务管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "流程任务管理",description = "流程任务管理", tags = "流程任务管理")
@RestController
@RequestMapping("/lcTask")
public class LcTaskController extends BaseAction {
	@Autowired
	private ActivitiUtil activitiUtil;

	@Autowired
	private XtUserinfoDao xtUserinfoDao;

	@Autowired
	private LcTaskService lcTaskService;

	@Autowired
	private LcDeploymentHisService lcDeploymentHisService;

	/**
	 * 查询并分页
	 * @param baseSearch
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@PostMapping(value="/list")
	@NeedLoginUnAuth
	public BasePage<List<LcTask>> getLcApprovalListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		if(null != baseSearch){
			condition.put("start",baseSearch.getStart());
			condition.put("pageSize",baseSearch.getPageSize());
		}
		Object instanceId = condition.get("instanceId");
		Map<String,Object> map = new HashMap<>();
		if(null == instanceId){
			map = activitiUtil.getTaskList(condition);
		}else{
			map = activitiUtil.getTaskListByInstanceId(condition);
		}
		List<Task> list = (List<Task>)map.get("TaskList");
		int total = new Integer(map.get("TaskCount").toString());
		List<LcTask> lcTasks = new ArrayList<>();
		for(int i = 0; i < list.size(); i++){
			Task task = list.get(i);
			LcTask lcTask = new LcTask();
			lcTask.setCategory(task.getCategory());
			if(!StringUtil.isEmpty(task.getAssignee())){
				XtUserinfo xtUserinfo = xtUserinfoDao.getXtUserinfoById(task.getAssignee());
				if(null != xtUserinfo){
					lcTask.setAssignee(xtUserinfo.getXt_userinfo_realName());
				}
			}else{
				lcTask.setAssignee("X");
			}
			LcDeploymentHis lcDeploymentHis = getLcDeploymentHis(task.getId());
			if(null != lcDeploymentHis){
				lcTask.setProcessTitle(lcDeploymentHis.getLc_process_title());
				lcTask.setVersion(lcDeploymentHis.getVersion());
			}
			lcTask.setDescription(task.getDescription());
			lcTask.setExecutionId(task.getExecutionId());
			lcTask.setFormKey(task.getFormKey());
			lcTask.setTaskId(task.getId());
			lcTask.setName(task.getName());
			HistoricProcessInstance historicProcessInstance = activitiUtil.getHistoryProcessInstance(task.getProcessInstanceId());
			if(null != historicProcessInstance){
				lcTask.setBusinessKey(historicProcessInstance.getBusinessKey());
			}
			lcTask.setSuspended(task.isSuspended());
			if(!StringUtil.isEmpty(task.getOwner())){
				XtUserinfo xtUserinfo = xtUserinfoDao.getXtUserinfoById(task.getOwner());
				if(null != xtUserinfo){
					lcTask.setOwner(xtUserinfo.getXt_userinfo_realName());
				}
			}else{
				lcTask.setOwner("X");
			}
			lcTask.setParentTaskId(task.getParentTaskId());
			lcTask.setPriority(task.getPriority());
			lcTask.setProcessDefinitionId(task.getProcessDefinitionId());
			lcTask.setProcessInstanceId(task.getProcessInstanceId());
			lcTask.setTaskDefinitionKey(task.getTaskDefinitionKey());
			lcTask.setTenantId(task.getTenantId());
			lcTask.setCreateTime(task.getCreateTime());
			lcTask.setDelegationState(null != task.getDelegationState()?task.getDelegationState().name():"");
			lcTask.setDueDate(task.getDueDate());
			lcTask.setProcessVariables(task.getProcessVariables());
			lcTasks.add(lcTask);
		}
		PageInfo<LcTask> page = new PageInfo<LcTask>(lcTasks);
		page.setTotal(total);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 根据任务id查询部署对象
	 * @param taskId
	 * @return
	 */
	private LcDeploymentHis getLcDeploymentHis(String taskId){
		if(StringUtil.isEmpty(taskId)){
			return null;
		}
		String deploymentId = activitiUtil.getDeploymentIdByTaskId(taskId);
		if(StringUtil.isEmpty(deploymentId)){
			return null;
		}
		LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisByHisId(deploymentId);
		return lcDeploymentHis;
	}

	/**
	 * 查找指定人分配出去的任务集合并分页
	 * @param baseSearch
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查找指定人分配出去的任务集合并分页")
	@PostMapping(value="/owner/list")
	@NeedLoginUnAuth
	public BasePage<List<LcTask>> changeOwnerTaskList(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		if(null != baseSearch){
			condition.put("start",baseSearch.getStart());
			condition.put("pageSize",baseSearch.getPageSize());
		}
		Object instanceId = condition.get("instanceId");
		Map<String,Object> map = new HashMap<>();
		if(null == instanceId){
			map = activitiUtil.changeOwnerTaskList(condition);
		}else{
			map = activitiUtil.changeOwnerTaskListByInstanceId(condition);
		}
		List<Task> list = (List<Task>)map.get("TaskList");
		int total = new Integer(map.get("TaskCount").toString());
		List<LcTask> lcTasks = new ArrayList<>();
		for(int i = 0; i < list.size(); i++){
			Task task = list.get(i);
			LcTask lcTask = new LcTask();
			lcTask.setCategory(task.getCategory());
			if(!StringUtil.isEmpty(task.getAssignee())){
				XtUserinfo xtUserinfo = xtUserinfoDao.getXtUserinfoById(task.getAssignee());
				if(null != xtUserinfo){
					lcTask.setAssignee(xtUserinfo.getXt_userinfo_realName());
				}
			}else{
				lcTask.setAssignee("X");
			}
			LcDeploymentHis lcDeploymentHis = getLcDeploymentHis(task.getId());
			if(null != lcDeploymentHis){
				lcTask.setProcessTitle(lcDeploymentHis.getLc_process_title());
				lcTask.setVersion(lcDeploymentHis.getVersion());
			}
			lcTask.setDescription(task.getDescription());
			lcTask.setExecutionId(task.getExecutionId());
			lcTask.setFormKey(task.getFormKey());
			lcTask.setTaskId(task.getId());
			lcTask.setName(task.getName());
			if(!StringUtil.isEmpty(task.getOwner())){
				XtUserinfo xtUserinfo = xtUserinfoDao.getXtUserinfoById(task.getOwner());
				if(null != xtUserinfo){
					lcTask.setOwner(xtUserinfo.getXt_userinfo_realName());
				}
			}else{
				lcTask.setOwner("X");
			}
			lcTask.setParentTaskId(task.getParentTaskId());
			lcTask.setPriority(task.getPriority());
			lcTask.setProcessDefinitionId(task.getProcessDefinitionId());
			lcTask.setProcessInstanceId(task.getProcessInstanceId());
			lcTask.setTaskDefinitionKey(task.getTaskDefinitionKey());
			lcTask.setTenantId(task.getTenantId());
			lcTask.setCreateTime(task.getCreateTime());
			lcTask.setDelegationState(null != task.getDelegationState()?task.getDelegationState().name():"");
			lcTask.setDueDate(task.getDueDate());
			lcTask.setProcessVariables(task.getProcessVariables());
			lcTasks.add(lcTask);
		}
		PageInfo<LcTask> page = new PageInfo<LcTask>(lcTasks);
		page.setTotal(total);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 统一审批
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="审批", notes="统一审批")
	@PostMapping(value="/approve")
	public BaseResult approve(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.approve(lcReceiveNParam);
	}

	/**
	 * 根据任务Id同意
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="同意", notes="根据任务Id同意")
	@PostMapping(value="/agreement")
	public BaseResult agreement(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.agreement(lcReceiveNParam);
	}

	/**
	 * 根据任务Id驳回流程
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="驳回", notes="驳回流程")
	@PostMapping(value="/reject")
	public BaseResult reject(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.reject(lcReceiveNParam);
	}

	/**
	 * 根据任务Id撤回流程
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="撤回", notes="撤回流程")
	@PostMapping(value="/callBack")
	public BaseResult callBack(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.callBack(lcReceiveNParam);
	}

	/**
	 * 设置转办人，经办人
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="转办", notes="设置转办人，经办人")
	@PostMapping(value="/transfer")
	public BaseResult transfer(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.transfer(lcReceiveNParam);
	}

	/**
	 * 设置委派人
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="委派", notes="设置委派人")
	@PostMapping(value="/delegateTask")
	public BaseResult delegateTask(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.delegateTask(lcReceiveNParam);
	}

	/**
	 * 弃权
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="弃权", notes="弃权")
	@PostMapping(value="/waiver")
	public BaseResult waiver(@RequestBody LcReceiveNParam lcReceiveNParam){
		BaseResult baseResult = lcTaskService.waiver(lcReceiveNParam);
		return baseResult;
	}

	/**
	 * 根据任务Id直接结束流程实例
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="终止流程", notes="根据当前任务Id结束流程")
	@PostMapping(value="/termination")
	public BaseResult termination(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.termination(lcReceiveNParam);
	}

	/**
	 * 加签
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="加签", notes="加签")
	@PostMapping(value="/sign")
	public BaseResult sign(@RequestBody LcReceiveNParam lcReceiveNParam){
		BaseResult baseResult = lcTaskService.sign(lcReceiveNParam);
		return baseResult;
	}

	/**
	 * 催办
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="催办", notes="催办")
	@PostMapping(value="/urge")
	public BaseResult urge(@RequestBody LcReceiveNParam lcReceiveNParam){
		BaseResult baseResult = lcTaskService.urge(lcReceiveNParam);
		return baseResult;
	}

	/**
	 * 设置归属人
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="设置归属人", notes="设置归属人")
	@PostMapping(value="/setOwner")
	public BaseResult setOwner(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.setOwner(lcReceiveNParam);
	}

	/**
	 * 完成任务
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="提交", notes="完成任务")
	@PostMapping(value="/completeTask")
	public BaseResult completeTask(@RequestBody LcReceiveNParam lcReceiveNParam){
		lcReceiveNParam.setBehavior("0");
		BaseResult baseResult = lcTaskService.completeTask(lcReceiveNParam);
		return baseResult;
	}

	/**
	 * 向组任务中添加成员
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="向组任务中添加成员", notes="向组任务中添加成员")
	@PostMapping(value="/addGroupUser")
	public BaseResult addGroupUser(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.addGroupUser(lcReceiveNParam);
	}

	/**
	 * 向组任务中删除成员
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="向组任务中删除成员", notes="向组任务中删除成员")
	@PostMapping(value="/deleteGroupUser")
	public BaseResult deleteGroupUser(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.deleteGroupUser(lcReceiveNParam);
	}

	/**
	 * 查找流程实例下任务
	 */
	@ApiOperation(value="查找流程实例下任务", notes="根据流程实例id查找流程实例下任务")
	@GetMapping(value="/list/{processInstanceId}")
	@NeedLoginUnAuth
	public BaseResult<List<LcTask>> getLcTaskListByProcessInstanceId(@PathVariable("processInstanceId") String processInstanceId){
		return lcTaskService.getLcTaskListByProcessInstanceId(processInstanceId);
	}

	/**
	 * 查找流程实例下节点
	 */
	@ApiOperation(value="查找流程实例下节点", notes="根据流程实例id查找流程实例下任务节点")
	@GetMapping(value="/userTask/list/{procInstId}")
	@NeedLoginUnAuth
	public BaseResult<List<UserTask>> getUserTaskListByProcessInstanceId(@PathVariable("procInstId") String procInstId){
		return lcTaskService.getUserTaskListByProcessInstanceId(procInstId);
	}

	/**
	 * 根据当前任务节点任意跳转节点
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="跳转节点", notes="根据当前任务节点任意跳转节点")
	@PostMapping(value="/executeJump")
	public BaseResult executeJump(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.executeJump(lcReceiveNParam);
	}

	/**
	 * 根据当前任务Id查询可以驳回的任务节点
	 */
	@ApiOperation(value="查询可驳回的节点", notes="根据当前任务Id查询可以驳回的任务节点")
	@GetMapping(value="/canBackActivity/list/{taskId}")
	@NeedLoginUnAuth
	public BaseResult findCanBackActivity(@PathVariable("taskId") String taskId){
		return lcTaskService.findCanBackActivity(taskId);
	}

	/**
	 * 根据任务Id获取当前任务节点流出线指定属性集合
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="查询当前任务流出线名称集合", notes="查询当前任务节点流出线指定属性集合（可用于在办理任务节点操作进行驳回或通过）")
	@GetMapping(value="/outTrans/{taskId}")
	@AuthUneedLogin
	public BaseResult getOutTransListByTaskId(@PathVariable("taskId") String taskId){
		return lcTaskService.getOutTransListByTaskId(taskId);
	}

	/**
	 * 根据任务Id获取当前任务节点流出线指定属性集合
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="查询当前任务流程线条属性集合", notes="查询当前任务节点流出线指定属性集合（可用于在办理任务节点操作进行驳回或通过）")
	@GetMapping(value="/outTrans/{taskId}/{attribute}")
	@AuthUneedLogin
	public BaseResult getOutTransListByTaskId(@PathVariable("taskId") String taskId,@PathVariable("attribute")String attribute){
		return lcTaskService.getOutTransListByTaskId(taskId,attribute);
	}

	/**
	 * 设置Task变量（如在处理当前任务设置变量 在下一个节点中可以获取该变量）
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="设置Task变量", notes="设置Task变量（如在处理当前任务设置变量 在下一个节点中可以获取该变量）")
	@PostMapping(value="/setTaskVariable")
	@AuthUneedLogin
	public BaseResult setTaskVariable(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.setTaskVariable(lcReceiveNParam);
	}

	/**
	 * 根据任务Id获取当前任务节点流出线指定属性集合
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="查询设置Task变量", notes="查询设置Task变量（如在处理当前任务设置变量 在下一个节点中可以获取该变量）")
	@GetMapping(value="/taskVariable/{taskId}")
	@AuthUneedLogin
	public BaseResult getTaskVariable(@PathVariable("taskId") String taskId){
		return lcTaskService.getTaskVariable(taskId);
	}

	/**
	 * 查询设置Task变量（如在处理当前任务设置变量 在下一个节点中可以获取该变量）
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="查询设置Task变量", notes="查询设置Task变量（如在处理当前任务设置变量 在下一个节点中可以获取该变量）")
	@GetMapping(value="/taskVariable/{taskId}/{key}")
	@AuthUneedLogin
	public BaseResult getTaskVariable(@PathVariable("taskId") String taskId,@PathVariable("key")String key){
		return lcTaskService.getTaskVariable(taskId,key);
	}

	/**
	 * 查询业务Key
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="查询业务Key", notes="查询业务Key")
	@GetMapping(value="/businessKey/{taskId}")
	@AuthUneedLogin
	public BaseResult getBusinessKey(@PathVariable("taskId") String taskId){
		return lcTaskService.getBusinessKey(taskId);
	}

	/**
	 * 根据任务id查询任务表单属性集合
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="查询任务表单属性集合", notes="根据任务Id查询任务表单属性集合")
	@GetMapping(value="/formProperties/{taskId}")
	@AuthUneedLogin
	public BaseResult<List<FormProperty>> getTaskFormProperties(@PathVariable("taskId") String taskId){
		return new BaseResult(activitiUtil.getTaskFormProperties(taskId));
	}

	/**
	 * 根据任务Id查询任务表单
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="查询任务表单", notes="根据任务Id查询任务表单")
	@GetMapping(value="/form/{taskId}")
	@AuthUneedLogin
	public BaseResult getTaskFormData(@PathVariable("taskId") String taskId){
		return new BaseResult(activitiUtil.getTaskFormData(taskId));
	}

	/**
	 * 根据任务id查询当前任务节点及目标节点
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="查询当前任务节点及目标节点", notes="根据任务id查询当前任务节点及目标节点")
	@GetMapping(value="/node/{taskId}")
	@AuthUneedLogin
	public BaseResult getLcNodeAttribute(@PathVariable("taskId") String taskId){
		return lcTaskService.getLcNodeAttribute(taskId);
	}

	/**
	 * 根据任务Id查询下一个节点（按流程图走向获取）
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="根据任务Id查询下一个节点（按流程图走向获取）", notes="根据任务Id查询下一个节点（按流程图走向获取）")
	@GetMapping(value="/nextNode/{taskId}")
	@AuthUneedLogin
	public BaseResult<List<LcTask>> getLcNextNode(@PathVariable("taskId") String taskId){
		return lcTaskService.getLcNextNode(taskId);
	}

	/**
	 * 判断节点是否为会签节点,条件可以单独使用taskId,也可以使用流程实例：processInstanceId+节点id：activityId（多实例）
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="判断节点是否为会签节点", notes="判断节点是否为会签节点,条件可以单独使用taskId,也可以使用流程实例：processInstanceId+节点id：activityId")
	@PostMapping(value="/node/multi")
	@AuthUneedLogin
	public BaseResult multi(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.multi(lcReceiveNParam);
	}

	/**
	 * 查找行为属性
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="查找行为属性", notes="查找行为属性")
	@GetMapping(value="/node/activityBehavior/{taskId}")
	@AuthUneedLogin
	public BaseResult<BehaviorEntity> getActivityBehavior(@PathVariable("taskId") String taskId){
		return lcTaskService.getActivityBehavior(taskId);
	}

	/**
	 * 查找行为属性
	 * @param lcReceiveParam
	 * @return
	 */
	@ApiOperation(value="查找行为属性", notes="查找行为属性")
	@PostMapping(value="/node/activityBehavior")
	@AuthUneedLogin
	public BaseResult<BehaviorEntity> getActivityBehavior(@RequestBody LcReceiveParam lcReceiveParam){
		return lcTaskService.getActivityBehavior(lcReceiveParam);
	}

	/**
	 * 查找当前任务所在节点按流程图走向查找上级节点
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="查找当前任务所在节点按流程图走向查找上级节点", notes="查找当前任务所在节点按流程图走向查找上级节点")
	@PostMapping(value="/node/previousNode")
	@AuthUneedLogin
	public BaseResult<List<UserTask>> previousNode(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.previousNode(lcReceiveNParam);
	}

	/**
	 * 挂起流程实例
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="挂起", notes="挂起流程实例")
	@PostMapping(value="/suspendProcess")
	public BaseResult suspendProcessInstanceById(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.suspendProcess(lcReceiveNParam);
	}

	/**
	 * 激活流程实例
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="激活", notes="激活流程实例")
	@PostMapping(value="/activateProcess")
	public BaseResult activateProcessInstanceById(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.activateProcess(lcReceiveNParam);
	}

	/**
	 * 根据任务id查询当前任务节点及目标节点（包括自定义及流程图走向）
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="根据任务id查询当前任务节点及目标节点（包括自定义及流程图走向）", notes="根据任务id查询当前任务节点及目标节点（包括自定义及流程图走向）")
	@GetMapping(value="/attribute/{taskId}")
	@AuthUneedLogin
	public BaseResult<LcAttributeNEntity> getLcAttribute(@PathVariable("taskId") String taskId){
		return lcTaskService.getLcAttribute(taskId,null);
	}

	/**
	 * 查询指定节点已审批的人
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="查询指定节点已审批的人", notes="查询指定节点已审批的人")
	@GetMapping(value="/node/user")
	@AuthUneedLogin
	public BaseResult getNodeUser(LcReceiveNParam lcReceiveNParam){
		return lcTaskService.getNodeUser(lcReceiveNParam);
	}

	/**
	 * 根据条件查询任务
	 * @param lcTaskParam
	 * @return
	 */
	@ApiOperation(value="根据条件查询任务", notes="根据条件查询任务")
	@GetMapping(value="/single")
	@AuthUneedLogin
	public BaseResult<List<LcTask>> getTaskList(LcTaskParam lcTaskParam){
		List<Task> tasks = activitiUtil.getTaskList(lcTaskParam);
		List<LcTask> lcTasks = new ArrayList<>();
		for(int i = 0; i < tasks.size(); i++){
			Task task = tasks.get(i);
			LcTask lcTask = new LcTask();
			lcTask.setCategory(task.getCategory());
			if(!StringUtil.isEmpty(task.getAssignee())){
				lcTask.setAssignee(task.getAssignee());
			}else{
				lcTask.setAssignee("X");
			}
			if(StringUtil.isEmpty(lcTaskParam.getBusinessKey())){
				if(!StringUtil.isEmpty(task.getProcessInstanceId())){
					HistoricProcessInstance historicProcessInstance = activitiUtil.getHistoryProcessInstance(task.getProcessInstanceId());
					if(null != historicProcessInstance){
						lcTask.setBusinessKey(historicProcessInstance.getBusinessKey());
					}
				}
			}
			lcTask.setDescription(task.getDescription());
			lcTask.setExecutionId(task.getExecutionId());
			lcTask.setFormKey(task.getFormKey());
			lcTask.setTaskId(task.getId());
			lcTask.setName(task.getName());
			lcTask.setSuspended(task.isSuspended());
			if(!StringUtil.isEmpty(task.getOwner())){
				lcTask.setOwner(task.getOwner());
				lcTask.setAssignee(task.getAssignee());
			}else{
				lcTask.setOwner("X");
			}
			lcTask.setParentTaskId(task.getParentTaskId());
			lcTask.setPriority(task.getPriority());
			lcTask.setProcessDefinitionId(task.getProcessDefinitionId());
			lcTask.setProcessInstanceId(task.getProcessInstanceId());
			lcTask.setTaskDefinitionKey(task.getTaskDefinitionKey());
			lcTask.setTenantId(task.getTenantId());
			lcTask.setCreateTime(task.getCreateTime());
			lcTask.setDelegationState(null != task.getDelegationState()?task.getDelegationState().name():"");
			lcTask.setDueDate(task.getDueDate());
			lcTask.setProcessVariables(task.getProcessVariables());
			lcTasks.add(lcTask);
		}
		return BaseResult.success(lcTasks);
	}

	/**
	 * 查询节点的未处理人及已处理人
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="查询节点的未处理人及已处理人", notes="查询节点的未处理人及已处理人")
	@GetMapping(value="/nodeApproveUsers")
	@AuthUneedLogin
	public BaseResult<List<LcTaskApprovedEntity>> getNodeApproveUsers(LcReceiveNParam lcReceiveNParam){
		return lcTaskService.getNodeApproveUsers(lcReceiveNParam);
	}

	/**
	 * 根据任务查询组成员
	 * @param lcTaskParam
	 * @return
	 */
	@ApiOperation(value="根据任务查询组成员", notes="根据任务查询组成员")
	@GetMapping(value="/findGroupUsers")
	@AuthUneedLogin
	public BaseResult<List<LcIdentityLinkEntity>> findGroupUsers(LcTaskParam lcTaskParam){
		if(StringUtil.isEmpty(lcTaskParam.getTaskId())){
			return BaseResult.success();
		}
		List<LcIdentityLinkEntity> lcIdentityLinkEntities = new ArrayList<>();
		List<IdentityLink> identityLinks = activitiUtil.getTaskService().getIdentityLinksForTask(lcTaskParam.getTaskId());
		if(CollectionUtil.isNotEmpty(identityLinks)){
			for(IdentityLink identityLink:identityLinks){
				LcIdentityLinkEntity lcIdentityLinkEntity = new LcIdentityLinkEntity();
				lcIdentityLinkEntity.setType(identityLink.getType());
				lcIdentityLinkEntity.setTaskId(identityLink.getTaskId());
				lcIdentityLinkEntity.setUserId(identityLink.getUserId());
				if(!StringUtil.isEmpty(identityLink.getUserId())){
					XtUserinfo xtUserinfo = xtUserinfoDao.getXtUserinfoById(identityLink.getUserId());
					if(null != xtUserinfo){
						lcIdentityLinkEntity.setName(xtUserinfo.getXt_userinfo_realName());
					}
				}
				lcIdentityLinkEntity.setGroupId(identityLink.getGroupId());
				lcIdentityLinkEntities.add(lcIdentityLinkEntity);
			}
		}
		return BaseResult.success(lcIdentityLinkEntities);
	}

	/**
	 * 流程干预（任意节点跳转）
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="流程干预（任意节点跳转，排除会签）", notes="流程干预（任意节点跳转，排除会签）")
	@PostMapping(value="/globalTask")
	public BaseResult globalTask(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.globalTask(lcReceiveNParam);
	}

	/**
	 * 流程干预（转办）
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="流程干预（转办）", notes="流程干预（转办）")
	@PostMapping(value="/globalTransfer")
	public BaseResult globalTransfer(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.globalTransfer(lcReceiveNParam);
	}


	/**
	 * 流程干预（根据流程实例查找最新任务）
	 * @param procInstId
	 * @return
	 */
	@ApiOperation(value="流程干预（根据流程实例查找最新任务）", notes="流程干预（根据流程实例查找最新任务）")
	@GetMapping(value="/global/tasks/{procInstId}")
	public BaseResult<List<LcTaskEntity>> getRunTaskByInstanceId(@PathVariable("procInstId") String procInstId){
		return lcTaskService.getRunTaskByInstanceId(procInstId);
	}

	/**
	 * 撤回（流程实例已结束，正在运行强制撤回方式）
	 * @param lcReceiveNParam
	 * @return
	 */
	@ApiOperation(value="撤回（流程实例已结束，正在运行强制撤回方式）", notes="撤回（流程实例已结束，正在运行强制撤回方式）")
	@PostMapping(value="/globalForceCallBack")
	public BaseResult globalForceCallBack(@RequestBody LcReceiveNParam lcReceiveNParam){
		return lcTaskService.globalForceCallBack(lcReceiveNParam);
	}
}
