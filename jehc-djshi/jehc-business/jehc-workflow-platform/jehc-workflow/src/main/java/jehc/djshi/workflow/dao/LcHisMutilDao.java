package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.LcHisMutil;

import java.util.List;
import java.util.Map;

/**
 * @Desc 会签节点历史人员
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcHisMutilDao {

    /**
     * 查询列表
     * @param lcHisMutil
     * @return
     */
    List<LcHisMutil> getLcHisMutilListByCondition(LcHisMutil lcHisMutil);


    /**
     * 新增
     * @param lcHisMutil
     * @return
     */
    int addLcHisMutil(LcHisMutil lcHisMutil);

    /**
     * 批量新增
     * @param lcHisMutils
     * @return
     */
    int addBatchLcHisMutil(List<LcHisMutil> lcHisMutils);



    /**
     * 删除
     * @param condition
     * @return
     */
    int delLcHisMutil(Map<String,Object> condition);

    /**
     * 获取最大批次
     * @param lcHisMutil
     * @return
     */
    Integer getMaxBatch(LcHisMutil lcHisMutil);
}
