package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcBtnService;
import jehc.djshi.workflow.dao.LcBtnDao;
import jehc.djshi.workflow.model.LcBtn;

/**
* @Desc 按钮 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:31:14
*/
@Service("lcBtnService")
public class LcBtnServiceImpl extends BaseService implements LcBtnService{
	@Autowired
	private LcBtnDao lcBtnDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcBtn> getLcBtnListByCondition(Map<String,Object> condition){
		try{
			return lcBtnDao.getLcBtnListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param btn_id 
	* @return
	*/
	public LcBtn getLcBtnById(String btn_id){
		try{
			LcBtn lcBtn = lcBtnDao.getLcBtnById(btn_id);
			return lcBtn;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcBtn 
	* @return
	*/
	public int addLcBtn(LcBtn lcBtn){
		int i = 0;
		try {
			i = lcBtnDao.addLcBtn(lcBtn);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcBtn 
	* @return
	*/
	public int updateLcBtn(LcBtn lcBtn){
		int i = 0;
		try {
			i = lcBtnDao.updateLcBtn(lcBtn);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcBtn 
	* @return
	*/
	public int updateLcBtnBySelective(LcBtn lcBtn){
		int i = 0;
		try {
			i = lcBtnDao.updateLcBtnBySelective(lcBtn);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcBtn(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcBtnDao.delLcBtn(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
