package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcGroup;
import jehc.djshi.workflow.service.LcGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 产品组
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "产品组",description = "产品组", tags = "产品组")
@RestController
@RequestMapping("/lcGroup")
public class LcGroupController extends BaseAction {
    @Autowired
    LcGroupService lcGroupService;

    /**
     * 查询列表
     * @param baseSearch
     */
    @ApiOperation(value="查询列表", notes="查询列表")
    @PostMapping(value="/list")
    @NeedLoginUnAuth
    public BaseResult<List<LcGroup>> getLcGroupList(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<LcGroup> lcGroups = lcGroupService.getLcGroupList(condition);
        for(LcGroup lcGroup:lcGroups){
            if(!StringUtil.isEmpty(lcGroup.getCreate_id())){
                OauthAccountEntity createBy = getAccount(lcGroup.getCreate_id());
                if(null != createBy){
                    lcGroup.setCreateBy(createBy.getName());
                }
            }
            if(!StringUtil.isEmpty(lcGroup.getUpdate_id())){
                OauthAccountEntity modifieBy = getAccount(lcGroup.getUpdate_id());
                if(null != modifieBy){
                    lcGroup.setModifiedBy(modifieBy.getName());
                }
            }
        }
        PageInfo<LcGroup> page = new PageInfo<LcGroup>(lcGroups);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个对象
     * @param lc_group_id
     */
    @ApiOperation(value="查询单个对象", notes="查询单个对象")
    @GetMapping(value="/get/{lc_group_id}")
    @NeedLoginUnAuth
    public BaseResult<LcGroup> getLcGroupById(@PathVariable("lc_group_id")String lc_group_id){
        LcGroup lcGroup = lcGroupService.getLcGroupById(lc_group_id);
        return outDataStr(lcGroup);
    }
    /**
     * 添加
     * @param lcGroup
     */
    @ApiOperation(value="添加", notes="添加")
    @PostMapping(value="/add")
    public BaseResult addLcGroup(@RequestBody LcGroup lcGroup){
        int i = 0;
        if(null != lcGroup){
            lcGroup.setCreate_time(getDate());
            lcGroup.setCreate_id(getXtUid());
            lcGroup.setLc_group_id(toUUID());
            i=lcGroupService.addLcGroup(lcGroup);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 修改
     * @param lcGroup
     */
    @ApiOperation(value="修改", notes="修改")
    @PutMapping(value="/update")
    public BaseResult updateLcProduct(@RequestBody LcGroup lcGroup){
        int i = 0;
        if(null != lcGroup){
            lcGroup.setUpdate_id(getXtUid());
            lcGroup.setUpdate_time(getDate());
            i=lcGroupService.updateLcGroup(lcGroup);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 删除
     * @param lc_group_id
     */
    @ApiOperation(value="删除", notes="删除")
    @DeleteMapping(value="/delete")
    public BaseResult delLcProduct(String lc_group_id){
        int i = 0;
        if(!StringUtil.isEmpty(lc_group_id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("lc_group_id",lc_group_id.split(","));
            i=lcGroupService.delLcGroup(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 查询集合
     */
    @ApiOperation(value="查询集合", notes="查询集合")
    @GetMapping(value="/find/{lc_product_id}")
    @NeedLoginUnAuth
    public BaseResult<List<LcGroup>> find(@PathVariable("lc_product_id")String lc_product_id){
        Map<String,Object> condition = new HashMap<>();
        condition.put("lc_product_id",lc_product_id);
        List<LcGroup> lcGroups = lcGroupService.getLcGroupList(condition);
        return new BaseResult(lcGroups);
    }
}
