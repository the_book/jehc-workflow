package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.ActRuVariable;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程运行时变量 核心引擎模块 非扩展
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface ActRunVariableDao {

    /**
     * 根据流程实例id删除流程变量
     * @param processInstanceId
     * @return
     */
    int delActRunVariableByInstanceId(String processInstanceId);

    /**
     * 查询运行时变量集合
     * @param condition
     * @return
     */
    List<ActRuVariable> getActRuVariableListByCondition(Map<String,Object> condition);

    /**
     * 查询单个运行时变量
     * @param id
     * @return
     */
    ActRuVariable getActRuVariableById(String id);

    /**
     * 删除运行时变量
     * @param id
     * @return
     */
    int delActRunVariable(String id);

    /**
     * 根据运行实例删除运行时变量
     * @param executionId
     * @return
     */
    int delActRunVariableByExecutionId(String executionId);

    /**
     * 创建运行时变量
     * @param actRuVariable
     * @return
     */
    int addActRunVariable(ActRuVariable actRuVariable);
}
