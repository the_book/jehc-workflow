package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.LcSignRecord;

import java.util.List;
import java.util.Map;

/**
 * @Desc 会签动态扩展业务子表
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcSignRecordDao {

    /**
     *
     * @param condition
     * @return
     */
    List<LcSignRecord> getLcSignRecordListByCondition(Map<String,Object> condition);

    /**
     * 查找最新一条会签审批记录
     * @param condition
     * @return
     */
    LcSignRecord getLcSignRecordSingle(Map<String,Object> condition);

    /**
     *
     * @param lcSignRecord
     * @return
     */
    int addLcSignRecord(LcSignRecord lcSignRecord);

    /**
     *
     * @param lcSignRecords
     * @return
     */
    int addBatchLcSignRecord(List<LcSignRecord> lcSignRecords);

    /**
     *
     * @param condition
     * @return
     */
    int delLcSignRecord(Map<String,Object> condition);

    /**
     *
     * @param condition
     * @return
     */
    Integer getMaxBatch(Map<String,Object> condition);
}
