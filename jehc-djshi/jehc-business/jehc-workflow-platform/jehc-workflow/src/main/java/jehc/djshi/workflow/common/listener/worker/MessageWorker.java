package jehc.djshi.workflow.common.listener.worker;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.util.SpringUtils;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcNodeAttribute;
import jehc.djshi.workflow.param.NodeAttributeParam;
import jehc.djshi.workflow.service.LcNodeAttributeService;
import jehc.djshi.workflow.service.LcTaskService;
import jehc.djshi.workflow.util.message.MailService;
import jehc.djshi.workflow.util.message.vo.Constant;
import jehc.djshi.workflow.util.message.vo.MailDTO;
import jehc.djshi.workflow.util.message.vo.MessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.task.IdentityLink;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc 消息发送中心线程
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class MessageWorker implements Runnable {

    /**
     *
     */
    private MessageDTO messageDTO;

    /**
     *
     */
    public MessageWorker(){

    }

    /**
     *
     * @param messageDTO
     */
    public MessageWorker(MessageDTO messageDTO){
        this.messageDTO = messageDTO;
    }

    /**
     *
     */
    public void run(){
        try {
            LcTaskService lcTaskService = SpringUtils.getBean(LcTaskService.class);
            /*
            String hid_ = "";//部署编号
            LcNodeAttributeService lcNodeAttributeService = SpringUtils.getBean(LcNodeAttributeService.class);
            NodeAttributeParam nodeAttributeParam = new NodeAttributeParam();
            nodeAttributeParam.setNodeId(messageDTO.getNodeId());
            nodeAttributeParam.setHid_(hid_);*/
            BaseResult<LcNodeAttribute> lcNodeAttributeBaseResult = lcTaskService.getLcNodeAttribute(messageDTO.getTaskId());//获取当前任务所在节点配置信息
            LcNodeAttribute lcNodeAttribute = lcNodeAttributeBaseResult.getData();
            if(null == lcNodeAttribute){
                log.warn("当前节点未配置属性");
                return;
            }
            String noticeType = lcNodeAttribute.getNotice_type();
            if(!StringUtil.isEmpty(noticeType)){
                if(noticeType.indexOf(Constant.MAIL)>=0){//邮件
                    doMail(lcNodeAttribute);
                }
                if(noticeType.indexOf(Constant.SMS)>=0){//短信

                }
                if(noticeType.indexOf(Constant.NOTICE)>=0){//站内
                    doNotify(lcNodeAttribute);
                }
                if(noticeType.indexOf(Constant.ENTERPRISEWECHAT)>=0){//企业微信

                }
                if(noticeType.indexOf(Constant.NAILNAIL)>=0){//钉钉

                }
                if(noticeType.indexOf(Constant.OFFICIALWECHAT)>=0){//微信公众号

                }

            }
        } catch (Exception e) {
            log.info("发送消息异常，异常信息：{}",e);
        }
    }



    /**
     *
     * @param candidators
     */
    private void getTaskUser(List<IdentityLink> candidators){
        if (!CollectionUtil.isEmpty(candidators)) {
            for (IdentityLink identityLink : candidators) {
                log.info("任务id：" + identityLink.getTaskId());
                log.info("流程实例id：" + identityLink.getProcessInstanceId());
                log.info("候选人：" + identityLink.getUserId());
                log.info("候选组：" + identityLink.getGroupId());
            }
        }
    }

    /**
     * 调用发送邮件线程
     * @param lcNodeAttribute
     */
    private void doMail(LcNodeAttribute lcNodeAttribute){
        Runnable runnable = new MailWorker(lcNodeAttribute,messageDTO);
        Thread thread = new Thread(runnable);
        thread.start();
    }

    /**
     * 调用发送站内线程
     * @param lcNodeAttribute
     */
    private void doNotify(LcNodeAttribute lcNodeAttribute){
        Runnable runnable = new NotifyWorker(lcNodeAttribute,messageDTO);
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
