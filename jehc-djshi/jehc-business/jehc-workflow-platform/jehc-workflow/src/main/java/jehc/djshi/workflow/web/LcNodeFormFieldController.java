package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcNodeFormField;
import jehc.djshi.workflow.service.LcNodeFormFieldService;

/**
* @Desc 节点字段 
* @Author 邓纯杰
* @CreateTime 2022-04-06 11:41:43
*/
@Api(value = "节点字段",  description = "节点字段",tags = "节点字段")
@RestController
@RequestMapping("/lcNodeFormField")
public class LcNodeFormFieldController extends BaseAction{
	@Autowired
	private LcNodeFormFieldService lcNodeFormFieldService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcNodeFormField>> getLcNodeFormFieldListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcNodeFormField> lcNodeFormFieldList = lcNodeFormFieldService.getLcNodeFormFieldListByCondition(condition);
		PageInfo<LcNodeFormField> page = new PageInfo<LcNodeFormField>(lcNodeFormFieldList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param node_field_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{node_field_id}")
	public BaseResult<LcNodeFormField> getLcNodeFormFieldById(@PathVariable("node_field_id")String node_field_id){
		LcNodeFormField lcNodeFormField = lcNodeFormFieldService.getLcNodeFormFieldById(node_field_id);
		return outDataStr(lcNodeFormField);
	}
	/**
	* 添加
	* @param lcNodeFormField 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcNodeFormField(@RequestBody LcNodeFormField lcNodeFormField){
		int i = 0;
		if(null != lcNodeFormField){
			lcNodeFormField.setNode_field_id(toUUID());
			i=lcNodeFormFieldService.addLcNodeFormField(lcNodeFormField);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcNodeFormField 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcNodeFormField(@RequestBody LcNodeFormField lcNodeFormField){
		int i = 0;
		if(null != lcNodeFormField){
			i=lcNodeFormFieldService.updateLcNodeFormField(lcNodeFormField);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param node_field_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcNodeFormField(String node_field_id){
		int i = 0;
		if(!StringUtil.isEmpty(node_field_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("node_field_id",node_field_id.split(","));
			i=lcNodeFormFieldService.delLcNodeFormField(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
