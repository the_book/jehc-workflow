package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.workflow.dao.LcListenerDao;
import jehc.djshi.workflow.model.LcListener;
import jehc.djshi.workflow.service.LcListenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程监听器
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */

@Service
public class LcListenerServiceImpl extends BaseService implements LcListenerService {
    @Autowired
    LcListenerDao lcListenerDao;

    /**
     *
     * @param condition
     * @return
     */
    public List<LcListener> getLcListenerListByCondition(Map<String, Object> condition){
        return lcListenerDao.getLcListenerListByCondition(condition);
    }

    /**
     *
     * @param lc_listener_id
     * @return
     */
    public LcListener getLcListenerById(String lc_listener_id){
        return lcListenerDao.getLcListenerById(lc_listener_id);
    }

    /**
     * 添加
     * @param lcListener
     * @return
     */
    public int addLcListener(LcListener lcListener){
        return lcListenerDao.addLcListener(lcListener);
    }

    /**
     * 修改
     * @param lcListener
     * @return
     */
    public int updateLcListener(LcListener lcListener){
        return lcListenerDao.updateLcListener(lcListener);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delLcListener(Map<String, Object> condition){
        return lcListenerDao.delLcListener(condition);
    }
}
