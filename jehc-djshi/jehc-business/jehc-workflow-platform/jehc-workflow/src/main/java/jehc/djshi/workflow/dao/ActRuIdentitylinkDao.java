package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.ActRuIdentitylink;

import java.util.List;
import java.util.Map;

/**
 * @Desc 运行时流程人员
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface ActRuIdentitylinkDao {

    /**
     * 查询运行时流程人员集合
     * @param condition
     * @return
     */
    List<ActRuIdentitylink> getActRuIdentitylinkListByCondition(Map<String,Object> condition);

    /**
     * 查询单个运行时流程人员
     * @param id
     * @return
     */
    ActRuIdentitylink getActRuIdentitylinkById(String id);

    /**
     * 删除运行时流程人员
     * @param id
     * @return
     */
    int delActRuIdentitylink(String id);

    /**
     * 删除运行时流程人员
     * @param procInstId
     * @return
     */
    int delActRuIdentitylinkByProcInstId(String procInstId);

    /**
     * 创建运行时人员
     * @param actRuIdentitylink
     * @return
     */
    int addActRuIdentitylink(ActRuIdentitylink actRuIdentitylink);

}
