package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.ActRuIdentitylink;

import java.util.List;
import java.util.Map;

public interface ActHiIdentitylinkDao {

    /**
     * 查询历史流程人员集合
     * @param condition
     * @return
     */
    List<ActRuIdentitylink> getActHiIdentitylinkList(Map<String,Object> condition);
}
