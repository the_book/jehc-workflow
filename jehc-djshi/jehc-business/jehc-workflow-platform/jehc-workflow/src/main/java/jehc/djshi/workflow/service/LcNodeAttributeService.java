package jehc.djshi.workflow.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcNodeAttribute;
import jehc.djshi.workflow.param.NodeAttributeParam;

/**
* @Desc 节点属性 
* @Author 邓纯杰
* @CreateTime 2022-03-27 22:05:30
*/
public interface LcNodeAttributeService{

	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcNodeAttribute> getLcNodeAttributeListByCondition(Map<String, Object> condition);

	/**
	* 查询对象
	* @param lc_node_attribute_id 
	* @return
	*/
	LcNodeAttribute getLcNodeAttributeById(String lc_node_attribute_id);
	/**
	* 添加
	* @param lcNodeAttribute 
	* @return
	*/
	int addLcNodeAttribute(LcNodeAttribute lcNodeAttribute);

	/**
	* 修改
	* @param lcNodeAttribute 
	* @return
	*/
	int updateLcNodeAttribute(LcNodeAttribute lcNodeAttribute);

	/**
	* 修改（根据动态条件）
	* @param lcNodeAttribute 
	* @return
	*/
	int updateLcNodeAttributeBySelective(LcNodeAttribute lcNodeAttribute);

	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcNodeAttribute(Map<String, Object> condition);

	/**
	 * 保存
	 * @param lcNodeAttribute
	 * @return
	 */
	int saveLcNodeAttribute(LcNodeAttribute lcNodeAttribute);

	/**
	 * 强制同步
	 * @param lcNodeAttribute
	 * @return
	 */
	int syncLcNodeAttribute(LcNodeAttribute lcNodeAttribute);

	/**
	 * 根据节点id+部署记录查找唯一
	 * @param nodeAttributeParam
	 * @return
	 */
	LcNodeAttribute getLcNodeAttribute(NodeAttributeParam nodeAttributeParam);

	/**
	 * 根据编号删除
	 * @param lc_node_attribute_id
	 * @return
	 */
	int delLcNodeAttributeById(String lc_node_attribute_id);
}
