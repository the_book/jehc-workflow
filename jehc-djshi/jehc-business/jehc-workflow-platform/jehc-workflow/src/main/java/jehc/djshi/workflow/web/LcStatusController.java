package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcStatus;
import jehc.djshi.workflow.service.LcStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 流程状态
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "流程状态", description = "流程状态",tags = "流程状态")
@RestController
@RequestMapping("/lcStatus")
public class LcStatusController extends BaseAction {
	@Autowired
	private LcStatusService lcStatusService;

	/**
	* 查询并分页
	* @param baseSearch
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@PostMapping(value="/list")
	@NeedLoginUnAuth
	public BasePage<List<LcStatus>> getLcStatusListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcStatus> lcStatusList = lcStatusService.getLcStatusListByCondition(condition);
		for(LcStatus lcStatus:lcStatusList){
			if(!StringUtil.isEmpty(lcStatus.getCreate_id())){
				OauthAccountEntity createBy = getAccount(lcStatus.getCreate_id());
				if(null != createBy){
					lcStatus.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(lcStatus.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(lcStatus.getUpdate_id());
				if(null != modifiedBy){
					lcStatus.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<LcStatus> page = new PageInfo<LcStatus>(lcStatusList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个对象
	* @param lc_status_id
	*/
	@ApiOperation(value="查询单个对象", notes="查询单个对象")
	@GetMapping(value="/get/{lc_status_id}")
	@NeedLoginUnAuth
	public BaseResult<LcStatus> getLcStatusById(@PathVariable("lc_status_id")String lc_status_id){
		LcStatus lcStatus = lcStatusService.getLcStatusById(lc_status_id);
		if(!StringUtil.isEmpty(lcStatus.getCreate_id())){
			OauthAccountEntity createBy = getAccount(lcStatus.getCreate_id());
			if(null != createBy){
				lcStatus.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(lcStatus.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(lcStatus.getUpdate_id());
			if(null != modifiedBy){
				lcStatus.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(lcStatus);
	}
	/**
	* 添加
	* @param lcStatus
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcStatus(@RequestBody LcStatus lcStatus){
		int i = 0;
		if(null != lcStatus){
			lcStatus.setLc_status_id(toUUID());
			i=lcStatusService.addLcStatus(lcStatus);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcStatus
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcStatus(@RequestBody LcStatus lcStatus){
		int i = 0;
		if(null != lcStatus){
			i=lcStatusService.updateLcStatus(lcStatus);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param lc_status_id
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcStatus(String lc_status_id){
		int i = 0;
		if(!StringUtil.isEmpty(lc_status_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_status_id",lc_status_id.split(","));
			i=lcStatusService.delLcStatus(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 读取数据
	*/
	@ApiOperation(value="读取数据", notes="读取数据")
	@GetMapping(value="/getLcStatusList")
	@NeedLoginUnAuth
	public List<LcStatus> getLcStatusList(){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<LcStatus> lcStatusList = lcStatusService.getLcStatusListByCondition(condition);
		return lcStatusList;
	}

}
