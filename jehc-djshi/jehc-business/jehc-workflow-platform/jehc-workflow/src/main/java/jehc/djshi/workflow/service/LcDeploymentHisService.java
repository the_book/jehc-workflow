package jehc.djshi.workflow.service;

import jehc.djshi.workflow.model.LcDeploymentHis;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程部署历史记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcDeploymentHisService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcDeploymentHis> getLcDeploymentHisListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id
	* @return
	*/
	LcDeploymentHis getLcDeploymentHisById(String id);
	/**
	* 添加
	* @param lcDeploymentHis
	* @return
	*/
	int addLcDeploymentHis(LcDeploymentHis lcDeploymentHis);
	/**
	* 修改
	* @param lcDeploymentHis
	* @return
	*/
	int updateLcDeploymentHis(LcDeploymentHis lcDeploymentHis);
	/**
	* 删除
	* @param condition
	* @return
	*/
	int delLcDeploymentHis(Map<String, Object> condition);
	/**
	* 批量添加
	* @param lcDeploymentHisList
	* @return
	*/
	int addBatchLcDeploymentHis(List<LcDeploymentHis> lcDeploymentHisList);
	/**
	* 批量修改
	* @param lcDeploymentHisList
	* @return
	*/
	int updateBatchLcDeploymentHis(List<LcDeploymentHis> lcDeploymentHisList);

	/**
	 * 查询唯一一个对象
	 * @param condition
	 * @return
	 */
	LcDeploymentHis getLcDeploymentHisUnique(Map<String, Object> condition);

	/**
	 * 查询最新唯一一个对象
	 * @param condition
	 * @return
	 */
	LcDeploymentHis getLcDeploymentHisNewUnique(Map<String, Object> condition);

	/**
	 * 根据部署编号查找对象
	 * @param lc_deployment_his_id
	 * @return
	 */
	LcDeploymentHis getLcDeploymentHisByHisId(String lc_deployment_his_id);

	/**
	 * 根据模块key查找列表按最新排序
	 * @param moduleKey
	 * @return
	 */
	LcDeploymentHis getLcDeploymentHisByModuleKey(String moduleKey);

	/**
	 * 查询最新唯一一个对象（包含bpmn）
	 */
	LcDeploymentHis getLcDeploymentHisMoreAttrNewUnique(String moduleKey);
}
