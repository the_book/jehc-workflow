package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.ActRuTask;
import jehc.djshi.workflow.service.ActRuTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

/**
 * @Desc 运行时任务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "运行时任务",description = "运行时任务", tags = "运行时任务")
@RestController
@RequestMapping("/actRuTask")
public class ActRuTaskController extends BaseAction {

    @Autowired
    private ActRuTaskService actRuTaskService;

    /**
     * 查询并分页
     * @param baseSearch
     */
    @ApiOperation(value="查询并分页", notes="查询并分页")
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    public BasePage<List<ActRuTask>> getActRuTaskListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<ActRuTask> actRuTaskList = actRuTaskService.getActRuTaskListByCondition(condition);
        PageInfo<ActRuTask> page = new PageInfo<ActRuTask>(actRuTaskList);
        return outPageBootStr(page,baseSearch);
    }
    /**
     * 查询单条记录
     * @param id
     */
    @ApiOperation(value="查询单条记录", notes="查询单条记录")
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    public BaseResult<ActRuTask> getActRuTaskById(@PathVariable("id")String id){
        ActRuTask actRuTask = actRuTaskService.getActRuTaskById(id);
        return outDataStr(actRuTask);
    }

    /**
     * 删除运行时任务
     * @param id
     * @param procInstId
     * @return
     */
    @ApiOperation(value="删除运行时任务", notes="删除运行时任务")
    @DeleteMapping(value="/delete")
    public BaseResult delActRuTask(String id,String procInstId){
        int i = actRuTaskService.delActRuTask(id);
        if(i>0){
            return BaseResult.success();
        }else {
            return BaseResult.fail();
        }
    }
}
