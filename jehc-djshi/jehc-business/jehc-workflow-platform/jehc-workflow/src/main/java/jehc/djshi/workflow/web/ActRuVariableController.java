package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.ActRuVariable;
import jehc.djshi.workflow.service.ActRunVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程运行时变量 核心引擎模块 非扩展
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "流程运行时变量", description = "流程运行时变量", tags = "流程运行时变量")
@RestController
@RequestMapping("/actRuVariable")
public class ActRuVariableController extends BaseAction {

    @Autowired
    private ActRunVariableService actRunVariableService;

    /**
     * 查询并分页
     * @param baseSearch
     */
    @ApiOperation(value="查询并分页", notes="查询并分页")
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    public BasePage<List<ActRuVariable>> getActRuVariableListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<ActRuVariable> actRuVariableList = actRunVariableService.getActRuVariableListByCondition(condition);
        PageInfo<ActRuVariable> page = new PageInfo<ActRuVariable>(actRuVariableList);
        return outPageBootStr(page,baseSearch);
    }


    /**
     * 查询单条记录
     * @param id
     */
    @ApiOperation(value="查询单条记录", notes="查询单条记录")
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    public BaseResult<ActRuVariable> getActRuVariableById(@PathVariable("id")String id){
        ActRuVariable actRuVariable = actRunVariableService.getActRuVariableById(id);
        return outDataStr(actRuVariable);
    }

    /**
     * 删除运行时变量
     * @param id
     * @param procInstId
     * @return
     */
    @ApiOperation(value="删除运行时变量", notes="删除运行时变量")
    @DeleteMapping(value="/delete")
    public BaseResult delActRunVariable(String id,String procInstId){
        int i = actRunVariableService.delActRunVariable(id);
        if(i>0){
            return BaseResult.success();
        }else {
            return BaseResult.fail();
        }
    }
}
