package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.LcProcess;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程信息存储记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcProcessDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcProcess> getLcProcessListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param lc_process_id
	* @return
	*/
	LcProcess getLcProcessById(String lc_process_id);
	/**
	* 添加
	* @param lcProcess
	* @return
	*/
	int addLcProcess(LcProcess lcProcess);
	/**
	* 修改
	* @param lcProcess
	* @return
	*/
	int updateLcProcess(LcProcess lcProcess);
	/**
	* 删除
	* @param condition
	* @return
	*/
	int delLcProcess(Map<String, Object> condition);
	/**
	* 批量添加
	* @param lcProcessList
	* @return
	*/
	int addBatchLcProcess(List<LcProcess> lcProcessList);
	/**
	* 批量修改
	* @param lcProcessList
	* @return
	*/
	int updateBatchLcProcess(List<LcProcess> lcProcessList);
	/**
	 * 发布或关闭流程
	 * @param condition
	 * @return
	 */
	int updateLcProcessStatus(Map<String, Object> condition);

	/**
	 * 根据流程模块Key查找流程定义
	 * @param moduleKey
	 * @return
	 */
	LcProcess getLcProcessByModuleKey(String moduleKey);
}
