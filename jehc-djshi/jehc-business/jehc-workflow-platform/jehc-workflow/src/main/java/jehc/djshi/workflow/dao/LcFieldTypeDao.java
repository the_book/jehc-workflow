package jehc.djshi.workflow.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcFieldType;

/**
* @Desc 字段类型 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:42:35
*/
public interface LcFieldTypeDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcFieldType> getLcFieldTypeListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param type_id 
	* @return
	*/
	LcFieldType getLcFieldTypeById(String type_id);
	/**
	* 添加
	* @param lcFieldType 
	* @return
	*/
	int addLcFieldType(LcFieldType lcFieldType);
	/**
	* 修改
	* @param lcFieldType 
	* @return
	*/
	int updateLcFieldType(LcFieldType lcFieldType);
	/**
	* 修改（根据动态条件）
	* @param lcFieldType 
	* @return
	*/
	int updateLcFieldTypeBySelective(LcFieldType lcFieldType);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcFieldType(Map<String, Object> condition);
}
