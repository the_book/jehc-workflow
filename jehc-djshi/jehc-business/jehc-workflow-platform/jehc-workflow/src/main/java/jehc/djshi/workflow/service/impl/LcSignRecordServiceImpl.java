package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.workflow.dao.LcSignRecordDao;
import jehc.djshi.workflow.model.LcSignRecord;
import jehc.djshi.workflow.service.LcSignRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 会签动态扩展业务子表
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class LcSignRecordServiceImpl extends BaseService implements LcSignRecordService {
    @Autowired
    LcSignRecordDao lcSignRecordDao;
    /**
     *
     * @param condition
     * @return
     */
    public List<LcSignRecord> getLcSignRecordListByCondition(Map<String,Object> condition){
        return lcSignRecordDao.getLcSignRecordListByCondition(condition);
    }

    /**
     * 查找最新一条会签审批记录
     * @param condition
     * @return
     */
    public LcSignRecord getLcSignRecordSingle(Map<String,Object> condition){
        return lcSignRecordDao.getLcSignRecordSingle(condition);
    }

    /**
     *
     * @param lcSignRecord
     * @return
     */
    public int addLcSignRecord(LcSignRecord lcSignRecord){
        return lcSignRecordDao.addLcSignRecord(lcSignRecord);
    }

    /**
     *
     * @param lcSignRecords
     * @return
     */
    public int addBatchLcSignRecord(List<LcSignRecord> lcSignRecords){
        return lcSignRecordDao.addBatchLcSignRecord(lcSignRecords);
    }

    /**
     *
     * @param condition
     * @return
     */
    public int delLcSignRecord(Map<String,Object> condition){
        return lcSignRecordDao.delLcSignRecord(condition);
    }

    /**
     *
     * @param condition
     * @return
     */
    public Integer getMaxBatch(Map<String,Object> condition){
        return lcSignRecordDao.getMaxBatch(condition);
    }
}
