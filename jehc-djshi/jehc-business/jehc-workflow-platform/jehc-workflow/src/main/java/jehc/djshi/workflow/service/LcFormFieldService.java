package jehc.djshi.workflow.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcFormField;

/**
* @Desc 工作流表单字段 
* @Author 邓纯杰
* @CreateTime 2022-04-04 19:17:12
*/
public interface LcFormFieldService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcFormField> getLcFormFieldListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param lc_form_field_id 
	* @return
	*/
	LcFormField getLcFormFieldById(String lc_form_field_id);
	/**
	* 添加
	* @param lcFormField 
	* @return
	*/
	int addLcFormField(LcFormField lcFormField);
	/**
	* 修改
	* @param lcFormField 
	* @return
	*/
	int updateLcFormField(LcFormField lcFormField);
	/**
	* 修改（根据动态条件）
	* @param lcFormField 
	* @return
	*/
	int updateLcFormFieldBySelective(LcFormField lcFormField);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcFormField(Map<String, Object> condition);
}
