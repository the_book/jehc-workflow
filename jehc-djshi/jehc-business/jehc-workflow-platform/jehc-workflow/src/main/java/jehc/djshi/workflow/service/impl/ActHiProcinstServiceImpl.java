package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.workflow.dao.ActHiProcinstDao;
import jehc.djshi.workflow.service.ActHiProcinstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Desc 历史实例
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class ActHiProcinstServiceImpl extends BaseService implements ActHiProcinstService {
    @Autowired
    ActHiProcinstDao actHiProcinstDao;

    /**
     * 根据流程定义统计流程实例运行平均时间
     * @param proc_def_id
     * @return
     */
    public String getActHiProcinstAvgTime(String proc_def_id){
        return actHiProcinstDao.getActHiProcinstAvgTime(proc_def_id);
    }
}
