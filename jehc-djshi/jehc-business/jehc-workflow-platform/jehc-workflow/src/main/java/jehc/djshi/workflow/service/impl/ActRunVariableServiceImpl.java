package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.dao.ActRunVariableDao;
import jehc.djshi.workflow.model.ActRuVariable;
import jehc.djshi.workflow.service.ActRunVariableService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程运行时变量 核心引擎模块 非扩展
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class ActRunVariableServiceImpl extends BaseService implements ActRunVariableService {

    @Autowired
    ActRunVariableDao actRunVariableDao;

    /**
     * 根据流程实例id删除流程变量
     * @param processInstanceId
     * @return
     */
    public int delActRunVariableByInstanceId(String processInstanceId){
        if(StringUtil.isEmpty(processInstanceId)){
            return -1;
        }
        return actRunVariableDao.delActRunVariableByInstanceId(processInstanceId);
    }



    /**
     * 查询运行时变量集合
     * @param condition
     * @return
     */
    public List<ActRuVariable> getActRuVariableListByCondition(Map<String,Object> condition){
        return actRunVariableDao.getActRuVariableListByCondition(condition);
    }

    /**
     * 查询单个运行时变量
     * @param id
     * @return
     */
    public ActRuVariable getActRuVariableById(String id){
        return actRunVariableDao.getActRuVariableById(id);
    }

    /**
     * 删除运行时变量
     * @param id
     * @return
     */
    public int delActRunVariable(String id){
        return actRunVariableDao.delActRunVariable(id);
    }

    /**
     * 根据运行实例删除运行时变量
     * @param executionId
     * @return
     */
    public int delActRunVariableByExecutionId(String executionId){
        return actRunVariableDao.delActRunVariableByExecutionId(executionId);
    }
}
