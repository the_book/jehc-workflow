package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcListener;
import jehc.djshi.workflow.service.LcListenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 流程监听器
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "流程监听器",description = "流程监听器", tags = "流程监听器")
@RestController
@RequestMapping("/lcListener")
public class LcListenerController extends BaseAction {

    @Autowired
    private LcListenerService lcListenerService;

    /**
     * 查询列表
     * @param baseSearch
     */
    @ApiOperation(value="查询列表", notes="查询列表")
    @PostMapping(value="/list")
    @NeedLoginUnAuth
    public BaseResult<List<LcListener>> getLcListenerListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        if(null != condition.get("categories")){
            String categories = (String)condition.get("categories");
            condition.put("categories",categories.split(","));
        }
        commonHPager(baseSearch);
        List<LcListener> lcListeners = lcListenerService.getLcListenerListByCondition(condition);
        for(LcListener lcListener:lcListeners){
            if(!StringUtil.isEmpty(lcListener.getCreate_id())){
                OauthAccountEntity createBy = getAccount(lcListener.getCreate_id());
                if(null != createBy){
                    lcListener.setCreateBy(createBy.getName());
                }
            }
            if(!StringUtil.isEmpty(lcListener.getUpdate_id())){
                OauthAccountEntity modifieBy = getAccount(lcListener.getUpdate_id());
                if(null != modifieBy){
                    lcListener.setModifiedBy(modifieBy.getName());
                }
            }
        }
        PageInfo<LcListener> page = new PageInfo<LcListener>(lcListeners);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个对象
     * @param lc_listener_id
     */
    @ApiOperation(value="查询单个对象", notes="查询单个对象")
    @GetMapping(value="/get/{lc_listener_id}")
    @NeedLoginUnAuth
    public BaseResult<LcListener> getLcListenerById(@PathVariable("lc_listener_id")String lc_listener_id){
        LcListener lcListener = lcListenerService.getLcListenerById(lc_listener_id);
        if(!StringUtil.isEmpty(lcListener.getCreate_id())){
            OauthAccountEntity createBy = getAccount(lcListener.getCreate_id());
            if(null != createBy){
                lcListener.setCreateBy(createBy.getName());
            }
        }
        if(!StringUtil.isEmpty(lcListener.getUpdate_id())){
            OauthAccountEntity modifiedBy = getAccount(lcListener.getUpdate_id());
            if(null != modifiedBy){
                lcListener.setModifiedBy(modifiedBy.getName());
            }
        }
        return outDataStr(lcListener);
    }
    /**
     * 添加
     * @param lcListener
     */
    @ApiOperation(value="添加", notes="添加")
    @PostMapping(value="/add")
    public BaseResult addLcListener(@RequestBody LcListener lcListener){
        int i = 0;
        if(null != lcListener){
            lcListener.setCreate_time(getDate());
            lcListener.setCreate_id(getXtUid());
            lcListener.setLc_listener_id(toUUID());
            i=lcListenerService.addLcListener(lcListener);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 修改
     * @param lcListener
     */
    @ApiOperation(value="修改", notes="修改")
    @PutMapping(value="/update")
    public BaseResult updateLcListener(@RequestBody LcListener lcListener){
        int i = 0;
        if(null != lcListener){
            lcListener.setUpdate_id(getXtUid());
            lcListener.setUpdate_time(getDate());
            i=lcListenerService.updateLcListener(lcListener);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 删除
     * @param lc_listener_id
     */
    @ApiOperation(value="删除", notes="删除")
    @DeleteMapping(value="/delete")
    public BaseResult delLcListener(String lc_listener_id){
        int i = 0;
        if(!StringUtil.isEmpty(lc_listener_id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("lc_listener_id",lc_listener_id.split(","));
            i=lcListenerService.delLcListener(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
}
