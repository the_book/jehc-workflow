package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcApply;
import jehc.djshi.workflow.service.LcApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 流程申请
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "流程申请", description = "流程申请", tags = "流程申请")
@RestController
@RequestMapping("/lcApply")
public class LcApplyController extends BaseAction {
	@Autowired
	private LcApplyService lcApplyService;

	/**
	 * 查询并分页
	 * @param baseSearch
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@PostMapping(value="/list")
	@NeedLoginUnAuth
	public BasePage<List<LcApply>> getLcApplyListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		condition.put("create_id",getXtUid());
		List<LcApply> lcApplyList = lcApplyService.getLcApplyListByCondition(condition);
		PageInfo<LcApply> page = new PageInfo<LcApply>(lcApplyList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 查询单条记录
	 * @param lc_apply_id
	 * @return
	 */
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@GetMapping(value="/get/{lc_apply_id}")
	@NeedLoginUnAuth
	public BaseResult<LcApply> getLcApplyById(@PathVariable("lc_apply_id")String lc_apply_id){
		LcApply lcApply = lcApplyService.getLcApplyById(lc_apply_id);
		return outDataStr(lcApply);
	}

	/***
	 * 添加
	 * @param lcApply
	 * @return
	 */
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcApply(@RequestBody LcApply lcApply){
		int i = 0;
		if(null != lcApply){
			lcApply.setLcApplyId(toUUID());
			i=lcApplyService.addLcApply(lcApply);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcApply
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcApply(@RequestBody LcApply lcApply){
		int i = 0;
		if(null != lcApply){
			i=lcApplyService.updateLcApply(lcApply);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 删除
	* @param lc_apply_id
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcApply(String lc_apply_id){
		int i = 0;
		if(!StringUtil.isEmpty(lc_apply_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_apply_id",lc_apply_id.split(","));
			i=lcApplyService.delLcApply(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
