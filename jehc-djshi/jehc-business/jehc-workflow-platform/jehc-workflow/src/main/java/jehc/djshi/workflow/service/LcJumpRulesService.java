package jehc.djshi.workflow.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcJumpRules;

/**
* @Desc 自定义跳转规则 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:55:44
*/
public interface LcJumpRulesService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcJumpRules> getLcJumpRulesListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param jump_rules_id 
	* @return
	*/
	LcJumpRules getLcJumpRulesById(String jump_rules_id);
	/**
	* 添加
	* @param lcJumpRules 
	* @return
	*/
	int addLcJumpRules(LcJumpRules lcJumpRules);
	/**
	* 修改
	* @param lcJumpRules 
	* @return
	*/
	int updateLcJumpRules(LcJumpRules lcJumpRules);
	/**
	* 修改（根据动态条件）
	* @param lcJumpRules 
	* @return
	*/
	int updateLcJumpRulesBySelective(LcJumpRules lcJumpRules);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcJumpRules(Map<String, Object> condition);
}
