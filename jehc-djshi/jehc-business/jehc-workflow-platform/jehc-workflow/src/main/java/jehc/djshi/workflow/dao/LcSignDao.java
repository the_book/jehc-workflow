package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.LcSign;

import java.util.List;
import java.util.Map;

/**
 * @Desc 会签动态扩展业务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcSignDao {
    /**
     *
     * @param condition
     * @return
     */
    List<LcSign> getLcSignListByCondition(Map<String,Object> condition);

    /**
     *
     * @param lcSign
     * @return
     */
    int addLcSign(LcSign lcSign);

    /**
     *
     * @param lcSigns
     * @return
     */
    int addBatchLcSign(List<LcSign> lcSigns);


    /**
     *
     * @param condition
     * @return
     */
    int delLcSign(Map<String,Object> condition);


    /**
     *
     * @param condition
     * @return
     */
    Integer getMaxBatch(Map<String,Object> condition);

    /**
     * 更新会签
     * @param lcSign
     * @return
     */
    int updateLcSign(LcSign lcSign);
}
