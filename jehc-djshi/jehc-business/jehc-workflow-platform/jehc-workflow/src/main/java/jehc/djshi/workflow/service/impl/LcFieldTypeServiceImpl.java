package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcFieldTypeService;
import jehc.djshi.workflow.dao.LcFieldTypeDao;
import jehc.djshi.workflow.model.LcFieldType;

/**
* @Desc 字段类型 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:42:35
*/
@Service("lcFieldTypeService")
public class LcFieldTypeServiceImpl extends BaseService implements LcFieldTypeService{
	@Autowired
	private LcFieldTypeDao lcFieldTypeDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcFieldType> getLcFieldTypeListByCondition(Map<String,Object> condition){
		try{
			return lcFieldTypeDao.getLcFieldTypeListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param type_id 
	* @return
	*/
	public LcFieldType getLcFieldTypeById(String type_id){
		try{
			LcFieldType lcFieldType = lcFieldTypeDao.getLcFieldTypeById(type_id);
			return lcFieldType;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcFieldType 
	* @return
	*/
	public int addLcFieldType(LcFieldType lcFieldType){
		int i = 0;
		try {
			i = lcFieldTypeDao.addLcFieldType(lcFieldType);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcFieldType 
	* @return
	*/
	public int updateLcFieldType(LcFieldType lcFieldType){
		int i = 0;
		try {
			i = lcFieldTypeDao.updateLcFieldType(lcFieldType);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcFieldType 
	* @return
	*/
	public int updateLcFieldTypeBySelective(LcFieldType lcFieldType){
		int i = 0;
		try {
			i = lcFieldTypeDao.updateLcFieldTypeBySelective(lcFieldType);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcFieldType(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcFieldTypeDao.delLcFieldType(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
