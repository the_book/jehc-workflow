package jehc.djshi.workflow.service;

import jehc.djshi.workflow.model.ActHiTaskinst;

/**
 * @Desc 历史任务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface ActHiTaskinstService {

    /**
     * 根据流程定义统计流程任务运行平均时间
     * @param proc_def_id
     * @return
     */
    String getActHiTaskInstAvgTime(String proc_def_id);

    /**
     * 更新
     * @param actHiTaskinst
     * @return
     */
    int updateActHiTaskInst(ActHiTaskinst actHiTaskinst);
}
