package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcFieldHtmlService;
import jehc.djshi.workflow.dao.LcFieldHtmlDao;
import jehc.djshi.workflow.model.LcFieldHtml;

/**
* @Desc 字段html类型 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:38:14
*/
@Service("lcFieldHtmlService")
public class LcFieldHtmlServiceImpl extends BaseService implements LcFieldHtmlService{
	@Autowired
	private LcFieldHtmlDao lcFieldHtmlDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcFieldHtml> getLcFieldHtmlListByCondition(Map<String,Object> condition){
		try{
			return lcFieldHtmlDao.getLcFieldHtmlListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param lc_field_html_id 
	* @return
	*/
	public LcFieldHtml getLcFieldHtmlById(String lc_field_html_id){
		try{
			LcFieldHtml lcFieldHtml = lcFieldHtmlDao.getLcFieldHtmlById(lc_field_html_id);
			return lcFieldHtml;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcFieldHtml 
	* @return
	*/
	public int addLcFieldHtml(LcFieldHtml lcFieldHtml){
		int i = 0;
		try {
			i = lcFieldHtmlDao.addLcFieldHtml(lcFieldHtml);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcFieldHtml 
	* @return
	*/
	public int updateLcFieldHtml(LcFieldHtml lcFieldHtml){
		int i = 0;
		try {
			i = lcFieldHtmlDao.updateLcFieldHtml(lcFieldHtml);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcFieldHtml 
	* @return
	*/
	public int updateLcFieldHtmlBySelective(LcFieldHtml lcFieldHtml){
		int i = 0;
		try {
			i = lcFieldHtmlDao.updateLcFieldHtmlBySelective(lcFieldHtml);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcFieldHtml(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcFieldHtmlDao.delLcFieldHtml(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
