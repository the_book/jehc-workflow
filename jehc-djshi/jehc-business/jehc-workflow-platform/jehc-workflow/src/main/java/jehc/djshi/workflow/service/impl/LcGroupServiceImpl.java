package jehc.djshi.workflow.service.impl;

import jehc.djshi.workflow.dao.LcGroupDao;
import jehc.djshi.workflow.model.LcGroup;
import jehc.djshi.workflow.service.LcGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 产品组
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class LcGroupServiceImpl implements LcGroupService {

    @Autowired
    private LcGroupDao lcGroupDao;

    /**
     *
     * @param condition
     * @return
     */
    public List<LcGroup> getLcGroupList(Map<String,Object> condition){
        return lcGroupDao.getLcGroupList(condition);
    }

    /**
     *
     * @return
     */
    public LcGroup getLcGroupById(String lc_group_id){
        return lcGroupDao.getLcGroupById(lc_group_id);
    }

    /**
     * 添加
     * @param lcGroup
     * @return
     */
    public int addLcGroup(LcGroup lcGroup){
        return lcGroupDao.addLcGroup(lcGroup);
    }
    /**
     * 修改
     * @param lcGroup
     * @return
     */
    public int updateLcGroup(LcGroup lcGroup){
        return lcGroupDao.updateLcGroup(lcGroup);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delLcGroup(Map<String, Object> condition){
        return lcGroupDao.delLcGroup(condition);
    }
}
