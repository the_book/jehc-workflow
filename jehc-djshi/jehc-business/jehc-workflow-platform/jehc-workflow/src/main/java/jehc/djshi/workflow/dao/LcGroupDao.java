package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.LcGroup;

import java.util.List;
import java.util.Map;

/**
 * @Desc 产品组
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcGroupDao {
    /**
     *
     * @param condition
     * @return
     */
    List<LcGroup> getLcGroupList(Map<String, Object> condition);

    /**
     *
     * @return
     */
    LcGroup getLcGroupById(String lc_group_id);

    /**
     * 添加
     * @param lcGroup
     * @return
     */
    int addLcGroup(LcGroup lcGroup);
    /**
     * 修改
     * @param lcGroup
     * @return
     */
    int updateLcGroup(LcGroup lcGroup);
    /**
     * 删除
     * @param condition
     * @return
     */
    int delLcGroup(Map<String, Object> condition);
}
