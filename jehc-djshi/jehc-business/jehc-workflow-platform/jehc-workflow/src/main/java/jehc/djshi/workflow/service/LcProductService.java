package jehc.djshi.workflow.service;

import jehc.djshi.workflow.model.LcProduct;

import java.util.List;
import java.util.Map;

/**
 * @Desc 产品线
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcProductService {
    /**
     *
     * @param condition
     * @return
     */
    List<LcProduct> getLcProductListByCondition(Map<String, Object> condition);

    /**
     * 查询对象
     * @param lc_product_id
     * @return
     */
    LcProduct getLcProductById(String lc_product_id);

    /**
     * 添加
     * @param lcProduct
     * @return
     */
    int addLcProduct(LcProduct lcProduct);
    /**
     * 修改
     * @param lcProduct
     * @return
     */
    int updateLcProduct(LcProduct lcProduct);
    /**
     * 删除
     * @param condition
     * @return
     */
    int delLcProduct(Map<String, Object> condition);
}
