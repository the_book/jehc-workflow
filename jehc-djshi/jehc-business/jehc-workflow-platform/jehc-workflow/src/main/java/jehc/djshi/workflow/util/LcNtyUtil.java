package jehc.djshi.workflow.util;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcNotifyTemplate;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
/**
 * @Desc 通知工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class LcNtyUtil {

    /**
     * 根据类型 查找唯一的通知模板
     * @param lcNotifyTemplates
     * @param type
     * @return
     */
    public static LcNotifyTemplate getlcNotifyTemplate(List<LcNotifyTemplate> lcNotifyTemplates,String type){
        if(StringUtil.isEmpty(type)){
            return null;
        }
        LcNotifyTemplate template = null;
        if(CollectionUtil.isNotEmpty(lcNotifyTemplates)){
            for(LcNotifyTemplate lcNotifyTemplate: lcNotifyTemplates){
                if(lcNotifyTemplate.getType().equals(type)){
                    template = lcNotifyTemplate;
                    break;
                }
            }
        }
        return template;
    }
}
