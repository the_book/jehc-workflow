package jehc.djshi.workflow.common.listener.worker;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseNotifyEntity;
import jehc.djshi.common.base.BaseNotifyReceiverEntity;
import jehc.djshi.common.base.BaseUtils;
import jehc.djshi.common.entity.UserinfoEntity;
import jehc.djshi.common.util.SpringUtils;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcNodeAttribute;
import jehc.djshi.workflow.model.LcNotifyTemplate;
import jehc.djshi.workflow.util.LcNtyUtil;
import jehc.djshi.workflow.util.message.vo.Constant;
import jehc.djshi.workflow.util.message.vo.MessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.task.IdentityLink;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 站内通知发送业务处理线程
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class NotifyWorker  implements Runnable{
    /**
     *
     */
    private MessageDTO messageDTO;

    private LcNodeAttribute lcNodeAttribute;

    /**
     *
     */
    public NotifyWorker(){

    }

    /**
     *
     * @param messageDTO
     */
    public NotifyWorker(MessageDTO messageDTO){
        this.messageDTO = messageDTO;
    }

    /**
     *
     * @param messageDTO
     */
    public NotifyWorker(LcNodeAttribute lcNodeAttribute,MessageDTO messageDTO){
        this.messageDTO = messageDTO;
        this.lcNodeAttribute = lcNodeAttribute;
    }

    /**
     *
     */
    public void run(){
        try {
            LcNotifyTemplate template = null;
            if(null != lcNodeAttribute){
                List<LcNotifyTemplate> lcNotifyTemplates = lcNodeAttribute.getLcNotifyTemplates();
                template = LcNtyUtil.getlcNotifyTemplate(lcNotifyTemplates, Constant.NOTICE);
            }
            BaseNotifyEntity baseNotifyEntity =  new BaseNotifyEntity();

            if(null == template){//默认
                baseNotifyEntity.setTitle(Constant.NOTICE_SUBJECT);
                baseNotifyEntity.setContent(Constant.NOTICE_HTML);
            }else{
                baseNotifyEntity.setContent(template.getContent());
                baseNotifyEntity.setTitle(template.getTitle());
            }
            List<String> userIds = new ArrayList<>();
            List<String> departIds = new ArrayList<>();
            List<String> postIds = new ArrayList<>();
            List<String> roleIds = new ArrayList<>();
            List<IdentityLink> identityLinks = messageDTO.getCandidators();
            /*
            if(!StringUtil.isEmpty(messageDTO.getAssigne())){
                userIds.add(messageDTO.getAssigne());
            }
            */
            Map<String,Object> condition = new HashMap<>();
            if(CollectionUtil.isNotEmpty(identityLinks)){
                for(IdentityLink identityLink: identityLinks){
                    if(!StringUtil.isEmpty(identityLink.getUserId())){
                        userIds.add(identityLink.getUserId());
                    }
                    if(!StringUtil.isEmpty(identityLink.getGroupId())){
                        departIds.add(identityLink.getGroupId().replaceAll("DEPART:",""));
                    }
                    if(!StringUtil.isEmpty(identityLink.getGroupId())){
                        postIds.add(identityLink.getGroupId().replaceAll("POST:",""));
                    }
                    if(!StringUtil.isEmpty(identityLink.getGroupId())){
                        roleIds.add(identityLink.getGroupId().replaceAll("ROLE:",""));
                    }
                }
            }
            if(CollectionUtil.isNotEmpty(userIds)){
                condition.put("xt_userinfo_id",userIds.toArray(new String[]{}));
            }
            if(CollectionUtil.isNotEmpty(postIds)){
                condition.put("xt_post_id",postIds.toArray(new String[]{}));
            }
            if(CollectionUtil.isNotEmpty(departIds)){
                condition.put("xt_departinfo_id",departIds.toArray(new String[]{}));
            }
            List<BaseNotifyReceiverEntity> baseNotifyReceiverEntities = new ArrayList<>();
            BaseUtils baseUtils = SpringUtils.getBean(BaseUtils.class);
            if(CollectionUtil.isNotEmpty(condition)){
                List<UserinfoEntity> userinfoEntities = baseUtils.getUserinfoEntityList(condition);
                if(CollectionUtil.isNotEmpty(userinfoEntities)){
                    for(UserinfoEntity userinfoEntity: userinfoEntities){
                        BaseNotifyReceiverEntity baseNotifyReceiverEntity = new BaseNotifyReceiverEntity();
                        baseNotifyReceiverEntity.setReceive_id(userinfoEntity.getXt_userinfo_id());
                        baseNotifyReceiverEntity.setReceive_name(userinfoEntity.getXt_userinfo_realName());
                        baseNotifyReceiverEntities.add(baseNotifyReceiverEntity);
                    }
                }
            }
            if(CollectionUtil.isNotEmpty(baseNotifyReceiverEntities)){
                baseNotifyEntity.setNotifyReceivers(baseNotifyReceiverEntities);
                baseUtils.createNotify(baseNotifyEntity);
            }else{
                log.warn("没有接收人，不能发送站内通知！");
            }
        }catch (Exception e){
            log.error("发送站内通知失败，异常信息：{}",e);
        }
    }
}
