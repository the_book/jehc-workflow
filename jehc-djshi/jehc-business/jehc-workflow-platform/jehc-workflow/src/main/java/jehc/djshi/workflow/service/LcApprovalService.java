package jehc.djshi.workflow.service;

import jehc.djshi.workflow.model.LcApproval;
import jehc.djshi.workflow.param.LcApprovalParam;

import java.util.List;
import java.util.Map;

/**
 * @Desc 工作流批审
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcApprovalService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcApproval> getLcApprovalListByCondition(Map<String, Object> condition);

	/**
	* 查询对象
	* @param lcApprovalId
	* @return
	*/
	LcApproval getLcApprovalById(String lcApprovalId);

	/**
	* 添加
	* @param lcApproval
	* @return
	*/
	int addLcApproval(LcApproval lcApproval);

	/**
	* 删除
	* @param condition
	* @return
	*/
	int delLcApproval(Map<String, Object> condition);

	/**
	 * 根据业务Key批量删除
	 * @param condition
	 * @return
	 */
	int delLcApprovalByBusinessKey(Map<String, Object> condition);

	/**
	 * 查询审批记录
	 * @param lcApprovalParam
	 * @return
	 */
	List<LcApproval> getLcApprovalList(LcApprovalParam lcApprovalParam);

}
