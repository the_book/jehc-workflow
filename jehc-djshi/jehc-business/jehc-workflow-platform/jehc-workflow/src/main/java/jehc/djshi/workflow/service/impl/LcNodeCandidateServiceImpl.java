package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcNodeCandidateService;
import jehc.djshi.workflow.dao.LcNodeCandidateDao;
import jehc.djshi.workflow.model.LcNodeCandidate;

/**
* @Desc 节点办理人 
* @Author 邓纯杰
* @CreateTime 2022-04-03 20:12:39
*/
@Service("lcNodeCandidateService")
public class LcNodeCandidateServiceImpl extends BaseService implements LcNodeCandidateService{
	@Autowired
	private LcNodeCandidateDao lcNodeCandidateDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcNodeCandidate> getLcNodeCandidateListByCondition(Map<String,Object> condition){
		try{
			return lcNodeCandidateDao.getLcNodeCandidateListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param lc_node_candidate_id 
	* @return
	*/
	public LcNodeCandidate getLcNodeCandidateById(String lc_node_candidate_id){
		try{
			LcNodeCandidate lcNodeCandidate = lcNodeCandidateDao.getLcNodeCandidateById(lc_node_candidate_id);
			return lcNodeCandidate;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcNodeCandidate 
	* @return
	*/
	public int addLcNodeCandidate(LcNodeCandidate lcNodeCandidate){
		int i = 0;
		try {
			i = lcNodeCandidateDao.addLcNodeCandidate(lcNodeCandidate);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcNodeCandidate 
	* @return
	*/
	public int updateLcNodeCandidate(LcNodeCandidate lcNodeCandidate){
		int i = 0;
		try {
			i = lcNodeCandidateDao.updateLcNodeCandidate(lcNodeCandidate);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcNodeCandidate 
	* @return
	*/
	public int updateLcNodeCandidateBySelective(LcNodeCandidate lcNodeCandidate){
		int i = 0;
		try {
			i = lcNodeCandidateDao.updateLcNodeCandidateBySelective(lcNodeCandidate);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcNodeCandidate(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcNodeCandidateDao.delLcNodeCandidate(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
