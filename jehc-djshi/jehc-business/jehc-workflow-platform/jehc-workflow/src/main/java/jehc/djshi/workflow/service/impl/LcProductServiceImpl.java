package jehc.djshi.workflow.service.impl;

import jehc.djshi.workflow.dao.LcProductDao;
import jehc.djshi.workflow.model.LcProduct;
import jehc.djshi.workflow.service.LcProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 产品线
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class LcProductServiceImpl implements LcProductService {
    @Autowired
    private LcProductDao lcProductDao;

    /**
     *
     * @param condition
     * @return
     */
    public List<LcProduct> getLcProductListByCondition(Map<String,Object> condition){
        return lcProductDao.getLcProductListByCondition(condition);
    }

    /**
     * 查询对象
     * @param lc_product_id
     * @return
     */
    public LcProduct getLcProductById(String lc_product_id){
        return lcProductDao.getLcProductById(lc_product_id);
    }

    /**
     * 添加
     * @param lcProduct
     * @return
     */
    public int addLcProduct(LcProduct lcProduct){
        return lcProductDao.addLcProduct(lcProduct);
    }
    /**
     * 修改
     * @param lcProduct
     * @return
     */
    public int updateLcProduct(LcProduct lcProduct){
        return lcProductDao.updateLcProduct(lcProduct);
    }
    /**
     * 删除
     * @param condition
     * @return
     */
    public int delLcProduct(Map<String, Object> condition){
        return lcProductDao.delLcProduct(condition);
    }
}
