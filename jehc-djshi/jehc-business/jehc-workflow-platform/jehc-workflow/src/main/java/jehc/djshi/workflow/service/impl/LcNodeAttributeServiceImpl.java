package jehc.djshi.workflow.service.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.dao.*;
import jehc.djshi.workflow.model.*;
import jehc.djshi.workflow.param.NodeAttributeParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcNodeAttributeService;

/**
* @Desc 节点属性 
* @Author 邓纯杰
* @CreateTime 2022-03-27 22:05:30
*/
@Service("lcNodeAttributeService")
public class LcNodeAttributeServiceImpl extends BaseService implements LcNodeAttributeService{

	@Autowired
	private LcNodeAttributeDao lcNodeAttributeDao;

	@Autowired
	private LcJumpRulesDao lcJumpRulesDao;

	@Autowired
	private LcNodeBtnDao lcNodeBtnDao;

	@Autowired
	private LcNodeCandidateDao lcNodeCandidateDao;

	@Autowired
	private LcNodeFormFieldDao lcNodeFormFieldDao;

	@Autowired
	private LcBtnDao lcBtnDao;

	@Autowired
	private LcProcessDao lcProcessDao;

	@Autowired
	private LcDeploymentHisDao lcDeploymentHisDao;

	@Autowired
	private LcNotifyTemplateDao lcNotifyTemplateDao;

	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcNodeAttribute> getLcNodeAttributeListByCondition(Map<String,Object> condition){
		try{
			return lcNodeAttributeDao.getLcNodeAttributeListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param lc_node_attribute_id 
	* @return
	*/
	public LcNodeAttribute getLcNodeAttributeById(String lc_node_attribute_id){
		try{
			LcNodeAttribute lcNodeAttribute = lcNodeAttributeDao.getLcNodeAttributeById(lc_node_attribute_id);
			if(null != lcNodeAttribute){
				if(!StringUtil.isEmpty(lcNodeAttribute.getMounts_id())){//查询bpm文件
					LcProcess lcProcess = lcProcessDao.getLcProcessById(lcNodeAttribute.getMounts_id());
					if(null != lcProcess){
						lcNodeAttribute.setBpm(lcProcess.getLc_process_bpmn());
					}
				}
				if(!StringUtil.isEmpty(lcNodeAttribute.getHid_())){//查询bpm文件
					LcDeploymentHis lcDeploymentHis = lcDeploymentHisDao.getLcDeploymentHisById(lcNodeAttribute.getHid_());
					if(null != lcDeploymentHis){
						lcNodeAttribute.setBpm(lcDeploymentHis.getLc_process_bpmn());
					}
				}
				Map<String,Object> condition = new HashMap<>();
				condition.put("lc_node_attribute_id",lc_node_attribute_id);
				List<LcJumpRules> lcJumpRules = lcJumpRulesDao.getLcJumpRulesListByCondition(condition);
				List<LcJumpRules> btnRules = new ArrayList<>();
				if(!CollectionUtil.isEmpty(lcJumpRules)){
					List<LcJumpRules> jumpRulesList = new ArrayList<>();
					//过滤掉按钮下的自由规则
					for(LcJumpRules rules : lcJumpRules){
						if(StringUtil.isEmpty(rules.getNode_btn_id())){
							jumpRulesList.add(rules);
						}else{
							btnRules.add(rules);
						}
					}
					if(!CollectionUtil.isEmpty(jumpRulesList)){
						lcNodeAttribute.setLcJumpRules(jumpRulesList);
					}
				}
				List<LcNodeBtn> lcNodeBtns = lcNodeBtnDao.getLcNodeBtnListByCondition(condition);
				if(!CollectionUtil.isEmpty(lcNodeBtns)){
					for(LcNodeBtn lcNodeBtn : lcNodeBtns){
						if(StringUtil.isEmpty(lcNodeBtn.getBtn_label())){
							lcNodeBtn.setBtn_label(lcNodeBtn.get_label());//如果自定义按钮扩展label不存在则调用按钮中label
						}
						if(StringUtil.isEmpty(lcNodeBtn.getBtn_action())){
							lcNodeBtn.setBtn_action(lcNodeBtn.getB_action());//如果自定义扩展action为空 则调用按钮中action
						}

						if(!StringUtil.isEmpty(lcNodeBtn.getBtn_id())){
							List<LcJumpRules> lcBtnRules = new ArrayList<>();
							if(!CollectionUtil.isEmpty(btnRules)){
								for(LcJumpRules rules: btnRules){
									if(rules.getNode_btn_id().equals(lcNodeBtn.getNode_btn_id())){
										lcBtnRules.add(rules);
									}
								}
							}
							if(!CollectionUtil.isEmpty(lcBtnRules)){
								lcNodeBtn.setLcJumpRules(lcBtnRules);//设置按钮对应的节点跳转规则
							}
							lcNodeBtn.setLcBtn(lcBtnDao.getLcBtnById(lcNodeBtn.getBtn_id()));
						}
					}
					lcNodeAttribute.setLcNodeBtns(lcNodeBtns);
				}
				List<LcNodeCandidate> lcNodeCandidates = lcNodeCandidateDao.getLcNodeCandidateListByCondition(condition);
				if(!CollectionUtil.isEmpty(lcNodeCandidates)){
					if(lcNodeCandidates.size() > 1){
						throw new ExceptionUtil("查出多个审批记录，错误！");
					}
					lcNodeAttribute.setLcNodeCandidate(lcNodeCandidates);
				}

				List<LcNodeFormField> lcNodeFormFields = lcNodeFormFieldDao.getLcNodeFormFieldListByCondition(condition);
				if(!CollectionUtil.isEmpty(lcNodeFormFields)){
					lcNodeAttribute.setNodeFormField(lcNodeFormFields);
				}

				//处理通知模板
				if(!StringUtil.isEmpty(lcNodeAttribute.getNotify_template_ids())){
					condition = new HashMap<>();
					condition.put("notifyTemplateIds",lcNodeAttribute.getNotify_template_ids().split(","));
				}
				List<LcNotifyTemplate> lcNotifyTemplates =  lcNotifyTemplateDao.getLcNotifyTemplateListByCondition(condition);
				if(!CollectionUtil.isEmpty(lcNotifyTemplates)){
					lcNodeAttribute.setLcNotifyTemplates(lcNotifyTemplates);
				}
			}
			return lcNodeAttribute;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcNodeAttribute 
	* @return
	*/
	public int addLcNodeAttribute(LcNodeAttribute lcNodeAttribute){
		int i = 0;
		try {
			i = lcNodeAttributeDao.addLcNodeAttribute(lcNodeAttribute);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcNodeAttribute 
	* @return
	*/
	public int updateLcNodeAttribute(LcNodeAttribute lcNodeAttribute){
		int i = 0;
		try {
			i = lcNodeAttributeDao.updateLcNodeAttribute(lcNodeAttribute);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcNodeAttribute 
	* @return
	*/
	public int updateLcNodeAttributeBySelective(LcNodeAttribute lcNodeAttribute){
		int i = 0;
		try {
			i = lcNodeAttributeDao.updateLcNodeAttributeBySelective(lcNodeAttribute);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcNodeAttribute(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcNodeAttributeDao.delLcNodeAttribute(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}


	/**
	 * 保存
	 * @param lcNodeAttribute
	 * @return
	 */
	public int saveLcNodeAttribute(LcNodeAttribute lcNodeAttribute){
		int i = 0;
		try {
			if(StringUtil.isEmpty(lcNodeAttribute.getHid_()) && StringUtil.isEmpty(lcNodeAttribute.getMounts_id())){
				throw new ExceptionUtil("挂载编号和历史版本号为空");
			}
			if(StringUtil.isEmpty(lcNodeAttribute.getLc_process_id())){
				throw new ExceptionUtil("流程id为空！");
			}
			if(StringUtil.isEmpty(lcNodeAttribute.getNodeId())){
				throw new ExceptionUtil("节点编号为空！");
			}
			//1.处理主信息
			if(StringUtil.isEmpty(lcNodeAttribute.getLc_node_attribute_id())){
				lcNodeAttribute.setLc_node_attribute_id(toUUID());
				lcNodeAttribute.setCreate_id(getXtUid());
				lcNodeAttribute.setCreate_time(getDate());
				i = lcNodeAttributeDao.addLcNodeAttribute(lcNodeAttribute);
			}else{
				i = lcNodeAttributeDao.updateLcNodeAttributeBySelective(lcNodeAttribute);
			}


			//2.处理按钮
			if(!CollectionUtil.isEmpty(lcNodeAttribute.getLcNodeBtns())){
				lcNodeBtnDao.delLcNodeBtnByAttributeId(lcNodeAttribute.getLc_node_attribute_id());
				List<LcNodeBtn> lcNodeBtns = lcNodeAttribute.getLcNodeBtns();
				Map<String,Object> condition = new HashMap<>();
				condition.put("unNeedDelBtn",true);
				condition.put("lc_node_attribute_id",lcNodeAttribute.getLc_node_attribute_id());
				lcJumpRulesDao.delLcJumpRulesByAttributeId(condition);//只删除包括按钮关联的节点
				for(LcNodeBtn lcNodeBtn : lcNodeBtns){
					lcNodeBtn.setCreate_id(getXtUid());
					lcNodeBtn.setCreate_time(getDate());
					lcNodeBtn.setLc_deployment_id(lcNodeAttribute.getHid_());
					lcNodeBtn.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
					lcNodeBtn.setNode_btn_id(toUUID());
					lcNodeBtnDao.addLcNodeBtn(lcNodeBtn);
					List<LcJumpRules> lcJumpRules = lcNodeBtn.getLcJumpRules();
					if(!CollectionUtil.isEmpty(lcJumpRules)){
						for(LcJumpRules lcJumpRule : lcJumpRules){
							lcJumpRule.setCreate_id(getXtUid());
							lcJumpRule.setCreate_time(getDate());
							lcJumpRule.setJump_rules_id(toUUID());
							lcJumpRule.setNode_btn_id(lcNodeBtn.getNode_btn_id());//设置按钮id
							lcJumpRule.setNode_id(lcNodeAttribute.getNodeId());
							lcJumpRule.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
							lcJumpRulesDao.addLcJumpRules(lcJumpRule);
						}
					}
				}
			}

			//3.处理跳转规则
			if(!CollectionUtil.isEmpty(lcNodeAttribute.getLcJumpRules())){
				Map<String,Object> condition = new HashMap<>();
				condition.put("lc_node_attribute_id",lcNodeAttribute.getLc_node_attribute_id());
				condition.put("unNeedDelBtn",false);
				lcJumpRulesDao.delLcJumpRulesByAttributeId(condition);//只删除节点（不包括按钮关联的节点）
				List<LcJumpRules> lcJumpRules = lcNodeAttribute.getLcJumpRules();
				for(LcJumpRules lcJumpRule : lcJumpRules){
					lcJumpRule.setCreate_id(getXtUid());
					lcJumpRule.setCreate_time(getDate());
					lcJumpRule.setJump_rules_id(toUUID());
					lcJumpRule.setNode_id(lcNodeAttribute.getNodeId());
					lcJumpRule.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
					lcJumpRulesDao.addLcJumpRules(lcJumpRule);
				}
			}


			//4.处理审批人
			if(!CollectionUtil.isEmpty(lcNodeAttribute.getLcNodeCandidate())){
				lcNodeCandidateDao.delLcNodeCandidateByAttributeId(lcNodeAttribute.getLc_node_attribute_id());
				List<LcNodeCandidate> lcNodeCandidates = lcNodeAttribute.getLcNodeCandidate();
				for(LcNodeCandidate lcNodeCandidate : lcNodeCandidates){
					lcNodeCandidate.setLc_deployment_id(lcNodeAttribute.getHid_());
					lcNodeCandidate.setNode_id(lcNodeAttribute.getNodeId());
					lcNodeCandidate.setLc_process_id(lcNodeAttribute.getLc_process_id());
					lcNodeCandidate.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
					lcNodeCandidate.setCreate_id(getXtUid());
					lcNodeCandidate.setCreate_time(getDate());
					lcNodeCandidate.setLc_node_candidate_id(toUUID());
					lcNodeCandidateDao.addLcNodeCandidate(lcNodeCandidate);
				}
			}

			//5.字段权限
			if(!CollectionUtil.isEmpty(lcNodeAttribute.getNodeFormField())){
				lcNodeFormFieldDao.delLcNodeFormFieldAttributeId(lcNodeAttribute.getLc_node_attribute_id());
				List<LcNodeFormField> lcNodeFormFields = lcNodeAttribute.getNodeFormField();
				for(LcNodeFormField lcNodeFormField : lcNodeFormFields){
					lcNodeFormField.setCreate_id(getXtUid());
					lcNodeFormField.setCreate_time(getDate());
					lcNodeFormField.setNode_field_id(toUUID());
					lcNodeFormField.setNode_id(lcNodeAttribute.getNodeId());
					lcNodeFormField.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
					lcNodeFormFieldDao.addLcNodeFormField(lcNodeFormField);
				}
			}

		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 强制同步
	 * @param lcNodeAttribute
	 * @return
	 */
	public int syncLcNodeAttribute(LcNodeAttribute lcNodeAttribute){
		int i = 0;
		try {
			if(StringUtil.isEmpty(lcNodeAttribute.getLc_node_attribute_id())){
				throw new ExceptionUtil("节点属性id为空！");
			}
			if(StringUtil.isEmpty(lcNodeAttribute.getHid_()) && StringUtil.isEmpty(lcNodeAttribute.getMounts_id())){
				throw new ExceptionUtil("挂载编号和历史版本号为空");
			}
			if(StringUtil.isEmpty(lcNodeAttribute.getLc_process_id())){
				throw new ExceptionUtil("流程id为空！");
			}
			if(StringUtil.isEmpty(lcNodeAttribute.getNodeId())){
				throw new ExceptionUtil("节点编号为空！");
			}

			//1.处理主信息
			lcNodeAttributeDao.delLcNodeAttributeById(lcNodeAttribute.getLc_node_attribute_id());//强删
			i = lcNodeAttributeDao.addLcNodeAttribute(lcNodeAttribute);//重新添加

			//2.处理按钮
			if(!CollectionUtil.isEmpty(lcNodeAttribute.getLcNodeBtns())){
				lcNodeBtnDao.delLcNodeBtnByAttributeId(lcNodeAttribute.getLc_node_attribute_id());
				List<LcNodeBtn> lcNodeBtns = lcNodeAttribute.getLcNodeBtns();
				Map<String,Object> condition = new HashMap<>();
				condition.put("unNeedDelBtn",true);
				condition.put("lc_node_attribute_id",lcNodeAttribute.getLc_node_attribute_id());
				lcJumpRulesDao.delLcJumpRulesByAttributeId(condition);//只删除包括按钮关联的节点
				for(LcNodeBtn lcNodeBtn : lcNodeBtns){
					lcNodeBtn.setUpdate_id(getXtUid());
					lcNodeBtn.setUpdate_time(getDate());
					lcNodeBtn.setLc_deployment_id(lcNodeAttribute.getHid_());
					lcNodeBtn.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
					lcNodeBtn.setNode_btn_id(toUUID());
					lcNodeBtnDao.addLcNodeBtn(lcNodeBtn);
					List<LcJumpRules> lcJumpRules = lcNodeBtn.getLcJumpRules();
					if(!CollectionUtil.isEmpty(lcJumpRules)){
						for(LcJumpRules lcJumpRule : lcJumpRules){
							lcJumpRule.setUpdate_id(getXtUid());
							lcJumpRule.setUpdate_time(getDate());
							lcJumpRule.setJump_rules_id(toUUID());
							lcJumpRule.setNode_btn_id(lcNodeBtn.getNode_btn_id());//设置按钮id
							lcJumpRule.setNode_id(lcNodeAttribute.getNodeId());
							lcJumpRule.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
							lcJumpRulesDao.addLcJumpRules(lcJumpRule);
						}
					}
				}
			}

			//3.处理跳转规则
			if(!CollectionUtil.isEmpty(lcNodeAttribute.getLcJumpRules())){
				Map<String,Object> condition = new HashMap<>();
				condition.put("lc_node_attribute_id",lcNodeAttribute.getLc_node_attribute_id());
				condition.put("unNeedDelBtn",false);
				lcJumpRulesDao.delLcJumpRulesByAttributeId(condition);//只删除节点（不包括按钮关联的节点）
				List<LcJumpRules> lcJumpRules = lcNodeAttribute.getLcJumpRules();
				for(LcJumpRules lcJumpRule : lcJumpRules){
					lcJumpRule.setUpdate_id(getXtUid());
					lcJumpRule.setUpdate_time(getDate());
					lcJumpRule.setJump_rules_id(toUUID());
					lcJumpRule.setNode_id(lcNodeAttribute.getNodeId());
					lcJumpRule.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
					lcJumpRulesDao.addLcJumpRules(lcJumpRule);
				}
			}

			//4.处理审批人
			if(!CollectionUtil.isEmpty(lcNodeAttribute.getLcNodeCandidate())){
				lcNodeCandidateDao.delLcNodeCandidateByAttributeId(lcNodeAttribute.getLc_node_attribute_id());
				List<LcNodeCandidate> lcNodeCandidates = lcNodeAttribute.getLcNodeCandidate();
				for(LcNodeCandidate lcNodeCandidate : lcNodeCandidates){
					lcNodeCandidate.setLc_deployment_id(lcNodeAttribute.getHid_());
					lcNodeCandidate.setNode_id(lcNodeAttribute.getNodeId());
					lcNodeCandidate.setLc_process_id(lcNodeAttribute.getLc_process_id());
					lcNodeCandidate.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
					lcNodeCandidate.setUpdate_id(getXtUid());
					lcNodeCandidate.setUpdate_time(getDate());
					lcNodeCandidate.setLc_node_candidate_id(toUUID());
					lcNodeCandidateDao.addLcNodeCandidate(lcNodeCandidate);
				}
			}

			//5.字段权限
			if(!CollectionUtil.isEmpty(lcNodeAttribute.getNodeFormField())){
				lcNodeFormFieldDao.delLcNodeFormFieldAttributeId(lcNodeAttribute.getLc_node_attribute_id());
				List<LcNodeFormField> lcNodeFormFields = lcNodeAttribute.getNodeFormField();
				for(LcNodeFormField lcNodeFormField : lcNodeFormFields){
					lcNodeFormField.setUpdate_id(getXtUid());
					lcNodeFormField.setUpdate_time(getDate());
					lcNodeFormField.setNode_field_id(toUUID());
					lcNodeFormField.setNode_id(lcNodeAttribute.getNodeId());
					lcNodeFormField.setLc_node_attribute_id(lcNodeAttribute.getLc_node_attribute_id());
					lcNodeFormFieldDao.addLcNodeFormField(lcNodeFormField);
				}
			}
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 根据节点id+部署记录查找唯一
	 * @param nodeAttributeParam
	 * @return
	 */
	public LcNodeAttribute getLcNodeAttribute(NodeAttributeParam nodeAttributeParam){
		LcNodeAttribute lcNodeAttribute = lcNodeAttributeDao.getLcNodeAttribute(nodeAttributeParam);
		if(null != lcNodeAttribute){
			if(!StringUtil.isEmpty(lcNodeAttribute.getMounts_id())){//查询bpm文件
				LcProcess lcProcess = lcProcessDao.getLcProcessById(lcNodeAttribute.getMounts_id());
				if(null != lcProcess){
					lcNodeAttribute.setBpm(lcProcess.getLc_process_bpmn());
				}
			}
			if(!StringUtil.isEmpty(lcNodeAttribute.getHid_())){//查询bpm文件
				LcDeploymentHis lcDeploymentHis = lcDeploymentHisDao.getLcDeploymentHisById(lcNodeAttribute.getHid_());
				if(null != lcDeploymentHis){
					lcNodeAttribute.setBpm(lcDeploymentHis.getLc_process_bpmn());
				}
			}
			String lc_node_attribute_id = lcNodeAttribute.getLc_node_attribute_id();
			Map<String,Object> condition = new HashMap<>();
			condition.put("lc_node_attribute_id",lc_node_attribute_id);
			List<LcJumpRules> lcJumpRules = lcJumpRulesDao.getLcJumpRulesListByCondition(condition);
			List<LcJumpRules> btnRules = new ArrayList<>();
			if(!CollectionUtil.isEmpty(lcJumpRules)){
				List<LcJumpRules> jumpRulesList = new ArrayList<>();
				//过滤掉按钮下的自由规则
				for(LcJumpRules rules : lcJumpRules){
					if(StringUtil.isEmpty(rules.getNode_btn_id())){
						jumpRulesList.add(rules);
					}else{
						btnRules.add(rules);
					}
				}
				if(!CollectionUtil.isEmpty(jumpRulesList)){
					lcNodeAttribute.setLcJumpRules(jumpRulesList);
				}
			}
			List<LcNodeBtn> lcNodeBtns = lcNodeBtnDao.getLcNodeBtnListByCondition(condition);
			if(!CollectionUtil.isEmpty(lcNodeBtns)){
				for(LcNodeBtn lcNodeBtn : lcNodeBtns){
					if(StringUtil.isEmpty(lcNodeBtn.getBtn_label())){
						lcNodeBtn.setBtn_label(lcNodeBtn.get_label());//如果自定义按钮扩展label不存在则调用按钮中label
					}
					if(StringUtil.isEmpty(lcNodeBtn.getBtn_action())){
						lcNodeBtn.setBtn_action(lcNodeBtn.getB_action());//如果自定义扩展action为空 则调用按钮中action
					}
					if(!StringUtil.isEmpty(lcNodeBtn.getBtn_id())){
						List<LcJumpRules> lcBtnRules = new ArrayList<>();
						if(!CollectionUtil.isEmpty(btnRules)){
							for(LcJumpRules rules: btnRules){
								if(rules.getNode_btn_id().equals(lcNodeBtn.getNode_btn_id())){
									lcBtnRules.add(rules);
								}
							}
						}
						if(!CollectionUtil.isEmpty(lcBtnRules)){
							lcNodeBtn.setLcJumpRules(lcBtnRules);//设置按钮对应的节点跳转规则
						}
						lcNodeBtn.setLcBtn(lcBtnDao.getLcBtnById(lcNodeBtn.getBtn_id()));
					}
				}
				lcNodeAttribute.setLcNodeBtns(lcNodeBtns);
			}
			List<LcNodeCandidate> lcNodeCandidates = lcNodeCandidateDao.getLcNodeCandidateListByCondition(condition);
			if(!CollectionUtil.isEmpty(lcNodeCandidates)){
				if(lcNodeCandidates.size() > 1){
					throw new ExceptionUtil("查出多个审批记录，错误！");
				}
				lcNodeAttribute.setLcNodeCandidate(lcNodeCandidates);
			}

			List<LcNodeFormField> lcNodeFormFields = lcNodeFormFieldDao.getLcNodeFormFieldListByCondition(condition);
			if(!CollectionUtil.isEmpty(lcNodeFormFields)){
				lcNodeAttribute.setNodeFormField(lcNodeFormFields);
			}

			//处理通知模板
			if(!StringUtil.isEmpty(lcNodeAttribute.getNotify_template_ids())){
				condition = new HashMap<>();
				condition.put("notifyTemplateIds",lcNodeAttribute.getNotify_template_ids().split(","));
			}
			List<LcNotifyTemplate> lcNotifyTemplates =  lcNotifyTemplateDao.getLcNotifyTemplateListByCondition(condition);
			if(!CollectionUtil.isEmpty(lcNotifyTemplates)){
				lcNodeAttribute.setLcNotifyTemplates(lcNotifyTemplates);
			}
		}
		return lcNodeAttribute;
	}

	/**
	 * 根据编号删除
	 * @param lc_node_attribute_id
	 * @return
	 */
	public int delLcNodeAttributeById(String lc_node_attribute_id){
		int i = 0;
		if(!StringUtil.isEmpty(lc_node_attribute_id)){
			Map<String,Object> condition = new HashMap<>();
			condition.put("lc_node_attribute_id",lc_node_attribute_id);
			lcJumpRulesDao.delLcJumpRulesByAttributeId(condition);//1.删除跳转规则
			lcNodeBtnDao.delLcNodeBtnByAttributeId(lc_node_attribute_id);//2.删除节点按钮
			lcNodeCandidateDao.delLcNodeCandidateByAttributeId(lc_node_attribute_id);//3.删除办理人
			lcNodeFormFieldDao.delLcNodeFormFieldAttributeId(lc_node_attribute_id);//4.删除节点字段
			i = lcNodeAttributeDao.delLcNodeAttributeById(lc_node_attribute_id);//5.删除主节点
		}
		return i;
	}
}
