package jehc.djshi.workflow.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.dao.LcDeploymentHisDao;
import jehc.djshi.workflow.model.LcDeploymentHis;
import jehc.djshi.workflow.service.LcDeploymentHisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程部署历史记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class LcDeploymentHisServiceImpl extends BaseService implements LcDeploymentHisService {
	@Autowired
	private LcDeploymentHisDao lcDeploymentHisDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcDeploymentHis> getLcDeploymentHisListByCondition(Map<String,Object> condition){
		try{
			return lcDeploymentHisDao.getLcDeploymentHisListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LcDeploymentHis getLcDeploymentHisById(String id){
		try{
			return lcDeploymentHisDao.getLcDeploymentHisById(id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcDeploymentHis
	* @return
	*/
	public int addLcDeploymentHis(LcDeploymentHis lcDeploymentHis){
		int i = 0;
		try {
			lcDeploymentHis.setCreate_id(getXtUid());
			lcDeploymentHis.setCreate_time(getDate());
			i = lcDeploymentHisDao.addLcDeploymentHis(lcDeploymentHis);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcDeploymentHis
	* @return
	*/
	public int updateLcDeploymentHis(LcDeploymentHis lcDeploymentHis){
		int i = 0;
		try {
			lcDeploymentHis.setUpdate_id(getXtUid());
			lcDeploymentHis.setUpdate_time(getDate());
			i = lcDeploymentHisDao.updateLcDeploymentHis(lcDeploymentHis);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcDeploymentHis(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcDeploymentHisDao.delLcDeploymentHis(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param lcDeploymentHisList
	* @return
	*/
	public int addBatchLcDeploymentHis(List<LcDeploymentHis> lcDeploymentHisList){
		int i = 0;
		try {
			i = lcDeploymentHisDao.addBatchLcDeploymentHis(lcDeploymentHisList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param lcDeploymentHisList
	* @return
	*/
	public int updateBatchLcDeploymentHis(List<LcDeploymentHis> lcDeploymentHisList){
		int i = 0;
		try {
			i = lcDeploymentHisDao.updateBatchLcDeploymentHis(lcDeploymentHisList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	
	/**
	 * 查询唯一一个对象
	 * @param condition
	 * @return
	 */
	public LcDeploymentHis getLcDeploymentHisUnique(Map<String,Object> condition){
		try{
			return lcDeploymentHisDao.getLcDeploymentHisUnique(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 查询最新唯一一个对象
	 * @param condition
	 * @return
	 */
	public LcDeploymentHis getLcDeploymentHisNewUnique(Map<String,Object> condition){
		try{
			List<LcDeploymentHis> lcDeploymentHisList = lcDeploymentHisDao.getLcDeploymentHisNewUnique(condition);//过滤条件为关闭状态
			if(!CollectionUtil.isEmpty(lcDeploymentHisList)){
				return lcDeploymentHisList.get(0);
			}
			return null;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 根据部署编号查找对象
	 * @param lc_deployment_his_id
	 * @return
	 */
	public LcDeploymentHis getLcDeploymentHisByHisId(String lc_deployment_his_id){
		try{
			return lcDeploymentHisDao.getLcDeploymentHisByHisId(lc_deployment_his_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 根据模块key查找列表按最新排序
	 * @param moduleKey
	 * @return
	 */
	public LcDeploymentHis getLcDeploymentHisByModuleKey(String moduleKey){
		try{
			if(StringUtil.isEmpty(moduleKey)){
				throw new ExceptionUtil("未能获取到moduleKey");
			}
			List<LcDeploymentHis> lcDeploymentHisList = lcDeploymentHisDao.getLcDeploymentHisByModuleKey(moduleKey);
			if(!CollectionUtil.isEmpty(lcDeploymentHisList)){
				return lcDeploymentHisList.get(0);
			}
			return new LcDeploymentHis();
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	 * 查询最新唯一一个对象（包含bpmn）
	 */
	public LcDeploymentHis getLcDeploymentHisMoreAttrNewUnique(String moduleKey){
		try{
			List<LcDeploymentHis> lcDeploymentHisList = lcDeploymentHisDao.getLcDeploymentHisMoreAttrNewUnique(moduleKey);
			if(!CollectionUtil.isEmpty(lcDeploymentHisList)){
				return lcDeploymentHisList.get(0);
			}
			return null;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
