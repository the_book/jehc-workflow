package jehc.djshi.workflow.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcNodeFormField;

/**
* @Desc 节点字段 
* @Author 邓纯杰
* @CreateTime 2022-04-06 11:41:43
*/
public interface LcNodeFormFieldDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcNodeFormField> getLcNodeFormFieldListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param node_field_id 
	* @return
	*/
	LcNodeFormField getLcNodeFormFieldById(String node_field_id);
	/**
	* 添加
	* @param lcNodeFormField 
	* @return
	*/
	int addLcNodeFormField(LcNodeFormField lcNodeFormField);
	/**
	* 修改
	* @param lcNodeFormField 
	* @return
	*/
	int updateLcNodeFormField(LcNodeFormField lcNodeFormField);
	/**
	* 修改（根据动态条件）
	* @param lcNodeFormField 
	* @return
	*/
	int updateLcNodeFormFieldBySelective(LcNodeFormField lcNodeFormField);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcNodeFormField(Map<String, Object> condition);

	/**
	 * 根据属性id删除
	 * @param lc_node_attribute_id
	 * @return
	 */
	int delLcNodeFormFieldAttributeId(String lc_node_attribute_id);
}
