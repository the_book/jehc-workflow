package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcFormService;
import jehc.djshi.workflow.dao.LcFormDao;
import jehc.djshi.workflow.model.LcForm;

/**
* @Desc 工作流表单 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:33:49
*/
@Service("lcFormService")
public class LcFormServiceImpl extends BaseService implements LcFormService{
	@Autowired
	private LcFormDao lcFormDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcForm> getLcFormListByCondition(Map<String,Object> condition){
		try{
			return lcFormDao.getLcFormListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param lc_form_id 
	* @return
	*/
	public LcForm getLcFormById(String lc_form_id){
		try{
			LcForm lcForm = lcFormDao.getLcFormById(lc_form_id);
			return lcForm;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcForm 
	* @return
	*/
	public int addLcForm(LcForm lcForm){
		int i = 0;
		try {
			i = lcFormDao.addLcForm(lcForm);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcForm 
	* @return
	*/
	public int updateLcForm(LcForm lcForm){
		int i = 0;
		try {
			i = lcFormDao.updateLcForm(lcForm);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcForm 
	* @return
	*/
	public int updateLcFormBySelective(LcForm lcForm){
		int i = 0;
		try {
			i = lcFormDao.updateLcFormBySelective(lcForm);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcForm(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcFormDao.delLcForm(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
