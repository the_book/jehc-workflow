package jehc.djshi.workflow.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.dao.LcApprovalDao;
import jehc.djshi.workflow.model.LcApproval;
import jehc.djshi.workflow.param.LcApprovalParam;
import jehc.djshi.workflow.service.LcApprovalService;
import jehc.djshi.workflow.util.ActivitiUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * @Desc 工作流批审
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Service
public class LcApprovalServiceImpl extends BaseService implements LcApprovalService {
	@Autowired
	private LcApprovalDao lcApprovalDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcApproval> getLcApprovalListByCondition(Map<String,Object> condition){
		try{
			return lcApprovalDao.getLcApprovalListByCondition(condition);
		} catch (Exception e) {
			log.error("查询审批列表异常：{}",e);
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param lcApprovalId
	* @return
	*/
	public LcApproval getLcApprovalById(String lcApprovalId){
		try{
			return lcApprovalDao.getLcApprovalById(lcApprovalId);
		} catch (Exception e) {
			log.error("查询审批记录异常：{}",e);
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcApproval
	* @return
	*/
	public int addLcApproval(LcApproval lcApproval){
		int i = 0;
		try {
			if(StringUtil.isEmpty(lcApproval.getLcApprovalId())){
				lcApproval.setCreate_time(getDate());
				lcApproval.setCreate_id(getXtUid());
				lcApproval.setLcApprovalId(toUUID());
			}
			i = lcApprovalDao.addLcApproval(lcApproval);
		} catch (Exception e) {
			log.error("创建审批记录异常：{}",e);
			throw new ExceptionUtil("创建审批记录异常：{}",e.getCause());
		}
		return i;
	}

	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcApproval(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcApprovalDao.delLcApproval(condition);
		} catch (Exception e) {
			log.error("删除审批记录异常：{}",e);
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 根据业务Key批量删除
	 * @param condition
	 * @return
	 */
	public int delLcApprovalByBusinessKey(Map<String, Object> condition){
		int i = 0;
		try {
			i = lcApprovalDao.delLcApprovalByBusinessKey(condition);
		} catch (Exception e) {
			log.error("删除审批记录异常：{}",e);
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	 * 查询审批记录（供外部调用）
	 * @param lcApprovalParam
	 * @return
	 */
	public List<LcApproval> getLcApprovalList(LcApprovalParam lcApprovalParam){
		try{
			List<LcApproval> lcApprovalList = lcApprovalDao.getLcApprovalList(lcApprovalParam);
			if(!CollectionUtil.isEmpty(lcApprovalList)){
				for(LcApproval lcApproval: lcApprovalList){
					if(StringUtil.isEmpty(lcApproval.getBehaviorText())){
						String behavior = lcApproval.getBehavior();
						switch (behavior){
							case ActivitiUtil.BEHAVIOR_0://提交
								lcApproval.setBehaviorText("提交");
								break;
							case ActivitiUtil.BEHAVIOR_10://同意
								lcApproval.setBehaviorText("同意");
								break;
							case ActivitiUtil.BEHAVIOR_20://驳回
								lcApproval.setBehaviorText("驳回");
								break;
							case ActivitiUtil.BEHAVIOR_30://弃权
								lcApproval.setBehaviorText("弃权");
								break;
							case ActivitiUtil.BEHAVIOR_40://撤回
								lcApproval.setBehaviorText("撤回");
								break;
							case ActivitiUtil.BEHAVIOR_50://强行终止（终止流程）
								lcApproval.setBehaviorText("终止流程");
								break;
							case ActivitiUtil.BEHAVIOR_60://执行跳转
								lcApproval.setBehaviorText("跳转");
								break;
							case ActivitiUtil.BEHAVIOR_70://转办
								lcApproval.setBehaviorText("转办");
								break;
							case ActivitiUtil.BEHAVIOR_80://委派
								lcApproval.setBehaviorText("委派");
								break;
							case ActivitiUtil.BEHAVIOR_90://加签
								lcApproval.setBehaviorText("加签");
								break;
							case ActivitiUtil.BEHAVIOR_100://催办
								lcApproval.setBehaviorText("催办");
								break;
							case ActivitiUtil.BEHAVIOR_110://设置任务归属人
								lcApproval.setBehaviorText("设置任务归属人");
								break;
							case ActivitiUtil.BEHAVIOR_120://挂起
								lcApproval.setBehaviorText("挂起");
								break;
							case ActivitiUtil.BEHAVIOR_130://激活
								lcApproval.setBehaviorText("激活");
								break;
							default:
								lcApproval.setBehaviorText("缺省");
						}
					}
				}
			}
			return lcApprovalList;
		} catch (Exception e) {
			log.error("查询审批列表异常：{}",e);
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
