package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.workflow.util.message.vo.Constant;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcNotifyTemplate;
import jehc.djshi.workflow.service.LcNotifyTemplateService;

/**
* @Desc 流程通知模板 
* @Author 邓纯杰
* @CreateTime 2022-11-30 11:29:02
*/
@RestController
@RequestMapping("/lcNotifyTemplate")
public class LcNotifyTemplateController extends BaseAction{
	@Autowired
	private LcNotifyTemplateService lcNotifyTemplateService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcNotifyTemplate>> getLcNotifyTemplateListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcNotifyTemplate> lcNotifyTemplateList = lcNotifyTemplateService.getLcNotifyTemplateListByCondition(condition);
		for(LcNotifyTemplate lcNotifyTemplate:lcNotifyTemplateList){
			if(!StringUtil.isEmpty(lcNotifyTemplate.getCreate_id())){
				OauthAccountEntity createBy = getAccount(lcNotifyTemplate.getCreate_id());
				if(null != createBy){
					lcNotifyTemplate.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(lcNotifyTemplate.getUpdate_id())){
				OauthAccountEntity modifieBy = getAccount(lcNotifyTemplate.getUpdate_id());
				if(null != modifieBy){
					lcNotifyTemplate.setModifiedBy(modifieBy.getName());
				}
			}
		}
		PageInfo<LcNotifyTemplate> page = new PageInfo<LcNotifyTemplate>(lcNotifyTemplateList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult<LcNotifyTemplate> getLcNotifyTemplateById(@PathVariable("id")String id){
		LcNotifyTemplate lcNotifyTemplate = lcNotifyTemplateService.getLcNotifyTemplateById(id);
		if(!StringUtil.isEmpty(lcNotifyTemplate.getCreate_id())){
			OauthAccountEntity createBy = getAccount(lcNotifyTemplate.getCreate_id());
			if(null != createBy){
				lcNotifyTemplate.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(lcNotifyTemplate.getUpdate_id())){
			OauthAccountEntity modifieBy = getAccount(lcNotifyTemplate.getUpdate_id());
			if(null != modifieBy){
				lcNotifyTemplate.setModifiedBy(modifieBy.getName());
			}
		}
		return outDataStr(lcNotifyTemplate);
	}
	/**
	* 添加
	* @param lcNotifyTemplate 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcNotifyTemplate(@RequestBody LcNotifyTemplate lcNotifyTemplate){
		int i = 0;
		if(null != lcNotifyTemplate){
			lcNotifyTemplate.setId(toUUID());
			lcNotifyTemplate.setCreate_time(getDate());
			lcNotifyTemplate.setCreate_id(getXtUid());
			i=lcNotifyTemplateService.addLcNotifyTemplate(lcNotifyTemplate);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcNotifyTemplate 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcNotifyTemplate(@RequestBody LcNotifyTemplate lcNotifyTemplate){
		int i = 0;
		if(null != lcNotifyTemplate){
			lcNotifyTemplate.setUpdate_time(getDate());
			lcNotifyTemplate.setUpdate_id(getXtUid());
			i=lcNotifyTemplateService.updateLcNotifyTemplate(lcNotifyTemplate);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcNotifyTemplate(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=lcNotifyTemplateService.delLcNotifyTemplate(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询列表
	 * @return
	 */
	@ApiOperation(value="查询列表", notes="查询列表")
	@NeedLoginUnAuth
	@GetMapping(value="/find")
	public BaseResult<List<LcNotifyTemplate>> getLcNotifyTemplateList(){
		Map<String, Object> condition = new HashMap<>();
		List<LcNotifyTemplate> lcNotifyTemplates = lcNotifyTemplateService.getLcNotifyTemplateListByCondition(condition);
		if(CollectionUtil.isNotEmpty(lcNotifyTemplates)){
			for(LcNotifyTemplate lcNotifyTemplate: lcNotifyTemplates){
				if(Constant.MAIL.equals(lcNotifyTemplate.getType())){
					lcNotifyTemplate.setTitle("【邮箱】"+lcNotifyTemplate.getTitle());
				}
				if(Constant.SMS.equals(lcNotifyTemplate.getType())){
					lcNotifyTemplate.setTitle("【短信】"+lcNotifyTemplate.getTitle());
				}
				if(Constant.NOTICE.equals(lcNotifyTemplate.getType())){
					lcNotifyTemplate.setTitle("【站内】"+lcNotifyTemplate.getTitle());
				}
				if(Constant.ENTERPRISEWECHAT.equals(lcNotifyTemplate.getType())){
					lcNotifyTemplate.setTitle("【企业微信】"+lcNotifyTemplate.getTitle());
				}
				if(Constant.NAILNAIL.equals(lcNotifyTemplate.getType())){
					lcNotifyTemplate.setTitle("【钉钉】"+lcNotifyTemplate.getTitle());
				}
				if(Constant.OFFICIALWECHAT.equals(lcNotifyTemplate.getType())){
					lcNotifyTemplate.setTitle("【微信公众号】"+lcNotifyTemplate.getTitle());
				}
			}
		}
		return new BaseResult(lcNotifyTemplates);
	}
}
