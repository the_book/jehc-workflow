package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcProduct;
import jehc.djshi.workflow.service.LcProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 产品线
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "产品线",description = "产品线", tags = "产品线")
@RestController
@RequestMapping("/lcProduct")
public class LcProductController extends BaseAction {
    @Autowired
    private LcProductService lcProductService;

    /**
     * 查询并分页
     * @param baseSearch
     */
    @ApiOperation(value="查询并分页", notes="查询并分页")
    @PostMapping(value="/list")
    @NeedLoginUnAuth
    public BasePage<List<LcProduct>> getLcProcessBListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<LcProduct> lcProductList = lcProductService.getLcProductListByCondition(condition);
        for(LcProduct lcProduct:lcProductList){
            if(!StringUtil.isEmpty(lcProduct.getCreate_id())){
                OauthAccountEntity createBy = getAccount(lcProduct.getCreate_id());
                if(null != createBy){
                    lcProduct.setCreateBy(createBy.getName());
                }
            }
            if(!StringUtil.isEmpty(lcProduct.getUpdate_id())){
                OauthAccountEntity modifieBy = getAccount(lcProduct.getUpdate_id());
                if(null != modifieBy){
                    lcProduct.setModifiedBy(modifieBy.getName());
                }
            }
        }
        PageInfo<LcProduct> page = new PageInfo<LcProduct>(lcProductList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个对象
     * @param lc_group_id
     */
    @ApiOperation(value="查询单个对象", notes="查询单个对象")
    @GetMapping(value="/get/{lc_group_id}")
    @NeedLoginUnAuth
    public BaseResult<LcProduct> getLcProductById(@PathVariable("lc_group_id")String lc_group_id){
        LcProduct lcProduct = lcProductService.getLcProductById(lc_group_id);
        return outDataStr(lcProduct);
    }

    /**
     * 添加
     * @param lcProduct
     */
    @ApiOperation(value="添加", notes="添加")
    @PostMapping(value="/add")
    public BaseResult addLcProduct(@RequestBody LcProduct lcProduct){
        int i = 0;
        if(null != lcProduct){
            lcProduct.setCreate_time(getDate());
            lcProduct.setCreate_id(getXtUid());
            lcProduct.setLc_product_id(toUUID());
            i=lcProductService.addLcProduct(lcProduct);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 修改
     * @param lcProduct
     */
    @ApiOperation(value="修改", notes="修改")
    @PutMapping(value="/update")
    public BaseResult updateLcProduct(@RequestBody LcProduct lcProduct){
        int i = 0;
        if(null != lcProduct){
            lcProduct.setUpdate_id(getXtUid());
            lcProduct.setUpdate_time(getDate());
            i=lcProductService.updateLcProduct(lcProduct);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 删除
     * @param lc_product_id
     */
    @ApiOperation(value="删除", notes="删除")
    @DeleteMapping(value="/delete")
    public BaseResult delLcProduct(String lc_product_id){
        int i = 0;
        if(!StringUtil.isEmpty(lc_product_id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("lc_product_id",lc_product_id.split(","));
            i=lcProductService.delLcProduct(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 查询集合
     */
    @ApiOperation(value="查询集合", notes="查询集合")
    @PostMapping(value="/find")
    @NeedLoginUnAuth
    public BaseResult<List<LcProduct>> find(){
        Map<String,Object> condition = new HashMap<>();
        List<LcProduct> lcProductList = lcProductService.getLcProductListByCondition(condition);
        return new BaseResult(lcProductList);
    }
}
