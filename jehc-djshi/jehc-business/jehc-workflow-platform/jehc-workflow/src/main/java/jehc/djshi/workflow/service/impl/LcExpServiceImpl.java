package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.workflow.dao.LcExpDao;
import jehc.djshi.workflow.model.LcExp;
import jehc.djshi.workflow.service.LcExpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程表达式
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class LcExpServiceImpl extends BaseService implements LcExpService {

    @Autowired
    LcExpDao lcExpDao;


    /**
     *
     * @param condition
     * @return
     */
    public List<LcExp> getLcExpListByCondition(Map<String, Object> condition){
        return lcExpDao.getLcExpListByCondition(condition);
    }

    /**
     *
     * @param lc_exp_id
     * @return
     */
    public LcExp getLcExpById(String lc_exp_id){
        return lcExpDao.getLcExpById(lc_exp_id);
    }

    /**
     * 添加
     * @param lcExp
     * @return
     */
    public int addLcExp(LcExp lcExp){
        return lcExpDao.addLcExp(lcExp);
    }

    /**
     * 修改
     * @param lcExp
     * @return
     */
    public int updateLcExp(LcExp lcExp){
        return lcExpDao.updateLcExp(lcExp);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delLcExp(Map<String, Object> condition){
        return lcExpDao.delLcExp(condition);
    }
}
