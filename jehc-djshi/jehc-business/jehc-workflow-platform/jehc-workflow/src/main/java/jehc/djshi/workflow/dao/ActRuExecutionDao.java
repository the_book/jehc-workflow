package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.ActRuExecution;

import java.util.List;
import java.util.Map;

/**
 * @Desc 运行实例
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface ActRuExecutionDao {

    /**
     * 查询运行实例集合
     * @param condition
     * @return
     */
    List<ActRuExecution> getActRuExecutionListByCondition(Map<String,Object> condition);

    /**
     * 查询单个运行实例
     * @param id
     * @return
     */
    ActRuExecution getActRuExecutionById(String id);

    /**
     * 删除运行实例
     * @param id
     * @return
     */
    int delActRuExecution(String id);

    /**
     * 创建运行实例(actRuExecution)
     * @param actRuExecution
     * @return
     */
    int addActRunExecution(ActRuExecution actRuExecution);
}
