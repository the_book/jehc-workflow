package jehc.djshi.workflow.service;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.workflow.design.MxGraphModel;
import jehc.djshi.workflow.model.LcProcess;
import jehc.djshi.workflow.param.LcHisParam;
import jehc.djshi.workflow.param.LcProcessParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @Desc 流程信息存储记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcProcessService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcProcess> getLcProcessListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param lc_process_id
	* @return
	*/
	LcProcess getLcProcessById(String lc_process_id);
	/**
	* 添加
	* @param lcProcess
	* @return
	*/
	int addLcProcess(LcProcess lcProcess);
	/**
	* 修改
	* @param lcProcess
	* @return
	*/
	int updateLcProcess(LcProcess lcProcess);
	/**
	* 删除
	* @param condition
	* @return
	*/
	int delLcProcess(Map<String, Object> condition);
	/**
	* 批量添加
	* @param lcProcessList
	* @return
	*/
	int addBatchLcProcess(List<LcProcess> lcProcessList);
	/**
	* 批量修改
	* @param lcProcessList
	* @return
	*/
	int updateBatchLcProcess(List<LcProcess> lcProcessList);
	/**
	 * 发布或关闭流程
	 * @param condition
	 * @return
	 */
	int updateLcProcessStatus(String lc_process_id, Map<String, Object> condition);

	/**
	 * 根据流程模块Key查找流程定义
	 * @param moduleKey
	 * @return
	 */
	LcProcess getLcProcessByModuleKey(String moduleKey);

	/**
	 * 根据部署编号+其它条件发起流程示例（其它条件可省略）
	 * @param lcHisParam
	 * @return
	 */
	BaseResult startProcessInstance(LcHisParam lcHisParam);

	/**
	 * 根据模块Key发起流程实例
	 * @param lcHisParam
	 * @return
	 */
	BaseResult start(LcHisParam lcHisParam);

	/**
	 * 在线设计
	 * @param mxGraphModel
	 * @param request
	 * @param response
	 * @return
	 */
	BaseResult create(MxGraphModel mxGraphModel, HttpServletRequest request, HttpServletResponse response);

	/**
	 * 复制
	 * @param lcProcessParam
	 * @return
	 */
	BaseResult copy(LcProcessParam lcProcessParam, HttpServletRequest request, HttpServletResponse response);

	/**
	 * 导出流程
	 * @param response
	 * @param lc_process_id
	 */
	void exportProcess(HttpServletResponse response,String lc_process_id);

	/**
	 * 导入流程
	 * @param multipartFile
	 * @return
	 */
	BaseResult importProcess(HttpServletRequest request,HttpServletResponse response,MultipartFile multipartFile);
}
