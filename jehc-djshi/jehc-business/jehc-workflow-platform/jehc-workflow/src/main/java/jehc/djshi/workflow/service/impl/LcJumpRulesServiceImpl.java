package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcJumpRulesService;
import jehc.djshi.workflow.dao.LcJumpRulesDao;
import jehc.djshi.workflow.model.LcJumpRules;

/**
* @Desc 自定义跳转规则 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:55:44
*/
@Service("lcJumpRulesService")
public class LcJumpRulesServiceImpl extends BaseService implements LcJumpRulesService{
	@Autowired
	private LcJumpRulesDao lcJumpRulesDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcJumpRules> getLcJumpRulesListByCondition(Map<String,Object> condition){
		try{
			return lcJumpRulesDao.getLcJumpRulesListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param jump_rules_id 
	* @return
	*/
	public LcJumpRules getLcJumpRulesById(String jump_rules_id){
		try{
			LcJumpRules lcJumpRules = lcJumpRulesDao.getLcJumpRulesById(jump_rules_id);
			return lcJumpRules;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcJumpRules 
	* @return
	*/
	public int addLcJumpRules(LcJumpRules lcJumpRules){
		int i = 0;
		try {
			i = lcJumpRulesDao.addLcJumpRules(lcJumpRules);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcJumpRules 
	* @return
	*/
	public int updateLcJumpRules(LcJumpRules lcJumpRules){
		int i = 0;
		try {
			i = lcJumpRulesDao.updateLcJumpRules(lcJumpRules);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcJumpRules 
	* @return
	*/
	public int updateLcJumpRulesBySelective(LcJumpRules lcJumpRules){
		int i = 0;
		try {
			i = lcJumpRulesDao.updateLcJumpRulesBySelective(lcJumpRules);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcJumpRules(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcJumpRulesDao.delLcJumpRules(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
