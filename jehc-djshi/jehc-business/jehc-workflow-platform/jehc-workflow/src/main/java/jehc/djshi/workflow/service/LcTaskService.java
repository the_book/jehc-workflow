package jehc.djshi.workflow.service;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.workflow.model.LcNodeAttribute;
import jehc.djshi.workflow.param.LcHisParam;
import jehc.djshi.workflow.param.LcReceiveNParam;
import jehc.djshi.workflow.param.LcReceiveParam;
import jehc.djshi.workflow.vo.LcAttributeNEntity;
import jehc.djshi.workflow.vo.LcTask;
import jehc.djshi.workflow.vo.LcTaskEntity;
import org.activiti.bpmn.model.UserTask;

import java.util.List;
import java.util.Map;

/**
 * @Desc 任务业务处理服务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcTaskService {

    /**
     * 发起流程实例并设置第一个节点发起人并完成第一个节点
     * @param lcHisParam
     * @return
     */
    BaseResult start(LcHisParam lcHisParam);

    /**
     * 统一审批
     * @param lcReceiveNParam
     * @return
     */
    BaseResult approve(LcReceiveNParam lcReceiveNParam);

    /**
     * 发起流程实例并设置第一个节点发起人并完成第一个节点
     * @param lcReceiveNParam
     * @return
     */
    BaseResult submit(LcReceiveNParam lcReceiveNParam);

    /**
     * 完成任务
     * @param lcReceiveNParam
     * @return
     */
    BaseResult completeTask(LcReceiveNParam lcReceiveNParam);

    /**
     * 同意
     * @param lcReceiveNParam
     * @return
     */
    BaseResult agreement(LcReceiveNParam lcReceiveNParam);

    /**
     * 驳回
     * @param lcReceiveNParam
     * @return
     */
    BaseResult reject(LcReceiveNParam lcReceiveNParam);

    /**
     * 撤回（运行时实例撤回方式）
     * @param lcReceiveNParam
     * @return
     */
    BaseResult callBack(LcReceiveNParam lcReceiveNParam);

    /**
     * 转办
     * @param lcReceiveNParam
     * @return
     */
    BaseResult transfer(LcReceiveNParam lcReceiveNParam);

    /**
     * 加签
     * @param lcReceiveNParam
     * @return
     */
    BaseResult sign(LcReceiveNParam lcReceiveNParam);

    /**
     * 弃权
     * @param lcReceiveNParam
     * @return
     */
    BaseResult waiver(LcReceiveNParam lcReceiveNParam);

    /**
     * 委派
     * @param lcReceiveNParam
     * @return
     */
    BaseResult delegateTask(LcReceiveNParam lcReceiveNParam);

    /**
     * 催办
     * @param lcReceiveNParam
     * @return
     */
    BaseResult urge(LcReceiveNParam lcReceiveNParam);

    /**
     * 终止流程
     * @param lcReceiveNParam
     * @return
     */
    BaseResult termination(LcReceiveNParam lcReceiveNParam);

    /**
     * 设置归属人
     * @param lcReceiveNParam
     * @return
     */
    BaseResult setOwner(LcReceiveNParam lcReceiveNParam);

    /**
     * 根据流程实例id查找流程实例下任务节点
     * @param procInstId
     * @return
     */
    BaseResult<List<UserTask>> getUserTaskListByProcessInstanceId(String procInstId);

    /**
     * 根据当前任务节点任意跳转节点
     * @param lcReceiveNParam
     * @return
     */
    BaseResult executeJump(LcReceiveNParam lcReceiveNParam);

    /**
     * 根据当前任务Id查询可以驳回的任务节点
     * @param taskId
     * @return
     */
    BaseResult findCanBackActivity(String taskId);

    /**
     * 根据任务Id查询下一个节点
     * @param taskId
     * @return
     */
    BaseResult getLcNextNode(String taskId);

    /**
     * 查找行为属性
     * @param taskId
     * @return
     */
    BaseResult getActivityBehavior(String taskId);

    /**
     * 查找当前任务所在节点按流程图走向查找上级节点
     * @param lcReceiveNParam
     * @return
     */
    BaseResult previousNode(LcReceiveNParam lcReceiveNParam);

    /**
     * 查找行为属性
     * @param lcReceiveParam
     * @return
     */
    BaseResult getActivityBehavior(LcReceiveParam lcReceiveParam);

    /**
     * 查询当前任务节点流出线指定属性集合（可用于在办理任务节点操作进行驳回或通过）
     * @param taskId
     * @param attribute
     * @return
     */
    BaseResult getOutTransListByTaskId(String taskId,String attribute);

    /**
     * 查询当前任务流出线名称集合
     * @param taskId
     * @return
     */
    BaseResult getOutTransListByTaskId(String taskId);

    /**
     * 根据任务Id查询自定义节点属性
     * @param taskId
     * @return
     */
    BaseResult<LcNodeAttribute> getLcNodeAttribute(String taskId);

    /**
     * 判断节点是否为会签节点,条件可以单独使用taskId,也可以使用流程实例：processInstanceId+节点id：activityId（多实例）
     * @param lcReceiveNParam
     * @return
     */
    BaseResult multi(LcReceiveNParam lcReceiveNParam);

    /**
     * 查询业务Key
     * @param taskId
     * @return
     */
    BaseResult getBusinessKey(String taskId);

    /**
     * 查询设置Task变量（如在处理当前任务设置变量 在下一个节点中可以获取该变量）
     * @param taskId
     * @param key
     * @return
     */
    BaseResult getTaskVariable(String taskId,String key);

    /**
     * 根据任务Id获取当前任务节点流出线指定属性集合
     * @param taskId
     * @return
     */
    BaseResult getTaskVariable(String taskId);

    /**
     * 设置Task变量（如在处理当前任务设置变量 在下一个节点中可以获取该变量）
     * @param lcReceiveNParam
     * @return
     */
    BaseResult setTaskVariable(LcReceiveNParam lcReceiveNParam);

    /**
     * 向组任务中添加成员
     * @param lcReceiveNParam
     * @return
     */
    BaseResult addGroupUser(LcReceiveNParam lcReceiveNParam);

    /**
     * 向组任务中删除成员
     * @param lcReceiveNParam
     * @return
     */
    BaseResult deleteGroupUser(LcReceiveNParam lcReceiveNParam);

    /**
     * 查找流程实例下任务
     * @param processInstanceId
     * @return
     */
    BaseResult<List<LcTask>> getLcTaskListByProcessInstanceId(String processInstanceId);

    /**
     * 挂起流程实例
     * @param lcReceiveNParam
     * @return
     */
    BaseResult suspendProcess(LcReceiveNParam lcReceiveNParam);

    /**
     * 激活流程实例
     * @param lcReceiveNParam
     * @return
     */
    BaseResult activateProcess(LcReceiveNParam lcReceiveNParam);


    /**
     * 根据id查询当前节点及目标节点属性
     * @param taskId
     * @param condition
     * @return
     */
    BaseResult<LcAttributeNEntity> getLcAttribute(String taskId, Map<String,Object> condition);

    /**
     * 根据模块Key查询发起人所在节点及发起人所在节点的下个节点（查询最新部署）
     * @param moduleKey
     * @return
     */
    BaseResult<LcAttributeNEntity> getLcAttributeByModuleKey(String moduleKey);

    /**
     * 根据模块Key+版本号查询发起人所在节点及发起人所在节点的下个节点（查询最新部署）
     * @param moduleKey
     * @return
     */
    BaseResult<LcAttributeNEntity> getLcAttributeByModuleKey(String moduleKey,String version);

    /**
     * 查询指定节点已审批的人
     * @param lcReceiveNParam
     * @return
     */
    BaseResult getNodeUser(LcReceiveNParam lcReceiveNParam);

    /**
     * 查询节点的未处理人及已处理人
     * @param lcReceiveNParam
     * @return
     */
    BaseResult getNodeApproveUsers(LcReceiveNParam lcReceiveNParam);

    /**
     * 流程干预（任意节点跳转）
     * @param lcReceiveNParam
     * @return
     */
    BaseResult globalTask(LcReceiveNParam lcReceiveNParam);

    /**
     * 流程干预（转办）
     * @param lcReceiveNParam
     * @return
     */
    BaseResult globalTransfer(LcReceiveNParam lcReceiveNParam);

    /**
     * 流程干预（根据流程实例查找最新任务）
     * @param processInstanceId
     * @return
     */
    BaseResult<List<LcTaskEntity>> getRunTaskByInstanceId(String processInstanceId);

    /**
     * 全局强制处理任意节点跳转（包括流程实例已结束，正在运行实例均支持）
     * @param lcReceiveNParam
     * @return
     */
    BaseResult globalForceJumpTask(LcReceiveNParam lcReceiveNParam);

    /**
     * 撤回（流程实例已结束，正在运行强制撤回方式）
     * @param lcReceiveNParam
     * @return
     */
    BaseResult globalForceCallBack(LcReceiveNParam lcReceiveNParam);

}
