package jehc.djshi.workflow.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcNotifyTemplate;

/**
* @Desc 流程通知模板 
* @Author 邓纯杰
* @CreateTime 2022-11-30 11:29:02
*/
public interface LcNotifyTemplateDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcNotifyTemplate> getLcNotifyTemplateListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	LcNotifyTemplate getLcNotifyTemplateById(String id);
	/**
	* 添加
	* @param lcNotifyTemplate 
	* @return
	*/
	int addLcNotifyTemplate(LcNotifyTemplate lcNotifyTemplate);
	/**
	* 修改
	* @param lcNotifyTemplate 
	* @return
	*/
	int updateLcNotifyTemplate(LcNotifyTemplate lcNotifyTemplate);
	/**
	* 修改（根据动态条件）
	* @param lcNotifyTemplate 
	* @return
	*/
	int updateLcNotifyTemplateBySelective(LcNotifyTemplate lcNotifyTemplate);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcNotifyTemplate(Map<String, Object> condition);
}
