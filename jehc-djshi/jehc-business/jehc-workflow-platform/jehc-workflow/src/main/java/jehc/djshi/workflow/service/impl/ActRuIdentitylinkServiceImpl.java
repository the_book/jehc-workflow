package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.workflow.dao.ActRuIdentitylinkDao;
import jehc.djshi.workflow.model.ActRuIdentitylink;
import jehc.djshi.workflow.service.ActRuIdentitylinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 运行时流程人员
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class ActRuIdentitylinkServiceImpl extends BaseService implements ActRuIdentitylinkService {
    @Autowired
    private ActRuIdentitylinkDao actRuIdentitylinkDao;

    /**
     * 查询运行时流程人员集合
     * @param condition
     * @return
     */
    public List<ActRuIdentitylink> getActRuIdentitylinkListByCondition(Map<String,Object> condition){
        return actRuIdentitylinkDao.getActRuIdentitylinkListByCondition(condition);
    }

    /**
     * 查询单个运行时流程人员
     * @param id
     * @return
     */
    public ActRuIdentitylink getActRuIdentitylinkById(String id){
        return actRuIdentitylinkDao.getActRuIdentitylinkById(id);
    }

    /**
     * 删除运行时流程人员
     * @param id
     * @return
     */
    public int delActRuIdentitylink(String id){
        return actRuIdentitylinkDao.delActRuIdentitylink(id);
    }

    /**
     * 删除运行时流程人员
     * @param procInstId
     * @return
     */
    public int delActRuIdentitylinkByProcInstId(String procInstId){
        return actRuIdentitylinkDao.delActRuIdentitylinkByProcInstId(procInstId);
    }
}
