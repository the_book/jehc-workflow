package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.workflow.dao.ActRuTaskDao;
import jehc.djshi.workflow.model.ActRuTask;
import jehc.djshi.workflow.service.ActRuTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * @Desc 运行时任务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class ActRuTaskServiceImpl extends BaseService implements ActRuTaskService {

    @Autowired
    private ActRuTaskDao actRuTaskDao;


    /**
     * 查询运行任务集合
     * @param condition
     * @return
     */
    public List<ActRuTask> getActRuTaskListByCondition(Map<String,Object> condition){
        return actRuTaskDao.getActRuTaskListByCondition(condition);
    }

    /**
     * 查询单个运行任务
     * @param id
     * @return
     */
    public ActRuTask getActRuTaskById(String id){
        return actRuTaskDao.getActRuTaskById(id);
    }



    /**
     * 删除运行任务
     * @param id
     * @return
     */
    public int delActRuTask(String id){
        return actRuTaskDao.delActRuTask(id);
    }

    /**
     * 根据运行实例删除运行任务
     * @param executionId
     * @return
     */
    public int delActRuTaskByExecutionId(String executionId){
        return actRuTaskDao.delActRuTaskByExecutionId(executionId);
    }
}
