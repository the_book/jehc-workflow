package jehc.djshi.workflow.common.listener.worker;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseUtils;
import jehc.djshi.common.entity.UserinfoEntity;
import jehc.djshi.common.util.SpringUtils;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcNodeAttribute;
import jehc.djshi.workflow.model.LcNotifyTemplate;
import jehc.djshi.workflow.util.LcNtyUtil;
import jehc.djshi.workflow.util.message.MailService;
import jehc.djshi.workflow.util.message.vo.Constant;
import jehc.djshi.workflow.util.message.vo.MailDTO;
import jehc.djshi.workflow.util.message.vo.MessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.task.IdentityLink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 邮件发送业务处理线程
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class MailWorker implements Runnable{
    /**
     *
     */
    private MessageDTO messageDTO;

    private LcNodeAttribute lcNodeAttribute;

    /**
     *
     */
    public MailWorker(){

    }

    /**
     *
     * @param messageDTO
     */
    public MailWorker(MessageDTO messageDTO){
        this.messageDTO = messageDTO;
    }

    /**
     *
     * @param messageDTO
     */
    public MailWorker(LcNodeAttribute lcNodeAttribute,MessageDTO messageDTO){
        this.messageDTO = messageDTO;
        this.lcNodeAttribute = lcNodeAttribute;
    }

    /**
     *
     */
    public void run(){
        try {
            MailService mailService = SpringUtils.getBean(MailService.class);
            MailDTO mailDTO = new MailDTO();
//        mailDTO.setFrom("XXXXX.com");//废弃
            LcNotifyTemplate template = null;
            if(null != lcNodeAttribute){
                List<LcNotifyTemplate> lcNotifyTemplates = lcNodeAttribute.getLcNotifyTemplates();
                template = LcNtyUtil.getlcNotifyTemplate(lcNotifyTemplates,Constant.MAIL);
            }
            if(null == template){//默认
                mailDTO.setHtml(Constant.MAIL_HTML);
                mailDTO.setSubject(Constant.MAIL_SUBJECT);
            }else{
                mailDTO.setHtml(template.getContent());
                mailDTO.setSubject(template.getTitle());
            }
            List<String> tos = new ArrayList<>();
            List<String> userIds = new ArrayList<>();
            List<String> departIds = new ArrayList<>();
            List<String> postIds = new ArrayList<>();
            List<String> roleIds = new ArrayList<>();
            List<IdentityLink> identityLinks = messageDTO.getCandidators();
            /*
            if(!StringUtil.isEmpty(messageDTO.getAssigne())){
                userIds.add(messageDTO.getAssigne());
            }
            */
            Map<String,Object> condition = new HashMap<>();
            if(CollectionUtil.isNotEmpty(identityLinks)){
                for(IdentityLink identityLink: identityLinks){
                    if(!StringUtil.isEmpty(identityLink.getUserId())){
                        userIds.add(identityLink.getUserId());
                    }
                    if(!StringUtil.isEmpty(identityLink.getGroupId())){
                        departIds.add(identityLink.getGroupId().replaceAll("DEPART:",""));
                    }
                    if(!StringUtil.isEmpty(identityLink.getGroupId())){
                        postIds.add(identityLink.getGroupId().replaceAll("POST:",""));
                    }
                    if(!StringUtil.isEmpty(identityLink.getGroupId())){
                        roleIds.add(identityLink.getGroupId().replaceAll("ROLE:",""));
                    }
                }
            }
            if(CollectionUtil.isNotEmpty(userIds)){
                condition.put("xt_userinfo_id",userIds.toArray(new String[]{}));
            }
            if(CollectionUtil.isNotEmpty(postIds)){
                condition.put("xt_post_id",postIds.toArray(new String[]{}));
            }
            if(CollectionUtil.isNotEmpty(departIds)){
                condition.put("xt_departinfo_id",departIds.toArray(new String[]{}));
            }
            if(CollectionUtil.isNotEmpty(condition)){
                BaseUtils baseUtils = SpringUtils.getBean(BaseUtils.class);
                List<UserinfoEntity> userinfoEntities = baseUtils.getUserinfoEntityList(condition);
                if(CollectionUtil.isNotEmpty(userinfoEntities)){
                    for(UserinfoEntity userinfoEntity: userinfoEntities){
                        if(!StringUtil.isEmpty(userinfoEntity.getXt_userinfo_email())){
                            tos.add(userinfoEntity.getXt_userinfo_email());
                        }else{
                            log.warn("用户:{0},账号：{1} 未配置邮箱地址",userinfoEntity.getXt_userinfo_realName(),userinfoEntity.getXt_userinfo_name());
                        }
                    }
                }
            }
            if(CollectionUtil.isNotEmpty(tos)){
                mailDTO.setTos(tos);
                mailService.send(mailDTO);
            }else{
                log.warn("没有接收人，不能发送邮件！");
            }
        }catch (Exception e){
            log.error("发送邮件失败，异常信息：{}",e);
        }
    }
}
