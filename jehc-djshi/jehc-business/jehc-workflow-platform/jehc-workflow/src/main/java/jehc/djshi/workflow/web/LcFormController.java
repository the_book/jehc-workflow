package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcForm;
import jehc.djshi.workflow.service.LcFormService;

/**
* @Desc 工作流表单 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:33:49
*/
@Api(value = "工作流表单",description = "工作流表单", tags = "工作流表单")
@RestController
@RequestMapping("/lcForm")
public class LcFormController extends BaseAction{
	@Autowired
	private LcFormService lcFormService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcForm>> getLcFormListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcForm> lcFormList = lcFormService.getLcFormListByCondition(condition);
		for(LcForm lcForm:lcFormList){
			if(!StringUtil.isEmpty(lcForm.getCreate_id())){
				OauthAccountEntity createBy = getAccount(lcForm.getCreate_id());
				if(null != createBy){
					lcForm.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(lcForm.getUpdate_id())){
				OauthAccountEntity modifieBy = getAccount(lcForm.getUpdate_id());
				if(null != modifieBy){
					lcForm.setModifiedBy(modifieBy.getName());
				}
			}
		}
		PageInfo<LcForm> page = new PageInfo<LcForm>(lcFormList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param lc_form_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{lc_form_id}")
	public BaseResult<LcForm> getLcFormById(@PathVariable("lc_form_id")String lc_form_id){
		LcForm lcForm = lcFormService.getLcFormById(lc_form_id);
		OauthAccountEntity createBy = getAccount(lcForm.getCreate_id());
		if(null != createBy){
			lcForm.setCreateBy(createBy.getName());
		}
		if(!StringUtil.isEmpty(lcForm.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(lcForm.getUpdate_id());
			if(null != modifiedBy){
				lcForm.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(lcForm);
	}
	/**
	* 添加
	* @param lcForm 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcForm(@RequestBody LcForm lcForm){
		int i = 0;
		if(null != lcForm){
			lcForm.setLc_form_id(toUUID());
			lcForm.setCreate_id(getXtUid());
			lcForm.setCreate_time(getDate());
			i=lcFormService.addLcForm(lcForm);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcForm 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcForm(@RequestBody LcForm lcForm){
		int i = 0;
		if(null != lcForm){
			lcForm.setUpdate_id(getXtUid());
			lcForm.setUpdate_time(getDate());
			i=lcFormService.updateLcForm(lcForm);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param lc_form_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcForm(String lc_form_id){
		int i = 0;
		if(!StringUtil.isEmpty(lc_form_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_form_id",lc_form_id.split(","));
			i=lcFormService.delLcForm(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
