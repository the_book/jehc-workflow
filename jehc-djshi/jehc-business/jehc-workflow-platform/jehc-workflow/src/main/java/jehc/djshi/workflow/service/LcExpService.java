package jehc.djshi.workflow.service;

import jehc.djshi.workflow.model.LcExp;

import java.util.List;
import java.util.Map;

/**
 * @Desc 流程表达式
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcExpService {
    /**
     *
     * @param condition
     * @return
     */
    List<LcExp> getLcExpListByCondition(Map<String, Object> condition);

    /**
     *
     * @param lc_exp_id
     * @return
     */
    LcExp getLcExpById(String lc_exp_id);

    /**
     * 添加
     * @param lcExp
     * @return
     */
    int addLcExp(LcExp lcExp);

    /**
     * 修改
     * @param lcExp
     * @return
     */
    int updateLcExp(LcExp lcExp);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delLcExp(Map<String, Object> condition);
}
