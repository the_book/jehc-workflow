package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcNodeFormFieldService;
import jehc.djshi.workflow.dao.LcNodeFormFieldDao;
import jehc.djshi.workflow.model.LcNodeFormField;

/**
* @Desc 节点字段 
* @Author 邓纯杰
* @CreateTime 2022-04-06 11:41:43
*/
@Service("lcNodeFormFieldService")
public class LcNodeFormFieldServiceImpl extends BaseService implements LcNodeFormFieldService{
	@Autowired
	private LcNodeFormFieldDao lcNodeFormFieldDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcNodeFormField> getLcNodeFormFieldListByCondition(Map<String,Object> condition){
		try{
			return lcNodeFormFieldDao.getLcNodeFormFieldListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param node_field_id 
	* @return
	*/
	public LcNodeFormField getLcNodeFormFieldById(String node_field_id){
		try{
			LcNodeFormField lcNodeFormField = lcNodeFormFieldDao.getLcNodeFormFieldById(node_field_id);
			return lcNodeFormField;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcNodeFormField 
	* @return
	*/
	public int addLcNodeFormField(LcNodeFormField lcNodeFormField){
		int i = 0;
		try {
			i = lcNodeFormFieldDao.addLcNodeFormField(lcNodeFormField);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcNodeFormField 
	* @return
	*/
	public int updateLcNodeFormField(LcNodeFormField lcNodeFormField){
		int i = 0;
		try {
			i = lcNodeFormFieldDao.updateLcNodeFormField(lcNodeFormField);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcNodeFormField 
	* @return
	*/
	public int updateLcNodeFormFieldBySelective(LcNodeFormField lcNodeFormField){
		int i = 0;
		try {
			i = lcNodeFormFieldDao.updateLcNodeFormFieldBySelective(lcNodeFormField);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcNodeFormField(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcNodeFormFieldDao.delLcNodeFormField(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
