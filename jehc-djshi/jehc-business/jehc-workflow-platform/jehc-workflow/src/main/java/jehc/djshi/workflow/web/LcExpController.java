package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcExp;
import jehc.djshi.workflow.service.LcExpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 流程表达式
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "流程表达式", description = "流程表达式",tags = "流程表达式")
@RestController
@RequestMapping("/lcExp")
public class LcExpController extends BaseAction {

    @Autowired
    LcExpService lcExpService;

    /**
     * 查询列表
     * @param baseSearch
     */
    @ApiOperation(value="查询列表", notes="查询列表")
    @PostMapping(value="/list")
    @NeedLoginUnAuth
    public BaseResult<List<LcExp>> getLcExpListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<LcExp> lcExps = lcExpService.getLcExpListByCondition(condition);
        for(LcExp lcExp : lcExps){
            if(!StringUtil.isEmpty(lcExp.getCreate_id())){
                OauthAccountEntity createBy = getAccount(lcExp.getCreate_id());
                if(null != createBy){
                    lcExp.setCreateBy(createBy.getName());
                }
            }
            if(!StringUtil.isEmpty(lcExp.getUpdate_id())){
                OauthAccountEntity modifieBy = getAccount(lcExp.getUpdate_id());
                if(null != modifieBy){
                    lcExp.setModifiedBy(modifieBy.getName());
                }
            }
        }
        PageInfo<LcExp> page = new PageInfo<LcExp>(lcExps);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个对象
     * @param lc_exp_id
     */
    @ApiOperation(value="查询单个对象", notes="查询单个对象")
    @GetMapping(value="/get/{lc_exp_id}")
    @NeedLoginUnAuth
    public BaseResult<LcExp> getLcExpById(@PathVariable("lc_exp_id")String lc_exp_id){
        LcExp lcExp = lcExpService.getLcExpById(lc_exp_id);
        if(!StringUtil.isEmpty(lcExp.getCreate_id())){
            OauthAccountEntity createBy = getAccount(lcExp.getCreate_id());
            if(null != createBy){
                lcExp.setCreateBy(createBy.getName());
            }
        }
        if(!StringUtil.isEmpty(lcExp.getUpdate_id())){
            OauthAccountEntity modifiedBy = getAccount(lcExp.getUpdate_id());
            if(null != modifiedBy){
                lcExp.setModifiedBy(modifiedBy.getName());
            }
        }
        return outDataStr(lcExp);
    }
    /**
     * 添加
     * @param lcExp
     */
    @ApiOperation(value="添加", notes="添加")
    @PostMapping(value="/add")
    public BaseResult addLcExp(@RequestBody LcExp lcExp){
        int i = 0;
        if(null != lcExp){
            lcExp.setCreate_time(getDate());
            lcExp.setCreate_id(getXtUid());
            lcExp.setLc_exp_id(toUUID());
            i=lcExpService.addLcExp(lcExp);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 修改
     * @param lcExp
     */
    @ApiOperation(value="修改", notes="修改")
    @PutMapping(value="/update")
    public BaseResult updateLcExp(@RequestBody LcExp lcExp){
        int i = 0;
        if(null != lcExp){
            lcExp.setUpdate_id(getXtUid());
            lcExp.setUpdate_time(getDate());
            i=lcExpService.updateLcExp(lcExp);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 删除
     * @param lc_exp_id
     */
    @ApiOperation(value="删除", notes="删除")
    @DeleteMapping(value="/delete")
    public BaseResult delLcExp(String lc_exp_id){
        int i = 0;
        if(!StringUtil.isEmpty(lc_exp_id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("lc_exp_id",lc_exp_id.split(","));
            i=lcExpService.delLcExp(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
}
