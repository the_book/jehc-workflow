package jehc.djshi.workflow.web;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcDeploymentHis;
import jehc.djshi.workflow.param.LcReceiveParam;
import jehc.djshi.workflow.service.ActHiProcinstService;
import jehc.djshi.workflow.service.ActHiTaskinstService;
import jehc.djshi.workflow.service.LcDeploymentHisService;
import jehc.djshi.workflow.util.ActivitiUtil;
import jehc.djshi.workflow.vo.LcReportInstEntity;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 流程报表
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "流程报表",description = "流程报表",tags = "流程报表")
@RestController
@RequestMapping("/lcReport")
public class LcReportController extends BaseAction {

    @Autowired
    private LcDeploymentHisService lcDeploymentHisService;

    @Autowired
    private ActivitiUtil activitiUtil;

    @Autowired
    private ActHiProcinstService actHiProcinstService;

    @Autowired
    private ActHiTaskinstService actHiTaskinstService;

    /**
     * 流程运行实例统计
     * @param lcReceiveParam
     */
    @ApiOperation(value="流程运行实例统计", notes="流程运行实例统计")
    @PostMapping(value="/inst")
    @NeedLoginUnAuth
    public BaseResult<List<LcReportInstEntity>> inst(@RequestBody LcReceiveParam lcReceiveParam){
        Map<String,Object>condition = new HashMap<>();
        List<LcDeploymentHis> lcDeploymentHisList = lcDeploymentHisService.getLcDeploymentHisListByCondition(condition);
        List<LcReportInstEntity> lcReportInstEntities = new ArrayList<>();
        if(!CollectionUtil.isEmpty(lcDeploymentHisList)){
            for(LcDeploymentHis lcDeploymentHis: lcDeploymentHisList){
                Long totalCount = instCount(""+lcReceiveParam.getType(),lcDeploymentHis.getLc_deployment_his_id());
                LcReportInstEntity lcReportInstEntity = new LcReportInstEntity(lcDeploymentHis.getLc_deployment_his_name(),""+totalCount);
                lcReportInstEntities.add(lcReportInstEntity);
            }
        }
        return new BaseResult(lcReportInstEntities);
    }

    /**
     * 统计运行实例
     * @param type 类型 (空全部 1已完成 2运行中)
     * @param lc_deployment_his_id
     * @return
     */
    public Long instCount(String type,String lc_deployment_his_id){
        Long totalCount = 0L;
        HistoricActivityInstanceQuery historicActivityInstanceQuery=activitiUtil.getHistoryService().createHistoricActivityInstanceQuery(); // 创建历史活动实例查询
        //设置查询条件
        ProcessDefinition processDefinition = activitiUtil.getProcessDefinition(lc_deployment_his_id);
        historicActivityInstanceQuery.processDefinitionId(processDefinition.getId());
        if(type.equals("1")){
            //设置只查询已完成的
            historicActivityInstanceQuery.finished();
        }else if(type.equals("2")){
            historicActivityInstanceQuery.unfinished();
        }else{
            //全部
        }
        totalCount = historicActivityInstanceQuery.count();
        return totalCount;
    }


    /**
     * 流程运行实例平均时间统计
     * @param lcReceiveParam
     */
    @ApiOperation(value="流程运行实例平均时间统计", notes="流程运行实例平均时间统计")
    @PostMapping(value="/instTime")
    @NeedLoginUnAuth
    public BaseResult<List<LcReportInstEntity>> instTime(@RequestBody LcReceiveParam lcReceiveParam){
        Map<String,Object>condition = new HashMap<>();
        List<LcDeploymentHis> lcDeploymentHisList = lcDeploymentHisService.getLcDeploymentHisListByCondition(condition);
        List<LcReportInstEntity> lcReportInstEntities = new ArrayList<>();
        if(!CollectionUtil.isEmpty(lcDeploymentHisList)){
            for(LcDeploymentHis lcDeploymentHis: lcDeploymentHisList){
                String totalCount = instTimeCount(lcDeploymentHis.getLc_deployment_his_id());
                if(StringUtil.isEmpty(totalCount)){
                    totalCount =  "0";
                }
                LcReportInstEntity lcReportInstEntity = new LcReportInstEntity(lcDeploymentHis.getLc_deployment_his_name(),""+totalCount);
                lcReportInstEntities.add(lcReportInstEntity);
            }
        }
        return new BaseResult(lcReportInstEntities);
    }

    /**
     * 流程运行实例时间统计
     * @param lc_deployment_his_id
     * @return
     */
    public String instTimeCount(String lc_deployment_his_id){
        ProcessDefinition processDefinition = activitiUtil.getProcessDefinition(lc_deployment_his_id);
        return actHiProcinstService.getActHiProcinstAvgTime(processDefinition.getId());
    }

    /**
     * 流程任务运行统计
     * @param lcReceiveParam
     */
    @ApiOperation(value="流程任务运行统计", notes="流程任务运行统计")
    @PostMapping(value="/task")
    @NeedLoginUnAuth
    public BaseResult<List<LcReportInstEntity>> task(@RequestBody LcReceiveParam lcReceiveParam){
        Map<String,Object>condition = new HashMap<>();
        List<LcDeploymentHis> lcDeploymentHisList = lcDeploymentHisService.getLcDeploymentHisListByCondition(condition);
        List<LcReportInstEntity> lcReportInstEntities = new ArrayList<>();
        if(!CollectionUtil.isEmpty(lcDeploymentHisList)){
            for(LcDeploymentHis lcDeploymentHis: lcDeploymentHisList){
                Long totalCount = taskCount(""+lcReceiveParam.getType(),lcDeploymentHis.getLc_deployment_his_id());
                LcReportInstEntity lcReportInstEntity = new LcReportInstEntity(lcDeploymentHis.getLc_deployment_his_name(),""+totalCount);
                lcReportInstEntities.add(lcReportInstEntity);
            }
        }
        return new BaseResult(lcReportInstEntities);
    }

    /**
     * 统计流程任务运行统计
     * @param type 类型 (空全部 1已完成 2运行中)
     * @param lc_deployment_his_id
     * @return
     */
    public Long taskCount(String type,String lc_deployment_his_id){
       Long totalCount = 0L;
        if(type.equals("1")){
            //设置只查询已完成的
            totalCount =  activitiUtil.getHistoryService().createHistoricTaskInstanceQuery().deploymentId(lc_deployment_his_id).unfinished().count();
        }else if(type.equals("2")){
            totalCount = activitiUtil.getHistoryService().createHistoricTaskInstanceQuery().deploymentId(lc_deployment_his_id).finished().count();
        }else{
            //全部
            totalCount = activitiUtil.getHistoryService().createHistoricTaskInstanceQuery().deploymentId(lc_deployment_his_id).count();
        }

        return totalCount;
    }


    /**
     * 统计流程任务平均运行时间统计
     * @param lcReceiveParam
     */
    @ApiOperation(value="统计流程任务平均运行时间统计", notes="统计流程任务平均运行时间统计")
    @PostMapping(value="/taskTime")
    @NeedLoginUnAuth
    public BaseResult<List<LcReportInstEntity>> taskTime(@RequestBody LcReceiveParam lcReceiveParam){
        Map<String,Object>condition = new HashMap<>();
        List<LcDeploymentHis> lcDeploymentHisList = lcDeploymentHisService.getLcDeploymentHisListByCondition(condition);
        List<LcReportInstEntity> lcReportInstEntities = new ArrayList<>();
        if(!CollectionUtil.isEmpty(lcDeploymentHisList)){
            for(LcDeploymentHis lcDeploymentHis: lcDeploymentHisList){
                String totalCount = taskTimeCount(lcDeploymentHis.getLc_deployment_his_id());
                if(StringUtil.isEmpty(totalCount)){
                    totalCount = "0";
                }
                LcReportInstEntity lcReportInstEntity = new LcReportInstEntity(lcDeploymentHis.getLc_deployment_his_name(),""+totalCount);
                lcReportInstEntities.add(lcReportInstEntity);
            }
        }
        return new BaseResult(lcReportInstEntities);
    }

    /**
     * 统计流程任务平均运行时间统计
     * @param lc_deployment_his_id
     * @return
     */
    public String taskTimeCount(String lc_deployment_his_id){
        ProcessDefinition processDefinition = activitiUtil.getProcessDefinition(lc_deployment_his_id);
        return actHiTaskinstService.getActHiTaskInstAvgTime(processDefinition.getId());
    }
}
