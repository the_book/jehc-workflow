package jehc.djshi.workflow.web;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.annotation.AuthNeedLogin;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.base.*;
import jehc.djshi.common.util.*;
import jehc.djshi.workflow.design.*;
import jehc.djshi.workflow.model.LcApply;
import jehc.djshi.workflow.model.LcDeploymentHis;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.file.FileUtil;
import jehc.djshi.workflow.model.LcNodeAttribute;
import jehc.djshi.workflow.param.LcHisParam;
import jehc.djshi.workflow.param.LcImgParam;
import jehc.djshi.workflow.param.LcProcessParam;
import jehc.djshi.workflow.param.NodeAttributeParam;
import jehc.djshi.workflow.service.*;
import jehc.djshi.workflow.util.ActivitiUtil;
import jehc.djshi.workflow.model.LcProcess;
import jehc.djshi.workflow.vo.*;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.activiti.bpmn.model.EndEvent;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 流程信息存储记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "流程管理",description = "流程管理", tags = "流程配置，部署，发布等等")
@RestController
@RequestMapping("/lcProcess")
@Slf4j
public class LcProcessController  extends BaseAction {

	@Autowired
	private LcProcessService lcProcessService;

	@Autowired
    ActivitiUtil activitiUtil;

	@Autowired
	private LcApplyService lcApplyService;

	@Autowired
	private LcDeploymentHisService lcDeploymentHisService;

	@Autowired
	private LcNodeAttributeService lcNodeAttributeService;

	@Autowired
	private LcTaskService lcTaskService;

	/**
	 * 查询并分页
	 * @param baseSearch
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@PostMapping(value="/list")
	@NeedLoginUnAuth
	public BasePage<List<LcProcess>> getLcProcessBListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcProcess> lcProcessList = lcProcessService.getLcProcessListByCondition(condition);
		for(LcProcess lcProcess:lcProcessList){
			if(!StringUtil.isEmpty(lcProcess.getCreate_id())){
				OauthAccountEntity createBy = getAccount(lcProcess.getCreate_id());
				if(null != createBy){
					lcProcess.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(lcProcess.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(lcProcess.getUpdate_id());
				if(null != modifiedBy){
					lcProcess.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<LcProcess> page = new PageInfo<LcProcess>(lcProcessList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 查询单条记录
	 * @param lc_process_id
	 */
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@GetMapping(value="/get/{lc_process_id}")
	@NeedLoginUnAuth
	public BaseResult<LcProcess> getLcProcessById(@PathVariable("lc_process_id")String lc_process_id){
		LcProcess lcProcess = lcProcessService.getLcProcessById(lc_process_id);
		if(!StringUtil.isEmpty(lcProcess.getCreate_id())){
			OauthAccountEntity createBy = getAccount(lcProcess.getCreate_id());
			if(null != createBy){
				lcProcess.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(lcProcess.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(lcProcess.getUpdate_id());
			if(null != modifiedBy){
				lcProcess.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(lcProcess);
	}
	/**
	 * 添加
	 * @param lcProcess
	 */
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcProcess(@RequestBody LcProcess lcProcess){
		int i = 0;
		if(null != lcProcess){
			lcProcess.setLc_process_id(toUUID());
			i=lcProcessService.addLcProcess(lcProcess);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 修改
	 * @param lcProcess
	 */
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcProcess(@RequestBody LcProcess lcProcess){
		int i = 0;
		if(null != lcProcess){
			i=lcProcessService.updateLcProcess(lcProcess);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	 * 删除
	 * @param lc_process_id
	 * @param request
	 */
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcProcess(String lc_process_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(lc_process_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_process_id",lc_process_id.split(","));
			i=lcProcessService.delLcProcess(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 通过在线设计生成流程信息
	 * @param mxGraphModel
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value="在线设计", notes="通过在线设计生成流程信息")
	@PostMapping(value="/create")
	public BaseResult<LcProcess> create(@RequestBody MxGraphModel mxGraphModel, HttpServletRequest request, HttpServletResponse response){
		return lcProcessService.create(mxGraphModel,request,response);
	}

	/**
	 * 部署流程
	 * @param lcProcess
	 * @return
	 */
	@ApiOperation(value="部署流程", notes="部署流程")
	@PutMapping(value="/createDeployment")
	public BaseResult createDeployment(@RequestBody LcProcess lcProcess){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("lc_process_id", lcProcess.getLc_process_id());
		condition.put("lc_process_status", 1);
		condition.put("update_id",getXtUid());
		condition.put("update_time",getDate());
		int i = lcProcessService.updateLcProcessStatus(lcProcess.getLc_process_id(), condition);
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 根据部署编号+其它条件发起流程示例（其它条件可省略）
	 * @param lcHisParam
	 * @return
	 */
	@ApiOperation(value="根据部署编号+其它条件发起流程示例（其它条件可省略）", notes="根据部署编号+其它条件发起流程示例（其它条件可省略）")
	@PostMapping(value="/startProcessInstance")
	public BaseResult startProcessInstance(@RequestBody LcHisParam lcHisParam){
		return lcProcessService.startProcessInstance(lcHisParam);
	}

	/**
	 * 获取所有实例
	 * @param lc_deployment_his_id
	 * @return
	 */
	@ApiOperation(value="获取所有实例", notes="获取所有实例")
	@GetMapping(value="/getProcessInstance")
	public BaseResult getProcessInstance(String lc_deployment_his_id){
		ProcessDefinition processDefinition = activitiUtil.getProcessDefinition(lc_deployment_his_id);
		List<ProcessInstance> list = activitiUtil.getProcessInstaceListById(processDefinition.getId());
		JSONArray jsonArray = new JSONArray();
		Map<String, Object> model = new HashMap<String, Object>();
		for(int i = 0; i < list.size(); i++){
			ProcessInstance processInstance = list.get(i);
			model.put("id", processInstance.getId());
			model.put("name",processInstance.getName()) ;
			model.put("processDefinitionName", processInstance.getProcessDefinitionName());
			model.put("processDefinitionVersion", processInstance.getProcessDefinitionVersion());
			if(processInstance.isSuspended()){
				model.put("suspended", "是");
			}else{
				model.put("suspended", "否");
			}
			if(activitiUtil.isEnded(processInstance.getId())){
				model.put("pstatus", "已结束");
			}else{
				model.put("pstatus", "运行中");
			}
			model.put("description", processInstance.getDescription());
			model.put("processDefinitionId", processInstance.getProcessDefinitionId());
			model.put("processDefinitionKey", processInstance.getProcessDefinitionKey());
			jsonArray.add(model);
		}
		return outItemsStr(jsonArray);
	}

	/**
	 * 根据taskId高亮图
	 * @param taskId
	 * @return
	 */
	@ApiOperation(value="根据taskId高亮图", notes="根据taskId高亮图")
	@GetMapping(value = "/view/image/{taskId}")
	@AuthUneedLogin
	public BaseResult getImageByTaskId(@PathVariable("taskId")String taskId){
		BaseResult baseResult = new BaseResult();
		try {
			HistoricTaskInstance historicTaskInstance = activitiUtil.getTaskById(taskId);
			String definitionId = historicTaskInstance.getProcessDefinitionId();
			LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisByHisId(definitionId);
			List<ActivityImplEntity> activityImplEntities = activitiUtil.getImageByInstance(historicTaskInstance.getProcessInstanceId());
			/*
			LcProcess lcProcess = lcProcessService.getLcProcessById(lcDeploymentHis.getLc_process_id());
			String versionPath = "";
			if(null != lcDeploymentHis.getVersion()){
				versionPath = lcDeploymentHis.getVersion()+"/";
			}
			String attachPath = FileUtil.validOrCreateFile(getXtPathCache("ActivitiLc").get(0).getXt_path()+lcProcess.getLc_process_title()+"/"+versionPath+lcProcess.getLc_process_img_path());			String base64 = Base64Util.imgToBase64ByLocal(attachPath);
			map.put("image", "data:image/png;base64," + base64);
			*/
			ActivityImplImageEntity activityImplImageEntity = new ActivityImplImageEntity(lcDeploymentHis.getLc_process_mxgraphxml(),lcDeploymentHis.getLc_process_mxgraph_style(),activityImplEntities);
			baseResult.setData(activityImplImageEntity);
		}catch (Exception e){
			log.error("根据taskId高亮图异常，{}",e);
			throw new ExceptionUtil("根据taskId高亮图异常,异常信息："+e.getMessage());
		}
		return baseResult;
	}

	/**
	 * 根据实例id高亮图
	 * @param processInstanceId
	 * @return
	 */
	@ApiOperation(value="根据实例id高亮图", notes="根据实例id高亮图")
	@GetMapping(value = "/image/{processInstanceId}")
	@AuthUneedLogin
	public BaseResult getImageByInstance(@PathVariable("processInstanceId")String processInstanceId){
		BaseResult baseResult = new BaseResult();
		try {
			ProcessDefinition processDefinition = activitiUtil.getProcessDefinitionByInstanceId(processInstanceId);
			String deploymentId = processDefinition.getDeploymentId();
			List<ActivityImplEntity> activityImplEntities = activitiUtil.getImageByInstance(processInstanceId);
			LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisByHisId(deploymentId);
			/*
			LcProcess lcProcess = lcProcessService.getLcProcessById(lcDeploymentHis.getLc_process_id());
			String versionPath = "";
			if(null != lcDeploymentHis.getVersion()){
				versionPath = lcDeploymentHis.getVersion()+"/";
			}
			String attachPath = FileUtil.validOrCreateFile(getXtPathCache("ActivitiLc").get(0).getXt_path()+lcProcess.getLc_process_title()+"/"+versionPath+lcProcess.getLc_process_img_path());
			String base64 = Base64Util.imgToBase64ByLocal(attachPath);
			map.put("image", "data:image/png;base64," + base64);
			*/
			ActivityImplImageEntity activityImplImageEntity = new ActivityImplImageEntity(lcDeploymentHis.getLc_process_mxgraphxml(),lcDeploymentHis.getLc_process_mxgraph_style(),activityImplEntities);
			baseResult.setData(activityImplImageEntity);
			baseResult.setSuccess(true);
		}catch (Exception e){
			log.error("根据实例id高亮图异常，{}",e);
			throw new ExceptionUtil("根据实例id高亮图异常,异常信息："+e.getMessage());
		}
		return baseResult;
	}

	/**
	 * 根据流程部署编号+申请编号查找流程图
	 * @param deploymentId
	 * @throws Exception
	 */
	@ApiOperation(value="根据流程部署编号+申请编号查找流程图", notes="根据流程部署编号+申请编号查找流程图")
	@AuthUneedLogin
	@RequestMapping(value="/image/{deploymentId}/{lc_apply_id}")
	public BaseResult getImageByDeploymentId(@PathVariable("deploymentId")String deploymentId,@PathVariable("lc_apply_id")String lc_apply_id){
		BaseResult baseResult = new BaseResult();
		Map<String,Object> map = new HashMap<>();
		// 从仓库中找需要展示的文件
		List<String> names = activitiUtil.getRepositoryService().getDeploymentResourceNames(deploymentId);
		String imageName = null;
		for (String name : names) {
			if(name.indexOf(".png")>=0){
				imageName = name;
			}
		}
		if(imageName!=null){
			LcApply lcApply = lcApplyService.getLcApplyById(lc_apply_id);
			if(lcApply != null){
				Map<String, Object> condition = new HashMap<String, Object>();
//				condition.put("xt_constant_id", lcApply.getModelKey());
				LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisNewUnique(condition);
				LcProcess lcProcess = lcProcessService.getLcProcessById(lcDeploymentHis.getLc_process_id());
				String attachPath = FileUtil.validOrCreateFile(getXtPathCache("ActivitiLc").get(0).getXt_path()+lcProcess.getLc_process_title()+"/"+lcProcess.getLc_process_img_path());
				String base64 = Base64Util.imgToBase64ByLocal(attachPath);
				map.put("image",base64);
				baseResult.setData(map);
			}
		}
		return baseResult;
	}

	/**
	 * 根据申请编号查找流程图
	 * @param lc_apply_id
	 * @throws Exception
	 */
	@ApiOperation(value="根据申请编号查找流程图", notes="根据申请编号查找流程图")
	@AuthUneedLogin
	@RequestMapping(value="/viewImage/{lc_apply_id}")
	public BaseResult getImageByLcApplyId(@PathVariable("lc_apply_id")String lc_apply_id){
		LcApply lcApply = lcApplyService.getLcApplyById(lc_apply_id);
		BaseResult baseResult = new BaseResult();
		Map<String,Object> map = new HashMap<>();
		if(lcApply != null){
			Map<String, Object> condition = new HashMap<String, Object>();
//			condition.put("xt_constant_id", lcApply.getModelKey());
			LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisNewUnique(condition);
			LcProcess lc_Process = lcProcessService.getLcProcessById(lcDeploymentHis.getLc_process_id());
			String attachPath = FileUtil.validOrCreateFile(getXtPathCache("ActivitiLc").get(0).getXt_path()+lc_Process.getLc_process_title()+"/"+lc_Process.getLc_process_img_path());
			String base64 = Base64Util.imgToBase64ByLocal(attachPath);
			map.put("image",base64);
			baseResult.setData(map);
		}
		return baseResult;
	}

	/**
	 * 挂起流程实例（可作为撤销流程）
	 * @param id
	 * @return
	 */
	@ApiOperation(value="挂起流程实例", notes="挂起流程实例")
	@GetMapping(value="/suspendProcessInstanceById")
	public BaseResult suspendProcessInstanceById(String id){
		boolean flag = activitiUtil.suspendProcessInstanceById(id);
		return outAudStr(flag);
	}

	/**
	 * 激活流程实例(开启流程实例)
	 * @param id
	 * @return
	 */
	@ApiOperation(value="激活流程实例", notes="激活流程实例")
	@GetMapping(value="/activateProcessInstanceById")
	public BaseResult activateProcessInstanceById(String id){
		boolean flag = activitiUtil.activateProcessInstanceById(id);
		return outAudStr(flag);
	}

	/**
	 * 完成任务
	 * @return
	 */
	@ApiOperation(value="完成任务", notes="完成任务")
	@GetMapping(value="/completeTask")
	public BaseResult completeTask(String taskId){
		return outAudStr(activitiUtil.completeTask(taskId));
	}

	/**
	 * 下载Bpmn文件
	 * @param lc_process_id
	 * @param response
	 */
	@ApiOperation(value="下载Bpmn文件", notes="下载Bpmn文件")
	@GetMapping(value="/downFileBpmn")
	@AuthUneedLogin
	public void downFile(String lc_process_id, HttpServletResponse response){
		LcProcess lcProcess = lcProcessService.getLcProcessById(lc_process_id);
		String versionPath = "";
		if(null != lcProcess.getVersion()){
			versionPath = lcProcess.getVersion()+"/";
		}
		response.setContentType("text/html;charset=utf-8");
		String attachPath = FileUtil.validOrCreateFile(getXtPathCache("ActivitiLc").get(0).getXt_path()+lcProcess.getLc_process_title()+"/"+versionPath+lcProcess.getLc_process_bpmn_path());
		File file = new File(attachPath);
		String xt_attachment_name=lcProcess.getLc_process_bpmn_path();
		try {
			if(!file.exists()){
				response.getWriter().println("<script>window.parent.toastrBoot(3,'文件不存在');</script>");
			}else{
				int len=0;
				byte []buffers = new byte[1024];
				BufferedInputStream br = null;
				OutputStream ut = null;
				response.reset();
				response.setContentType("application/x-msdownload");
				response.setHeader("Content-Disposition","attachment;filename=" +java.net.URLEncoder.encode(xt_attachment_name,"UTF-8"));
				br = new BufferedInputStream(new FileInputStream(file));
				ut = response.getOutputStream();
				while((len=br.read(buffers))!=-1){
					ut.write(buffers, 0, len);
				}
				ut.flush();
				ut.close();
				br.close();
			}
		} catch (IOException e) {
			log.error("下载Bpmn文件异常，{}",e);
		}
	}

	/**
	 * 下载image文件
	 * @param lc_process_id
	 * @param response
	 */
	@ApiOperation(value="下载image文件", notes="下载image文件")
	@GetMapping(value="/downFileImg")
	@AuthUneedLogin
	public void downFileImg(String lc_process_id,HttpServletResponse response){
		LcProcess lcProcess = lcProcessService.getLcProcessById(lc_process_id);
		String versionPath = "";
		if(null != lcProcess.getVersion()){
			versionPath = lcProcess.getVersion()+"/";
		}
		response.setContentType("text/html;charset=utf-8");
		String attachPath = FileUtil.validOrCreateFile(getXtPathCache("ActivitiLc").get(0).getXt_path()+lcProcess.getLc_process_title()+"/"+versionPath+lcProcess.getLc_process_img_path());
		File file = new File(attachPath);
		String xt_attachment_name=lcProcess.getLc_process_img_path();
		try {
			if(!file.exists()){
				response.getWriter().println("<script>window.parent.toastrBoot(3,'文件不存在');</script>");
			}else{
				int len=0;
				byte []buffers = new byte[1024];
				BufferedInputStream br = null;
				OutputStream ut = null;
				response.reset();
				response.setContentType("application/x-msdownload");
				response.setHeader("Content-Disposition","attachment;filename=" +java.net.URLEncoder.encode(xt_attachment_name,"UTF-8"));
				br = new BufferedInputStream(new FileInputStream(file));
				ut = response.getOutputStream();
				while((len=br.read(buffers))!=-1){
					ut.write(buffers, 0, len);
				}
				ut.flush();
				ut.close();
				br.close();
			}
		} catch (IOException e) {
			log.error("下载image文件异常，{}",e);
		}
	}

	/**
	 * 根据taskId高亮图返回base64图片流
	 * @param taskId
	 * @param response
	 * @return
	 */
	@ApiOperation(value="根据taskId高亮图返回base64图片流", notes="根据taskId高亮图返回base64图片流")
	@GetMapping(value = "/view/base64/image/{taskId}")
	@AuthUneedLogin
	public BaseResult getImageBase64ByTaskId(@PathVariable("taskId")String taskId,HttpServletRequest request, HttpServletResponse response){
		BaseResult baseResult = new BaseResult();
		try {
			HistoricTaskInstance historicTaskInstance = activitiUtil.getTaskById(taskId);
			if(null == historicTaskInstance){
				log.info("未能查找到流程实例,任务编号：{}",taskId);
				baseResult.setSuccess(false);
				baseResult.setMessage("未能查找到流程实例");
				return baseResult;
			}
			String lcDeploymentHisId = activitiUtil.getDeploymentIdByTaskId(taskId);
			LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisByHisId(lcDeploymentHisId);
			if(null == lcDeploymentHis){
				log.info("未能查找到部署对象,流程部署编号：{}",lcDeploymentHisId);
				baseResult.setSuccess(false);
				baseResult.setMessage("未能查找到部署对象");
				return baseResult;
			}
			List<ActivityImplEntity> activityImplEntities = activitiUtil.getImageByInstance(historicTaskInstance.getProcessInstanceId());
			MxGraphToBase64 mxGraphToBase64 = new MxGraphToBase64();
			String imgXml = lcDeploymentHis.getImgxml();
			ImgXmlEntity imgXmlEntity = null;
			String base64 = null;

			if(!CollectionUtil.isEmpty(activityImplEntities)){
				List<String> idList = new ArrayList<>();
				for(ActivityImplEntity activityImplEntity: activityImplEntities){
					idList.add(activityImplEntity.getId());
				}
				imgXmlEntity = mxGraphToBase64.doImgXml(imgXml,idList);
				String xml = imgXmlEntity.getImgXml();
				BufferedImage bufferedImage = mxGraphToBase64.imageXmlBase64(request.getRequestURI(),xml,lcDeploymentHis.getW(),lcDeploymentHis.getH(),response);
				base64 = "data:image/png;base64," +Base64Util.toBase64(bufferedImage);
			}

			baseResult.setData(new ActivityImplImageEntity(base64,activityImplEntities));
		}catch (Exception e){
			log.error("根据taskId高亮图异常，{}",e);
		}
		return baseResult;
	}

	/**
	 * 根据processInstanceId高亮图返回base64图片流
	 * @param processInstanceId
	 * @param response
	 * @return
	 */
	@ApiOperation(value="根据processInstanceId高亮图返回base64图片流", notes="根据processInstanceId高亮图返回base64图片流")
	@GetMapping(value = "/view/base64/image/instance/{processInstanceId}")
	@AuthUneedLogin
	public BaseResult getImageBase64ByProcessInstanceId(@PathVariable("processInstanceId")String processInstanceId,HttpServletRequest request, HttpServletResponse response){
		BaseResult baseResult = new BaseResult();
		try {
			ProcessInstance processInstance = activitiUtil.getProcessInstanceById(processInstanceId);
			if(null == processInstance){
				log.info("未能查找到流程实例,实例编号：{}",processInstanceId);
				baseResult.setSuccess(false);
				baseResult.setMessage("未能查找到流程实例");
				return baseResult;
			}
			String lcDeploymentHisId = processInstance.getDeploymentId();
			LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisByHisId(lcDeploymentHisId);
			if(null == lcDeploymentHis){
				log.info("未能查找到部署对象,流程部署编号：{}",lcDeploymentHisId);
				baseResult.setSuccess(false);
				baseResult.setMessage("未能查找到部署对象");
				return baseResult;
			}
			List<ActivityImplEntity> activityImplEntities = activitiUtil.getImageByInstance(processInstanceId);
			MxGraphToBase64 mxGraphToBase64 = new MxGraphToBase64();
			String imgXml = lcDeploymentHis.getImgxml();
			ImgXmlEntity imgXmlEntity = null;
			String base64 = null;

			if(!CollectionUtil.isEmpty(activityImplEntities)){
				List<String> idList = new ArrayList<>();
				for(ActivityImplEntity activityImplEntity: activityImplEntities){
					idList.add(activityImplEntity.getId());
				}
				imgXmlEntity = mxGraphToBase64.doImgXml(imgXml,idList);
				String xml = imgXmlEntity.getImgXml();
				BufferedImage bufferedImage = mxGraphToBase64.imageXmlBase64(request.getRequestURI(),xml,lcDeploymentHis.getW(),lcDeploymentHis.getH(),response);
				base64 = "data:image/png;base64," +Base64Util.toBase64(bufferedImage);
			}

			baseResult.setData(new ActivityImplImageEntity(base64,activityImplEntities));
		}catch (Exception e){
			log.error("根据processInstanceId高亮图返回Base64图片流异常，{}",e);
		}
		return baseResult;
	}

	/**
	 * 根据流程模板id查找流程图base64
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value="根据流程模板id查找流程图base64", notes="根据流程模板id查找流程图base64")
	@GetMapping(value = "/image/base64/{id}")
	@AuthUneedLogin
	public BaseResult getImageBase64ByProcessId(@PathVariable("id")String id, HttpServletRequest request, HttpServletResponse response){
		BaseResult baseResult = new BaseResult();
		try {
			LcProcess lcProcess = lcProcessService.getLcProcessById(id);
			if(null == lcProcess){
				log.info("未能查找到流程对象，id：{}",id);
				baseResult.setSuccess(false);
				baseResult.setMessage("未能查找到流程对象");
				return baseResult;
			}
			MxGraphToBase64 mxGraphToBase64 = new MxGraphToBase64();
			BufferedImage bufferedImage = mxGraphToBase64.imageXmlBase64(request.getRequestURI(),lcProcess.getImgxml(),lcProcess.getW(),lcProcess.getH(),response);
			String base64 = "data:image/png;base64," + Base64Util.toBase64(bufferedImage);
			baseResult.setData(base64);
		}catch (Exception e){
			log.error("查找到流程对象异常，{}",e);
		}
		return baseResult;
	}

	/**
	 * 根据流程模板id及节点属性查找流程图base64
	 * @param id 流程模板id
	 * @param lcImgParamList
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value="根据流程模板id及节点属性查找流程图base64", notes="根据流程模板id及节点属性查找流程图base64")
	@PostMapping(value = "/image/attr/{id}")
	@AuthUneedLogin
	public BaseResult getActImgByProId(@PathVariable("id")String id, @RequestBody List<LcImgParam> lcImgParamList, HttpServletRequest request, HttpServletResponse response){
		BaseResult baseResult = new BaseResult();
		try {
			LcProcess lcProcess = lcProcessService.getLcProcessById(id);
			if(null == lcProcess){
				log.info("未能查找到流程对象，id：{}",id);
				baseResult.setSuccess(false);
				baseResult.setMessage("未能查找到流程对象");
				return baseResult;
			}

			List<String> idList = new ArrayList<>();
			if(!CollectionUtil.isEmpty(lcImgParamList)){
				for(LcImgParam lcImgParam: lcImgParamList){
					idList.add(lcImgParam.getActivityId());
				}
			}
			MxGraphToBase64 mxGraphToBase64 = new MxGraphToBase64();
			ImgXmlEntity imgXmlEntity = mxGraphToBase64.doImgXml(lcProcess.getImgxml(),idList);
			String xml = imgXmlEntity.getImgXml();
			BufferedImage bufferedImage = mxGraphToBase64.imageXmlBase64(request.getRequestURI(),xml,lcProcess.getW(),lcProcess.getH(),response);
			String base64 = "data:image/png;base64," +Base64Util.toBase64(bufferedImage);
			baseResult.setData(base64);
		}catch (Exception e){
			log.error("查找到流程对象异常，{}",e);
		}
		return baseResult;
	}

	/**
	 * 根据流程定义id查询发起表单数据
	 * @param processDefinitionId
	 * @return
	 */
	@ApiOperation(value="查询开始表单数据", notes="根据流程定义id查询发起表单数据")
	@GetMapping(value="/form/{processDefinitionId}")
	@AuthUneedLogin
	public BaseResult getStartFormData(@PathVariable("processDefinitionId") String processDefinitionId){
		return new BaseResult(activitiUtil.getStartFormData(processDefinitionId));
	}

	/**
	 * 根据流程定义id查询发起表单数据
	 * @param processDefinitionId
	 * @return
	 */
	@ApiOperation(value="查询开始表单数据", notes="根据流程定义id查询发起表单数据")
	@GetMapping(value="/formProperties/{processDefinitionId}")
	@AuthUneedLogin
	public BaseResult getStartFormProperties(@PathVariable("processDefinitionId") String processDefinitionId){
		return new BaseResult(activitiUtil.getStartFormProperties(processDefinitionId));
	}

	/**
	 * 根据流程模块Key查找最新流程版本
	 * @param moduleKey
	 * @return
	 */
	@ApiOperation(value="查询最新流程版本", notes="根据流程模块Key查找最新流程版本")
	@GetMapping(value="/business/{moduleKey}")
	@AuthUneedLogin
	public BaseResult<LcDeploymentHis> getHisProcess(@PathVariable("moduleKey") String moduleKey){
		LcProcess lcProcess = lcProcessService.getLcProcessByModuleKey(moduleKey);
		if(null == lcProcess){
			return new BaseResult("未能获取到模块key",false,null);
		}
		Map<String,Object> condition = new HashMap<>();
		condition.put("lc_process_id",lcProcess.getLc_process_id());
		LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisNewUnique(condition);
		return new BaseResult(lcDeploymentHis);
	}

	/**
	 * 根据模块Key查询发起人所在节点及发起人所在节点的下个节点（查询最新部署）
	 * @param moduleKey
	 * @return
	 */
	@ApiOperation(value="根据模块Key查询发起人所在节点及发起人所在节点的下个节点（查询最新部署）", notes="根据模块Key查询发起人所在节点及发起人所在节点的下个节点（查询最新部署）")
	@GetMapping(value="/attribute/{moduleKey}")
	@AuthUneedLogin
	public BaseResult<LcAttributeNEntity> getLcAttributeByModuleKey(@PathVariable("moduleKey") String moduleKey){
		BaseResult baseResult = lcTaskService.getLcAttributeByModuleKey(moduleKey);
		return baseResult;
	}

	/**
	 * 根据模块Key+版本号查询发起人所在节点及发起人所在节点的下个节点（查询最新部署）
	 * @param moduleKey
	 * @return
	 */
	@ApiOperation(value="根据模块Key+版本号查询发起人所在节点及发起人所在节点的下个节点（查询最新部署）", notes="根据模块Key+版本号查询发起人所在节点及发起人所在节点的下个节点（查询最新部署）")
	@GetMapping(value="/attribute/{moduleKey}/{version}")
	@AuthUneedLogin
	public BaseResult<LcAttributeNEntity> getLcAttributeByModuleKey(@PathVariable("moduleKey") String moduleKey,@PathVariable("version") String version){
		BaseResult baseResult = lcTaskService.getLcAttributeByModuleKey(moduleKey,version);
		return baseResult;
	}

	/**
	 * 根据模块Key发起流程实例
	 * @param lcHisParam
	 * @return
	 */
	@ApiOperation(value="根据模块Key发起流程实例", notes="根据模块Key发起流程实例")
	@PostMapping(value="/module/start")
	public BaseResult start(@RequestBody LcHisParam lcHisParam){
		return lcProcessService.start(lcHisParam);
	}

	/**
	 * 复制
	 * @param lcProcessParam
	 */
	@ApiOperation(value="复制", notes="复制")
	@PostMapping(value="/copy")
	public BaseResult copy(@RequestBody LcProcessParam lcProcessParam, HttpServletRequest request, HttpServletResponse response){
		return lcProcessService.copy(lcProcessParam,request,response);
	}

	/**
	 * 流程树
	 */
	@AuthUneedLogin
	@GetMapping(value="/tree")
	@ApiOperation(value="流程树", notes="流程树")
	public BaseResult getProcessList(LcProcess lcProcessPar){
		Map<String, Object> condition = new HashMap<String, Object>();
		if(!StringUtil.isEmpty(lcProcessPar.getLc_process_title())){
			condition.put("lc_process_title",lcProcessPar.getLc_process_title());
		}
		if(!StringUtil.isEmpty(lcProcessPar.getLc_product_id())){
			condition.put("lc_product_id",lcProcessPar.getLc_product_id());
		}
		if(!StringUtil.isEmpty(lcProcessPar.getLc_group_id())){
			condition.put("lc_group_id",lcProcessPar.getLc_group_id());
		}
		if(!StringUtil.isEmpty(lcProcessPar.getLc_process_title())){
			condition.put("lc_process_status",lcProcessPar.getLc_process_status());
		}
		List<BaseJSTreeEntity> list = new ArrayList<BaseJSTreeEntity>();
		List<LcProcess> lcProcesses = lcProcessService.getLcProcessListByCondition(condition);
		if(!CollectionUtil.isEmpty(lcProcesses)){
			for(LcProcess lcProcess: lcProcesses){
				BaseJSTreeEntity baseJSTreeEntity = new BaseJSTreeEntity();
				baseJSTreeEntity.setId(lcProcess.getLc_process_id());
				baseJSTreeEntity.setParent("0");
				baseJSTreeEntity.setText(lcProcess.getLc_process_title());
				baseJSTreeEntity.setSingleClickExpand(true);
				baseJSTreeEntity.setTempObject("P");
				list.add(baseJSTreeEntity);
			}
			List<LcDeploymentHis> lcDeploymentHisList = lcDeploymentHisService.getLcDeploymentHisListByCondition(condition);
			if(!CollectionUtil.isEmpty(lcDeploymentHisList)){
				for(LcDeploymentHis lcDeploymentHis: lcDeploymentHisList){
					BaseJSTreeEntity baseJSTreeEntity = new BaseJSTreeEntity();
					baseJSTreeEntity.setId(lcDeploymentHis.getId());
					baseJSTreeEntity.setParent(lcDeploymentHis.getLc_process_id());
					baseJSTreeEntity.setText("版本-"+lcDeploymentHis.getVersion());
					baseJSTreeEntity.setSingleClickExpand(true);
					baseJSTreeEntity.setTempObject("C");
//					baseJSTreeEntity.setIcon("fa fa-folder m--font-danger");
					baseJSTreeEntity.setIcon("fa fa-folder");
					list.add(baseJSTreeEntity);
				}
			}
		}
		BaseJSTreeEntity baseJSTreeEntity = new BaseJSTreeEntity();
		List<BaseJSTreeEntity> baseJSTreeEntitiesList = baseJSTreeEntity.buildTree(list,"0");
		/*根目录则显示方法
		BaseJSTree baseJSTree = new BaseJSTree("部门",baseJSTreeEntitiesList,new BaseJSTreeStateEntity(false,false,false));
		return new BaseResult(baseJSTree);
		*/
		return new BaseResult(JsonUtil.toFastJson(baseJSTreeEntitiesList));
	}

	/**
	 *
	 * @param nodeId
	 * @param mounts_id
	 * @return
	 */
	private LcNodeAttribute getLcNodeAttribute(String nodeId,String mounts_id){
		NodeAttributeParam nodeAttributeParam = new NodeAttributeParam();
		nodeAttributeParam.setNodeId(nodeId);
		nodeAttributeParam.setMounts_id(mounts_id);
		LcNodeAttribute lcNodeAttribute = lcNodeAttributeService.getLcNodeAttribute(nodeAttributeParam);
		return lcNodeAttribute;
	}

	/**
	 * 根据主流程id查询任务节点集合
	 * @param id
	 * @return
	 */
	@ApiOperation(value="查询任务节点集合", notes="根据主流程id查询任务节点集合")
	@PostMapping(value = "/userTasks/{id}")
	@AuthUneedLogin
	public BaseResult<List<LcNodeAttribute>> getTaskNodeList(@PathVariable("id")String id){
		List<LcNodeAttribute> lcNodeAttributes = new ArrayList<>();
		if(StringUtil.isEmpty(id)){
			return new BaseResult();
		}
		LcProcess lcProcess = lcProcessService.getLcProcessById(id);
		if(null != lcProcess){
			List<UserTask> userTasks = activitiUtil.getUserTaskElementsByBpmn(lcProcess.getLc_process_bpmn());
			for(UserTask userTask:userTasks){
				LcNodeAttribute lcNodeAttribute = new LcNodeAttribute();
				lcNodeAttribute.setNodeId(userTask.getId());
				lcNodeAttribute.setNodeName(userTask.getName());
				lcNodeAttribute.setLc_process_title(lcProcess.getLc_process_title());
				lcNodeAttribute.setLc_process_id(lcProcess.getLc_process_id());
				lcNodeAttribute.setMounts_id(lcProcess.getLc_process_id());
				lcNodeAttribute.setNodeType(ActivitiUtil.USER_TASK);
				LcNodeAttribute nodeAttribute = getLcNodeAttribute(userTask.getId(),lcProcess.getLc_process_id());
				if(null != nodeAttribute){
					lcNodeAttribute.setLc_node_attribute_id(nodeAttribute.getLc_node_attribute_id());
					lcNodeAttribute.setInitiator(nodeAttribute.getInitiator());
					lcNodeAttribute.setOrderNumber(nodeAttribute.getOrderNumber());
				}else{
					lcNodeAttribute.setOrderNumber(0);
				}
				if(activitiUtil.validateNodeIsMultiInstance(userTask)){
					lcNodeAttribute.setMutil("20");
				}else{
					lcNodeAttribute.setMutil("10");
				}
				lcNodeAttributes.add(lcNodeAttribute);
			}
			SortUtil.Sort(lcNodeAttributes, "orderNumber", "asc");
			List<EndEvent> endEvents = activitiUtil.getEventElementsByBpmn(lcProcess.getLc_process_bpmn());
			for(EndEvent endEvent : endEvents){
				LcNodeAttribute lcNodeAttribute = new LcNodeAttribute();
				lcNodeAttribute.setNodeId(endEvent.getId());
				lcNodeAttribute.setNodeName(endEvent.getName());
				lcNodeAttribute.setLc_process_title(lcProcess.getLc_process_title());
				lcNodeAttribute.setLc_process_id(lcProcess.getLc_process_id());
				lcNodeAttribute.setMounts_id(lcProcess.getLc_process_id());
				lcNodeAttribute.setMutil("10");
				lcNodeAttribute.setNodeType(ActivitiUtil.END);
				lcNodeAttributes.add(lcNodeAttribute);
			}
			return new BaseResult(lcNodeAttributes);
		}
		return new BaseResult(lcNodeAttributes);
	}

	/**
	 * 导出流程
	 * @param response
	 * @param lc_process_id
	 */
	@ApiOperation(value="导出流程", notes="导出流程")
	@AuthNeedLogin
	@GetMapping(value = "/export")
	public void exportProcess(HttpServletResponse response,String lc_process_id) {
		lcProcessService.exportProcess(response,lc_process_id);
	}

	/**
	 * 导入流程
	 * @param request
	 * @return
	 */
	@PostMapping(value = "/import")
	@AuthNeedLogin
	public BaseResult importProcess(HttpServletRequest request,HttpServletResponse response) {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;//转型为MultipartHttpRequest：
		MultiValueMap<String, MultipartFile> multipartFileMultiValueMap =  multipartRequest.getMultiFileMap();//获取所有文件
		MultipartFile multipartFile = multipartRequest.getFile("lcProcessFile");//获取指定文件即前端file框name标签
		return lcProcessService.importProcess(request,response,multipartFile);
	}

	/**
	 * 根据模块Key查询所有节点
	 * @param moduleKey
	 * @return
	 */
	@ApiOperation(value="根据模块Key查询所有节点", notes="根据模块Key查询所有节点")
	@GetMapping(value="/node/{moduleKey}")
	public BaseResult<List<LcNodeAttrEntity>> getNodesByModuleKey(@PathVariable("moduleKey") String moduleKey){
		if(StringUtil.isEmpty(moduleKey)){
			return new BaseResult();
		}
		String[] moduleKeyArray = moduleKey.split(",");
		List<LcNodeAttrEntity> lcNodeAttrEntities = new ArrayList<>();
		if(null != moduleKeyArray){
			for(String key : moduleKeyArray){
				LcNodeAttrEntity lcNodeAttrEntity = new LcNodeAttrEntity();
				lcNodeAttrEntity.setModuleKey(key);
				LcProcess lcProcess = lcProcessService.getLcProcessByModuleKey(key);
				if(null == lcProcess){
					log.info("根据模块Key条件，未能查到流程定义");
					lcNodeAttrEntities.add(lcNodeAttrEntity);
					continue;
				}
				Map<String,Object> condition = new HashMap<>();
				condition.put("lc_process_id",lcProcess.getLc_process_id());
				LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisMoreAttrNewUnique(key);
				if(null == lcDeploymentHis){
					log.info("未能获取到流程部署,模块Key：{}",moduleKey);
					lcNodeAttrEntities.add(lcNodeAttrEntity);
					continue;
				}
				if(StringUtil.isEmpty(lcDeploymentHis.getLc_process_bpmn())){
					log.info("未能获取到流程Bpm,模块Key：{}",moduleKey);
					lcNodeAttrEntities.add(lcNodeAttrEntity);
					continue;
				}

				Map<String,Object> condition_ = new HashMap<>();
				condition_.put("hid_",lcDeploymentHis.getId());
				List<LcNodeAttribute> lcNodeAttributes = lcNodeAttributeService.getLcNodeAttributeListByCondition(condition_);
				if(CollectionUtil.isEmpty(lcNodeAttributes)){
					log.info("未能获取到流程节点,模块Key：{}",moduleKey);
					lcNodeAttrEntities.add(lcNodeAttrEntity);
					continue;
				}
				List<LcNodeEntity> lcNodeEntities = new ArrayList<>();
				for(LcNodeAttribute lcNodeAttribute : lcNodeAttributes){
					UserTask userTask = activitiUtil.getUserTask(lcDeploymentHis.getLc_process_bpmn(),lcNodeAttribute.getNodeId());
					if(null != userTask){
						LcNodeEntity lcNodeEntity = new LcNodeEntity();
						lcNodeEntity.setActivityId(userTask.getId());
						lcNodeEntity.setName(userTask.getName());
						lcNodeEntity.setAttr(lcNodeAttribute.getAttr());
						lcNodeEntities.add(lcNodeEntity);
					}
				}
				lcNodeAttrEntity.setLcNodeEntities(lcNodeEntities);
				lcNodeAttrEntities.add(lcNodeAttrEntity);
			}
		}
		BaseResult baseResult = new BaseResult();
		baseResult.setData(lcNodeAttrEntities);
		return baseResult;
	}

	/**
	 * 根据流程实例id查询所有节点
	 * @param procInstId
	 * @return
	 */
	@ApiOperation(value="根据流程实例id查询所有节点", notes="根据流程实例id查询所有节点")
	@GetMapping(value="/node/list/{procInstId}")
	public BaseResult<List<LcNodeAttrEntity>> getNodesByProcInstId(@PathVariable("procInstId") String procInstId){
		if(StringUtil.isEmpty(procInstId)){
			return new BaseResult();
		}
		List<LcNodeAttrEntity> lcNodeAttrEntities = new ArrayList<>();
		LcNodeAttrEntity lcNodeAttrEntity = new LcNodeAttrEntity();

//		lcNodeAttrEntity.setModuleKey(key);
//		Map<String,Object> condition = new HashMap<>();
//		condition.put("lc_process_id",lcProcess.getLc_process_id());
		String deploymentId = activitiUtil.getDeploymentIdByProcInstId(procInstId);

		LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisByHisId(deploymentId);
		if(null == lcDeploymentHis){
			log.info("未能获取到流程部署,流程实例id：{}",procInstId);
			lcNodeAttrEntities.add(lcNodeAttrEntity);
		}
		if(StringUtil.isEmpty(lcDeploymentHis.getLc_process_bpmn())){
			log.info("未能获取到流程Bpm,流程实例id：{}",procInstId);
			lcNodeAttrEntities.add(lcNodeAttrEntity);
		}
		LcProcess lcProcess = lcProcessService.getLcProcessById(lcDeploymentHis.getLc_process_id());
		if(null != lcProcess){
			lcNodeAttrEntity.setModuleKey(lcProcess.getModuleKey());
		}

		Map<String,Object> condition_ = new HashMap<>();
		condition_.put("hid_",lcDeploymentHis.getId());
		List<LcNodeAttribute> lcNodeAttributes = lcNodeAttributeService.getLcNodeAttributeListByCondition(condition_);
		if(CollectionUtil.isEmpty(lcNodeAttributes)){
			log.info("未能获取到流程节点,流程实例id：{}",procInstId);
			lcNodeAttrEntities.add(lcNodeAttrEntity);
		}
		List<LcNodeEntity> lcNodeEntities = new ArrayList<>();
		for(LcNodeAttribute lcNodeAttribute : lcNodeAttributes){
			UserTask userTask = activitiUtil.getUserTask(lcDeploymentHis.getLc_process_bpmn(),lcNodeAttribute.getNodeId());
			if(null != userTask){
				LcNodeEntity lcNodeEntity = new LcNodeEntity();
				lcNodeEntity.setActivityId(userTask.getId());
				lcNodeEntity.setName(userTask.getName());
				lcNodeEntity.setAttr(lcNodeAttribute.getAttr());
				lcNodeEntities.add(lcNodeEntity);
			}
		}
		lcNodeAttrEntity.setLcNodeEntities(lcNodeEntities);
		lcNodeAttrEntities.add(lcNodeAttrEntity);
		BaseResult baseResult = new BaseResult();
		baseResult.setData(lcNodeAttrEntities);
		return baseResult;
	}

	/**
	 * 根据流程实例id查询所有节点 批量
	 * @param procInstId
	 * @return
	 */
	@ApiOperation(value="根据流程实例id查询所有节点 批量", notes="根据流程实例id查询所有节点 批量")
	@GetMapping(value="/node/list/batch/{procInstId}")
	public BaseResult<List<LcNodeAttrEntity>> getNodesBatchByProcInstId(@PathVariable("procInstId") String procInstId){
		if(StringUtil.isEmpty(procInstId)){
			return new BaseResult();
		}
		String[] procInstIdArray = procInstId.split(",");

		List<LcNodeAttrEntity> lcNodeAttrEntities = new ArrayList<>();
		if(null != procInstIdArray){
			for(String key : procInstIdArray){
				LcNodeAttrEntity lcNodeAttrEntity = new LcNodeAttrEntity();

				String deploymentId = activitiUtil.getDeploymentIdByProcInstId(key);

				LcDeploymentHis lcDeploymentHis = lcDeploymentHisService.getLcDeploymentHisByHisId(deploymentId);
				if(null == lcDeploymentHis){
					log.info("未能获取到流程部署,流程实例id：{}",key);
					lcNodeAttrEntities.add(lcNodeAttrEntity);
				}
				if(StringUtil.isEmpty(lcDeploymentHis.getLc_process_bpmn())) {
					log.info("未能获取到流程Bpm,流程实例id：{}", key);
				}
				LcProcess lcProcess = lcProcessService.getLcProcessById(lcDeploymentHis.getLc_process_id());
				if(null != lcProcess){
					lcNodeAttrEntity.setModuleKey(lcProcess.getModuleKey());
				}
				Map<String,Object> condition_ = new HashMap<>();
				condition_.put("hid_",lcDeploymentHis.getId());
				List<LcNodeAttribute> lcNodeAttributes = lcNodeAttributeService.getLcNodeAttributeListByCondition(condition_);
				if(CollectionUtil.isEmpty(lcNodeAttributes)){
					log.info("未能获取到流程节点,流程实例id：{}",key);
					lcNodeAttrEntities.add(lcNodeAttrEntity);
					continue;
				}
				List<LcNodeEntity> lcNodeEntities = new ArrayList<>();
				for(LcNodeAttribute lcNodeAttribute : lcNodeAttributes){
					UserTask userTask = activitiUtil.getUserTask(lcDeploymentHis.getLc_process_bpmn(),lcNodeAttribute.getNodeId());
					if(null != userTask){
						LcNodeEntity lcNodeEntity = new LcNodeEntity();
						lcNodeEntity.setActivityId(userTask.getId());
						lcNodeEntity.setName(userTask.getName());
						lcNodeEntity.setAttr(lcNodeAttribute.getAttr());
						lcNodeEntities.add(lcNodeEntity);
					}
				}
				lcNodeAttrEntity.setLcNodeEntities(lcNodeEntities);
				lcNodeAttrEntities.add(lcNodeAttrEntity);
			}
		}
		BaseResult baseResult = new BaseResult();
		baseResult.setData(lcNodeAttrEntities);
		return baseResult;
	}
}
