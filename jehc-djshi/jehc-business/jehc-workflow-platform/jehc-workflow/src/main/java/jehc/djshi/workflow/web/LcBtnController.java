package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcBtn;
import jehc.djshi.workflow.service.LcBtnService;

/**
* @Desc 按钮 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:31:14
*/
@Api(value = "按钮", description = "按钮", tags = "按钮")
@RestController
@RequestMapping("/lcBtn")
public class LcBtnController extends BaseAction {
	@Autowired
	private LcBtnService lcBtnService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcBtn>> getLcBtnListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcBtn> lcBtnList = lcBtnService.getLcBtnListByCondition(condition);
		for(LcBtn lcBtn:lcBtnList){
			if(!StringUtil.isEmpty(lcBtn.getCreate_id())){
				OauthAccountEntity createBy = getAccount(lcBtn.getCreate_id());
				if(null != createBy){
					lcBtn.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(lcBtn.getUpdate_id())){
				OauthAccountEntity modifieBy = getAccount(lcBtn.getUpdate_id());
				if(null != modifieBy){
					lcBtn.setModifiedBy(modifieBy.getName());
				}
			}
		}
		PageInfo<LcBtn> page = new PageInfo<LcBtn>(lcBtnList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param btn_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{btn_id}")
	public BaseResult<LcBtn> getLcBtnById(@PathVariable("btn_id")String btn_id){
		LcBtn lcBtn = lcBtnService.getLcBtnById(btn_id);
		if(!StringUtil.isEmpty(lcBtn.getCreate_id())){
			OauthAccountEntity createBy = getAccount(lcBtn.getCreate_id());
			if(null != createBy){
				lcBtn.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(lcBtn.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(lcBtn.getUpdate_id());
			if(null != modifiedBy){
				lcBtn.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(lcBtn);
	}
	/**
	* 添加
	* @param lcBtn 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcBtn(@RequestBody LcBtn lcBtn){
		int i = 0;
		if(null != lcBtn){
			lcBtn.setCreate_id(getXtUid());
			lcBtn.setCreate_time(getDate());
			lcBtn.setBtn_id(toUUID());
			i=lcBtnService.addLcBtn(lcBtn);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcBtn 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcBtn(@RequestBody LcBtn lcBtn){
		int i = 0;
		if(null != lcBtn){
			lcBtn.setUpdate_id(getXtUid());
			lcBtn.setUpdate_time(getDate());
			i=lcBtnService.updateLcBtn(lcBtn);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param btn_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcBtn(String btn_id){
		int i = 0;
		if(!StringUtil.isEmpty(btn_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("btn_id",btn_id.split(","));
			i=lcBtnService.delLcBtn(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询按钮
	 * @return
	 */
	@ApiOperation(value="查询全部按钮", notes="查询全部按钮")
	@NeedLoginUnAuth
	@GetMapping(value="/collections")
	public BaseResult<List<LcBtn>> collections(){
		Map<String,Object> condition = new HashMap<>();
		List<LcBtn> lcBtnList = lcBtnService.getLcBtnListByCondition(condition);
		return new BaseResult(lcBtnList);
	}
}
