package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.model.LcApply;
import jehc.djshi.workflow.model.LcApproval;
import jehc.djshi.workflow.service.LcApplyService;
import jehc.djshi.workflow.service.LcApprovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 工作流批审
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "工作流批审", description = "工作流批审",tags = "工作流批审")
@RestController
@RequestMapping("/lcApproval")
public class LcApprovalController extends BaseAction {
	@Autowired
	private LcApprovalService lcApprovalService;
	@Autowired
	private LcApplyService lcApplyService;

	/**
	 *  查询并分页
	 * @param baseSearch
	 * @param instanceId
	 * @return
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@PostMapping(value="/list")
	@NeedLoginUnAuth
	public BasePage<List<LcApproval>> getLcApprovalListByCondition(@RequestBody BaseSearch baseSearch, String instanceId){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcApproval> lcApprovalList = lcApprovalService.getLcApprovalListByCondition(condition);
		for(LcApproval lcApproval:lcApprovalList){
			if(!StringUtil.isEmpty(lcApproval.getUserId())){
				OauthAccountEntity createBy = getAccount(lcApproval.getUserId());
				if(null != createBy){
					lcApproval.setCreateBy(createBy.getName());
				}
			}
		}
		PageInfo<LcApproval> page = new PageInfo<LcApproval>(lcApprovalList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	* 查询单条记录
	* @param lcApprovalId
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@GetMapping(value="/get/{lcApprovalId}")
	public BaseResult<LcApproval> getLcApprovalById(@PathVariable("lcApprovalId")String lcApprovalId){
		LcApproval lcApproval = lcApprovalService.getLcApprovalById(lcApprovalId);
		if(!StringUtil.isEmpty(lcApproval.getUserId())){
			OauthAccountEntity createBy = getAccount(lcApproval.getUserId());
			if(null != createBy){
				lcApproval.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(lcApproval.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(lcApproval.getUpdate_id());
			if(null != modifiedBy){
				lcApproval.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(lcApproval);
	}

//	/**
//	* 添加
//	* @param lcApproval
//	*/
//	@ApiOperation(value="添加", notes="添加")
//	@PostMapping(value="/add")
//	public BaseResult addLcApproval(@RequestBody LcApproval lcApproval){
//		int i = 0;
//		if(null != lcApproval){
//			lcApproval.setLcApprovalId(toUUID());
//			i=lcApprovalService.addLcApproval(lcApproval);
//		}
//		if(i>0){
//			return outAudStr(true);
//		}else{
//			return outAudStr(false);
//		}
//	}

	/**
	* 删除
	* @param lcApprovalId
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcApproval(String lcApprovalId){
		int i = 0;
		if(!StringUtil.isEmpty(lcApprovalId)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_approval_id",lcApprovalId.split(","));
			i=lcApprovalService.delLcApproval(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 根据业务Key批量删除
	 * @param businessKey
	 */
	@ApiOperation(value="根据业务Key批量删除", notes="根据业务Key批量删除")
	@DeleteMapping(value="/delLcApprovalByBusinessKey")
	public BaseResult delLcApprovalByBusinessKey(String businessKey){
		int i = 0;
		if(!StringUtil.isEmpty(businessKey)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("business_key",businessKey.split(","));
			i=lcApprovalService.delLcApprovalByBusinessKey(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
