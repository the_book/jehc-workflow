package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.ActRuIdentitylink;
import jehc.djshi.workflow.service.ActRuIdentitylinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

/**
 * @Desc 运行时流程人员
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "运行时流程人员",description = "运行时流程人员", tags = "运行时流程人员")
@RestController
@RequestMapping("/actRuIdentitylink")
public class ActRuIdentitylinkController extends BaseAction {

    @Autowired
    private ActRuIdentitylinkService actRuIdentitylinkService;

    /**
     * 查询并分页
     * @param baseSearch
     */
    @ApiOperation(value="查询并分页", notes="查询并分页")
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    public BasePage<List<ActRuIdentitylink>> getActRuIdentitylinkListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<ActRuIdentitylink> actRuIdentitylinkList = actRuIdentitylinkService.getActRuIdentitylinkListByCondition(condition);
        PageInfo<ActRuIdentitylink> page = new PageInfo<ActRuIdentitylink>(actRuIdentitylinkList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单条记录
     * @param id
     */
    @ApiOperation(value="查询单条记录", notes="查询单条记录")
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    public BaseResult<ActRuIdentitylink> getActRuIdentitylinkById(@PathVariable("id")String id){
        ActRuIdentitylink actRuIdentitylink = actRuIdentitylinkService.getActRuIdentitylinkById(id);
        return outDataStr(actRuIdentitylink);
    }

    /**
     * 删除运行时流程人员
     * @param id
     * @param procInstId
     * @return
     */
    @ApiOperation(value="删除运行时流程人员", notes="删除运行时流程人员")
    @DeleteMapping(value="/delete")
    public BaseResult delActRuIdentitylink(String id,String procInstId){
        int i = actRuIdentitylinkService.delActRuIdentitylink(id);
        if(i>0){
            return BaseResult.success();
        }else {
            return BaseResult.fail();
        }
    }
}
