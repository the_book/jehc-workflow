package jehc.djshi.workflow.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.dao.LcSignDao;
import jehc.djshi.workflow.dao.LcSignRecordDao;
import jehc.djshi.workflow.model.LcSign;
import jehc.djshi.workflow.service.LcSignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 会签动态扩展业务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Service
public class LcSignServiceImpl extends BaseService implements LcSignService {
    @Autowired
    LcSignDao lcSignDao;

    @Autowired
    LcSignRecordDao lcSignRecordDao;
    /**
     *
     * @param condition
     * @return
     */
    public List<LcSign> getLcSignListByCondition(Map<String,Object> condition){
        return lcSignDao.getLcSignListByCondition(condition);
    }

    /**
     * 创建会签
     * @param lcSign
     * @return
     */
    public int addLcSign(LcSign lcSign){
        try {
            if(null == lcSign){
                throw new ExceptionUtil("创建会签对象失败，原因会签对象不存在");
            }
            if(StringUtil.isEmpty(lcSign.getLc_sign_id())){
                lcSign.setLc_sign_id(toUUID());
                lcSign.setCreate_id(getXtUid());
                lcSign.setCreate_time(getDate());
            }
            int i = lcSignDao.addLcSign(lcSign);
//            if(!CollectionUtil.isEmpty(lcSign.getLcSignRecords())){
//                List<LcSignRecord> lcSignRecordList = lcSign.getLcSignRecords();
//                for(LcSignRecord lcSignRecord: lcSignRecordList){
//                    lcSignRecord.setLc_sign_id(lcSign.getLc_sign_id());
//                    lcSignRecord.setCreate_time(getDate());
//                    lcSignRecord.setCreate_id(getXtUid());
//                    lcSignRecord.setBatch(lcSign.getBatch());
//                    lcSignRecord.setLc_sign_record_id(toUUID());
//                }
//                i = lcSignRecordDao.addBatchLcSignRecord(lcSignRecordList);
//            }
            return i;
        }catch (Exception e){
            throw new ExceptionUtil("创建会签对象失败,{}",e);
        }
    }

    /**
     *
     * @param lcSigns
     * @return
     */
    public int addBatchLcSign(List<LcSign> lcSigns){
        return lcSignDao.addBatchLcSign(lcSigns);
    }


    /**
     *
     * @param condition
     * @return
     */
    public int delLcSign(Map<String,Object> condition){
        return lcSignDao.delLcSign(condition);
    }


    /**
     *
     * @param condition
     * @return
     */
    public Integer getMaxBatch(Map<String,Object> condition){
        return lcSignDao.getMaxBatch(condition);
    }

    /**
     * 更新会签
     * @param lcSign
     * @return
     */
    public int updateLcSign(LcSign lcSign){
        return lcSignDao.updateLcSign(lcSign);
    }
}
