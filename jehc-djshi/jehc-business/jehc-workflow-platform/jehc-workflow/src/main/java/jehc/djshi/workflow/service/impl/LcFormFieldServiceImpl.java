package jehc.djshi.workflow.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.workflow.service.LcFormFieldService;
import jehc.djshi.workflow.dao.LcFormFieldDao;
import jehc.djshi.workflow.model.LcFormField;

/**
* @Desc 工作流表单字段 
* @Author 邓纯杰
* @CreateTime 2022-04-04 19:17:12
*/
@Service("lcFormFieldService")
public class LcFormFieldServiceImpl extends BaseService implements LcFormFieldService{
	@Autowired
	private LcFormFieldDao lcFormFieldDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LcFormField> getLcFormFieldListByCondition(Map<String,Object> condition){
		try{
			return lcFormFieldDao.getLcFormFieldListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param lc_form_field_id 
	* @return
	*/
	public LcFormField getLcFormFieldById(String lc_form_field_id){
		try{
			LcFormField lcFormField = lcFormFieldDao.getLcFormFieldById(lc_form_field_id);
			return lcFormField;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param lcFormField 
	* @return
	*/
	public int addLcFormField(LcFormField lcFormField){
		int i = 0;
		try {
			i = lcFormFieldDao.addLcFormField(lcFormField);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param lcFormField 
	* @return
	*/
	public int updateLcFormField(LcFormField lcFormField){
		int i = 0;
		try {
			i = lcFormFieldDao.updateLcFormField(lcFormField);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param lcFormField 
	* @return
	*/
	public int updateLcFormFieldBySelective(LcFormField lcFormField){
		int i = 0;
		try {
			i = lcFormFieldDao.updateLcFormFieldBySelective(lcFormField);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLcFormField(Map<String,Object> condition){
		int i = 0;
		try {
			i = lcFormFieldDao.delLcFormField(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
