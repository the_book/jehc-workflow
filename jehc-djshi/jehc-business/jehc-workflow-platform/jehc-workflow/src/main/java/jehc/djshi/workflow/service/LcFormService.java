package jehc.djshi.workflow.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcForm;

/**
* @Desc 工作流表单 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:33:49
*/
public interface LcFormService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcForm> getLcFormListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param lc_form_id 
	* @return
	*/
	LcForm getLcFormById(String lc_form_id);
	/**
	* 添加
	* @param lcForm 
	* @return
	*/
	int addLcForm(LcForm lcForm);
	/**
	* 修改
	* @param lcForm 
	* @return
	*/
	int updateLcForm(LcForm lcForm);
	/**
	* 修改（根据动态条件）
	* @param lcForm 
	* @return
	*/
	int updateLcFormBySelective(LcForm lcForm);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcForm(Map<String, Object> condition);
}
