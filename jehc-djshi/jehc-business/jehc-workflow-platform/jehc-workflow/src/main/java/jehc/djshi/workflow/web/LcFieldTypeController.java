package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcFieldType;
import jehc.djshi.workflow.service.LcFieldTypeService;

/**
* @Desc 字段类型 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:42:35
*/
@Api(value = "字段类型", description = "字段类型",tags = "字段类型")
@RestController
@RequestMapping("/lcFieldType")
public class LcFieldTypeController extends BaseAction{
	@Autowired
	private LcFieldTypeService lcFieldTypeService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcFieldType>> getLcFieldTypeListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcFieldType> lcFieldTypeList = lcFieldTypeService.getLcFieldTypeListByCondition(condition);
		for(LcFieldType lcFieldType:lcFieldTypeList){
			if(!StringUtil.isEmpty(lcFieldType.getCreate_id())){
				OauthAccountEntity createBy = getAccount(lcFieldType.getCreate_id());
				if(null != createBy){
					lcFieldType.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(lcFieldType.getUpdate_id())){
				OauthAccountEntity modifieBy = getAccount(lcFieldType.getUpdate_id());
				if(null != modifieBy){
					lcFieldType.setModifiedBy(modifieBy.getName());
				}
			}
		}
		PageInfo<LcFieldType> page = new PageInfo<LcFieldType>(lcFieldTypeList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param type_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{type_id}")
	public BaseResult<LcFieldType> getLcFieldTypeById(@PathVariable("type_id")String type_id){
		LcFieldType lcFieldType = lcFieldTypeService.getLcFieldTypeById(type_id);
		OauthAccountEntity createBy = getAccount(lcFieldType.getCreate_id());
		if(null != createBy){
			lcFieldType.setCreateBy(createBy.getName());
		}
		if(!StringUtil.isEmpty(lcFieldType.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(lcFieldType.getUpdate_id());
			if(null != modifiedBy){
				lcFieldType.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(lcFieldType);
	}
	/**
	* 添加
	* @param lcFieldType 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcFieldType(@RequestBody LcFieldType lcFieldType){
		int i = 0;
		if(null != lcFieldType){
			lcFieldType.setCreate_id(getXtUid());
			lcFieldType.setCreate_time(getDate());
			lcFieldType.setType_id(toUUID());
			i=lcFieldTypeService.addLcFieldType(lcFieldType);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcFieldType 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcFieldType(@RequestBody LcFieldType lcFieldType){
		int i = 0;
		if(null != lcFieldType){
			lcFieldType.setUpdate_id(getXtUid());
			lcFieldType.setUpdate_time(getDate());
			i=lcFieldTypeService.updateLcFieldType(lcFieldType);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param type_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcFieldType(String type_id){
		int i = 0;
		if(!StringUtil.isEmpty(type_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("type_id",type_id.split(","));
			i=lcFieldTypeService.delLcFieldType(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询列表
	 * @return
	 */
	@ApiOperation(value="查询列表", notes="查询列表")
	@NeedLoginUnAuth
	@GetMapping(value="/find")
	public BaseResult<List<LcFieldType>> getFieldTypeList(){
		Map<String, Object> condition = new HashMap<>();
		List<LcFieldType> lcFieldTypeList = lcFieldTypeService.getLcFieldTypeListByCondition(condition);
		return new BaseResult(lcFieldTypeList);
	}
}
