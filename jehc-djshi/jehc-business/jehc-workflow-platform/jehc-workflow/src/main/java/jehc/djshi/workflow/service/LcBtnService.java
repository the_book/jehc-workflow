package jehc.djshi.workflow.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.workflow.model.LcBtn;

/**
* @Desc 按钮 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:31:14
*/
public interface LcBtnService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcBtn> getLcBtnListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param btn_id 
	* @return
	*/
	LcBtn getLcBtnById(String btn_id);
	/**
	* 添加
	* @param lcBtn 
	* @return
	*/
	int addLcBtn(LcBtn lcBtn);
	/**
	* 修改
	* @param lcBtn 
	* @return
	*/
	int updateLcBtn(LcBtn lcBtn);
	/**
	* 修改（根据动态条件）
	* @param lcBtn 
	* @return
	*/
	int updateLcBtnBySelective(LcBtn lcBtn);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLcBtn(Map<String, Object> condition);
}
