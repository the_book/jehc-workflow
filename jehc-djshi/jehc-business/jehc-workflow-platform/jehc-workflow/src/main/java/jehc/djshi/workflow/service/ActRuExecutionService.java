package jehc.djshi.workflow.service;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.workflow.model.ActRuExecution;

import java.util.List;
import java.util.Map;

/**
 * @Desc 运行实例
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface ActRuExecutionService {
    /**
     * 查询运行实例集合
     * @param condition
     * @return
     */
    List<ActRuExecution> getActRuExecutionListByCondition(Map<String,Object> condition);

    /**
     * 查询单个运行实例
     * @param id
     * @return
     */
    ActRuExecution getActRuExecutionById(String id);

    /**
     * 删除运行实例
     * @param id
     * @return
     */
    BaseResult delActRuExecution(String id, String procInstId);
}
