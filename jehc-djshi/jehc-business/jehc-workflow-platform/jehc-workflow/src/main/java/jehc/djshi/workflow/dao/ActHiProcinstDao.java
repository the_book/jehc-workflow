package jehc.djshi.workflow.dao;

/**
 * @Desc 历史实例
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface ActHiProcinstDao {
    /**
     * 根据流程定义统计流程实例运行平均时间
     * @param proc_def_id
     * @return
     */
    String getActHiProcinstAvgTime(String proc_def_id);
}
