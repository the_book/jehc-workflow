package jehc.djshi.workflow.util.message.vo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @Desc 常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Data
public class Constant {

    /**通知类型：邮件（mail），短信（sms）站内（notice）,企业微信（enterpriseWechat），钉钉（nailNail），微信公众号（officialWechat）**/
    public static final String MAIL="mail";//邮件

    public static final String SMS="sms";//短信

    public static final String NOTICE="notice";//站内

    public static final String ENTERPRISEWECHAT="enterpriseWechat";//企业微信

    public static final String NAILNAIL="nailNail";//钉钉

    public static final String OFFICIALWECHAT="officialWechat";//微信公众号

    public static final String MAIL_HTML = "您的任务快超时了，请赶快处理！";//邮件提醒HTML结构体

    public static final String MAIL_SUBJECT = "您有一条超时任务";//邮件提醒HTML结构体

    public static final String NOTICE_HTML = "您的任务快超时了，请赶快处理！";//站内提醒HTML结构体

    public static final String NOTICE_SUBJECT = "您有一条超时任务";//站内提醒HTML结构体
}
