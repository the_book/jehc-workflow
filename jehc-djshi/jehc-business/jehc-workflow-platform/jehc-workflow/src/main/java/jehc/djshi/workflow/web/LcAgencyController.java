package jehc.djshi.workflow.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.util.ActivitiUtil;
import jehc.djshi.workflow.vo.LcTask;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * @Desc 代办事项
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "代办事项",description = "代办事项", tags = "代办事项")
@RestController
@RequestMapping("/lcAgency")
public class LcAgencyController extends BaseAction {
	@Autowired
	ActivitiUtil activitiUtil;

	/**
	 * 查找个人任务
	 * @param baseSearch
	 * @return
	 */
	@ApiOperation(value="查找个人任务", notes="查找个人任务")
	@PostMapping(value="/assigneeTask/list")
	@NeedLoginUnAuth
	public BaseResult<List<LcTask>> getLcApprovalListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("assignee", getXtUid());
		if(null != baseSearch){
			condition.put("start",baseSearch.getStart());
			condition.put("pageSize",baseSearch.getPageSize());
		}
		Map<String, Object> map = activitiUtil.getAssigneeTaskPageList(condition);
		List<Task> list = (List<Task>)map.get("TaskList");
		int total = new Integer(map.get("TaskCount").toString());
		List<LcTask> lcTasks = new ArrayList<>();
		for(int i = 0; i < list.size(); i++){
			Task task = list.get(i);
			LcTask lcTask = new LcTask();
			lcTask.setCategory(task.getCategory());
			lcTask.setAssignee(task.getAssignee());
			lcTask.setDescription(task.getDescription());
			lcTask.setExecutionId(task.getExecutionId());
			lcTask.setFormKey(task.getFormKey());
			lcTask.setTaskId(task.getId());
			lcTask.setName(task.getName());
			lcTask.setOwner(task.getOwner());
			lcTask.setParentTaskId(task.getParentTaskId());
			lcTask.setPriority(task.getPriority());
			lcTask.setProcessDefinitionId(task.getProcessDefinitionId());
			lcTask.setProcessInstanceId(task.getProcessInstanceId());
			lcTask.setTaskDefinitionKey(task.getTaskDefinitionKey());
			lcTask.setTenantId(task.getTenantId());
			lcTask.setCreateTime(task.getCreateTime());
			lcTask.setDelegationState(null != task.getDelegationState()?task.getDelegationState().name():"");
			lcTask.setDueDate(task.getDueDate());
			lcTask.setProcessVariables(task.getProcessVariables());
			lcTasks.add(lcTask);
		}
		PageInfo<LcTask> page = new PageInfo<LcTask>(lcTasks);
		page.setTotal(total);
		return outPageBootStr(page,baseSearch);
	}
	
	/**
	* 查找候选人任务
	* @param baseSearch
	*/
	@ApiOperation(value="查找候选人任务", notes="查找候选人任务")
	@PostMapping(value="/candidateTask/list")
	@NeedLoginUnAuth
	public BaseResult<List<LcTask>> getCandidateTaskPageList(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("candidateUser", getXtUid());
		if(null != baseSearch){
			condition.put("start",baseSearch.getStart());
			condition.put("pageSize",baseSearch.getPageSize());
		}
		Map<String, Object> map = activitiUtil.getCandidateTaskPageList(condition);
		List<Task> list = (List<Task>)map.get("TaskList");
		int total = new Integer(map.get("TaskCount").toString());
		List<LcTask> lcTasks = new ArrayList<>();
		for(int i = 0; i < list.size(); i++){
			Task task = list.get(i);
			LcTask lcTask = new LcTask();
			lcTask.setCategory(task.getCategory());
			lcTask.setAssignee(task.getAssignee());
			lcTask.setDescription(task.getDescription());
			lcTask.setExecutionId(task.getExecutionId());
			lcTask.setFormKey(task.getFormKey());
			lcTask.setTaskId(task.getId());
			lcTask.setName(task.getName());
			lcTask.setOwner(task.getOwner());
			lcTask.setParentTaskId(task.getParentTaskId());
			lcTask.setPriority(task.getPriority());
			lcTask.setProcessDefinitionId(task.getProcessDefinitionId());
			lcTask.setProcessInstanceId(task.getProcessInstanceId());
			lcTask.setTaskDefinitionKey(task.getTaskDefinitionKey());
			lcTask.setTenantId(task.getTenantId());
			lcTask.setCreateTime(task.getCreateTime());
			lcTask.setDelegationState(null != task.getDelegationState()?task.getDelegationState().name():"");
			lcTask.setDueDate(task.getDueDate());
			lcTask.setProcessVariables(task.getProcessVariables());
			lcTasks.add(lcTask);
		}
		PageInfo<LcTask> page = new PageInfo<LcTask>(lcTasks);
		page.setTotal(total);
		return outPageBootStr(page,baseSearch);
	}
	
	/**
	* 查找处理组任务
	* @param baseSearch
	*/
	@ApiOperation(value="查找处理组任务", notes="查找处理组任务")
	@PostMapping(value="/candidateGroupTask/list")
	@NeedLoginUnAuth
	public BaseResult<List<LcTask>> getCandidateGroupTaskPageList(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("candidateGroup", getUdId());
		if(null != baseSearch){
			condition.put("start",baseSearch.getStart());
			condition.put("pageSize",baseSearch.getPageSize());
		}
		Map<String, Object> map = activitiUtil.getCandidateGroupTaskPageList(condition);
		List<Task> list = (List<Task>)map.get("TaskList");
		int total = new Integer(map.get("TaskCount").toString());
		List<LcTask> lcTasks = new ArrayList<>();
		for(int i = 0; i < list.size(); i++){
			Task task = list.get(i);
			LcTask lcTask = new LcTask();
			lcTask.setCategory(task.getCategory());
			lcTask.setAssignee(task.getAssignee());
			lcTask.setDescription(task.getDescription());
			lcTask.setExecutionId(task.getExecutionId());
			lcTask.setFormKey(task.getFormKey());
			lcTask.setTaskId(task.getId());
			lcTask.setName(task.getName());
			lcTask.setOwner(task.getOwner());
			lcTask.setParentTaskId(task.getParentTaskId());
			lcTask.setPriority(task.getPriority());
			lcTask.setProcessDefinitionId(task.getProcessDefinitionId());
			lcTask.setProcessInstanceId(task.getProcessInstanceId());
			lcTask.setTaskDefinitionKey(task.getTaskDefinitionKey());
			lcTask.setTenantId(task.getTenantId());
			lcTask.setCreateTime(task.getCreateTime());
			lcTask.setDelegationState(null != task.getDelegationState()?task.getDelegationState().name():"");
			lcTask.setDueDate(task.getDueDate());
			lcTask.setProcessVariables(task.getProcessVariables());
			lcTasks.add(lcTask);
		}
		PageInfo<LcTask> page = new PageInfo<LcTask>(lcTasks);
		page.setTotal(total);
		return outPageBootStr(page,baseSearch);
	}
}
