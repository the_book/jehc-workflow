package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.ActRuTask;

import java.util.List;
import java.util.Map;

/**
 * @Desc 运行时任务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface ActRuTaskDao {
    /**
     * 查询运行任务集合
     * @param condition
     * @return
     */
    List<ActRuTask> getActRuTaskListByCondition(Map<String,Object> condition);

    /**
     * 查询单个运行任务
     * @param id
     * @return
     */
    ActRuTask getActRuTaskById(String id);

    /**
     * 删除运行任务
     * @param id
     * @return
     */
    int delActRuTask(String id);

    /**
     * 根据运行实例删除运行任务
     * @param executionId
     * @return
     */
    int delActRuTaskByExecutionId(String executionId);

    /**
     *
     * @param actRuTask
     * @return
     */
    int addActRuTask(ActRuTask actRuTask);
}
