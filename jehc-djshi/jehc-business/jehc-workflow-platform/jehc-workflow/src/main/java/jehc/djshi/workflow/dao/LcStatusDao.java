package jehc.djshi.workflow.dao;

import jehc.djshi.workflow.model.LcStatus;

import java.util.List;
import java.util.Map;


/**
 * @Desc 流程状态
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LcStatusDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LcStatus> getLcStatusListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param lc_status_id
	* @return
	*/
	LcStatus getLcStatusById(String lc_status_id);
	/**
	* 添加
	* @param lcStatus
	* @return
	*/
	int addLcStatus(LcStatus lcStatus);
	/**
	* 修改
	* @param lcStatus
	* @return
	*/
	int updateLcStatus(LcStatus lcStatus);
	/**
	* 删除
	* @param condition
	* @return
	*/
	int delLcStatus(Map<String, Object> condition);
}
