package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcNodeBtn;
import jehc.djshi.workflow.service.LcNodeBtnService;

/**
* @Desc 节点按钮（关系表） 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:37:18
*/
@Api(value = "节点按钮", description = "节点按钮",tags = "节点按钮")
@RestController
@RequestMapping("/lcNodeBtn")
public class LcNodeBtnController extends BaseAction{
	@Autowired
	private LcNodeBtnService lcNodeBtnService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcNodeBtn>> getLcNodeBtnListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcNodeBtn> lcNodeBtnList = lcNodeBtnService.getLcNodeBtnListByCondition(condition);
		PageInfo<LcNodeBtn> page = new PageInfo<LcNodeBtn>(lcNodeBtnList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param node_btn_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{node_btn_id}")
	public BaseResult<LcNodeBtn> getLcNodeBtnById(@PathVariable("node_btn_id")String node_btn_id){
		LcNodeBtn lcNodeBtn = lcNodeBtnService.getLcNodeBtnById(node_btn_id);
		return outDataStr(lcNodeBtn);
	}
	/**
	* 添加
	* @param lcNodeBtn 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcNodeBtn(@RequestBody LcNodeBtn lcNodeBtn){
		int i = 0;
		if(null != lcNodeBtn){
			lcNodeBtn.setNode_btn_id(toUUID());
			i=lcNodeBtnService.addLcNodeBtn(lcNodeBtn);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcNodeBtn 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcNodeBtn(@RequestBody LcNodeBtn lcNodeBtn){
		int i = 0;
		if(null != lcNodeBtn){
			i=lcNodeBtnService.updateLcNodeBtn(lcNodeBtn);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param node_btn_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcNodeBtn(String node_btn_id){
		int i = 0;
		if(!StringUtil.isEmpty(node_btn_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("node_btn_id",node_btn_id.split(","));
			i=lcNodeBtnService.delLcNodeBtn(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
