package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcJumpRules;
import jehc.djshi.workflow.service.LcJumpRulesService;

/**
* @Desc 自定义跳转规则 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:55:44
*/
@Api(value = "自定义跳转规则",description = "自定义跳转规则", tags = "自定义跳转规则")
@RestController
@RequestMapping("/lcJumpRules")
public class LcJumpRulesController extends BaseAction{
	@Autowired
	private LcJumpRulesService lcJumpRulesService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcJumpRules>> getLcJumpRulesListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcJumpRules> lcJumpRulesList = lcJumpRulesService.getLcJumpRulesListByCondition(condition);
		PageInfo<LcJumpRules> page = new PageInfo<LcJumpRules>(lcJumpRulesList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param jump_rules_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{jump_rules_id}")
	public BaseResult<LcJumpRules> getLcJumpRulesById(@PathVariable("jump_rules_id")String jump_rules_id){
		LcJumpRules lcJumpRules = lcJumpRulesService.getLcJumpRulesById(jump_rules_id);
		return outDataStr(lcJumpRules);
	}
	/**
	* 添加
	* @param lcJumpRules 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcJumpRules(@RequestBody LcJumpRules lcJumpRules){
		int i = 0;
		if(null != lcJumpRules){
			lcJumpRules.setJump_rules_id(toUUID());
			i=lcJumpRulesService.addLcJumpRules(lcJumpRules);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcJumpRules 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcJumpRules(@RequestBody LcJumpRules lcJumpRules){
		int i = 0;
		if(null != lcJumpRules){
			i=lcJumpRulesService.updateLcJumpRules(lcJumpRules);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param jump_rules_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcJumpRules(String jump_rules_id){
		int i = 0;
		if(!StringUtil.isEmpty(jump_rules_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("jump_rules_id",jump_rules_id.split(","));
			i=lcJumpRulesService.delLcJumpRules(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
