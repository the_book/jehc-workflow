package jehc.djshi.workflow.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.workflow.model.LcFieldHtml;
import jehc.djshi.workflow.service.LcFieldHtmlService;

/**
* @Desc 字段html类型 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:38:14
*/
@Api(value = "字段html类型",description = "字段html类型", tags = "字段html类型")
@RestController
@RequestMapping("/lcFieldHtml")
public class LcFieldHtmlController extends BaseAction{
	@Autowired
	private LcFieldHtmlService lcFieldHtmlService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LcFieldHtml>> getLcFieldHtmlListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LcFieldHtml> lcFieldHtmlList = lcFieldHtmlService.getLcFieldHtmlListByCondition(condition);
		for(LcFieldHtml lcFieldHtml:lcFieldHtmlList){
			if(!StringUtil.isEmpty(lcFieldHtml.getCreate_id())){
				OauthAccountEntity createBy = getAccount(lcFieldHtml.getCreate_id());
				if(null != createBy){
					lcFieldHtml.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(lcFieldHtml.getUpdate_id())){
				OauthAccountEntity modifieBy = getAccount(lcFieldHtml.getUpdate_id());
				if(null != modifieBy){
					lcFieldHtml.setModifiedBy(modifieBy.getName());
				}
			}
		}
		PageInfo<LcFieldHtml> page = new PageInfo<LcFieldHtml>(lcFieldHtmlList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param lc_field_html_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{lc_field_html_id}")
	public BaseResult<LcFieldHtml> getLcFieldHtmlById(@PathVariable("lc_field_html_id")String lc_field_html_id){
		LcFieldHtml lcFieldHtml = lcFieldHtmlService.getLcFieldHtmlById(lc_field_html_id);
		OauthAccountEntity createBy = getAccount(lcFieldHtml.getCreate_id());
		if(null != createBy){
			lcFieldHtml.setCreateBy(createBy.getName());
		}
		if(!StringUtil.isEmpty(lcFieldHtml.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(lcFieldHtml.getUpdate_id());
			if(null != modifiedBy){
				lcFieldHtml.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(lcFieldHtml);
	}
	/**
	* 添加
	* @param lcFieldHtml 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addLcFieldHtml(@RequestBody LcFieldHtml lcFieldHtml){
		int i = 0;
		if(null != lcFieldHtml){
			lcFieldHtml.setCreate_id(getXtUid());
			lcFieldHtml.setCreate_time(getDate());
			lcFieldHtml.setLc_field_html_id(toUUID());
			i=lcFieldHtmlService.addLcFieldHtml(lcFieldHtml);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param lcFieldHtml 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateLcFieldHtml(@RequestBody LcFieldHtml lcFieldHtml){
		int i = 0;
		if(null != lcFieldHtml){
			lcFieldHtml.setUpdate_id(getXtUid());
			lcFieldHtml.setUpdate_time(getDate());
			i=lcFieldHtmlService.updateLcFieldHtml(lcFieldHtml);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param lc_field_html_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLcFieldHtml(String lc_field_html_id){
		int i = 0;
		if(!StringUtil.isEmpty(lc_field_html_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("lc_field_html_id",lc_field_html_id.split(","));
			i=lcFieldHtmlService.delLcFieldHtml(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}


	/**
	 * 查询列表
	 * @return
	 */
	@ApiOperation(value="查询列表", notes="查询列表")
	@NeedLoginUnAuth
	@GetMapping(value="/find")
	public BaseResult<List<LcFieldHtml>> getFieldTypeList(){
		Map<String, Object> condition = new HashMap<>();
		List<LcFieldHtml> lcFieldHtmlList = lcFieldHtmlService.getLcFieldHtmlListByCondition(condition);
		return new BaseResult(lcFieldHtmlList);
	}
}
