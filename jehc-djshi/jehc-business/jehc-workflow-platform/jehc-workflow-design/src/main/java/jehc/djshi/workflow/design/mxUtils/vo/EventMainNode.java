package jehc.djshi.workflow.design.mxUtils.vo;

import lombok.Data;

import java.util.List;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class EventMainNode {
    private String javaclass_express;//执行的类或表达式
    private String event_type;//类型
    private String event;//事件
    private String fields;//字段
    private String script;//脚本
    private String runAs;//运行方式
    private String scriptProcessor;//脚本解析器
    private List<Field> fieldList;
}
