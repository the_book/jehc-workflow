package jehc.djshi.workflow.design;

import lombok.Data;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class ImgXmlEntity extends MxGraphModel{
    private String x;
    private String y;
    private String w;
    private String h;
    private String imgXml;
}
