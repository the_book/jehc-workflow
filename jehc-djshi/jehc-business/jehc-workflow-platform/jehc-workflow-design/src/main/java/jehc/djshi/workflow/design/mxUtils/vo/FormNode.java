package jehc.djshi.workflow.design.mxUtils.vo;

import lombok.Data;

import java.util.List;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class FormNode {
    private String formID;//表单编号
    private String formName; //表单名称
    private String formType;//表单类型
    private String formExpression;//表达式
    private String formVariable;//变量
    private String formDefault;//默认值
    private String formDatePattern;//日期
    private String formReadable;//是否只读
    private String formWriteable;//可写
    private String formRequired;//是否校验
    private String formFormValues;//配置字段
    private List<Field> fields;
}
