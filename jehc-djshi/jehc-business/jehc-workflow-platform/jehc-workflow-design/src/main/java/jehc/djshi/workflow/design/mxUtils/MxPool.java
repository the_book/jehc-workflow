package jehc.djshi.workflow.design.mxUtils;

import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.design.mxUtils.communal.MxUtils;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Element;

import java.util.List;
import java.util.Map;

/**
 * @Desc 泳道池
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class MxPool {
	/**
	 *  泳道池操作（泳道池没有连线）
	 * @param root
	 * @param mxCellList
	 * @param mxCell
	 * @return
	 */
	public static String pool(Element root,List mxCellList,Element mxCell){
		String pool_node = "";
 		String processId_ = mxCell.attributeValue("processId_");
 		String processName_ = mxCell.attributeValue("processName_");
 		String candidateStarterUsers_ = mxCell.attributeValue("candidateStarterUsers_");
 		String candidateStarterGroups_ = mxCell.attributeValue("candidateStarterGroups_");
 		//节点中其他基本属性
 		//获取mxCell节点下的mxGeometry节点
        Element mxGeometry = mxCell.element("mxGeometry"); 
        String x = mxGeometry.attributeValue("x");
        String y = mxGeometry.attributeValue("y");
        if(null == y || "".equals(y)){
        	y = "0";
        }
        if(null == x || "".equals(x)){
        	x = "0";
        }
        if(!StringUtil.isEmpty(candidateStarterUsers_)){
        	candidateStarterUsers_ =" activiti:candidateStarterUsers='"+candidateStarterUsers_+"' ";
        }else{
        	candidateStarterUsers_ = "";
        }
        if(!StringUtil.isEmpty(candidateStarterGroups_)){
        	candidateStarterGroups_ =" activiti:candidateStarterGroups='"+candidateStarterGroups_+"' ";
        }else{
        	candidateStarterGroups_ = "";
        }
		//开区间
        pool_node+="<process id='"+processId_+"' name='"+processName_+"' "+candidateStarterUsers_+candidateStarterGroups_+">";
        //****开始区间与闭区间属性 开始****//
        pool_node+= MxUtils.documentation(mxCell);
        pool_node += "<extensionElements>";
        pool_node+=MxUtils.eventListenerNode(mxCell);
        pool_node += "</extensionElements>";
        //****开始区间与闭区间属性 结束****//
		//闭区间
        pool_node += "</process>";
		return pool_node;
	}
	
	/**
	 *  泳道bpmndi(泳道池没有连线)
	 * @param root
	 * @param mxCellList
	 * @param mxCell
	 * @return
	 */
	public static String poolBpmndi(Element root,List mxCellList,Element mxCell){
		String nodeID = mxCell.attributeValue("nodeID");
		Map<String, Object> xyMap = MxUtils.resultBoundsXY(mxCellList, mxCell);
        String x = xyMap.get("x").toString();
        String y = xyMap.get("y").toString();
        String width = xyMap.get("width").toString();
        String height = xyMap.get("height").toString();
		String bpmndi="";
		bpmndi += "<bpmndi:BPMNShape bpmnElement='"+nodeID+"' id='BPMNShape_"+nodeID+"'>";
        bpmndi += "<omgdc:Bounds height='"+height+"' width='"+width+"' x='"+x+"' y='"+y+"'></omgdc:Bounds>";
        bpmndi += "</bpmndi:BPMNShape>";
		return bpmndi;
	}
}
