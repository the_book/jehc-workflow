package jehc.djshi.workflow.design.mxUtils.communal;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.design.mxUtils.vo.CallActivityInputOut;
import jehc.djshi.workflow.design.mxUtils.vo.GraphNode;
import org.dom4j.Element;

import java.util.List;

/**
 * @Desc 会签（包括子流程节点解析）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class MxMultiInstanceUtils {

    /**
     * 会签
     * @param mxCell
     * @return
     */
    public static String multiInstanceNode(Element mxCell){
        String multiInstanceNode="";
        String collection =  mxCell.attributeValue("collection");
        String elementVariable = mxCell.attributeValue("elementVariable");
        String isSequential = mxCell.attributeValue("isSequential");
        String loopCardinality = mxCell.attributeValue("loopCardinality");
        String completionCondition = mxCell.attributeValue("completionCondition");
        if(!StringUtil.isEmpty(loopCardinality) || !StringUtil.isEmpty(collection) || !StringUtil.isEmpty(elementVariable) || !StringUtil.isEmpty(completionCondition) || !StringUtil.isEmpty(isSequential)){
            multiInstanceNode += "<multiInstanceLoopCharacteristics";
            String val = "";
            if(!StringUtil.isEmpty(collection)){
                val += " activiti:collection='"+collection+"' ";
            }
            if(!StringUtil.isEmpty(elementVariable)){
                val += " activiti:elementVariable='"+elementVariable+"' ";
            }
            if(!StringUtil.isEmpty(isSequential)){
                val += " isSequential='"+isSequential+"' ";
            }
            multiInstanceNode += val+">";

            String attr="";
            if(!StringUtil.isEmpty(loopCardinality)){
                attr += " <loopCardinality><![CDATA["+isSequential+"]]></loopCardinality> ";
            }

            if(!StringUtil.isEmpty(completionCondition)){
                attr += "  <completionCondition><![CDATA["+completionCondition+"]]></completionCondition> ";
            }

            multiInstanceNode += attr+"</multiInstanceLoopCharacteristics>";

        }
        return multiInstanceNode;
    }
}
