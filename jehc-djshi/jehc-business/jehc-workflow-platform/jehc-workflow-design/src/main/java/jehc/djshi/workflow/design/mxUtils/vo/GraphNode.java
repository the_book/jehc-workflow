package jehc.djshi.workflow.design.mxUtils.vo;

import lombok.Data;

import java.util.List;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class GraphNode {
    private List<FormNode> formNode;
    private List<EventNode> nodeEvent;
    private List<CallActivityInputOut>  callActivityInputOut;
    private List<SignalNode> nodeSignal;
    private List<DataMainProperties> dataMainProperties;
    private List<MessageNode> nodeMessage;
    private List<DataProperties> dataProperties;
    private List<EventMainNode> nodeEventMain;
}
