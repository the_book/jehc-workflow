package jehc.djshi.workflow.design.mxUtils.communal;

import org.dom4j.Element;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class MxCellModel {
	private Element mxCell;

	public Element getMxCell() {
		return mxCell;
	}

	public void setMxCell(Element mxCell) {
		this.mxCell = mxCell;
	}
}
