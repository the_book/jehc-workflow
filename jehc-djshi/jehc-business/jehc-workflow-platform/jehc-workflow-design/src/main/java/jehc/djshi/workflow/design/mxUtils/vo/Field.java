package jehc.djshi.workflow.design.mxUtils.vo;

import lombok.Data;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class Field {
    private String fid;//字段ID
    private String fname;//字段名称
    private String fieldtype;//字段类型
    private String fieldValue;//字段值

}
