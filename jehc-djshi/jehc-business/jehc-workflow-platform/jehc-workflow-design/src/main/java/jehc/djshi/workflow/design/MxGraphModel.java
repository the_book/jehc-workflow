package jehc.djshi.workflow.design;

import lombok.Data;

/**
 * @Desc MxGraphModel到JPDL属性
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class MxGraphModel {
	private String w;
	private String h;
	private String mxgraphxml;
	private String imgxml;
	private String processName;
	private String processId;
	private String lc_process_id;
	private String remark;
	private String m_event_value;
	private String candidateStarterUsers;//流程开始发起人集合 流程信息中
	private String candidateStarterGroups;//流程开始发起人组 流程中
	private String isExecutable;
	private String signals;
	private String messages;
	private String dataObject;
	private String lc_product_id;
	private String lc_group_id;
	private String bpmn;
	private String moduleKey;
	private String candidate_group_type;
}
