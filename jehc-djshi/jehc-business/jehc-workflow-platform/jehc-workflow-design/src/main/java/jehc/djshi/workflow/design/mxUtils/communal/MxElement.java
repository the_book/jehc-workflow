package jehc.djshi.workflow.design.mxUtils.communal;

import lombok.Data;
import org.dom4j.Element;

import java.util.List;

/**
 * @Desc 五级节点集合
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class MxElement {
	private List<Element> mxCellChildList;/**子节点**/
	private List<Element> mxCellEdgeList;/**连线**/
	private List<Element> mxCellOneList;/**一级节点**/
	private List<Element> mxCellTwoList;/**二级节点**/
	private List<Element> mxCellThreeList;/**三级节点**/
	private List<Element> mxCellFourList;/**四级节点**/
	private List<Element> mxCellFiveList;/**五级节点**/
}
