package jehc.djshi.workflow.design.mxUtils.vo;

import lombok.Data;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class SignalNode {
    private String ID;
    private String name;
    private String scope;
}
