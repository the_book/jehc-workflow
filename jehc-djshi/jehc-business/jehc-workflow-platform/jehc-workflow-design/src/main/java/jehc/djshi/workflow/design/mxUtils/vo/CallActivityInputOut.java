package jehc.djshi.workflow.design.mxUtils.vo;

import lombok.Data;

/**
 * @Desc 输入输出
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class CallActivityInputOut {
    private String source;//source
    private String sourceExpression;//源表达式
    private String target;//目标
}
