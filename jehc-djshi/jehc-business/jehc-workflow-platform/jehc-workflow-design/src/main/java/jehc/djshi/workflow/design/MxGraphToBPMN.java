package jehc.djshi.workflow.design;

import jehc.djshi.common.idgeneration.SnowflakeIdWorker;
import jehc.djshi.common.idgeneration.UUID;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.workflow.design.mxUtils.communal.MxUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * @Desc 生成BPMN文件
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class MxGraphToBPMN{
	/**
	 * 生成BPMN文件 返回BPMN+MaxGraph信息对象
	 * @param mxGraphModel
	 * @return
	 */
	public MxGraphModel createBPMN(MxGraphModel mxGraphModel){
		String mxgraphxml=null;
		try {
			mxgraphxml = formatXML(mxGraphModel.getMxgraphxml());
			mxgraphxml = mxgraphxml.replaceAll("&amp;quot;","'");
			mxgraphxml = mxgraphxml.replaceAll("&gt;",">");
			mxgraphxml = mxgraphxml.replaceAll("&amp;gt;",">");
			mxgraphxml = mxgraphxml.replaceAll("&#xa;","\n");
			mxgraphxml = mxgraphxml.replaceAll("&amp;#xa;","");


//			mxgraphxml = StringEscapeUtils.unescapeHtml3(mxgraphxml);//转义&nbsp;&quot;&amp;&lt;&gt;等符号
			String bpmnxml = MxUtils.mxgraphxmlToBpmnxml(mxgraphxml,mxGraphModel);
			bpmnxml = formatXML(bpmnxml);
//			bpmnxml = paseJson(bpmnxml);
			if(!StringUtil.isEmpty(bpmnxml)){
				String bpmnxml_ = bpmnxml.toString().replaceAll("<extensionElements/>", "");
				bpmnxml_ = bpmnxml_.replaceAll("<extensionElements></extensionElements>", "");
				mxGraphModel.setBpmn(bpmnxml_);
			}
		} catch (Exception e) {
			throw new ExceptionUtil("调用createBPMN出现异常，异常信息："+e.getMessage());
		}
		return mxGraphModel;
	}
	
	/**
	 * 流程的基本信息
	 * @return
	 */
	public static String basically(MxGraphModel mxGraphModel){
		String candidateStarterUsers = "";
		if(!StringUtils.isEmpty(mxGraphModel.getCandidateStarterUsers())){
			candidateStarterUsers = " activiti:candidateStarterUsers='"+mxGraphModel.getCandidateStarterUsers()+"'";
		}
		String candidateStarterGroups ="";
		if(!StringUtils.isEmpty(mxGraphModel.getCandidateStarterGroups())){
			candidateStarterGroups = " activiti:candidateStarterGroups='"+mxGraphModel.getCandidateStarterGroups()+"'";
		}
		String processId ="";
		if(StringUtils.isEmpty(mxGraphModel.getProcessId())){
			SnowflakeIdWorker snowflakeIdWorker = new SnowflakeIdWorker();
			processId = "process" + snowflakeIdWorker.nextId();
			mxGraphModel.setProcessId(processId);
		}else{
			processId = mxGraphModel.getProcessId();
		}
		String name = "";
		if(StringUtils.isEmpty(mxGraphModel.getProcessName())){
			name = UUID.toUUID();
			mxGraphModel.setProcessName(mxGraphModel.getProcessName());
		}else{
			name = mxGraphModel.getProcessName();
		}
		String basically = " id='"+ processId+"' name='"+name+"' "+candidateStarterUsers+candidateStarterGroups;
		return basically;
	}
	
	/**
	 * 流程的基本信息
	 * @return
	 */
	public static String basically(MxGraphModel mxGraphModel,Element pool){
		String candidateStarterUsers = "";
		if(!StringUtils.isEmpty(pool.attributeValue("candidateStarterUsers_"))){
			candidateStarterUsers = " activiti:candidateStarterUsers='"+pool.attributeValue("candidateStarterUsers_")+"'";
		}
		String candidateStarterGroups ="";
		if(!StringUtils.isEmpty(pool.attributeValue("candidateStarterGroups_"))){
			candidateStarterGroups = " activiti:candidateStarterGroups='"+pool.attributeValue("candidateStarterGroups_")+"'";
		}
		String basically = " id='"+ pool.attributeValue("processId_")+"' name='"+pool.attributeValue("processName_")+"' "+candidateStarterUsers+candidateStarterGroups;
		return basically;
	}
	/**
	 * 转换特殊符号
	 * @param str
	 * @return
	 */
	public String paseJson(String str) {
		str = str.replace("<", "&lt;");
		str = str.replace(">", "&gt;");
		return str;
	}

	/**
	 * 格式化XML
	 * @param inputXML
	 * @return
	 * @throws Exception
	 */
	public String formatXML(String inputXML) throws Exception {
		if(StringUtils.isEmpty(inputXML)){
			return "";
		}
		inputXML =inputXML.replace("&", "&amp;");
		SAXReader reader = new SAXReader();
		Document document = reader.read(new StringReader(inputXML));
		String requestXML = null;
		XMLWriter writer = null;
		if (document != null) {
			try {
				StringWriter stringWriter = new StringWriter();
				OutputFormat format = new OutputFormat(" ", true);
				writer = new XMLWriter(stringWriter, format);
				writer.write(document);
				writer.flush();
				requestXML = stringWriter.getBuffer().toString();
			} finally {
				if (writer != null) {
					try {
						writer.close();
					} catch (IOException e) {
					}
				}
			}
		}
		return requestXML;
	}
}
