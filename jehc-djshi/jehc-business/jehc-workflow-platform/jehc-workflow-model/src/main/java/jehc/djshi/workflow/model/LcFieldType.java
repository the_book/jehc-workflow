package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
* @Desc 字段类型 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:42:35
*/
@Data
@ApiModel(value="字段类型对象", description="字段类型")
public class LcFieldType extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String type_id;/**主键**/

	@ApiModelProperty(value = "字段类型")
	private String field_type;/**字段类型**/

	@ApiModelProperty(value = "名称")
	private String name;/**名称**/

	@ApiModelProperty(value = "备注")
	private String remark;/**备注**/
}
