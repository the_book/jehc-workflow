package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.activiti.engine.impl.task.TaskDefinition;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(value="节点实体VO对象", description="节点实体VO")
public class NodeEntity {

    @ApiModelProperty("节点集合")
    private List<TaskDefinition> taskDefinitions = new ArrayList<>();
}
