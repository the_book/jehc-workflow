package jehc.djshi.workflow.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 业务处理参数中心通用参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("业务处理参数中心通用参数")
public class LcReceiveCommonParam {
    @ApiModelProperty("审批意见")
    String comment;/**意见**/

    @ApiModelProperty("批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人120挂起流程130激活流程")
    String behavior;/**批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人120挂起流程130激活流程**/

    @ApiModelProperty("审批行为描述")
    private String behaviorText;/**审批行为描述**/

    @ApiModelProperty("节点统一处理人，如果会签节点则处理人如（张三,李四等等）,逗号分隔 | 普通节点则为单个处理人")
    private String mutilValue;/**节点统一处理人，如果会签节点则处理人如（张三,李四等等）,逗号分隔 | 普通节点则为单个处理人**/

    @ApiModelProperty("是否会签")
    private String mutil;

    @ApiModelProperty("会签节点审批人表达式")
    private String mutilExpression;/**会签节点审批人表达式**/

    @ApiModelProperty("设置归属人（示例：当前任务进行了转办给其他人办理，或者委派给其它办理人，那么如果要知道我交出去后的任务 那么就需要设置所属人，这样方便查询）")
    private boolean setOwner = false;//设置归属人（示例：当前任务进行了转办给其他人办理，或者委派给其它办理人，那么如果要知道我交出去后的任务 那么就需要设置所属人，这样方便查询）

    @ApiModelProperty("任务所属者 配合'转办'或“委派”类型使用，其实也是类似于业务上规避 为了找到分出去的任务而已")
    private String ownerId;//任务所属者 配合"转办"或“委派”类型使用，其实也是类似于业务上规避 为了找到分出去的任务而已

    @ApiModelProperty("结束所有活动实例 默认false (表示当执行跳转规则的时候 结束其它所有正在运行的实例)")
    private boolean endAllActivityInstances = false;//结束所有活动实例 默认false (表示当执行跳转规则的时候 结束其它所有正在运行的实例)

    @ApiModelProperty("办理任务传递的按钮动作id用户后端关联 如同意则将该节点的同意按钮对应的目标节点查找处理")
    private String nodeBtnId;/**办理任务传递的按钮动作id用户后端关联 如同意则将该节点的同意按钮对应的目标节点查找处理**/

    @ApiModelProperty("完成动作向下个节点流转采用模式:10跳转模式 20采用原生引擎完成动作")
    private String complete;/**完成动作向下个节点流转采用模式:10跳转模式 20采用原生引擎完成动作**/

    @ApiModelProperty("按钮对应的条件结果 用于解决根据结果自动找到最新一条记录")
    private String nodeBtnCondition;/**按钮对应的条件结果 用于解决根据结果自动找到最新一条记录*/

    @ApiModelProperty("是否需要持久化日志")
    private boolean needPersistenceAppro = true;

    @ApiModelProperty("是否需要计算会签")
    private boolean needComputeMutil = true;
}


