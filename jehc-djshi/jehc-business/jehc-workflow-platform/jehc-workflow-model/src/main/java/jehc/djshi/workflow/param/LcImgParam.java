package jehc.djshi.workflow.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 流程图自定义参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("流程图自定义参数")
public class LcImgParam {

    @ApiModelProperty("节点id")
    private String activityId;

    @ApiModelProperty("背景颜色")
    private String background;
}
