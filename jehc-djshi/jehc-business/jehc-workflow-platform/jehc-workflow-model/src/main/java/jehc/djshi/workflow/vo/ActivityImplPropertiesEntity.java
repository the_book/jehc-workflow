package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 节点属性
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="节点属性对象", description="节点属性")
public class ActivityImplPropertiesEntity {

    @ApiModelProperty("key")
    private String key;

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("value")
    private String value;
}
