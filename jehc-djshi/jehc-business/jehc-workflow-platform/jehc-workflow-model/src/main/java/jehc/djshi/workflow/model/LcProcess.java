package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
 * @Desc 流程信息存储记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="流程信息对象", description="流程信息")
public class LcProcess extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String lc_process_id;/**流程编号**/

	@ApiModelProperty(value = "打包后的ZIP路径")
	private String lc_process_path;/**打包后的ZIP路径**/

	@ApiModelProperty(value = "流程定义中id（uuid）")
	private String lc_process_uid;/**流程定义中id（uuid）**/

	@ApiModelProperty(value = "流程uk（键）")
	private String lc_process_uk;/**流程uk（键）**/

	@ApiModelProperty(value = "bpmn xml内容")
	private String lc_process_bpmn;/**bpmn xml内容**/

	@ApiModelProperty(value = "流程标题")
	private String lc_process_title;/**流程标题**/

	@ApiModelProperty(value = "bpmn文件路径")
	private String lc_process_bpmn_path;/**bpmn文件路径**/

	@ApiModelProperty(value = "图片路径")
	private String lc_process_img_path;/**图片路径**/

	@ApiModelProperty(value = "状态0待发布1发布中2已关闭")
	private Integer lc_process_status;/**0待发布1发布中2已关闭**/

	@ApiModelProperty(value = "mxgraphxml字符串")
	private String lc_process_mxgraphxml;/**mxgraphxml字符串**/

	@ApiModelProperty(value = "样式风格0直线1曲线 默认直线")
	private Integer lc_process_mxgraph_style = 0;/**样式风格0直线1曲线 默认直线**/

	@ApiModelProperty(value = "标识:0通过平台设计器设计1通过上传部署")
	private int lc_process_flag;/**标识:0通过平台设计器设计1通过上传部署**/

	@ApiModelProperty(value = "附件编号")
	private String xt_attachment;/**附件编号**/

	@ApiModelProperty(value = "备注")
	private String lc_process_remark;/**备注**/

	@ApiModelProperty(value = "imgxml")
	private String imgxml;/**imgxml**/

	@ApiModelProperty(value = "mxgraphxml")
	private String mxgraphxml;

	@ApiModelProperty(value = "产品线id")
	private String lc_product_id;/**产品线id**/

	@ApiModelProperty(value = "产品组id")
	private String lc_group_id;/**产品组id**/

	@ApiModelProperty(value = "产品线名称")
	private String name;/**产品线名称**/

	@ApiModelProperty(value = "产品组")
	private String groupName;/**产品组**/

	@ApiModelProperty(value = "宽度")
	private String w;/**宽度**/

	@ApiModelProperty(value = "高度")
	private String h;/**高度**/

	@ApiModelProperty(value = "模块key")
	private String moduleKey;/**模块key**/

	//扩展属性 用于持久化至设计器中
	@ApiModelProperty(value = "流程发起组类型：0部门1岗位3角色")
	private String candidate_group_type;//流程发起组类型：0部门1岗位3角色

	@ApiModelProperty(value = "发起人")
	private String candidateStarterUsers;//发起人

	@ApiModelProperty(value = "发起组")
	private String candidateStarterGroups;//发起组
	
}
