package jehc.djshi.workflow.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @Desc 节点参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("节点参数")
public class NodeAttributeParam {
    @ApiModelProperty("节点id")
    private String nodeId;

    @ApiModelProperty("历史版本id")
    private String hid_;

    @ApiModelProperty("流程挂靠id（流程id）")
    private String mounts_id;
}
