package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 节点属性
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("节点")
public class LcNodeEntity {
    @ApiModelProperty("节点id")
    private String activityId;//节点id
    @ApiModelProperty("节点名称")
    private String name;//节点名称
    @ApiModelProperty("扩展属性")
    private String attr;/**扩展属性**/
}
