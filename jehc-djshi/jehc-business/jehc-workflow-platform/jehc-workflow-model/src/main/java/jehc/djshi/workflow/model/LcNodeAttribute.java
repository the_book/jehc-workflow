package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.util.List;

/**
 * @Desc 节点属性
 * @Author 邓纯杰
 * @CreateTime 2022-03-27 22:05:30
 */
@Data
@ApiModel("节点属性")
public class LcNodeAttribute extends BaseEntity{
	@ApiModelProperty("主键")
	private String lc_node_attribute_id;/**主键**/
	@ApiModelProperty("属性Key")
	private String attributeKey;/**属性键**/
	@ApiModelProperty("备注")
	private String remark;/**备注**/
	@ApiModelProperty("通知类型")
	private String notice_type;/**通知类型：邮件（mail），短信（sms）站内（notice）,企业微信（enterpriseWechat），钉钉（nailNail），微信公众号（officialWechat）**/
	@ApiModelProperty("扩展属性")
	private String attr;/**扩展属性**/
	@ApiModelProperty("流程历史版本id")
	private String hid_;/**流程历史版本id**/
	@ApiModelProperty("pc表单地址")
	private String pcFormUri;/**pc表单地址**/
	@ApiModelProperty("移动端表单地址")
	private String mobileFormUri;/**移动端表单地址**/
	@ApiModelProperty("流程id")
	private String lc_process_id;/**流程id**/
	@ApiModelProperty("流程标题")
	private String lc_process_title;/**流程标题**/
	@ApiModelProperty("发起人节点：10否20是")
	private String initiator;/**发起人节点：10否20是**/
	@ApiModelProperty("提交意见:10无需提交意见 20必须提交意见，缺省为：无需提交意见")
	private String opinion;/**提交意见:10无需提交意见 20必须提交意见，缺省为：无需提交意见**/
	@ApiModelProperty("允许被撤回:10不允许 20允许")
	private String allowRecall;/**允许被撤回:10不允许 20允许**/
	@ApiModelProperty("允许主动撤回:10不允许 20允许")
	private String recall;/**允许主动撤回:10不允许 20允许**/
	@ApiModelProperty("驳回方式:10按最近一次流入节点 20驳回至发起人 30按自定义节点驳回 40按流程图驳回至上级 默认10")
	private String rejectType;/**驳回方式:10按最近一次流入节点 20驳回至发起人 30按自定义节点驳回 40按流程图驳回至上级 默认10**/
	@ApiModelProperty("完成动作向下个节点流转采用模式:10跳转模式 20原生引擎完成")
	private String supportFreeJump;/**完完成动作向下个节点流转采用模式:10跳转模式 20原生引擎完成**/
	@ApiModelProperty("驳回后执行人：10采用原节点审批人执行20重新指定人30采用原节点配置规则选举人")
	private String rejectExecutor;/**驳回后执行人：10采用原节点审批人执行20重新指定人30采用原节点配置规则选举人**/
	@ApiModelProperty("驳该节点点击驳回时是否终止其它运行实例10否20是")
	private String rejectTerminationOtherExecution;/**驳该节点点击驳回时是否终止其它运行实例10否20是**/
	@ApiModelProperty("按流程图上级驳回规则:10如果当前节点在流程图上存在多个任务随机一个 20如果当前节点在流程图上存在多个任务抛出异常")
	private String rejectRule;/**按流程图上级驳回规则:10如果当前节点在流程图上存在多个任务随机一个 20如果当前节点在流程图上存在多个任务抛出异常**/
	@ApiModelProperty("挂载编号，挂载主模板，发布版本时，会同步至历史版本节点")
	private String mounts_id;/**挂载编号，挂载主模板，发布版本时，会同步至历史版本节点**/
	@ApiModelProperty("节点排序序号")
	private Integer orderNumber;/**节点排序序号**/
	@ApiModelProperty("任务紧急程度：0一般，1较急，2加急")
	private String urgent;/**任务紧急程度：0一般，1较急，2加急**/
	@ApiModelProperty("节点通知模板id集合")
	private String notify_template_ids;/**节点通知模板id集合**/

	@ApiModelProperty("集合名,必须要与流程设计器中会签节点'集合'中变量一致")
	private String mutilKey;/**集合名,必须要与流程设计器中会签节点"集合"中变量一致。如流程设计器中节点集合值为${assigneeList}那么你这个时候就要设置成"assigneeList"即可 这样在传递的时候方便前端选择多个审批人，{"assigneeList":{"JEHC1","JEHC2","JEHC3"}} .**/
	@ApiModelProperty("是否会签节点10否20是")
	private String mutil;/**是否会签节点10否20是**/
	@ApiModelProperty("会签决策方式：10通过20驳回30弃权")
	private String policy;/**会签决策方式：10通过20驳回30弃权**/
	@ApiModelProperty("投票策略:10百分比20投票数")
	private String votingStrategy;/**投票策略:10百分比20投票数**/
	@ApiModelProperty("百分比")
	private Integer percentage;/**百分比**/
	@ApiModelProperty("投票数")
	private Integer numberVotes;/**投票数**/
	@ApiModelProperty("是否允许加签10允许")
	private String allowSign;/**是否允许加签10允许**/
	@ApiModelProperty("是否需要所有人员投票10是20否")
	private String needAllVote;/**是否需要所有人员投票10是20否**/


	@ApiModelProperty("节点按钮集合")
	private List<LcNodeBtn> lcNodeBtns;/**节点按钮集合**/
	@ApiModelProperty("跳转规则集合")
	private List<LcJumpRules> lcJumpRules;/**跳转规则集合**/
	@ApiModelProperty("办理人集合")
	private List<LcNodeCandidate> lcNodeCandidate;/**办理人集合**/
	@ApiModelProperty("字段集合")
	private List<LcNodeFormField> nodeFormField;/**字段集合**/
	@ApiModelProperty("节点通知模板集合")
	private List<LcNotifyTemplate> lcNotifyTemplates;/**节点通知模板集合**/

	/////////////BPM定义中参数////////////
	@ApiModelProperty("bpm节点名称")
	private String nodeName;/**bpm节点名称**/
	@ApiModelProperty("bpm节点编码")
	private String nodeId;/**bpm节点编码**/
	@ApiModelProperty("bpm节点类型")
	private String nodeType;/**bpm节点类型**/
	@ApiModelProperty("bpm")
	private String bpm;
}