package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 流程监听器
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="流程监听器对象", description="流程监听器 ")
public class LcListener extends BaseEntity {

    @ApiModelProperty(value = "主键")
    private String lc_listener_id ;//主键

    @ApiModelProperty(value = "名称")
    private String  lc_listener_name;//名称

    @ApiModelProperty(value = "监听类型：javaclass，express，delegateExpression，ScriptExecutionListener，ScriptTaskListener")
    private String type;//监听类型：javaclass，express，delegateExpression，ScriptExecutionListener，ScriptTaskListener

    @ApiModelProperty(value = "事件类型：create，assignment，complete，all | start，end | start，end，take")
    private String event;//事件类型：create，assignment，complete，all | start，end | start，end，take

    @ApiModelProperty(value = "事件值")
    private String listener_val;//事件值

    @ApiModelProperty(value = "产品线")
    private String lc_product_id;//产品线

    @ApiModelProperty(value = "产品组")
    private String lc_group_id;//产品组

    @ApiModelProperty(value = "备注")
    private String remark;//备注

    @ApiModelProperty(value = "分类：100任务监听 200执行监听 300基于连线执行监听")
    private String categories;//分类：100任务监听 200执行监听 300基于连线执行监听

    @ApiModelProperty(value = "产品线名称")
    private String productName;//产品线名称

    @ApiModelProperty(value = "产品组名称")
    private String groupName;//产品组名称
}
