package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;

/**
* @Desc 工作流表单 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:33:49
*/
@Data
@ApiModel(value="工作流表单对象", description="工作流表单")
public class LcForm extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String lc_form_id;/**主键**/

	@ApiModelProperty(value = "表单name")
	private String name;/**表单name**/

	@ApiModelProperty(value = "表单id")
	private String id;/**表单id**/

	@ApiModelProperty(value = "名称")
	private String title;/**名称**/

	@ApiModelProperty(value = "备注")
	private String remark;/**备注**/

	@ApiModelProperty(value = "产品线id")
	private String lc_product_id;/**产品线id**/

	@ApiModelProperty(value = "产品组id")
	private String lc_group_id;/**产品组id**/

	@ApiModelProperty(value = "表单key")
	private String form_key;//表单key

	@ApiModelProperty(value = "产品线名称")
	private String productName;//产品线名称

	@ApiModelProperty(value = "产品组名称")
	private String groupName;//产品组名称
}
