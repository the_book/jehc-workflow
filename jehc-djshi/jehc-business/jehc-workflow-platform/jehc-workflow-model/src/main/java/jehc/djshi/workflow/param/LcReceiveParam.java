package jehc.djshi.workflow.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * @Desc 接收参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("接收参数")
public class LcReceiveParam extends LcReceiveCommonParam{
    @ApiModelProperty("流程定义id")
    String processDefinitionId;

    @ApiModelProperty("流程实例id")
    String processInstanceId;

    @ApiModelProperty("任务id")
    String taskId;

    @ApiModelProperty("节点id")
    String activityId;

    @ApiModelProperty("流程变量")
    Map<String, Object> variables;

    @ApiModelProperty("类型 报表使用")
    int type;//类型报表使用

    @ApiModelProperty("是否需要设置办理人 默认false，由于不可能在设计器上对每个节点设置变量，并且前端动态选人，同时点击提交下一个任务处理者的时候，完成当前任务activiti并不能返回最新的taskid，所以需要重新设定人，如选择下一个审批人（包括人或候选人或候选组）点击当前节点完成，需要标记出下个节点绑定的用户，并且过滤掉会签节点用户分配人，普通节点使用")
    Boolean needHandledTransactor = false;//是否需要设置办理人 默认false，由于不可能在设计器上对每个节点设置变量，并且前端动态选人，同时点击提交下一个任务处理者的时候，完成当前任务activiti并不能返回最新的taskid，所以需要重新设定人，如选择下一个审批人（包括人或候选人或候选组）点击当前节点完成，需要标记出下个节点绑定的用户，并且过滤掉会签节点用户分配人，普通节点使用

    @ApiModelProperty("候选人 默认不使用")
    private String candidates;//候选人 默认不使用

    @ApiModelProperty("组 默认不使用")
    private String candidateGroup;//组 默认不使用
}
