package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

/**
 * @Desc 活动实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="活动实体对象", description="活动实体")
public class LcActivityEntity {

    @ApiModelProperty("id")
    String id;

    @ApiModelProperty("节点id")
    String activityId;

    @ApiModelProperty("节点名称")
    String activityName;

    @ApiModelProperty("节点类型")
    String activityType;

    @ApiModelProperty("流程定义id")
    String processDefinitionId;

    @ApiModelProperty("流程实例id")
    String processInstanceId;

    @ApiModelProperty("流程运行实例id")
    String executionId;

    @ApiModelProperty("任务id")
    String taskId;

    @ApiModelProperty("调用流程实例id")
    String calledProcessInstanceId;

    @ApiModelProperty("审批人")
    String assignee;

    @ApiModelProperty("开始时间")
    Date startTime;

    @ApiModelProperty("结束时间")
    Date endTime;

    @ApiModelProperty("运行时长")
    Long durationInMillis;

    @ApiModelProperty("租户id")
    String tenantId;
}
