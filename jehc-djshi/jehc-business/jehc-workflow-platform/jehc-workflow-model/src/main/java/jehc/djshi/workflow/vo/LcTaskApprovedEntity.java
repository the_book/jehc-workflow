package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 节点已审批及未审批信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="节点已审批及未审批信息对象", description="节点已审批及未审批信息")
public class LcTaskApprovedEntity {
    @ApiModelProperty("用户工号")
    private String userId;//用户工号

    @ApiModelProperty("批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人120挂起流程130激活流程")
    private String behavior;/**批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人120挂起流程130激活流程**/

    @ApiModelProperty("审批行为描述")
    private String behaviorText;/**审批行为描述**/

    @ApiModelProperty("是否已审批  false未审批 true已审批")
    private Boolean isApproved = false;//是否已审批  false未审批 true已审批

    public LcTaskApprovedEntity(String userId, String behavior, String behaviorText, Boolean isApproved){
        this.userId = userId;
        this.behavior = behavior;
        this.behaviorText =  behaviorText;
        this.isApproved = isApproved;
    }
}
