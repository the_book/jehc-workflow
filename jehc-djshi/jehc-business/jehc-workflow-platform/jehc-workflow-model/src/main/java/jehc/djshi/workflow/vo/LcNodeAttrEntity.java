package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Desc 节点属性
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("节点属性")
public class LcNodeAttrEntity {
    @ApiModelProperty("模块Key")
    private String moduleKey;
    @ApiModelProperty("节点集合")
    private List<LcNodeEntity> lcNodeEntities;
}
