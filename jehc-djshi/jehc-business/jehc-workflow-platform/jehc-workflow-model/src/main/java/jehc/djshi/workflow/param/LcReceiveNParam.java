package jehc.djshi.workflow.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;
import java.util.Map;

/**
 * @Desc 业务处理参数中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("业务处理参数中心")
public class LcReceiveNParam extends LcReceiveCommonParam{

    @ApiModelProperty("流程定义id")
    String processDefinitionId;

    @ApiModelProperty("流程实例id")
    String processInstanceId;

    @ApiModelProperty("任务id")
    String taskId;

    @ApiModelProperty("节点id")
    String activityId;

    @ApiModelProperty("流程变量")
    Map<String, Object> variables;

    @ApiModelProperty("流程参数对象")
    private List<LcReceiveParam> lcReceiveParams;
}