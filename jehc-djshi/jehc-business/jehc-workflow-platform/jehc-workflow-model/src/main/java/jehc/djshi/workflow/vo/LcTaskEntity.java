package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 任务实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="任务实体对象", description="任务实体")
public class LcTaskEntity {

    @ApiModelProperty("任务id")
    private String taskId;//任务id

    @ApiModelProperty("单个任务处理人id（如设置单个审批人）， 如果多个任务处理人id 则逗号分隔（用于多个实例如会签等）")
    private String userId;//单个任务处理人id（如设置单个审批人）， 如果多个任务处理人id 则逗号分隔（用于多个实例如会签等）

    @ApiModelProperty("任务处理人类型：0按人员，1按部门，2按岗位，3按公司（缺省）")
    private int doUserType = 0;//任务处理人类型：0按人员，1按部门，2按岗位，3按公司（缺省）

    @ApiModelProperty("任务处理类型：0单实例，1多实例（如会签，子流程）缺省")
    private int doType = 0;//任务处理类型：0单实例，1多实例（如会签，子流程）缺省

    @ApiModelProperty("会签节点Collection中变量，默认signList")
    private String collectionKey = "signList";//会签节点Collection中变量，默认signList

    @ApiModelProperty("设置归属人（示例：当前任务进行了转办给其他人办理，或者委派给其它办理人，那么如果要知道我交出去后的任务 那么就需要设置所属人，这样方便查询）")
    private boolean setOwner = false;//设置归属人（示例：当前任务进行了转办给其他人办理，或者委派给其它办理人，那么如果要知道我交出去后的任务 那么就需要设置所属人，这样方便查询）

    @ApiModelProperty("任务所属者 配合\"转办\"或“委派”类型使用，其实也是类似于业务上规避 为了找到分出去的任务而已")
    private String ownerId;//任务所属者 配合"转办"或“委派”类型使用，其实也是类似于业务上规避 为了找到分出去的任务而已

}
