package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
* @Desc 节点按钮（关系表） 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:37:18
*/
@Data
@ApiModel(value="节点按钮（关系表） 对象", description="节点按钮（关系表） ")
public class LcNodeBtn extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "节点按钮id")
	private String node_btn_id;/**节点按钮id**/

	@ApiModelProperty(value = "按钮id（关联主表lc_btn）")
	private String btn_id;/**按钮id（关联主表lc_btn）**/

	@ApiModelProperty(value = "流程id")
	private String lc_process_id;/**流程编号**/

	@ApiModelProperty(value = "发布版本id")
	private String lc_deployment_id;/**发布版本id**/

	@ApiModelProperty(value = "节点编号（工作流节点编号）")
	private String node_id;/**节点编号（工作流节点编号）**/

	@ApiModelProperty(value = "节点属性id")
	private String lc_node_attribute_id;/**节点属性id**/

	@ApiModelProperty(value = "扩展节点按钮名称")
	private String btn_label;/**扩展节点按钮名称**/

	@ApiModelProperty(value = "扩展节点按钮动作")
	private String btn_action;/**扩展节点按钮动作**/

	@ApiModelProperty(value = "按钮标签")
	private String _label;//按钮标签

	@ApiModelProperty(value = "按钮对象")
	private LcBtn lcBtn;//按钮对象

	@ApiModelProperty(value = "动作")
	private String b_action;/**动作**/

	@ApiModelProperty(value = "按钮规则")
	private List<LcJumpRules> lcJumpRules;//按钮规则
}
