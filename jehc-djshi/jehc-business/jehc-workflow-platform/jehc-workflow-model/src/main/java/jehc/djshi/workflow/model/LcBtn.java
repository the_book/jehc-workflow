package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
* @Desc 按钮 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:31:14
*/
@Data
@ApiModel(value="按钮对象", description="按钮")
public class LcBtn extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String btn_id;/**主键**/

	@ApiModelProperty(value = "按钮标签")
	private String _label;/**按钮标签**/

	@ApiModelProperty(value = "备注")
	private String remark;/**备注**/

	@ApiModelProperty(value = "动作")
	private String b_action;/**动作**/

	@ApiModelProperty(value = "脚本")
	private String b_script;/**脚本**/

	@ApiModelProperty(value = "扩展属性")
	private String attr;/**扩展属性**/

	@ApiModelProperty(value = "元素id")
	private String element_id;/**元素id**/

	@ApiModelProperty(value = "元素名称")
	private String element_name;/**元素名称**/

	@ApiModelProperty(value = "提示语")
	private String tips;/**提示语**/

	@ApiModelProperty(value = "元素class样式")
	private String element_class;/**元素class样式**/

	@ApiModelProperty(value = "图标")
	private String icons;/**图标**/

	@ApiModelProperty(value = "排序号")
	private int sort_;//排序号

	@ApiModelProperty(value = "行为：0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办")
	private String behavior;/**行为：0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办**/
}
