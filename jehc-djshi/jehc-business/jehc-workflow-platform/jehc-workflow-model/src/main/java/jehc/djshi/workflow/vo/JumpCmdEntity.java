package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 跳转实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="跳转实体对象", description="跳转实体")
public class JumpCmdEntity {

    @ApiModelProperty("流程实例Id")
    String processInstanceId; //流程实例Id

    @ApiModelProperty("任务id")
    String taskId;

    @ApiModelProperty("目标任务节点")
    String activityId; //目标任务节点

    @ApiModelProperty("是否级联删除历史记录 false 否 true 是")
    boolean casCade = true;//是否级联删除历史记录 false 否 true 是

    @ApiModelProperty("跳转节点原因")
    String reason; //跳转节点原因

    @ApiModelProperty("类型：0.默认 确保只要执行跳转则需要删除当前其并行的实例（即执行跳转时候 删除当流程实例下正在运行的其它节点） 1.删除并行任务非同一节点（如网关并行任务的时候，当出现多个任务的时候，如果其中一个节点上任务执行跳转其它节点时候 需删除其它节点任务 与会签有本质区别）。2.删除同一节点多任务并行（在同一个任务节点如会签 在该节点生成多个任务的时候 如果当前一个节点要求跳转其它节点的时候 需要删除与其并行的任务）。3.驳回时删除其它并行任务（只要跳转都要删除与其同一实例中的其它任务也删除）")
    int type;//0.默认 确保只要执行跳转则需要删除当前其并行的实例（即执行跳转时候 删除当流程实例下正在运行的其它节点） 1.删除并行任务非同一节点（如网关并行任务的时候，当出现多个任务的时候，如果其中一个节点上任务执行跳转其它节点时候 需删除其它节点任务 与会签有本质区别）。2.删除同一节点多任务并行（在同一个任务节点如会签 在该节点生成多个任务的时候 如果当前一个节点要求跳转其它节点的时候 需要删除与其并行的任务）。3.驳回时删除其它并行任务（只要跳转都要删除与其同一实例中的其它任务也删除）
}
