package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Desc 运行实例图片
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="运行实例图片对象", description="运行实例图片")
public class ActivityImplImageEntity {

    @ApiModelProperty("图片Base64格式")
    private String image;//图片Base64格式

    @ApiModelProperty("activityImplEntities")
    private List<ActivityImplEntity> activityImplEntities;

    //仅供流程预览设计器使用///
    @ApiModelProperty("mxGraphXml")
    private String mxGraphXml;

    @ApiModelProperty("连线样式")
    private int lineStyle;

    public ActivityImplImageEntity(){

    }

    public ActivityImplImageEntity(String image,List<ActivityImplEntity> activityImplEntities){
        this.image = image;
        this.activityImplEntities = activityImplEntities;

    }

    public ActivityImplImageEntity(String mxGraphXml,int lineStyle,List<ActivityImplEntity> activityImplEntities){
        this.mxGraphXml = mxGraphXml;
        this.lineStyle = lineStyle;
        this.activityImplEntities = activityImplEntities;

    }
}
