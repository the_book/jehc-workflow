package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 历史任务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="历史任务对象", description="历史任务")
public class ActHiTaskinst {

    @ApiModelProperty("主键id")
    private String id;

    @ApiModelProperty("流程定义id")
    private String procDefId;

    @ApiModelProperty("任务节点id")
    private String taskDefKey;

    @ApiModelProperty("流程实例id")
    private String procInstId;

    @ApiModelProperty("流程执行实例id")
    private String executionId;
}
