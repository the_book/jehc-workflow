package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
* @Desc 节点字段 
* @Author 邓纯杰
* @CreateTime 2022-04-06 11:41:43
*/
@Data
@ApiModel(value="节点字段对象", description="节点字段")
public class LcNodeFormField extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String node_field_id;/**主键**/

	@ApiModelProperty(value = "标签")
	private String field_label;/**标签**/

	@ApiModelProperty(value = "字段id")
	private String field_id;/**字段id**/

	@ApiModelProperty(value = "字段name")
	private String field_name;/**字段name**/

	@ApiModelProperty(value = "只读：0是1否")
	private Integer field_read;/**只读：0是1否**/

	@ApiModelProperty(value = "可写：0是1否")
	private Integer field_write;/**可写：0是1否**/

	@ApiModelProperty(value = "隐藏：0是1否")
	private Integer filed_hide;/**隐藏：0是1否**/

	@ApiModelProperty(value = "必填：0是1否")
	private Integer filed_required;/**必填：0是1否**/

	@ApiModelProperty(value = "流程id")
	private String lc_process_id;/**流程id**/

	@ApiModelProperty(value = "历史版本id")
	private String lc_deployment_id;/**历史版本id**/

	@ApiModelProperty(value = "节点id")
	private String node_id;/**节点id**/

	@ApiModelProperty(value = "节点属性id")
	private String lc_node_attribute_id;/**节点属性id**/
}
