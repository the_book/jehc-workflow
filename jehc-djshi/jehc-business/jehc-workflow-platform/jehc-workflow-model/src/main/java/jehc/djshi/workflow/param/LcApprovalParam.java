package jehc.djshi.workflow.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BasePage;
import lombok.Data;

/**
 * @Desc 审批记录
 * @Author 邓纯杰
 * @CreateTime 2022-08-18 12:12:12
 */
@Data
@ApiModel("审批记录")
public class LcApprovalParam extends BasePage {
    @ApiModelProperty("流程实例id")
    private String instanceId;//流程实例id

    @ApiModelProperty("批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人120挂起流程130激活流程")
    String behavior;/**批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人120挂起流程130激活流程**/

    @ApiModelProperty("节点id")
    String activityId;

    @ApiModelProperty("办理人")
    String userId;

    @ApiModelProperty("业务Key")
    private String businessKey;//业务Key

}
