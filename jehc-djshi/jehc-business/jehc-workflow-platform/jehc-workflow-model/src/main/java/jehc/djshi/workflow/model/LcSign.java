package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * @Desc 会签动态扩展业务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="会签动态扩展业务对象", description="会签动态扩展业务")
public class LcSign extends BaseEntity {

    @ApiModelProperty(value = "主键")
    private String lc_sign_id;/**主键**/

    @ApiModelProperty(value = "流程定义主键")
    private String proc_def_id;/**流程定义主键**/

    @ApiModelProperty(value = "流程任务节点定义KEY（同activityimpl中id）")
    private String task_def_key;/**流程任务节点定义KEY（同activityimpl中id）**/

    @ApiModelProperty(value = "流程实例id")
    private String proc_inst_id;/**流程实例id**/

    @ApiModelProperty(value = "运行实例id主键")
    private String execution_id;/**运行实例id主键**/

    @ApiModelProperty(value = "节点名称")
    private String name;/**节点名称**/

    @ApiModelProperty(value = "会签总用户数")
    private int nr_of_instances;/**会签总用户数**/

    @ApiModelProperty(value = "已经完成的会签签用户数")
    private int nr_of_completed_instances;/**已经完成的会签签用户数**/

    @ApiModelProperty(value = "节域名（租户）")
    private String tennet_id;/**节域名（租户）**/

    @ApiModelProperty(value = "加签记录")
    private List<LcSignRecord> lcSignRecords;

    @ApiModelProperty(value = "批次")
    private int batch;/**批次**/
}
