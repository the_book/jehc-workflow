package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
/**
 * @Desc 流程部署历史记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="流程部署历史记录对象", description="流程部署历史记录")
public class LcDeploymentHis extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String id;/**主键**/

	@ApiModelProperty(value = "流程部署Id")
	private String lc_deployment_his_id;/**流程部署Id**/

	@ApiModelProperty(value = "名称")
	private String lc_deployment_his_name;/**名称**/

	@ApiModelProperty(value = "部署时间")
	private Date lc_deployment_his_time;/**部署时间**/

	@ApiModelProperty(value = "租户编号")
	private String lc_deployment_his_tenantId;/**租户编号**/

	@ApiModelProperty(value = "流程编号")
	private String lc_process_id;/**流程编号**/

	@ApiModelProperty(value = "常量id")
	private String xt_constant_id;

	@ApiModelProperty(value = "状态0正常1关闭")
	private int lc_deployment_his_status;/**状态0正常1关闭**/

	@ApiModelProperty(value = "Bpmn流程中定义id")
	private String uid;/**Bpmn流程中定义id**/

	@ApiModelProperty(value = "流程uk（键）")
	private String uk;/** 流程uk（键）**/

	@ApiModelProperty(value = "bpmn xml内容")
	private String lc_process_bpmn;/**bpmn xml内容**/

	@ApiModelProperty(value = "mxgraphxml字符串")
	private String lc_process_mxgraphxml;/**mxgraphxml字符串**/

	@ApiModelProperty(value = "样式风格0直线1曲线")
	private int lc_process_mxgraph_style;/**样式风格0直线1曲线**/

	@ApiModelProperty(value = "image xml字符串")
	private String imgxml;/**image xml字符串**/

	@ApiModelProperty(value = "流程标题")
	private String lc_process_title;

	@ApiModelProperty(value = "产品线名称")
	private String name;/**产品线名称**/

	@ApiModelProperty(value = "产品组")
	private String groupName;/**产品组**/

	@ApiModelProperty(value = "标识:0通过平台设计器设计1通过上传部署")
	private int lc_process_flag;/**标识:0通过平台设计器设计1通过上传部署**/

	@ApiModelProperty(value = "宽度")
	private String w;/**宽度**/

	@ApiModelProperty(value = "高度")
	private String h;/**高度**/

	@ApiModelProperty(value = "流程状态：0待发布1发布中2已关闭")
	private Integer lc_process_status;/**0待发布1发布中2已关闭**/

	@ApiModelProperty(value = "流程设计是否删除0正常1删除")
	private Integer lcProcessDelFlag;/**流程设计是否删除0正常1删除**/

	//扩展属性 用于持久化至设计器中
	@ApiModelProperty(value = "流程发起组类型：0部门1岗位3角色")
	private String candidate_group_type;//流程发起组类型：0部门1岗位3角色

	@ApiModelProperty(value = "发起人")
	private String candidateStarterUsers;//发起人

	@ApiModelProperty(value = "发起组")
	private String candidateStarterGroups;//发起组

}

