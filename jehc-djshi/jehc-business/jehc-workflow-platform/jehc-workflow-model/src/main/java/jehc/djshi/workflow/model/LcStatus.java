package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 流程状态
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="流程状态对象", description="流程状态")
public class LcStatus extends BaseEntity {

	@ApiModelProperty(value = "流程状态编号")
	private String lc_status_id;/**流程状态编号**/

	@ApiModelProperty(value = "状态名称")
	private String lc_status_name;/**状态名称**/

	@ApiModelProperty(value = "备注")
	private String lc_status_remark;/**备注**/

	@ApiModelProperty(value = "流程常量")
	private String lc_status_constant;/**流程常量**/
}
