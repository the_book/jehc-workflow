package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @Desc 流程通知模板
 * @Author 邓纯杰
 * @CreateTime 2022-04-06 11:41:43
 */
@Data
@ApiModel(value="流程通知模板对象", description="流程通知模板")
public class LcNotifyTemplate extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;/**主键**/

    @ApiModelProperty(value = "标题")
    private String title;/**标题**/

    @ApiModelProperty(value = "模板内容")
    private String content;/**模板内容**/

    @ApiModelProperty(value = "通知类型：邮件（mail）短信（sms）站内（notice）")
    private String type;/**通知类型：邮件（mail）短信（sms）站内（notice）**/

    @ApiModelProperty(value = "模板key")
    private Integer template_key;/**模板key**/
}
