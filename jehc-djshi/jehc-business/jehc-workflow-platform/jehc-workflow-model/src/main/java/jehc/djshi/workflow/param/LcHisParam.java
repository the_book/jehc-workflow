package jehc.djshi.workflow.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * @Desc 接收参数（包括流程发起实例等等）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("实例参数")
public class LcHisParam {
    @ApiModelProperty("发布版本id")
    private String hid_;//发布版本id

    @ApiModelProperty("业务编号")
    private String businessKey;//业务Key

    @ApiModelProperty("流程部署编号")
    private String deploymentId;//流程部署id

    @ApiModelProperty("模块Key")
    private String moduleKey;//模块Key

    @ApiModelProperty("发起人节点处理人")
    private String mutilValue;/**发起人节点统一处理人，如果会签节点则处理人如（张三,李四等等）,逗号分隔 | 普通节点则为单个处理人**/

    @ApiModelProperty("流程变量")
    Map<String, Object> variables;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("下一个节点统一处理人，如果会签节点则处理人如（张三,李四等等）,逗号分隔 | 普通节点则为单个处理人")
    private String nextMutilValue;

    @ApiModelProperty("下一个节点id")
    String nextActivityId;

    @ApiModelProperty("是否完成任务10完成 20不执行完成，如是否发起流程实例完成第一个节点")
    String complated = "10";

    @ApiModelProperty("流程定义版本号 配合模块key使用（注意 如果版本号为空 则采用模块Key下最新流程定义）")
    String version;
}
