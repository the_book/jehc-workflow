package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;
/**
 * @Desc 运行时任务
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("运行时任务")
public class ActRuTask {
    @ApiModelProperty("主键id")
    private String id;/****/

    @ApiModelProperty("乐观锁版本号")
    private Integer rev;/**版本号，乐观锁**/

    @ApiModelProperty("执行实例id")
    private String executionId;/**执行实例id对应act_ru_execution表id**/

    @ApiModelProperty("流程定义ID")
    private String procDefId;/**流程定义ID**/

    @ApiModelProperty("流程实例ID")
    private String procInstId;/**一个流程实例不管有多少条分支实例，这个ID都是一致的**/

    @ApiModelProperty("节点名称")
    private String name;/**节点名称**/

    @ApiModelProperty("父节点实例id")
    private String parentTaskId;/**父节点实例id**/

    @ApiModelProperty("节点定义描述")
    private String description;/**节点定义描述**/

    @ApiModelProperty("节点key")
    private String taskDefKey;/**节点key**/

    @ApiModelProperty("实际签收人")
    private String owner;/**拥有者（一般情况下为空，在委托时使用）**/

    @ApiModelProperty("办理人")
    private String assignee;/**签收人或委托人**/

    @ApiModelProperty("委托类型")
    private String delegation;/**委托类型，DelegationState分为两种：PENDING，RESOLVED。如无委托则为空**/

    @ApiModelProperty("优先级别")
    private Integer priority;/**优先级别，默认为：50**/

    @ApiModelProperty("创建时间")
    private Date createTime;/**创建时间**/

    @ApiModelProperty("过期时间")
    private Date dueDate;/**过期时间	**/

    @ApiModelProperty("分类")
    private String category;/**分类**/

    @ApiModelProperty("租户ID")
    private String tenantId;/**租户ID，可以应对多租户的设计**/

    @ApiModelProperty("表单Key")
    private String formKey;/**表单Key**/

    @ApiModelProperty("是否挂起")
    private Integer suspensionState;/**是否挂起：1代表激活 2代表挂起	**/
}
