package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.util.Map;
/**
 * @Desc 流程申请
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="流程申请对象", description="流程申请")
public class LcApply extends BaseEntity{

	@ApiModelProperty(value = "流程申请id，主键")
	private String lcApplyId;/**流程申请id**/

	@ApiModelProperty(value = "标题")
	private String title;/**标题**/

	@ApiModelProperty(value = "模块")
	private String modelKey;/**模块**/

	@ApiModelProperty(value = "流程实例id")
	private String procInstId;/**流程实例id**/

	@ApiModelProperty(value = "流程定义id编号")
	private String procDefId;/**流程定义id编号**/

	@ApiModelProperty(value = "流程部署id")
	private String deploymentId;/**流程部署id**/

	@ApiModelProperty(value = "URL可缺省")
	private String lcUrl;/**URL可缺省**/

	@ApiModelProperty(value = "业务Key")
	private String businessKey;/**业务Key**/

	@ApiModelProperty(value = "业务对象状态")
	private String status;/**业务对象状态**/

	@ApiModelProperty(value = "业务对象流程是否完成0否1完成")
	private int completed;/**业务对象流程是否完成0否1完成**/

	@ApiModelProperty(value = "备注")
	private String remark;/**备注**/

	@ApiModelProperty(value = "变量")
	private Map<String,Object> variables;//变量
}
