package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.workflow.model.LcNodeAttribute;
import jehc.djshi.workflow.model.LcProcess;
import lombok.Data;

import java.util.List;

/**
 * @Desc 流程对象
 * @Author 邓纯杰
 * @CreateTime 2022-03-27 22:05:30
 */
@Data
@ApiModel("流程节点属性")
public class LcProcessEntity {

    @ApiModelProperty("流程对象")
    private LcProcess lcProcess;//流程对象

    @ApiModelProperty("流程节点属性对象")
    private List<LcNodeAttribute> lcNodeAttributes;//流程节点属性
}
