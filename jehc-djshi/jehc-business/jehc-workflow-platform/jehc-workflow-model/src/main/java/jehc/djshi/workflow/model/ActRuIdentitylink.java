package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 运行时流程人员
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("运行时流程人员")
public class ActRuIdentitylink {
    @ApiModelProperty("主键id")
    private String id;/****/

    @ApiModelProperty("乐观锁版本号")
    private Integer rev;/**版本号，乐观锁**/

    @ApiModelProperty("组id")
    private String groupId;/**对应act_id_group中的ID_,在TYPE_字段为 candidate时有效**/

    @ApiModelProperty("类型")
    private String type;/**类型，取值有：starter、candidate、participant、assignee、owner**/

    @ApiModelProperty("用户id")
    private String userId;/**用户id**/

    @ApiModelProperty("任务id")
    private String taskId;/**任务id，注意只有在任务中添加组成员时，该任务id会存在值**/

    @ApiModelProperty("流程定义ID")
    private String procDefId;/**流程定义ID**/

    @ApiModelProperty("流程实例ID")
    private String procInstId;/**一个流程实例不管有多少条分支实例，这个ID都是一致的**/
}
