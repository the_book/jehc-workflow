package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 流程表达式
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="流程表达式对象", description="流程表达式")
public class LcExp extends BaseEntity {

    @ApiModelProperty(value = "主键")
    private String lc_exp_id ;//主键

    @ApiModelProperty(value = "名称")
    private String  lc_exp_name;//名称

    @ApiModelProperty(value = "表达式")
    private String lc_exp_val;//表达式

    @ApiModelProperty(value = "扩展字段")
    private String lc_expcol;//扩展字段

    @ApiModelProperty(value = "产品线")
    private String lc_product_id;//产品线

    @ApiModelProperty(value = "产品组")
    private String lc_group_id;//产品组

    @ApiModelProperty(value = "备注")
    private String remark;//备注

    @ApiModelProperty(value = "产品线名称")
    private String productName;//产品线名称

    @ApiModelProperty(value = "产品组名称")
    private String groupName;//产品组名称
}
