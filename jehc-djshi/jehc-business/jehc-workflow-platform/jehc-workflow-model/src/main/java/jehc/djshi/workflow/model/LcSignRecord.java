package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 会签动态扩展业务子表
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="会签动态扩展业务子表对象", description="会签动态扩展业务子表")
public class LcSignRecord extends BaseEntity {

    @ApiModelProperty(value = "主键")
    private String lc_sign_record_id;/**主键**/

    @ApiModelProperty(value = "会签节点主表id")
    private String lc_sign_id;/**会签节点主表id**/

    @ApiModelProperty(value = "备注")
    private String comment;/**备注**/

    @ApiModelProperty(value = "审批人任务id")
    private String task_id;/**审批人任务id**/

    @ApiModelProperty(value = "审批动作")
    private String action_;/**审批动作：10同意20拒绝30弃权**/

    @ApiModelProperty(value = "审批人")
    private String user_id;/**审批人**/

    @ApiModelProperty(value = "批次")
    private int batch;/**批次**/

    @ApiModelProperty(value = "目标节点id")
    private String activityId;/**目标节点id**/

    @ApiModelProperty(value = "目标节点处理人")
    private String mutilValue;/**目标节点处理人**/
}
