package jehc.djshi.workflow.vo;
import jehc.djshi.workflow.design.MxGraphModel;
import jehc.djshi.workflow.model.LcProcess;
import lombok.Data;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class LcBaseEntity {
    private LcProcess lcProcess;
    private MxGraphModel mxGraphModel;
}
