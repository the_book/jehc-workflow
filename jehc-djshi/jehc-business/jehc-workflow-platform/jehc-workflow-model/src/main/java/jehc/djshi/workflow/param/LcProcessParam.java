package jehc.djshi.workflow.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 流程参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("流程参数")
public class LcProcessParam {

    @ApiModelProperty("流程id")
    private String lc_process_id;/**流程id**/

    @ApiModelProperty("流程标题")
    private String lc_process_title;/**流程标题**/

    @ApiModelProperty("模块key")
    private String moduleKey;/**模块key**/

    @ApiModelProperty("是否同步扩展属性")
    private Integer syncAttr;/**是否同步扩展属性**/

    @ApiModelProperty("类型：0项目1需求")
    private Integer type;/**类型0项目1需求**/

    @ApiModelProperty("产品线id")
    private String lc_product_id;/**产品线id**/

    @ApiModelProperty("产品组id")
    private String lc_group_id;/**产品组id**/
}