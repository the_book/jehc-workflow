package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
* @Desc 节点办理人 
* @Author 邓纯杰
* @CreateTime 2022-04-03 20:12:39
*/
@Data
@ApiModel(value="节点办理人对象", description="节点办理人")
public class LcNodeCandidate extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String lc_node_candidate_id;/**主键**/

	@ApiModelProperty(value = "按人员设定类型（sponsor：用户发起人，Specific：具体人）")
	private String assignee_type;/**按人员设定类型（sponsor：用户发起人，Specific：具体人**/

	@ApiModelProperty(value = "办理人id")
	private String assignee_id;/**办理人id**/

	@ApiModelProperty(value = "候选人id集合")
	private String candidate_id;/**候选人id集合**/

	@ApiModelProperty(value = "按组设定类型（department：部门，post：岗位，role：角色）")
	private String candidate_group_type;/**按组设定类型（department：部门，post：岗位，role：角色）**/

	@ApiModelProperty(value = "组id集合")
	private String candidate_group_id;/**组id集合**/

	@ApiModelProperty(value = "流程id")
	private String lc_process_id;/**流程id**/

	@ApiModelProperty(value = "部署id")
	private String lc_deployment_id;/**部署id**/

	@ApiModelProperty(value = "节点id")
	private String node_id;/**节点id**/

	@ApiModelProperty(value = "节点属性id")
	private String lc_node_attribute_id;/**节点属性id**/

	@ApiModelProperty(value = "备注")
	private String remark;/**备注**/

	@ApiModelProperty(value = "候选人备注")
	private String remark1;/**候选人备注**/

	@ApiModelProperty(value = "组备注")
	private String remark2;/**组备注**/
}
