package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 产品组
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="产品组对象", description="产品组")
public class LcGroup extends BaseEntity {

    @ApiModelProperty(value = "主键")
    private String lc_group_id;

    @ApiModelProperty(value = "产品id")
    private String lc_product_id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "产品名称")
    private String productName;
}
