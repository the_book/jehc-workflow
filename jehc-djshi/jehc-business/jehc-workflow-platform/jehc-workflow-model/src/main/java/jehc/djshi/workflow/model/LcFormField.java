package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
* @Desc 工作流表单字段 
* @Author 邓纯杰
* @CreateTime 2022-04-04 19:17:12
*/
@Data
@ApiModel(value="工作流表单字段对象", description="工作流表单字段")
public class LcFormField extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String lc_form_field_id;/**主键**/

	@ApiModelProperty(value = "标签")
	private String field_label;/**标签**/

	@ApiModelProperty(value = "字段id")
	private String field_id;/**字段id**/

	@ApiModelProperty(value = "字段name")
	private String field_name;/**字段name**/

	@ApiModelProperty(value = "只读：0是1否")
	private Integer field_read;/**只读：0是1否**/

	@ApiModelProperty(value = "可写：0是1否")
	private Integer field_write;/**可写：0是1否**/

	@ApiModelProperty(value = "隐藏：0是1否")
	private Integer filed_hide;/**隐藏：0是1否**/

	@ApiModelProperty(value = "必填：0是1否")
	private Integer filed_required;/**必填：0是1否**/

	@ApiModelProperty(value = "字段长度")
	private Integer field_maxlength;/**字段长度**/

	@ApiModelProperty(value = "字段类型id")
	private String field_type_id;/**字段类型id**/

	@ApiModelProperty(value = "字段html类型id")
	private String field_html_id;/**字段html类型id**/

	@ApiModelProperty(value = "排序")
	private Integer sort_;/**排序**/

	@ApiModelProperty(value = "参与业务：0参与1否")
	private Integer participation;/**参与业务：0参与1否**/

	@ApiModelProperty(value = "提示")
	private String tips;/**提示**/

	@ApiModelProperty(value = "表单id")
	private String lc_form_id;/**表单id**/

	@ApiModelProperty(value = "字段后端类型名称")
	private String typeName;//字段后端类型名称

	@ApiModelProperty(value = "字段输入类型名称")
	private String htmlName;//字段输入类型名称
}
