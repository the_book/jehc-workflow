package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;

/**
* @Desc 字段html类型 
* @Author 邓纯杰
* @CreateTime 2022-04-04 18:38:14
*/
@Data
@ApiModel(value="字段html类型对象", description="字段html类型")
public class LcFieldHtml extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String lc_field_html_id;/****/

	@ApiModelProperty(value = "名称")
	private String name;/**名称**/

	@ApiModelProperty(value = "编码")
	private String code;/**编码**/

	@ApiModelProperty(value = "内容")
	private String content;/**内容**/

	@ApiModelProperty(value = "html标签类型：如text,combo,textarea等等")
	private String type;/**html标签类型：如text,combo,textarea等等**/
}
