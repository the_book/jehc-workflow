package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="流程报表实例对象", description="流程报表实例")
public class LcReportInstEntity {

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("值")
    private String value;

    public LcReportInstEntity(){

    }

    public LcReportInstEntity(String name,String value){
        this.name = name;
        this.value = value;
    }
}
