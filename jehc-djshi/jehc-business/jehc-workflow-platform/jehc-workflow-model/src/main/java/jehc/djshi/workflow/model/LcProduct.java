package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 产品线
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="产品线对象", description="产品线")
public class LcProduct extends BaseEntity {

    @ApiModelProperty(value = "主键")
    private String lc_product_id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "状态")
    private int status;

    @ApiModelProperty(value = "键")
    private String l_key;

    @ApiModelProperty(value = "md5")
    private String md5;

    @ApiModelProperty(value = "描述")
    private String content;
}
