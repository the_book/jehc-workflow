package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 会签节点历史人员
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="会签节点历史人员对象", description="会签节点历史人员")
public class LcHisMutil extends BaseEntity{

    @ApiModelProperty(value = "主键")
    private String lc_his_mutil_id;/**主键**/

    @ApiModelProperty(value = "审批人")
    private String assignee;/**审批人**/

    @ApiModelProperty(value = "审批人名称")
    private String assignee_text;/**审批人名称**/

    @ApiModelProperty(value = "历史实例id")
    private String pro_inst_id;/**历史实例id**/

    @ApiModelProperty(value = "节点编号")
    private String activity_id;/**节点编号**/

    @ApiModelProperty(value = "节点名称")
    private String activity_name;/**节点名称**/

    @ApiModelProperty(value = "执行实例id")
    private String excute_id;/**执行实例id**/

    @ApiModelProperty(value = "最大批次")
    private Integer batch;/**最大批次**/
}
