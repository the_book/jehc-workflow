package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @Desc 行为实体部分属性
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="行为实体部分属性对象", description="行为实体部分属性")
public class BehaviorEntity {

    @ApiModelProperty("节点表达式Text")
    private String collectionExpressionText;

    @ApiModelProperty("节点表达式Value")
    private String collectionExpressionValue;

    @ApiModelProperty("节点表达式变量")
    private String collectionVariable;

    @ApiModelProperty("collectionElement变量")
    private String collectionElementVariable;

    @ApiModelProperty("collectionElementIndex变量")
    private String collectionElementIndexVariable;
}
