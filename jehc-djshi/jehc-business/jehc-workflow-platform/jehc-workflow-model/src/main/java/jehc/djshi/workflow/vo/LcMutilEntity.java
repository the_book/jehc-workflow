package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
/**
 * @Desc 会签结果
 * @Author 邓纯杰
 * @CreateTime 2022-03-27 22:05:30
 */
@Data
@ApiModel(value="会签结果对象", description="会签结果")
public class LcMutilEntity {

    @ApiModelProperty("任务实体对象")
    private TaskEntity taskEntity;

    @ApiModelProperty("总的会签任务数量")
    private int nrOfInstances; //总的会签任务数量

    @ApiModelProperty("当前获取的会签任务数量 ---未执行的")
    private int nrOfActiveInstances; //当前获取的会签任务数量 ---未执行的

    @ApiModelProperty("总的会签任务数量---已执行的")
    private int nrOfCompletedInstances; //总的会签任务数量---已执行的

    /**
     * 构造函数
     */
    public LcMutilEntity(){}

    /**
     * 构造函数
     * @param nrOfInstances
     * @param nrOfActiveInstances
     * @param nrOfCompletedInstances
     */
    public LcMutilEntity(int nrOfInstances,int nrOfActiveInstances,int nrOfCompletedInstances){
        this.nrOfInstances = nrOfInstances;
        this.nrOfActiveInstances = nrOfActiveInstances;
        this.nrOfCompletedInstances = nrOfCompletedInstances;

    }
    /**
     * 构造函数
     * @param nrOfInstances
     * @param nrOfActiveInstances
     * @param nrOfCompletedInstances
     */
    public LcMutilEntity(TaskEntity taskEntity,int nrOfInstances,int nrOfActiveInstances,int nrOfCompletedInstances){
        this.taskEntity = taskEntity;
        this.nrOfInstances = nrOfInstances;
        this.nrOfActiveInstances = nrOfActiveInstances;
        this.nrOfCompletedInstances = nrOfCompletedInstances;

    }
}
