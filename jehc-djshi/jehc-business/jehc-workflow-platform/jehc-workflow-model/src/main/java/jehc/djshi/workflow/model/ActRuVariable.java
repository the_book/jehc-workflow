package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 运行时流程变量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("运行时流程变量")
public class ActRuVariable {
    @ApiModelProperty("主键id")
    private String id;/****/

    @ApiModelProperty("乐观锁版本号")
    private Integer rev;/**版本号，乐观锁**/

    @ApiModelProperty("参数类型")
    private String type;/**参数类型,不为空可选值：boolean. bytes、serializable. date. double、 integer、jpa-entity、long、 null、 short 、string以上字段值由Activiti 提供，也可自定义扩展类型**/

    @ApiModelProperty("参数名称")
    private String name;/**参数名称**/

    @ApiModelProperty("执行实例id")
    private String executionId;/**执行实例id对应act_ru_execution表id**/

    @ApiModelProperty("流程实例ID")
    private String procInstId;/**一个流程实例不管有多少条分支实例，这个ID都是一致的**/

    @ApiModelProperty("任务id")
    private String taskId;/**任务id**/

    @ApiModelProperty("资源表id")
    private String byteArrayId;/**资源表ID(若参数值是序列化对象，可将该对象作为资源保存到资源表中）**/

    @ApiModelProperty("双精度")
    private String double_;/**参数类型为double,则值会保存到该字段中**/

    @ApiModelProperty("参数类型为long")
    private String long_;/**参数类型为long, 则值会保存到该字段中**/

    @ApiModelProperty("参数类型为text")
    private String text;/**参数类型为text, 则值会保存到该字段中**/

    @ApiModelProperty("文本类型的参数值")
    private String text2;/**文本类型的参数值**/
}
