package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 执行实例
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("执行实例")
public class ActRuExecution {

    @ApiModelProperty("主键id")
    private String id;/**EXECUTION主键，这个主键有可能和PROC_INST_ID_相同，相同的情况表示这条记录为主实例记录**/

    @ApiModelProperty("乐观锁版本号")
    private Integer rev;/**数据库表更新次数**/

    @ApiModelProperty("流程实例ID")
    private String procInstId;/**一个流程实例不管有多少条分支实例，这个ID都是一致的**/

    @ApiModelProperty("业务主键")
    private String businessKey;/**业务主键，主流程才会使用业务主键，另外这个业务主键字段在表中有唯一约束**/

    @ApiModelProperty("父节点实例ID")
    private String parentId;/**表示父实例ID，如上图，同步节点会产生两条执行记录，这两条记录的父ID为主线的ID**/

    @ApiModelProperty("流程定义ID")
    private String procDefId;/**流程定义ID**/

    @ApiModelProperty("SUPER_EXEC_")
    private String superExec;/**表示这个实例记录为一个外部子流程记录，对应主流程的主键ID**/

    @ApiModelProperty("节点编号")
    private String actId;/**流程运行到的节点，如上图主实例运行到ANDGateway1 节点，两个子实例运行到UserTask1,UserTask2节点。**/

    @ApiModelProperty("是否存活")
    private Integer isActive;/**是否活动流程实例，比如上图，主流程为非活动实例，下面两个为活动实例，如果UserTask2完成，那么这个值将变为0即非活动。**/

    @ApiModelProperty("是否并行")
    private Integer isConcurrent;/**是否并发。上图同步节点后为并发，如果是并发多实例也是为1**/

    @ApiModelProperty("IS_SCOPE_")
    private Integer isScope;/**主实例的情况这个字段为1，子实例这个字段为0**/

    @ApiModelProperty("使用事件")
    private Integer isEventScope;/**没有使用到事件的情况下，一般都为0**/

    @ApiModelProperty("是否挂起")
    private Integer suspensionState;/**是否挂起**/

    @ApiModelProperty("缓存状态")
    private Integer cachedEntState;/**缓存状态**/

    @ApiModelProperty("租户ID")
    private String tenantId;/**租户ID，可以应对多租户的设计**/

    @ApiModelProperty("名称")
    private String name;/**名称**/

    private Integer parentIdRowSpan;
    private Integer procInstIdRowSpan;
    private Integer procDefIdRowSpan;
}
