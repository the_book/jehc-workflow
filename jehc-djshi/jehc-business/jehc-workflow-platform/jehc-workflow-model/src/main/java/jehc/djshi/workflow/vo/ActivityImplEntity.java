package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @Desc 自定义活动节点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="自定义活动节点对象", description="自定义活动节点")
public class ActivityImplEntity {

    @ApiModelProperty("x")
    private int x;

    @ApiModelProperty("y")
    private int y;

    @ApiModelProperty("宽")
    private int width;

    @ApiModelProperty("高")
    private int height;

    @ApiModelProperty("id")
    private String id;

    public ActivityImplEntity(){

    }

    /**
     *
     * @param x
     * @param y
     * @param width
     * @param height
     * @param id
     */
    public ActivityImplEntity(int x, int y, int width, int height, String id){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height =height;
        this.id = id;
    }
}
