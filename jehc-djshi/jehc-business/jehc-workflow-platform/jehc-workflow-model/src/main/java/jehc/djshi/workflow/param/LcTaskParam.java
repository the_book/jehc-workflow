package jehc.djshi.workflow.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("任务参数")
public class LcTaskParam {

    @ApiModelProperty("任务id")
    private String taskId;//任务id

    @ApiModelProperty("任务名称")
    private String taskName;//任务名称

    @ApiModelProperty("流程实例id")
    private String procInstId; //流程实例id

    @ApiModelProperty("经办人")
    private String transactors;//经办人

    @ApiModelProperty("候选人")
    private String candidates;//候选人

    @ApiModelProperty("候选组")
    private String group;//候选组

    @ApiModelProperty("候选组")
    private String owner;//任务所属人

    @ApiModelProperty("业务Key")
    private String businessKey;//业务Key

}
