package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
/**
 * @Desc 流程实例实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="流程实例实体对象", description="流程实例实体")
public class LcInstanceEntity {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("流程定义id")
    private String processDefinitionId;

    @ApiModelProperty("业务key")
    private String businessKey;

    @ApiModelProperty("部署id")
    private String deploymentId;

    @ApiModelProperty("部署描述")
    String description;

    @ApiModelProperty("名称")
    String name;

    @ApiModelProperty("执行id")
    String superExecutionId;

    @ApiModelProperty("节点id（节点编号）")
    private String activityId;

    @ApiModelProperty("流程定义名称")
    private String processDefinitionName;

    @ApiModelProperty("流程实例id")
    private String processInstanceId;

    @ApiModelProperty("是否挂起")
    private boolean suspended;

    @ApiModelProperty("开始时间")
    Date startTime;

    @ApiModelProperty("结束时间")
    Date endTime;

    @ApiModelProperty("运行时长")
    Long durationInMillis;

    @ApiModelProperty("任务名称")
    private String taskName;//当前任务

    @ApiModelProperty("是否结束")
    private boolean end;
}
