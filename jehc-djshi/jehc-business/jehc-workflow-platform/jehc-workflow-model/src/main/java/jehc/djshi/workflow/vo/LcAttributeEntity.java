package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.workflow.model.LcBtn;
import jehc.djshi.workflow.model.LcNodeCandidate;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Desc 目标节点属性
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("目标节点属性")
public class LcAttributeEntity {

    @ApiModelProperty("批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人")
    String behavior;/**批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人**/

    @ApiModelProperty("目标节点id")
    String activityId;

    @ApiModelProperty("目标节点名称")
    String activityName;

    @ApiModelProperty("流程变量")
    Map<String, Object> variables;

    @ApiModelProperty("办理任务传递的按钮动作id用户后端关联 如同意则将该节点的同意按钮对应的目标节点查找处理")
    private String nodeBtnId;/**办理任务传递的按钮动作id用户后端关联 如同意则将该节点的同意按钮对应的目标节点查找处理**/

//    @ApiModelProperty("节点统一处理人，如果会签节点则处理人如（张三,李四等等）,逗号分隔 | 普通节点则为单个处理人")
//    private String mutilValue;/**节点统一处理人，如果会签节点则处理人如（张三,李四等等）,逗号分隔 | 普通节点则为单个处理人**/

    @ApiModelProperty("是否会签节点10否20是")
    private String mutil;

    @ApiModelProperty("会签节点审批人表达式")
    private String mutilExpression;/**会签节点审批人表达式**/

    @ApiModelProperty("动作")
    private String btnAction;//动作

    @ApiModelProperty("按钮标签")
    private String _label;//按钮标签

    @ApiModelProperty("办理人集合")
    private List<LcNodeCandidate> lcNodeCandidateList;/**办理人集合**/

    @ApiModelProperty("按钮对象")
    private LcBtn lcBtn;

    @ApiModelProperty("目标节点是否为结束节点")
    private Boolean end = false;//目标节点是否为结束节点

    @ApiModelProperty("扩展属性")
    private String attr;/**扩展属性**/
}
