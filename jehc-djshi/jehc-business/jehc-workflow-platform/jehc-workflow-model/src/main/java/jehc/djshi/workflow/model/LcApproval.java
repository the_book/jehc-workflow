package jehc.djshi.workflow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 工作流批审记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="工作流批审记录对象", description="工作流批审记录")
public class LcApproval extends BaseEntity {

	@ApiModelProperty(value = "主键")
	private String lcApprovalId;/**主键**/

	@ApiModelProperty(value = "标题")
	private String title;/**标题**/

	@ApiModelProperty(value = "申请编号 可选")
	private String lcApplyId;/**申请编号 可选**/

	@ApiModelProperty(value = "流程定义id")
	private String procDefId;/**流程定义id**/

	@ApiModelProperty(value = "流程实例")
	private String procInstId;/**流程实例 **/

	@ApiModelProperty(value = "任务节点定义编号")
	private String taskDefKey;/**任务节点定义编号**/

	@ApiModelProperty(value = "activiti任务id")
	private String taskId;/**activiti任务id**/

	@ApiModelProperty(value = "任务名称")
	private String taskName;/**任务名称**/

	@ApiModelProperty(value = "批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人120挂起流程130激活流程")
	private String behavior;/**批审行为0提交10通过20驳回30弃权40撤回50终止流程60执行跳转70转办80委派90加签100催办110设置归属人120挂起流程130激活流程**/

	@ApiModelProperty(value = "审批行为描述")
	private String behaviorText;/**审批行为描述**/

	@ApiModelProperty(value = "审批意见")
	private String comment;/**审批意见**/

	@ApiModelProperty(value = "审批人")
	private String userId;/**审批人**/

	@ApiModelProperty(value = "业务编号")
	private String businessKey;/**业务编号**/
}
