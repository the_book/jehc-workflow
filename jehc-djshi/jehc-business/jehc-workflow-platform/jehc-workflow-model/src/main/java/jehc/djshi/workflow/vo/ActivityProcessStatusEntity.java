package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 流程实例状态
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="流程实例状态对象", description="流程实例状态")
public class ActivityProcessStatusEntity {
    @ApiModelProperty("当前审批节点id")
    private String currentActivityId;//当前审批节点id

    @ApiModelProperty("当前审批任务节点名称")
    private String currentName;//当前审批任务节点名称

    @ApiModelProperty("流程实例id")
    private String procInstId;//流程实例id

    @ApiModelProperty("是否结束")
    private Boolean end = false;//是否结束 默认false

    @ApiModelProperty("最新任务节点名称")
    private String name;//最新任务节点名称

    @ApiModelProperty("最新任务节点id")
    private String activityId;//最新任务节点id

    @ApiModelProperty("最新任务节点处理人工号")
    private String account;//最新任务节点处理人工号

    @ApiModelProperty("会签节点是否有结果导向")
    private Boolean mutilComplate = false;//会签节点是否有结果导向 默认false

    /**
     * 默认构造函数
     */
    public ActivityProcessStatusEntity(){

    }

    /**
     * 构造函数
     */
    public ActivityProcessStatusEntity(String procInstId,Boolean end){
        this.procInstId = procInstId;
        this.end = end;
    }

    /**
     * 构造函数
     */
    public ActivityProcessStatusEntity(String procInstId,Boolean end,String name){
        this.procInstId = procInstId;
        this.end = end;
        this.name = name;
    }

    /**
     * 构造函数
     */
    public ActivityProcessStatusEntity(String procInstId,Boolean end,String name,String activityId){
        this.procInstId = procInstId;
        this.end = end;
        this.name = name;
        this.activityId = activityId;
    }

    /**
     * 构造函数
     */
    public ActivityProcessStatusEntity(String procInstId,Boolean end,String name,String activityId,String currentActivityId){
        this.procInstId = procInstId;
        this.end = end;
        this.name = name;
        this.activityId = activityId;
        this.currentActivityId = currentActivityId;
    }
}
