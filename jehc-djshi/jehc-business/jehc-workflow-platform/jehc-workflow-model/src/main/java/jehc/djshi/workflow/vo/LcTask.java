package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.Map;

/**
 * @Desc 任务实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="任务实体VO对象", description="任务实体")
public class LcTask {

    @ApiModelProperty("分类")
    private String category;

    @ApiModelProperty("处理人")
    private String assignee;

    @ApiModelProperty("描述")
    private String description;

    @ApiModelProperty("执行实例id")
    private String executionId;

    @ApiModelProperty("表单key")
    private String formKey;

    @ApiModelProperty("任务id")
    private String taskId;

    @ApiModelProperty("任务名称")
    private String name;

    @ApiModelProperty("任务所属者")
    private String owner;

    @ApiModelProperty("父任务id")
    private String parentTaskId;

    @ApiModelProperty("priority")
    private Integer priority;

    @ApiModelProperty("流程定义id")
    private String processDefinitionId;

    @ApiModelProperty("流程实例id")
    private String processInstanceId;

    @ApiModelProperty("节点key")
    private String taskDefinitionKey;

    @ApiModelProperty("租户")
    private String tenantId;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("级联状态")
    private String delegationState;

    @ApiModelProperty("日期规则")
    private Date dueDate;

    @ApiModelProperty("流程变量")
    private Map<String,Object> processVariables;

    @ApiModelProperty("开始日期")
    private Date startTime;

    @ApiModelProperty("结束日期")
    private Date endTime;

    @ApiModelProperty("工作时长")
    private Long workTimeInMillis;

    @ApiModelProperty("耽搁时长")
    private Long durationInMillis;

    @ApiModelProperty("挂起")
    private Boolean suspended;//挂起


    //扩展属性
    @ApiModelProperty("流程名称")
    private String processTitle;/**流程名称**/

    @ApiModelProperty("产品线名称")
    private String productName;/**产品线名称**/

    @ApiModelProperty("产品组")
    private String groupName;/**产品组**/

    @ApiModelProperty("版本")
    private Integer version;/**版本**/

    @ApiModelProperty("节点类型")
    private String nodeType;/**节点类型**/

    @ApiModelProperty("是否会签节点10否20是")
    private String mutil;/**是否会签节点10否20是**/

    @ApiModelProperty("业务编号")
    private String businessKey;/**业务编号**/
}
