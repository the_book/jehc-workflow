package jehc.djshi.workflow.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.workflow.model.LcNodeCandidate;
import jehc.djshi.workflow.model.LcNodeFormField;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Desc 当前节点属性
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel("节点属性")
public class LcAttributeNEntity {
    @ApiModelProperty("任务id")
    String taskId;

    @ApiModelProperty("节点id")
    String activityId;

    @ApiModelProperty("节点名称")
    String activityName;

    @ApiModelProperty("流程变量")
    Map<String, Object> variables;

    @ApiModelProperty("目标节点")
    private List<LcAttributeEntity> lcAttributeEntities;

    @ApiModelProperty("pc表单地址")
    private String pcFormUri;

    @ApiModelProperty("移动端表单地址")
    private String mobileFormUri;

    @ApiModelProperty("字段集合")
    private List<LcNodeFormField> nodeFormFieldList;/**字段集合**/

    @ApiModelProperty("扩展属性")
    private String attr;/**扩展属性**/

    @ApiModelProperty("办理人集合")
    private List<LcNodeCandidate> lcNodeCandidateList;/**办理人集合**/

    @ApiModelProperty("提交意见:10无需提交意见 20必须提交意见，缺省为：无需提交意见")
    private String opinion;/**提交意见:10无需提交意见 20必须提交意见，缺省为：无需提交意见**/

    @ApiModelProperty("是否会签节点10否20是")
    private String mutil;

    @ApiModelProperty("当前任务是否挂起：true是 false否")
    private Boolean suspended = false;//当前任务是否挂起：true是 false否

    @ApiModelProperty("流程实例id")
    String processInstanceId;
}
