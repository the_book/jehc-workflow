package jehc.djshi.workflow.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;

/**
* @Desc 自定义跳转规则 
* @Author 邓纯杰
* @CreateTime 2022-03-31 16:55:44
*/
@Data
@ApiModel(value="自定义跳转规则对象", description="自定义跳转规则")
public class LcJumpRules extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String jump_rules_id;/**主键**/

	@ApiModelProperty(value = "节点名称")
	private String node_name;//节点名称

	@ApiModelProperty(value = "节点id")
	private String node_id;/**节点id**/

	@ApiModelProperty(value = "目标节点")
	private String target_node_id;/**目标节点**/

	@ApiModelProperty(value = "基于条件跳转：0关闭1启用")
	private Integer based_on_condition;/**基于条件跳转：0关闭1启用**/

	@ApiModelProperty(value = "节点属性id")
	private String lc_node_attribute_id;/**节点属性id**/

	@ApiModelProperty(value = "条件（如果基于条件则填写条件内容 自定义 开发人员可以根据设定条件进行跳转，如研发总监角色可以跳转任意节点）")
	private String conditions_;/**条件（如果基于条件则填写条件内容 自定义 开发人员可以根据设定条件进行跳转，如研发总监角色可以跳转任意节点）**/

	@ApiModelProperty(value = "基于按钮id")
	private String node_btn_id;//基于按钮id
}
