package jehc.djshi.log.dao;

import jehc.djshi.log.model.LogLoadinfo;

import java.util.List;
import java.util.Map;

/**
* @Desc 页面加载信息 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:22:05
*/
public interface LogLoadinfoDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LogLoadinfo> getLogLoadinfoListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	LogLoadinfo getLogLoadinfoById(String id);
	/**
	* 添加
	* @param logLoadinfo 
	* @return
	*/
	int addLogLoadinfo(LogLoadinfo logLoadinfo);
	/**
	* 修改
	* @param logLoadinfo 
	* @return
	*/
	int updateLogLoadinfo(LogLoadinfo logLoadinfo);
	/**
	* 修改（根据动态条件）
	* @param logLoadinfo 
	* @return
	*/
	int updateLogLoadinfoBySelective(LogLoadinfo logLoadinfo);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLogLoadinfo(Map<String, Object> condition);
}
