package jehc.djshi.oauth.service.impl;
import java.util.List;
import java.util.Map;

import jehc.djshi.oauth.service.OauthFunctionCommonService;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.oauth.model.OauthFunctionCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.oauth.dao.OauthFunctionCommonDao;
import jehc.djshi.oauth.model.OauthFunctionCommon;
/**
 * @Desc 授权中心公共功能
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthFunctionCommonService")
public class OauthFunctionCommonServiceImpl extends BaseService implements OauthFunctionCommonService {
	@Autowired
	private OauthFunctionCommonDao oauthFunctionCommonDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthFunctionCommon> getOauthFunctionCommonListByCondition(Map<String,Object> condition){
		try{
			return oauthFunctionCommonDao.getOauthFunctionCommonListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param function_common_id 
	* @return
	*/
	public OauthFunctionCommon getOauthFunctionCommonById(String function_common_id){
		try{
			OauthFunctionCommon oauthFunctionCommon = oauthFunctionCommonDao.getOauthFunctionCommonById(function_common_id);
			return oauthFunctionCommon;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param oauthFunctionCommon 
	* @return
	*/
	public int addOauthFunctionCommon(OauthFunctionCommon oauthFunctionCommon){
		int i = 0;
		try {
			oauthFunctionCommon.setCreate_id(getXtUid());
			oauthFunctionCommon.setCreate_time(getDate());
			i = oauthFunctionCommonDao.addOauthFunctionCommon(oauthFunctionCommon);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param oauthFunctionCommon 
	* @return
	*/
	public int updateOauthFunctionCommon(OauthFunctionCommon oauthFunctionCommon){
		int i = 0;
		try {
			oauthFunctionCommon.setUpdate_id(getXtUid());
			oauthFunctionCommon.setUpdate_time(getDate());
			i = oauthFunctionCommonDao.updateOauthFunctionCommon(oauthFunctionCommon);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param oauthFunctionCommon 
	* @return
	*/
	public int updateOauthFunctionCommonBySelective(OauthFunctionCommon oauthFunctionCommon){
		int i = 0;
		try {
			oauthFunctionCommon.setUpdate_id(getXtUid());
			oauthFunctionCommon.setUpdate_time(getDate());
			i = oauthFunctionCommonDao.updateOauthFunctionCommonBySelective(oauthFunctionCommon);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delOauthFunctionCommon(Map<String,Object> condition){
		int i = 0;
		try {
			i = oauthFunctionCommonDao.delOauthFunctionCommon(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param oauthFunctionCommonList 
	* @return
	*/
	public int updateBatchOauthFunctionCommon(List<OauthFunctionCommon> oauthFunctionCommonList){
		int i = 0;
		try {
			i = oauthFunctionCommonDao.updateBatchOauthFunctionCommon(oauthFunctionCommonList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param oauthFunctionCommonList 
	* @return
	*/
	public int updateBatchOauthFunctionCommonBySelective(List<OauthFunctionCommon> oauthFunctionCommonList){
		int i = 0;
		try {
			i = oauthFunctionCommonDao.updateBatchOauthFunctionCommonBySelective(oauthFunctionCommonList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
