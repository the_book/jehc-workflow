package jehc.djshi.oauth.service.impl;

import jehc.djshi.oauth.dao.OauthAdminSysDao;
import jehc.djshi.oauth.model.OauthAdminSys;
import jehc.djshi.oauth.service.OauthAdminSysService;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.oauth.model.OauthAdminSys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 管理员拥有系统范围
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class OauthAdminSysServiceImpl extends BaseService implements OauthAdminSysService {
    @Autowired
    private OauthAdminSysDao oauthAdminSysDao;
    /**
     *
     * @param condition
     * @return
     */
    public List<OauthAdminSys> getOauthAdminSysList(Map<String,Object> condition){
        return oauthAdminSysDao.getOauthAdminSysList(condition);
    }

    /**
     *
     * @param oauthAdminSys
     * @return
     */
    public int addOauthAdminSys(OauthAdminSys oauthAdminSys){
        int i = 0;
        try {
            i = oauthAdminSysDao.addOauthAdminSys(oauthAdminSys);
        } catch (Exception e) {
            i = 0;
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }
}
