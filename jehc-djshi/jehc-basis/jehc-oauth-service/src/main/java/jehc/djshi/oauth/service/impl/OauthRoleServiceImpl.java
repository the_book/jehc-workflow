package jehc.djshi.oauth.service.impl;
import java.util.List;
import java.util.Map;

import jehc.djshi.oauth.service.OauthRoleService;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.oauth.model.OauthRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.oauth.dao.OauthRoleDao;
import jehc.djshi.oauth.model.OauthRole;
/**
 * @Desc 角色模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("oauthRoleService")
public class OauthRoleServiceImpl extends BaseService implements OauthRoleService {
	@Autowired
	private OauthRoleDao oauthRoleDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OauthRole> getOauthRoleListByCondition(Map<String,Object> condition){
		try{
			return oauthRoleDao.getOauthRoleListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param role_id 
	* @return
	*/
	public OauthRole getOauthRoleById(String role_id){
		try{
			OauthRole oauthRole = oauthRoleDao.getOauthRoleById(role_id);
			return oauthRole;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param oauthRole 
	* @return
	*/
	public int addOauthRole(OauthRole oauthRole){
		int i = 0;
		try {
			oauthRole.setCreate_id(getXtUid());
			oauthRole.setCreate_time(getDate());
			i = oauthRoleDao.addOauthRole(oauthRole);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param oauthRole 
	* @return
	*/
	public int updateOauthRole(OauthRole oauthRole){
		int i = 0;
		try {
			oauthRole.setUpdate_id(getXtUid());
			oauthRole.setUpdate_time(getDate());
			i = oauthRoleDao.updateOauthRole(oauthRole);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param oauthRole 
	* @return
	*/
	public int updateOauthRoleBySelective(OauthRole oauthRole){
		int i = 0;
		try {
			oauthRole.setUpdate_id(getXtUid());
			oauthRole.setUpdate_time(getDate());
			i = oauthRoleDao.updateOauthRoleBySelective(oauthRole);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delOauthRole(Map<String,Object> condition){
		int i = 0;
		try {
			i = oauthRoleDao.delOauthRole(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param oauthRoleList 
	* @return
	*/
	public int updateBatchOauthRole(List<OauthRole> oauthRoleList){
		int i = 0;
		try {
			i = oauthRoleDao.updateBatchOauthRole(oauthRoleList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param oauthRoleList 
	* @return
	*/
	public int updateBatchOauthRoleBySelective(List<OauthRole> oauthRoleList){
		int i = 0;
		try {
			i = oauthRoleDao.updateBatchOauthRoleBySelective(oauthRoleList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 恢复
	 * @param condition
	 * @return
	 */
	public int recoverOauthRole(Map<String, Object> condition){
		int i = 0;
		try {
			i = oauthRoleDao.recoverOauthRole(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
