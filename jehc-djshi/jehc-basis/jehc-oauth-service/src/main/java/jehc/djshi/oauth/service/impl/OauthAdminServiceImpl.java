package jehc.djshi.oauth.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.dao.OauthAdminDao;
import jehc.djshi.oauth.dao.OauthAdminSysDao;
import jehc.djshi.oauth.model.OauthAdmin;
import jehc.djshi.oauth.model.OauthAdminSys;
import jehc.djshi.oauth.service.OauthAdminService;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.oauth.service.OauthAdminSysService;
import jehc.djshi.oauth.dao.OauthAdminDao;
import jehc.djshi.oauth.model.OauthAdminSys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 管理员中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
public class OauthAdminServiceImpl extends BaseService implements OauthAdminService {
    @Autowired
    private OauthAdminDao oauthAdminDao;

    @Autowired
    private OauthAdminSysDao oauthAdminSysDao;
    /**
     *
     * @param condition
     * @return
     */
    public List<OauthAdmin> getOauthAdminList(Map<String,Object> condition){
        return oauthAdminDao.getOauthAdminList(condition);
    }

    /**
     *
     * @param oauthAdmin
     * @return
     */
    public int addOauthAdmin(OauthAdmin oauthAdmin){
        int i = 0;
        try {
            Map<String,Object> condition = new HashMap<>();
            condition.put("account_id",oauthAdmin.getAccount_id());
            List<OauthAdmin> oauthAdmins = oauthAdminDao.getOauthAdminList(condition);
            if(CollectionUtil.isNotEmpty(oauthAdmins)){
                String sys_mode_id = oauthAdmin.getSys_mode_id();
                oauthAdmin =  oauthAdmins.get(0);
                oauthAdmin.setSys_mode_id(sys_mode_id);
            }else{
                i = oauthAdminDao.addOauthAdmin(oauthAdmin);
            }
            condition = new HashMap<>();
            condition.put("sysmode_id",oauthAdmin.getSys_mode_id());
            condition.put("admin_id",oauthAdmin.getId());
            OauthAdminSys oauthAdminSys = oauthAdminSysDao.getSingleOauthAdminSys(condition);
            if(null == oauthAdminSys){
                oauthAdminSys = new OauthAdminSys();
                oauthAdminSys.setId(toUUID());
                oauthAdminSys.setAdmin_id(oauthAdmin.getId());
                oauthAdminSys.setSysmode_id(oauthAdmin.getSys_mode_id());
                oauthAdminSysDao.addOauthAdminSys(oauthAdminSys);
            }
            i =1;
        } catch (Exception e) {
            i = 0;
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }

    /**
     *
     * @param condition
     * @return
     */
    public List<OauthAdmin> getOauthAdminListAll(Map<String,Object> condition){
        return oauthAdminDao.getOauthAdminListAll(condition);
    }

    /**
     * 从模块系统中移除管理员
     * @param oauthAdmin
     * @return
     */
    public int delOauthAdmin(OauthAdmin oauthAdmin){
        if(StringUtil.isEmpty(oauthAdmin.getSys_mode_id())){
            throw new ExceptionUtil("未能获取到模块编号：sys_mode_id");
        }

        if(StringUtil.isEmpty(oauthAdmin.getAccount_id())){
            throw new ExceptionUtil("未能获取到账户编号：account_id");
        }
        int i = 0;
        try {
            Map<String,Object> condition = new HashMap<>();
            condition.put("account_id",oauthAdmin.getAccount_id());
            List<OauthAdmin> oauthAdmins = oauthAdminDao.getOauthAdminList(condition);
            if(CollectionUtil.isNotEmpty(oauthAdmins) && oauthAdmins.size() == 1){
                //移除当前
                oauthAdminDao.delOauthAdmin(oauthAdmins.get(0).getId());
            }
            //移除分配表数据
            condition.put("sysmode_id",oauthAdmin.getSys_mode_id());
            List<OauthAdminSys> oauthAdminSysList = oauthAdminSysDao.getOauthAdminSysListByCondition(condition);
            if(!CollectionUtil.isEmpty(oauthAdminSysList)){
                for(OauthAdminSys oauthAdminSys: oauthAdminSysList){
                    oauthAdminSysDao.delOauthAdminSys(oauthAdminSys.getId());
                }
            }
            i = 1;
        }catch (Exception e){
            throw  new ExceptionUtil("移除管理员出现异常");
        }

        return i;
    }
}
