package jehc.djshi.log.service;

import jehc.djshi.log.model.LogLogin;

import java.util.List;
import java.util.Map;

/**
* @Desc 登录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:21:01
*/
public interface LogLoginService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LogLogin> getLogLoginListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	LogLogin getLogLoginById(String id);
	/**
	* 添加
	* @param logLogin 
	* @return
	*/
	int addLogLogin(LogLogin logLogin);
	/**
	* 修改
	* @param logLogin 
	* @return
	*/
	int updateLogLogin(LogLogin logLogin);
	/**
	* 修改（根据动态条件）
	* @param logLogin 
	* @return
	*/
	int updateLogLoginBySelective(LogLogin logLogin);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLogLogin(Map<String, Object> condition);
}
