package jehc.djshi.log.service;

import jehc.djshi.log.model.LogStartStop;

import java.util.List;
import java.util.Map;

/**
* @Desc 服务器启动与关闭日志; InnoDB free: 9216 kB 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:28:59
*/
public interface LogStartStopService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LogStartStop> getLogStartStopListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	LogStartStop getLogStartStopById(String id);
	/**
	* 添加
	* @param logStartStop 
	* @return
	*/
	int addLogStartStop(LogStartStop logStartStop);
	/**
	* 修改
	* @param logStartStop 
	* @return
	*/
	int updateLogStartStop(LogStartStop logStartStop);
	/**
	* 修改（根据动态条件）
	* @param logStartStop 
	* @return
	*/
	int updateLogStartStopBySelective(LogStartStop logStartStop);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLogStartStop(Map<String, Object> condition);
}
