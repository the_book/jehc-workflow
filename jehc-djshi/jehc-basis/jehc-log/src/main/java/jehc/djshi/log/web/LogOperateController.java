package jehc.djshi.log.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.log.model.LogOperate;
import jehc.djshi.log.service.LogOperateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @Desc 操作日志表 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:27:00
*/
@RestController
@RequestMapping("/logOperate")
@Api(value = "操作日志API",tags = "操作日志API",description = "操作日志")
public class LogOperateController extends BaseAction{
	@Autowired
	private LogOperateService logOperateService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LogOperate>> getLogOperateListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LogOperate> logOperateList = logOperateService.getLogOperateListByCondition(condition);
		PageInfo<LogOperate> page = new PageInfo<LogOperate>(logOperateList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult<LogOperate> getLogOperateById(@PathVariable("id")String id){
		LogOperate logOperate = logOperateService.getLogOperateById(id);
		return outDataStr(logOperate);
	}
	/**
	* 添加
	* @param logOperate 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	@AuthUneedLogin
	public BaseResult addLogOperate(@RequestBody LogOperate logOperate){
		int i = 0;
		if(null != logOperate){
			logOperate.setId(toUUID());
			i=logOperateService.addLogOperate(logOperate);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLogOperate(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=logOperateService.delLogOperate(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
