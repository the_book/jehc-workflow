package jehc.djshi.log.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.log.model.LogModifyRecord;
import jehc.djshi.log.service.LogModifyRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @Desc 修改记录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:24:23
*/
@RestController
@RequestMapping("/logModifyRecord")
@Api(value = "修改记录日志API",tags = "修改记录日志API",description = "修改记录日志")
public class LogModifyRecordController extends BaseAction{
	@Autowired
	private LogModifyRecordService logModifyRecordService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LogModifyRecord>> getLogModifyRecordListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LogModifyRecord> logModifyRecordList = logModifyRecordService.getLogModifyRecordListByCondition(condition);
		PageInfo<LogModifyRecord> page = new PageInfo<LogModifyRecord>(logModifyRecordList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult<LogModifyRecord> getLogModifyRecordById(@PathVariable("id")String id){
		LogModifyRecord logModifyRecord = logModifyRecordService.getLogModifyRecordById(id);
		return outDataStr(logModifyRecord);
	}
	/**
	* 添加
	* @param logModifyRecord 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	@AuthUneedLogin
	public BaseResult addLogModifyRecord(@RequestBody LogModifyRecord logModifyRecord){
		int i = 0;
		if(null != logModifyRecord){
			logModifyRecord.setId(toUUID());
			i=logModifyRecordService.addLogModifyRecord(logModifyRecord);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLogModifyRecord(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=logModifyRecordService.delLogModifyRecord(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
