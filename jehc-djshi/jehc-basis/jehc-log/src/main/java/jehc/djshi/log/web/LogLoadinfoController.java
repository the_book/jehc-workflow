package jehc.djshi.log.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.log.model.LogLoadinfo;
import jehc.djshi.log.service.LogLoadinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @Desc 页面加载信息 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:22:05
*/
@RestController
@RequestMapping("/logLoadinfo")
@Api(value = "页面加载信息API",tags = "页面加载信息API",description = "页面加载信息")
public class LogLoadinfoController extends BaseAction{
	@Autowired
	private LogLoadinfoService logLoadinfoService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<LogLoadinfo>> getLogLoadinfoListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<LogLoadinfo> logLoadinfoList = logLoadinfoService.getLogLoadinfoListByCondition(condition);
		PageInfo<LogLoadinfo> page = new PageInfo<LogLoadinfo>(logLoadinfoList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult<LogLoadinfo> getLogLoadinfoById(@PathVariable("id")String id){
		LogLoadinfo logLoadinfo = logLoadinfoService.getLogLoadinfoById(id);
		return outDataStr(logLoadinfo);
	}
	/**
	* 添加
	* @param logLoadinfo
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	@AuthUneedLogin
	public BaseResult addLogLoadinfo(@RequestBody LogLoadinfo logLoadinfo){
		int i = 0;
		if(null != logLoadinfo){
			logLoadinfo.setId(toUUID());
			i=logLoadinfoService.addLogLoadinfo(logLoadinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delLogLoadinfo(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=logLoadinfoService.delLogLoadinfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
