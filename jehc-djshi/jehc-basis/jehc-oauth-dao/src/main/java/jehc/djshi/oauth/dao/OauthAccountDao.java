package jehc.djshi.oauth.dao;

import jehc.djshi.oauth.model.OauthAccount;

import java.util.List;
import java.util.Map;
/**
 * @Desc 授权中心账户
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthAccountDao {
    /**
     * 分页
     * @param condition
     * @return
     */
    List<OauthAccount> getOauthAccountList(Map<String,Object> condition);
    /**
     * 查询对象
     * @param xt_account_id
     * @return
     */
    OauthAccount getOauthAccountById(String xt_account_id);

    /**
     * 登录
     * @param condition
     * @return
     */
    OauthAccount login(Map<String,Object> condition);

    /**
     * 添加
     * @param oauthAccount
     * @return
     */
    int addOauthAccount(OauthAccount oauthAccount);
    /**
     * 修改（根据动态条件）
     * @param oauthAccount
     * @return
     */
    int updateOauthAccount(OauthAccount oauthAccount);

    /**
     * 冻结账户
     * @param oauthAccount
     * @return
     */
    int freezeAccount(OauthAccount oauthAccount);

    /**
     * 查询唯一账户
     * @param account
     * @return
     */
    OauthAccount getSingleOauthAccount(String account);

    /**
     *  查询账户集合
     * @param condition
     * @return
     */
    List<OauthAccount> infoList(Map<String,Object> condition);

    /**
     * 修改登录时间
     * @param oauthAccount
     */
    void updateLoginTime(OauthAccount oauthAccount);
}
