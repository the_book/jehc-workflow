package jehc.djshi.oauth.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.oauth.model.OauthFunctionRole;

/**
* 授权中心功能对角色 
* 2019-06-20 15:02:00  邓纯杰
*/
public interface OauthFunctionRoleDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthFunctionRole> getOauthFunctionRoleListByCondition(Map<String, Object> condition);

	/**
	* 添加
	* @param oauthFunctionRole 
	* @return
	*/
	int addOauthFunctionRole(OauthFunctionRole oauthFunctionRole);

	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthFunctionRole(Map<String, Object> condition);

}
