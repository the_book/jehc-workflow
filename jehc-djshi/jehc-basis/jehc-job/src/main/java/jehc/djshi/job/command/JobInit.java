package jehc.djshi.job.command;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import jehc.djshi.common.entity.ScheduleJob;
import lombok.Data;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 初始化加载调度任务
 * @author 邓纯杰
 *
 */
@Data
public class JobInit extends Thread{
	Logger log = LoggerFactory.getLogger(this.getClass());
	//private SchedulerFactoryBean schedulerFactoryBean;
	private Scheduler scheduler; 
	private String jobId;/**任务id*/
	private String jobTitle;//标题
	private String jobName;/**任务名称*/
	private String jobGroup;/**任务分组*/
	private String jobStatus;/**任务状态 0禁用 1启用 2删除*/
	private String cronExpression;/**任务运行时间表达式*/
	private String desc;/**任务描述*/
	private String clientId;/**客户端Id*/
	private String clientGroupId;/**客户端组Id**/
	private String jobHandler;/**注解事件**/
	private String jobPara;/**参数**/
	public JobInit(Scheduler scheduler, String jobId, String jobName, String jobGroup, String cronExpression, String desc, String clientId, String clientGroupId,String jobHandler,String jobPara,String jobTitle){
//	public JobInit(SchedulerFactoryBean schedulerFactoryBean,String jobId,String jobName,String jobGroup,String cronExpression,String desc,String clientId,String clientGroupId){
//		this.schedulerFactoryBean = schedulerFactoryBean;
		this.scheduler = scheduler;
		this.jobId = jobId;
		this.jobName = jobName;
		this.jobGroup = jobGroup;
		this.cronExpression=cronExpression;
		this.desc = desc;
		this.clientId=clientId;
		this.clientGroupId = clientGroupId;
		this.jobHandler = jobHandler;
		this.jobPara = jobPara;
		this.jobTitle = jobTitle;
	}
	public void run() {
		ScheduleJob job = new ScheduleJob();
		job.setCronExpression(getCronExpression());
		job.setDesc(getDesc());
		job.setJobGroup(getJobGroup());
		job.setJobId(getJobId());
		job.setJobTitle(getJobTitle());
		job.setJobName(getJobName());
		job.setClientId(getClientId());
		job.setClientGroupId(getClientGroupId());
		job.setJobHandler(getJobHandler());
		job.setJobPara(getJobPara());
		try {
			initQuartz(scheduler,job);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
    }
	public void initQuartz(Scheduler scheduler,ScheduleJob job) throws IOException, SchedulerException {
//	public void initQuartz(SchedulerFactoryBean schedulerFactoryBean,ScheduleJob job) throws IOException, SchedulerException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try{
//	        Scheduler scheduler = schedulerFactoryBean.getScheduler();
			//一定要紧跟Validate之后写验证结果类
			String seconds = job.getCronExpression();
			String cronExp = seconds;
			job.setCronExpression(cronExp);
			TriggerKey triggerKey = TriggerKey.triggerKey(job.getJobName(), job.getJobGroup());
			//获取trigger，即在spring配置文件中定义的 bean id="myTrigger"
			CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
			//不存在，创建一个
			if(null == trigger){
				JobDetail jobDetail = JobBuilder.newJob(JobFactory.class).withIdentity(job.getJobName(), job.getJobGroup()).build();
				jobDetail.getJobDataMap().put("scheduleJob", job);
				//表达式调度构建器
				CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());
				//按新的cronExpression表达式构建一个新的trigger
				trigger = TriggerBuilder.newTrigger().withIdentity(job.getJobName(), job.getJobGroup()).withSchedule(scheduleBuilder).build();
				scheduler.scheduleJob(jobDetail, trigger);
			}else{
				//Trigger已存在，那么更新相应的定时设置
				//表达式调度构建器
				CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());
				//按新的cronExpression表达式重新构建trigger
				trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
				//按新的trigger重新设置job执行
				scheduler.rescheduleJob(triggerKey, trigger);
			}
		}catch(Exception e){
			log.info(""+sdf.format(new Date())+"--->初始化启动调度任务失败...");
		}
	}

//	public SchedulerFactoryBean getSchedulerFactoryBean() {
//		return schedulerFactoryBean;
//	}
//	public void setSchedulerFactoryBean(SchedulerFactoryBean schedulerFactoryBean) {
//		this.schedulerFactoryBean = schedulerFactoryBean;
//	}
	public Scheduler getScheduler() {
		return scheduler;
	}
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}
}
