package jehc.djshi.job.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.job.model.JobLog;
import jehc.djshi.job.model.JobLog;

/**
* 调度器日志 
* 2016-05-25 20:16:23  邓纯杰
*/
public interface JobLogService {
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<JobLog> getJobLogListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param job_log_id
	* @return
	*/
	JobLog getJobLogById(String job_log_id);
	/**
	* 添加
	* @param jobLog
	* @return
	*/
	int addJobLog(JobLog jobLog);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delJobLog(Map<String, Object> condition);
}
