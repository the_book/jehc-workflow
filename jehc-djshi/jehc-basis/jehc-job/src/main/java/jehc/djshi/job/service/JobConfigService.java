package jehc.djshi.job.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.job.model.JobConfig;
import jehc.djshi.job.model.JobConfig;

/**
* 任务调度配置信息表 
* 2015-10-29 16:50:03  邓纯杰
*/
public interface JobConfigService {
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<JobConfig> getJobConfigListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	JobConfig getJobConfigById(String id);
	/**
	* 添加
	* @param jobConfig
	* @return
	*/
	int addJobConfig(JobConfig jobConfig);
	/**
	* 修改
	* @param jobConfig
	* @return
	*/
	int updateJobConfig(JobConfig jobConfig);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delJobConfig(Map<String, Object> condition);
	/**
	 * 查找集合
	 * @param condition
	 * @return
	 */
	List<JobConfig> getJobConfigListAllByCondition(Map<String, Object> condition);
}
