package jehc.djshi.job.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.job.model.JobLog;
import jehc.djshi.job.service.JobLogService;
import jehc.djshi.job.model.JobLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
/**
 * @Desc 调度器日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/jobLog")
@Api(value = "调度器日志API",tags = "调度器日志API",description = "调度器日志API")
public class JobLogController extends BaseAction {
	@Autowired
	private JobLogService jobLogService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value=" 查询并分页", notes=" 查询并分页")
	public BasePage<List<JobLog>> getJobListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition =  baseSearch.convert();
		commonHPager(baseSearch);
		List<JobLog> jobLogList = jobLogService.getJobLogListByCondition(condition);
		PageInfo<JobLog> page = new PageInfo<JobLog>(jobLogList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 获取对象
	* @param job_log_id
	*/
	@NeedLoginUnAuth
	@ApiOperation(value=" 查询单个对象", notes=" 查询单个对象")
	@GetMapping(value="/get/{job_log_id}")
	public BaseResult getJobLogById(@PathVariable("job_log_id")String job_log_id){
		JobLog jobLog = jobLogService.getJobLogById(job_log_id);
		return outDataStr(jobLog);
	}
	
	/**
	* 删除
	* @param job_log_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value=" 删除", notes=" 删除")
	public BaseResult delJobLog(String job_log_id){
		int i = 0;
		if(!StringUtil.isEmpty(job_log_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("job_log_id",job_log_id.split(","));
			i= jobLogService.delJobLog(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 添加
	 * @param jobLog
	 */
	@AuthUneedLogin
	@PostMapping(value="/add")
	@ApiOperation(value=" 添加", notes=" 添加")
	public BaseResult addJobLog(@RequestBody JobLog jobLog){
		int i = 0;
		if(null != jobLog){
			i= jobLogService.addJobLog(jobLog);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
