package jehc.djshi.oauth.client.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.client.service.PushService;
import jehc.djshi.oauth.client.vo.Token;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
/**
 * @Desc 推送
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
@Data
public class PushServiceImpl implements PushService {

    public Map<String,Token> expireTokenData = null;

    /**
     * 存放Token对象
     * @param key
     * @param v
     * @return
     */
    public boolean put(String key,Token v){
        boolean result = true;
        if(StringUtil.isEmpty(key)){
            throw new ExceptionUtil("The key is empty in PushServiceImpl");
        }
        if(null == v){
            throw new ExceptionUtil("The value is empty in PushServiceImpl");
        }
        if(CollectionUtil.isEmpty(expireTokenData)){
            expireTokenData = new ConcurrentHashMap<>();
        }
        try {
            Token token = expireTokenData.get(key);
            if(null != token){
                v.setTimes(token.getTimes()+1);
            }
            expireTokenData.put(key,v);
        }catch (Exception e){
            log.error("存放Token对象异常：{}",e);
            result = false;
        }
        return result;
    }

    /**
     * 删除Token
     * @param key
     * @return
     */
    public boolean remove(String key){
        boolean result = true;
        if(StringUtil.isEmpty(key)){
            throw new ExceptionUtil("The key is empty in PushServiceImpl");
        }
        try {
            expireTokenData.remove(key);
        }catch (Exception e){
            log.error("移除Token对象异常：{}",e);
            result = false;
        }
        return result;
    }

    /**
     *
     * @param key
     * @return
     */
    public Token get(String key){
        if(StringUtil.isEmpty(key)){
            throw new ExceptionUtil("The key is empty in PushServiceImpl");
        }
        return expireTokenData.get(key);
    }

    /**
     * 推送更新Token失效时间
     * @param token
     * @return
     */
    public boolean syncPushTokenExpire(String token){
        Token tk = new Token();
        return true;
    }

    public Map<String,Token> getTokenData(){
        return this.getExpireTokenData();
    }
}
