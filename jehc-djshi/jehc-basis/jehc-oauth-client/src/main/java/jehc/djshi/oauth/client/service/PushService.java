package jehc.djshi.oauth.client.service;

import jehc.djshi.oauth.client.vo.Token;

import java.util.Map;

/**
 * @Desc 推送
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface PushService {

    /**
     * 存放Token对象
     * @param key
     * @param v
     * @return
     */
    boolean put(String key,Token v);

    /**
     * 推送更新Token失效时间
     * @param token
     * @return
     */
    boolean syncPushTokenExpire(String token);

    /**
     * 删除Token
     * @param key
     * @return
     */
    boolean remove(String key);

    /**
     *
     * @param key
     * @return
     */
    Token get(String key);

    /**
     *
     * @return
     */
    Map<String,Token> getTokenData();
}
