package jehc.djshi.oauth.client.constant;
/**
 * @Desc 常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class Constant {
    public static final String SYS_MODE_KEY = "SysMode";

    public static final Integer KEEP_ALIVE_TIME = 10*60*60;

    public static final Integer EXPIRE_TOKEN_GROUP_NUMBER = 100;//Token有效期分批处理数量


    public static final String OAUTH_CHANGED = "OAUTH_CHANGED";//权限变更
    public static final String TOKEN_DESTORY = "TOKEN_DESTORY";//Token销毁
    public static final Integer PULL_TOKEN_GROUP_NUMBER = 100;//处理拉取TOKEN分批处理数量

    public static final String LOG_PATH = System.getProperty("user.home")+"/logs/oauth";//日志路径
}
