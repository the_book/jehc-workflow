package jehc.djshi.oauth.client.util;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Data
@Component
public class HeartbeatAttributesUtil {
    private boolean connected = true;
}
