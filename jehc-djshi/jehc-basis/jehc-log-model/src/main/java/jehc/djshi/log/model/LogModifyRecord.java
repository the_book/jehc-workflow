package jehc.djshi.log.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
* @Desc 修改记录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:24:23
*/
@Data
@ApiModel(value="修改记录日志对象", description="修改记录日志")
public class LogModifyRecord extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "id")
	private String id;/**id**/

	@ApiModelProperty(value = "更变前值")
	private String before_value;/**更变前值**/

	@ApiModelProperty(value = "变更后值")
	private String after_value;/**变更后值**/

	@ApiModelProperty(value = "模块")
	private String modules;/**模块**/

	@ApiModelProperty(value = "字段")
	private String field;/**字段**/

	@ApiModelProperty(value = "业务编号")
	private String business_id;/**业务编号**/

	@ApiModelProperty(value = "批次")
	private String batch;/**批次**/
}
