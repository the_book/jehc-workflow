package jehc.djshi.log.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
* @Desc 异常日志表 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:24:05
*/
@Data
@ApiModel(value="异常日志对象", description="异常日志")
public class LogError extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "id")
	private String id;/**异常ID**/

	@ApiModelProperty(value = "异常日志内容")
	private String content;/**异常日志内容**/

	@ApiModelProperty(value = "异常日志级别")
	private Integer type;/**异常日志级别**/
}
