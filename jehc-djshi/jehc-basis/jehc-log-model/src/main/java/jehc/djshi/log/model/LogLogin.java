package jehc.djshi.log.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
* @Desc 登录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:21:01
*/
@Data
@ApiModel(value="登录日志对象", description="登录日志")
public class LogLogin extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "id")
	private String id;/**id**/

	@ApiModelProperty(value = "登录ip")
	private String ip;/**登录ip**/

	@ApiModelProperty(value = "内容")
	private String content;/**内容**/

	@ApiModelProperty(value = "浏览器类型")
	private String browser_type;/**浏览器类型**/

	@ApiModelProperty(value = "浏览器名称")
	private String browser_name;/**浏览器名称**/

	@ApiModelProperty(value = "浏览器版本")
	private String browser_version;/**浏览器版本**/

	@ApiModelProperty(value = "操作系统")
	private String system;/**操作系统**/
}
