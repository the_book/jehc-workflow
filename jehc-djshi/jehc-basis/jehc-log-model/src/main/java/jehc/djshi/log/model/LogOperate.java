package jehc.djshi.log.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
* @Desc 操作日志表
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:27:00
*/
@Data
@ApiModel(value="操作日志对象", description="操作日志")
public class LogOperate extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "id")
	private String id;/**id**/

	@ApiModelProperty(value = "开始时间")
	private Long begin_time;/**开始时间**/

	@ApiModelProperty(value = "结束时间")
	private Long end_time;/**结束时间**/

	@ApiModelProperty(value = "执行类名")
	private String class_name;/**执行类名**/

	@ApiModelProperty(value = "模块")
	private String modules;/**模块**/

	@ApiModelProperty(value = "执行方式（POST，GET,PUT,DELETE等等）")
	private String method;/**执行方式（POST，GET,PUT,DELETE等等）**/

	@ApiModelProperty(value = "方法参数")
	private String param;/**方法参数**/

	@ApiModelProperty(value = "执行结果")
	private String result;/**执行结果**/

	@ApiModelProperty(value = "执行总时间")
	private String total_time;/**执行总时间**/

	@ApiModelProperty(value = "访问方法")
	private String action_;/**访问方法**/

	@ApiModelProperty(value = "访问地址")
	private String uri;/**访问地址**/

	@ApiModelProperty(value = "最大内存")
	private Integer max_memory;/**最大内存**/

	@ApiModelProperty(value = "已分配内存")
	private Integer total_memory;/**已分配内存**/

	@ApiModelProperty(value = "已分配内存中的剩余空间")
	private Integer free_memory;/**已分配内存中的剩余空间**/

	@ApiModelProperty(value = "最大可用内存")
	private Integer use_memory;/**最大可用内存**/

	@ApiModelProperty(value = "批次")
	private String batch;/**批次**/

	@ApiModelProperty(value = "类型：0业务1参数拦截")
	private Integer type;/**类型：0业务1参数拦截**/
}
