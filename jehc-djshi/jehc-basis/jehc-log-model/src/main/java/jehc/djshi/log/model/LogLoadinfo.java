package jehc.djshi.log.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
/**
* @Desc 页面加载信息 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:22:05
*/
@Data
@ApiModel(value="页面加载信息对象", description="页面加载信息")
public class LogLoadinfo extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "id")
	private String id;/**id**/

	@ApiModelProperty(value = "载加模块")
	private String modules;/**载加模块**/

	@ApiModelProperty(value = "载入时间")
	private Long begtime;/**载入时间**/

	@ApiModelProperty(value = "载入结束时间")
	private Long endtime;/**载入结束时间**/
}
