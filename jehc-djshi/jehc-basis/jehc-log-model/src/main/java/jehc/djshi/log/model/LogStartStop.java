package jehc.djshi.log.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
* @Desc 服务器启动与关闭日志
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:28:59
*/
@Data
@ApiModel(value="服务器启动与关闭日志对象", description="服务器启动与关闭日志")
public class LogStartStop extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "id")
	private String id;/**主键**/

	@ApiModelProperty(value = "是否出错0正常1错误")
	private String error;/**是否出错0正常1错误**/

	@ApiModelProperty(value = "加载内容")
	private String content;/**加载内容**/
}
