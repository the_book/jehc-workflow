package jehc.djshi.oauth.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.oauth.model.OauthAdmin;
import jehc.djshi.oauth.service.OauthAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 授权中心管理员API
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthAdmin")
@Api(value = "授权中心管理员API",tags = "授权中心管理员API",description = "授权中心管理员API")
public class OauthAdminController extends BaseAction {
    @Autowired
    private OauthAdminService oauthAdminService;
    /**
     * 读取全部数据
     * @param sys_mode_id
     */
    @NeedLoginUnAuth
    @PostMapping(value="/listAll")
    @ApiOperation(value="查询管理员列表并分页", notes="查询管理员列表并分页")
    public BaseResult<List<OauthAdmin>> getOauthAccountListByCondition(String sys_mode_id){
        Map<String, Object> condition = new HashMap<>();
        condition.put("sysmode_id",sys_mode_id);
        return outDataStr(oauthAdminService.getOauthAdminListAll(condition));
    }

    /**
     * 添加
     * @param oauthAdmin
     */
    @PostMapping(value="/add")
    @ApiOperation(value="创建单个管理员", notes="创建单个管理员")
    public BaseResult addAdminSys(@RequestBody OauthAdmin oauthAdmin){
        if(null != oauthAdmin){
            oauthAdmin.setId(toUUID());
            int i = oauthAdminService.addOauthAdmin(oauthAdmin);
            if(i>0){
                return outAudStr(true);
            }else{
                return outAudStr(false);
            }
        }else {
            throw new ExceptionUtil("OauthAdmin");
        }
    }

    /**
     * 移除模块系统中管理员
     * @param oauthAdmin
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="移除模块系统中管理员", notes="移除模块系统中管理员")
    public BaseResult delOauthAdminSys(OauthAdmin oauthAdmin){
        int i = 0;
        if(null != oauthAdmin){
            i=oauthAdminService.delOauthAdmin(oauthAdmin);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
}
