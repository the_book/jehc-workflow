package jehc.djshi.oauth.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseHttpSessionEntity;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.entity.OauthAdminSysEntity;
import jehc.djshi.common.session.HttpSessionUtils;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.oauth.util.OauthUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * @Desc 授权中心通用API
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@Api(value = "授权中心通用API",tags = "授权中心通用API",description = "授权中心通用API")
public class CommonController extends BaseAction {
    ///////////////获取用户信息相关/////////////////////
    @Autowired
    private OauthUtil oauthUtil;
    @Autowired
    HttpSessionUtils httpSessionUtils;
    /**
     * 当前登录者姓名
     * @param request
     * @return
     */
    @ApiOperation(value="当前登录者姓名", notes="当前登录者姓名")
    @GetMapping(value="/uName")
    @AuthUneedLogin
    public BaseResult getUname(HttpServletRequest request) {
        try {
            String baseHttpSessionEntityJson = oauthUtil.getTokenInfo(request);
            if(StringUtils.isEmpty(baseHttpSessionEntityJson)){
                return null;
            }
            BaseHttpSessionEntity baseHttpSessionEntity = JsonUtil.fromAliFastJson(baseHttpSessionEntityJson, BaseHttpSessionEntity.class);
            if(null != baseHttpSessionEntity){
                OauthAccountEntity oauthAccountEntity = baseHttpSessionEntity.getOauthAccountEntity();
                if (null != oauthAccountEntity) {
                    return outDataStr(oauthAccountEntity.getName());
                }
            }
        } catch (Exception e) {
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return null;
    }

    /**
     * 当前登录账户
     * @param request
     * @return
     */
    @GetMapping(value="/account")
    @AuthUneedLogin
    @ApiOperation(value="当前登录者账号", notes="当前登录者账号")
    public BaseResult getAccount(HttpServletRequest request) {
        try {
            String baseHttpSessionEntityJson = oauthUtil.getTokenInfo(request);
            if(StringUtils.isEmpty(baseHttpSessionEntityJson)){
                return null;
            }
            BaseHttpSessionEntity baseHttpSessionEntity = JsonUtil.fromAliFastJson(baseHttpSessionEntityJson, BaseHttpSessionEntity.class);
            if(null != baseHttpSessionEntity){
                OauthAccountEntity oauthAccountEntity = baseHttpSessionEntity.getOauthAccountEntity();
                if (null != oauthAccountEntity) {
                    return outDataStr(oauthAccountEntity.getAccount());
                }
            }
        } catch (Exception e) {
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return null;
    }

    /**
     * 当前账号编号
     * @param request
     * @return
     */
    @GetMapping(value="/accountId")
    @AuthUneedLogin
    @ApiOperation(value="当前登录者编号", notes="当前登录者编号")
    public BaseResult getAccountId(HttpServletRequest request) {
        try {
            String baseHttpSessionEntityJson = oauthUtil.getTokenInfo(request);
            if(StringUtils.isEmpty(baseHttpSessionEntityJson)){
                return null;
            }
            BaseHttpSessionEntity baseHttpSessionEntity = JsonUtil.fromAliFastJson(baseHttpSessionEntityJson, BaseHttpSessionEntity.class);
            if(null != baseHttpSessionEntity){
                OauthAccountEntity oauthAccountEntity = baseHttpSessionEntity.getOauthAccountEntity();
                if(null != oauthAccountEntity){
                    return outDataStr(oauthAccountEntity.getAccount_id());
                }
            }
        } catch (Exception e) {
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return null;
    }

    /**
     * 获取当前用户对象信息
     *
     * @return
     */
    @GetMapping(value="httpSessionEntity")
    @NeedLoginUnAuth
    @ApiOperation(value="获取当前用户对象信息", notes="获取当前用户对象信息")
    public BaseHttpSessionEntity getBaseHttpSessionEntity() {
        try {
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            String baseHttpSessionEntityJson = oauthUtil.getTokenInfo(request);
            if(StringUtils.isEmpty(baseHttpSessionEntityJson)){
                return new BaseHttpSessionEntity();
            }
            BaseHttpSessionEntity baseHttpSessionEntity = JsonUtil.fromAliFastJson(baseHttpSessionEntityJson, BaseHttpSessionEntity.class);
            return baseHttpSessionEntity;
        } catch (Exception e) {
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
    }

    /**
     * 获取当前用户对象信息
     *
     * @return
     */
    @GetMapping(value="httpSessionEntity/{token}")
    @NeedLoginUnAuth
    @ApiOperation(value="获取当前用户对象信息", notes="获取当前用户对象信息")
    public BaseHttpSessionEntity getBaseHttpSessionEntity(@PathVariable("token") String token) {
        try {
            String baseHttpSessionEntityJson = oauthUtil.getTokenInfo(token);
            if(StringUtils.isEmpty(baseHttpSessionEntityJson)){
                return new BaseHttpSessionEntity();
            }
            BaseHttpSessionEntity baseHttpSessionEntity = JsonUtil.fromAliFastJson(baseHttpSessionEntityJson, BaseHttpSessionEntity.class);
            return baseHttpSessionEntity;
        } catch (Exception e) {
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
    }

    /**
     * 当前登录者信息
     * @return
     */
    @NeedLoginUnAuth
    @GetMapping(value="/accountInfo")
    @ApiOperation(value="当前登录者信息", notes="当前登录者信息")
    public OauthAccountEntity account() {
        try {
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            String baseHttpSessionEntityJson = oauthUtil.getTokenInfo(request);
            if(StringUtils.isEmpty(baseHttpSessionEntityJson)){
                return null;
            }
            BaseHttpSessionEntity baseHttpSessionEntity = JsonUtil.fromAliFastJson(baseHttpSessionEntityJson, BaseHttpSessionEntity.class);
            if(null != baseHttpSessionEntity){
                return baseHttpSessionEntity.getOauthAccountEntity();
            }
        } catch (Exception e) {
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return null;
    }

    /**
     * 获取数据权限
     * @return
     */
    @AuthUneedLogin
    @GetMapping(value="/systemUandM")
    @ApiOperation(value="获取数据权限", notes="获取数据权限")
    public List<String> systemUandM(){
        try {
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
            String baseHttpSessionEntityJson = oauthUtil.getTokenInfo(request);
            if(StringUtils.isEmpty(baseHttpSessionEntityJson)){
                return null;
            }
            BaseHttpSessionEntity baseHttpSessionEntity = JsonUtil.fromAliFastJson(baseHttpSessionEntityJson, BaseHttpSessionEntity.class);
            if(null != baseHttpSessionEntity){
                return baseHttpSessionEntity.getSystemAM();
            }
        } catch (Exception e) {
            throw new ExceptionUtil("获取systemUandM出现异常："+e.getMessage());
        }
        return null;
    }

    /**
     * 判断当前账号是否为超级管理员
     * @return
     */
    @AuthUneedLogin
    @GetMapping(value="/isAdmin")
    @ApiOperation(value="判断当前账号是否为超级管理员", notes="判断当前账号是否为超级管理员")
    public boolean isAdmin() {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
        String baseHttpSessionEntityJson = oauthUtil.getTokenInfo(request);
        if(StringUtils.isEmpty(baseHttpSessionEntityJson)){
            return false;
        }
        BaseHttpSessionEntity baseHttpSessionEntity = JsonUtil.fromAliFastJson(baseHttpSessionEntityJson, BaseHttpSessionEntity.class);
        if(null != baseHttpSessionEntity){
            OauthAccountEntity oauthAccountEntity = baseHttpSessionEntity.getOauthAccountEntity();
            List<OauthAdminSysEntity> oauthAdminSysEntities = JsonUtil.toFList(baseHttpSessionEntity.getOauthAdminSysEntities(), OauthAdminSysEntity.class);
            if (null != oauthAccountEntity && null != oauthAdminSysEntities && !oauthAdminSysEntities.isEmpty()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}
