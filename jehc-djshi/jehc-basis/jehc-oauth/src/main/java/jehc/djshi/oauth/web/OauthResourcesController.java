package jehc.djshi.oauth.web;
import java.util.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthNeedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.*;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.model.OauthFunctionInfo;
import jehc.djshi.oauth.service.OauthFunctionInfoService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jehc.djshi.oauth.model.OauthResources;
import jehc.djshi.oauth.service.OauthResourcesService;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * @Desc 授权中心资源中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthResources")
@Api(value = "授权中心资源中心API",tags = "授权中心资源中心API",description = "授权中心资源中心API")
public class OauthResourcesController extends BaseAction {
	@Autowired
	private OauthResourcesService oauthResourcesService;

	@Autowired
	private OauthFunctionInfoService oauthFunctionInfoService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询资源中心列表并分页", notes="查询资源中心列表并分页")
	public BasePage<List<OauthResources>> getOauthResourcesListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<OauthResources> oauthResourcesList = oauthResourcesService.getOauthResourcesListByCondition(condition);
		for(OauthResources oauthResources:oauthResourcesList){
			if(!StringUtil.isEmpty(oauthResources.getCreate_id())){
				OauthAccountEntity createBy = getAccount(oauthResources.getCreate_id());
				if(null != createBy){
					oauthResources.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(oauthResources.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(oauthResources.getUpdate_id());
				if(null != modifiedBy){
					oauthResources.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<OauthResources> page = new PageInfo<OauthResources>(oauthResourcesList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 读取所有资源列表
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/all/list")
	@ApiOperation(value="查询全部资源中心列表", notes="查询全部资源中心列表")
	public BaseResult getOauthResourcesListAll(String sys_mode_id){
		//采用BTree操作模式
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("sysmode_id",sys_mode_id);
		List<BaseBTreeGridEntity> list = new ArrayList<BaseBTreeGridEntity>();
		List<OauthFunctionInfo> oauthFunctionInfoList = oauthFunctionInfoService.getOauthFunctionInfoList(condition);
		List<OauthResources> oauthResourcesList = oauthResourcesService.getOauthResourcesListAll(condition);
		for(int j = 0; j < oauthResourcesList.size(); j++){
			OauthResources oauthResources = oauthResourcesList.get(j);
			BaseBTreeGridEntity baseBTreeGridEntity = new BaseBTreeGridEntity();
			baseBTreeGridEntity.setId(oauthResources.getResources_id());
			baseBTreeGridEntity.setPid(oauthResources.getResources_parentid());
			baseBTreeGridEntity.setText(oauthResources.getResources_title());
			baseBTreeGridEntity.setTempObject("Sources");
			list.add(baseBTreeGridEntity);
		}
		for(int i = 0; i < oauthFunctionInfoList.size(); i++){
			OauthFunctionInfo oauthFunctionInfo = oauthFunctionInfoList.get(i);
			BaseBTreeGridEntity baseBTreeGridEntity = new BaseBTreeGridEntity();
			baseBTreeGridEntity.setId(oauthFunctionInfo.getFunction_info_id());
			baseBTreeGridEntity.setPid(oauthFunctionInfo.getMenu_id());
			baseBTreeGridEntity.setText(oauthFunctionInfo.getFunction_info_name());
			baseBTreeGridEntity.setIcon("/deng/images/icons/target_point.png");
			baseBTreeGridEntity.setTempObject("Function");
			baseBTreeGridEntity.setContent(""+oauthFunctionInfo.getFunction_info_name());
			baseBTreeGridEntity.setIntegerappend(oauthFunctionInfo.getIsAuthority()+","+oauthFunctionInfo.getIsfilter());
			list.add(baseBTreeGridEntity);
		}
		return outStr(BaseBTreeGridEntity.buildTreeF(list));
	}

	/**
	* 查询单个资源中心
	* @param resources_id 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{resources_id}")
	@ApiOperation(value="查询单个资源中心", notes="查询单个资源中心")
	public BaseResult<OauthResources> getOauthResourcesById(@PathVariable("resources_id")String resources_id){
		OauthResources oauthResources = oauthResourcesService.getOauthResourcesById(resources_id);
		if(!StringUtil.isEmpty(oauthResources.getResources_parentid())){
			OauthResources resources = oauthResourcesService.getOauthResourcesById(oauthResources.getResources_parentid());
			if(null != resources){
				oauthResources.setResources_parentidTitle_(resources.getResources_title());
			}
		}

		return outDataStr(oauthResources);
	}
	/**
	* 添加
	* @param oauthResources 
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个资源中心", notes="创建单个资源中心")
	public BaseResult addOauthResources(@RequestBody OauthResources oauthResources){
		int i = 0;
		if(null != oauthResources){
			oauthResources.setResources_id(toUUID());
			i=oauthResourcesService.addOauthResources(oauthResources);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param oauthResources 
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个资源中心", notes="编辑单个资源中心")
	public BaseResult updateOauthResources(@RequestBody OauthResources oauthResources){
		int i = 0;
		if(null != oauthResources){
			i=oauthResourcesService.updateOauthResources(oauthResources);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param resources_id 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除资源中心", notes="删除资源中心")
	public BaseResult delOauthResources(String resources_id){
		int i = 0;
		if(!StringUtil.isEmpty(resources_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("resources_id",resources_id.split(","));
			i=oauthResourcesService.delOauthResources(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}


	/**
	 * 读取所有资源列表（Bztree风格）
	 * @param request
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/bZTree")
	@ApiOperation(value="查询全部资源中心（树列表）", notes="查询全部资源中心（树列表）")
	public BaseResult getOauthResourcesBZTree(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("sysmode_id",request.getParameter("sysmode_id"));
		String expanded = request.getParameter("expanded");
		String singleClickExpand = request.getParameter("singleClickExpand");
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		List<OauthResources> oauthResourcesList = oauthResourcesService.getOauthResourcesListAll(condition);
		for(int i = 0; i < oauthResourcesList.size(); i++){
			OauthResources oauthResources = oauthResourcesList.get(i);
			BaseZTreeEntity baseZTreeEntity = new BaseZTreeEntity();
			baseZTreeEntity.setId(oauthResources.getResources_id());
			baseZTreeEntity.setPId(oauthResources.getResources_parentid());
			baseZTreeEntity.setText(oauthResources.getResources_title());
			baseZTreeEntity.setName(oauthResources.getResources_title());
			baseZTreeEntity.setIcon("/deng/images/icons/target_point.png");
//			if("0".equals(oauthResources.getResources_leaf())){
//				baseZTreeEntity.setHasLeaf(false);
//			}else{
//				baseZTreeEntity.setHasLeaf(true);
//			}
			if(("true").equals(expanded)){
				baseZTreeEntity.setExpanded(true);
			}else{
				baseZTreeEntity.setExpanded(false);
			}
			if("true".equals(singleClickExpand)){
				baseZTreeEntity.setSingleClickExpand(true);
			}else{
				baseZTreeEntity.setSingleClickExpand(false);
			}
			list.add(baseZTreeEntity);
		}
		BaseZTreeEntity baseZTreeEntity = new BaseZTreeEntity();
		List<BaseZTreeEntity> baseZTreeEntityList = baseZTreeEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseZTreeEntityList);
		return outStr(json);
//		return outStr(BaseZTreeEntity.buildTree(list,false));
	}

	/**
	 * 判断菜单下面是否有功能
	 * @param oauthFunctionInfoList
	 * @param resources_id
	 * @return
	 */
	public boolean hasLeaf(List<OauthFunctionInfo> oauthFunctionInfoList,String resources_id){
		boolean flag = true;
		for(int i = 0; i < oauthFunctionInfoList.size(); i++){
			if(oauthFunctionInfoList.get(i).getMenu_id().equals(resources_id)){
				return true;
			}
		}
		flag = false;
		return flag;
	}

	/**
	 * 设置为一级资源
	 * @param resources_id
	 * @return
	 */
	@GetMapping(value="/chMenuInfo")
	@ApiOperation(value="设置为一级资源", notes="设置为一级资源")
	public BaseResult chMenuInfo(String resources_id){
		int i = 0;
		OauthResources oauthResources = oauthResourcesService.getOauthResourcesById(resources_id);
		if(null != oauthResources){
			oauthResources.setResources_parentid("0");
			i = oauthResourcesService.updateOauthResources(oauthResources);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 导出资源
	 * @param response
	 * @param resources_id
	 */
	@ApiOperation(value="导出资源", notes="导出资源")
	@AuthNeedLogin
	@GetMapping(value = "/export")
	public void exportOauthResources(HttpServletResponse response, String resources_id) {
		oauthResourcesService.exportOauthResources(response,resources_id);
	}

	/**
	 * 导入资源
	 * @param request
	 * @return
	 */
	@PostMapping(value = "/import")
	@AuthNeedLogin
	@ApiOperation(value="导入资源", notes="导入资源")
	public BaseResult importOauthResources(HttpServletRequest request,HttpServletResponse response) {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;//转型为MultipartHttpRequest：
		MultiValueMap<String, MultipartFile> multipartFileMultiValueMap =  multipartRequest.getMultiFileMap();//获取所有文件
		Iterator<Map.Entry<String, List<MultipartFile>>> iterator = multipartFileMultiValueMap.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, List<MultipartFile>> entry = iterator.next();
			for (MultipartFile multipartFile : entry.getValue()) {
				oauthResourcesService.importOauthResources(request,response,multipartFile);
			}
		}
		return BaseResult.success();
	}
}
