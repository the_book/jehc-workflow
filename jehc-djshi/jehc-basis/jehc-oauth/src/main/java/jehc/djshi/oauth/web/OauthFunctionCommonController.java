package jehc.djshi.oauth.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.idgeneration.UUID;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.model.OauthFunctionCommon;
import jehc.djshi.oauth.service.OauthFunctionCommonService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import com.github.pagehelper.PageInfo;
/**
 * @Desc 授权中心公共功能
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthFunctionCommon")
@Api(value = "授权中心公共功能API",tags = "授权中心公共功能API",description = "授权中心公共功能API")
public class OauthFunctionCommonController extends BaseAction {
	@Autowired
	private OauthFunctionCommonService oauthFunctionCommonService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch 
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询公共功能列表并分页", notes="查询公共功能列表并分页")
	public BasePage<List<OauthFunctionCommon>> getOauthFunctionCommonListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<OauthFunctionCommon> oauthFunctionCommonList = oauthFunctionCommonService.getOauthFunctionCommonListByCondition(condition);
		for(OauthFunctionCommon oauthFunctionCommon:oauthFunctionCommonList){
			if(!StringUtil.isEmpty(oauthFunctionCommon.getCreate_id())){
				OauthAccountEntity createBy = getAccount(oauthFunctionCommon.getCreate_id());
				if(null != createBy){
					oauthFunctionCommon.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(oauthFunctionCommon.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(oauthFunctionCommon.getUpdate_id());
				if(null != modifiedBy){
					oauthFunctionCommon.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<OauthFunctionCommon> page = new PageInfo<OauthFunctionCommon>(oauthFunctionCommonList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个公共功能
	* @param function_common_id 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{function_common_id}")
	@ApiOperation(value="查询单个公共功能", notes="查询单个公共功能")
	public BaseResult<OauthFunctionCommon> getOauthFunctionCommonById(@PathVariable("function_common_id")String function_common_id){
		OauthFunctionCommon oauthFunctionCommon = oauthFunctionCommonService.getOauthFunctionCommonById(function_common_id);
		if(!StringUtil.isEmpty(oauthFunctionCommon.getCreate_id())){
			OauthAccountEntity createBy = getAccount(oauthFunctionCommon.getCreate_id());
			if(null != createBy){
				oauthFunctionCommon.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(oauthFunctionCommon.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(oauthFunctionCommon.getUpdate_id());
			if(null != modifiedBy){
				oauthFunctionCommon.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(oauthFunctionCommon);
	}
	/**
	* 添加
	* @param oauthFunctionCommon 
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个公共功能", notes="创建单个公共功能")
	public BaseResult addOauthFunctionCommon(@RequestBody OauthFunctionCommon oauthFunctionCommon){
		int i = 0;
		if(null != oauthFunctionCommon){
			oauthFunctionCommon.setFunction_common_id(UUID.toUUID());
			oauthFunctionCommon.setCreate_time(super.getDate());
			i=oauthFunctionCommonService.addOauthFunctionCommon(oauthFunctionCommon);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param oauthFunctionCommon 
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个公共功能", notes="编辑单个公共功能")
	public BaseResult updateOauthFunctionCommon(@RequestBody OauthFunctionCommon oauthFunctionCommon){
		int i = 0;
		if(null != oauthFunctionCommon){
			oauthFunctionCommon.setUpdate_time(super.getDate());
			i=oauthFunctionCommonService.updateOauthFunctionCommon(oauthFunctionCommon);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param function_common_id 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除公共功能", notes="删除公共功能")
	public BaseResult delOauthFunctionCommon(String function_common_id){
		int i = 0;
		if(!StringUtil.isEmpty(function_common_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("function_common_id",function_common_id.split(","));
			i=oauthFunctionCommonService.delOauthFunctionCommon(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
