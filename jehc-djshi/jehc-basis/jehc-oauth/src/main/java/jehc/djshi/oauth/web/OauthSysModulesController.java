package jehc.djshi.oauth.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.model.OauthSysModules;
import jehc.djshi.oauth.service.OauthSysModulesService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import com.github.pagehelper.PageInfo;
/**
 * @Desc 授权中心子系统模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthSysModules")
@Api(value = "授权中心子系统模块API",tags = "授权中心子系统模块API",description = "授权中心子系统模块API")
public class OauthSysModulesController extends BaseAction {
	@Autowired
	private OauthSysModulesService oauthSysModulesService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch 
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询子系统模块列表并分页", notes="查询子系统模块列表并分页")
	public BasePage<List<OauthSysModules>> getOauthSysModulesListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<OauthSysModules> oauthSysModulesList = oauthSysModulesService.getOauthSysModulesListByCondition(condition);
		for(OauthSysModules oauthSysModules:oauthSysModulesList){
			if(!StringUtil.isEmpty(oauthSysModules.getCreate_id())){
				OauthAccountEntity createBy = getAccount(oauthSysModules.getCreate_id());
				if(null != createBy){
					oauthSysModules.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(oauthSysModules.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(oauthSysModules.getUpdate_id());
				if(null != modifiedBy){
					oauthSysModules.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<OauthSysModules> page = new PageInfo<OauthSysModules>(oauthSysModulesList);
		return outPageBootStr(page,baseSearch);
	}

	@NeedLoginUnAuth
	@PostMapping(value="/list/{sysmode_id}")
	@ApiOperation(value="查询子系统模块列表", notes="查询子系统模块列表")
	public BaseResult<List<OauthSysModules>> getOauthSysModulesListBySysId(@PathVariable("sysmode_id")String sysmode_id){
		Map<String,Object> condition = new HashMap<>();
		condition.put("sysmode_id",sysmode_id.split(","));
		List<OauthSysModules> oauthSysModulesList = oauthSysModulesService.getOauthSysModulesListByCondition(condition);
		return new BaseResult(oauthSysModulesList);
	}

	/**
	 * 全部模块
	 * @return
	 */
	@NeedLoginUnAuth
	@PostMapping(value="/listAll")
	@ApiOperation(value="查询全部模块", notes="查询全部模块")
	public BaseResult<List<OauthSysModules>> getOauthSysModulesList(){
		Map<String,Object> condition = new HashMap<>();
		List<OauthSysModules> oauthSysModulesList = oauthSysModulesService.getOauthSysModulesListByCondition(condition);
		return outDataStr(oauthSysModulesList);
	}

	/**
	* 查询单个子系统模块
	* @param sys_modules_id 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{sys_modules_id}")
	@ApiOperation(value="查询单个子系统模块", notes="查询单个子系统模块")
	public BaseResult<OauthSysModules> getOauthSysModulesById(@PathVariable("sys_modules_id")String sys_modules_id){
		OauthSysModules oauthSysModules = oauthSysModulesService.getOauthSysModulesById(sys_modules_id);
		if(!StringUtil.isEmpty(oauthSysModules.getCreate_id())){
			OauthAccountEntity createBy = getAccount(oauthSysModules.getCreate_id());
			if(null != createBy){
				oauthSysModules.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(oauthSysModules.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(oauthSysModules.getUpdate_id());
			if(null != modifiedBy){
				oauthSysModules.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(oauthSysModules);
	}
	/**
	* 添加
	* @param oauthSysModules 
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个子系统模块", notes="创建单个子系统模块")
	public BaseResult addOauthSysModules(@RequestBody OauthSysModules oauthSysModules){
		int i = 0;
		if(null != oauthSysModules){
			oauthSysModules.setSys_modules_id(toUUID());
			oauthSysModules.setCreate_time(getDate());
			i=oauthSysModulesService.addOauthSysModules(oauthSysModules);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param oauthSysModules 
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个子系统模块", notes="编辑单个子系统模块")
	public BaseResult updateOauthSysModules(@RequestBody OauthSysModules oauthSysModules){
		int i = 0;
		if(null != oauthSysModules){
			oauthSysModules.setUpdate_time(getDate());
			i=oauthSysModulesService.updateOauthSysModules(oauthSysModules);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param sys_modules_id 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除子系统模块", notes="删除子系统模块")
	public BaseResult delOauthSysModules(String sys_modules_id){
		int i = 0;
		if(!StringUtil.isEmpty(sys_modules_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("sys_modules_id",sys_modules_id.split(","));
			i=oauthSysModulesService.delOauthSysModules(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
