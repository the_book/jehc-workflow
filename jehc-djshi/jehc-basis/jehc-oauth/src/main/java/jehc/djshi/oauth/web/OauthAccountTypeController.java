package jehc.djshi.oauth.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.idgeneration.UUID;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.model.OauthAccountType;
import jehc.djshi.oauth.service.OauthAccountTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 授权中心账户类型API
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthAccountType")
@Api(value = "授权中心账户类型API",tags = "授权中心账户类型API",description = "授权中心账户类型API")
public class OauthAccountTypeController extends BaseAction {
    @Autowired
    OauthAccountTypeService oauthAccountTypeService;


    /**
     * 加载初始化列表数据并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询账户类型集合并分页", notes="查询账户类型集合并分页")
    public BasePage<List<OauthAccountType>> getOauthAccountTypeList(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<OauthAccountType> oauthAccountTypeList = oauthAccountTypeService.getOauthAccountTypeList(condition);
        for(OauthAccountType oauthAccountType:oauthAccountTypeList){
            if(!StringUtil.isEmpty(oauthAccountType.getCreate_id())){
                OauthAccountEntity createBy = getAccount(oauthAccountType.getCreate_id());
                if(null != createBy){
                    oauthAccountType.setCreateBy(createBy.getName());
                }
            }
            if(!StringUtil.isEmpty(oauthAccountType.getUpdate_id())){
                OauthAccountEntity modifiedBy = getAccount(oauthAccountType.getUpdate_id());
                if(null != modifiedBy){
                    oauthAccountType.setModifiedBy(modifiedBy.getName());
                }
            }
        }
        PageInfo<OauthAccountType> page = new PageInfo<OauthAccountType>(oauthAccountTypeList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个账户类型
     * @param account_type_id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{account_type_id}")
    @ApiOperation(value="查询单个账户类型", notes="根据Id查找对象")
    public BaseResult<OauthAccountType> getOauthAccountTypeById(@PathVariable("account_type_id")String account_type_id){
        OauthAccountType oauthAccountType = oauthAccountTypeService.getOauthAccountTypeById(account_type_id);
        if(!StringUtil.isEmpty(oauthAccountType.getCreate_id())){
            OauthAccountEntity createBy = getAccount(oauthAccountType.getCreate_id());
            if(null != createBy){
                oauthAccountType.setCreateBy(createBy.getName());
            }
        }
        if(!StringUtil.isEmpty(oauthAccountType.getUpdate_id())){
            OauthAccountEntity modifiedBy = getAccount(oauthAccountType.getUpdate_id());
            if(null != modifiedBy){
                oauthAccountType.setModifiedBy(modifiedBy.getName());
            }
        }
        return outDataStr(oauthAccountType);
    }

    /**
     * 添加
     * @param oauthAccountType
     */
    @PostMapping(value="/add")
    @ApiOperation(value="创建单个账户类型", notes="创建单个账户类型")
    public BaseResult addOauthAccountType(@RequestBody OauthAccountType oauthAccountType){
        int i = 0;
        if(null != oauthAccountType){
            oauthAccountType.setAccount_type_id(UUID.toUUID());
            i=oauthAccountTypeService.addOauthAccountType(oauthAccountType);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 修改
     * @param oauthAccountType
     */
    @PutMapping(value="/update")
    @ApiOperation(value="编辑单个账户类型", notes="编辑单个账户类型")
    public BaseResult updateOauthAccountType(@RequestBody OauthAccountType oauthAccountType){
        int i = 0;
        if(null != oauthAccountType){
            i=oauthAccountTypeService.updateOauthAccountType(oauthAccountType);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 删除
     * @param account_type_id
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="删除账户类型", notes="删除账户类型")
    public BaseResult delAccountType(String account_type_id){
        int i = 0;
        if(!StringUtil.isEmpty(account_type_id)){

            OauthAccountType oauthAccountType = oauthAccountTypeService.getOauthAccountTypeById(account_type_id);
            i=oauthAccountTypeService.delAccountType(oauthAccountType);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 读取全部数据
     */
    @NeedLoginUnAuth
    @GetMapping(value="/listAll")
    @ApiOperation(value="查询全部账户类型", notes="查询全部账户类型")
    public BaseResult<List<OauthAccountType>> listAll(){
        Map<String, Object> condition = new HashMap<>();
        List<OauthAccountType> oauthAccountTypeList = oauthAccountTypeService.getOauthAccountTypeList(condition);
        return outDataStr(oauthAccountTypeList);
    }
}
