package jehc.djshi.job.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
 * @Desc 任务调度配置信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="任务调度配置信息对象", description="任务调度配置信息")
public class JobConfig extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "主键")
	private String id;/**主键**/

	@ApiModelProperty(value = "标题")
	private String jobTitle;//标题

	@ApiModelProperty(value = "任务id")
	private String jobId;/**任务id**/

	@ApiModelProperty(value = "任务名称")
	private String jobName;/**任务名称**/

	@ApiModelProperty(value = "任务分组")
	private String jobGroup;/**任务分组**/

	@ApiModelProperty(value = "任务状态 0禁用 1启用 2删除")
	private String jobStatus;/**任务状态 0禁用 1启用 2删除**/

	@ApiModelProperty(value = "任务运行时间表达式")
	private String cronExpression;/**任务运行时间表达式**/

	@ApiModelProperty(value = "任务描述")
	private String desc_;/**任务描述**/

	@ApiModelProperty(value = "客户端Id")
	private String clientId;/**客户端Id**/

	@ApiModelProperty(value = "客户端组Id")
	private String clientGroupId;/**客户端组Id**/

	@ApiModelProperty(value = "注解事件")
	private String jobHandler;/**注解事件**/

	@ApiModelProperty(value = "参数")
	private String jobPara;/**参数**/
}
