package jehc.djshi.job.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
/**
 * @Desc 调度器请求参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@Slf4j
@ApiModel(value="调度器请求参数对象", description="调度器请求参数")
public class ParamInfo<T> {

    @ApiModelProperty(value = "对象")
    private Object obj;//对象

    @ApiModelProperty(value = "泛型对象")
    private T data;//泛型对象

    @ApiModelProperty(value = "消息体内容")
    private String message;//消息体内容

    public ParamInfo(){

    }

    public ParamInfo(Object obj){

    }

    @Override
    public String toString() {
        return "ParamInfo{" +
                "obj=" + obj +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }
}
