package jehc.djshi.job.model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 调度器日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="调度器日志对象", description="调度器日志")
public class JobLog extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String job_log_id;/**主键**/

	@ApiModelProperty(value = "名称")
	private String job_log_name;/**名称**/

	@ApiModelProperty(value = "调度键")
	private String job_log_key;/**调度键**/

	@ApiModelProperty(value = "调度内容")
	private String job_log_content;/**调度内容**/

	@ApiModelProperty(value = "开始时间")
	private String job_log_ctime;/**开始时间**/

	@ApiModelProperty(value = "结束时间")
	private String job_log_etime;/**结束时间**/

	@ApiModelProperty(value = "操作人编号")
	private String xt_userinfo_id;/**操作人编号**/

	@ApiModelProperty(value = "运行标识")
	private String flag;/**运行标识**/
}
