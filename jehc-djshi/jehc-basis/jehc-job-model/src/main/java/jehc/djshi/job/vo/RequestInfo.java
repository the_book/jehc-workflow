package jehc.djshi.job.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.constant.StatusConstant;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
/**
 * @Desc 通信请求体对象
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Data
@ApiModel(value="通信请求体对象", description="通信请求体")
public class RequestInfo<T> {
    @ApiModelProperty(value = "是否第一次握手")
    private boolean shakeHands = false;//是否第一次握手

    @ApiModelProperty(value = "消息体内容")
    private String message;//消息体内容

    @ApiModelProperty(value = "客户端id")
    private String clientId;//客户端id

    @ApiModelProperty(value = "客户端组id")
    private String clientGroupId;//客户端组id

    @ApiModelProperty(value = "状态码")
    private Integer status = StatusConstant.XT_PT_STATUS_VAL_200;;//状态码

    @ApiModelProperty(value = "是否成功")
    private Boolean success = true;//是否成功

    @ApiModelProperty(value = "对象")
    private Object obj;//对象

    @ApiModelProperty(value = "泛型对象")
    private T data;//泛型对象

    @ApiModelProperty(value = "JobHandlerEntity对象")
    private JobHandlerEntity jobHandlerEntity;
}
