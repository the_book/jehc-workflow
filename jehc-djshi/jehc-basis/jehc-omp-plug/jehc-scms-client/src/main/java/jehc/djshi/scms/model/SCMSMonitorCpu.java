package jehc.djshi.scms.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Desc 服务器CPU运行
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class SCMSMonitorCpu implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String mac;/**主表mac**/
	private int cpu_total_mhz;/**CPU实际使用主频**/
	private String cpu_producer;/**CPU生产商**/
	private Long cpu_cache;/**CPU缓存数量**/
	private String cpu_user_use_rate;/**CPU用户使用率**/
	private String cpu_sys_use_rate;/**CPU系统使用率**/
	private String cpu_wait_use_rate;/**CPU当前等待率**/
	private String cpu_error_use_rate;/**CPU当前错误率**/
	private String cpu_currently_idle;/**CPU当前空闲率**/
	private String cpu_use_rate;/**CPU总的使用率**/
	private int num;/**第几模块CPU信息**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
}
