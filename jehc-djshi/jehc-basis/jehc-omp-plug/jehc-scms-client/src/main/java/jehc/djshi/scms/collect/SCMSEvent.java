package jehc.djshi.scms.collect;

import jehc.djshi.scms.model.SCMSMonitor;
import jehc.djshi.scms.model.SCMSMonitorCpu;
import jehc.djshi.scms.model.SCMSMonitorMem;
import jehc.djshi.scms.util.OSUtil;
import jehc.djshi.scms.util.SigarUtil;
import lombok.extern.slf4j.Slf4j;
import org.hyperic.sigar.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.stereotype.Component;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * @Desc 服务器信息监控
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Slf4j
@Order(1)
public class SCMSEvent implements CommandLineRunner {

//
//    @Scheduled(cron="*/30 * * * * *")
//    public void execute() {
//    }

    @Autowired
    RmiProxyFactoryBean rmiProxyFactoryBean;

    @Override
    public void run(String... args) throws Exception {
        while (true){
            try {
                try {
                    SCMSMonitor scmsMonitor = monitor();
                    scmsMonitor.setScmsMonitorMem(memory());
                    scmsMonitor.setScmsMonitorCpus(cpu());
                    SCMSMonitorService scmsMonitorService = (SCMSMonitorService)rmiProxyFactoryBean.getObject();
                    scmsMonitorService.saveSCMSMonitor(scmsMonitor);
                } catch (Exception e) {
                    log.error("采集服务器信息异常1：{}",e);
                }
            }catch (Exception e){
                log.error("采集服务器信息异常2：{}",e);
            }finally {
                Thread.sleep(5000);
            }
        }
    }

    /**
     * 主服务器监控
     */
    public SCMSMonitor monitor(){
        Runtime r = Runtime.getRuntime();
        Properties props = System.getProperties();
        InetAddress addr;
        SCMSMonitor scmsMonitor = new SCMSMonitor();
        try {
            addr = InetAddress.getLocalHost();
            String ip = addr.getHostAddress();
            Map<String, String> map = System.getenv();
            String userName = map.get("USERNAME");//获取用户名
            String computerName = map.get("COMPUTERNAME");//获取计算机名
            scmsMonitor.setUser_name(userName);
            scmsMonitor.setAccount_name(props.getProperty("user.name"));
            scmsMonitor.setCom_name(computerName);
            scmsMonitor.setLocal_name(addr.getHostName());
            scmsMonitor.setJvm_total_mem(Integer.parseInt(""+r.totalMemory()));
            scmsMonitor.setJvm_mem(Integer.parseInt(""+r.freeMemory()));
            scmsMonitor.setOperate_sys_name(props.getProperty("os.name"));
            scmsMonitor.setOperate_org(props.getProperty("os.arch"));
            scmsMonitor.setJvm_cpu_count(r.availableProcessors());
            scmsMonitor.setIp(ip);
            scmsMonitor.setEnvironment(props.getProperty("java.version"));
            scmsMonitor.setPath(props.getProperty("java.home"));
            scmsMonitor.setCreate_time(new Date());
            scmsMonitor.setUpdate_time(new Date());
            scmsMonitor.setMac(OSUtil.getMacAddress());
        } catch (UnknownHostException e) {
            log.error("主服务器信息获取失败{}",e);
        }
        return scmsMonitor;
    }

    /**
     * 内存监控
     * @return
     */
    public SCMSMonitorMem memory(){
        Mem mem;
        SCMSMonitorMem scmsMonitorMem = new SCMSMonitorMem();
        try {
            mem = SigarUtil.getMemory();
            Swap swap = SigarUtil.getSwap();
            scmsMonitorMem.setMem_total(""+mem.getTotal() / 1024L);
            scmsMonitorMem.setMem_curr_use(""+mem.getUsed() / 1024L);
            scmsMonitorMem.setMem_curr_sy(""+mem.getFree() / 1024L);
            scmsMonitorMem.setMem_jh_total(""+swap.getTotal() / 1024L);
            scmsMonitorMem.setMem_jh_curr_use(""+swap.getUsed() / 1024L);
            scmsMonitorMem.setMem_jh_sy(""+swap.getFree() / 1024L);
            scmsMonitorMem.setCreate_time(new Date());
        } catch (SigarException e) {
            log.error("内存监控失败{}",e);
        }
        return scmsMonitorMem;
    }

    /**
     * CPU监控
     */
    public List<SCMSMonitorCpu> cpu(){
        List<SCMSMonitorCpu> scmsMonitorCpuList = new ArrayList<>();
        String libs = System.getProperty("java.library.path");
        System.setProperty("java.library.path", libs);
        try {
            CpuInfo[] cpuInfoList = SigarUtil.getCpuInfoList();
            CpuPerc cpu  = SigarUtil.getCpu();
            for(int i = 0; i < cpuInfoList.length; i++) {
                SCMSMonitorCpu scmsMonitorCpu = new SCMSMonitorCpu();
                CpuInfo info = cpuInfoList[i];
                scmsMonitorCpu.setCpu_total_mhz(info.getMhz());
                scmsMonitorCpu.setCpu_producer(info.getVendor());
                scmsMonitorCpu.setCpu_cache(info.getCacheSize());
                /*scmsMonitorCpu.setCpu_user_use_rate(CpuPerc.format(cpu.getUser()));
                scmsMonitorCpu.setCpu_sys_use_rate(CpuPerc.format(cpu.getSys()));
                scmsMonitorCpu.setCpu_wait_use_rate(CpuPerc.format(cpu.getWait()));
                scmsMonitorCpu.setCpu_error_use_rate(CpuPerc.format(cpu.getNice()));
                scmsMonitorCpu.setCpu_currently_idle(CpuPerc.format(cpu.getIdle()));
                scmsMonitorCpu.setCpu_use_rate(CpuPerc.format(cpu.getCombined()));*/
                scmsMonitorCpu.setCpu_user_use_rate(""+cpu.getUser());
                scmsMonitorCpu.setCpu_sys_use_rate(""+cpu.getSys());
                scmsMonitorCpu.setCpu_wait_use_rate(""+cpu.getWait());
                scmsMonitorCpu.setCpu_error_use_rate(""+cpu.getNice());
                scmsMonitorCpu.setCpu_currently_idle(""+cpu.getIdle());
                scmsMonitorCpu.setCpu_use_rate(""+cpu.getCombined());
                scmsMonitorCpu.setNum(i+1);
                scmsMonitorCpu.setCreate_time(new Date());
                scmsMonitorCpuList.add(scmsMonitorCpu);
            }
        } catch (SigarException e) {
            log.error("内存监控失败{}",e);
        }
        return scmsMonitorCpuList;
    }
}
