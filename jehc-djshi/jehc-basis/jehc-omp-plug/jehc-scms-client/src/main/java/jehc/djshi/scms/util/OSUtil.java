package jehc.djshi.scms.util;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

/**
 * @Desc OS工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class OSUtil {

    /**
     * 操作系统
     * @return
     */
    public static String os(){
        String osName = System.getProperty("os.name");
        String OSname=null;
        if (Pattern.matches("Linux.*", osName)) {
            OSname="Linux";
        } else if (Pattern.matches("Windows.*", osName)) {
            OSname="Windows";
        } else if (Pattern.matches("Mac.*", osName)) {
            OSname="Mac";
        } else if (Pattern.matches("Solaris.*", osName)) {
            OSname="Solaris";
        } else if (Pattern.matches("Unix.*", osName)) {
            OSname="Unix";
        } else if (Pattern.matches("OpenVMS.*", osName)) {
            OSname="OpenVMS";
        }else {
            OSname = "Others";
        }
        return OSname;
    }

    /**
     * 获取widnowXp网卡的mac地址
     * @param execStr
     * @return
     */
    public static String getWindowXPMACAddress(String execStr) {
        String mac = null;
        BufferedReader bufferedReader = null;
        Process process = null;
        try {
            // windows下的命令，显示信息中包含有mac地址信息
            process = Runtime.getRuntime().exec(execStr);
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            int index = -1;
            while ((line = bufferedReader.readLine()) != null) {
                if(line.indexOf("本地连接") != -1)//排除有虚拟网卡的情况
                    continue;
                index = line.toLowerCase().indexOf("physical address");// 寻找标示字符串[physical address]
                if (index != -1) {
                    index = line.indexOf(":");
                    if (index != -1) {
                        mac = line.substring(index + 1).trim(); //取出mac地址并去除2边空格
                    }
                    break;
                }
            }
        } catch (IOException e) {
            log.error("异常：{}",e);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e1) {
                log.error("异常：{}",e1);
            }
            bufferedReader = null;
            process = null;
        }
        return mac;
    }

    /**
     * 获取widnow7网卡的mac地址
     * @return
     */
    public static String getWindow7MACAddress() {
        //获取本地IP对象
        InetAddress ia = null;
        try {
            ia = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            log.error("异常：{}",e);
        }
        //获得网络接口对象（即网卡），并得到mac地址，mac地址存在于一个byte数组中。
        byte[] mac = null;
        try {
            mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
        } catch (SocketException e) {
            log.error("异常：{}",e);
        }
        //下面代码是把mac地址拼装成String
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<mac.length;i++){
            if(i!=0){
                sb.append("-");
            }
            //mac[i] & 0xFF 是为了把byte转化为正整数
            String s = Integer.toHexString(mac[i] & 0xFF);
            sb.append(s.length()==1?0+s:s);
        }
        //把字符串所有小写字母改为大写成为正规的mac地址并返回
        return sb.toString().toUpperCase();
    }

    /**
     * 获取Linux网卡的mac地址
     * @return
     */
    public static String getLinuxMACAddress() {
        String mac = null;
        BufferedReader bufferedReader = null;
        Process process = null;
        try {
            process = Runtime.getRuntime().exec("ifconfig eth0");// linux下的命令，一般取eth0作为本地主网卡 显示信息中包含有mac地址信息
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            int index = -1;
            while ((line = bufferedReader.readLine()) != null) {
                index = line.toLowerCase().indexOf("硬件地址");
                if (index != -1) {
                    mac = line.substring(index + 4).trim(); // 取出mac地址并去除2边空格
                    break;
                }
            }
        } catch (IOException e) {
            log.error("异常：{}",e);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e1) {
                log.error("异常：{}",e1);
            }
            bufferedReader = null;
            process = null;
        }
        return mac;
    }

    /**
     * 获取Unix网卡的mac地址
     * @return
     */
    public static String getUnixMACAddress() {
        String mac = null;
        BufferedReader bufferedReader = null;
        Process process = null;
        try {
            process = Runtime.getRuntime().exec("ifconfig eth0");// Unix下的命令，一般取eth0作为本地主网卡 显示信息中包含有mac地址信息
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            int index = -1;
            while ((line = bufferedReader.readLine()) != null) {
                index = line.toLowerCase().indexOf("hwaddr");// 寻找标示字符串[hwaddr]
                if (index != -1) {
                    mac = line.substring(index + "hwaddr".length() + 1).trim(); // 取出mac地址并去除2边空格
                    break;
                }
            }
        } catch (IOException e) {
            log.error("异常：{}",e);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e1) {
                log.error("异常：{}",e1);
            }
            bufferedReader = null;
            process = null;
        }
        return mac;
    }

    /**
     * 获取当前操作系统名称.
     * @return
     */
    public static String getOS() {
        return System.getProperty("os.name").toLowerCase();
    }

    /**
     * 获取MAC地址
     */
    public static String getMacAddress(){
        String os = getOS();
        String execStr = getSystemRoot()+"/system32/ipconfig /all";
        String mac = null;
        if(os.startsWith("windows")){
            if(os.equals("windows xp")){//Windows XP
                mac = getWindowXPMACAddress(execStr);
            }else if(os.equals("windows 2003")){//Windows 2003
                mac = getWindowXPMACAddress(execStr);
            }else{//Windows 7
                mac = getWindow7MACAddress();
            }
        }else if (os.startsWith("linux")) {
            mac = getLinuxMACAddress();
        }else{
            mac = getUnixMACAddress();
        }
        return mac;
    }

    /**
     *获取系统命令路径
     */
    public static String getSystemRoot(){
        String cmd = null;
        String os = null;
        String result = null;
        String envName = "windir";
        os = System.getProperty("os.name").toLowerCase();
        if(os.startsWith("windows")) {
            cmd = "cmd /c SET";
        }else {
            cmd = "env";
        }
        try {
            Process p = Runtime.getRuntime().exec(cmd);
            InputStreamReader isr = new InputStreamReader(p.getInputStream());
            BufferedReader commandResult = new BufferedReader(isr);
            String line = null;
            while ((line = commandResult.readLine()) != null) {
                line=line.toLowerCase();
                if (line.indexOf(envName) > -1) {
                    result =  line.substring(line.indexOf(envName)
                            + envName.length() + 1);
                    return result;
                }
            }
        } catch (Exception e) {
            log.error("获取系统命令路径 error: " + cmd + ":{}" , e);
        }
        return null;
    }
}
