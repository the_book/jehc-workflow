package jehc.djshi.scms.model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * @Desc 监控主表
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class SCMSMonitor implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String user_name;/**用户名**/
	private String account_name;/**用户的账户名称**/
	private String com_name;/**计算机名**/
	private String local_name;/**本地主机名**/
	private int jvm_total_mem;/**JVM可以使用的总内存**/
	private int jvm_mem;/**JVM可以使用的剩余内存**/
	private String operate_sys_name;/**操作系统的名称**/
	private String operate_org;/**操作系统的构架**/
	private int jvm_cpu_count;/**JVM可以使用的处理器个数**/
	private String ip;/**本地IP地址**/
	private String environment;/**Java的运行环境版本**/
	private String path;/**Java的安装路径**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private SCMSMonitorMem scmsMonitorMem;//内存
	private List<SCMSMonitorCpu> scmsMonitorCpus;//CPU
	String mac;
}
