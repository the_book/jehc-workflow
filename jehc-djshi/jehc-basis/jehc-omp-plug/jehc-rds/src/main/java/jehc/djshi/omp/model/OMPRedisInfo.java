package jehc.djshi.omp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc Redis 信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="Redis信息对象", description="Redis信息")
public class OMPRedisInfo extends BaseEntity {

    @ApiModelProperty(value = "主键")
    private String id;//主键

    @ApiModelProperty(value = "主监控日志id")
    private String main_id;//主监控日志id

    @ApiModelProperty(value = "配置id")
    private String config_id;//配置id

    @ApiModelProperty(value = "连接name")
    private String name;//连接name

    @ApiModelProperty(value = "连接ip")
    private String host;//连接ip

    //Stats（一般统计信息）
    @ApiModelProperty(value = "新创建的链接个数，如果过多，会影响性能")
    private String total_connections_received;//新创建的链接个数，如果过多，会影响性能

    @ApiModelProperty(value = "服务器已执行命令数")
    private String total_commands_processed;//服务器已执行命令数

    @ApiModelProperty(value = "redis当前的qps，redis内部较实时的每秒执行命令数")
    private String instantaneous_ops_per_sec;//redis当前的qps，redis内部较实时的每秒执行命令数

    @ApiModelProperty(value = "redis网络入口流量字节数")
    private String total_net_input_bytes;//redis网络入口流量字节数

    @ApiModelProperty(value = "redis网络出口流量字节数")
    private String total_net_output_bytes;//redis网络出口流量字节数

    @ApiModelProperty(value = "网络入口KPS")
    private String instantaneous_input_kbps;//网络入口KPS

    @ApiModelProperty(value = "网络出口KPS")
    private String instantaneous_output_kbps;//网络出口KPS

    @ApiModelProperty(value = "拒绝的连接个数，redis连接个数已经达到maxclients限制。")
    private String rejected_connections;//拒绝的连接个数，redis连接个数已经达到maxclients限制。

    @ApiModelProperty(value = "主从完全同步成功次数")
    private String sync_full;//主从完全同步成功次数

    @ApiModelProperty(value = "主从部分同步成功次数")
    private String sync_partial_ok;//主从部分同步成功次数

    @ApiModelProperty(value = "主从部分同步失败次数")
    private String sync_partial_err;//主从部分同步失败次数

    @ApiModelProperty(value = "运行以来过期的key的数量")
    private String expired_keys;//运行以来过期的key的数量

    @ApiModelProperty(value = "运行以来剔除（超过maxmemory）的key的数量")
    private String evicted_keys;//运行以来剔除（超过maxmemory）的key的数量

    @ApiModelProperty(value = "命中次数，缓存命中率=keyspace_hits/(keyspace_hits+keyspace_misses)")
    private String keyspace_hits;//命中次数，缓存命中率=keyspace_hits/(keyspace_hits+keyspace_misses)

    @ApiModelProperty(value = "未命中次数")
    private String keyspace_misses;//未命中次数

    @ApiModelProperty(value = "当前使用中的频道数量")
    private String pubsub_channels;//当前使用中的频道数量

    @ApiModelProperty(value = "当前使用的模式数量")
    private String pubsub_patterns;//当前使用的模式数量

    @ApiModelProperty(value = "最近一次fork操作阻塞redis进程的耗时数，单位微秒")
    private String latest_fork_usec;//最近一次fork操作阻塞redis进程的耗时数，单位微秒

    @ApiModelProperty(value = "是否已经缓存了到该地址的连接")
    private String migrate_cached_sockets;//是否已经缓存了到该地址的连接

    @ApiModelProperty(value = "从实例到期key数量")
    private String slave_expires_tracked_keys;//从实例到期key数量

    @ApiModelProperty(value = "主动碎片整理命中次数")
    private String active_defrag_hits;//主动碎片整理命中次数

    @ApiModelProperty(value = "主动碎片整理未命中次数")
    private String active_defrag_misses;//主动碎片整理未命中次数

    @ApiModelProperty(value = "主动碎片整理key命中次数")
    private String active_defrag_key_hits;//主动碎片整理key命中次数

    @ApiModelProperty(value = "主动碎片整理key未命中次数")
    private String active_defrag_key_misses;//主动碎片整理key未命中次数

    // Replication (主从信息，slave上显示的信息)
    @ApiModelProperty(value = "实例的角色，是master or slave")

    private String role;//实例的角色，是master or slave
    @ApiModelProperty(value = "此节点对应的master的ip")
    private String master_host;//此节点对应的master的ip

    @ApiModelProperty(value = "此节点对应的master的port")
    private String master_port;//此节点对应的master的port

    @ApiModelProperty(value = "slave端可查看它与master之间同步状态,当复制断开后表示down")
    private String master_link_status;//slave端可查看它与master之间同步状态,当复制断开后表示down

    @ApiModelProperty(value = "主库多少秒未发送数据到从库")
    private String master_last_io_seconds_ago;//主库多少秒未发送数据到从库------------

    @ApiModelProperty(value = "从服务器是否在与主服务器进行同步")
    private String master_sync_in_progress;//从服务器是否在与主服务器进行同步

    @ApiModelProperty(value = "slave复制偏移量")
    private String slave_repl_offset;//slave复制偏移量

    @ApiModelProperty(value = "slave优先级")
    private String slave_priority;//slave优先级

    @ApiModelProperty(value = "从库是否设置只读")
    private String slave_read_only;//从库是否设置只读

    @ApiModelProperty(value = "连接的slave实例个数")
    private String connected_slaves;//连接的slave实例个数

    @ApiModelProperty(value = "主从同步偏移量,此值如果和上面的offset相同说明主从是否一致是否有延迟，与master_replid可被用来标识主实例复制流中的位置")
    private String master_repl_offset;//主从同步偏移量,此值如果和上面的offset相同说明主从是否一致是否有延迟，与master_replid可被用来标识主实例复制流中的位置

    @ApiModelProperty(value = "复制积压缓冲区是否开启")
    private String repl_backlog_active;//复制积压缓冲区是否开启

    @ApiModelProperty(value = "环形缓冲复制队列容量")
    private String repl_backlog_size;//环形缓冲复制队列容量

    @ApiModelProperty(value = "此值等于 master_repl_offset -")
    private String repl_backlog_histlen;//此值等于 master_repl_offset -

    @ApiModelProperty(value = "复制缓冲区里偏移量的大小")
    private String repl_backlog_first_byte_offset;//复制缓冲区里偏移量的大小

    //Clients
    @ApiModelProperty(value = "已经连接客户端数量（不包括slave连接的客户端）")
    private String connected_clients;//已经连接客户端数量（不包括slave连接的客户端）

    @ApiModelProperty(value = "当前连接的客户端当中，最长的输出列表")
    private String client_recent_max_input_buffer;//当前连接的客户端当中，最长的输出列表

    @ApiModelProperty(value = "前连接的客户端当中，最大输入缓存")
    private String client_recent_max_output_buffer;//前连接的客户端当中，最大输入缓存

    @ApiModelProperty(value = "正在等待阻塞命令的客户端数量（即被阻塞的客户端数）")
    private String blocked_clients;//正在等待阻塞命令的客户端数量（即被阻塞的客户端数）

    @ApiModelProperty(value = "当前连接的客户端当中，最长的输出列表")
    private String client_longest_output_list;//当前连接的客户端当中，最长的输出列表

    @ApiModelProperty(value = "当前客户端当中，最大输入缓存")
    private String client_biggest_input_buf;//当前客户端当中，最大输入缓存

    //配置型号
    @ApiModelProperty(value = "redis服务器版本")
    private String redis_version;//redis服务器版本

    @ApiModelProperty(value = "Git SHA1")
    private String redis_git_sha1;//Git SHA1

    @ApiModelProperty(value = "Git dirty flag")
    private String redis_git_dirty;//Git dirty flag

    @ApiModelProperty(value = "redis build id")
    private String redis_build_id;//redis build id

    @ApiModelProperty(value = "运行模式，单机或集群")
    private String redis_mode;//运行模式，单机或集群

    @ApiModelProperty(value = "redis服务器宿主机操作系统")
    private String os;//redis服务器宿主机操作系统

    @ApiModelProperty(value = "架构（32 或 64 位）")
    private String arch_bits;//架构（32 或 64 位）

    @ApiModelProperty(value = "事件处理机制")
    private String multiplexing_api;//事件处理机制

    @ApiModelProperty(value = "atomicvar_api")
    private String atomicvar_api;//

    @ApiModelProperty(value = "gcc_version")
    private String gcc_version;//

    @ApiModelProperty(value = "redis服务器进程的pid")
    private String process_id;//redis服务器进程的pid

    @ApiModelProperty(value = "redis服务器监听端口")
    private String tcp_port;//redis服务器监听端口

    @ApiModelProperty(value = "自 Redis 服务器启动以来，经过的秒数")
    private String uptime_in_seconds;//自 Redis 服务器启动以来，经过的秒数

    @ApiModelProperty(value = "redis服务器启动总时间，单位天")
    private String uptime_in_days;//redis服务器启动总时间，单位天

    @ApiModelProperty(value = "redis内部调度频率（关闭timeout客户端，删除过期key）")
    private String hz;//redis内部调度频率（关闭timeout客户端，删除过期key）

    @ApiModelProperty(value = "configured_hz")
    private String configured_hz;//

    @ApiModelProperty(value = "自增时间，用于LRU管理")
    private String lru_clock;//自增时间，用于LRU管理

    @ApiModelProperty(value = "executable")
    private String executable;//

    @ApiModelProperty(value = "配置文件路径")
    private String config_file;//配置文件路径

    @ApiModelProperty(value = "服务器运行id")
    private String run_id;//服务器运行id

    //CPU
    @ApiModelProperty(value = "cpu在内核态所消耗的cpu的时间")
    private String used_cpu_sys;//cpu在内核态所消耗的cpu的时间

    @ApiModelProperty(value = "已使用CPU")
    private String used_cpu_user;//已使用CPU

    @ApiModelProperty(value = "将后台进程在用户态所占用的CPU时求和累计起来")
    private String used_cpu_user_children;//将后台进程在用户态所占用的CPU时求和累计起来

    @ApiModelProperty(value = "将后台进程在核心态所占用的CPU时求和累计起来")
    private String used_cpu_sys_children;//将后台进程在核心态所占用的CPU时求和累计起来

    //内存
    @ApiModelProperty(value = "由redis分配器分配的内存总量，单位字节")
    private String used_memory;//由redis分配器分配的内存总量，单位字节

    @ApiModelProperty(value = "由redis分配器分配的内存总量，单位字节")
    private String used_memory_human;//以可读方式返回redis已分配的内存总量

    @ApiModelProperty(value = "从操作系统角度，返回redis已分配内存总量")
    private String used_memory_rss;//从操作系统角度，返回redis已分配内存总量

    @ApiModelProperty(value = "以可读方式，从操作系统的角度，返回 Redis 已分配的内存总量（俗称常驻集大小）。这个值和 top 、 ps等命令的输出一致。")
    private String used_memory_rss_human;//以可读方式，从操作系统的角度，返回 Redis 已分配的内存总量（俗称常驻集大小）。这个值和 top 、 ps等命令的输出一致。

    @ApiModelProperty(value = "redis的内存消耗峰值（以字节为单位）")
    private String used_memory_peak;//redis的内存消耗峰值（以字节为单位）

    @ApiModelProperty(value = "以可读方式返回redis内存消耗峰值")
    private String used_memory_peak_human;//以可读方式返回redis内存消耗峰值

    @ApiModelProperty(value = "(used_memory/ used_memory_peak) *100%")
    private String used_memory_peak_perc;//(used_memory/ used_memory_peak) *100%

    @ApiModelProperty(value = "Redis为了维护数据集的内部机制所需的内存开销，包括所有客户端输出缓冲区、查询缓冲区、AOF重写缓冲区和主从复制的backlog")
    private String used_memory_overhead;//Redis为了维护数据集的内部机制所需的内存开销，包括所有客户端输出缓冲区、查询缓冲区、AOF重写缓冲区和主从复制的backlog

    @ApiModelProperty(value = "Redis服务器启动时消耗的内存")
    private String used_memory_startup;//Redis服务器启动时消耗的内存

    @ApiModelProperty(value = "used_memory—used_memory_overhead")
    private String used_memory_dataset;//used_memory—used_memory_overhead

    @ApiModelProperty(value = "100%*(used_memory_dataset/(used_memory—used_memory_startup)")
    private String used_memory_dataset_perc;//100%*(used_memory_dataset/(used_memory—used_memory_startup)

    @ApiModelProperty(value = "allocator_allocated")
    private String allocator_allocated;//

    @ApiModelProperty(value = "allocator_active")
    private String allocator_active;//

    @ApiModelProperty(value = "allocator_resident")
    private String allocator_resident;//

    @ApiModelProperty(value = "整个系统内存")
    private String total_system_memory;//整个系统内存

    @ApiModelProperty(value = "以可读方式，显示整个系统内存")
    private String total_system_memory_human;//以可读方式，显示整个系统内存

    @ApiModelProperty(value = "lua引擎所使用的内存大小（单位字节）")
    private String used_memory_lua;//lua引擎所使用的内存大小（单位字节）

    @ApiModelProperty(value = "以可读方式，显示Lua脚本存储占用的内存")
    private String used_memory_lua_human;//以可读方式，显示Lua脚本存储占用的内存

    @ApiModelProperty(value = "内存分配器")
    private String mem_allocator;//内存分配器

    @ApiModelProperty(value = "used_memory_rss和used_memory比率，小于1表示使用了swap，大于1表示碎片多，redis进行增加删除的动作，会引起内存碎片化")
    private String mem_fragmentation_ratio;//used_memory_rss和used_memory比率，小于1表示使用了swap，大于1表示碎片多，redis进行增加删除的动作，会引起内存碎片化

    //Persistence
    @ApiModelProperty(value = "服务器是否正在载入持久化文件")
    private String loading;//服务器是否正在载入持久化文件

    @ApiModelProperty(value = "有多少个已经写入的命令还未被持久化")
    private String rdb_changes_since_last_save;//有多少个已经写入的命令还未被持久化

    @ApiModelProperty(value = "服务器是否正在创建rdb文件")
    private String rdb_bgsave_in_progress;//服务器是否正在创建rdb文件

    @ApiModelProperty(value = "已经有多长时间没有进行持久化了")
    private String rdb_last_save_time;//已经有多长时间没有进行持久化了

    @ApiModelProperty(value = "最后一次的rdb持久化是否成功")
    private String rdb_last_bgsave_status;//最后一次的rdb持久化是否成功

    @ApiModelProperty(value = "最后一次生成rdb文件耗时秒数")
    private String rdb_last_bgsave_time_sec;//最后一次生成rdb文件耗时秒数

    @ApiModelProperty(value = "如果服务器正在创建rdb文件，那么当前这个记录就是创建操作耗时秒数")
    private String rdb_current_bgsave_time_sec;//如果服务器正在创建rdb文件，那么当前这个记录就是创建操作耗时秒数

    @ApiModelProperty(value = "rdb_last_cow_size")
    private String rdb_last_cow_size;//

    @ApiModelProperty(value = "如果rewrite操作正在进行，则记录所使用的时间")
    private String aof_current_rewrite_time_sec;//如果rewrite操作正在进行，则记录所使用的时间

    @ApiModelProperty(value = "上次bgrewriteaof操作的状态")
    private String aof_last_bgrewrite_status;//上次bgrewriteaof操作的状态

    @ApiModelProperty(value = "标识aof的rewrite操作是否进行中")
    private String aof_rewrite_in_progress;//标识aof的rewrite操作是否进行中

    @ApiModelProperty(value = "上次rewrite操作使用的时间(单位s)")
    private String aof_last_rewrite_time_sec;//上次rewrite操作使用的时间(单位s)

    @ApiModelProperty(value = "AOF过程中父进程与子进程相比执行了多少修改(包括读缓冲区，写缓冲区，数据修改等)")
    private String aof_last_cow_size;//AOF过程中父进程与子进程相比执行了多少修改(包括读缓冲区，写缓冲区，数据修改等)

    @ApiModelProperty(value = "是否开启了aof 默认没开启(已开启)")
    private String aof_enabled;//是否开启了aof 默认没开启(已开启)

    @ApiModelProperty(value = "上次aof写入状态")
    private String aof_last_write_status;//上次aof写入状态

    @ApiModelProperty(value = "标识是否将要在rdb save操作结束后执行")
    private String aof_rewrite_scheduled;//标识是否将要在rdb save操作结束后执行

    @ApiModelProperty(value = "最近一次aof rewrite耗费的时长")
    private String braof_last_rewrite_time_sec;//最近一次aof rewrite耗费的时长

    @ApiModelProperty(value = "如果rewrite操作正在进行，则记录所使用的时间，单位秒")
    private String braof_current_rewrite_time_sec;//如果rewrite操作正在进行，则记录所使用的时间，单位秒

    @ApiModelProperty(value = "上次bgrewriteaof操作的状态")
    private String braof_last_bgrewrite_status;//上次bgrewriteaof操作的状态

    @ApiModelProperty(value = "上次aof写入状态")
    private String braof_last_write_status;//上次aof写入状态

    @ApiModelProperty(value = "aof当前尺寸")
    private String braof_current_size;//aof当前尺寸

    @ApiModelProperty(value = "服务器启动时或者aof重写最近一次执行之后aof文件的大小")
    private String braof_base_size;//服务器启动时或者aof重写最近一次执行之后aof文件的大小

    @ApiModelProperty(value = "是否有aof重写操作在等待rdb文件创建完毕之后执行")
    private String braof_pending_rewrite;//是否有aof重写操作在等待rdb文件创建完毕之后执行

    @ApiModelProperty(value = "aof buffer的大小")
    private String braof_buffer_length;//aof buffer的大小

    @ApiModelProperty(value = "aof rewrite buffer的大小")
    private String braof_rewrite_buffer_length;//aof rewrite buffer的大小

    @ApiModelProperty(value = "后台I/O队列里面，等待执行的fsync调用数量")
    private String braof_pending_bio_fsync;//后台I/O队列里面，等待执行的fsync调用数量

    @ApiModelProperty(value = "被延迟的fsync调用数量")
    private String braof_delayed_fsync;//被延迟的fsync调用数量

    //集群（集群相关信息）

    @ApiModelProperty(value = "实例是否启用集群模式 0否")
    private String cluster_enabled;//实例是否启用集群模式 0否

    //Keyspace（数据库相关信息）
    @ApiModelProperty(value = "db0的key数量以及带有生存周期的key的个数，平均存活时间")
    private String db0;//db0的key数量以及带有生存周期的key的个数，平均存活时间
}
