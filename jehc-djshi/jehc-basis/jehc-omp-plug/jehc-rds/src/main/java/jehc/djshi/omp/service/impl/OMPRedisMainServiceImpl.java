package jehc.djshi.omp.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.omp.dao.OMPRedisMainDao;
import jehc.djshi.omp.model.OMPRedisMain;
import jehc.djshi.omp.service.OMPRedisMainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc Redis主监控信息日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class OMPRedisMainServiceImpl extends BaseService implements OMPRedisMainService {

    @Autowired
    OMPRedisMainDao ompRedisMainDao;

    /**
     * 初始化分页
     * @param condition
     * @return
     */
    public List<OMPRedisMain> getRedisMainListByCondition(Map<String,Object> condition){
        return ompRedisMainDao.getRedisMainListByCondition(condition);
    }

    /**
     * 查询对象
     * @param id
     * @return
     */
    public OMPRedisMain getRedisMainById(String id){
        return ompRedisMainDao.getRedisMainById(id);
    }

    /**
     * 添加
     * @param ompRedisMain
     * @return
     */
    public int addRedisMain(OMPRedisMain ompRedisMain){
        return ompRedisMainDao.addRedisMain(ompRedisMain);
    }

    /**
     * 删除
     * @param map
     * @return
     */
    public int delRedisMain(Map<String,Object> map){
        return ompRedisMainDao.delRedisMain(map);
    }


    /**
     * 查询列表
     * @param ompRedisMain
     * @return
     */
    public List<OMPRedisMain> getRedisMainList(OMPRedisMain ompRedisMain){
        return ompRedisMainDao.getRedisMainList(ompRedisMain);
    }
}
