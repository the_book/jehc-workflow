package jehc.djshi.omp.web;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.omp.model.OMPRedisConfig;
import jehc.djshi.omp.service.OMPRedisConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc Redis配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/omp/redisConfig")
@Api(value = "Redis配置",tags = "Redis配置",description = "Redis配置")
public class OMPRedisConfigController extends BaseAction{

    @Autowired
    OMPRedisConfigService redisConfigService;

    /**
     * 查询Redis配置列表并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询Redis配置列表并分页", notes="查询Redis配置列表并分页")
    public BasePage<List<OMPRedisConfig>> getRedisConfigListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<OMPRedisConfig> redisConfigList = redisConfigService.getRedisConfigListByCondition(condition);
        for(OMPRedisConfig redisConfig:redisConfigList){
            redisConfigService.check(redisConfig);
            if(!StringUtil.isEmpty(redisConfig.getCreate_id())){
                OauthAccountEntity createBy = getAccount(redisConfig.getCreate_id());
                if(null != createBy){
                    redisConfig.setCreateBy(createBy.getName());
                }
            }
            if(!StringUtil.isEmpty(redisConfig.getUpdate_id())){
                OauthAccountEntity modifiedBy = getAccount(redisConfig.getUpdate_id());
                if(null != modifiedBy){
                    redisConfig.setModifiedBy(modifiedBy.getName());
                }
            }
        }
        PageInfo<OMPRedisConfig> page = new PageInfo<OMPRedisConfig>(redisConfigList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个Redis配置
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    @ApiOperation(value="查询单个Redis配置", notes="查询单个Redis配置")
    public BaseResult<OMPRedisConfig> getRedisConfigById(@PathVariable("id")String id){
        OMPRedisConfig redisConfig = redisConfigService.getRedisConfigById(id);
        if(!StringUtil.isEmpty(redisConfig.getCreate_id())){
            OauthAccountEntity createBy = getAccount(redisConfig.getCreate_id());
            if(null != createBy){
                redisConfig.setCreateBy(createBy.getName());
            }
        }
        if(!StringUtil.isEmpty(redisConfig.getUpdate_id())){
            OauthAccountEntity modifiedBy = getAccount(redisConfig.getUpdate_id());
            if(null != modifiedBy){
                redisConfig.setModifiedBy(modifiedBy.getName());
            }
        }
        return outDataStr(redisConfig);
    }

    /**
     * 创建单个Redis配置
     * @param redisConfig
     */
    @PostMapping(value="/add")
    @ApiOperation(value="创建单个Redis配置", notes="创建单个Redis配置")
    public BaseResult addRedisConfig(@RequestBody OMPRedisConfig redisConfig){
        int i = 0;
        if(null != redisConfig){
            redisConfig.setCreate_id(getXtUid());
            redisConfig.setCreate_time(getDate());
            redisConfig.setId(toUUID());
            i=redisConfigService.addRedisConfig(redisConfig);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 编辑单个Redis配置
     * @param redisConfig
     */
    @PutMapping(value="/update")
    @ApiOperation(value="编辑单个Redis配置", notes="编辑单个Redis配置")
    public BaseResult updateRedisConfig(@RequestBody OMPRedisConfig redisConfig){
        int i = 0;
        if(null != redisConfig){
            redisConfig.setUpdate_id(getXtUid());
            redisConfig.setUpdate_time(getDate());
            i=redisConfigService.updateRedisConfig(redisConfig);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 删除Redis配置
     * @param id
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="删除Redis配置", notes="删除Redis配置")
    public BaseResult delRedisConfig(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=redisConfigService.delRedisConfig(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * check运行状态
     * @param id
     */
    @GetMapping(value="/check")
    @ApiOperation(value="check运行状态", notes="check运行状态")
    @NeedLoginUnAuth
    public BaseResult<List<OMPRedisConfig>> check(String id){
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id);
            List<OMPRedisConfig> ompRedisConfigs =redisConfigService.getRedisConfigListByCondition(condition);
            if(!CollectionUtil.isEmpty(ompRedisConfigs)){
                for(OMPRedisConfig ompRedisConfig: ompRedisConfigs){
                    redisConfigService.check(ompRedisConfig);
                }
                return BaseResult.success(ompRedisConfigs);
            }
        }
        return BaseResult.success();
    }

    /**
     * 查询Redis配置列表
     */
    @NeedLoginUnAuth
    @GetMapping(value="/lists")
    @ApiOperation(value="查询Redis配置列表", notes="查询Redis配置列表")
    public BaseResult<List<OMPRedisConfig>> getRedisConfigList(){
        Map<String, Object> condition = new HashMap<>();
        List<OMPRedisConfig> redisConfigList = redisConfigService.getRedisConfigListByCondition(condition);
        return BaseResult.success(redisConfigList);
    }
}
