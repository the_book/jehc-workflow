package jehc.djshi.omp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc Redis主监控信息日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="Redis主监控信息日志对象", description="Redis主监控信息日志")
public class OMPRedisMain extends BaseEntity{
    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "配置id")
    private String config_id;//配置id

    @ApiModelProperty(value = "监控信息")
    private String info;//监控信息

    @ApiModelProperty(value = "连接name")
    private String name;//连接name

    @ApiModelProperty(value = "连接ip")
    private String host;//连接ip
}
