package jehc.djshi.omp.web;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.common.util.date.DateUtils;
import jehc.djshi.omp.model.OMPRedisInfo;
import jehc.djshi.omp.model.OMPRedisMain;
import jehc.djshi.omp.service.OMPRedisMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Desc Redis主监控信息日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/omp/redisMain")
@Api(value = "Redis主监控信息日志",tags = "Redis主监控信息日志",description = "Redis主监控信息日志")
public class OMPRedisMainController extends BaseAction {
    @Autowired
    OMPRedisMainService ompRedisMainService;

    /**
     * 查询Redis主监控信息日志列表并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询Redis主监控信息日志列表并分页", notes="查询Redis主监控信息日志列表并分页")
    public BasePage<List<OMPRedisMain>> getRedisMainListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<OMPRedisMain> redisMainList = ompRedisMainService.getRedisMainListByCondition(condition);
        PageInfo<OMPRedisMain> page = new PageInfo<OMPRedisMain>(redisMainList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个Redis主监控信息日志
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    @ApiOperation(value="查询单个Redis主监控信息日志", notes="查询单个Redis主监控信息日志")
    public BaseResult<OMPRedisMain> getRedisMainById(@PathVariable("id")String id){
        OMPRedisMain ompRedisMain = ompRedisMainService.getRedisMainById(id);
        return outDataStr(ompRedisMain);
    }

    /**
     * 删除
     * @param id
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="删除", notes="删除")
    public BaseResult delRedisMain(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=ompRedisMainService.delRedisMain(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * cpu
     * @param ompRedisMain
     */
    @NeedLoginUnAuth
    @PostMapping(value="/cpu")
    @ApiOperation(value="cpu", notes="cpu")
    public BaseResult<Map<String,Object>> cpu(OMPRedisMain ompRedisMain){
        if(StringUtil.isEmpty(ompRedisMain.getConfig_id())){
            ompRedisMain.setConfig_id("-1");
        }
        PageHelper.offsetPage(0, 30);
        List<OMPRedisMain> ompRedisMains = ompRedisMainService.getRedisMainList(ompRedisMain);
        Map<String,Object> map = new HashMap<>();
        List<String> xdata = new ArrayList<>();
        List<String> ydata = new ArrayList<>();
        if(!CollectionUtil.isEmpty(ompRedisMains)){
            for(OMPRedisMain redisMain:ompRedisMains){
                if(!StringUtil.isEmpty(redisMain.getInfo())){
                    OMPRedisInfo ompRedisInfo = JsonUtil.fromAliFastJson(redisMain.getInfo(), OMPRedisInfo.class);
                    xdata.add(getTime(redisMain.getCreate_time()));
                    ydata.add(ompRedisInfo.getUsed_cpu_sys());
//                    data.put(DateUtils.formatDateTime(redisMain.getCreate_time()),ompRedisInfo.getUsed_cpu_user());
                }
            }
        }
        map.put("xdata",xdata);
        map.put("ydata",ydata);
        return BaseResult.success(map);
    }

//    /**
//     * cpu
//     * @param ompRedisMain
//     */
//    @NeedLoginUnAuth
//    @PostMapping(value="/cpu")
//    @ApiOperation(value="cpu", notes="cpu")
//    public BaseResult<Map<String,Object>> cpu(OMPRedisMain ompRedisMain){
//        PageHelper.offsetPage(0, 30);
//        List<OMPRedisMain> ompRedisMains = ompRedisMainService.getRedisMainList(ompRedisMain);
//        List<RedisDTO> redisDTOS = new ArrayList<>();
//        if(!CollectionUtil.isEmpty(ompRedisMains)){
//            for(OMPRedisMain redisMain:ompRedisMains){
//                if(!StringUtil.isEmpty(redisMain.getInfo())){
//                    OMPRedisInfo ompRedisInfo = JsonUtil.fromAliFastJson(redisMain.getInfo(), OMPRedisInfo.class);
//                    redisDTOS.add(new RedisDTO(DateUtils.formatDateTime(redisMain.getCreate_time()),ompRedisInfo.getUsed_cpu_user()));
//                }
//            }
//        }
//        return BaseResult.success(redisDTOS);
//    }

    /**
     * 分配器分配的内存总量
     * @param ompRedisMain
     */
    @NeedLoginUnAuth
    @PostMapping(value="/mem")
    @ApiOperation(value="分配器分配的内存总量", notes="分配器分配的内存总量")
    public BaseResult<Map<String,Object>> mem(OMPRedisMain ompRedisMain){
        if(StringUtil.isEmpty(ompRedisMain.getConfig_id())){
            ompRedisMain.setConfig_id("-1");
        }
        PageHelper.offsetPage(0, 30);
        List<OMPRedisMain> ompRedisMains = ompRedisMainService.getRedisMainList(ompRedisMain);
        Map<String,Object> map = new HashMap<>();
        List<String> xdata = new ArrayList<>();
        List<String> ydata = new ArrayList<>();
        if(!CollectionUtil.isEmpty(ompRedisMains)){
            for(OMPRedisMain redisMain:ompRedisMains){
                if(!StringUtil.isEmpty(redisMain.getInfo())){
                    OMPRedisInfo ompRedisInfo = JsonUtil.fromAliFastJson(redisMain.getInfo(), OMPRedisInfo.class);
                    xdata.add(getTime(redisMain.getCreate_time()));
                    try {
                        String mem = ""+(new Long(ompRedisInfo.getUsed_memory()))/(1024*1024);
                        ydata.add(""+mem);
                    }catch (Exception e){
                        ydata.add("0");
                    }
//                    data.put(DateUtils.formatDateTime(redisMain.getCreate_time()),ompRedisInfo.getTotal_system_memory());
                }
            }
        }
        map.put("xdata",xdata);
        map.put("ydata",ydata);
        return BaseResult.success(map);
    }

    /**
     * 内存碎片比率
     * @param ompRedisMain
     */
    @NeedLoginUnAuth
    @PostMapping(value="/memFragmentationRatio")
    @ApiOperation(value="内存碎片比率", notes="内存碎片比率")
    public BaseResult<Map<String,Object>> memFragmentationRatio(OMPRedisMain ompRedisMain){
        if(StringUtil.isEmpty(ompRedisMain.getConfig_id())){
            ompRedisMain.setConfig_id("-1");
        }
        PageHelper.offsetPage(0, 30);
        List<OMPRedisMain> ompRedisMains = ompRedisMainService.getRedisMainList(ompRedisMain);
        Map<String,Object> map = new HashMap<>();
        List<String> xdata = new ArrayList<>();
        List<String> ydata = new ArrayList<>();
        if(!CollectionUtil.isEmpty(ompRedisMains)){
            for(OMPRedisMain redisMain:ompRedisMains){
                if(!StringUtil.isEmpty(redisMain.getInfo())){
                    OMPRedisInfo ompRedisInfo = JsonUtil.fromAliFastJson(redisMain.getInfo(), OMPRedisInfo.class);
                    xdata.add(getTime(redisMain.getCreate_time()));
                    try {
                        ydata.add(ompRedisInfo.getMem_fragmentation_ratio());
                    }catch (Exception e){
                        ydata.add("0");
                    }
//                    data.put(DateUtils.formatDateTime(redisMain.getCreate_time()),ompRedisInfo.getTotal_system_memory());
                }
            }
        }
        map.put("xdata",xdata);
        map.put("ydata",ydata);
        return BaseResult.success(map);
    }


    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     */
    public static String getTime(Date date) {
        return DateUtils.formatDate(date, "HH:mm:ss");
    }

    /**
     * 实时信息
     * @param ompRedisMain
     */
    @NeedLoginUnAuth
    @GetMapping(value="/info")
    @ApiOperation(value="实时信息", notes="实时信息")
    public BaseResult<OMPRedisInfo> info(OMPRedisMain ompRedisMain){
        if(StringUtil.isEmpty(ompRedisMain.getConfig_id())){
            ompRedisMain.setConfig_id("-1");
        }
        PageHelper.offsetPage(0, 1);
        List<OMPRedisMain> ompRedisMains = ompRedisMainService.getRedisMainList(ompRedisMain);
        OMPRedisInfo ompRedisInfo = new OMPRedisInfo();
        if(!CollectionUtil.isEmpty(ompRedisMains)){
            OMPRedisMain redisMain = ompRedisMains.get(0);
            if(!StringUtil.isEmpty(redisMain.getInfo())){
                ompRedisInfo = JsonUtil.fromAliFastJson(redisMain.getInfo(), OMPRedisInfo.class);
            }
        }
        return BaseResult.success(ompRedisInfo);
    }

    /**
     * 内存
     * @param ompRedisMain
     */
    @NeedLoginUnAuth
    @PostMapping(value="/mems")
    @ApiOperation(value="内存", notes="内存")
    public BaseResult<Map<String,Object>> mems(OMPRedisMain ompRedisMain){
        if(StringUtil.isEmpty(ompRedisMain.getConfig_id())){
            ompRedisMain.setConfig_id("-1");
        }
        PageHelper.offsetPage(0, 5);
        List<OMPRedisMain> ompRedisMains = ompRedisMainService.getRedisMainList(ompRedisMain);
        Map<String,Object> map = new HashMap<>();
        List<String> xdata = new ArrayList<>();
        List<String> ydata0 = new ArrayList<>();
        List<String> ydata1 = new ArrayList<>();
        List<String> ydata2 = new ArrayList<>();
        List<String> ydata3 = new ArrayList<>();
        if(!CollectionUtil.isEmpty(ompRedisMains)){
            for(OMPRedisMain redisMain:ompRedisMains){
                if(!StringUtil.isEmpty(redisMain.getInfo())){
                    OMPRedisInfo ompRedisInfo = JsonUtil.fromAliFastJson(redisMain.getInfo(), OMPRedisInfo.class);
                    xdata.add(getTime(redisMain.getCreate_time()));
                    try {
                        ydata0.add(""+(new Long(ompRedisInfo.getUsed_memory()))/(1024*1024));
                        ydata1.add(""+(new Long(ompRedisInfo.getUsed_memory_rss()))/(1024*1024));
                        ydata2.add(""+(new Long(ompRedisInfo.getUsed_memory_lua()))/(1024*1024));
                        ydata3.add(""+(new Long(ompRedisInfo.getUsed_memory_peak()))/(1024*1024));
                    }catch (Exception e){
                        ydata0.add("0");
                        ydata1.add("0");
                        ydata2.add("0");
                        ydata3.add("0");
                    }
                }
            }
        }
        map.put("xdata",xdata);
        map.put("ydata0",ydata0);
        map.put("ydata1",ydata1);
        map.put("ydata2",ydata2);
        map.put("ydata3",ydata3);
        return BaseResult.success(map);
    }
}
