package jehc.djshi.omp.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc Redis主监控信息日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="Redis主监控信息日志对象", description="Redis主监控信息日志")
public class RedisDTO {

    @ApiModelProperty(value = "key")
    private String key;

    @ApiModelProperty(value = "value")
    private String value;

    public RedisDTO(){

    }

    public RedisDTO(String key,String value){
        this.key = key;
        this.value = value;
    }
}
