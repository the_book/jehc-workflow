package jehc.djshi.scms.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.scms.dao.SCMSMonitorMemDao;
import jehc.djshi.scms.model.SCMSMonitorMem;
import jehc.djshi.scms.service.SCMSMonitorMemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 服务器内存
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class SCMSMonitorMemServiceImpl extends BaseService implements SCMSMonitorMemService {
    @Autowired
    SCMSMonitorMemDao scmsMonitorMemDao;

    /**
     * 初始化分页
     * @param condition
     * @return
     */
    public List<SCMSMonitorMem> getSCMSMonitorMemListByCondition(Map<String,Object> condition){
        return scmsMonitorMemDao.getSCMSMonitorMemListByCondition(condition);
    }

    /**
     * 查询对象
     * @param id
     * @return
     */
    public SCMSMonitorMem getSCMSMonitorMemById(String id){
        return scmsMonitorMemDao.getSCMSMonitorMemById(id);
    }

    /**
     * 新增
     * @param scmsMonitorMem
     * @return
     */
    public int addSCMSMonitorMem(SCMSMonitorMem scmsMonitorMem){
        return scmsMonitorMemDao.addSCMSMonitorMem(scmsMonitorMem);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delSCMSMonitorMem(Map<String,Object> condition){
        return scmsMonitorMemDao.delSCMSMonitorMem(condition);
    }


    /**
     * 查询列表
     * @param scmsMonitorMem
     * @return
     */
    public List<SCMSMonitorMem> getSCMSMonitorMemList(SCMSMonitorMem scmsMonitorMem){
        return scmsMonitorMemDao.getSCMSMonitorMemList(scmsMonitorMem);
    }
}
