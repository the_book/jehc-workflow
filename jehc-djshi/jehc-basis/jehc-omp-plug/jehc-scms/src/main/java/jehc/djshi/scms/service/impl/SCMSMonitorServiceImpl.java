package jehc.djshi.scms.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.idgeneration.SnowflakeIdWorker;
import jehc.djshi.scms.dao.SCMSMonitorCpuDao;
import jehc.djshi.scms.dao.SCMSMonitorDao;
import jehc.djshi.scms.dao.SCMSMonitorMemDao;
import jehc.djshi.scms.model.SCMSMonitor;
import jehc.djshi.scms.model.SCMSMonitorCpu;
import jehc.djshi.scms.model.SCMSMonitorMem;
import jehc.djshi.scms.service.SCMSMonitorCpuService;
import jehc.djshi.scms.service.SCMSMonitorMemService;
import jehc.djshi.scms.service.SCMSMonitorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Desc 监控主表
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service
@Slf4j
public class SCMSMonitorServiceImpl extends BaseService implements jehc.djshi.scms.service.SCMSMonitorService {
    @Autowired
    SCMSMonitorDao scmsMonitorDao;

    @Autowired
    SnowflakeIdWorker snowflakeIdWorker;

    @Autowired
    SCMSMonitorMemDao scmsMonitorMemDao;

    @Autowired
    SCMSMonitorCpuDao scmsMonitorCpuDao;
    /**
     * 初始化分页
     * @param condition
     * @return
     */
    public List<SCMSMonitor> getSCMSMonitorListByCondition(Map<String,Object> condition){
        return scmsMonitorDao.getSCMSMonitorListByCondition(condition);
    }

    /**
     * 查询对象
     * @param id
     * @return
     */
    public SCMSMonitor getSCMSMonitorById(String id){
        return scmsMonitorDao.getSCMSMonitorById(id);
    }

    /**
     * 添加
     * @param scmsMonitor
     * @return
     */
    public int addSCMSMonitor(SCMSMonitor scmsMonitor){
        return scmsMonitorDao.addSCMSMonitor(scmsMonitor);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delSCMSMonitor(Map<String,Object> condition){
        return scmsMonitorDao.delSCMSMonitor(condition);
    }

    /**
     * 根据条件修改
     * @param scmsMonitor
     * @return
     */
    public int updateSCMSMonitorBySelective(SCMSMonitor scmsMonitor){
        return scmsMonitorDao.updateSCMSMonitorBySelective(scmsMonitor);
    }

    /**
     * 保存上报信息
     * @param scmsMonitor
     * @return
     */
    public int saveSCMSMonitor(SCMSMonitor scmsMonitor){
        scmsMonitor.setId(""+snowflakeIdWorker.nextId());
        int i = addSCMSMonitor(scmsMonitor);
        SCMSMonitorMem scmsMonitorMem = scmsMonitor.getScmsMonitorMem();
        List<SCMSMonitorCpu> scmsMonitorCpuList = scmsMonitor.getScmsMonitorCpus();
        if(null != scmsMonitorMem){
            scmsMonitorMem.setId(""+snowflakeIdWorker.nextId());
            scmsMonitorMem.setMac(scmsMonitor.getMac());
            scmsMonitorMemDao.addSCMSMonitorMem(scmsMonitorMem);
        }
        if(!CollectionUtil.isEmpty(scmsMonitorCpuList)){
            for(SCMSMonitorCpu scmsMonitorCpu: scmsMonitorCpuList){
                scmsMonitorCpu.setId(""+snowflakeIdWorker.nextId());
                scmsMonitorCpu.setMac(scmsMonitor.getMac());
                scmsMonitorCpuDao.addSCMSMonitorCpu(scmsMonitorCpu);
            }
        }
        return i;
    }

    /**
     *根据Mac查询单个服务器主信息
     * @param mac
     * @return
     */
    public SCMSMonitor getSCMSMonitorByMac(String mac){
        return scmsMonitorDao.getSCMSMonitorByMac(mac);
    }
}
