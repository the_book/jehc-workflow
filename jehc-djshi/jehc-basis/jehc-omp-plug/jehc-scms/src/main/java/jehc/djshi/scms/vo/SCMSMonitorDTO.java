package jehc.djshi.scms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 监控信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="监控信息对象", description="监控信息")
public class SCMSMonitorDTO {

    @ApiModelProperty(value = "key")
    private String key;

    @ApiModelProperty(value = "value")
    private String value;

    public SCMSMonitorDTO(){

    }

    public SCMSMonitorDTO(String key, String value){
        this.key = key;
        this.value = value;
    }
}
