package jehc.djshi.scms.web;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.common.util.date.DateUtils;
import jehc.djshi.scms.model.SCMSMonitorCpu;
import jehc.djshi.scms.model.SCMSMonitorMem;
import jehc.djshi.scms.service.SCMSMonitorCpuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Desc 服务器CPU运行
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/scms/monitorCpu")
@Api(value = "服务器CPU运行监控",tags = "服务器CPU运行监控",description = "服务器CPU运行监控")
public class SCMSMonitorCpuController extends BaseAction {

    @Autowired
    SCMSMonitorCpuService scmsMonitorCpuService;

    /**
     * 查询服务器CPU运行监控列表并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询服务器CPU运行监控列表并分页", notes="查询服务器CPU运行监控列表并分页")
    public BasePage<List<SCMSMonitorCpu>> getSCMSMonitorCpuListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<SCMSMonitorCpu> scmsMonitorCpuList = scmsMonitorCpuService.getSCMSMonitorCpuListByCondition(condition);
        PageInfo<SCMSMonitorCpu> page = new PageInfo<SCMSMonitorCpu>(scmsMonitorCpuList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个服务器CPU运行监控
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    @ApiOperation(value="查询单个服务器CPU运行监控", notes="查询单个服务器CPU运行监控")
    public BaseResult<SCMSMonitorCpu> getSCMSMonitorCpuById(@PathVariable("id")String id){
        SCMSMonitorCpu scmsMonitorCpu = scmsMonitorCpuService.getSCMSMonitorCpuById(id);
        return outDataStr(scmsMonitorCpu);
    }

    /**
     * 删除
     * @param id
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="删除", notes="删除")
    public BaseResult delSCMSMonitorCpu(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=scmsMonitorCpuService.delSCMSMonitorCpu(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * CPU
     * @param scmsMonitorCpu
     */
    @NeedLoginUnAuth
    @PostMapping(value="/cpu")
    @ApiOperation(value="CPU", notes="CPU")
    public BaseResult<Map<String,Object>> mems(SCMSMonitorCpu scmsMonitorCpu){
        if(StringUtil.isEmpty(scmsMonitorCpu.getMac())){
            scmsMonitorCpu.setMac("-1");
        }
        PageHelper.offsetPage(0, 5);
        List<SCMSMonitorCpu> scmsMonitorCpuList = scmsMonitorCpuService.getSCMSMonitorCpuList(scmsMonitorCpu);
        Map<String,Object> map = new HashMap<>();
        List<String> xdata = new ArrayList<>();
        List<String> ydata0 = new ArrayList<>();
        List<String> ydata1 = new ArrayList<>();
        List<String> ydata2 = new ArrayList<>();
        List<String> ydata3 = new ArrayList<>();
        if(!CollectionUtil.isEmpty(scmsMonitorCpuList)){
            for(SCMSMonitorCpu monitorCpu:scmsMonitorCpuList){
                xdata.add(getTime(monitorCpu.getCreate_time()));
                try {
                    ydata0.add(StringUtil.decimalFormat(new Double(monitorCpu.getCpu_user_use_rate())));
                    ydata1.add(StringUtil.decimalFormat(new Double(monitorCpu.getCpu_sys_use_rate())));
                    ydata2.add(StringUtil.decimalFormat(new Double(monitorCpu.getCpu_use_rate())));
                    ydata3.add(StringUtil.decimalFormat(new Double(monitorCpu.getCpu_currently_idle())));
                }catch (Exception e){
                    ydata0.add("0");
                    ydata1.add("0");
                    ydata2.add("0");
                    ydata3.add("0");
                }
            }
        }
        map.put("xdata",xdata);
        map.put("ydata0",ydata0);
        map.put("ydata1",ydata1);
        map.put("ydata2",ydata2);
        map.put("ydata3",ydata3);
        return BaseResult.success(map);
    }

    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     */
    public static String getTime(Date date) {
        return DateUtils.formatDate(date, "HH:mm:ss");
    }
}
