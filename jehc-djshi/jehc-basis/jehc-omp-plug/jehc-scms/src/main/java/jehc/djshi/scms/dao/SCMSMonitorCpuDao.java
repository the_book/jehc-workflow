package jehc.djshi.scms.dao;

import jehc.djshi.scms.model.SCMSMonitorCpu;

import java.util.List;
import java.util.Map;

/**
 * @Desc 监控CPU
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface SCMSMonitorCpuDao {

    /**
     * 查询监控CPU列表
     * @param condition
     * @return
     */
    List<SCMSMonitorCpu> getSCMSMonitorCpuListByCondition(Map<String,Object> condition);

    /**
     * 查询对象
     * @param id
     * @return
     */
    SCMSMonitorCpu getSCMSMonitorCpuById(String id);

    /**
     * 添加
     * @param scmsMonitorCpu
     * @return
     */
    int addSCMSMonitorCpu(SCMSMonitorCpu scmsMonitorCpu);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delSCMSMonitorCpu(Map<String,Object> condition);

    /**
     * 查询监控CPU列表
     * @return
     */
    List<SCMSMonitorCpu> getSCMSMonitorCpuList(SCMSMonitorCpu scmsMonitorCpu);
}
