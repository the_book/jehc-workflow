package jehc.djshi.oauth.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.util.Date;
import java.util.List;
/**
 * @Desc 账号管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data@ApiModel(value="账号管理对象", description="账号管理")
public class OauthAccount extends BaseEntity {

    @ApiModelProperty(value = "账号ID与UserID一致")
    private String account_id;//账号ID与UserID一致

    @ApiModelProperty(value = "登录名")
    private String account;//登录名

    @ApiModelProperty(value = "密码")
    private String password;//密码

    @ApiModelProperty(value = "姓名")
    private String name;//姓名

    @ApiModelProperty(value = "账号类型")
    private String account_type_id;//账号类型

    @ApiModelProperty(value = "账号类型别名")
    private String account_type_text;

    @ApiModelProperty(value = "状态0正常1锁定2禁用")
    private Integer status;//状态0正常1锁定2禁用

    @ApiModelProperty(value = "电子邮件")
    private String email;//电子邮件

    @ApiModelProperty(value = "最后一次登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date last_login_time;//最后一次登录时间

    @ApiModelProperty(value = "对象信息如：会员的对象json格式存储")
    private String info_body;//对象信息如：会员的对象json格式存储

    @ApiModelProperty(value = "账户类型条件 “并且”，“或”")
    private Integer typeCondition;//账户类型条件 “并且”，“或”

    @ApiModelProperty(value = "账号类型集合")
    private List<OauthAccountType> oauthAccountTypes;//账号类型集合

    /**
     *
     */
    public OauthAccount(){ }
}
