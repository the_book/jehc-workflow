package jehc.djshi.oauth.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 授权中心子系统模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="授权中心子系统模块对象", description="授权中心子系统模块")
public class OauthSysModules extends BaseEntity{

	@ApiModelProperty(value = "id")
	private String sys_modules_id;/****/

	@ApiModelProperty(value = "模块名")
	private String sys_modules_name;/**模块名**/

	@ApiModelProperty(value = "模块标记不可修改")
	private String sys_modules_mode;/**模块标记不可修改**/

	@ApiModelProperty(value = "状态")
	private int sys_modules_status;/**状态**/

	@ApiModelProperty(value = "子系统id外键")
	private String sysmode_id;/**子系统id外键**/

	@ApiModelProperty(value = "密钥keyid")
	private String keyid;

	@ApiModelProperty(value = "子系统名称")
	private String sysname;
}
