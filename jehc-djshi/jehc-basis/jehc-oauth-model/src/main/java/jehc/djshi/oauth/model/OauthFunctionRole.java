package jehc.djshi.oauth.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 授权中心功能对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="授权中心功能对角色对象", description="授权中心功能对角色")
public class OauthFunctionRole extends BaseEntity{

	@ApiModelProperty(value = "id")
	private String function_role_id;/**主键**/

	@ApiModelProperty(value = "功能编号外键（即功能表主键）")
	private String function_info_id;/**功能编号外键（即功能表主键）**/

	@ApiModelProperty(value = "角色编号外键（即角色表主键）")
	private String role_id;/**角色编号外键（即角色表主键）**/

	@ApiModelProperty(value = "资源编号外键（即资源表主键）")
	private String resources_id;/**资源编号外键（即资源表主键）**/

	@ApiModelProperty(value = "拦截地址")
	private String function_info_url;

	@ApiModelProperty(value = "方法")
	private String function_info_method;

	@ApiModelProperty(value = "是否拦截")
	private int isfilter;

	@ApiModelProperty(value = "是否数据权限")
	private int isAuthority;
}
