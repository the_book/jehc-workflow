package jehc.djshi.oauth.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 功能中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="功能中心对象", description="功能中心")
public class OauthFunctionInfo extends BaseEntity{

	@ApiModelProperty(value = "id")
	private String function_info_id;/**编号**/

	@ApiModelProperty(value = "功能名称")
	private String function_info_name;/**功能名称**/

	@ApiModelProperty(value = "拦截地址")
	private String function_info_url;/**拦截地址**/

	@ApiModelProperty(value = "拦截方法")
	private String function_info_method;/**拦截方法**/

	@ApiModelProperty(value = "子系统标记外键")
	private String sys_id;/**子系统标记外键**/

	@ApiModelProperty(value = "模块编号（即模块表主键）")
	private String modules_id;/**模块编号（即模块表主键）**/

	@ApiModelProperty(value = "是否平台模块")
	private int issysmodules;/**是否平台模块**/

	@ApiModelProperty(value = "是否拦截")
	private int isfilter;/**是否拦截**/

	@ApiModelProperty(value = "资源编号外键即菜单表主键")
	private String menu_id;/**资源编号外键即菜单表主键**/

	@ApiModelProperty(value = "是否数据权限0是1否")
	private int isAuthority;//是否数据权限0是1否

	@ApiModelProperty(value = "状态0正常1禁用")
	private int status;//状态0正常1禁用

	@ApiModelProperty(value = "资源名称")
	private String resources_title;
}
