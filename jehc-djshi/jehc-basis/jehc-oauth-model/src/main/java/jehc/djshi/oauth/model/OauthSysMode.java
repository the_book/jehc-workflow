package jehc.djshi.oauth.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 授权中心子系统标记
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="授权中心子系统标记对象", description="授权中心子系统标记")
public class OauthSysMode extends BaseEntity {

	@ApiModelProperty(value = "主键")
	private String sys_mode_id;/****/

	@ApiModelProperty(value = "系统标记不可更改")
	private String sysmode;/**系统标记不可更改**/

	@ApiModelProperty(value = "系统名称")
	private String sysname;/**系统名称**/

	@ApiModelProperty(value = "状态正常/禁用")
	private int sys_mode_status;/**状态正常/禁用**/

	@ApiModelProperty(value = "秘钥ID外键")
	private String key_info_id;/**秘钥ID外键**/

	@ApiModelProperty(value = "排序编号")
	private int sort;/**排序编号**/

	@ApiModelProperty(value = "隶属平台图标")
	private String sys_mode_icon;/**隶属平台图标**/

	@ApiModelProperty(value = "平台地址路径")
	private String sys_mode_url;/**平台地址路径**/
}
