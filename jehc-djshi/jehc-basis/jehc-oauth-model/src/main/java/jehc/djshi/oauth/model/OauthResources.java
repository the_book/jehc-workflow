package jehc.djshi.oauth.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 授权中心资源中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="授权中心资源中心对象", description="授权中心资源中心")
public class OauthResources extends BaseEntity{

	@ApiModelProperty(value = "id")
	private String resources_id;/**主键**/

	@ApiModelProperty(value = "名称")
	private String resources_title;/**名称**/

	@ApiModelProperty(value = "访问资源地址")
	private String resources_parentid;/**访问资源地址**/

	@ApiModelProperty(value = "图标")
	private String resources_iconCls;/**图标**/

	@ApiModelProperty(value = "是否叶子")
	private int resources_leaf;/**是否叶子**/

	@ApiModelProperty(value = "图片路路径")
	private String resources_images;/**图片路路径**/

	@ApiModelProperty(value = "排序")
	private int resources_sort;/**排序**/

	@ApiModelProperty(value = "是否是系统菜单1是")
	private int resources_sys;/**是否是系统菜单1是/0否**/

	@ApiModelProperty(value = "状态0可用/1禁用")
	private int resources_status;/**状态0可用/1禁用**/

	@ApiModelProperty(value = "所属子系统模块外键（即资源模块表主键）")
	private String resources_module_id;/**所属子系统模块外键（即资源模块表主键）**/

	@ApiModelProperty(value = "keyid")
	private String keyid;

	@ApiModelProperty(value = "资源uri")
	private String resources_url;

	@ApiModelProperty(value = "资源父名称")
	private String resources_parentidTitle_;

	@ApiModelProperty(value = "隶属平台编号")
	private String sys_mode_id;/**隶属平台编号**/

	@ApiModelProperty(value = "隶属平台名称")
	private String sysname;/**隶属平台名称**/

	@ApiModelProperty(value = "隶属平台图标")
	private String sys_mode_icon;/**隶属平台图标**/

	@ApiModelProperty(value = "平台地址路径")
	private String sys_mode_url;/**平台地址路径**/

	@ApiModelProperty(value = "模块排序号")
	private int mode_sort;/**模块排序号**/

	@ApiModelProperty(value = "组件路由")
	private String component_router;/**组件路由**/

	@ApiModelProperty(value = "组件地址即转发")
	private String component_router_to;/**组件地址即转发**/

	@ApiModelProperty(value = "组件图标")
	private String component_icon;/**组件图标**/

	@ApiModelProperty(value = "跳转方式： 0站内 1站外 2 iframe")
	private int component_jump_type;/**跳转方式： 0站内 1站外 2 iframe**/

	@ApiModelProperty(value = "是否隐藏 0否 1是")
	private int component_hide_menu;/**是否隐藏 0否 1是**/

	@ApiModelProperty(value = "是否打开新页面 0是 1否")
	private int component_open_new_page;/**是否打开新页面 0是 1否**/

	@ApiModelProperty(value = "激活展开样式（注意存在子菜单时候展开菜单后是否激活样式）")
	private String component_open_style;/**激活展开样式（注意存在子菜单时候展开菜单后是否激活样式）**/
}
