package jehc.djshi.oauth.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 授权中心公共功能
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="授权中心公共功能对象", description="授权中心公共功能")
public class OauthFunctionCommon extends BaseEntity {

	@ApiModelProperty(value = "id")
	private String function_common_id;/****/

	@ApiModelProperty(value = "标题")
	private String function_common_title;/**标题**/

	@ApiModelProperty(value = "功能地址")
	private String function_common_url;/**功能地址**/

	@ApiModelProperty(value = "方法名称")
	private String function_common_method;/**方法名称**/

	@ApiModelProperty(value = "创建人")
	private String userName;/**创建人**/

	@ApiModelProperty(value = "状态0启用1禁用")
	private int function_common_status;/**状态0启用1禁用**/

	@ApiModelProperty(value = "备注")
	private String function_common_content;/**备注**/

	@ApiModelProperty(value = "子系统id外键（即只有该系统下才能使用）")
	private String sysmode_id;/**子系统id外键（即只有该系统下才能使用）**/

	@ApiModelProperty(value = "子系统名称")
	private String sysname;
}
