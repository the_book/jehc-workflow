package jehc.djshi.oauth.vo;

import io.netty.channel.Channel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @Desc 通道信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="通道信息对象", description="通道信息")
public class ChannelEntity {

    @ApiModelProperty(value = "通道号")
    Channel channel;

    @ApiModelProperty(value = "客户端id")
    private String clientId;

    @ApiModelProperty(value = "组 可用于客户端集群中只有一个可被激活")
    private String clientGroupId;//组 可用于客户端集群中只有一个可被激活

    @ApiModelProperty(value = "请求体对象")
    private RequestInfo requestInfo;

    public ChannelEntity(){

    }

    public ChannelEntity(Channel channel){
        this.channel = channel;
    }

    public ChannelEntity(Channel channel, String clientGroupId){
        this.channel = channel;
        this.clientId = clientGroupId;
    }

    public ChannelEntity(Channel channel, String clientGroupId, String clientId){
        this.channel = channel;
        this.clientId = clientGroupId;
        this.clientId = clientId;
    }

    public ChannelEntity(Channel channel, String clientGroupId, String clientId, RequestInfo requestInfo){
        this.channel = channel;
        this.clientGroupId = clientGroupId;
        this.clientId = clientId;
        this.requestInfo =requestInfo;
    }
}
