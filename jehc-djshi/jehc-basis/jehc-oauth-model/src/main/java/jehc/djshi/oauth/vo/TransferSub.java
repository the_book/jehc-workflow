package jehc.djshi.oauth.vo;

import lombok.Data;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class TransferSub extends Transfer {
    private String info;
    private boolean status = true;

    public TransferSub(){

    }
    public TransferSub( String token,boolean status,String info){
        this.token = token;
        this.status = status;
        this.info = info;
    }
}
