package jehc.djshi.oauth.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
/**
 * @Desc 授权中心密钥
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="授权中心密钥对象", description="授权中心密钥")
public class OauthKeyInfo extends BaseEntity{

	@ApiModelProperty(value = "id")
	private String key_info_id;/**主键**/

	@ApiModelProperty(value = "标题")
	private String title;/**标题**/

	@ApiModelProperty(value = "名称")
	private String key_name;/**名称**/

	@ApiModelProperty(value = "秘钥")
	private String key_pass;/**秘钥**/

	@ApiModelProperty(value = "有效期")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	private Date key_exp_date;/**有效期**/

	@ApiModelProperty(value = "是否采用有效期默认False")
	private int isUseExpDate;/**是否采用有效期默认False**/

	@ApiModelProperty(value = "状态 正常/冻结/禁用")
	private int status;/**状态 正常/冻结/禁用**/
}
