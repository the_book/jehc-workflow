package jehc.djshi.oauth.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 授权中心账户对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="授权中心账户对角色对象", description="授权中心账户对角色")
public class OauthAccountRole extends BaseEntity {

	@ApiModelProperty(value = "id")
	private String account_role_id;/****/

	@ApiModelProperty(value = "角色ID外键")
	private String role_id;/**角色ID外键**/

	@ApiModelProperty(value = "账号ID外键")
	private String account_id;/**账号ID外键**/

	@ApiModelProperty(value = "角色名称")
	private String role_name;
}
