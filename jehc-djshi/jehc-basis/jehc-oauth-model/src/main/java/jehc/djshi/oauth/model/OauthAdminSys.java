package jehc.djshi.oauth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;
/**
 * @Desc 管理员拥有系统范围
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="管理员拥有系统范围对象", description="管理员拥有系统范围")
public class OauthAdminSys extends BaseEntity {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "子系统Id 外键")
    private String sysmode_id;//子系统Id 外键

    @ApiModelProperty(value = "admin_id")
    private String admin_id;

    @ApiModelProperty(value = "子系统名称")
    private String sysname;

    @ApiModelProperty(value = "隶属子系统")
    private String sysmode;

    @ApiModelProperty(value = "名称")
    private String key_name;/**名称**/

    @ApiModelProperty(value = "秘钥")
    private String key_pass;/**秘钥**/

    @ApiModelProperty(value = "有效期")
    private Date key_exp_date;/**有效期**/

    @ApiModelProperty(value = "是否采用有效期默认False")
    private int isUseExpDate;/**是否采用有效期默认False**/

    public OauthAdminSys(){}
}
