package jehc.djshi.oauth.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 授权中心资源对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="授权中心资源对角色对象", description="授权中心资源对角色")
public class OauthResourcesRole extends BaseEntity{

	@ApiModelProperty(value = "id")
	private String resources_role_id;/****/

	@ApiModelProperty(value = "角色ID外键")
	private String role_id;/**角色ID外键**/

	@ApiModelProperty(value = "资源ID外键")
	private String resources_id;/**资源ID外键**/
}
