package jehc.djshi.oauth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 账号类型
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="账号类型对象", description="账号类型")
public class OauthAccountType extends BaseEntity {

    @ApiModelProperty(value = "id")
    private String account_type_id;

    @ApiModelProperty(value = "子系统")
    private String sys_mode_id;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "key")
    private String keyname;

    @ApiModelProperty(value = "描述")
    private String content;

    @ApiModelProperty(value = "状态")
    private int status;

    @ApiModelProperty(value = "隶属系统名称")
    private String sysname;
}
