package jehc.djshi.oauth.constant;

/**
 * @Desc 常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class Constant {
    public static final String OAUTH_CHANGED = "OAUTH_CHANGED";//权限变更
    public static final String TOKEN_DESTORY = "TOKEN_DESTORY";//Token销毁
}
