package jehc.djshi.oauth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @Desc 管理员中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="管理员中心对象", description="管理员中心")
public class OauthAdmin extends OauthAccount{

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "账户id")
    private String account_id;

    @ApiModelProperty(value = "级别：全平台管理员/子系统管理员")
    private Integer level;//级别：全平台管理员/子系统管理员

    @ApiModelProperty(value = "模块id")
    private String sys_mode_id;//模块id
}
