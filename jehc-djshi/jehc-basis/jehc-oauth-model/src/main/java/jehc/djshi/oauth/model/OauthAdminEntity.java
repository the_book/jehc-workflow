package jehc.djshi.oauth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="管理员中心Entity对象", description="管理员中心")
public class OauthAdminEntity extends BaseEntity {

    @ApiModelProperty(value = "id")
    private String account_id;

    @ApiModelProperty(value = "级别：全平台管理员/子系统管理员")
    private int level;
}
