package jehc.djshi.log.client.worker;

import jehc.djshi.common.util.SpringUtils;
import jehc.djshi.log.dao.LogModifyRecordDao;
import jehc.djshi.log.model.LogModifyRecord;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 记录变更工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogModifyRecordWorker implements Runnable {

    private List<LogModifyRecord> logModifyRecords;

    private HttpServletRequest request;

    /**
     * 无参构造函数
     */
    public LogModifyRecordWorker(){
    }

    /**
     * 有参构造函数
     * @param logModifyRecords
     */
    public LogModifyRecordWorker(List<LogModifyRecord> logModifyRecords){
        this.logModifyRecords = logModifyRecords;
    }

    /**
     * 有参构造函数
     * @param logModifyRecords
     * @param request
     */
    public LogModifyRecordWorker(List<LogModifyRecord> logModifyRecords, HttpServletRequest request){
        this.logModifyRecords = logModifyRecords;
        this.request = request;
    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录变更记录失败:"+logModifyRecords.toString()+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            LogModifyRecordDao logModifyRecordDao = SpringUtils.getBean(LogModifyRecordDao.class);
            log.info("记录变更记录开始");
            for(LogModifyRecord logModifyRecord: logModifyRecords){
                logModifyRecordDao.addLogModifyRecord(logModifyRecord);
            }
            log.info("记录变更记录结束");
        } catch (Exception e) {
            log.info("记录变更记录失败:"+logModifyRecords.toString()+"，异常信息：{}",e);
        }
    }
}
