package jehc.djshi.log.client.worker;

import jehc.djshi.common.util.SpringUtils;
import jehc.djshi.log.dao.LogOperateDao;
import jehc.djshi.log.model.LogOperate;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 操作日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogOperateWorker implements Runnable {

    private LogOperate logOperate;//操作日志传输对象

    private HttpServletRequest request;//操作日志传输对象

    public LogOperateWorker(LogOperate logOperate){
        this.logOperate = logOperate;
    }

    public LogOperateWorker(LogOperate logOperate, HttpServletRequest request){
        this.logOperate = logOperate;
        this.request = request;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录日志失败:"+logOperate.toString()+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
		try {
            LogOperateDao logOperateDao = SpringUtils.getBean(LogOperateDao.class);
			log.info("记录操作日志开始");
            logOperateDao.addLogOperate(logOperate);
            log.info("记录操作日志结束");
		} catch (Exception e) {
			log.info("记录操作日志失败:"+logOperate.toString()+"，异常信息：{}",e);
		}
    }
}
