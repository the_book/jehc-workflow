package jehc.djshi.log.client.worker;

import jehc.djshi.common.util.SpringUtils;
import jehc.djshi.log.dao.LogStartStopDao;
import jehc.djshi.log.model.LogStartStop;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 服务器启动关闭日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogStartStopWorker implements Runnable{

    LogStartStop logStartStop;//日志信息

    HttpServletRequest request;//请求

    public LogStartStopWorker(){

    }

    /**
     *
     * @param logStartStop
     * @param request
     */
    public LogStartStopWorker(LogStartStop logStartStop, HttpServletRequest request){
        this.logStartStop = logStartStop;
        this.request = request;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录异常日志失败:"+logStartStop+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            LogStartStopDao logStartStopDao = SpringUtils.getBean(LogStartStopDao.class);
            log.info("记录启动关闭日志失败开始");
            logStartStopDao.addLogStartStop(logStartStop);
            log.info("记录启动关闭日志失败结束");
        } catch (Exception e) {
            log.info("记录启动关闭日志失败失败:"+logStartStop+"，异常信息：{}",e);
        }
    }
}
