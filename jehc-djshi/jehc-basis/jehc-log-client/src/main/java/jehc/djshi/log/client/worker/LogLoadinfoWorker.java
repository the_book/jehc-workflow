package jehc.djshi.log.client.worker;
import jehc.djshi.common.util.SpringUtils;
import jehc.djshi.log.dao.LogLoadinfoDao;
import jehc.djshi.log.model.LogLoadinfo;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 页面监控日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogLoadinfoWorker implements Runnable {

    LogLoadinfo logLoadinfo;//页面监控日志信息

    HttpServletRequest request;//请求

    public LogLoadinfoWorker(){

    }

    /**
     *
     * @param logLoadinfo
     * @param request
     */
    public LogLoadinfoWorker(LogLoadinfo logLoadinfo, HttpServletRequest request){
        this.logLoadinfo = logLoadinfo;
        this.request = request;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录页面监控日志失败:"+logLoadinfo+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            log.info("记录页面监控日志失败开始");
            LogLoadinfoDao logLoadinfoDao = SpringUtils.getBean(LogLoadinfoDao.class);
            logLoadinfoDao.addLogLoadinfo(logLoadinfo);
            log.info("记录页面监控日志失败结束");
        } catch (Exception e) {
            log.info("记录页面监控日志失败失败:"+logLoadinfo+"，异常信息：{}",e);
        }
    }
}
