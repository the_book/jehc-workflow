package jehc.djshi.sys.dao;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.model.XtEncoderqrcode;

/**
 * @Desc 平台二维码
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtEncoderqrcodeDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtEncoderqrcode> getXtEncoderqrcodeListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_encoderQRCode_id 
	* @return
	*/
	XtEncoderqrcode getXtEncoderqrcodeById(String xt_encoderQRCode_id);
	/**
	* 添加
	* @param xtEncoderqrcode
	* @return
	*/
	int addXtEncoderqrcode(XtEncoderqrcode xtEncoderqrcode);
	/**
	* 修改
	* @param xtEncoderqrcode
	* @return
	*/
	int updateXtEncoderqrcode(XtEncoderqrcode xtEncoderqrcode);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtEncoderqrcode(Map<String,Object> condition);
}
