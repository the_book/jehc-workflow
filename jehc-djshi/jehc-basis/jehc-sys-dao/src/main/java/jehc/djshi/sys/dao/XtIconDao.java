package jehc.djshi.sys.dao;

import jehc.djshi.sys.model.XtIcon;
import jehc.djshi.sys.param.XtIconForm;

import java.util.List;
import java.util.Map;

/**
 * @Desc 字体图标库
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtIconDao {

    /**
     * 初始化分页
     * @param condition
     * @return
     */
    List<XtIcon> getXtIconListByCondition(Map<String,Object> condition);

    /**
     * 查询单个对象
     * @param id
     * @return
     */
    XtIcon getXtIconById(String id);

    /**
     * 新增
     * @param xtIcon
     * @return
     */
    int addXtIcon(XtIcon xtIcon);

    /**
     * 修改
     * @param xtIcon
     * @return
     */
    int updateXtIcon(XtIcon xtIcon);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delXtIcon(Map<String,Object> condition);

    /**
     * 查询集合
     * @param xtIconForm
     * @return
     */
    List<XtIcon> getXtIconList(XtIconForm xtIconForm);
}
