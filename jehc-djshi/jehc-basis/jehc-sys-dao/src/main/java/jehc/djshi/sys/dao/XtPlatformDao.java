package jehc.djshi.sys.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.sys.model.XtPlatform;

/**
 * @Desc 平台信息发布
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtPlatformDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtPlatform> getXtPlatformListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param xt_platform_id 
	* @return
	*/
	XtPlatform getXtPlatformById(String xt_platform_id);
	/**
	* 添加
	* @param xtPlatform 
	* @return
	*/
	int addXtPlatform(XtPlatform xtPlatform);
	/**
	* 修改
	* @param xtPlatform 
	* @return
	*/
	int updateXtPlatform(XtPlatform xtPlatform);
	/**
	* 修改（根据动态条件）
	* @param xtPlatform 
	* @return
	*/
	int updateXtPlatformBySelective(XtPlatform xtPlatform);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtPlatform(Map<String, Object> condition);
	/**
	* 批量添加
	* @param xtPlatformList 
	* @return
	*/
	int addBatchXtPlatform(List<XtPlatform> xtPlatformList);
	/**
	* 批量修改
	* @param xtPlatformList 
	* @return
	*/
	int updateBatchXtPlatform(List<XtPlatform> xtPlatformList);
	/**
	* 批量修改（根据动态条件）
	* @param xtPlatformList 
	* @return
	*/
	int updateBatchXtPlatformBySelective(List<XtPlatform> xtPlatformList);
}
