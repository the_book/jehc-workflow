package jehc.djshi.sys.dao;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.model.XtDepartinfo;

/**
 * @Desc 部门信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtDepartinfoDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtDepartinfo> getXtDepartinfoListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_departinfo_id 
	* @return
	*/
	XtDepartinfo getXtDepartinfoById(String xt_departinfo_id);
	/**
	* 添加
	* @param xtDepartinfo
	* @return
	*/
	int addXtDepartinfo(XtDepartinfo xtDepartinfo);
	/**
	* 修改
	* @param xtDepartinfo
	* @return
	*/
	int updateXtDepartinfo(XtDepartinfo xtDepartinfo);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtDepartinfo(Map<String,Object> condition);
	
	/**
	 * 部门根目录集合
	 * @return
	 */
	List<XtDepartinfo> getXtDepartinfoList();
	
	/**
	 * 查找子集合
	 * @param condition
	 * @return
	 */
	List<XtDepartinfo> getXtDepartinfoListChild(Map<String,Object> condition);
	
	/**
	 * 查找所有集合
	 * @param condition
	 * @return
	 */
	List<XtDepartinfo> getXtDepartinfoListAll(Map<String,Object> condition);
	
	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理组 发起组等使用）
	 * @param condition
	 * @return
	 */
	List<XtDepartinfo> queryXtDepartinfoList(Map<String,Object> condition);
}
