package jehc.djshi.job.client;

import io.netty.channel.Channel;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.job.vo.RequestInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
/**
 * @Desc Netty客户端工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Component
public class NettyClientUtil {
    /**
     * 发送消息
     * @param channel
     * @param requestInfo
     * @return
     */
    public boolean sendMessage(Channel channel,RequestInfo requestInfo){
        boolean result = true;
        try {
            if(null == channel){
                log.info("未能获取到通道");
                result = false;
            }else{
                channel.writeAndFlush(JsonUtil.toJson(requestInfo));
            }
        }catch (Exception e){
            log.error("发送消息异常：",e);
            result =  false;
        }
        return result;
    }
}
