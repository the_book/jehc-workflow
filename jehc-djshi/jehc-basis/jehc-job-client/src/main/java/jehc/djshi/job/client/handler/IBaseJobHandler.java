package jehc.djshi.job.client.handler;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.job.vo.ParamInfo;

/**
 * @Desc 任务处理Handler
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public abstract class IBaseJobHandler {
    public IBaseJobHandler() {
    }

    public abstract BaseResult<String> execute(ParamInfo paramInfo) throws Exception;

    public void init() {
    }

    public void destroy() {
    }
}
