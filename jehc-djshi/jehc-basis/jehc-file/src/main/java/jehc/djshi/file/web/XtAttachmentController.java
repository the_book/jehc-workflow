package jehc.djshi.file.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.file.model.XtAttachment;
import jehc.djshi.file.param.AttachmentForm;
import jehc.djshi.file.service.XtAttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 附件管理 
* 2015-05-24 08:36:53  邓纯杰
*/
@RestController
@RequestMapping("/xtAttachment")
@Api(value = "平台附件API",tags = "平台附件API",description = "平台附件API")
public class XtAttachmentController extends BaseAction {

	@Autowired
	private XtAttachmentService xtAttachmentService;

	/**
	 * 加载初始化列表数据并分页
	 * @param baseSearch
	 */
	@PostMapping(value="/list")
	@NeedLoginUnAuth
	@ApiOperation(value="查询附件列表并分页", notes="查询附件列表并分页")
	public BasePage<List<XtAttachment>> getXtAttachmentListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtAttachment> xtAttachmentList = xtAttachmentService.getXtAttachmentListByCondition(condition);
		PageInfo<XtAttachment> page = new PageInfo<XtAttachment>(xtAttachmentList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 查询单个附件
	 * @param xt_attachment_id
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_attachment_id}")
	@ApiOperation(value="查询单个附件", notes="查询单个附件")
	public BaseResult<XtAttachment> getXtAttachmentById(@PathVariable("xt_attachment_id")String xt_attachment_id){
		XtAttachment xtAttachment = xtAttachmentService.getXtAttachmentById(xt_attachment_id);
		return outDataStr(xtAttachment);
	}

	/**
	 * 批量查询附件
	 * @param xt_attachment_id
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/list/{xt_attachment_id}")
	@ApiOperation(value="批量查询附件", notes="批量查询附件")
	public BaseResult<List<XtAttachment>> getXtAttachmentList(@PathVariable("xt_attachment_id")String xt_attachment_id){
		Map<String,Object> condition = new HashMap<>();
		condition.put("xt_attachment_id", xt_attachment_id.split(","));
		List<XtAttachment> list = xtAttachmentService.getXtAttachmentList(condition);
		return outDataStr(list);
	}

	/**
	 * 添加
	 * @param xtAttachment
	 */
	@PostMapping(value="/add")
	@ApiOperation(value="创建附件", notes="创建附件")
	public BaseResult addXtAttachment(@RequestBody XtAttachment xtAttachment){
		int i = 0;
		if(null != xtAttachment){
			xtAttachment.setXt_attachment_id(toUUID());
			i=xtAttachmentService.addXtAttachment(xtAttachment);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 修改
	 * @param xtAttachment
	 */
	@PutMapping(value="/update")
	@ApiOperation(value="编辑附件", notes="编辑附件")
	public BaseResult updateXtAttachment(@RequestBody XtAttachment xtAttachment){
		int i = 0;
		if(null !=xtAttachment){
			i=xtAttachmentService.updateXtAttachment(xtAttachment);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 删除
	 * @param xt_attachment_id
	 */
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除附件", notes="删除附件")
	public BaseResult delXtAttachment(String xt_attachment_id){
		int i = 0;
		if(null != xt_attachment_id && !"".equals(xt_attachment_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_attachment_id",xt_attachment_id.split(","));
			i=xtAttachmentService.delXtAttachment(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 批量查询附件
	 * @param attachmentForm
	 */
	@NeedLoginUnAuth
	@PostMapping(value="/batch/list")
	@ApiOperation(value="批量查询附件", notes="批量查询附件")
	public BaseResult<List<XtAttachment>> getBatchAttachmentList(@RequestBody AttachmentForm attachmentForm){
		if(StringUtil.isEmpty(attachmentForm.getXt_attachment_id())){
			return BaseResult.fail("未能获取到附件编号");
		}
		Map<String,Object> condition = new HashMap<>();
		condition.put("xt_attachment_id", attachmentForm.getXt_attachment_id().split(","));
		List<XtAttachment> list = xtAttachmentService.getXtAttachmentList(condition);
		return BaseResult.success(list);
	}
}
