package jehc.djshi.sys.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.*;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtDepartinfo;
import jehc.djshi.sys.model.XtPost;
import jehc.djshi.sys.service.XtDepartinfoService;
import jehc.djshi.sys.service.XtPostService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 平台组织机构
 * @author邓纯杰
 *
 */
@RestController
@RequestMapping("/xtOrg")
@Api(value = "平台组织机构API",tags = "平台组织机构API",description = "平台组织机构API")
public class XtOrgController extends BaseAction {
	@Autowired
	private XtDepartinfoService xtDepartinfoService;
	@Autowired
	private XtPostService xtPostService;
	
	/**
	 * 组织机构树列表
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/list")
	@ApiOperation(value="组织机构树列表", notes="组织机构树列表")
	public BaseResult orgTree(){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListAll(condition);
		for(int i = 0; i < xtDepartinfoList.size(); i++){
			XtDepartinfo xtDepartinfo = xtDepartinfoList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtDepartinfo.getXt_departinfo_id());
			BaseZTreeEntity.setPId(xtDepartinfo.getXt_departinfo_parentId());
			BaseZTreeEntity.setText(xtDepartinfo.getXt_departinfo_name());
			BaseZTreeEntity.setName(xtDepartinfo.getXt_departinfo_name());
			BaseZTreeEntity.setExpanded(true);
			BaseZTreeEntity.setSingleClickExpand(true);
			BaseZTreeEntity.setTempObject("DEPART");
			list.add(BaseZTreeEntity);
		}
		
		//根岗位遍历
		List<XtPost> xtPostList = xtPostService.getXtPostinfoList(condition);
		for(XtPost xtPost:xtPostList){
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtPost.getXt_post_id());
			BaseZTreeEntity.setPId(xtPost.getXt_departinfo_id());
			BaseZTreeEntity.setText(xtPost.getXt_post_name());
			BaseZTreeEntity.setName(xtPost.getXt_post_name());
			BaseZTreeEntity.setExpanded(true);
			BaseZTreeEntity.setSingleClickExpand(true);
			BaseZTreeEntity.setTempObject("POST");
			list.add(BaseZTreeEntity);
		}
		
		//非根岗位
		xtPostList = xtPostService.getXtPostinfoUnRootList(condition);
		for(XtPost xtPost:xtPostList){
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtPost.getXt_post_id());
			BaseZTreeEntity.setPId(xtPost.getXt_post_parentId());
			BaseZTreeEntity.setText(xtPost.getXt_post_name());
			BaseZTreeEntity.setName(xtPost.getXt_post_name());
			BaseZTreeEntity.setExpanded(true);
			BaseZTreeEntity.setSingleClickExpand(true);
			BaseZTreeEntity.setTempObject("POST");
			list.add(BaseZTreeEntity);
		}
		BaseZTreeEntity baseZTreeEntity = new BaseZTreeEntity();
		List<BaseZTreeEntity> baseZTreeEntityList = baseZTreeEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseZTreeEntityList);
		return outStr(json);
	}

	/**
	 * 组织机构树列表
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/tree")
	@ApiOperation(value="组织机构树列表", notes="组织机构树列表")
	public BaseResult getXtOrgTree(){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListAll(condition);
		for(int i = 0; i < xtDepartinfoList.size(); i++){
			XtDepartinfo xtDepartinfo = xtDepartinfoList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtDepartinfo.getXt_departinfo_id());
			BaseZTreeEntity.setPId(xtDepartinfo.getXt_departinfo_parentId());
			BaseZTreeEntity.setText(xtDepartinfo.getXt_departinfo_name());
			BaseZTreeEntity.setName(xtDepartinfo.getXt_departinfo_name());
			BaseZTreeEntity.setExpanded(false);
			BaseZTreeEntity.setSingleClickExpand(true);
			BaseZTreeEntity.setTempObject("DEPART");
			list.add(BaseZTreeEntity);
		}

		//根岗位遍历
		List<XtPost> xtPostList = xtPostService.getXtPostinfoList(condition);
		for(XtPost xtPost:xtPostList){
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtPost.getXt_post_id());
			BaseZTreeEntity.setPId(xtPost.getXt_departinfo_id());
			BaseZTreeEntity.setText(xtPost.getXt_post_name());
			BaseZTreeEntity.setName(xtPost.getXt_post_name());
			BaseZTreeEntity.setExpanded(false);
			BaseZTreeEntity.setSingleClickExpand(true);
			BaseZTreeEntity.setTempObject("POST");
			list.add(BaseZTreeEntity);
		}

		//非根岗位
		xtPostList = xtPostService.getXtPostinfoUnRootList(condition);
		for(XtPost xtPost:xtPostList){
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtPost.getXt_post_id());
			BaseZTreeEntity.setPId(xtPost.getXt_post_parentId());
			BaseZTreeEntity.setText(xtPost.getXt_post_name());
			BaseZTreeEntity.setName(xtPost.getXt_post_name());
			BaseZTreeEntity.setExpanded(false);
			BaseZTreeEntity.setSingleClickExpand(true);
			BaseZTreeEntity.setTempObject("POST");
			list.add(BaseZTreeEntity);
		}
		BaseZTreeEntity baseZTreeEntity = new BaseZTreeEntity();
		List<BaseZTreeEntity> baseZTreeEntityList = baseZTreeEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseZTreeEntityList);
		return outStr(json);
	}
	
	/**
	 * 获取静态部门及岗位组成的树
	 * @param id
	 * @param type
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/static/xtDepartinfo/post/treegrid/{id}/{type}")
	@ApiOperation(value=" 获取静态部门及岗位组成的树", notes=" 获取静态部门及岗位组成的树")
	public BaseResult getStaticDepartinfoAndPostTreeGrid(@PathVariable("id")String id,@PathVariable("type")String type){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListAll(condition);
		List<XtPost> xtPostList = xtPostService.getXtPostListAll(condition);
		List<BaseTreeGridEntity> list = new ArrayList<BaseTreeGridEntity>();
		for(int i = 0; i < xtDepartinfoList.size(); i++){
			XtDepartinfo xtDepartinfo = xtDepartinfoList.get(i);
			BaseTreeGridEntity BaseTreeGridEntity = new BaseTreeGridEntity();
			BaseTreeGridEntity.setId(xtDepartinfo.getXt_departinfo_id());
			BaseTreeGridEntity.setPid(xtDepartinfo.getXt_departinfo_parentId());
			BaseTreeGridEntity.setText(xtDepartinfo.getXt_departinfo_name());
			BaseTreeGridEntity.setExpanded(false);
			BaseTreeGridEntity.setSingleClickExpand(true);
			BaseTreeGridEntity.setTempObject("部门");
			BaseTreeGridEntity.setContent("");
			BaseTreeGridEntity.setIntegerappend(xtDepartinfo.getXt_departinfo_id()+"@"+xtDepartinfo.getXt_departinfo_name());
			BaseTreeGridEntity.setIcon("/deng/images/icons/target.png");
			list.add(BaseTreeGridEntity);
			for(int j = 0; j < xtPostList.size(); j++){
				XtPost xtPost = xtPostList.get(j);
				BaseTreeGridEntity = new BaseTreeGridEntity();
				BaseTreeGridEntity.setId(xtPost.getXt_post_id());
				BaseTreeGridEntity.setText(xtPost.getXt_post_name());
				BaseTreeGridEntity.setExpanded(false);
				BaseTreeGridEntity.setSingleClickExpand(true);
				BaseTreeGridEntity.setTempObject("岗位");
				BaseTreeGridEntity.setIcon("/deng/images/icons/target_point.png");
				BaseTreeGridEntity.setContent("");
				BaseTreeGridEntity.setIntegerappend(xtPost.getXt_departinfo_id()+"@"+xtPost.getXt_departinfo_name());
				if(xtPost.getXt_departinfo_id().equals(xtDepartinfo.getXt_departinfo_id()) && xtPost.getXt_post_parentId().equals("0")){
					BaseTreeGridEntity.setPid(xtPost.getXt_departinfo_id());
				}else if(xtPost.getXt_departinfo_id().equals(xtDepartinfo.getXt_departinfo_id()) && !xtPost.getXt_post_parentId().equals("0")){
					BaseTreeGridEntity.setPid(xtPost.getXt_post_parentId());
				}
				list.add(BaseTreeGridEntity);
			}
		}
		BaseTreeGridEntity baseTreeGridEntity = new BaseTreeGridEntity();
		List<BaseTreeGridEntity> baseTreeGridEntityList = baseTreeGridEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseTreeGridEntityList);
		return outStr(json);
	}
	
	/**
	* 添加或编辑
	* @param xtDepartinfo
	*/
	@PostMapping(value="/xtDepartinfo/saveOrUpdate")
	@ApiOperation(value=" 添加或编辑部门", notes=" 添加或编辑部门")
	public BaseResult saveOrUpdateXtDepartinfo(@RequestBody XtDepartinfo xtDepartinfo){
		int i = 0;
		if(StringUtil.isEmpty(xtDepartinfo.getXt_departinfo_parentId())){
			xtDepartinfo.setXt_departinfo_parentId("0");
		}
		if(StringUtils.isEmpty(xtDepartinfo.getXt_departinfo_id())){
			xtDepartinfo.setXt_departinfo_id(toUUID());
			i=xtDepartinfoService.addXtDepartinfo(xtDepartinfo);
		}else{
			//编辑
			i=xtDepartinfoService.updateXtDepartinfo(xtDepartinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param map
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value=" 删除（多参数传递）", notes=" 删除（多参数传递）")
	public BaseResult delXtOrg(@RequestBody Map<String,String> map){
		int i = 0;
		String tempObject = ""+map.get("tempObject");
		if(StringUtils.isEmpty(tempObject)){
			throw new ExceptionUtil("未能获取到tempObject");
		}
		String id = ""+map.get("id");
		if(StringUtils.isEmpty(id)){
			throw new ExceptionUtil("未能获取到id");
		}
		if(tempObject.equals("DEPART")){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_departinfo_id",id.split(","));
			i=xtDepartinfoService.delXtDepartinfo(condition);
		}else if(tempObject.equals("POST")){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_post_id",id.split(","));
			i=xtPostService.delXtPost(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 添加
	* @param xtPost
	*/
	@PostMapping(value="/xtPost/saveOrUpdate")
	@ApiOperation(value=" 添加或编辑岗位", notes=" 添加或编辑岗位")
	public BaseResult saveOrUpdateXtPost(@RequestBody XtPost xtPost){
		int i = 0;
		if(StringUtil.isEmpty(xtPost.getXt_post_parentId())){
			xtPost.setXt_post_parentId("0");
		}
		if(StringUtils.isEmpty(xtPost.getXt_post_id())){
			//新增
			xtPost.setXt_post_id(toUUID());
			i=xtPostService.addXtPost(xtPost);
		}else{
			//编辑
			i=xtPostService.updateXtPost(xtPost);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 部门组织机构
	 */
	@AuthUneedLogin
	@GetMapping(value="/depart/tree")
	@ApiOperation(value="部门组织机构", notes="部门组织机构")
	public BaseResult departTree(){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseJSTreeEntity> list = new ArrayList<BaseJSTreeEntity>();
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListAll(condition);
		for(int i = 0; i < xtDepartinfoList.size(); i++){
			XtDepartinfo xtDepartinfo = xtDepartinfoList.get(i);
			BaseJSTreeEntity baseJSTreeEntity = new BaseJSTreeEntity();
			baseJSTreeEntity.setId(xtDepartinfo.getXt_departinfo_id());
			baseJSTreeEntity.setParent(xtDepartinfo.getXt_departinfo_parentId());
			baseJSTreeEntity.setText(xtDepartinfo.getXt_departinfo_name());
			baseJSTreeEntity.setSingleClickExpand(true);
			baseJSTreeEntity.setTempObject("DEPART");
			list.add(baseJSTreeEntity);
		}
		BaseJSTreeEntity baseJSTreeEntity = new BaseJSTreeEntity();
		List<BaseJSTreeEntity> baseJSTreeEntitiesList = baseJSTreeEntity.buildTree(list,"0");
		/*根目录则显示方法
		BaseJSTree baseJSTree = new BaseJSTree("部门",baseJSTreeEntitiesList,new BaseJSTreeStateEntity(false,false,false));
		return new BaseResult(baseJSTree);
		*/
		return new BaseResult(JsonUtil.toFastJson(baseJSTreeEntitiesList));
	}


	/**
	 * 岗位树（即职务）
	 */
	@AuthUneedLogin
	@GetMapping(value="/post/tree")
	@ApiOperation(value="岗位树", notes="岗位树")
	public BaseResult postTree(){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseJSTreeEntity> list = new ArrayList<BaseJSTreeEntity>();
		List<XtPost> xtPostList = xtPostService.getXtPostListAll(condition);
		for(XtPost xtPost:xtPostList){
			BaseJSTreeEntity baseJSTreeEntity = new BaseJSTreeEntity();
			baseJSTreeEntity.setId(xtPost.getXt_post_id());
			baseJSTreeEntity.setParent(xtPost.getXt_post_parentId());
			baseJSTreeEntity.setText(xtPost.getXt_post_name());
			baseJSTreeEntity.setSingleClickExpand(true);
			baseJSTreeEntity.setTempObject("POST");
			list.add(baseJSTreeEntity);
		}
		BaseJSTreeEntity baseJSTreeEntity = new BaseJSTreeEntity();
		List<BaseJSTreeEntity> baseJSTreeEntitiesList = baseJSTreeEntity.buildTree(list,"0");
		/*根目录则显示方法
		BaseJSTree baseJSTree = new BaseJSTree("岗位",baseJSTreeEntitiesList,new BaseJSTreeStateEntity(false,false,false));
		return new BaseResult(baseJSTree);
		*/
		return new BaseResult(JsonUtil.toFastJson(baseJSTreeEntitiesList));
	}
}
