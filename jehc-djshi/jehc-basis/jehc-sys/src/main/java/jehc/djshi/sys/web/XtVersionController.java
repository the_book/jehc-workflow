package jehc.djshi.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtVersion;
import jehc.djshi.sys.service.XtVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

/**
 * @Desc 平台版本
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtVersion")
@Api(value = "平台版本API",tags = "平台版本API",description = "平台版本API")
public class XtVersionController extends BaseAction {
	@Autowired
	private XtVersionService xtVersionService;
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询平台版本列表并分页", notes="查询平台版本列表并分页")
	public BasePage<List<XtVersion>> getXtVersionListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtVersion> xtVersionList = xtVersionService.getXtVersionListByCondition(condition);
		for(XtVersion xtVersion:xtVersionList){
			if(!jehc.djshi.common.util.StringUtil.isEmpty(xtVersion.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtVersion.getCreate_id());
				if(null != createBy){
					xtVersion.setCreateBy(createBy.getName());
				}
			}
			if(!jehc.djshi.common.util.StringUtil.isEmpty(xtVersion.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtVersion.getUpdate_id());
				if(null != modifiedBy){
					xtVersion.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtVersion> page = new PageInfo<XtVersion>(xtVersionList);
		return outPageBootStr(page,baseSearch);
	}
	
	/**
	* 查询单个平台版本
	* @param xt_version_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_version_id}")
	@ApiOperation(value="查询单个平台版本", notes="查询单个平台版本")
	public BaseResult<XtVersion> getXtVersionById(@PathVariable("xt_version_id")String xt_version_id){
		XtVersion xtVersion = xtVersionService.getXtVersionById(xt_version_id);
		if(!jehc.djshi.common.util.StringUtil.isEmpty(xtVersion.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtVersion.getCreate_id());
			if(null != createBy){
				xtVersion.setCreateBy(createBy.getName());
			}
		}
		if(!jehc.djshi.common.util.StringUtil.isEmpty(xtVersion.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtVersion.getUpdate_id());
			if(null != modifiedBy){
				xtVersion.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtVersion);
	}
	
	/**
	* 添加
	* @param xtVersion
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个平台版本", notes="创建单个平台版本")
	public BaseResult addXtVersion(@RequestBody XtVersion xtVersion){
		int i = 0;
		if(null != xtVersion){
			xtVersion.setXt_version_id(toUUID());
			i=xtVersionService.addXtVersion(xtVersion);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 修改
	* @param xtVersion
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个平台版本", notes="编辑单个平台版本")
	public BaseResult updateXtVersion(@RequestBody XtVersion xtVersion){
		int i = 0;
		if(null != xtVersion){
			i=xtVersionService.updateXtVersion(xtVersion);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param xt_version_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除平台版本", notes="删除平台版本")
	public BaseResult delXtVersion(String xt_version_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_version_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_version_id",xt_version_id.split(","));
			i=xtVersionService.delXtVersion(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
