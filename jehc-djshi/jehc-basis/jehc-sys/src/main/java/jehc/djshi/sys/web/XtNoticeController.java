package jehc.djshi.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtNotice;
import jehc.djshi.sys.service.XtNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.github.pagehelper.PageInfo;
/**
 * @Desc 平台公告
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtNotice")
@Api(value = "平台公告API",tags = "平台公告API",description = "平台公告API")
public class XtNoticeController extends BaseAction {

	@Autowired
	private XtNoticeService xtNoticeService;

	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	* @param request 
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询平台公告列表并分页", notes="查询平台公告列表并分页")
	public BasePage<List<XtNotice>> getXtNoticeListByCondition(@RequestBody(required=true)BaseSearch baseSearch, HttpServletRequest request){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtNotice> xtNoticeList = xtNoticeService.getXtNoticeListByCondition(condition);
		PageInfo<XtNotice> page = new PageInfo<XtNotice>(xtNoticeList);
		return outPageBootStr(page,baseSearch);
	}
	
	/**
	* 查询单个平台公告
	* @param xt_notice_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_notice_id}")
	@ApiOperation(value="查询单个平台公告", notes="查询单个平台公告")
	public BaseResult<XtNotice> getXtNoticeById(@PathVariable("xt_notice_id")String xt_notice_id){
		XtNotice xtNotice = xtNoticeService.getXtNoticeById(xt_notice_id);
		return BaseResult.success(xtNotice);
	}
	
	/**
	* 添加
	* @param xtNotice
	* @param bindingResult
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个平台公告", notes="创建单个平台公告")
	public BaseResult addXtNotice(@Valid @RequestBody XtNotice xtNotice,BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return outAudStr(false,backFem(bindingResult));
		}
		int i = 0;
		if(null != xtNotice){
			xtNotice.setXt_notice_id(toUUID());
			xtNotice.setXt_userinfo_id(getXtUid());
			xtNotice.setXt_createTime(getSimpleDateFormat());
			i=xtNoticeService.addXtNotice(xtNotice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 修改
	* @param xtNotice
	* @param bindingResult
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个平台公告", notes="编辑单个平台公告")
	public BaseResult updateXtNotice(@Valid @RequestBody XtNotice xtNotice,BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return outAudStr(false,backFem(bindingResult));
		}
		int i = 0;
		if(null != xtNotice){
			i=xtNoticeService.updateXtNotice(xtNotice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param xt_notice_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除平台公告", notes="删除平台公告")
	public BaseResult delXtNotice(String xt_notice_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_notice_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_notice_id",xt_notice_id.split(","));
			i=xtNoticeService.delXtNotice(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 公告数
	 * @return
	 */
	@GetMapping(value="/count")
	@NeedLoginUnAuth
	@ApiOperation(value="公告数", notes="公告数")
	public BaseResult count(){
		return outDataStr(xtNoticeService.getXtNoticeCountByCondition(new HashMap<>()));
	}
}
