package jehc.djshi.sys.web;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseBTreeGridEntity;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.sys.model.XtDataDictionary;
import jehc.djshi.sys.service.XtDataDictionaryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Desc 数据字典
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtDataDictionary")
@Api(value = "数据字典API",tags = "数据字典API",description = "数据字典API")
public class XtDataDictionaryController extends BaseAction {
	@Autowired
	private XtDataDictionaryService xtDataDictionaryService;
	/**
	* 加载初始化列表数据并分页
	* @param request 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/list")
	@ApiOperation(value="查询数据字典列表并分页", notes="查询数据字典列表并分页")
	public BaseResult getXtDataDictionaryListByCondition(HttpServletRequest request){
		List<BaseBTreeGridEntity> list = new ArrayList<BaseBTreeGridEntity>();
		Map<String, Object> condition = new HashMap<String, Object>();
		List<XtDataDictionary> XtDataDictionaryList = xtDataDictionaryService.getXtDataDictionaryListByCondition(condition);
		for(int i = 0; i < XtDataDictionaryList.size(); i++){
			XtDataDictionary XtDataDictionary = XtDataDictionaryList.get(i);
			BaseBTreeGridEntity BaseBTreeGridEntity = new BaseBTreeGridEntity();
			BaseBTreeGridEntity.setId(XtDataDictionary.getXt_data_dictionary_id());
			BaseBTreeGridEntity.setPid(XtDataDictionary.getXt_data_dictionary_pid());
			BaseBTreeGridEntity.setText(XtDataDictionary.getXt_data_dictionary_name());
			BaseBTreeGridEntity.setContent(XtDataDictionary.getXt_data_dictionary_remark());
//			BaseZTreeEntity.setIcon("../deng/images/icons/target.png");
			if(0==XtDataDictionary.getXt_data_dictionary_state()){
				BaseBTreeGridEntity.setTempObject("启用");
			}else{
				BaseBTreeGridEntity.setTempObject("禁用");
			}
			if(StringUtils.isEmpty(XtDataDictionary.getXt_data_dictionary_pid()) || "0".equals(XtDataDictionary.getXt_data_dictionary_pid())){
				BaseBTreeGridEntity.setIntegerappend("0");
			}else{
				BaseBTreeGridEntity.setIntegerappend("1");
			}
			list.add(BaseBTreeGridEntity);
		}
		return outStr(BaseBTreeGridEntity.buildTree(list));
	}
	/**
	* 获取对象
	* @param xt_data_dictionary_id 
	* @param request 
	*/
	@NeedLoginUnAuth
	@ApiOperation(value="查询单个数据字典", notes="查询单个数据字典")
	@GetMapping(value="/get/{xt_data_dictionary_id}")
	public BaseResult<XtDataDictionary> getXtDataDictionaryById(@PathVariable("xt_data_dictionary_id") String xt_data_dictionary_id,HttpServletRequest request){
		XtDataDictionary xtDataDictionary = xtDataDictionaryService.getXtDataDictionaryById(xt_data_dictionary_id);
		return outDataStr(xtDataDictionary);
	}

	/**
	 * 全部
	 * @param request
	 */
	@AuthUneedLogin
	@GetMapping(value="/lists")
	@ApiOperation(value="查询全部数据字典", notes="查询全部数据字典")
	public BaseResult<List<XtDataDictionary>> getXtDataDictionaryById(HttpServletRequest request){
		Map<String,Object> condition = new HashMap<>();
		List<XtDataDictionary> XtDataDictionaryList = xtDataDictionaryService.getXtDataDictionaryListAllByCondition(condition);
		return outDataStr(XtDataDictionaryList);
	}

	/**
	* 添加
	* @param xtDataDictionary
	* @param request 
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个数据字典", notes="创建单个数据字典")
	public BaseResult addXtDataDictionary(@RequestBody XtDataDictionary xtDataDictionary,HttpServletRequest request){
		int i = 0;
		if(null != xtDataDictionary){
			xtDataDictionary.setXt_data_dictionary_id(toUUID());
			i=xtDataDictionaryService.addXtDataDictionary(xtDataDictionary);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtDataDictionary
	* @param request 
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个数据字典", notes="编辑单个数据字典")
	public BaseResult updateXtDataDictionary(@RequestBody XtDataDictionary xtDataDictionary,HttpServletRequest request){
		int i = 0;
		if(null != xtDataDictionary){
			i=xtDataDictionaryService.updateXtDataDictionary(xtDataDictionary);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_data_dictionary_id 
	* @param request 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除数据字典", notes="删除数据字典")
	public BaseResult delXtDataDictionary(String xt_data_dictionary_id,HttpServletRequest request){
		int i = 0;
		if(StringUtils.isNotBlank(xt_data_dictionary_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_data_dictionary_id",xt_data_dictionary_id);
			i=xtDataDictionaryService.delXtDataDictionary(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
