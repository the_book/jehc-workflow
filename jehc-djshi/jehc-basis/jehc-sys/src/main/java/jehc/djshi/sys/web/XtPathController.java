package jehc.djshi.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtPath;
import jehc.djshi.sys.service.XtPathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

/**
 * @Desc 文件路径
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtPath")
@Api(value = "文件路径API",tags = "文件路径API",description = "文件路径API")
public class XtPathController extends BaseAction {
	@Autowired
	private XtPathService xtPathService;
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询文件路径列表并分页", notes="查询文件路径列表并分页")
	public BasePage<List<XtPath>> getXtPathListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtPath> xtPathList = xtPathService.getXtPathListByCondition(condition);
		for(XtPath xtPath:xtPathList){
			if(!StringUtil.isEmpty(xtPath.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtPath.getCreate_id());
				if(null != createBy){
					xtPath.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(xtPath.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtPath.getUpdate_id());
				if(null != modifiedBy){
					xtPath.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtPath> page = new PageInfo<XtPath>(xtPathList);
		return outPageBootStr(page,baseSearch);
	}
	
	/**
	* 查询单个文件路径
	* @param xt_path_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_path_id}")
	@ApiOperation(value="查询单个文件路径", notes="查询单个文件路径")
	public BaseResult<XtPath> getXtPathById(@PathVariable("xt_path_id")String xt_path_id){
		XtPath xtPath = xtPathService.getXtPathById(xt_path_id);
		if(!StringUtil.isEmpty(xtPath.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtPath.getCreate_id());
			if(null != createBy){
				xtPath.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(xtPath.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtPath.getUpdate_id());
			if(null != modifiedBy){
				xtPath.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtPath);
	}


	/**
	 * 全部
	 */
	@AuthUneedLogin
	@GetMapping(value="/lists")
	@ApiOperation(value="查询全部文件路径", notes="查询全部文件路径")
	public BaseResult<List<XtPath>> lists(){
		Map<String,Object> condition = new HashMap<>();
		List<XtPath> xtPathList = xtPathService.getXtPathListAllByCondition(condition);
		return outDataStr(xtPathList);
	}
	
	/**
	* 添加
	* @param xtPath
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个文件路径", notes="创建单个文件路径")
	public BaseResult addXtPath(@RequestBody XtPath xtPath){
		int i = 0;
		if(null != xtPath){
			xtPath.setCreate_time(getDate());
			xtPath.setXt_path_id(toUUID());
			i=xtPathService.addXtPath(xtPath);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 修改
	* @param xtPath
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个文件路径", notes="编辑单个文件路径")
	public BaseResult updateXtPath(@RequestBody XtPath xtPath){
		int i = 0;
		if(null != xtPath){
			i=xtPathService.updateXtPath(xtPath);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param xt_path_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除文件路径", notes="删除文件路径")
	public BaseResult delXtPath(String xt_path_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_path_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_path_id",xt_path_id.split(","));
			i=xtPathService.delXtPath(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
}
