package jehc.djshi.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtUnit;
import jehc.djshi.sys.service.XtUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

/**
 * @Desc 商品(产品)单位
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtUnit")
@Api(value = "商品(产品)单位API",tags = "商品(产品)单位API",description = "商品(产品)单位API")
public class XtUnitController extends BaseAction {
	@Autowired
	private XtUnitService xtUnitService;
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询商品(产品)单位列表并分页", notes="查询商品(产品)单位列表并分页")
	public BasePage<List<XtUnit>> getXtUnitListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtUnit> xtUnitList = xtUnitService.getXtUnitListByCondition(condition);
		PageInfo<XtUnit> page = new PageInfo<XtUnit>(xtUnitList);
		return outPageBootStr(page,baseSearch);
	}
	
	/**
	* 查询单个商品(产品)单位
	* @param xt_unit_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_unit_id}")
	@ApiOperation(value="查询单个商品(产品)单位", notes="查询单个商品(产品)单位")
	public BaseResult<XtUnit> getXtUnitById(@PathVariable("xt_unit_id")String xt_unit_id){
		XtUnit xtUnit = xtUnitService.getXtUnitById(xt_unit_id);
		return outDataStr(xtUnit);
	}
	
	/**
	* 添加
	* @param xtUnit
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个商品(产品)单位", notes="创建单个商品(产品)单位")
	public BaseResult addXtUnit(@RequestBody XtUnit xtUnit){
		int i = 0;
		if(null != xtUnit){
			xtUnit.setXt_unit_id(toUUID());
			i=xtUnitService.addXtUnit(xtUnit);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 修改
	* @param xtUnit
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个商品(产品)单位", notes="编辑单个商品(产品)单位")
	public BaseResult updateXtUnit(@RequestBody XtUnit xtUnit){
		int i = 0;
		if(null != xtUnit){
			i=xtUnitService.updateXtUnit(xtUnit);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param xt_unit_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除商品(产品)单位", notes="删除单个商品(产品)单位")
	public BaseResult delXtUnit(String xt_unit_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_unit_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_unit_id",xt_unit_id.split(","));
			i=xtUnitService.delXtUnit(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
