package jehc.djshi.sys.web;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.*;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.common.base.*;
import jehc.djshi.sys.model.XtDepartinfo;
import jehc.djshi.sys.service.XtDepartinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

/**
 * @Desc 部门信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtDepartinfo")
@Api(value = "部门信息API",tags = "部门信息API",description = "部门信息API")
public class XtDepartinfoController extends BaseAction {
	@Autowired
	private XtDepartinfoService xtDepartinfoService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询部门信息列表并分页", notes="查询部门信息列表并分页")
	public BasePage<List<XtDepartinfo>> getXtDepartinfoListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListByCondition(condition);
		for(XtDepartinfo xtDepartinfo:xtDepartinfoList){
			if(!StringUtil.isEmpty(xtDepartinfo.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtDepartinfo.getCreate_id());
				if(null != createBy){
					xtDepartinfo.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(xtDepartinfo.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtDepartinfo.getUpdate_id());
				if(null != modifiedBy){
					xtDepartinfo.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtDepartinfo> page = new PageInfo<XtDepartinfo>(xtDepartinfoList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个部门信息
	* @param xt_departinfo_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_departinfo_id}")
	@ApiOperation(value="查询单个部门信息", notes="查询单个部门信息")
	public BaseResult<XtDepartinfo> getXtDepartinfoById(@PathVariable("xt_departinfo_id")String xt_departinfo_id){
		XtDepartinfo xtDepartinfo = xtDepartinfoService.getXtDepartinfoById(xt_departinfo_id);
		if(!StringUtil.isEmpty(xtDepartinfo.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtDepartinfo.getCreate_id());
			if(null != createBy){
				xtDepartinfo.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(xtDepartinfo.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtDepartinfo.getUpdate_id());
			if(null != modifiedBy){
				xtDepartinfo.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtDepartinfo);
	}

	/**
	 *一级目录
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/one/level/lists")
	@ApiOperation(value="一级目录", notes="一级目录")
	public BaseResult<List<XtDepartinfo>> lists(){
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoList();
		return outDataStr(xtDepartinfoList);
	}

	/**
	 * 子级目录
	 * @param id
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/child/lists/{id}")
	@ApiOperation(value="子级目录", notes="子级目录")
	public BaseResult<List<XtDepartinfo>> childLists(@PathVariable("id")String id){
		Map<String,Object> condition = new HashMap<>();
		condition.put("xt_departinfo_parentId",id);
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListChild(condition);
		return outDataStr(xtDepartinfoList);
	}

	/**
	* 添加
	* @param xtDepartinfo
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个部门信息", notes="创建单个部门信息")
	public BaseResult addXtDepartinfo(@RequestBody XtDepartinfo xtDepartinfo){
		int i = 0;
		if(null != xtDepartinfo){
			xtDepartinfo.setXt_departinfo_id(toUUID());
			if(StringUtil.isEmpty(xtDepartinfo.getXt_departinfo_parentId())){
				xtDepartinfo.setXt_departinfo_parentId("0");
			}
			i=xtDepartinfoService.addXtDepartinfo(xtDepartinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtDepartinfo
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个部门信息", notes="编辑单个部门信息")
	public BaseResult updateXtDepartinfo(@RequestBody XtDepartinfo xtDepartinfo){
		int i = 0;
		if(null != xtDepartinfo){
			if(StringUtil.isEmpty(xtDepartinfo.getXt_departinfo_parentId())){
				xtDepartinfo.setXt_departinfo_parentId("0");
			}
			i=xtDepartinfoService.updateXtDepartinfo(xtDepartinfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_departinfo_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除部门信息", notes="删除部门信息")
	public BaseResult delXtDepartinfo(String xt_departinfo_id){
		int i = 0;
		if(null != xt_departinfo_id && !"".equals(xt_departinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_departinfo_id",xt_departinfo_id.split(","));
			i=xtDepartinfoService.delXtDepartinfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	
	/**
	 * 读取部门树
	 * @param id
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/tree/{id}")
	@ApiOperation(value="根据Id查询部门树", notes="根据Id查询部门树")
	public BaseResult getXtDepartinfoTree(@PathVariable("id")String id){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseTreeGridEntity> list = new ArrayList<BaseTreeGridEntity>();
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListAll(condition);
		for(int i = 0; i < xtDepartinfoList.size(); i++){
			XtDepartinfo xtDepartinfo = xtDepartinfoList.get(i);
			BaseTreeGridEntity BaseTreeGridEntity = new BaseTreeGridEntity();
			BaseTreeGridEntity.setId(xtDepartinfo.getXt_departinfo_id());
			BaseTreeGridEntity.setPid(xtDepartinfo.getXt_departinfo_parentId());
			BaseTreeGridEntity.setText(xtDepartinfo.getXt_departinfo_name());
			BaseTreeGridEntity.setExpanded(true);
			BaseTreeGridEntity.setSingleClickExpand(true);
			BaseTreeGridEntity.setIcon("/deng/images/icons/target.png");
			list.add(BaseTreeGridEntity);
		}

		BaseTreeGridEntity baseTreeGridEntity = new BaseTreeGridEntity();
		List<BaseTreeGridEntity> baseTreeGridEntityList = baseTreeGridEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseTreeGridEntityList);
		return outStr(json);
	}
	
	
	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理组 发起组等使用）
	 * @param xt_departinfo_id
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/list/{xt_departinfo_id}")
	@ApiOperation(value=" 根据各种情况查找集合不分页", notes=" 根据各种情况查找集合不分页")
	public BaseResult<List<XtDepartinfo>> queryXtDepartinfoList(@PathVariable("xt_departinfo_id")String xt_departinfo_id){
		List<XtDepartinfo> list = new ArrayList<XtDepartinfo>();
		if(!StringUtil.isEmpty(xt_departinfo_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_departinfo_id", xt_departinfo_id.split(","));
			list = xtDepartinfoService.queryXtDepartinfoList(condition);
		}
		return  outItemsStr(list);
	}
	
	
	/**
	 * 读取部门树（Bootstrap---ztree）
	 * @param request
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/bztree")
	@ApiOperation(value=" 查询部门树（Btree）", notes="查询部门树（Btree）")
	public BaseResult getXtDepartinfoBZTree(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		List<XtDepartinfo> xtDepartinfoList = xtDepartinfoService.getXtDepartinfoListAll(condition);
		for(int i = 0; i < xtDepartinfoList.size(); i++){
			XtDepartinfo xtDepartinfo = xtDepartinfoList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(xtDepartinfo.getXt_departinfo_id());
			BaseZTreeEntity.setPId(xtDepartinfo.getXt_departinfo_parentId());
			BaseZTreeEntity.setText(xtDepartinfo.getXt_departinfo_name());
			BaseZTreeEntity.setName(xtDepartinfo.getXt_departinfo_name());
			BaseZTreeEntity.setExpanded(true);
			BaseZTreeEntity.setSingleClickExpand(true);
			list.add(BaseZTreeEntity);
		}
		BaseZTreeEntity baseZTreeEntity = new BaseZTreeEntity();
		List<BaseZTreeEntity> baseZTreeEntityList = baseZTreeEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseZTreeEntityList);
		return outStr(json);
	}
}
