package jehc.djshi.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.dao.XtKwordsDao;
import jehc.djshi.sys.model.XtKwords;
import jehc.djshi.sys.service.XtKwordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;

/**
 * @Desc 关键词（敏感词）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtKwordsService")
public class XtKwordsServiceImpl extends BaseService implements XtKwordsService {
	@Autowired
	private XtKwordsDao xtKwordsDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtKwords> getXtKwordsListByCondition(Map<String,Object> condition){
		try{
			return xtKwordsDao.getXtKwordsListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_kwords_id 
	* @return
	*/
	public XtKwords getXtKwordsById(String xt_kwords_id){
		try{
			return xtKwordsDao.getXtKwordsById(xt_kwords_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtKwords
	* @return
	*/
	public int addXtKwords(XtKwords xtKwords){
		int i = 0;
		try {
			xtKwords.setCreate_id(getXtUid());
			xtKwords.setCreate_time(getDate());
			i = xtKwordsDao.addXtKwords(xtKwords);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtKwords
	* @return
	*/
	public int updateXtKwords(XtKwords xtKwords){
		int i = 0;
		try {
			xtKwords.setUpdate_id(getXtUid());
			xtKwords.setUpdate_time(getDate());
			i = xtKwordsDao.updateXtKwords(xtKwords);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtKwords(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtKwordsDao.delXtKwords(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
