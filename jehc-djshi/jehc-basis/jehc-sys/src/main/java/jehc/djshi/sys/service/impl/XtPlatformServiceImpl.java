package jehc.djshi.sys.service.impl;
import java.util.List;
import java.util.Map;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.sys.model.XtPlatformFeedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.sys.service.XtPlatformFeedbackService;
import java.util.HashMap;
import java.util.ArrayList;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.service.XtPlatformService;
import jehc.djshi.sys.dao.XtPlatformDao;
import jehc.djshi.sys.model.XtPlatform;

/**
 * @Desc 平台信息发布
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtPlatformService")
public class XtPlatformServiceImpl extends BaseService implements XtPlatformService{
	@Autowired
	private XtPlatformDao xtPlatformDao;

	@Autowired
	private XtPlatformFeedbackService xtPlatformFeedbackService;

	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtPlatform> getXtPlatformListByCondition(Map<String,Object> condition){
		try{
			return xtPlatformDao.getXtPlatformListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	* 查询对象
	* @param xt_platform_id 
	* @return
	*/
	public XtPlatform getXtPlatformById(String xt_platform_id){
		try{
			XtPlatform xtPlatform = xtPlatformDao.getXtPlatformById(xt_platform_id);
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_platform_id", xt_platform_id);
			List<XtPlatformFeedback> xtPlatformFeedback = xtPlatformFeedbackService.getXtPlatformFeedbackListByCondition(condition);
			xtPlatform.setXtPlatformFeedback(xtPlatformFeedback);
			return xtPlatform;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	* 添加
	* @param xtPlatform 
	* @return
	*/
	public int addXtPlatform(XtPlatform xtPlatform){
		int i = 0;
		try {
			xtPlatform.setCreate_id(getXtUid());
			xtPlatform.setCreate_time(getDate());
			i = xtPlatformDao.addXtPlatform(xtPlatform);
			List<XtPlatformFeedback> xtPlatformFeedbackTempList = xtPlatform.getXtPlatformFeedback();
			List<XtPlatformFeedback> xtPlatformFeedbackList = new ArrayList<XtPlatformFeedback>();
			if(CollectionUtil.isNotEmpty(xtPlatformFeedbackTempList)){
				for(int j = 0; j < xtPlatformFeedbackTempList.size(); j++){
					if(xtPlatform.getXtPlatformFeedback_removed_flag().indexOf(","+j+",") == -1){
						xtPlatformFeedbackTempList.get(j).setCreate_id(xtPlatform.getCreate_id());
						xtPlatformFeedbackTempList.get(j).setCreate_time(xtPlatform.getCreate_time());
						xtPlatformFeedbackTempList.get(j).setXt_platform_id(xtPlatform.getXt_platform_id());
						xtPlatformFeedbackTempList.get(j).setXt_platform_feedback_id(toUUID());
						xtPlatformFeedbackList.add(xtPlatformFeedbackTempList.get(j));
					}
				}
			}
			if(!xtPlatformFeedbackList.isEmpty()&&xtPlatformFeedbackList.size()>0){
				xtPlatformFeedbackService.addBatchXtPlatformFeedback(xtPlatformFeedbackList);
			}
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 修改
	* @param xtPlatform 
	* @return
	*/
	public int updateXtPlatform(XtPlatform xtPlatform){
		int i = 0;
		try {
			i = xtPlatformDao.updateXtPlatform(xtPlatform);
			List<XtPlatformFeedback> xtPlatformFeedbackList = xtPlatform.getXtPlatformFeedback();
			List<XtPlatformFeedback> xtPlatformFeedbackAddList = new ArrayList<XtPlatformFeedback>();
			List<XtPlatformFeedback> xtPlatformFeedbackUpdateList = new ArrayList<XtPlatformFeedback>();
			if(CollectionUtil.isNotEmpty(xtPlatformFeedbackList)){
				for(int j = 0; j < xtPlatformFeedbackList.size(); j++){
					if(xtPlatform.getXtPlatformFeedback_removed_flag().indexOf(","+j+",") == -1){
						xtPlatformFeedbackList.get(j).setXt_platform_id(xtPlatform.getXt_platform_id());
						if(StringUtil.isEmpty(xtPlatformFeedbackList.get(j).getXt_platform_feedback_id())){
							xtPlatformFeedbackList.get(j).setXt_platform_feedback_id(toUUID());
							xtPlatformFeedbackList.get(j).setCreate_id(xtPlatform.getUpdate_id());
							xtPlatformFeedbackList.get(j).setCreate_time(getDate());
							xtPlatformFeedbackAddList.add(xtPlatformFeedbackList.get(j));
						}else{
							xtPlatformFeedbackList.get(j).setUpdate_time(getDate());
							xtPlatformFeedbackList.get(j).setUpdate_id(xtPlatform.getUpdate_id());
							xtPlatformFeedbackUpdateList.add(xtPlatformFeedbackList.get(j));
						}
					}
				}
			}
			if(!xtPlatformFeedbackAddList.isEmpty()&&xtPlatformFeedbackAddList.size()>0){
				xtPlatformFeedbackService.addBatchXtPlatformFeedback(xtPlatformFeedbackAddList);
			}
			if(!xtPlatformFeedbackUpdateList.isEmpty()&&xtPlatformFeedbackUpdateList.size()>0){
				xtPlatformFeedbackService.updateBatchXtPlatformFeedback(xtPlatformFeedbackUpdateList);
			}
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 修改（根据动态条件）
	* @param xtPlatform 
	* @return
	*/
	public int updateXtPlatformBySelective(XtPlatform xtPlatform){
		int i = 0;
		try {
			i = xtPlatformDao.updateXtPlatformBySelective(xtPlatform);
			List<XtPlatformFeedback> xtPlatformFeedbackList = xtPlatform.getXtPlatformFeedback();
			List<XtPlatformFeedback> xtPlatformFeedbackAddList = new ArrayList<XtPlatformFeedback>();
			List<XtPlatformFeedback> xtPlatformFeedbackUpdateList = new ArrayList<XtPlatformFeedback>();
			if(CollectionUtil.isNotEmpty(xtPlatformFeedbackList)){
				for(int j = 0; j < xtPlatformFeedbackList.size(); j++){
					if(xtPlatform.getXtPlatformFeedback_removed_flag().indexOf(","+j+",") == -1){
						xtPlatformFeedbackList.get(j).setUpdate_id(xtPlatform.getUpdate_id());
						xtPlatformFeedbackList.get(j).setXt_platform_id(xtPlatform.getXt_platform_id());
						if(StringUtil.isEmpty(xtPlatformFeedbackList.get(j).getXt_platform_feedback_id())){
							xtPlatformFeedbackList.get(j).setXt_platform_feedback_id(toUUID());
							xtPlatformFeedbackList.get(j).setCreate_time(getDate());
							xtPlatformFeedbackAddList.add(xtPlatformFeedbackList.get(j));
						}else{
							xtPlatformFeedbackList.get(j).setUpdate_time(getDate());
							xtPlatformFeedbackUpdateList.add(xtPlatformFeedbackList.get(j));
						}
					}
				}
			}
			if(!xtPlatformFeedbackAddList.isEmpty()&&xtPlatformFeedbackAddList.size()>0){
				xtPlatformFeedbackService.addBatchXtPlatformFeedback(xtPlatformFeedbackAddList);
			}
			if(!xtPlatformFeedbackUpdateList.isEmpty()&&xtPlatformFeedbackUpdateList.size()>0){
				xtPlatformFeedbackService.updateBatchXtPlatformFeedbackBySelective(xtPlatformFeedbackUpdateList);
			}
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtPlatform(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtPlatformDao.delXtPlatform(condition);
			String[] xt_platform_idList= (String[])condition.get("xt_platform_id");
			for(String xt_platform_id:xt_platform_idList){
				xtPlatformFeedbackService.delXtPlatformFeedbackByForeignKey(xt_platform_id);
			}
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 批量添加
	* @param xtPlatformList 
	* @return
	*/
	public int addBatchXtPlatform(List<XtPlatform> xtPlatformList){
		int i = 0;
		try {
			i = xtPlatformDao.addBatchXtPlatform(xtPlatformList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 批量修改
	* @param xtPlatformList 
	* @return
	*/
	public int updateBatchXtPlatform(List<XtPlatform> xtPlatformList){
		int i = 0;
		try {
			i = xtPlatformDao.updateBatchXtPlatform(xtPlatformList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 批量修改（根据动态条件）
	* @param xtPlatformList 
	* @return
	*/
	public int updateBatchXtPlatformBySelective(List<XtPlatform> xtPlatformList){
		int i = 0;
		try {
			i = xtPlatformDao.updateBatchXtPlatformBySelective(xtPlatformList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
