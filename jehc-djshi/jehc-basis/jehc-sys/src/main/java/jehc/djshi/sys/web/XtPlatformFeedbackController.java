package jehc.djshi.sys.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.sys.model.XtPlatformFeedback;
import jehc.djshi.sys.service.XtPlatformFeedbackService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;

/**
 * @Desc 平台反馈意见
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtPlatformFeedback")
@Api(value = "平台反馈意见API",tags = "平台反馈意见API",description = "平台反馈意见API")
public class XtPlatformFeedbackController extends BaseAction{
	@Autowired
	private XtPlatformFeedbackService xtPlatformFeedbackService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage<List<XtPlatformFeedback>> getXtPlatformFeedbackListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtPlatformFeedback> xtPlatformFeedbackList = xtPlatformFeedbackService.getXtPlatformFeedbackListByCondition(condition);
		PageInfo<XtPlatformFeedback> page = new PageInfo<XtPlatformFeedback>(xtPlatformFeedbackList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param xt_platform_feedback_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_platform_feedback_id}")
	public BaseResult<XtPlatformFeedback> getXtPlatformFeedbackById(@PathVariable("xt_platform_feedback_id")String xt_platform_feedback_id){
		XtPlatformFeedback xtPlatformFeedback = xtPlatformFeedbackService.getXtPlatformFeedbackById(xt_platform_feedback_id);
		return outDataStr(xtPlatformFeedback);
	}
	/**
	* 添加
	* @param xtPlatformFeedback 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addXtPlatformFeedback(@RequestBody XtPlatformFeedback xtPlatformFeedback){
		int i = 0;
		if(null != xtPlatformFeedback){
			xtPlatformFeedback.setXt_platform_feedback_id(toUUID());
			i=xtPlatformFeedbackService.addXtPlatformFeedback(xtPlatformFeedback);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtPlatformFeedback 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateXtPlatformFeedback(@RequestBody XtPlatformFeedback xtPlatformFeedback){
		int i = 0;
		if(null != xtPlatformFeedback){
			i=xtPlatformFeedbackService.updateXtPlatformFeedback(xtPlatformFeedback);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_platform_feedback_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delXtPlatformFeedback(String xt_platform_feedback_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_platform_feedback_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_platform_feedback_id",xt_platform_feedback_id.split(","));
			i=xtPlatformFeedbackService.delXtPlatformFeedback(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
