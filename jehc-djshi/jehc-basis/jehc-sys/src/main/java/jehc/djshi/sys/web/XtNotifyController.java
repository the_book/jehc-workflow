package jehc.djshi.sys.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtNotice;
import jehc.djshi.sys.model.XtNotify;
import jehc.djshi.sys.model.XtNotifyReceiver;
import jehc.djshi.sys.service.XtNotifyReceiverService;
import jehc.djshi.sys.service.XtNotifyService;
import jehc.djshi.sys.service.XtUserinfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.github.pagehelper.PageInfo;

/**
 * @Desc 通知
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtNotify")
@Api(value = "通知API",tags = "通知API",description = "通知API")
public class XtNotifyController extends BaseAction {
	@Autowired
	private XtNotifyService xtNotifyService;
	@Autowired
	private XtUserinfoService xtUserinfoService;
	@Autowired
	private XtNotifyReceiverService xtNotifyReceiverService;
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询通知列表并分页", notes="查询通知列表并分页")
	public BasePage<List<XtNotify>> getXtNotifyListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("create_id", getXtUid());
		commonHPager(baseSearch);
		List<XtNotify> xtNotifyList = xtNotifyService.getXtNotifyListByCondition(condition);
		for(XtNotify xtNotify : xtNotifyList){
			if(!StringUtil.isEmpty(xtNotify.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtNotify.getCreate_id());
				if(null != createBy){
					xtNotify.setCreateBy(createBy.getName());
				}
			}
		}
		PageInfo<XtNotify> page = new PageInfo<XtNotify>(xtNotifyList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 查询单个通知
	 * @param notify_id
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/get/{notify_id}")
	@ApiOperation(value="查询单个通知", notes="查询单个通知")
	public BaseResult<XtNotify> getXtNoticeById(@PathVariable("notify_id")String notify_id){
		XtNotify notify = xtNotifyService.getXtNotifyById(notify_id);
		if(null != notify){
			List<XtNotifyReceiver> notifyReceivers = xtNotifyReceiverService.getXtNotifyReceiverListById(notify.getNotify_id());
			notify.setNotifyReceivers(notifyReceivers);
		}
		return BaseResult.success(notify);
	}

	/**
	* 添加
	* @param xtNotify
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个通知", notes="创建单个通知")
	public BaseResult addXtNotify(@RequestBody XtNotify xtNotify){
		int i = 0;
		if(null != xtNotify){
			List<XtNotifyReceiver> notifyReceiverList = new ArrayList<XtNotifyReceiver>();
			xtNotify.setNotify_id(toUUID());
			xtNotify.setCreate_time(getDate());
			String userId = xtNotify.getCreate_id();
			if(!StringUtils.isEmpty(userId)){
				String[] userIds = userId.split(",");
				for(int j = 0; j < userIds.length; j++){
					XtNotifyReceiver xtNotifyReceiver = new XtNotifyReceiver();
					xtNotifyReceiver.setNotify_receiver_id(toUUID());
					xtNotifyReceiver.setReceive_id(userIds[j]);
					xtNotifyReceiver.setReceive_name(xtUserinfoService.getXtUserinfoById(userIds[j]).getXt_userinfo_realName());
					xtNotifyReceiver.setReceive_time(xtNotify.getCreate_time());
					notifyReceiverList.add(xtNotifyReceiver);
				}
				xtNotify.setNotifyReceivers(notifyReceiverList);
			}
			xtNotify.setCreate_id(getXtUid());
			xtNotify.setXt_userinfo_realName(getXtU().getXt_userinfo_realName());
			i=xtNotifyService.addXtNotify(xtNotify);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param notify_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除单个通知", notes="删除单个通知")
	public BaseResult delXtNotify(String notify_id){
		int i = 0;
		if(!StringUtil.isEmpty(notify_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("notify_id",notify_id.split(","));
			i=xtNotifyService.delXtNotify(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
