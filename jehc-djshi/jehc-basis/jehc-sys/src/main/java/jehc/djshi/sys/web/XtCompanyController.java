package jehc.djshi.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtCompany;
import jehc.djshi.sys.service.XtCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Desc 公司信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtCompany")
@Api(value = "平台公司信息API",tags = "平台公司信息API",description = "平台公司信息API")
public class XtCompanyController extends BaseAction {
	@Autowired
	private XtCompanyService xtCompanyService;
	/**
	* 获取对象
	* @param request 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/singel")
	@ApiOperation(value="查询公司信息", notes="查询公司信息")
	public BaseResult<XtCompany> getXtCompany(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		List<XtCompany> XtCompanyList = xtCompanyService.getXtCompanyListByCondition(condition);
		XtCompany xt_Company = new XtCompany();
		if(!XtCompanyList.isEmpty()){
			xt_Company = XtCompanyList.get(0);
		}
		return outDataStr(xt_Company);
	}
	
	/**
	* 添加或修改
	* @param xtCompany
	* @param request 
	*/
	@PostMapping(value="/addOrUpdate")
	@ApiOperation(value="添加或修改公司信息", notes="添加或修改公司信息")
	public BaseResult addOrUpdateXtCompany(@RequestBody XtCompany xtCompany,HttpServletRequest request){
		int i = 0;
		if(null != xtCompany){
			if(null != xtCompany.getXt_company_id() && !"".equals(xtCompany.getXt_company_id())){
				i = xtCompanyService.updateXtCompany(xtCompany);
			}else{
				xtCompany.setXt_company_id(toUUID());
				i=xtCompanyService.addXtCompany(xtCompany);
			}
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param xt_company_id 
	* @param request 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除公司信息", notes="删除公司信息")
	public BaseResult delXtCompany(String xt_company_id,HttpServletRequest request){
		int i = 0;
		if(!StringUtil.isEmpty(xt_company_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_company_id",xt_company_id);
			i=xtCompanyService.delXtCompany(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
