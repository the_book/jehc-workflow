package jehc.djshi.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtConstant;
import jehc.djshi.sys.service.XtConstantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.github.pagehelper.PageInfo;

/**
 * @Desc 台平常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtConstant")
@Api(value = "平台常量API",tags = "平台常量API",description = "平台常量API")
public class XtConstantController extends BaseAction {
	@Autowired
	private XtConstantService xtConstantService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询平台常量并分页", notes="查询平台常量并分页")
	public BasePage<List<XtConstant>> getXtConstantListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtConstant> xtConstantList = xtConstantService.getXtConstantListByCondition(condition);
		for(XtConstant xtConstant:xtConstantList){
			if(!StringUtil.isEmpty(xtConstant.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtConstant.getCreate_id());
				if(null != createBy){
					xtConstant.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(xtConstant.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtConstant.getUpdate_id());
				if(null != modifiedBy){
					xtConstant.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtConstant> page = new PageInfo<XtConstant>(xtConstantList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个平台常量
	* @param xt_constant_id 
	* @param request 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_constant_id}")
	@ApiOperation(value="查询单个平台常量", notes="查询单个平台常量")
	public BaseResult<XtConstant> getXtConstantById(@PathVariable("xt_constant_id") String xt_constant_id, HttpServletRequest request){
		XtConstant xtConstant = xtConstantService.getXtConstantById(xt_constant_id);
		if(!StringUtil.isEmpty(xtConstant.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtConstant.getCreate_id());
			if(null != createBy){
				xtConstant.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(xtConstant.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtConstant.getUpdate_id());
			if(null != modifiedBy){
				xtConstant.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtConstant);
	}


	@AuthUneedLogin
	@GetMapping(value="/lists")
	@ApiOperation(value="查询全部平台常量", notes="查询全部平台常量")
	public BaseResult<List<XtConstant>> lists(HttpServletRequest request){
		Map<String,Object> condition = new HashMap<>();
		List<XtConstant> xtConstantList = xtConstantService.getXtConstantListAllByCondition(condition);
		return outDataStr(xtConstantList);
	}
	
	/**
	* 添加
	* @param xtConstant
	* @param request 
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个平台常量", notes="创建单个平台常量")
	public BaseResult addXtConstant(@RequestBody XtConstant xtConstant,HttpServletRequest request){
		int i = 0;
		if(null != xtConstant){
			xtConstant.setXt_constant_id(toUUID());
			i=xtConstantService.addXtConstant(xtConstant);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 修改
	* @param xtConstant
	* @param request 
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个平台常量", notes="编辑单个平台常量")
	public BaseResult updateXtConstant(@RequestBody XtConstant xtConstant,HttpServletRequest request){
		int i = 0;
		if(null != xtConstant){
			i=xtConstantService.updateXtConstant(xtConstant);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	
	/**
	* 删除
	* @param xt_constant_id 
	* @param request 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除平台常量", notes="删除平台常量")
	public BaseResult delXtConstant(String xt_constant_id,HttpServletRequest request){
		int i = 0;
		if(null != xt_constant_id && !"".equals(xt_constant_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_constant_id",xt_constant_id.split(","));
			i=xtConstantService.delXtConstant(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 根据类型查找常量集合
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/list/{type}")
	@ApiOperation(value="根据类型平台常量", notes="根据类型平台常量")
	public BaseResult<List<XtConstant>> getXtConstantList(@PathVariable("type")String type){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("type", type);
		List<XtConstant> xtConstantList = xtConstantService.getXtConstantListByCondition(condition);
		return outItemsStr(xtConstantList);
	}
}
