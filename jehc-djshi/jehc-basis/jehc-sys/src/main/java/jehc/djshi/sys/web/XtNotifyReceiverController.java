package jehc.djshi.sys.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtNotify;
import jehc.djshi.sys.model.XtNotifyReceiver;
import jehc.djshi.sys.service.XtNotifyReceiverService;
import jehc.djshi.sys.service.XtNotifyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.github.pagehelper.PageInfo;
/**
 * @Desc 通知接收人
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtNotifyReceiver")
@Api(value = "通知接收人API",tags = "通知接收人API",description = "通知接收人API")
@Slf4j
public class XtNotifyReceiverController extends BaseAction {
	@Autowired
	private XtNotifyReceiverService xtNotifyReceiverService;
	@Autowired
	private XtNotifyService xtNotifyService;
	
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	* @param receive_status
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询通知接收人列表并分页", notes="查询通知接收人列表并分页")
	public BasePage<List<XtNotifyReceiver>> getXtNotifyReceiverListByCondition(@RequestBody(required=true)BaseSearch baseSearch, String receive_status){
		Map<String, Object> condition = baseSearch.convert();
		condition.put("receive_id", getXtUid());
		condition.put("status",receive_status);
		commonHPager(baseSearch);
		List<XtNotifyReceiver> xtNotifyReceiverList = xtNotifyReceiverService.getXtNotifyReceiverListByCondition(condition);
		if(CollectionUtil.isNotEmpty(xtNotifyReceiverList)){
			for(XtNotifyReceiver xtNotifyReceiver: xtNotifyReceiverList){
				if(StringUtil.isEmpty(xtNotifyReceiver.getSendUserRealName())){
					XtNotify xtNotify = xtNotifyService.getXtNotifyById(xtNotifyReceiver.getNotify_id());
					if(null != xtNotify){
						OauthAccountEntity createBy = getAccount(xtNotify.getCreate_id());
						if(null != createBy){
							xtNotifyReceiver.setSendUserRealName(createBy.getName());
						}
					}
				}
			}
		}
		PageInfo<XtNotifyReceiver> page = new PageInfo<XtNotifyReceiver>(xtNotifyReceiverList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 查询单个我的通知
	 * @param notify_receiver_id
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/get/{notify_receiver_id}")
	@ApiOperation(value="查询单个我的通知", notes="查询单个我的通知")
	public BaseResult<XtNotifyReceiver> getXtNotifyReceiverById(@PathVariable("notify_receiver_id")String notify_receiver_id){
		XtNotifyReceiver xtNotifyReceiver = xtNotifyReceiverService.getXtNotifyReceiverById(notify_receiver_id);
		if(null == xtNotifyReceiver.getRead_time()){
			try {
				Map<String, Object> condition = new HashMap<>();
				Date date = getDate();
				condition.put("read_time", date);
				condition.put("notify_receiver_id",notify_receiver_id);
				xtNotifyReceiverService.updateXtNotifyReceiver(condition);
				xtNotifyReceiver.setRead_time(date);
			}catch (Exception e){
				log.error("更新已读出错：{}",e);
			}
		}
		if(StringUtil.isEmpty(xtNotifyReceiver.getSendUserRealName())){
			XtNotify xtNotify = xtNotifyService.getXtNotifyById(xtNotifyReceiver.getNotify_id());
			if(null != xtNotify){
				OauthAccountEntity createBy = getAccount(xtNotify.getCreate_id());
				if(null != createBy){
					xtNotifyReceiver.setSendUserRealName(createBy.getName());
				}
			}
		}
		return BaseResult.success(xtNotifyReceiver);
	}
	
	/**
	* 删除
	* @param xt_notify_receiver_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除通知接收人", notes="删除通知接收人")
	public BaseResult delXtNotifyReceiver(String xt_notify_receiver_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_notify_receiver_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("notify_receiver_id",xt_notify_receiver_id.split(","));
			i=xtNotifyReceiverService.delXtNotifyReceiver(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
