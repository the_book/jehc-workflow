package jehc.djshi.sys.service.impl;

import java.util.List;
import java.util.Map;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.sys.dao.XtNotifyDao;
import jehc.djshi.sys.dao.XtNotifyReceiverDao;
import jehc.djshi.sys.model.XtNotify;
import jehc.djshi.sys.model.XtNotifyReceiver;
import jehc.djshi.sys.service.XtNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
/**
 * @Desc 通知
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtNotifyService")
public class XtNotifyServiceImpl extends BaseService implements XtNotifyService {
	@Autowired
	private XtNotifyDao xtNotifyDao;
	@Autowired
	private XtNotifyReceiverDao xtNotifyReceiverDao;
	/**
	 * 初始化分页
	 * @param condition
	 * @return
	 */
	public List<XtNotify> getXtNotifyListByCondition(Map<String, Object> condition){
		try {
			return xtNotifyDao.getXtNotifyListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 查询对象
	 * @param notify_id
	 * @return
	 */
	public XtNotify getXtNotifyById(String notify_id){
		try {
			return xtNotifyDao.getXtNotifyById(notify_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * 插入对象
	 * @param xtNotify
	 * @return
	 */
	public int addXtNotify(XtNotify xtNotify){
		try {
			List<XtNotifyReceiver> notifyReceivers = xtNotify.getNotifyReceivers();
			//先插入主表
			xtNotifyDao.addXtNotify(xtNotify);
			//操作子表
			if(CollectionUtil.isNotEmpty(notifyReceivers)){
				for(XtNotifyReceiver notifyReceiver: notifyReceivers){
					notifyReceiver.setNotify_id(xtNotify.getNotify_id());
					xtNotifyReceiverDao.addXtNotifyReceiver(notifyReceiver);
				}
			}

			return 1;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	
	/***
	 * 删除
	 * @param condition
	 * @return
	 */
	public int delXtNotify(Map<String, Object> condition){
		try {
			return xtNotifyDao.delXtNotify(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
