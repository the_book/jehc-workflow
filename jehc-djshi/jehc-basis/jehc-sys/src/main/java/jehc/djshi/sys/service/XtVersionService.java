package jehc.djshi.sys.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.model.XtVersion;

/**
 * @Desc 平台版本
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtVersionService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtVersion> getXtVersionListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_version_id 
	* @return
	*/
	XtVersion getXtVersionById(String xt_version_id);
	/**
	* 添加
	* @param xtVersion
	* @return
	*/
	int addXtVersion(XtVersion xtVersion);
	/**
	* 修改
	* @param xtVersion
	* @return
	*/
	int updateXtVersion(XtVersion xtVersion);
	/**
	* 修改（根据动态条件）
	* @param xt_Version
	* @return
	*/
	int updateXtVersionBySelective(XtVersion xt_Version);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtVersion(Map<String,Object> condition);
}
