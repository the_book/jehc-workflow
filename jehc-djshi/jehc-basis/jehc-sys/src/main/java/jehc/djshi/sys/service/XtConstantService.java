package jehc.djshi.sys.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.model.XtConstant;

/**
 * @Desc 台平常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtConstantService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtConstant> getXtConstantListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_constant_id 
	* @return
	*/
	XtConstant getXtConstantById(String xt_constant_id);
	/**
	* 添加
	* @param xtConstant
	* @return
	*/
	int addXtConstant(XtConstant xtConstant);
	/**
	* 修改
	* @param xtConstant
	* @return
	*/
	int updateXtConstant(XtConstant xtConstant);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtConstant(Map<String,Object> condition);
	/**
	 * 读取所有常量
	 * @param condition
	 * @return
	 */
	List<XtConstant> getXtConstantListAllByCondition(Map<String,Object> condition);
}
