package jehc.djshi.sys.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.UserinfoEntity;
import jehc.djshi.sys.model.XtUserinfo;

/**
 * @Desc 员工信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtUserinfoService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtUserinfo> getXtUserinfoListByCondition(Map<String,Object> condition);
	
	/**
	 * 已删除用户
	 * @param condition
	 * @return
	 */
	List<XtUserinfo> getXtUserinfoDeletedListByCondition(Map<String,Object> condition);
	
	/**
	 * 恢复用户
	 * @param condition
	 * @return
	 */
	int recoverXtUserinfo(Map<String,Object> condition);
	
	/**
	* 查询对象
	* @param xt_userinfo_id 
	* @return
	*/
	XtUserinfo getXtUserinfoById(String xt_userinfo_id);
	/**
	* 添加
	* @param xtUserinfo
	* @return
	*/
	int addXtUserinfo(XtUserinfo xtUserinfo);
	/**
	* 修改
	* @param xtUserinfo
	* @return
	*/
	int updateXtUserinfo(XtUserinfo xtUserinfo);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtUserinfo(Map<String,Object> condition);
	/**
	 * 登录
	 * @param condition
	 * @return
	 */
	XtUserinfo getXtUserinfoByUP(Map<String,Object> condition);
	/**
	 * 根据用户名查找对象
	 * @param userName
	 * @return
	 */
	XtUserinfo getXtUserinfoByUserName(String userName);
	
	/**
	 * 读取所有用户根据各种情况非分页
	 * @param condition
	 * @return
	 */
	List<XtUserinfo> getXtUserinfoListAllByCondition(Map<String,Object> condition);
	
	/**
	 * 修改密码
	 * @param condition
	 * @return
	 */
	int updatePwd(Map<String,Object> condition);
	
	/**
	 * 验证用户名是否存在
	 * @return
	 */
	int validateUser(Map<String,Object> condition);
	
	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理人 发起人 发起人组及同步数据之授权中心等使用）
	 * @param condition
	 * @return
	 */
	List<XtUserinfo> getXtUserinfoList(Map<String,Object> condition);
}
