package jehc.djshi.sys.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtIcon;
import jehc.djshi.sys.param.XtIconForm;
import jehc.djshi.sys.service.XtIconService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 字体图标库
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/icon")
@Api(value = "字体图标库API",tags = "字体图标库API",description = "字体图标库API")
public class XtIconController extends BaseAction{

    @Resource
    XtIconService xtIconService;


    /**
     * 加载初始化列表数据并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询字体库图标列表并分页", notes="查询字体库图标列表并分页")
    public BasePage<List<XtIcon>> getXtIconListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<XtIcon> xtIconList = xtIconService.getXtIconListByCondition(condition);
        for(XtIcon xtIcon:xtIconList){
            if(!jehc.djshi.common.util.StringUtil.isEmpty(xtIcon.getCreate_id())){
                OauthAccountEntity createBy = getAccount(xtIcon.getCreate_id());
                if(null != createBy){
                    xtIcon.setCreateBy(createBy.getName());
                }
            }
            if(!jehc.djshi.common.util.StringUtil.isEmpty(xtIcon.getUpdate_id())){
                OauthAccountEntity modifiedBy = getAccount(xtIcon.getUpdate_id());
                if(null != modifiedBy){
                    xtIcon.setModifiedBy(modifiedBy.getName());
                }
            }
        }
        PageInfo<XtIcon> page = new PageInfo<XtIcon>(xtIconList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个字体库
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    @ApiOperation(value="查询单个字体库", notes="查询单个字体库")
    public BaseResult<XtIcon> getXtIconById(@PathVariable("id")String id){
        XtIcon xtIcon = xtIconService.getXtIconById(id);
        if(!jehc.djshi.common.util.StringUtil.isEmpty(xtIcon.getCreate_id())){
            OauthAccountEntity createBy = getAccount(xtIcon.getCreate_id());
            if(null != createBy){
                xtIcon.setCreateBy(createBy.getName());
            }
        }
        if(!jehc.djshi.common.util.StringUtil.isEmpty(xtIcon.getUpdate_id())){
            OauthAccountEntity modifiedBy = getAccount(xtIcon.getUpdate_id());
            if(null != modifiedBy){
                xtIcon.setModifiedBy(modifiedBy.getName());
            }
        }
        return outDataStr(xtIcon);
    }

    /**
     * 添加
     * @param xtIcon
     */
    @PostMapping(value="/add")
    @ApiOperation(value="创建单个字体库图标", notes="创建单个字体库图标")
    public BaseResult addXtIcon(@RequestBody XtIcon xtIcon){
        int i = 0;
        if(null != xtIcon){
            xtIcon.setId(toUUID());
            xtIcon.setCreate_time(getDate());
            xtIcon.setCreate_id(getXtUid());
            i=xtIconService.addXtIcon(xtIcon);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 修改
     * @param xtIcon
     */
    @PutMapping(value="/update")
    @ApiOperation(value="编辑单个字体库图标", notes="编辑单个字体库图标")
    public BaseResult updateXtIcon(@RequestBody XtIcon xtIcon){
        int i = 0;
        if(null != xtIcon){
            xtIcon.setUpdate_time(getDate());
            xtIcon.setUpdate_id(getXtUid());
            i=xtIconService.updateXtIcon(xtIcon);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 删除
     * @param id
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="删除字体库图标", notes="删除字体库图标")
    public BaseResult delXtIcon(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=xtIconService.delXtIcon(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 查询字体库图标列表
     * @param xtIconForm
     */
    @NeedLoginUnAuth
    @GetMapping(value="/query/list")
    @ApiOperation(value="查询字体库图标列表", notes="查询字体库图标列表")
    public BaseResult<List<XtIcon>> getXtIconList(XtIconForm xtIconForm){
        List<XtIcon> xtIconList = xtIconService.getXtIconList(xtIconForm);
        return BaseResult.success(xtIconList);
    }

//    @PostMapping(value="/batchAdd")
//    @AuthUneedLogin
//    public BaseResult batchAdd(String msg){
//        String[]array = msg.split(",");
//        for(String str:array){
//            XtIcon xtIcon = new XtIcon();
//            str = str.trim();
//            if(str.substring(0,1).equals("l")){
//                continue;
//            }
//            xtIcon.setName(str);
//            xtIcon.setIcon(str);
//            xtIcon.setId(toUUID());
//            xtIcon.setCreate_id(getXtUid());
//            xtIcon.setCreate_time(getDate());
//            xtIcon.setCategories("Lineawesome");
//            xtIconService.addXtIcon(xtIcon);
//        }
//        return BaseResult.success();
//    }
}
