package jehc.djshi.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 合同管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="合同管理对象", description="合同管理")
public class XtConcordat extends BaseEntity{

	@ApiModelProperty(value = "id")
	private String xt_concordat_id;/**合同编号**/

	@ApiModelProperty(value = "合同名称")
	private String name;/**合同名称**/

	@ApiModelProperty(value = "合同描述")
	private String content;/**合同描述**/
}
