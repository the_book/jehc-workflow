package jehc.djshi.sys.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 通知接收人
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="通知接收人对象", description="通知接收人")
public class XtNotifyReceiver extends BaseEntity{

	@ApiModelProperty(value = "id")
	private String notify_receiver_id;/**id**/

	@ApiModelProperty(value = "接收时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date receive_time;/**接收时间**/

	@ApiModelProperty(value = "接收人id")
	private String receive_id;/**接收人id**/

	@ApiModelProperty(value = "接收人名称")
	private String receive_name;/**接收人名称**/

	@ApiModelProperty(value = "通知id外键")
	private String notify_id;/**通知id外键**/

	@ApiModelProperty(value = "已读时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date read_time;/**已读时间**/

	@ApiModelProperty(value = "状态0未读1已读")
	private int status;/**状态0未读1已读**/
	
	//通知主表信息
	@ApiModelProperty(value = "发送人名称")
	private String sendUserRealName;/**发送人名称**/

	@ApiModelProperty(value = "标题")
	private String title;

	@ApiModelProperty(value = "内容")
	private String content;
}
