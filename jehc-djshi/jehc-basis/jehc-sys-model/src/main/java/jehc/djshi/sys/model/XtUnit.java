package jehc.djshi.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 商品(产品)单位
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="商品(产品)单位对象", description="商品(产品)单位")
public class XtUnit extends BaseEntity{

	@ApiModelProperty(value = "单位编号")
	private String xt_unit_id;/**单位编号**/

	@ApiModelProperty(value = "单位名称")
	private String name;/**单位名称**/
}
