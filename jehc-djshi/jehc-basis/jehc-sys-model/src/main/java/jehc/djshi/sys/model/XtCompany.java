package jehc.djshi.sys.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 公司信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="公司信息对象", description="公司信息")
public class XtCompany extends BaseEntity{

	@ApiModelProperty(value = "id")
	private String xt_company_id;/**公司ID**/

	@ApiModelProperty(value = "父id")
	private String parent_id;/**上级公司ID**/

	@ApiModelProperty(value = "公司名称")
	private String name;/**公司名称**/

	@ApiModelProperty(value = "公司电话")
	private String tel;/**公司电话**/

	@ApiModelProperty(value = "公司简介")
	private String remark;/**公司简介**/

	@ApiModelProperty(value = "公司联系人")
	private String contact;/**公司联系人**/

	@ApiModelProperty(value = "公司地址")
	private String address;/**公司地址**/

	@ApiModelProperty(value = "公司性质")
	private String type;/**公司性质**/

	@ApiModelProperty(value = "公司成立时间")
	private String uptime;/**公司成立时间**/

	@ApiModelProperty(value = "是否为子叶0表示存在")
	private int leaf;/**是否为子叶0表示存在**/

	@ApiModelProperty(value = "logo")
	private String images;/****/

	@ApiModelProperty(value = "公司总体负责人")
	private String ceo;/**公司总体负责人**/

	@ApiModelProperty(value = "省份")
	private String xt_provinceID;/**省份**/

	@ApiModelProperty(value = "城市")
	private String xt_cityID;/**城市**/

	@ApiModelProperty(value = "区县")
	private String xt_districtID;/**区县**/
}
