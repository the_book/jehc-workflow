package jehc.djshi.sys.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 行政区划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="行政区划对象", description="行政区划")
public class XtAreaRegion extends BaseEntity{

	@ApiModelProperty(value = "id")
	@JsonProperty(value = "ID")
	private String ID;/**ID编号**/

	@ApiModelProperty(value = "父级ID")
	@JsonProperty(value = "PARENT_ID")
	private String PARENT_ID;/**父级ID**/

	@ApiModelProperty(value = "政行编码")
	@JsonProperty(value = "CODE")
	private String CODE;/**政行编码**/

	@ApiModelProperty(value = "名称")
	@JsonProperty(value = "NAME")
	private String NAME;/**名称**/

	@ApiModelProperty(value = "行政级别")
	@JsonProperty(value = "REGION_LEVEL")
	private int REGION_LEVEL;/**行政级别**/

	@ApiModelProperty(value = "英文")
	@JsonProperty(value = "NAME_EN")
	private String NAME_EN;/**英文**/

	@ApiModelProperty(value = "经度")
	@JsonProperty(value = "LONGITUDE")
	private double LONGITUDE;/**经度**/

	@ApiModelProperty(value = "纬度")
	@JsonProperty(value = "LATITUDE")
	private double LATITUDE;/**纬度**/
}
