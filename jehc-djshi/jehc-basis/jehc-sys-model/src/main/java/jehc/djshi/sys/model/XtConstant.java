package jehc.djshi.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 平台常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="平台常量对象", description="平台常量")
public class XtConstant extends BaseEntity {

	@ApiModelProperty(value = "主键")
	private String xt_constant_id;/**编号**/

	@ApiModelProperty(value = "值")
	private String value;/****/

	@ApiModelProperty(value = "类型：0平台常量1业务常量2工作流常量")
	private int type;/**类型：0平台常量1业务常量2工作流常量**/

	@ApiModelProperty(value = "述描")
	private String remark;/**述描**/

	@ApiModelProperty(value = "常量名称")
	private String ckey;/**常量名称**/

	@ApiModelProperty(value = "流程常量URL可缺省")
	private String url;/**流程常量URL可缺省**/

	@ApiModelProperty(value = "名称")
	private String name;/**名称**/
}
