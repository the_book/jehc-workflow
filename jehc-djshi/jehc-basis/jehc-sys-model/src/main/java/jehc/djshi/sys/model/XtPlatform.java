package jehc.djshi.sys.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.List;
/**
 * @Desc 平台信息发布
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="平台信息发布对象", description="平台信息发布")
public class XtPlatform extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String xt_platform_id;/**主键**/

	@ApiModelProperty(value = "标题")
	private String title;/**标题**/

	@ApiModelProperty(value = "状态0正常1关闭")
	private String status;/**状态0正常1关闭**/

	@ApiModelProperty(value = "备注")
	private String remark;/**备注**/

	@ApiModelProperty(value = "平台反馈意见")
	private List<XtPlatformFeedback> xtPlatformFeedback;/**平台反馈意见**/

	@ApiModelProperty(value = "平台反馈意见移除标识")
	private String xtPlatformFeedback_removed_flag;/**平台反馈意见移除标识**/
}
