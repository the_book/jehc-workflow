package jehc.djshi.sys.model;
import java.util.Date;
import javax.validation.constraints.Size;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import jehc.djshi.common.base.BaseEntity;
/**
 * @Desc 短消息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="短消息对象", description="短消息")
public class XtMessage extends BaseEntity{

	@ApiModelProperty(value = "主键")
	private String xt_message_id;/**主键**/

	@ApiModelProperty(value = "发送者编号")
	private String from_id;/**发送者编号**/

	@ApiModelProperty(value = "接收者编号")
	private String to_id;/**接收者编号**/

	@ApiModelProperty(value = "送发内容")
	@Size(min=1 ,max= 500 ,message = "聊天内容字数必须在1-500之内")
	private String content;/**送发内容**/

	@ApiModelProperty(value = "是否已读0未读1已读")
	private int isread;/**是否已读0未读1已读**/

	@ApiModelProperty(value = "发送时间")
	private Date ctime;/**发送时间**/

	@ApiModelProperty(value = "取读时间")
	private Date readtime;/**取读时间**/

	@ApiModelProperty(value = "发送者")
	private String fromName;/**发送者**/

	@ApiModelProperty(value = "接收者")
	private String toName;/**接收者**/

	@ApiModelProperty(value = "统计个数")
	private int count;/**统计个数**/
}
