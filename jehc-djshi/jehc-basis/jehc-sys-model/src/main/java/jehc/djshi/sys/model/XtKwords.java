package jehc.djshi.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 关键词（敏感词）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="关键词（敏感词）对象", description="关键词（敏感词）")
public class XtKwords extends BaseEntity {

	@ApiModelProperty(value = "平台关键字编号")
	private String xt_kwords_id;/**平台关键字编号**/

	@ApiModelProperty(value = "关键字内容")
	private String content;/**关键字内容**/

	@ApiModelProperty(value = "状态0正常1关闭")
	private String  status;/**状态0正常1关闭**/
}
