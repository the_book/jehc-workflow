package jehc.djshi.sys.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="图标查询参数对象", description="图标查询参数")
public class XtIconForm {

    @ApiModelProperty(value = "名称")
    private String name;/**名称**/

    @ApiModelProperty(value = "分类")
    private String categories;/**分类**/

    @ApiModelProperty(value = "字体")
    private String icon;/**字体**/

    @ApiModelProperty(value = "备注")
    private String remark;/**备注**/
}
