package jehc.djshi.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * @Desc 全局动态提醒
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="全局动态提醒对象", description="全局动态提醒")
public class XtDyRemind extends XtUserinfo{

	@ApiModelProperty(value = "我的通知")
	private List<XtNotifyReceiver> xtNotifyReceiverList;/**我的通知**/

	@ApiModelProperty(value = "我的消息")
	private List<XtMessage> xtMessageList;/**我的消息**/
}
