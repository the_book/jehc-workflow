package jehc.djshi.sys.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 平台IP冻结账户
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="平台IP冻结账户对象", description="平台IP冻结账户")
public class XtIpFrozen extends BaseEntity{

	@ApiModelProperty(value = "IP冻结编号")
	private String xt_ip_frozen_id;/**IP冻结编号**/

	@ApiModelProperty(value = "IP地址")
	private String address;/**IP地址**/

	@ApiModelProperty(value = "状态0正常1冻结2黑名单")
	private Integer status;/**状态0正常1冻结2黑名单**/

	@ApiModelProperty(value = "内容")
	private String content;/**内容**/
}
