package jehc.djshi.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 短信配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="短信配置对象", description="短信配置")
public class XtSms extends BaseEntity{

	@ApiModelProperty(value = "主键")
	private String xt_sms_id;/**ID**/

	@ApiModelProperty(value = "用户名")
	private String name;/**用户名**/

	@ApiModelProperty(value = "短信接口密码")
	private String password;/**短信接口密码**/

	@ApiModelProperty(value = "URL地址")
	private String url;/**URL地址**/

	@ApiModelProperty(value = "公司")
	private String company;/**公司**/

	@ApiModelProperty(value = "电话")
	private String tel;/**电话**/

	@ApiModelProperty(value = "短信平台")
	private String value;/**短信平台**/

	@ApiModelProperty(value = "公司地址")
	private String address;/**公司地址**/

	@ApiModelProperty(value = "联系人")
	private String contacts;/**联系人**/

	@ApiModelProperty(value = "短信协议类型0http1其他")
	private int type;/**短信协议类型0http1其他**/

	@ApiModelProperty(value = "状态0正常1启用")
	private int state;/**状态0正常1启用**/
}
