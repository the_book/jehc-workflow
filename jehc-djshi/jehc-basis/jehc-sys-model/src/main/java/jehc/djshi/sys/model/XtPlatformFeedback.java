package jehc.djshi.sys.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
 * @Desc 平台反馈意见
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="平台反馈意见对象", description="平台反馈意见")
public class XtPlatformFeedback extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String xt_platform_feedback_id;/**主键编号**/

	@ApiModelProperty(value = "平台发布信息编号")
	private String xt_platform_id;/**平台发布信息编号**/

	@ApiModelProperty(value = "评论内容")
	private String content;/**评论内容**/

	@ApiModelProperty(value = "状态")
	private Integer status;/**状态**/
}
