package jehc.djshi.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 字体图标库
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="字体图标库对象", description="字体图标库")
public class XtIcon extends BaseEntity {

    @ApiModelProperty(value = "id")
    private String id;/**id**/

    @ApiModelProperty(value = "名称")
    private String name;/**名称**/

    @ApiModelProperty(value = "分类")
    private String categories;/**分类**/

    @ApiModelProperty(value = "字体")
    private String icon;/**字体**/

    @ApiModelProperty(value = "备注")
    private String remark;/**备注**/
}
