package jehc.djshi.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 平台二维码
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="平台二维码对象", description="平台二维码")
public class XtEncoderqrcode extends BaseEntity{

	@ApiModelProperty(value = "二维码编号")
	private String xt_encoderqrcode_id;/**二维码编号**/

	@ApiModelProperty(value = "二维码链接地址")
	private String url;/**二维码链接地址**/

	@ApiModelProperty(value = "标题")
	private String title;/**标题**/

	@ApiModelProperty(value = "备注")
	private String content;/**备注**/

	@ApiModelProperty(value = "图片编号")
	private String xt_attachment_id;/**图片编号**/

	@ApiModelProperty(value = "二维码图片base64")
	private String qrImage;//二维码图片base64
}
