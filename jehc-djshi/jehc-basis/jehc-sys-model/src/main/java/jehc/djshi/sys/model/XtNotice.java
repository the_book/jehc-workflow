package jehc.djshi.sys.model;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import jehc.djshi.common.base.BaseEntity;
/**
 * @Desc 平台公告
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="平台公告对象", description="平台公告")
public class XtNotice extends BaseEntity{

	@ApiModelProperty(value = "主键")
	private String xt_notice_id;/**主键**/

	@ApiModelProperty(value = "标题")
	private String xt_title;/**标题**/

	@ApiModelProperty(value = "内容")
	@Size(min=0 ,max= 200 ,message = "内容长度不符合标准")
	private String xt_content;/**内容**/

	@ApiModelProperty(value = "创建人")
	private String xt_userinfo_id;/**创建人**/

	@ApiModelProperty(value = "标识0正常1删除")
	private String xt_isDel;/**标识0正常1删除**/

	@ApiModelProperty(value = "状态0初稿1审核中2审核通过3审核未通过")
	private String xt_state;/**状态0初稿1审核中2审核通过3审核未通过**/

	@ApiModelProperty(value = "创建时间")
	private String xt_createTime;/**创建时间**/

	@ApiModelProperty(value = "附件编号")
	private String xt_attachment_id;/**附件编号**/

	@ApiModelProperty(value = "其他附件")
	private String xt_attachment_id_;/**其他附件**/
}
