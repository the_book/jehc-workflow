package jehc.djshi.sys.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 部门信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="部门信息对象", description="部门信息")
public class XtDepartinfo extends BaseEntity{

	@ApiModelProperty(value = "主键")
	private String xt_departinfo_id;/**序列号**/

	@ApiModelProperty(value = "公司id")
	private String xt_company_id;/**公司id**/

	@ApiModelProperty(value = "部门父id")
	private String xt_departinfo_parentId;/**部门父id**/

	@ApiModelProperty(value = "部门名称")
	private String xt_departinfo_name;/**部门名称**/

	@ApiModelProperty(value = "联系电话")
	private String xt_departinfo_connectTelNo;/**联系电话**/

	@ApiModelProperty(value = "移动电话")
	private String xt_departinfo_mobileTelNo;/**移动电话**/

	@ApiModelProperty(value = "传真")
	private String xt_departinfo_faxes;/**传真**/

	@ApiModelProperty(value = "描述部门信息")
	private String xt_departinfo_desc;/**描述部门信息**/

	@ApiModelProperty(value = "图片")
	private String xt_departinfo_image;/**图片**/

	@ApiModelProperty(value = "是否存在子叶0表示存在子节")
	private int xt_departinfo_leaf;/**是否存在子叶0表示存在子节**/

	@ApiModelProperty(value = "成立时间")
	private String xt_departinfo_time;/**成立时间**/

	@ApiModelProperty(value = "部门性质")
	private String xt_departinfo_type;/**部门性质**/

	@ApiModelProperty(value = "父名称")
	private String xt_departinfo_parentName;//父名称

	@ApiModelProperty(value = "部门编码")
	private String d_code;//部门编码
}
