package jehc.djshi.sys.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 平台版本
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="平台版本对象", description="平台版本")
public class XtVersion extends BaseEntity{

	@ApiModelProperty(value = "主键")
	private String xt_version_id;/**平台版本编号**/

	@ApiModelProperty(value = "版本名称")
	private String name;/**版本名称**/

	@ApiModelProperty(value = "下载次数")
	private int down;/**下载次数**/

	@ApiModelProperty(value = "版本描述")
	private String remark;/**版本描述**/

	@ApiModelProperty(value = "附件编号")
	private String xt_attachment_id;/**附件编号**/
}
