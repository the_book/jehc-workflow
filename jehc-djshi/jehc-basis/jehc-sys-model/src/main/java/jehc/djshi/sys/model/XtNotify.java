package jehc.djshi.sys.model;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 通知
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="通知对象", description="通知")
public class XtNotify  extends BaseEntity{

	@ApiModelProperty(value = "主键")
	private String notify_id;/**主键**/

	@ApiModelProperty(value = "标题")
	private String title;/**标题**/

	@ApiModelProperty(value = "内容")
	private String content;/**内容**/

	@ApiModelProperty(value = "通知类型0默认1平台通知")
	private int type;/**通知类型0默认1平台通知(系统自动通知）**/
	private List<XtNotifyReceiver> notifyReceivers;
}
