package jehc.djshi.sys.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 平台知识内容
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="平台知识内容对象", description="平台知识内容")
public class XtKnowledge extends BaseEntity{

	@ApiModelProperty(value = "主键")
	private String xt_knowledge_id;/**ID**/

	@ApiModelProperty(value = "标题")
	private String title;/**标题**/

	@ApiModelProperty(value = "内容")
	private String content;/**内容**/

	@ApiModelProperty(value = "类型：0平台问题1学习知识")
	private String type;/**类型：0平台问题1学习知识**/

	@ApiModelProperty(value = "状态：0待解决1已解决")
	private int state;/**状态：0待解决1已解决**/

	@ApiModelProperty(value = "1紧急2正常3一般")
	private int level;/**1紧急2正常3一般**/
}
