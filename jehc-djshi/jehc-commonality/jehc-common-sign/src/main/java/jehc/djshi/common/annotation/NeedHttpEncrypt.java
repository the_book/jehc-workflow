package jehc.djshi.common.annotation;

import java.lang.annotation.*;

/**
 * @Desc 参数需要"加密"注解
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NeedHttpEncrypt {
    String desc = "Body体参数需要加密注解";
}
