package jehc.djshi.common;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * @Desc 心跳
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@Api(value = "心跳API",tags = "心跳API",description = "心跳API")
public class Heartbeat extends BaseAction {
    /**
     * 心跳
     * @return
     */
    @AuthUneedLogin
    @GetMapping(value="/heartbeat")
    @ApiOperation(value="心跳", notes="心跳")
    public BaseResult heartbeat(){
        return outAudStr(true);
    }
}
