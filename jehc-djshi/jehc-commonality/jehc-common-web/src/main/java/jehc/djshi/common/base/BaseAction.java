package jehc.djshi.common.base;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import net.sf.json.JSONArray;
/**
 * @Desc 控制层基类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@CrossOrigin(origins = "*", maxAge = 3600)
public class BaseAction extends BaseUtils {

	@Autowired
	public BaseUtils baseUtils;

	/**
	 * 采用PageHelper分页方式三
	 * 输出带分页的JSON格式字符串
	 * @param page
	 * @param baseSearch
	 */
	protected BasePage outPageStr(PageInfo page,BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),page.getTotal(),page.getList(),true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),page.getTotal(),page.getList(),true);
		}
	}
	
	/**
	 * 采用PageHelper分页方式三-------（Bootstrap分页）
	 * 输出带分页的JSON格式字符串
	 */
	protected BasePage outPageBootStr(PageInfo page,BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),page.getTotal(),page.getList(),true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),page.getTotal(),page.getList(),true);
		}
	}

	/**
	 * 分页方式二（bootstrap风格）
	 * 输出带分页的JSON格式字符串
	 * @param jsonArray
	 */
	protected BasePage outPageBootStr(JSONArray jsonArray,int total,BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			return new BasePage(baseSearch.getStart(),baseSearch.getPageSize(),new Long(total),jsonArray,true);
		}else{
			return new BasePage(baseSearch.getPageNo(),baseSearch.getPageSize(),new Long(total),jsonArray,true);
		}
	}

	/**
	 * 输出添加删除修改结果JSON格式字符串
	 * @param flag
	 * @param msg
	 */
	protected BaseResult outAudStr(boolean flag,String msg){
		return new BaseResult(msg,flag);
	}
	
	/**
	 * 输出添加删除修改结果JSON格式字符串
	 * @param flag
	 * @param msg
	 */
	protected BaseResult outAudStr(boolean flag,String msg,Object data){
		return new BaseResult(msg,flag,data);
	}
	
	/**
	 * 输出添加删除修改结果JSON格式字符串
	 * @param flag
	 */
	protected BaseResult outAudStr(boolean flag){
		if(flag){
			return BaseResult.success();
		}else{
			return BaseResult.fail();
		}
	}

	/**
	 * 输出对象
	 * @param obj
	 */
	protected BaseResult outDataStr(Object obj){
		return new BaseResult(obj);
	}

	/**
	 * 方式一
	 * 输出查找对象的JSON格式字符串 或集合JSON格式字符串
	 * @param obj
	 */
	protected BaseResult outItemsStr(Object obj){
		return new BaseResult(obj);
	}

	/**
	 * @param obj
	 */
	protected BaseResult outComboDataStr(Object obj){
		return new BaseResult(obj);
	}

	/**
	 * 输出普通的字符串
	 * @param jsonStr
	 */
	protected BaseResult outStr(String jsonStr){
		return new BaseResult(jsonStr);
	}

	/**
	 * 封装共同分页参数
	 */
	@Deprecated
	protected void commonPager(Map<String,Object> condition,BaseSearch baseSearch){
		Integer start = baseSearch.getStart();
		Integer pageSize = baseSearch.getPageSize();
		condition.put("offset", start);
		condition.put("pageSize", pageSize);
	}

	/**
	 * 封装共同分页参数（采用PageHelper offsetPage方式）
	 * @param baseSearch
	 */
	protected void commonHPager(BaseSearch baseSearch){
		if(baseSearch.getUsePageNo() == false){
			Integer start = baseSearch.getStart();
			Integer pageSize = baseSearch.getPageSize();
			PageHelper.offsetPage(start, pageSize);
		}else{
			Integer pageNo = baseSearch.getPageNo();
			Integer pageSize = baseSearch.getPageSize();
			if(null == pageNo){
				pageNo = 1;
			}
			if(null == pageSize){
				pageSize = 30;
			}
			PageHelper.startPage(pageNo, pageSize);
		}
	}
}
