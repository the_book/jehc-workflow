package jehc.djshi.common.exception;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseUtils;
import jehc.djshi.common.constant.StatusConstant;
import jehc.djshi.common.idgeneration.UUID;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.log.client.service.LogsUtil;
import jehc.djshi.log.model.LogError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
/**
 * @Desc 全局异常捕捉
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestControllerAdvice
@Slf4j
public class GlobalException {

    @Autowired
    BaseUtils baseUtils;

    @Autowired
    LogsUtil logsUtil;


    /**
     * 处理自定义的业务异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = ExceptionUtil.class)
    public BaseResult bizExceptionHandler(HttpServletRequest req, ExceptionUtil e){
        log(e.getMessage(),6,req);
        return new BaseResult(StatusConstant.XT_PT_STATUS_VAL_500,e.getMessage(),false);
    }

    /**
     * 处理空指针的异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =NullPointerException.class)
    public BaseResult exceptionHandler(HttpServletRequest req, NullPointerException e){
        log(e.getMessage(),5,req);
        return new BaseResult(StatusConstant.XT_PT_STATUS_VAL_500,"操作出现空指针异常",false);
    }

    /**
     * 处理IO的异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =IOException.class)
    public BaseResult exceptionHandler(HttpServletRequest req, IOException e){
        log(e.getMessage(),1,req);
        return new BaseResult(StatusConstant.XT_PT_STATUS_VAL_500,"操作出现IO异常",false);
    }

    /**
     * 处理SQL的异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =SQLException.class)
    public BaseResult exceptionHandler(HttpServletRequest req, SQLException e){
        log(e.getMessage(),2,req);
        return new BaseResult(StatusConstant.XT_PT_STATUS_VAL_500,"操作出现SQL异常",false);
    }

    /**
     * 处理数字格式化的异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =NumberFormatException.class)
    public BaseResult exceptionHandler(HttpServletRequest req, NumberFormatException e){
        log(e.getMessage(),5,req);
        return new BaseResult(StatusConstant.XT_PT_STATUS_VAL_500,"操作出现数字格式化异常",false);
    }


    /**
     * 处理其他异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =Exception.class)
    public BaseResult exceptionHandler(HttpServletRequest req, Exception e){
        log(e.getMessage(),0,req);
        return new BaseResult(StatusConstant.XT_PT_STATUS_VAL_500,"操作出现未知异常",false);
    }

    /**
     *
     * @param message
     * @param type
     */
    void log(String message,int type,HttpServletRequest req){
        LogError logError = new LogError();
        logError.setId(UUID.toUUID());
        logError.setContent(message);
        logError.setCreate_time(new Date());
        logError.setType(type);
        logError.setCreate_id(baseUtils.getXtUid());
        try {
            logsUtil.aBLogError(logError);
        }catch (Exception e){
            log.debug("调用存入异常日志出现异常:" + e.getMessage(), e);
        }
    }
}
