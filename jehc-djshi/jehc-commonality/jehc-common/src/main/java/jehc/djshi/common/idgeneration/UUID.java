package jehc.djshi.common.idgeneration;

import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.date.DateUtil;

/**
 * @Desc UUID生成策略
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class UUID extends DateUtil {
	/**
	 * 生成UUID
	 * @return
	 */
	public static String toUUID(){
		try {
			return java.util.UUID.randomUUID().toString().replace("-", "").toUpperCase();
		} catch (Exception e) {
			throw new ExceptionUtil("生成UUID失败，原因：{}",e);
		}
	}

	/**
	 * 生成UUID
	 * @return
	 */
	public static String uuid(){
		try {
			return java.util.UUID.randomUUID().toString();
		} catch (Exception e) {
			throw new ExceptionUtil("生成UUID失败，原因：{}",e);
		}
	}
}
