package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 员工信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="员工信息实体", description="员工信息实体")
public class UserinfoEntity extends BaseEntity{

    @ApiModelProperty(value = "用户ID")
    private String xt_userinfo_id;/**用户ID**/

    @ApiModelProperty(value = "公司ID外键")
    private String xt_company_id;/**公司ID外键**/

    @ApiModelProperty(value = "所属部门外键，DepartInfo的DepartId")
    private String xt_departinfo_id;/**所属部门外键，DepartInfo的DepartId**/

    @ApiModelProperty(value = "岗位")
    private String xt_post_id;/**岗位**/

    @ApiModelProperty(value = "用户名")
    private String xt_userinfo_name;/**用户名**/

    @ApiModelProperty(value = "密码")
    private String xt_userinfo_passWord;/**密码**/

    @ApiModelProperty(value = "地址")
    private String xt_userinfo_address;/**地址**/

    @ApiModelProperty(value = "状态:数据字典")
    private String xt_userinfo_state;/**状态:数据字典**/

    @ApiModelProperty(value = "0-未删除，1-删除")
    private int xt_userinfo_isDelete;/**0-未删除，1-删除**/

    @ApiModelProperty(value = "用户头像")
    private String xt_userinfo_image;/**用户头像**/

    @ApiModelProperty(value = "0-离线1-在线2-忙碌3-离开")
    private int xt_userinfo_status;/**0-离线1-在线2-忙碌3-离开**/

    @ApiModelProperty(value = "真实姓名")
    private String xt_userinfo_realName;/**真实姓名**/

    @ApiModelProperty(value = "联系电话")
    private String xt_userinfo_phone;/**联系电话**/

    @ApiModelProperty(value = "身份证号码")
    private String xt_userinfo_card;/**身份证号码**/

    @ApiModelProperty(value = "性别:数据字典")
    private String xt_userinfo_sex;/**性别:数据字典**/

    @ApiModelProperty(value = "是否已婚:数据字典")
    private String xt_userinfo_ismarried;/**是否已婚:数据字典**/

    @ApiModelProperty(value = "0,普通用户 1,超级管理")
    private int xt_userinfo_isAdmin;/**0,普通用户 1,超级管理**/

    @ApiModelProperty(value = "名族:数据字典")
    private String xt_userinfo_nation;/**名族:数据字典**/

    @ApiModelProperty(value = "籍贯")
    private String xt_userinfo_origo;/**籍贯**/

    @ApiModelProperty(value = "照片")
    private String xt_userinfo_pic;/**照片**/

    @ApiModelProperty(value = "移动电话")
    private String xt_userinfo_mobile;/**移动电话**/

    @ApiModelProperty(value = "其他电话")
    private String xt_userinfo_ortherTel;/**其他电话**/

    @ApiModelProperty(value = "入职时间")
    private String xt_userinfo_intime;/**入职时间**/

    @ApiModelProperty(value = "离职时间")
    private String xt_userinfo_outTime;/**离职时间**/

    @ApiModelProperty(value = "合同到期时间")
    private String xt_userinfo_contractTime;/**合同到期时间**/

    @ApiModelProperty(value = "备注")
    private String xt_userinfo_remark;/**备注**/

    @ApiModelProperty(value = "生日")
    private String xt_userinfo_birthday;/**生日**/

    @ApiModelProperty(value = "qq号码")
    private String xt_userinfo_qq;/**qq号码**/

    @ApiModelProperty(value = "电子邮件")
    private String xt_userinfo_email;/**电子邮件**/

    @ApiModelProperty(value = "政治面貌:数据字典")
    private String xt_userinfo_politicalStatus;/**政治面貌:数据字典**/

    @ApiModelProperty(value = "文化程度:数据字典")
    private String xt_userinfo_highestDegree;/**文化程度:数据字典**/

    @ApiModelProperty(value = "毕业学校")
    private String xt_userinfo_schoolName;/**毕业学校**/

    @ApiModelProperty(value = "工作年限:数据字典")
    private String xt_userinfo_workYear;/**工作年限:数据字典**/

    @ApiModelProperty(value = "部门名称")
    private String xt_departinfo_name;/**部门名称**/

    @ApiModelProperty(value = "岗位名称")
    private String xt_post_name;/**岗位名称**/

    @ApiModelProperty(value = "账号类型编号逗号分隔")
    private String account_type_id;/**账号类型编号逗号分隔**/

    @ApiModelProperty(value = "是否同步账号类型1同步 0否")
    private int sync;/**是否同步账号类型1同步 0否**/
}
