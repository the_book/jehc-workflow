package jehc.djshi.common.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 附件实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="附件实体对象", description="附件实体")
public class AttachmentEntity  extends BaseEntity {

    @ApiModelProperty(value = "附件编号")
    private String xt_attachment_id;/**附件编号**/

    @ApiModelProperty(value = "文件类型")
    private String xt_attachmentType;/**文件类型**/

    @ApiModelProperty(value = "文件上传时间")
    private String xt_attachmentCtime;/**文件上传时间**/

    @ApiModelProperty(value = "文件大小")
    private String xt_attachmentSize;/**文件大小**/

    @ApiModelProperty(value = "文件相对路径")
    private String xt_attachmentPath;/**文件相对路径**/

    @ApiModelProperty(value = "0正常1删除")
    private int xt_attachmentIsDelete;/**0正常1删除**/

    @ApiModelProperty(value = "附件名称")
    private String xt_attachmentName;/**附件名称**/

    @ApiModelProperty(value = "传上者编号")
    private String xt_userinfo_id;/**传上者编号**/

    @ApiModelProperty(value = "传上模块编号")
    private String xt_modules_id;/**传上模块编号**/

    @ApiModelProperty(value = "顺序")
    private int xt_modules_order;/**顺序**/

    @ApiModelProperty(value = "附件原名称")
    private String xt_attachmentTitle;/**附件原名称**/

    @ApiModelProperty(value = "平台路径配置中心键（自定义上传绝对路径使用）")
    private String xt_path_absolutek;/**平台路径配置中心键（自定义上传绝对路径使用）**/

    @ApiModelProperty(value = "平台路径配置中心键（自定义上传相对路径使用）")
    private String xt_path_relativek;/**平台路径配置中心键（自定义上传相对路径使用）**/

    @ApiModelProperty(value = "平台路径配置中心键（自定义上传路径 自定义URL地址）")
    private String xt_path_urlk;/**平台路径配置中心键（自定义上传路径 自定义URL地址）**/

    @ApiModelProperty(value = "字段名称")
    private String field_name;/**字段名称**/

    @ApiModelProperty(value = "整个路径如http://www.jehc.com/images/img.png")
    private String fullUrl;/**整个路径如http://www.jehc.com/images/img.png**/
}

