package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc DbEntity
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="DB实体对象", description="DB实体")
public class DbEntity extends BaseEntity {

    @ApiModelProperty(value = "序列号")
    private String xt_db_id;/**序列号**/

    @ApiModelProperty(value = "备份的数据库名称")
    private String xt_db_name;/**备份的数据库名称**/

    @ApiModelProperty(value = "备份时间")
    private String xt_db_time;/**备份时间**/

    @ApiModelProperty(value = "备份路径")
    private String xt_db_path;/**备份路径**/

    @ApiModelProperty(value = "当前执行人")
    private String xt_db_user;/**当前执行人**/

    @ApiModelProperty(value = "0表示整库备份1按指定表备份")
    private int xt_db_type;/**0表示整库备份1按指定表备份**/
}
