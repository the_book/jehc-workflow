package jehc.djshi.common.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * @Desc 字段注解
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Retention(RetentionPolicy.RUNTIME)  
@Target( { java.lang.annotation.ElementType.FIELD })  
public @interface AnnotationColumn {  
    String Field();  
} 
