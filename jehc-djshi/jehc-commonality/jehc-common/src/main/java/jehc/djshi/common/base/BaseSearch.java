package jehc.djshi.common.base;

import java.io.Serializable;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.util.FastJsonUtils;
import jehc.djshi.common.util.MapUtils;
import jehc.djshi.common.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import net.sf.json.JSONObject;
/**
 * @Desc 基础搜索
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class BaseSearch extends BasePage implements Serializable {
	private static final long serialVersionUID = 5216143475139692132L;

	@ApiModelProperty("通用查询参数")
	private String searchJson;

	@ApiModelProperty("自定义参数")
	private String definedJson;/**自定义参数**/

	public String getSearchJson() {
		return searchJson;
	}

	public void setSearchJson(String searchJson) {
		this.searchJson = searchJson;
	}

	/**
	 *
	 * @return
	 */
	public Map<String, Object> convert(){
		try {
			if(!StringUtil.isEmpty(searchJson)){
				Map<String, Object> map = JSONObject.fromObject(URLDecoder.decode(getSearchJson(), "UTF-8"));
				map = MapUtils.resetMap(map);
				return map;
			}
		} catch (Exception e) {
			super.error("BaseSearch","查询条件参数转换出现异常："+e.getMessage());
			new HashMap<String, Object>();
		}
		return new HashMap<String, Object>();
	}

	public String getDefinedJson() {
		return definedJson;
	}

	public void setDefinedJson(String definedJson) {
		this.definedJson = definedJson;
	}

	/**
	 * 自定义参数JSON转Map
	 * @return
	 */
	public Map<String,Object> ConvertToMap(){
		if(!StringUtils.isEmpty(getDefinedJson())){
			return FastJsonUtils.toBean(getDefinedJson(), Map.class);
		}
		return new HashMap<String,Object>();
	}
}
