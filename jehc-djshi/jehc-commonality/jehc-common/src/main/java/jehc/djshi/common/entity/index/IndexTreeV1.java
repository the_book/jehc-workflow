package jehc.djshi.common.entity.index;

import jehc.djshi.common.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc IndexTree
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class IndexTreeV1 {
    private List<ResourceEntity> nodes;

    public IndexTreeV1(List<ResourceEntity> nodes) {
        this.nodes = nodes;
    }

    /**
     *
     * @param adminText
     * @return
     */
    public String buildTree(boolean adminText) {
        StringBuffer html = new StringBuffer();
        for(ResourceEntity node:nodes){
            if ("0".equals(node.getResources_parentid()) && !StringUtil.isEmpty(node.getKeyid()) &&  !node.getKeyid().equals("jEhcDevModule")) {

                //根目录开始
                if(node.getResources_leaf() == 0){//存在子级菜单
                    html.append("<li " +
                            "aria-haspopup=\"true\" " +
                            "data-menu-toggle=\"hover\" " +
                            "class=\"menu-item menu-item-submenu\"" +
                            " v-bind:class=\"{ 'menu-item-open': hasActiveChildren('"+node.getComponent_open_style()+"') }\" " +
                            ">");
                    html.append("<a href=\"#\" class=\"menu-link menu-toggle\">");
                    html.append("<i class=\"menu-icon "+node.getComponent_icon()+"\"></i>");
                    html.append("<span class=\"menu-text\">"+node.getResources_title()+"</span>");
                    html.append("<i class=\"menu-arrow\"></i>");
                    html.append("</a>");

                    html.append(" <div class=\"menu-submenu\">");
                    html.append(" <span class=\"menu-arrow\"></span>");
                    html.append(" <ul class=\"menu-subnav\">");
                    html.append(" <li aria-haspopup=\"true\" class=\"menu-item menu-item-parent\">");
                    html.append(" <span class=\"menu-link\">");
                    html.append(" <span class=\"menu-text\">"+node.getResources_title()+"</span>");
                    html.append(" </span>");
                    html.append(" </li>");

                    //递归子目录
                    html.append(build(node,node.getResources_id()));

                    html.append(" </ul>");
                    html.append(" </div>");
                    html.append(" </li>");
                }else{

                    //一级菜单如果没有子级则可以连接
                    html.append("<router-link ");
                    html.append(" to="+node.getComponent_router());//组件路由
                    html.append(" v-slot=\"{ href, navigate, isActive, isExactActive }\">");


                    html.append("<li");
                    html.append(" aria-haspopup=\"true\"");
                    html.append(" data-menu-toggle=\"hover\"");
                    html.append(" class=\"menu-item\"");
                    html.append(" :class=\"[isActive && 'menu-item-active',isExactActive && 'menu-item-active']\"");
                    html.append(" >");
                    html.append(" <a :href=\"href\" class=\"menu-link\" @click=\"navigate\">");
                    html.append(" <i class=\"menu-bullet text-dark-50 "+node.getComponent_icon()+" mr-2\">");
                    html.append(" <span></span>");
                    html.append(" </i>");
                    html.append(" <span class=\"menu-text\">"+node.getResources_title()+"</span>");
                    html.append(" </a>");
                    html.append(" </li>");
                    html.append(" </router-link>");
                }
            }

        }
        return html.toString();
    }

    /**
     *
     * @param node
     * @param rootId
     * @return
     */
    private String build(ResourceEntity node,String rootId) {
        StringBuffer html = new StringBuffer();
        List<ResourceEntity> children = getChildren(node);
        if (!children.isEmpty()) {
            for (ResourceEntity child:children) {
                if (child.getResources_leaf() == 0) {//存在子级
                    html.append(" <li");
                    html.append(" aria-haspopup=\"true\"");
                    html.append(" data-menu-toggle=\"hover\"");
                    html.append(" class=\"menu-item menu-item-submenu\"");
                    html.append(" v-bind:class=\"{'menu-item-open': hasActiveChildren('" + child.getComponent_open_style() + "')}\"");
                    html.append(" >");
                    html.append(" <a href=\"#\" class=\"menu-link menu-toggle\">");
                    html.append(" <i class=\"menu-bullet text-dark-50 " + child.getComponent_icon() + " mr-2\">");
                    html.append(" <span></span>");
                    html.append(" </i>");
                    html.append(" <span class=\"menu-text\">" + child.getResources_title() + "</span>");
                    html.append(" <i class=\"menu-arrow\"></i>");
                    html.append(" </a>");

                    //递归子目录
                    String buildSbf = build(child, rootId);
                    html.append(buildSbf);

                    html.append(" </li>");

                } else {
                    //不存在子级
                    html.append("<router-link ");
                    html.append(" to=" + child.getComponent_router());//组件路由
                    html.append(" v-slot=\"{ href, navigate, isActive, isExactActive }\">");


                    html.append("<li");
                    html.append(" aria-haspopup=\"true\" ");
                    html.append(" data-menu-toggle=\"hover\" ");
                    html.append(" class=\"menu-item\" ");
                    html.append(" :class=\"[isActive && 'menu-item-active',isExactActive && 'menu-item-active']\"");
                    html.append(" >");
                    html.append(" <a :href=\"href\" class=\"menu-link\" @click=\"navigate\">");
                    html.append(" <i class=\"menu-bullet text-dark-50 " + child.getComponent_icon() + " mr-2\">");
                    html.append(" <span></span>");
                    html.append(" </i>");
                    html.append(" <span class=\"menu-text\">" + child.getResources_title() + "</span>");
                    html.append(" </a>");
                    html.append(" </li>");
                    html.append(" </router-link>");
                }
            }
        }
        return html.toString();
    }

    /**
     *
     * @param node
     * @return
     */
    private List<ResourceEntity> getChildren(ResourceEntity node) {
        List<ResourceEntity> children = new ArrayList<ResourceEntity>();
        for (ResourceEntity child:nodes) {
            if (child.getResources_parentid().equals(node.getResources_id())) {
                children.add(child);
            }
        }
        return children;
    }

    /**
     *
     * @param id
     * @return
     */
    private String idBu(String id){
        StringBuffer ids = new StringBuffer();
        return ids.append(idList(id)).toString();
    }

    /**
     *
     * @param id
     * @return
     */
    private String idList(String id){
        StringBuffer ids = new StringBuffer();
        for(ResourceEntity node: nodes){
            if(id.equals(node.getResources_id())){
                if(!StringUtil.isEmpty(node.getResources_parentid()) && !"0".equals(node.getResources_parentid())){
                    ids.append(node.getResources_id()+",");
                    ids.append(idList(node.getResources_parentid()));
                }else{
                    ids.append(node.getResources_id());
                }
            }
        }
        return ids.toString();
    }
}
