package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @Desc 上传文件实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="上传文件实体", description="上传文件实体")
public class UploadEntity {

	@ApiModelProperty(value = "validateparameter校验类型")
	private String validateparameter;//validateparameter校验类型

	@ApiModelProperty(value = "validateSize校验大小")
	private String validateSize;//validateSize校验大小

	@ApiModelProperty(value = "xt_path_absolutek绝对路径键（自定义）")
	private String xt_path_absolutek;//xt_path_absolutek绝对路径键（自定义）

	@ApiModelProperty(value = "relative_path相对路径如数据库存放路径")
	private String xt_path_relativek;//relative_path相对路径如数据库存放路径

	@ApiModelProperty(value = "xt_path_urlk基础路径URL（自定义）")
	private String xt_path_urlk;//xt_path_urlk基础路径URL（自定义）
}
