package jehc.djshi.common.util;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.ByteArrayOutputStream;
import java.util.regex.Pattern;
import gui.ava.html.image.generator.HtmlImageGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import javax.imageio.ImageIO;

/**
 * @Desc 清空html标签
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class HtmlUtils{
	public static String returnTextFromTHML(String htmlStr) {
		Document doc = Jsoup.parse(htmlStr);
		String text = doc.text();
		// remove extra white space
		StringBuilder builder = new StringBuilder(text);
		int index = 0;
		while(builder.length()>index){
			char tmp = builder.charAt(index);
			if(Character.isSpaceChar(tmp) || Character.isWhitespace(tmp)){
				builder.setCharAt(index, ' ');
			}
			index++;
		}
		text = builder.toString().replaceAll(" +", " ").trim();
		return text;
	}


	/**
	 * 转码
	 * @param s
	 * @return
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}

	public static String Html2Text(String inputString){
	     String htmlStr = inputString; //含html标签的字符串
	     String textStr ="";
	     java.util.regex.Pattern p_script;
	     java.util.regex.Matcher m_script;
	     java.util.regex.Pattern p_style;
	     java.util.regex.Matcher m_style;
	     java.util.regex.Pattern p_html;
	     java.util.regex.Matcher m_html;

	    try{
	          String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>"; //定义script的正则表达式{或<script[^>]*?>[\\s\\S]*?<\\/script> }
	          String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>"; //定义style的正则表达式{或<style[^>]*?>[\\s\\S]*?<\\/style> }
	          String regEx_html = "<[^>]+>"; //定义HTML标签的正则表达式

	          p_script = Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE);
	          m_script = p_script.matcher(htmlStr);
	          htmlStr = m_script.replaceAll(""); //过滤script标签

	          p_style = Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE);
	          m_style = p_style.matcher(htmlStr);
	          htmlStr = m_style.replaceAll(""); //过滤style标签

	          p_html = Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE);
	          m_html = p_html.matcher(htmlStr);
	          htmlStr = m_html.replaceAll(""); //过滤html标签

	          textStr = htmlStr;
	     }catch(Exception e){
	     }
	     return textStr;//返回文本字符串
	 }

	public static String changeHtml(String text){
		text=text.replace("\n","");//回车
		text=text.replace("\t","");//换行
		text=text.replace("&","&amp;");
		text=text.replace(" ","&nbsp;");
		text=text.replace("<","&lt;");
		text=text.replace(">","&gt;");
		text=text.replace("\"","&quot;");//双引号转义
		text=text.replace("public","<b>public</b>");
		text=text.replace("class","<b>class</b>");
		text=text.replace("static","<b>static</b>");
		text=text.replace("void","<b>void</b>");
		String t=text.replace("//","<font color=green>//");
		if(!text.equals(t)){
			text = t+"</font>";
		}
		return text;
	}


	/**
	 * html生成图片
	 * @param template html文本模板
	 * @param times 执行时间 可以调控 如果模板中含有图片则可以通过增加该时间来控制生成 否则有问题
	 * @return
	 */
	public String html2Image(String template,Long times){
		HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
		imageGenerator.loadHtml(template);
		BufferedImage bufferedImage = null;
		ByteArrayOutputStream outputStream = null;
		String res = null;
		try {
			bufferedImage = imageGenerator.getBufferedImage();
			if(null != times){
				Thread.sleep(times);
			}
//			imageGenerator.saveAsImage("D://template.png");//生成文件方式保存位置
//			bufferedImage = getGrayPicture(bufferedImage);

			outputStream = new ByteArrayOutputStream();
			ImageIO.write(bufferedImage, "png", outputStream);
			String base64Img = Base64.encodeBase64String(outputStream.toByteArray());
			res = "data:image/png;base64," + base64Img.toString();
			log.info("Image base64：{}",res);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(outputStream != null){
				try {
					outputStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}


	/**
	 *
	 * @param originalImage
	 * @return
	 */
	public BufferedImage getGrayPicture(BufferedImage originalImage) {
		BufferedImage grayPicture;
		int imageWidth = originalImage.getWidth();
		int imageHeight = originalImage.getHeight();
		grayPicture = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
		ColorConvertOp cco = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
		cco.filter(originalImage, grayPicture);
		return grayPicture;
	}

//	/**
//	 * 测试
//	 * @param args
//	 */
//	public static void main(String args[]){
//		HtmlUtils htmlUtils = new HtmlUtils();
//		htmlUtils.html2Image(getTemplate(),10000L);
//	}


	/**
	 * 获取模板
	 * @return
	 */
	public static String getTemplate(){
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<table style='background: black; height: 304px; width: 400px; display: flex;'>");
		stringBuilder.append("<tr>");
		stringBuilder.append("<td style='width: 94px; height: 304px; align-items: center; border-right: 2px solid white; display: flex; justify-content: center;'>");
		stringBuilder.append("<table style='margin 0 auto;text-align:center;'>");
		stringBuilder.append("<tr>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("<tr>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("<tr>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("<tr>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("<tr>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("<img style='height: 36px; width: 28px;' src=''>");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("</table>");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("<table style='width: 306px; height: 304px; align-items: center;'>");
		stringBuilder.append("<tr style='width: 96px; color: white; font-size: 36px; margin 0 auto;text-align:center;'>");
		stringBuilder.append("<td colspan='2' style='border-bottom: 2px solid white;'>");
		stringBuilder.append("桩桩营服务区");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("<tr style='width: 96px; color: white; font-size: 32px; margin 0 auto;text-align:center;'>");
		stringBuilder.append("<td >");
		stringBuilder.append("小客车区");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("800");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("<tr style='width: 96px; color: white; font-size: 32px; margin 0 auto;text-align:center;'>");
		stringBuilder.append("<td >");
		stringBuilder.append("大客车区");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("680");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("<tr style='width: 96px; color: white; font-size: 32px; margin 0 auto;text-align:center;'>");
		stringBuilder.append("<td >");
		stringBuilder.append("重点车量区");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("10");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("<tr style='width: 96px; color: white; font-size: 32px; margin 0 auto;text-align:center;'>");
		stringBuilder.append("<td >");
		stringBuilder.append("客流饱和度");
		stringBuilder.append("</td>");
		stringBuilder.append("<td>");
		stringBuilder.append("16%");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");

		stringBuilder.append("</table>");
		stringBuilder.append("</td>");
		stringBuilder.append("</tr>");
		stringBuilder.append("</table>");
		System.out.println(stringBuilder.toString());
		return stringBuilder.toString();
	}
}
