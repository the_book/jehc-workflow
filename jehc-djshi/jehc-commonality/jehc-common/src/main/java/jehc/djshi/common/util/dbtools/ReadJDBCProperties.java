package jehc.djshi.common.util.dbtools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import jehc.djshi.common.constant.PathConstant;
import jehc.djshi.common.util.logger.LogUtil;
import jehc.djshi.common.constant.PathConstant;
import jehc.djshi.common.util.logger.LogUtil;
import org.springframework.util.ResourceUtils;
/**
 * @Desc 读取JDBC配置文件
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class ReadJDBCProperties{
	/**
	 * 读取配置文件
	 * @return
	 */
	public static Properties readProperties(){
		Properties pro = new Properties();
		try {
			InputStream input = new FileInputStream(ResourceUtils.getFile(PathConstant.JDBC_PROPERTIES_PATH));
			pro.load(input);
		} catch (IOException e) {
			LogUtil logUtil =new LogUtil();
			logUtil.error("ReadJDBCProperties","未找到配置文件,详细信息"+e.getCause().getMessage());
		}
		return pro;
	}
	/**
	 * 返回数据库类型1.Mysql 2.Oracle 3.Sqlserver 4.DB2 5.Sybase 6.其他
	 * @return
	 */
	public static int validateDriver(){
		Properties pro = ReadJDBCProperties.readProperties();
		String driverClassName = pro.get("driverClassName").toString().toLowerCase();
		if(driverClassName.indexOf("mysql")>0){
			return 1;
		}else if(driverClassName.indexOf("oracle")>0){
			return 2;
		}else if(driverClassName.indexOf("sqlserver")>0){
			return 3;
		}else if(driverClassName.indexOf("db2")>0){
			return 4;
		}else if(driverClassName.indexOf("sybase")>0){
			return 5;
		}else{
			return 6;
		}
	}
}
