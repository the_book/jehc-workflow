package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 数据字典实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="数据字典实体", description="数据字典实体")
public class DataDictionaryEntity {

    @ApiModelProperty(value = "主键")
    private String xt_data_dictionary_id;/**主键**/

    @ApiModelProperty(value = "数据字典名称")
    private String xt_data_dictionary_name;/**数据字典名称**/

    @ApiModelProperty(value = "父编号")
    private String xt_data_dictionary_pid;/**父编号**/

    @ApiModelProperty(value = "备注")
    private String xt_data_dictionary_remark;/**备注**/

    @ApiModelProperty(value = "数值")
    private String xt_data_dictionary_value;/**数值**/

    @ApiModelProperty(value = "态状0启用1暂停")
    private int xt_data_dictionary_state;/**态状0启用1暂停**/

    @ApiModelProperty(value = "排序号")
    private String xt_data_dictionary_soft;/**排序号**/
}
