package jehc.djshi.common.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
/**
 * @Desc 基类JSON
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@ApiModel(value = "基类JSON")
@Data
public class BaseJson  implements Serializable{
	private static final long serialVersionUID = 7063637441613949288L;

	@ApiModelProperty(value = "成功标志")
	private Boolean success;/**成功标志**/

	@ApiModelProperty(value = "提示消息")
	private String message;/**提示消息**/

	@ApiModelProperty(value = "临时ID")
	private String jsonID;/**临时ID**/

	@ApiModelProperty(value = "临时值")
	private String jsonValue;/**临时值**/

	@ApiModelProperty(value = "返回上传文件之后的类型")
	private String fileType;/**返回上传文件之后的类型**/
	
	public BaseJson(){}
	
	public BaseJson(Boolean success,String message){
		this.success =success;
		this.message =message;
	}
	
	public BaseJson(String message,String jsonID,String jsonValue,String fileType){
		this.message =message;
		this.jsonID =jsonID;
		this.jsonValue =jsonValue;
		this.fileType =fileType;
	}
	

}
