package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 角色实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="角色实体", description="角色实体")
public class RoleinfoEntity {

    @ApiModelProperty(value = "账号ID与UserID一致")
    private String account_id;

    @ApiModelProperty(value = "角色id")
    private String role_id;

    @ApiModelProperty(value = "资源id")
    private String sources_id;
}
