package jehc.djshi.common.util;

import java.io.*;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
/**
 * @Desc FTP工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class FtpUtil implements Serializable {
	private static final long serialVersionUID = -6305079897518518204L;
	private static String url;
	private static int port;
	private static String username;
	private static String password;

	public static String getUrl() {
		return url;
	}

	public static void setUrl(String url) {
		FtpUtil.url = url;
	}

	public static int getPort() {
		return port;
	}

	public static void setPort(int port) {
		FtpUtil.port = port;
	}

	public static String getUsername() {
		return username;
	}

	public static void setUsername(String username) {
		FtpUtil.username = username;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		FtpUtil.password = password;
	}

	/**
	 * @param path FTP服务器保存目录
	 * @param fname 上传到FTP服务器上的文件名
	 * @param input 输入流
	 * @return 成功返回true，否则返回false
	 */
	public static boolean uploadFile(String path, String fname,InputStream input) {
		boolean success = false;
		FTPClient ftp = new FTPClient();
		try {
			int reply;
			ftp.connect(getUrl(), getPort());
			ftp.login(getUsername(), getPassword());
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return success;
			}
			ftp.changeWorkingDirectory(path);
			//设置上传文件的类型为二进制类型  
            ftp.setFileType(FTP.BINARY_FILE_TYPE);  
			ftp.storeFile(fname, input);
			input.close();
			ftp.logout();
			success = true;
		} catch (IOException e) {
			throw new ExceptionUtil("上传出现异常"+e.getMessage());
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					throw new ExceptionUtil("上传出现异常"+ioe.getMessage());
				}
			}
		}
		return success;
	}

	/**
	 * @param remotePath FTP服务器上的相对路径
	 * @param fname 下载文件名
	 * @param localPath 下载后保存到本地的路径
	 * @return
	 */
	public static boolean downFile(String remotePath,String fname, String localPath) {
		boolean success = false;
		FTPClient ftp = new FTPClient();
		try {
			int reply;
			ftp.connect(getUrl(), getPort());
			ftp.login(getUsername(), getPassword());
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return success;
			}
			ftp.changeWorkingDirectory(remotePath);
			FTPFile[] fs = ftp.listFiles();
			for (FTPFile ff : fs) {
				if (ff.getName().equals(fname)) {
					File localFile = new File(localPath + "/" + ff.getName());
					OutputStream is = new FileOutputStream(localFile);
					ftp.retrieveFile(ff.getName(), is);
					is.close();
				}
			}
			ftp.logout();
			success = true;
		} catch (IOException e) {
			throw new ExceptionUtil("下载出现异常"+e.getMessage());
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					throw new ExceptionUtil("下载出现异常"+ioe.getMessage());
				}
			}
		}
		return success;
	}
}
