package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 文件路径
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="文件路径实体", description="文件路径")
public class PathEntity extends BaseEntity{

    @ApiModelProperty(value = "id")
    private String xt_path_id;/**ID**/

    @ApiModelProperty(value = "名称")
    private String xt_path_name;/**名称**/

    @ApiModelProperty(value = "路径")
    private String xt_path;/**路径**/

    @ApiModelProperty(value = "常量值唯一")
    private String xt_value;/**常量值唯一**/

    @ApiModelProperty(value = "0系统模块1业务模块")
    private String xt_type;/**0系统模块1业务模块**/
}
