package jehc.djshi.common.base;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Desc InitBean
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
public class InitBean {
    private static String jehcCloudKey;

    public static String jehcCloudSecurity;

    public String getJehcCloudKey() {
        return jehcCloudKey;
    }

    @Value("${jehc.cloud.key:''}")
    public void setJehcCloudKey(String jehcCloudKey) {
        InitBean.jehcCloudKey = jehcCloudKey;
    }

    public String getJehcCloudSecurity() {
        return jehcCloudSecurity;
    }
    @Value("${jehc.cloud.security:''}")
    public void setJehcCloudSecurity(String jehcCloudSecurity) {
        InitBean.jehcCloudSecurity = jehcCloudSecurity;
    }

}
