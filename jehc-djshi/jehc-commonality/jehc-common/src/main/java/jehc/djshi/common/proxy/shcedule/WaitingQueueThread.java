package jehc.djshi.common.proxy.shcedule;
import lombok.extern.slf4j.Slf4j;

/**
 * @Desc WaitingQueueThread
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class WaitingQueueThread  extends Thread{
    private static final int sleepTime = 2000;

    /**
     * 构造函数
     * @param threadName
     */
    public WaitingQueueThread(String threadName) {
        this.setName(threadName);
    }

    /**
     * 执行线程
     */
    public void run() {
        log.info("");
        while (true) {
            try {
            	 Thread.sleep(sleepTime);
            }
            catch (Exception e) {
                log.error("执行线程异常：{}",e);
            }
        }
    }
}
