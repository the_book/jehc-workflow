package jehc.djshi.common.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 定时任务实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="定时任务实体", description="定时任务实体")
public class ScheduleJob {

	@ApiModelProperty(value = "任务id")
	private String jobId;/**任务id*/

	@ApiModelProperty(value = "标题")
	private String jobTitle;//标题

	@ApiModelProperty(value = "任务名称")
	private String jobName;/**任务名称*/

	@ApiModelProperty(value = "任务分组")
	private String jobGroup;/**任务分组*/

	@ApiModelProperty(value = "任务状态 0禁用 1启用 2删除")
	private String jobStatus;/**任务状态 0禁用 1启用 2删除*/

	@ApiModelProperty(value = "任务运行时间表达式")
	private String cronExpression;/**任务运行时间表达式*/

	@ApiModelProperty(value = "任务描述")
	private String desc;/**任务描述*/

	@ApiModelProperty(value = "执行的类方法")
	private String clientId;/**执行的类方法*/

	@ApiModelProperty(value = "执行的类")
	private String clientGroupId;/**执行的类**/

	@ApiModelProperty(value = "注解事件")
	private String jobHandler;/**注解事件**/

	@ApiModelProperty(value = "参数")
	private String jobPara;/**参数**/

	@Override
	public String toString() {
		return "ScheduleJob{" +
				"jobId='" + jobId + '\'' +
				", jobTitle='" + jobTitle + '\'' +
				", jobName='" + jobName + '\'' +
				", jobGroup='" + jobGroup + '\'' +
				", jobStatus='" + jobStatus + '\'' +
				", cronExpression='" + cronExpression + '\'' +
				", desc='" + desc + '\'' +
				", clientId='" + clientId + '\'' +
				", clientGroupId='" + clientGroupId + '\'' +
				", jobHandler='" + jobHandler + '\'' +
				", jobPara='" + jobPara + '\'' +
				'}';
	}
}