package jehc.djshi.common.util;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.comparators.ComparableComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.*;

/**
 * @Desc 排序工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class SortUtil<E> {
	/** 
	 * 对List对象按照某个成员变量进行排序 
	 * @param list       List对象 
	 * @param sortField  排序的属性名称 
	 * @param sortMode   排序方式：ASC，DESC 任选其一 
	 */  
	public static <T> void Sort(List<T> list, final String sortField, final String sortMode) {  
	    if(list == null || list.size() < 2) {  
	        return;  
	    }  
	    Collections.sort(list, new Comparator<T>() {  
	        public int compare(T o1, T o2) {  
	            try {  
	                Class clazz = o1.getClass();  
	                Field field = clazz.getDeclaredField(sortField); //获取成员变量  
	                field.setAccessible(true); //设置成可访问状态  
	                String typeName = field.getType().getName().toLowerCase(); //转换成小写  
	  
	                Object v1 = field.get(o1); //获取field的值  
	                Object v2 = field.get(o2); //获取field的值  
	                boolean ASC_order = (sortMode == null || "ASC".equalsIgnoreCase(sortMode));  
	                //判断字段数据类型，并比较大小  
	                if(typeName.endsWith("string")) {  
	                    String value1 = v1.toString();  
	                    String value2 = v2.toString();  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("short")) {  
	                    Short value1 = Short.parseShort(v1.toString());  
	                    Short value2 = Short.parseShort(v2.toString());  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("bytesources")) {
	                    Byte value1 = Byte.parseByte(v1.toString());  
	                    Byte value2 = Byte.parseByte(v2.toString());  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("char")) {  
	                    Integer value1 = (int)(v1.toString().charAt(0));  
	                    Integer value2 = (int)(v2.toString().charAt(0));  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("int") || typeName.endsWith("integer")) {  
	                    Integer value1 = Integer.parseInt(v1.toString());  
	                    Integer value2 = Integer.parseInt(v2.toString());  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("long")) {  
	                    Long value1 = Long.parseLong(v1.toString());  
	                    Long value2 = Long.parseLong(v2.toString());  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("float")) {  
	                    Float value1 = Float.parseFloat(v1.toString());  
	                    Float value2 = Float.parseFloat(v2.toString());  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("double")) {  
	                    Double value1 = Double.parseDouble(v1.toString());  
	                    Double value2 = Double.parseDouble(v2.toString());  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("boolean")) {  
	                    Boolean value1 = Boolean.parseBoolean(v1.toString());  
	                    Boolean value2 = Boolean.parseBoolean(v2.toString());  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("date")) {  
	                    Date value1 = (Date)(v1);  
	                    Date value2 = (Date)(v2);  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else if(typeName.endsWith("timestamp")) {  
	                    Timestamp value1 = (Timestamp)(v1);  
	                    Timestamp value2 = (Timestamp)(v2);  
	                    return ASC_order ? value1.compareTo(value2) : value2.compareTo(value1);  
	                }  
	                else {  
	                    //调用对象的compareTo()方法比较大小  
	                    Method method = field.getType().getDeclaredMethod("compareTo", new Class[]{field.getType()});  
	                    method.setAccessible(true); //设置可访问权限  
	                    int result  = (Integer)method.invoke(v1, new Object[]{v2});  
	                    return ASC_order ? result : result*(-1);  
	                }  
	            }  
	            catch (Exception e) {  
	                String err = e.getLocalizedMessage();  
	                System.out.println(err);  
	                e.printStackTrace();  
	            }  
	            return 0; //未知类型，无法比较大小  
	        }  
	    }); 
	}



	/**
	 * 对list进行排序
	 * @param sortList 需要排序的list
	 * @param param1   排序的参数名称
	 * @param orderType 排序类型：正序-asc；倒序-desc
	 */
	public static List sort(List sortList, String param1, String orderType){
		Comparator mycmp1 = ComparableComparator.getInstance ();
		if("desc".equals(orderType)){
			mycmp1 = ComparatorUtils. reversedComparator(mycmp1); //逆序（默认为正序）
		}

		ArrayList<Object> sortFields = new ArrayList<Object>();
		sortFields.add( new BeanComparator(param1 , mycmp1)); //主排序（第一排序）

		ComparatorChain multiSort = new ComparatorChain(sortFields);
		Collections.sort (sortList , multiSort);

		return sortList;
	}

	/**
	 * 对list进行排序
	 * @param sortList 需要排序的list
	 * @param param1   排序的参数名称:参数长度
	 * @param param2   排序的参数名称:排序参数
	 * @param orderType 排序类型：正序-asc；倒序-desc
	 */
	public static List sortParam2(List sortList, String param1,String param2, String orderType){
		Comparator mycmp1 = ComparableComparator.getInstance ();
		Comparator mycmp2 = ComparableComparator.getInstance ();
		if("desc".equals(orderType)){
			mycmp1 = ComparatorUtils. reversedComparator(mycmp1); //逆序（默认为正序）
		}

		ArrayList<Object> sortFields = new ArrayList<Object>();
		sortFields.add( new BeanComparator(param1 , mycmp1)); //主排序（第一排序）
		sortFields.add( new BeanComparator(param2 , mycmp2)); //主排序（第一排序）

		ComparatorChain multiSort = new ComparatorChain(sortFields);
		Collections.sort (sortList , multiSort);

		return sortList;
	}

	public static List testMapSort(){
		List sortList = new ArrayList();

		Map map = new HashMap();
		map.put("name", "1");
		map.put("age", "1");

		Map map2 = new HashMap();
		map2.put("name", "2");
		map2.put("age", "13");

		Map map1 = new HashMap();
		map1.put("name", "2");
		map1.put("age", "12");

		List list = new ArrayList();
		list.add(map);
		list.add(map1);
		list.add(map2);

		//return sort(list, "age", "asc");
		return sortParam2(list, "name", "age", "asc");
	}
}

