package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc 修改个人密码参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="修改个人密码参数实体", description="修改个人密码参数")
public class UpdatePasswordEntity {

    @ApiModelProperty(value = "账户id")
    private String accountId;

    @ApiModelProperty(value = "原密码")
    private String oldPwd;//原密码

    @ApiModelProperty(value = "新密码")
    private String newPwd;//新密码
}
