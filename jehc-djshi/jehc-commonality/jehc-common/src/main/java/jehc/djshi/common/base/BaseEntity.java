package jehc.djshi.common.base;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.util.date.DateUtil;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
/**
 * @Desc Entity支持类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="BaseEntity支持类对象", description="实体基类")
public class BaseEntity extends DateUtil{

	@ApiModelProperty(value = "item")
	private Object item;

	@ApiModelProperty(value = "乐观锁使用")
	private Integer version;/**乐观锁使用**/

	@ApiModelProperty(value = "操作人名称")
	private String xt_userinfo_realName;/**操作人名称**/

	@ApiModelProperty(value = "用户邮箱地址")
	private String xt_userinfo_email;/**邮件**/

	@ApiModelProperty(value = "图片工程资源统一URL")
	private String jehcimg_base_url;/**图片工程资源统一URL**/

	@ApiModelProperty(value = "图片路径统一URL+路径（完整路径）")
	private String jehcimg_base_path_url;/**图片路径统一URL+路径（完整路径）**/

	@ApiModelProperty(value = "平台默认资源统一URL")
	private String jehcsources_base_url;/**平台默认资源统一URL**/

	@ApiModelProperty(value = "平台默认资源路径统一URL+路径（完整路径）")
	private String jehcsources_base_path_url;/**平台默认资源路径统一URL+路径（完整路径）**/

	@ApiModelProperty(value = "文件相对路径")
	private String xt_attachmentPath;/**文件相对路径**/

	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/

	@ApiModelProperty(value = "修改时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/

	@ApiModelProperty(value = "创建人名称")
	private String createBy;/**创建人名称**/

	@ApiModelProperty(value = "修改者名称")
	private String modifiedBy;/**修改者名称**/

	@ApiModelProperty(value = "创建人id")
	private String create_id;/**创建人id**/

	@ApiModelProperty(value = "修改人id")
	private String update_id;/**修改人id**/

	@Deprecated
	@ApiModelProperty(value = "删除标记：0正常1已删除（废弃）")
	private Integer delFlag;/**删除标记：0正常1已删除（废弃）**/

	@ApiModelProperty(value = "删除标记：0正常1已删除")
	private Integer del_flag;/**删除标记：0正常1已删除**/

	@ApiModelProperty(value = "sessionId")
	private String sessionId;

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
