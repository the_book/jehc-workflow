package jehc.djshi.common.util;

import java.util.*;
import com.alibaba.fastjson.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
/**
 * @Desc JSON转换工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class JsonUtil {
    /**
     * Convert objects to JSON strings
     * @param obj Object to be converted
     * @return The string character of the object
     */
    public static String toJson(Object obj) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
        JSONObject jSONObject = JSONObject.fromObject(obj,jsonConfig);
        return jSONObject.toString();
    }

    /**
     * Convert objects to JSON strings
     * @param obj Object to be converted
     * @return The string character of the object
     */
    public static JSONObject toJsonObj(Object obj) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
        JSONObject jSONObject = JSONObject.fromObject(obj,jsonConfig);
        return jSONObject;
    }

    /**
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> toJsonList(String json, Object clazz) {
        if(StringUtil.isEmpty(json)){
            return null;
        }
        JsonConfig jsonCfg = new JsonConfig();
        //register date
        jsonCfg.registerJsonValueProcessor(java.util.Date.class, new JsonDateValueProcessor());
        JSONArray jsonArray = JSONArray.fromObject(json.toString());
        List<T> tList = JSONArray.toList(jsonArray,clazz ,jsonCfg);
        return tList;
    }


    /**
     * Convert a JSONArray object to a list collection
     * @param jsonArr
     * @return
     */
    public static List<Object> jsonToList(JSONArray jsonArr) {
        List<Object> list = new ArrayList<Object>();
        for (Object obj : jsonArr) {
            if (obj instanceof JSONArray) {
                list.add(jsonToList((JSONArray) obj));
            } else if (obj instanceof JSONObject) {
                list.add(jsonToMap((JSONObject) obj));
            } else {
                list.add(obj);
            }
        }
        return list;
    }

    /**
     * Convert a json string to a map object
     * @param json
     * @return
     */
    public static Map<String, Object> jsonToMap(String json) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
        JSONObject jsonObject = JSONObject.fromObject(json,jsonConfig);
        return jsonToMap(jsonObject);
    }

    /**
     * Convert JSONObject to map object
     * @param obj
     * @return
     */
    public static Map<String, Object> jsonToMap(JSONObject obj) {
        Set<?> set = obj.keySet();
        Map<String, Object> map = new HashMap<String, Object>(set.size());
        for (Object key : obj.keySet()) {
            Object value = obj.get(key);
            if (value instanceof JSONArray) {
                map.put(key.toString(), jsonToList((JSONArray) value));
            } else if (value instanceof JSONObject) {
                map.put(key.toString(), jsonToMap((JSONObject) value));
            } else {
                map.put(key.toString(), obj.get(key));
            }
        }
        return map;
    }

    /**
     *
     * @param object
     * @return
     */
    public static String toFastJson(Object object){
        String json = com.alibaba.fastjson.JSONObject.toJSONString(object);
        return json;
    }

    /**
     *
     * @param object
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> toFastList(Object object, Class<T> clazz) {
        if(null == object){
            return null;
        }
        String json = toFastJson(object);
        if(StringUtil.isEmpty(json)){
            return null;
        }
        return JSON.parseArray(json,clazz);
    }

    /**
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> toFList(String json, Class<T> clazz) {
        if(StringUtil.isEmpty(json)){
            return null;
        }
        return JSON.parseArray(json,clazz);
    }

    /**
     *
     * @param object
     * @param type
     * @param <T>
     * @return
     */
    public static <T> T fromFastJson(Object object, Class<T> type) {
        if(null == object){
            return null;
        }
        String json = toFastJson(object);
        if(StringUtil.isEmpty(json)){
            return null;
        }
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
        JSONObject jsonObject = JSONObject.fromObject(json,jsonConfig);
        return (T) JSONObject.toBean(jsonObject, type);
    }

    /**
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T fromAliFastJson(String json, Class<T> clazz) {
        if(null == json){
            return null;
        }
        return com.alibaba.fastjson.JSONObject.parseObject(json,clazz);
    }

    /**
     *
     * @param json
     * @param type
     * @param <T>
     * @return
     */
    public static <T> T fromFJson(String json, Class<T> type) {
        if(StringUtil.isEmpty(json)){
            return null;
        }
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,new JsonDateValueProcessor());
        JSONObject jsonObject = JSONObject.fromObject(json,jsonConfig);
        return (T) JSONObject.toBean(jsonObject, type);
    }
}
