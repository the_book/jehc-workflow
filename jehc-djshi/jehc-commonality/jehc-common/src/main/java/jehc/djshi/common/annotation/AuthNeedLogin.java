package jehc.djshi.common.annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
/**
 * @Desc 需要登录认证注解
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Target(value = {java.lang.annotation.ElementType.METHOD})
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface AuthNeedLogin {
	String desc = "需要登录认证";
}
