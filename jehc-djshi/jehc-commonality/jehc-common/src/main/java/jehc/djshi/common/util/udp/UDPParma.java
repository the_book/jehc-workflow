package jehc.djshi.common.util.udp;

import lombok.Data;

/**
 * @Desc UDP参数
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class UDPParma {
    String host;//目标地址
    int port;//目标端口
    String message;//发送信息
    String charset = "GBK";//发送或接受字符编码 （如GBK或UTF-8或ASCII）
    Integer localPort;//绑定本地指定端口（可自定义绑定接收端口 可根据服务端要求指定端口接收）
    Integer soTimeout;//超时时间
    Boolean receive;//是否采用接收
    Boolean useLocalPort = false;//本地端口是否采用随机端口（即随机优先级最高），如果true则不指定本地绑定端口
}
