package jehc.djshi.common.cache.redis;

import java.util.*;
import java.util.concurrent.TimeUnit;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.constant.CacheConstant;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.common.util.logger.Logback4jUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
/**
 * @Desc redis工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Lazy
@Slf4j
public class RedisUtil extends Logback4jUtil {

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	StringRedisTemplate stringRedisTemplate;

	/**
	 * 指定缓存失效时间
	 * @param key 键
	 * @param time 时间(秒)
	 * @return
	 */
	public boolean expire(String key,long time){
		try {
			if(time>0){
				redisTemplate.expire(key, time, TimeUnit.SECONDS);
			}
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行expire方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 根据key 获取过期时间
	 * @param key 键 不能为null
	 * @return 时间(秒) 返回0代表为永久有效
	 */
	public long getExpire(String key){
		return redisTemplate.getExpire(key, TimeUnit.SECONDS);
	}

	/**
	 * 判断key是否存在
	 * @param key 键
	 * @return true 存在 false不存在
	 */
	public boolean hasKey(String key){
		try {
			return redisTemplate.hasKey(key);
		} catch (Exception e) {
			error("RedisUtil","执行hasKey方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 删除指定的key,也可以传入一个包含key的数组
	 * @param key
	 * @return 返回删除成功的个数
	 */
	public void del(String ... key){
		if(key!=null&&key.length>0){
			if(key.length==1){
				redisTemplate.delete(key[0]);
			}else{
				redisTemplate.delete(CollectionUtils.arrayToList(key));
			}
		}
	}

	/////////////////////////////////////String开始///////////////////////////////////
	/**
	 * String 通过key获取储存在redis中的value 并释放连接
	 * @param key
	 * @return 成功返回value 失败返回null
	 */
	public String get(String key) {
		Object object = redisTemplate.opsForValue().get(key);
		if(null == object){
			return null;
		}
		return ""+object;
	}

	/**
	 * string 向redis存入key和value,并释放连接资源 如果key已经存在 则覆盖
	 * @param key
	 * @param value
	 * @return 成功 返回OK 失败返回 0
	 */
	public boolean set(String key, String value) {
		try {
			redisTemplate.opsForValue().set(key,value);
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行set方法,异常信息："+e.getMessage());
			return false;
		}

	}
	/**
	 * 普通缓存放入并设置时间
	 * @param key 键
	 * @param value 值
	 * @param time 时间(秒) time要大于0 如果time小于等于0 将设置无限期
	 * @return true成功 false 失败
	 */
	public boolean set(String key,Object value,long time){
		try {
			if(time>0){
				redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
			}else{
				redisTemplate.opsForValue().set(key, value);
			}
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行set方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 递增
	 * @param key 键
	 * @param delta 要增加几(大于0)
	 * @return
	 */
	public long incr(String key, long delta){
		if(delta<0){
			throw new RuntimeException("递增因子必须大于0");
		}
		return redisTemplate.opsForValue().increment(key, delta);
	}

	/**
	 * 递减
	 * @param key 键
	 * @param delta 要减少几(小于0)
	 * @return
	 */
	public long decr(String key, long delta){
		if(delta<0){
			throw new RuntimeException("递减因子必须大于0");
		}
		return redisTemplate.opsForValue().increment(key, -delta);
	}
	/////////////////////////////////////String结束///////////////////////////////////

	/////////////////////////////////////HASH集合开始///////////////////////////////////
	/**
	 * hash 通过key给field设置指定的值,如果key不存在,则先创建 ,存在会覆盖原来的值
	 * @param key
	 * @param field 字段
	 * @param value
	 * @return 如果不存在，新建的返回1，存在返回0, 异常返回null
	 * 
	 */
	public boolean hset(String key, String field, String value) {
		try {
			redisTemplate.opsForHash().put(key, field, value);
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行hset方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 向一张hash表中放入数据,如果不存在将创建
	 * @param key 大key
	 * @param field 字段（小key）
	 * @param value 值
	 * @param time 时间(秒)  注意:如果已存在的hash表有时间,这里将会替换原有的时间
	 * @return true 成功 false失败
	 */
	public boolean hset(String key,String field,Object value,long time) {
		try {
			redisTemplate.opsForHash().put(key, field, value);
			if(time>0){
				expire(key, time);
			}
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行hset方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 删除hash表中的值
	 * @param key 大key 不能为null
	 * @param field 字段（小key） 可以使多个 不能为null
	 */
	public boolean hdel(String key, Object... field){
		try {
			redisTemplate.opsForHash().delete(key,field);
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行hdel方法,异常信息："+e.getMessage());
			return false;
		}

	}

	/**
	 * 判断hash表中是否有该项的值
	 * @param key 大key 不能为null
	 * @param field 字段（小key） 可以使多个 不能为null
	 * @return true 存在 false不存在
	 */
	public boolean hHasKey(String key, String field){
		return redisTemplate.opsForHash().hasKey(key, field);
	}

	/**
	 * hash递增 如果不存在,就会创建一个 并把新增后的值返回
	 * @param key 大key 不能为null
	 * @param field 字段（小key） 可以使多个 不能为null
	 * @param by 要增加几(大于0)
	 * @return
	 */
	public double hincr(String key, String field,double by){
		return redisTemplate.opsForHash().increment(key, field, by);
	}

	/**
	 * hash递减
	 * @param key 大key
	 * @param field 字段（小key）
	 * @param by 要减少记(小于0)
	 * @return
	 */
	public double hdecr(String key, String field,double by) {
		return redisTemplate.opsForHash().increment(key, field, -by);
	}

	/**
	 * HashSet
	 * @param key 键
	 * @param map 对应多个键值
	 * @return true 成功 false 失败
	 */
	public boolean hmset(String key, Map<String,Object> map){
		try {
			redisTemplate.opsForHash().putAll(key, map);
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行hmset方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 通过key 和 field 获取指定的 value
	 * @param key 大key
	 * @param field 小key
	 * @return 没有返回null
	 */
	public Object hget(String key, String field) {
		try {
			return redisTemplate.opsForHash().get(key, field);
		} catch (Exception e) {
			error("RedisUtil","执行hget方法,异常信息："+e.getMessage());
			return null;
		}
	}

	/**
	 * 获取hashKey对应的所有键值
	 * @param key 键
	 * @return 对应的多个键值
	 */
	public Map<Object,Object> hmget(String key){
		try {
			return redisTemplate.opsForHash().entries(key);
		} catch (Exception e) {
			error("RedisUtil","执行hmget方法,异常信息："+e.getMessage());
			return null;
		}
	}

	/**
	 * 获取hash表中所有key
	 * @param hashName
	 * @return
	 */
	public Set<String> hKeys(String hashName){
		try {
			if(StringUtil.isEmpty(hashName)){
				error("RedisUtil","getHashAllKey,未能获取到hashName值");
				throw new ExceptionUtil("getHashAllKey,未能获取到hashName值");
			}
			return redisTemplate.opsForHash().keys(hashName);
		} catch (Exception e) {
			error("RedisUtil","HttpSessionUtils类getHashAllKey方法,Redis Session 读取出现异常"+e.getMessage());
			throw new ExceptionUtil("getAttribute类getHashAllKey方法，获取Redis出现异常",e.getCause());
		}
	}
	/////////////////////////////////////HASH集合结束///////////////////////////////////

	/////////////////////////////////////SET集合开始///////////////////////////////////
	/**
	 * 根据key获取Set中的所有值
	 * @param key 键
	 * @return
	 */
	public Set<Object> sGet(String key){
		try {
			return redisTemplate.opsForSet().members(key);
		} catch (Exception e) {
			error("RedisUtil","执行sGet方法,异常信息："+e.getMessage());
			return null;
		}
	}

	/**
	 * 根据value从一个set中查询,是否存在
	 * @param key 键
	 * @param value 值
	 * @return true 存在 false不存在
	 */
	public boolean sHasKey(String key,Object value){
		try {
			return redisTemplate.opsForSet().isMember(key, value);
		} catch (Exception e) {
			error("RedisUtil","执行sHasKey方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 将数据放入set缓存
	 * @param key 键
	 * @param values 值 可以是多个
	 * @return 成功个数
	 */
	public long sSet(String key, Object...values) {
		try {
			return redisTemplate.opsForSet().add(key, values);
		} catch (Exception e) {
			error("RedisUtil","执行sSet方法,异常信息："+e.getMessage());
			return 0;
		}
	}

	/**
	 * 将set数据放入缓存
	 * @param key 键
	 * @param time 时间(秒)
	 * @param values 值 可以是多个
	 * @return 成功个数
	 */
	public long sSetAndTime(String key,long time,Object...values) {
		try {
			Long count = redisTemplate.opsForSet().add(key, values);
			if(time>0) expire(key, time);
			return count;
		} catch (Exception e) {
			error("RedisUtil","执行sSetAndTime方法,异常信息："+e.getMessage());
			return 0;
		}
	}

	/**
	 * 获取set缓存的长度
	 * @param key 键
	 * @return
	 */
	public long sGetSetSize(String key){
		try {
			return redisTemplate.opsForSet().size(key);
		} catch (Exception e) {
			error("RedisUtil","执行sGetSetSize方法,异常信息："+e.getMessage());
			return 0;
		}
	}

	/**
	 * 移除值为value的
	 * @param key 键
	 * @param values 值 可以是多个
	 * @return 移除的个数
	 */
	public long setRemove(String key, Object ...values) {
		try {
			Long count = redisTemplate.opsForSet().remove(key, values);
			return count;
		} catch (Exception e) {
			error("RedisUtil","执行setRemove方法,异常信息："+e.getMessage());
			return 0;
		}
	}
	/////////////////////////////////////SET集合结束///////////////////////////////////

	/////////////////////////////////////List集合开始///////////////////////////////////
	/**
	 * 获取list缓存的内容
	 * @param key 键
	 * @param start 开始
	 * @param end 结束  0 到 -1代表所有值
	 * @return
	 */
	public List<Object> lGet(String key,long start, long end){
		try {
			return redisTemplate.opsForList().range(key, start, end);
		} catch (Exception e) {
			error("RedisUtil","执行lGet方法,异常信息："+e.getMessage());
			return null;
		}
	}

	/**
	 * 获取list缓存的长度
	 * @param key 键
	 * @return
	 */
	public long lGetListSize(String key){
		try {
			return redisTemplate.opsForList().size(key);
		} catch (Exception e) {
			error("RedisUtil","执行lGetListSize方法,异常信息："+e.getMessage());
			return 0;
		}
	}

	/**
	 * 通过索引 获取list中的值
	 * @param key 键
	 * @param index 索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
	 * @return
	 */
	public Object lGetIndex(String key,long index){
		try {
			return redisTemplate.opsForList().index(key, index);
		} catch (Exception e) {
			error("RedisUtil","执行lGetIndex方法,异常信息："+e.getMessage());
			return null;
		}
	}

	/**
	 * 将list放入缓存
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public boolean lSet(String key, Object value) {
		try {
			redisTemplate.opsForList().rightPush(key, value);
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行lSet方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 将list放入缓存
	 * @param key 键
	 * @param value 值
	 * @param time 时间(秒)
	 * @return
	 */
	public boolean lSet(String key, Object value, long time) {
		try {
			redisTemplate.opsForList().rightPush(key, value);
			if(time > 0) expire(key, time);
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行lSet方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 将list放入缓存
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public boolean lSet(String key, List<Object> value) {
		try {
			redisTemplate.opsForList().rightPushAll(key, value);
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行lSet方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 将list放入缓存
	 * @param key 键
	 * @param value 值
	 * @param time 时间(秒)
	 * @return
	 */
	public boolean lSet(String key, List<Object> value, long time) {
		try {
			redisTemplate.opsForList().rightPushAll(key, value);
			if (time > 0) expire(key, time);
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行lSet方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 根据索引修改list中的某条数据
	 * @param key 键
	 * @param index 索引
	 * @param value 值
	 * @return
	 */
	public boolean lUpdateIndex(String key, long index,Object value) {
		try {
			redisTemplate.opsForList().set(key, index, value);
			return true;
		} catch (Exception e) {
			error("RedisUtil","执行lUpdateIndex方法,异常信息："+e.getMessage());
			return false;
		}
	}

	/**
	 * 移除N个值为value
	 * @param key 键
	 * @param count 移除多少个
	 * @param value 值
	 * @return 移除的个数
	 */
	public long lRemove(String key,long count,Object value) {
		try {
			Long remove = redisTemplate.opsForList().remove(key, count, value);
			return remove;
		} catch (Exception e) {
			error("RedisUtil","执行lRemove方法,异常信息："+e.getMessage());
			return 0;
		}
	}
	/////////////////////////////////////List集合结束///////////////////////////////////

	/**
	 * 获取 key集合
	 * @param prefix
	 * @return
	 */
	public Set<String> getKeys(String prefix){
		return redisTemplate.keys(prefix);
	}




	///////////////////////////分布式锁方式一 开始////////////////////////////
    private static ThreadLocal<Thread> THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 分布式锁(未获取锁的请求，允许丢弃)
     * @param lockEntity
     * @return
     */
    public BaseResult simpleLock(LockEntity lockEntity){
        String lockValue = lockEntity.getLockValue();
        BaseResult baseResult = new BaseResult(lockValue);
        boolean result = false;
        if (StringUtils.isEmpty(lockEntity.getLockKey())) {
           log.info("锁为空!");
           baseResult.setSuccess(false);
           return baseResult;
        }
        try {
            for (int i = 0; i < lockEntity.getRetryTimes(); i++) {
                boolean success = redisTemplate.opsForValue().setIfAbsent(lockEntity.getLockKey(), lockValue, lockEntity.getExpireLockTimeOut(), TimeUnit.SECONDS);
                if (success) {
                    result = true;
                    break;
                }
                try {
                    TimeUnit.MILLISECONDS.sleep(CacheConstant.LOCK_RETRY_INTERVAL);
                } catch (Exception ex) {
                    log.error("加锁失败,错误信息：{0}-{1}-{2}",ex,lockEntity.getLockKey(),lockValue);
                }
            }
            if (!result) {
                log.info("重试多次加锁机制都未能成功。lockKey："+lockEntity.getLockKey()+",更多详情：{0}：",lockEntity);
                baseResult.setSuccess(false);
            }
            return baseResult;
        } catch (Exception e) {
            log.info("重试多次加锁机制都未能成功。lockKey："+lockEntity.getLockKey()+",更多详情：{0}：",lockEntity);
            baseResult.setSuccess(false);
            return baseResult;
        }
    }

    /**
     * 分布式锁(未获取锁的请求，将在timeoutSecond时间范围内，一直等待重试)
     * @param lockEntity
     * @return
     */
    public BaseResult lock(LockEntity lockEntity){
        String lockValue = lockEntity.getLockValue();
        BaseResult baseResult = new BaseResult(lockValue);
        boolean result = false;
        if (StringUtils.isEmpty(lockEntity.getLockKey())) {
            log.info("锁为空!");
            baseResult.setSuccess(false);
            return baseResult;
        }
        if (lockEntity.getTimeoutSecond() >= lockEntity.getExpireLockTimeOut()) {
            log.info("重试等待时间（秒）不能超过锁的自动释放时间(秒)!");
            baseResult.setSuccess(false);
            return baseResult;
        }
        try {
            long timeoutAt = System.currentTimeMillis() + lockEntity.getTimeoutSecond() * 1000;
            while (true) {
                boolean success = redisTemplate.opsForValue().setIfAbsent(lockEntity.getLockKey(), lockValue, lockEntity.getExpireLockTimeOut(), TimeUnit.SECONDS);
                if (success) {
                    result = true;
                    //续租
                    Thread reNewLock = new Thread(new ReNewLock(lockEntity));
                    reNewLock.setDaemon(true);
                    THREAD_LOCAL.set(reNewLock);
                    reNewLock.start();
                    break;
                }
                if (System.currentTimeMillis() >= timeoutAt) {
                    break;
                }
                try {
                    TimeUnit.MILLISECONDS.sleep(CacheConstant.LOCK_RETRY_INTERVAL);
                } catch (Exception ex) {
                    log.error("加锁失败,错误信息：{0}-{1}-{2}",ex,lockEntity.getLockKey(),lockValue);
                }
            }
            if (!result) {
                baseResult.setSuccess(false);
            }
            return baseResult;
        }  catch (Exception e) {
            log.error("重试多次加锁机制都未能成功。lockKey："+lockEntity.getLockKey()+",更多详情：{0}：",lockEntity);
            baseResult.setSuccess(false);
            return baseResult;
        }
    }

    /**
     * 释放锁
     * @param lockEntity
     */
    public BaseResult releaseLock(LockEntity lockEntity) {
        BaseResult baseResult = new BaseResult(lockEntity.getLockValue());
        if (StringUtils.isEmpty(lockEntity.getLockKey())) {
            log.info("锁为空!");
            baseResult.setSuccess(false);
            return baseResult;
        }
        if (StringUtils.isEmpty(lockEntity.getLockValue())) {
            log.info("锁对应的值空!");
            baseResult.setSuccess(false);
            return baseResult;
        }
        try {
            redisTemplate.setEnableTransactionSupport(true);
            redisTemplate.watch(lockEntity.getLockKey());
            String value = ""+redisTemplate.opsForValue().get(lockEntity.getLockKey());
            if (value != null && value.equals(lockEntity.getLockValue())) {
                System.out.println(Thread.currentThread().getName() + "释放锁");
                redisTemplate.multi();
                boolean result = redisTemplate.delete(lockEntity.getLockKey());
                redisTemplate.exec();
                THREAD_LOCAL.get().interrupt();
                if (!result) {
                    log.error("当前线程："+Thread.currentThread().getName()+"解锁未能成功。lockKey："+lockEntity.getLockKey()+",更多详情：{0}：",lockEntity);
                    baseResult.setSuccess(false);
                } else {
                    log.info("当前线程："+Thread.currentThread().getName()+"解锁成功。lockKey："+lockEntity.getLockKey()+",更多详情：{0}：",lockEntity);
                }
                redisTemplate.unwatch();
            }else{
				log.error("当前线程："+Thread.currentThread().getName()+"解锁未能成功，原因是当前线程释放的锁非自身锁，lockKey："+lockEntity.getLockKey()+",更多详情：{0}：",lockEntity);
				baseResult.setSuccess(false);
			}
        } catch (Exception je) {
            log.error("当前线程："+Thread.currentThread().getName()+"解锁未能成功。lockKey："+lockEntity.getLockKey());
        }finally {
            THREAD_LOCAL.remove();
        }
        return baseResult;
    }

    /**
     * 续租
     */
    private class ReNewLock implements Runnable {
        private LockEntity lockEntity;

        public ReNewLock(LockEntity lockEntity) {
            this.lockEntity = lockEntity;
        }
        @Override
        public void run() {
            while (true) {
                try {
                    if(Thread.currentThread().isInterrupted()) {
                        return;
                    }
                    Thread.sleep(lockEntity.getTimeoutSecond()*1000 - 200);
                    redisTemplate.setEnableTransactionSupport(true);
                    redisTemplate.watch(lockEntity.getLockKey());
                    String lockValue = ""+ redisTemplate.opsForValue().get(lockEntity.getLockKey());
                    if(lockValue != null && lockValue != null && lockEntity.getLockValue().equals(lockValue)) {
                        redisTemplate.multi();
                        redisTemplate.expire(lockEntity.getLockKey(), lockEntity.getTimeoutSecond(), TimeUnit.SECONDS);
                        redisTemplate.exec();
                    }
                    redisTemplate.unwatch();
                } catch (InterruptedException e) {
                    log.error("守护线程中断，续租失败");
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
    ///////////////////////////分布式锁方式一 结束////////////////////////////


	/**
	 * 获取 有效期
	 * @param key
	 * @return
	 */
	public Long getExpire(String key,TimeUnit timeUnit){
		long ttl = redisTemplate.getExpire(key, timeUnit);
		return ttl;
	}
}
