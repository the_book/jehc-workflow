package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Desc InputEntity
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="输入实体", description="输入实体")
public class InputEntity {

    @ApiModelProperty(value = "uri")
    private String url;

    @ApiModelProperty(value = "无需登录标记")
    private String authUneedLogin;

    @ApiModelProperty(value = "需要登录但不拦截标记")
    private String needLoginUnAuth;

    @ApiModelProperty(value = "auth")
    private String auth;

    @ApiModelProperty(value = "token")
    private String token;
}
