package jehc.djshi.common.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @Desc 数据库信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="数据库信息", description="数据库信息")
public class DbinfoEntity{

    @ApiModelProperty(value = "数据库信息表")
    private String xt_dbinfo_id;/**数据库信息表**/

    @ApiModelProperty(value = "据库数名称")
    private String xt_dbinfoName;/**据库数名称**/

    @ApiModelProperty(value = "数据库用户名")
    private String xt_dbinfoUName;/**数据库用户名**/

    @ApiModelProperty(value = "据库数密码")
    private String xt_dbinfoPwd;/**据库数密码**/

    @ApiModelProperty(value = "备份IP")
    private String xt_dbinfoIp;/**备份IP**/

    @ApiModelProperty(value = "端口号")
    private String xt_dbinfoPort;/**端口号**/

    @ApiModelProperty(value = "数据库类型")
    private String xt_dbinfoType;/**数据库类型**/
}