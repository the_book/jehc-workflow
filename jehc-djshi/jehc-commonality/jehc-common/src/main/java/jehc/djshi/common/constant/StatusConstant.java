package jehc.djshi.common.constant;

/**
 * @Desc 状态常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class StatusConstant {
	public static final String XT_PT_ERROR_MSG = "xt_pt_error_msg";//错误信息
	public static final String EX = "ex";
	public static final String XT_PT_STATUS= "xt_pt_status";//xt_pt_status
	public static final int XT_PT_STATUS_VAL_001= 001;//非法页面
	public static final int XT_PT_STATUS_VAL_777= 777;//非法权限
	public static final int XT_PT_STATUS_VAL_888= 888;//全局会话失效
	public static final int XT_PT_STATUS_VAL_999= 999;//授权平台秘钥非法
	public static final int XT_PT_STATUS_VAL_500 = 500;//异常
	public static final int XT_PT_STATUS_VAL_200 = 200;//200 code

	public static final int XT_PT_STATUS_VAL_1000= 1000;//保险丝 code

	public static final int CUS_PT_STATUS_VAL_2000 = 2000;//2000 code 自定义异常

	public static final String EXPORTORDOWNLOADSYSFLAG="exportOrDownloadSysFlag";//是否导出或下载标识
	public static final String BDOWNFLAG="bdownflag";//jquery导出 或下载标识
	public static final String BDOWNFLAG_TEXT="<script type='text/javascript'>top.window.parent.toastrBoot(4,'没有操作权限，请联系管理员');</script>";
}
