package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * @Desc UserParamInfo
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="员工查询参数实体", description="员工查询参数实体")
public class UserParamInfo {

    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "账户id")
    private String account_id;

    @ApiModelProperty(value = "用户信息")
    private List<UserinfoEntity> userinfoEntities;

    public UserParamInfo(){

    }

    public UserParamInfo(String account_id){
        this.account_id = account_id;
    }
}