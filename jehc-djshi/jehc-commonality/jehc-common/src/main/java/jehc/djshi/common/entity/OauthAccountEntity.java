package jehc.djshi.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @Desc 账户信息实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="账户信息实体", description="账户信息实体")
public class OauthAccountEntity extends BaseEntity {

    @ApiModelProperty(value = "账号ID与UserID一致")
    private String account_id;//账号ID与UserID一致

    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "登录名")
    private String name;//登录名

    @ApiModelProperty(value = "密码")
    private String password;//密码

    @ApiModelProperty(value = "账号类型0平台用户1商户2会员3临时")
    private String type;//账号类型0平台用户1商户2会员3临时

    @ApiModelProperty(value = "状态0正常1锁定2禁用")
    private int status;//状态0正常1锁定2禁用

    @ApiModelProperty(value = "电子邮件")
    private String email;//电子邮件

    @ApiModelProperty(value = "最后一次登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date last_login_time;//最后一次登录时间

    @ApiModelProperty(value = "对象信息如：会员的对象json格式存储")
    private String info_body;//对象信息如：会员的对象json格式存储

    @ApiModelProperty(value = "是否为管理员")
    private Boolean isAdmin;

    public OauthAccountEntity(){

    }
}
