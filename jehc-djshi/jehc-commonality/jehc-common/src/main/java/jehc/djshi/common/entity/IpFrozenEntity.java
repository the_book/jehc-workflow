package jehc.djshi.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc IP白名单实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@ApiModel(value="IP白名单实体", description="IP白名单实体")
public class IpFrozenEntity  extends BaseEntity {

    @ApiModelProperty(value = "IP冻结编号")
    private String xt_ip_frozen_id;/**IP冻结编号**/

    @ApiModelProperty(value = "IP地址")
    private String xt_ip_frozen_address;/**IP地址**/

    @ApiModelProperty(value = "状态0正常1冻结2黑名单")
    private Integer xt_ip_frozen_status;/**状态0正常1冻结2黑名单**/

    @ApiModelProperty(value = "内容")
    private String xt_ip_frozen_content;/**内容**/

    @ApiModelProperty(value = "创建时间")
    private String xt_ip_frozen_ctime;/**创建时间**/

    @ApiModelProperty(value = "修改时间")
    private String xt_ip_frozen_mtime;/**修改时间**/

    @ApiModelProperty(value = "创建人")
    private String xt_userinfo_id;/**创建人**/
}
