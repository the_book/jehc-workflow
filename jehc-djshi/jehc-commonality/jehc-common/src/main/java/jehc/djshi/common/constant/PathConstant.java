package jehc.djshi.common.constant;

/**
 * @Desc 路径常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class PathConstant {

	public static final String JDBC_PROPERTIES_PATH = "classpath:config/jdbc.properties";//jdbc配置路径
	
	public static final String ZN_PROPERTIES_PATH = "/config/properties/zh.properties";//国际化中文
	
	public static final String MESSAGE_PROPERTIES_PATH = "/config/properties/message.properties";//消息资源文件
	
	public static final String CONFIG_PROPERTIES_PATH = "/config/properties/config.properties";//配置文件
	/*
	public static final String BASE_SPRING_PATH = "classpath*:/config/spring/spring.xml";//spring配置文件路径

	public static final String BASE_SPRING_MVC_PATH = "classpath*:/config/spring/springmvc.xml";//spring配置文件路径

	public static final String BASE_REDIS_PATH ="classpath*:/config/redis/redis.properties";//redis配置文件路径

	public static final String LOGBACK_PATH = "classpath:logback.xml";//logback路径
	*/
	
	public static final String EHCACHE_PATH = "/config/ehcache/ehcache.xml";//ehcache缓存路径
	
	public static final String REQUEST_ERROR = "error";

	public static final String SYS_SOURCES_DEFAULT_PATH = "xt_sources_default_path";//平台静态资源存放绝对路径位置（上传文件使用）

	public static final String SYS_SOURCES_DEFAULT_RELATIVE_PATH = "xt_sources_default_relative_path";//平台服务，相对路径 如http://www.jehc.com/file/jehc.png 中的“/file”

	public static final String SYS_SOURCES_BASE_URL = "jehcsources_base_url";//平台文件服务网址，默认域名url 如http://www.jehc.com

	public static final String SYS_SOURCES_UPLOAD_SUCCESS = "jehc_upload_sucess";//平台文件上传后成功提示语

	public static final String SYS_SOURCES_UPLOAD_FAIL = "jehc_upload_fail";//平台文件上传后失败提示语
}
