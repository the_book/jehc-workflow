//集成基础地址API（总控API）
let baseApi = "";

//授权中心API
let oauthModules = baseApi;

//监控中心API
let monitorModules = baseApi;

//运维中心API
let opModules = baseApi;

//平台中心API
let sysModules = baseApi;

//工作流中心API
let workflowModules = baseApi;

//文件中心API
let fileModules = baseApi;

//日志中心API
let logModules = baseApi;

//即时通讯中心API
let imModules = baseApi;

//即时通讯中心WebSocket
let imWebSocketModules = baseApi;

//报表中心API
let reportModules = baseApi;

//调度中心API
let jobModules = baseApi;

//病历云API
let medicalModules = baseApi;

//运管平台API
let ompModules = baseApi;

//////////////HTML相关基础路径///////////////
let basePath = "";

let loginPath = "";

let base_html_redirect = "";

//接收传递参数
window.addEventListener('message', function (messageEvent) { 
    let data = messageEvent.data; 
    baseApi = data.baseApi;
    //授权中心API
    oauthModules = data.oauthModules;

    //监控中心API
    monitorModules = data.monitorModules;

    //运维中心API
    opModules = data.opModules;

    //平台中心API
    sysModules = data.sysModules;

    //工作流中心API
    workflowModules = data.workflowModules;

    //文件中心API
    fileModules = data.fileModules;

    //日志中心API
    logModules = data.logModules;

    //即时通讯中心API
    imModules = data.imModules;

    //即时通讯中心WebSocket
    imWebSocketModules = data.imWebSocketModules;

    //报表中心API
    reportModules = data.reportModules;

    //调度中心API
    jobModules = data.jobModules;

    //病历云API
    medicalModules = data.medicalModules;

    //运管平台API
    ompModules = data.ompModules;

    basePath = data.basePath;
    loginPath = basePath+"/login";
    console.log("-----------",data,baseApi);
}, false);
//////////////服务端API相关API///////////////
