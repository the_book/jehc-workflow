var poolForm;

/**
 * 泳道池
 * @param cell
 * @param graph_refresh
 * @private
 */
function poolWin_(cell,graph_refresh){
    poolPanel(cell,graph_refresh);
}

/**
 *
 * @param cell
 * @returns {string|*}
 */
function createPoolNodeAttributeForm(cell){
    poolForm =
        "<div class=\"m-portlet\" id='mportletId'  style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//流程编号
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >流程编号</label>"+
						"</div>"+
						"<div class=\"col-md-3\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" readonly  onchange='setInputValue(this,\"Pool\")' data-bv-notempty data-bv-notempty-message=\"请输入流程编号\" id=\"processId_\" name=\"processId_\" placeholder=\"请输入流程编号\">"+
						"</div>"+
					"</div>"+

					//流程名称
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >流程名称</label>"+
						"</div>"+
						"<div class=\"col-md-3\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  onchange='setInputValue(this,\"Pool\")' data-bv-notempty data-bv-notempty-message=\"请输入流程名称\" id=\"processName_\" name=\"processName_\" placeholder=\"请输入流程名称\">"+
						"</div>"+
					"</div>"+

					//命名空间
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >命名空间</label>"+
						"</div>"+
						"<div class=\"col-md-3\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" readonly  onchange='setInputValue(this,\"Pool\")' data-bv-notempty data-bv-notempty-message=\"请输入命名空间\" value='http://www.activiti.org/test' id=\"poolnameSpace\" name=\"poolnameSpace\" placeholder=\"请输入命名空间\">"+
						"</div>"+
					"</div>"+

					//发起人
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >发&nbsp;起&nbsp;&nbsp;人</label>"+
						"</div>"+
						"<div class=\"col-md-3\">"+
							"<input class=\"form-control\" type=\"hidden\" maxlength=\"50\"  onchange='setInputValue(this,\"Pool\")' data-bv-notempty data-bv-notempty-message=\"请选择\" id=\"candidateStarterUsers_\" name=\"candidateStarterUsers_\" placeholder=\"请选择发起人\">"+
                             "<div class=\"input-group\"><input type=\"text\" readonly class=\"form-control m-input\" placeholder=\"请选择\"  onchange='setInputValue(this,\"Pool\")' id='candidateStarterUsers_Text_' name='candidateStarterUsers_Text_' aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetAssignee(3,1)\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" onclick='initassignee(1,3)'><i class=\"flaticon-user-ok\"></i></span></div></div>"+
                        "</div>"+
					"</div>"+

                    //组类型
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >组&nbsp;&nbsp;类&nbsp;型</label>"+
                        "</div>"+
                        "<div class=\"col-md-1\">"+
                            "<select id='candidate_group_type' onchange=\"canditateGroupChange(this,2)\" class='form-control' name='candidate_group_type'><option value=''>请选择</option><option value='0'>部门</option><option value='1'>岗位</option><option value='2'>角色</option></select>"+
                        "</div>"+
                        "<div class=\"col-md-4\">"+
                            "<div class='alert m-alert m-alert--default' role='alert'>注意：<code>组类型仅限于部门，岗位，角色</code> .</div>"+
                        "</div>"+
                    "</div>"+

					//发起人组
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >发起人组</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"hidden\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请选择\"  onchange='setInputValue(this,\"Pool\")' id=\"candidateStarterGroups_\" name=\"candidateStarterGroups_\" placeholder=\"请选择发起人组\">"+
                            "<div class=\"input-group\"><input type=\"text\" readonly class=\"form-control m-input\" placeholder=\"请选择\"  onchange='setInputValue(this,\"Pool\")' id=\"candidateStarterGroups_Text_\" name=\"candidateStarterGroups_Text_\" aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetAssignee(3,2)\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" onclick='initcandidateGroups(2,3)'><i class=\"fa flaticon-users-1\"></i></span></div></div>"+

                        "</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return poolForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initPoolData(cell){
    var processId_ = cell.processId_;
    var processName_ = cell.processName_;
    var poolnameSpace = cell.poolnameSpace;
    var candidateStarterUsers_ = cell.candidateStarterUsers_;
    var candidateStarterGroups_ = cell.candidateStarterGroups_;
    var candidate_group_type = cell.candidate_group_type;
    $('#processId_').val(processId_);
    $('#processName_').val(processName_);
    if(null != poolnameSpace && '' != poolnameSpace){
        $('#poolnameSpace').val(poolnameSpace);
    }else{
        $('#poolnameSpace').val('http://www.activiti.org/test');
    }
    $('#candidateStarterUsers_').val(candidateStarterUsers_);
    $('#candidateStarterGroups_').val(candidateStarterGroups_);
    $('#candidate_group_type').val(candidate_group_type);
    initACC(candidateStarterUsers_,null,candidateStarterGroups_,3);
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function poolPanel(cell,graph_refresh){
    poolForm = createPoolNodeAttributeForm(cell);
    nodeNormalForm = createNodeNormalForm(cell,2,"Pool");
    event_grid = creatEventGrid(cell,1);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">流&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;程</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">监听事件</a>"+

				// "<a href='javascript:setPoolValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+poolForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();

    //基本属性
    initPoolData(cell);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,2);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
    nodeScroll();
}


/**
 * 设置内容
 */
function setPoolValue(){
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        var processId_ = $('#processId_').val();
        var processName_ = $('#processName_').val();
        var poolnameSpace = $('#poolnameSpace').val();
        var candidateStarterUsers_ = $('#candidateStarterUsers_').val();
        var candidateStarterGroups_ = $('#candidateStarterGroups_').val();
        var candidate_group_type =  $('#candidate_group_type').val();

        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,2)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //4配置流程
        // if(null != processId_ && "" != processId_){
            JehcClickCell.processId_ = processId_;
        // }
        // if(null != processName_ && '' != processName_){
            JehcClickCell.processName_ = processName_;
        // }
        // if(null != poolnameSpace && '' != poolnameSpace){
            JehcClickCell.poolnameSpace = poolnameSpace;
        // }
        // if(null != candidateStarterUsers_ && '' != candidateStarterUsers_){
            JehcClickCell.candidateStarterUsers_ = candidateStarterUsers_;
        // }
        // if(null != candidateStarterGroups_ && '' != candidateStarterGroups_){
            JehcClickCell.candidateStarterGroups_ = candidateStarterGroups_;
        // }
        graph.startEditing();
        if(clickPOOLSureBtn(graph_refresh,JehcClickCell) == true){
            //赋值给流程基本属性
            var processId_ = JehcClickCell.processId_;
            var processName_ = JehcClickCell.processName_;
            var poolnameSpace = JehcClickCell.poolnameSpace;
            var candidateStarterUsers_ = JehcClickCell.candidateStarterUsers_;
            var candidateStarterGroups_ = JehcClickCell.candidateStarterGroups_;
            JehcClickCell.candidate_group_type = candidate_group_type;
            $('#processId').val(processId_);
            $('#processName').val(processName_);
            $('#mainNameSpace').val(poolnameSpace);
            $('#candidateStarterUsers').val(candidateStarterUsers_);
            $('#candidateStarterGroups').val(candidateStarterGroups_);
        }
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}
