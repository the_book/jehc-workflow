/**
 * 定时启动事件
 */
var timerStartEventForm;
function timerStartEventWin_(cell,graph_refresh){
    timerStartEventNodeAttributePanel(cell,graph_refresh);
    datetimeFormatInit();
}

/**
 * @param cell
 * @returns {string|*}
 */
function createTimerStartEventAttributeForm(cell){
    timerStartEventForm =
        "<div class=\"m-portlet\" id='mportletId' style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+

					//持续时间
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"col-form-label\" title='指定定时器之前要等待多长时间（定时间去经过多少时间后触发）.​ timeDuration可以设置为​timerEventDefinition​的子元素,使用ISO8601规定的格式，如P10D表示等待10天'>持续时间</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
                            "<input class=\"form-control\" onchange='setInputValue(this,\"TimerStartEvent\")' type=\"hidden\" id=\"timeDuration\" name=\"timeDuration\">"+
                            "<input class=\"form-control\" onchange='setInputValue(this,\"TimerStartEvent\")' type=\"hidden\" id=\"timeSelectFull\" name=\"timeSelectFull\">"+
                            "<div class=\"input-group\"><input type=\"text\" id=\"timeDurationText\" name=\"timeDurationText\" onchange='setInputValue(this,\"TimerStartEvent\")' readonly class=\"form-control m-input\" placeholder=\"请选择\" aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetTimeSelect(1)\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" title='时间选择器' onclick='showTimeSelect(1)'><i class=\"flaticon-event-calendar-symbol\"></i>选择时间</span></div></div>"+
						"</div>"+
					"</div>"+

                    //何时触发
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-2\">"+
                            "<label class=\"col-form-label\" title='指定一个固定时间（何时触发）， 这时定时器会触发，流程会继续。默认的时间格式是 dd/MM/yyyy hh:mm:ss。这是引擎范围的，可以通过设置 配置中的jbpm.duedatetime.format属性来改变。'>何时触发</label>"+
                        "</div>"+
                        "<div class=\"col-md-4\">"+
                            "<input class=\"form_datetime form-control\"  onchange='setInputValue(this,\"TimerStartEvent\")' type=\"text\" id=\"timeDate\" readonly name=\"timeDate\" placeholder=\"请选择日期\">"+
                        "</div>"+
                    "</div>"+

					//重复执行间隔（如【R3/PT10H表示：重复3次，每次间隔10小时】，【从整点开始,每5分钟执行一次0 0/5 * * * ?】）
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-2\">"+
							"<label class=\"col-form-label\" title='指定一个延迟时间段即重复执行间隔， 相对于流程进入定时器事件时。可以用两种定义方式：1.时间段表达式quantity [business] {second | seconds | minute | minutes | hour | hours | day | days | week | weeks | month | months | year | years} 比如“8 hours”。2.Cron表达式：“0 0 23 ? * FRI”如：如【R3/PT10H表示：重复3次，每次间隔10小时】，【从整点开始,每5分钟执行一次0 0/5 * * * ?】'>重复执行间隔</label>"+
						"</div>"+
                        "<div class=\"col-md-6\">"+
                            "<input class=\"form-control\" onchange='setInputValue(this,\"TimerStartEvent\")' type=\"hidden\" id=\"timeCycle\" name=\"timeCycle\">"+
                            "<input class=\"form-control\" onchange='setInputValue(this,\"TimerStartEvent\")' type=\"hidden\" id=\"timeCycleId\" name=\"timeCycleId\">"+
                            "<input class=\"form-control\" onchange='setInputValue(this,\"TimerStartEvent\")' type=\"hidden\" id=\"timeCycleFull\" name=\"timeCycleFull\">"+
                            "<div class=\"input-group\"><input type=\"text\" id=\"timeCycleText\" name=\"timeCycleText\" onchange='setInputValue(this,\"TimerStartEvent\")' readonly class=\"form-control m-input\" placeholder=\"请选择\" aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetTimeSelect(11)\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" title='时间选择器' onclick='showTimeSelect(11)'><i class=\"flaticon-event-calendar-symbol\"></i>选择时间</span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" title='选择表达式' onclick='showTimeExp(11,11)'><i class=\"flaticon-search\"></i>选择表达式</span></div></div>"+
                        "</div>"+
						"<div class=\"col-md-4\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return timerStartEventForm;
}



/**
 *
 * @param cell
 * @param graph_refresh
 */
function timerStartEventNodeAttributePanel(cell,graph_refresh){
    timerStartEventForm = createTimerStartEventAttributeForm(cell);
    nodeNormalForm = createNodeNormalForm(cell,1,"TimerStartEvent");
    form_grid = creatFormGrid(cell);
    event_grid = creatEventGrid(cell,1);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本属性</a>"+

                "<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">表&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">监听事件</a>"+

				// "<a href='javascript:setTimerStartEventValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+timerStartEventForm+"</div>"+
                "<div class=\"tab-pane fade\" id=\"v-pills-messages3\">"+form_grid+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();

    //基本属性
    initTimerStartEventData(cell);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,1);
    //表单配置信息
    initform_grid(cell);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
    nodeScroll();
}

/**
 *
 */
function initTimerStartEventData(cell){
    var timeDuration = cell.timeDuration;
    var timeDate = cell.timeDate;
    var timeCycle = cell.timeCycle;

    //扩展属性
    var timeSelectFull = cell.timeSelectFull;
    var timeDurationText = cell.timeDurationText;
    var timeCycleFull = cell.timeCycleFull;
    var timeCycleText = cell.timeCycleText;
    var timeCycleId = cell.timeCycleId;

    $('#timeDuration').val(timeDuration);
    $('#timeDate').val(timeDate);
    $('#timeCycle').val(timeCycle);
    $('#timeSelectFull').val(timeSelectFull);
    $('#timeDurationText').val(timeDurationText);
    $('#timeCycleFull').val(timeCycleFull);
    $('#timeCycleText').val(timeCycleText);
    $('#timeCycleId').val(timeCycleId);
}

/**
 * 设置内容
 */
function setTimerStartEventValue(){
    //var attachedToRef = $('#attachedToRef').val();
    var timeDuration = $('#timeDuration').val();
    var timeDate = $('#timeDate').val();
    var timeCycle = $('#timeCycle').val();

    //扩展属性
    var timeSelectFull =  $('#timeSelectFull').val();
    var timeDurationText = $('#timeDurationText').val();
    var timeCycleFull =  $('#timeCycleFull').val();
    var timeCycleText = $('#timeCycleText').val();
    var timeCycleId = $('#timeCycleId').val();


    console.log("--timeSelectFull-----",timeSelectFull);
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本属性
        /**
         if(null != attachedToRef && "" != attachedToRef){
			cell.attachedToRef = attachedToRef;
		}
         **/
        // if(null != timeDuration && "" != timeDuration){
            JehcClickCell.timeDuration = timeDuration;
        // }
        // if(null != timeDate && "" != timeDate){
            JehcClickCell.timeDate = timeDate;
        // }
        // if(null != timeCycle && "" != timeCycle){
            JehcClickCell.timeCycle = timeCycle;
        // }

        JehcClickCell.timeSelectFull = timeSelectFull;
        JehcClickCell.timeDurationText = timeDurationText;
        JehcClickCell.timeCycleFull = timeCycleFull;
        JehcClickCell.timeCycleText = timeCycleText;
        JehcClickCell.timeCycleId = timeCycleId;

        //4配置表单事件
        if(event_form_setvalue(JehcClickCell) == false){
            return;
        }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}