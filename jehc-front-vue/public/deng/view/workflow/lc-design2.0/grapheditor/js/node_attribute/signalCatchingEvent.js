var signalCatchingEventForm;
/**
 * 信号捕捉事件
 * @param cell
 * @param graph_refresh
 * @private
 */
function signalCatchingEventWin_(cell,graph_refresh){
    signalCatchingEventPanel(cell,graph_refresh);
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function signalCatchingEventPanel(cell,graph_refresh){
    nodeNormalForm = createNodeNormalForm(cell,2,"SignalCatchingEvent");
    event_grid = creatEventGrid(cell,1);
    signalCatchingEventForm = createSignalCatchingEvent(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本属性</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">监听事件</a>"+

				// "<a href='javascript:setSignalCatchingEventValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+signalCatchingEventForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,2);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
    //基本属性
    initSignalCatchingData(cell);

    nodeScroll();
}

/**
 *
 * @param cell
 */
function initSignalCatchingData(cell){
    var signalRef = cell.signalRef;
    $("#signalRef").val(signalRef);
}

/**
 *
 * @returns {string|*}
 */
function createSignalCatchingEvent(cell){
    signalCatchingEventForm =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
				//信号依附
				"<div class=\"form-group row\">"+
					"<div class=\"col-md-1\">"+
						"<label class=\"col-form-label\" >信号依附</label>"+
					"</div>"+
					"<div class=\"col-md-4\">"+
						"<input class=\"form-control\" type=\"text\"   onchange='setInputValue(this,\"SignalCatchingEvent\")' id=\"signalRef\" name=\"signalRef\" placeholder=\"请输入信号依附\">"+
					"</div>"+
				"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return signalCatchingEventForm;
}

function setSignalCatchingEventValue(){
    var signalRef = $('#signalRef').val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,2)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本属性
        // if(null != signalRef && "" != signalRef){
            JehcClickCell.signalRef = signalRef;
        // }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}