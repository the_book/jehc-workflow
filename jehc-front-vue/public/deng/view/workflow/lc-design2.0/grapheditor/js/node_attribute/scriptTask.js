var scriptTaskNodeAttributeForm;

/**
 * 脚本任务
 * @param cell
 * @param graph_refresh
 */
function showScriptTaskNodeAttributeWin(cell,graph_refresh){
    scriptTaskNodeAttributePanel(cell,graph_refresh);
}

/**
 *
 * @param cell
 * @returns {string|*}
 */
function createScriptTaskNodeAttributeForm(cell){
    scriptTaskNodeAttributeForm =
        "<div class=\"m-portlet\" id='mportletId' style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//语言类型
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >语言类型</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<select class=\"form-control\"  onchange='setInputValue(this,\"ScriptTask\")' data-bv-notempty data-bv-notempty-message=\"请选择\" id=\"scriptLanguage\" name=\"scriptLanguage\" placeholder=\"请选择\">"+
								"<option value='javascript'>javascript</option>" +
								"<option value='groovy'>groovy</option>" +
							"</select>"+
						"</div>"+
					"</div>"+

					//脚本
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >脚&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" onchange='setInputValue(this,\"ScriptTask\")' maxlength=\"50\"  data-bv-notempty data-bv-notempty-message=\"请输入脚本\" id=\"script\" name=\"script\" placeholder=\"请输入脚本\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return scriptTaskNodeAttributeForm;
}

/**
 *
 * @param cell
 */
function initScriptTaskData(cell){
    var scriptLanguage = cell.scriptLanguage;
    var script = cell.script;
    if(null != scriptLanguage && '' != scriptLanguage){
        $('#scriptLanguage').val(scriptLanguage);
    }
    if(null != script && '' != script){
        $('#script').val(script);
    }
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function scriptTaskNodeAttributePanel(cell,graph_refresh){
    nodeNormalForm = createNodeNormalForm(cell, 1,"ScriptTask");
    scriptTaskNodeAttributeForm = createScriptTaskNodeAttributeForm(cell);
    multiInstanceLoopCharacteristicForm = createMultiInstance(cell,"ScriptTask");
    event_grid = creatEventGrid(cell,1);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>" +
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">" +
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>" +

				"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">基本属性</a>" +

        		"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">会&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;签</a>" +

				"<a href=\"#v-pills-messages5\" data-toggle=\"pill\" class=\"\">监听事件</a>" +

				// "<a href='javascript:setScriptTaskValue()' class='svBtn'>保存配置</a>" +
			"</div>" +
		"</div>" +

		"<div class='col-md-11'>" +
			"<div class=\"tab-content tab-content-default\">" +
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">" + nodeNormalForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">" + scriptTaskNodeAttributeForm + "</div>" +
        		"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">" + multiInstanceLoopCharacteristicForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages5\">" + event_grid + "</div>" +
			"</div>" +
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>" + Tab + "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();

    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell, 1);
    //基本属性
    initScriptTaskData(cell);
    //初始化会签数据
    initMultiInstanceData(cell);
    //共用taskGrid属性事件
    initevent_grid(cell, 1);
    nodeScroll();
}

/**
 * 设置内容
 */
function setScriptTaskValue(){
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本属性
        var scriptLanguage = $('#scriptLanguage').val();
        var script = $('#script').val();
        // if(null != scriptLanguage && '' != scriptLanguage){
            JehcClickCell.scriptLanguage = scriptLanguage;
        // }
        // if(null != script && '' != script){
            JehcClickCell.script = script;
        // }
        //4配置会签
        multi_instance_setvalue(JehcClickCell);
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}
