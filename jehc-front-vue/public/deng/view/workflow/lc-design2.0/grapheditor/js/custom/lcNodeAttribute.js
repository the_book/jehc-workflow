
/**
 *
 * @param cell
 * @param graph
 */
function doLcNodeAttribute(cell,graph){
    var processName = $("#processName").val();
    var lc_process_id = $("#lc_process_id").val();
    if(null == processName || '' == processName){
        processName = "暂无";
    }
    var lcAttributeSelectModalCount = 0 ;
    $('#lcAttributeBody').height(reGetBodyHeight()-128);
    $('#lcAttributeModalLabel').html("流程节点：["+processName+"]-[<font color=red>"+cell.value+"</font>]");
    $('#lcAttributeModal').modal({backdrop: 'static', keyboard: false});
    $('#lcAttributeModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcAttributeModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++lcAttributeSelectModalCount == 1){
            // $("#lcAttributeIframe",document.body).attr("src",base_html_redirect+'/workflow/lc-design/lcAttribute.html?lc_process_id='+lc_process_id);
        }
    });
}