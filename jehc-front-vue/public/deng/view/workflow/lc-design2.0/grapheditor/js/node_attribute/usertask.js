//基本属性
var TaskNodeAttributeForm;
//配置表单
var TaskNodeAttributeFormSenior;

/**
 * 任务节点[task]
 * @param cell
 * @param graph_refresh
 */
function userTaskNodeAttributeWin(cell,graph_refresh){
    taskNodeAttributePanel(cell,graph_refresh);
}


/**
 * 创建用户任务  “审批人” 属性
 * @param cell
 * @returns {string|*}
 */

function createUserTaskNodeAttributeForm(cell){
    TaskNodeAttributeForm =
        "<div class=\"m-portlet\" id='mportletId' style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"userNodeForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\"><div class='alert m-alert m-alert--default' role='alert'>说明：流程设计器中办理人一旦设置则会持久化至引擎中<code>此处审批人为引擎审批人，优先级最高</code> .当开启该节点配置会签时候需要审批人必须采用流程表达式，否则在传递变量时 无法分配到指定人</div>"+
					//处理人
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >处&nbsp;&nbsp;理&nbsp;人</label>"+
						"</div>"+
						"<div class=\"col-md-6\">"+
							"<input type='hidden' id='useExpByAssignee' name='useExpByAssignee'><input type='hidden' id='useExpValueByAssignee' name='useExpValueByAssignee'><input class=\"form-control\" type=\"hidden\" maxlength=\"50\"  id=\"assignee\" name=\"assignee\" placeholder=\"请选择\">"+
							"<div class=\"input-group\"><input type=\"text\" readonly class=\"form-control m-input\"  onchange='setInputValue(this,\"UserTask\")' id='assignee_text' name='assignee_text' placeholder=\"请选择\" aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetAssignee(2,1)\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" onclick='initassignee(1,1)'><i class=\"flaticon-user-ok\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" title='基于表达式选择器' onclick='initasksigneeByExp(1,1)'><i class=\"flaticon-search-1\"></i>选择表达式</span></div></div>"+
						"</div>"+
					"</div>"+

					//候选人
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >候&nbsp;选&nbsp;&nbsp;人</label>"+
						"</div>"+
						"<div class=\"col-md-6\">"+
							"<input type='hidden' id='useExpByCandidateUsers' name='useExpByCandidateUsers'><input type='hidden' id='useExpValueByCandidateUsers' name='useExpValueByCandidateUsers'><input class=\"form-control\" type=\"hidden\" maxlength=\"50\"  id=\"candidateUsers\" name=\"candidateUsers\" placeholder=\"请选择\">"+
							"<div class=\"input-group\"><input type=\"text\" readonly class='form-control m-input' id='candidateUsers_Text'  onchange='setInputValue(this,\"UserTask\")' name='candidateUsers_Text' placeholder=\"请选择\" aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetAssignee(2,2)\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" onclick='initassignee(2,1)'><i class=\"fa flaticon-users\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" title='基于表达式选择器' onclick='initasksigneeByExp(1,2)'><i class=\"flaticon-search-1\"></i>选择表达式</span></div></div>"+
						"</div>"+
					"</div>"+

					//组类型
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >组&nbsp;&nbsp;类&nbsp;型</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<select id='candidate_group_type' class='form-control' onchange=\"canditateGroupChange(this,1)\"  name='candidate_group_type'><option value=''>请选择</option><option value='0'>部门</option><option value='1'>岗位</option><option value='2'>角色</option></select>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<div class='alert m-alert m-alert--default' role='alert'>注意：<code>组类型仅限于部门，岗位，角色</code> .</div>"+
						"</div>"+
					"</div>"+

					//候选群组
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >候选群组</label>"+
						"</div>"+
						"<div class=\"col-md-6\">"+
							"<input type='hidden' id='useExpByCandidateGroups' name='useExpByCandidateGroups'><input type='hidden' id='useExpValueByCandidateGroups' name='useExpValueByCandidateGroups'><input class=\"form-control\" type=\"hidden\" maxlength=\"50\"  id=\"candidateGroups\" name=\"candidateGroups\" placeholder=\"请选择\">"+
							"<div class=\"input-group\"><input type=\"text\" id=\"candidateGroups_Text\" name=\"candidateGroups_Text\"  onchange='setInputValue(this,\"UserTask\")' readonly class=\"form-control m-input\" placeholder=\"请选择\" aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetAssignee(2,3)\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" onclick='initcandidateGroups(2,1)'><i class=\"fa flaticon-users-1\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" title='基于表达式选择器' onclick='initasksigneeByExp(1,3)'><i class=\"flaticon-search-1\"></i>选择表达式</span></div></div>"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return TaskNodeAttributeForm;
}

/**
 * 基基本属性
 * @param cell
 */
function createTaskNodeAttributeFormSenior(cell){
    var isUseExpressionOption =
        "<option value='2'>否</option>"+
        "<option value='1'>是</option>";
    TaskNodeAttributeFormSenior =
        "<div class=\"m-portlet\" id='mportletId1'  style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//表单键
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
						"<label class=\"col-form-label\" >表&nbsp;单&nbsp;&nbsp;键</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
						"<input class=\"form-control\" type=\"text\" maxlength=\"50\"   onchange='setInputValue(this,\"UserTask\")' id=\"formKey\" name=\"formKey\" placeholder=\"请输入表单键\">"+
						"</div>"+
					"</div>"+

					//截止日期
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >截止日期</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  onchange='setInputValue(this,\"UserTask\")' id=\"dueDate\" name=\"dueDate\" placeholder=\"请选择截止日期\">"+
						"</div>"+
					"</div>"+

					//使用表达式
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >是否使用表达式</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<select class=\"form-control\" id='isUseExpression' onchange='setInputValue(this,\"UserTask\")' name=\"isUseExpression\">" +
        						isUseExpressionOption+
							"</select>"+
						"</div>"+
					"</div>"+

					//表达式
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >表&nbsp;达&nbsp;&nbsp;式</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"Expression\" onchange='setInputValue(this,\"UserTask\")' name=\"Expression\" placeholder=\"请输入\">"+
						"</div>"+
					"</div>"+

					//分类
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >分&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;类</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  id=\"category\" onchange='setInputValue(this,\"UserTask\")' name=\"category\" placeholder=\"请输入\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return TaskNodeAttributeFormSenior;
}

/**
 * 人工任务
 * @param cell
 */
function initUserTaskData(cell){
	//初始化配置用户数据
    var assignee = cell.assignee;
    var candidateUsers = cell.candidateUsers;
    var candidateGroups = cell.candidateGroups;

    var useExpByAssignee = cell.useExpByAssignee;
    var useExpValueByAssignee = cell.useExpValueByAssignee;
    var useExpByCandidateUsers = cell.useExpByCandidateUsers;
    var useExpValueByCandidateUsers =cell.useExpValueByCandidateUsers;
    var useExpByCandidateGroups = cell.useExpByCandidateGroups;
    var useExpValueByCandidateGroups = cell.useExpValueByCandidateGroups;
    var candidate_group_type = cell.candidate_group_type;

    $('#assignee').val(assignee);
    $('#candidateUsers').val(candidateUsers);
    $('#candidateGroups').val(candidateGroups);

    //扩展属性
	if($('#useExpByAssignee').val() == 0){
        $('#assignee_text').val(assignee);
	}
    if($('#useExpByCandidateUsers').val() == 0){
        $('#candidateUsers_Text').val(candidateUsers);
    }
    if($('#useExpByCandidateGroups').val() == 0){
        $('#candidateGroups_Text').val(candidateGroups);
    }
    $('#useExpByAssignee').val(useExpByAssignee);
    $('#useExpValueByAssignee').val(useExpValueByAssignee);
    $('#useExpByCandidateUsers').val(useExpByCandidateUsers);
    $('#useExpValueByCandidateUsers').val(useExpValueByCandidateUsers);
    $('#useExpByCandidateGroups').val(useExpByCandidateGroups);
    $('#useExpValueByCandidateGroups').val(useExpValueByCandidateGroups);
    $('#candidate_group_type').val(candidate_group_type);
    initACC(assignee,candidateUsers,candidateGroups,1);

	//初始化基本属性
    var formKey = cell.formKey;
    var dueDate = cell.dueDate;
    var priority = cell.priority;
    var Expression = cell.Expression;
    var isUseExpression = cell.isUseExpression;
    var category = cell.category;
    $('#formKey').val(formKey);
    $('#dueDate').val(dueDate);
    $('#priority').val(priority);
    $('#Expression').val(Expression);
    if(isUseExpression == 1){
        $('#isUseExpression').val(1);
    }else{
        $('#isUseExpression').val(2);
    }
    $('#category').val(category);
}

/**
 * 初始化面板属性
 * @param cell
 * @param graph_refresh
 */
function taskNodeAttributePanel(cell,graph_refresh){
    TaskNodeAttributeForm = createUserTaskNodeAttributeForm(cell);
    TaskNodeAttributeFormSenior = createTaskNodeAttributeFormSenior(cell);
    nodeNormalForm = createNodeNormalForm(cell,1,"UserTask");
    form_grid = creatFormGrid(cell);
    event_grid = creatEventGrid(cell,2);
    multiInstanceLoopCharacteristicForm = createMultiInstance(cell,"UserTask");
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本属性</a>"+

				"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">表&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">审&nbsp;批&nbsp;&nbsp;人</a>"+

				"<a href=\"#v-pills-messages5\" data-toggle=\"pill\" class=\"\">监听事件</a>"+

				"<a href=\"#v-pills-messages6\" data-toggle=\"pill\" class=\"\">会&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;签</a>"+

				// "<a href='javascript:setUserTaskValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
		"</div>"+

		"<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
			"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+TaskNodeAttributeFormSenior+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">"+form_grid+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+TaskNodeAttributeForm+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-messages5\">"+event_grid+"</div>"+
			"<div class=\"tab-pane fade\" id=\"v-pills-messages6\">"+multiInstanceLoopCharacteristicForm+"</div>"+
		"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();
    //基本属性,用户配置
    initUserTaskData(cell);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,1);
    //表单配置信息
    initform_grid(cell);
    //共用Event属性事件
    initevent_grid(cell,2);
	//初始化会签数据
    initMultiInstanceData(cell);
    nodeScroll();
}

/**
 * 设置内容
 */
function setUserTaskValue(){
    /**
     *assignee处理人
     *candidateUsers候选人ID
     *candidateGroups候选群组ID
     **/
    var assignee = $('#assignee').val();
    var candidateUsers = $('#candidateUsers').val();
    var candidateGroups = $('#candidateGroups').val();

    //扩展属性
    var useExpByAssignee = $('#useExpByAssignee').val();
    var useExpValueByAssignee = $('#useExpValueByAssignee').val();
    var useExpByCandidateUsers = $('#useExpByCandidateUsers').val();
    var useExpValueByCandidateUsers = $('#useExpValueByCandidateUsers').val();
    var useExpByCandidateGroups = $('#useExpByCandidateGroups').val();
    var useExpValueByCandidateGroups = $('#useExpValueByCandidateGroups').val();
    var candidate_group_type =  $('#candidate_group_type').val();

    /**
     *表单配置
     **/
    var formKey = $('#formKey').val();
    var dueDate = $('#dueDate').val();
    var priority = $('#priority').val();
    var Expression = $('#Expression').val();
    var isUseExpression = $('#isUseExpression').val();
    var category = $('#category').val();
    try
    {
		var graph = new mxGraph();
		graph.getModel().beginUpdate();

		//1.通用基本属性并具有赋值功能
		if(node_normal_setvalue(JehcClickCell,1)== false){
			return;
		}

		//2.事件配置
		if(event_setvalue(JehcClickCell)== false){
			return;
		}

		//3.配置用户
        JehcClickCell.assignee = assignee;
        JehcClickCell.candidateUsers = candidateUsers;
        JehcClickCell.candidateGroups = candidateGroups;

		//4.配置表单
        JehcClickCell.formKey = formKey;
        JehcClickCell.dueDate = dueDate;
        JehcClickCell.priority = priority;
        JehcClickCell.Expression = Expression;
		if(null != isUseExpression && '' != isUseExpression){
			JehcClickCell.isUseExpression = isUseExpression;
		}else{
			JehcClickCell.isUseExpression = "2"
		}
        JehcClickCell.category = category;

		//5.配置会签
		multi_instance_setvalue(JehcClickCell);

		//6.配置表单事件
		if(event_form_setvalue(JehcClickCell) == false){
			return;
		}

        //7.扩展属性
        JehcClickCell.useExpByAssignee = useExpByAssignee;
        JehcClickCell.useExpValueByAssignee = useExpValueByAssignee;
        JehcClickCell.useExpByCandidateUsers = useExpByCandidateUsers;
        JehcClickCell.useExpValueByCandidateUsers = useExpValueByCandidateUsers;
        JehcClickCell.useExpByCandidateGroups = useExpByCandidateGroups;
        JehcClickCell.useExpValueByCandidateGroups = useExpValueByCandidateGroups;
        JehcClickCell.candidate_group_type = candidate_group_type;
		graph.startEditing();
	}
	finally
	{
		graph.getModel().endUpdate();
		graph_refresh.refresh();
	}
}


	