var form_grid;
/**
 * 表单配置
 * @param cell
 * @returns {string|*}
 */
function creatFormGrid(cell){
    form_grid=
	"<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
		"<div class=\"m-portlet__head\">"+
			"<div class=\"m-portlet__head-caption\">" +
				"<a href=\"javascript:addEventFormRow()\" class=\"btn btn-light-primary mr-3 m-btn--custom m-btn--icon\" title='新一行'><i class=\"la la-plus\"></i>新一行</a>"+
       			"<a href=\"javascript:event_form_setvalue()\" class=\"btn btn-light-success font-weight-bold font-size-sm py-3 px-8 mr-2 mr-2\" title='保存'><i class=\"la la-save\"></i>保存</a>"+
				// "<button class=\"btn btn-light-warning font-weight-bold mr-2\" title=\"\" ><i class=\"icon-xl la la-bell\"></i></button>"+
				// "<button type=\"button\" class=\"btn btn-info m-btn m-btn--custom m-btn--icon\" onclick=\"addEventFormRow()\">新一行</button>"+
				// "&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-success m-btn m-btn--custom m-btn--icon\" value='保存' onclick='event_form_setvalue()'>"+
			"</div>"+
		"</div>"+

		"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"eventForm\" method=\"post\">"+
			// Tips
			"<div class=\"lert m-alert m-alert--default\" role=\"alert\">"+
				"说明：节点中展示不同的字段，<code>可写，可读，隐藏，必填</code>等，方便前端逻辑处理<code>灵活配置</code> .编号表示字段id，名称表示字段name"+
			"</div>"+
			"<div class=\"m-portlet__body\" id='event_form_grid'>"+
				// Title
				"<div class=\"form-group m-form__group row\" style='background: "+itemBackGround+"'>"+
					"<div class=\"col-md-1\">编 号</div>"+
					"<div class=\"col-md-1\">名 称</div>"+
					"<div class=\"col-md-1\">类 型</div>"+
					"<div class=\"col-md-1\">表达式</div>"+
					"<div class=\"col-md-1\">变 量</div>"+
					"<div class=\"col-md-1\">默认值</div>"+
					"<div class=\"col-md-1\">日期格式</div>"+
					"<div class=\"col-md-1\">可 读</div>"+
					"<div class=\"col-md-1\">可 写</div>"+
					"<div class=\"col-md-1\">必 填</div>"+
					"<div class=\"col-md-1\">值</div>"+
					"<div class=\"col-md-1\"></div>"+
				"</div>"+
			"</div>"+
		"</form>"+
	"</div>";
	return form_grid;
}

/**
 * 新一行 “表单配置”
 * @param formID          表单编号
 * @param formName        表单名称
 * @param formType        表单类型
 * @param formExpression  表达式
 * @param formVariable    变量
 * @param formDefault     默认
 * @param formDatePattern 格式日期
 * @param formReadable    可读
 * @param formWriteable   可写
 * @param formRequired    是否校验
 * @param formFormValues  配置字段
 */
function addEventFormRow(formID,formName,formType,formExpression,formVariable,formDefault,formDatePattern,formReadable,formRequired,formWriteable,formFormValues){
	if(formID === undefined){
        formID = "";
	}
	if(formName === undefined){
        formName = "";
	}
    if(formType === undefined){
        formType = "";
    }
    if(formExpression === undefined){
        formExpression = "";
    }
    if(formVariable === undefined){
        formVariable = "";
    }
    if(formDefault === undefined){
        formDefault = "";
    }
    if(formDatePattern === undefined){
        formDatePattern = "";
    }
    if(formReadable === undefined){
        formReadable = "";
    }
    if(formWriteable === undefined){
        formWriteable = "";
    }
    if(formRequired === undefined){
        formRequired = "";
    }
    if(formFormValues === undefined){
        formFormValues = "";
    }
	var uuid = guid();
    validatorDestroy('eventForm');
	var rows =
    "<div class=\"form-group m-form__group row\" id='event_form_rows_"+uuid+"'>"+
		//字段编号
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入编号' id='formID"+uuid+"' name='formNode[][formID]' value='"+formID+"' placeholder='请输入表单编号'>"+
		"</div>"+

		//字段名称
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message=\"请输入名称\" id='formName"+uuid+"' name='formNode[][formName]' value='"+formName+"' placeholder='请输入表单名称'>"+
		"</div>"+

		//字段类型
		"<div class=\"col-md-1\">"+
			"<select class=\"form-control\" id='formType"+uuid+"' name=\"formNode[][formType]\">" +
				"<option value=''>请选择</option>" +
				"<option value='string'>string</option>" +
				"<option value='long'>long</option>" +
				"<option value='enum'>enum</option>" +
				"<option value='date'>date</option>" +
				"<option value='boolean'>boolean</option>" +
			"</select>"+
		"</div>"+

		//表达式
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\" id='formExpression"+uuid+"' name=\"formNode[][formExpression]\" value='"+formExpression+"' placeholder=\"请输入表达式\">"+
		"</div>"+

		//变量
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\" id='formVariable"+uuid+"' name=\"formNode[][formVariable]\" value='"+formVariable+"' placeholder=\"请输入变量\">"+
		"</div>"+

		//默认值
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\"id='formDefault"+uuid+"' name=\"formNode[][formDefault]\" value='"+formDefault+"' placeholder=\"请输入\">"+
		"</div>"+

		//格式日期
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\" id='formDatePattern"+uuid+"' name=\"formNode[][formDatePattern]\" value='"+formDatePattern+"' placeholder=\"请输入\">"+
		"</div>"+

		//可读
		"<div class=\"col-md-1\">"+
			"<select class=\"form-control\" id='formReadable"+uuid+"' name=\"formNode[][formReadable]\">" +
				"<option value=''>请选择</option>" +
				"<option value='true'>true</option>" +
				"<option value='false'>false</option>" +
			"</select>"+
		"</div>"+

		//可写
		"<div class=\"col-md-1\">"+
			"<select class=\"form-control\" id='formWriteable"+uuid+"' name=\"formNode[][formWriteable]\">" +
				"<option value=''>请选择</option>" +
				"<option value='true'>true</option>" +
				"<option value='false'>false</option>" +
			"</select>"+
		"</div>"+

		//必填
		"<div class=\"col-md-1\">"+
			"<select class=\"form-control\" id='formRequired"+uuid+"' name=\"formNode[][formRequired]\">" +
				"<option value=''>请选择</option>" +
				"<option value='true'>true</option>" +
				"<option value='false'>false</option>" +
			"</select>"+
		"</div>"+

		//配置字段值
		"<div class=\"col-md-1\">"+
			"<input class=\"form-control\" type=\"text\" readonly='readonly' onclick='showFormValuesWin(this,"+null+",\""+uuid+"\")' id='formFormValues"+uuid+"' name=\"formNode[][formFormValues]\" value='"+formFormValues+"' placeholder=\"请配置值\">"+
		"</div>"+

        //操作
        "<div class=\"col-md-1\">"+
			"<button type=\"button\" class=\"btn btn-light-danger mr-3\" onclick='delEventFormRow(\""+uuid+"\")' title='删除'><i class=\"la la-times\"></i></button>"+
        "</div>"+
    "</div>";
    $("#event_form_grid").append(rows);
    $("#formType"+uuid).val(formType);
    $("#formReadable"+uuid).val(formReadable);
    $("#formWriteable"+uuid).val(formWriteable);
    $("#formRequired"+uuid).val(formRequired);
    reValidator('eventForm');
}

/**
 * 删除表单配置
 * @param rowID
 */
function delEventFormRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('eventForm');
        $("#event_form_rows_"+rowID).remove();
        reValidator('eventForm');
	});
}

/**
 * 保存表单配置
 * @param cell
 */
function event_form_setvalue(){
    var bootform = $('#eventForm');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        toastrBoot (2,"存在不合法的字段!");
        return false;
    }
    var form_node_value = JSON.stringify($("#eventForm").serializeJSON());
    console.log(form_node_value);
    if(null != form_node_value && "" != form_node_value){
        JehcClickCell.form_node_value = form_node_value;
	}else{
        JehcClickCell.form_node_value = "";
	}
	return true;
}

/**
 * 初始化init form grid
 * @param cell
 */
function initform_grid(cell){
    //表单配置
    $('#eventForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    initform_data(cell)
}
/**
 * 抽取出共同方法即渲染编辑表格方法(注意:flag为1表示只有开始和结束 2表示开始，结束，分配任务)
 * @param cell
 */
function initform_data(cell){
	var form_node_value = cell.form_node_value;
    if(null != form_node_value && "" != form_node_value){
        form_node_value = eval('(' + form_node_value + ')');
        form_node_value = form_node_value.formNode;
        if(form_node_value != undefined){
            for(var i = 0; i < form_node_value.length; i++){
                var formID = form_node_value[i].formID;
                var formName = form_node_value[i].formName;
                var formType = form_node_value[i].formType;
                var formExpression = form_node_value[i].formExpression;
                var formVariable = form_node_value[i].formVariable;
                var formDefault = form_node_value[i].formDefault;
                var formDatePattern = form_node_value[i].formDatePattern;
                var formReadable = form_node_value[i].formReadable;
                var formWriteable = form_node_value[i].formWriteable;
                var formRequired = form_node_value[i].formRequired;
                var formFormValues = form_node_value[i].formFormValues;
                addEventFormRow(formID,formName,formType,formExpression,formVariable,formDefault,formDatePattern,formReadable,formRequired,formWriteable,formFormValues);
            }
        }

    }
}

var form_values_grid;
/**
 * 设置表单值
 * @param thiz
 * @param cell
 * @param id
 */
function showFormValuesWin(thiz,cell,id){
	$('#jehcLcModalLabel').empty();
    $('#jehcLcForm').empty();
    $('#jehcLcModalLabel').append("配置字段值");
    form_values_grid = creatFormValuesGrid(cell,id);
    $("#jehcLcForm").append(form_values_grid);
    $('#eventFormValues').bootstrapValidator({
        message:'此值不是有效的'
    });
    initEventFormValues(id);//初始化数据
    $('#jehcLcModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#jehcLcModalDialog");
        $modal_dialog.css({'width':reGetBodyWidth()*0.4+'px'});
    });
    // $('#jehcLcModal').modal({backdrop: 'static', keyboard: false});
}

/**
 * 创建配置字段表单
 * @param cell
 * @param id
 * @returns {string|*}
 */
function creatFormValuesGrid(cell,id){
    form_values_grid=
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<a href=\"javascript:addEventFormValuesRow()\" class=\"btn btn-light-primary mr-3 m-btn--custom m-btn--icon\" title='新一行'><i class=\"la la-plus\"></i>新一行</a>"+
					"<a href=\"javascript:saveEventFormValues('"+id+"')\" class=\"btn btn-light-success font-weight-bold font-size-sm py-3 px-8 mr-2 mr-2\" title='保存'><i class=\"la la-save\"></i>保存</a>"+
					// "<button type=\"button\" class=\"btn btn-info m-btn m-btn--custom m-btn--icon\" onclick=\"addEventFormValuesRow()\">新一行</button>"+
					// "&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-success m-btn m-btn--custom m-btn--icon\" onclick='saveEventFormValues(\""+id+"\")'>保存</button>"+
        			// "&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--label-danger m-btn--bolder m-btn--uppercase\" onclick=\"closeJehcLcWin()\">关闭</button>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"eventFormValues\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='event_form_values_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: "+itemBackGround+"'>"+
						"<div class=\"col-md-3\">编号</div>"+
						"<div class=\"col-md-5\">名称</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return form_values_grid;
}

/**
 * 初始化表单配置字段值
 * @param id
 */
function initEventFormValues(id){
    var formFormValues = $("#formFormValues"+id).val();
    if(null != formFormValues && "" != formFormValues){
        formFormValues = eval('(' + formFormValues + ')');
        if(null != formFormValues && "" != formFormValues && formFormValues.fields != undefined){
            formFormValues = formFormValues.fields;
            for(var i = 0; i < formFormValues.length; i++){
                var fid = formFormValues[i].fid;
                var fname = formFormValues[i].fname;
                addEventFormValuesRow(fid,fname);
            }
        }
    }
}

/**
 * 动态添加 新一行 ”表单字段“
 * @param fid
 * @param fname
 */
function addEventFormValuesRow(fid,fname){
    validatorDestroy('eventFormValues');
    var uuid = guid();
    if(fid === undefined){
        fid = "";
	}
	if(fname === undefined){
    	fname = "";
	}
    var rows =
        "<div class=\"form-group m-form__group row\" id='event_form_values_rows_"+uuid+"'>"+
			//编号
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入编号' id='fid"+uuid+"' value='"+fid+"' name='fields[][fid]' placeholder='请输入编号'>"+
			"</div>"+

			//名称
			"<div class=\"col-md-5\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message=\"请输入名称\" id='fname"+uuid+"' value='"+fname+"' name='fields[][fname]' placeholder='请输入名称'>"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn btn-light-danger mr-3\" onclick='delEventFormValuesRow(\""+uuid+"\")' title='删除'><i class=\"la la-times\"></i></button>"+
			"</div>"+
        "</div>";
    $("#event_form_values_grid").append(rows);
    reValidator('eventFormValues');
}

/**
 * 保存配置字段
 * @param id
 */
function saveEventFormValues(id){
	var bootform = $('#eventFormValues');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        toastrBoot (2,"存在不合法的字段!");
        return;
    }
    $("#formFormValues"+id).val(JSON.stringify($("#eventFormValues").serializeJSON()));
    closeJehcLcWin();
}

/**
 * 删除配置字段
 * @param id
 */
function delEventFormValuesRow(id){
    validatorDestroy('eventFormValues');
	$("#event_form_values_rows_"+id).remove();
    reValidator('eventFormValues');
}