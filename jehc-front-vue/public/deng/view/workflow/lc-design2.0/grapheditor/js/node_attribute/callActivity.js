var callActivityNodeAttributeForm;
/**
 *内嵌子流程节点
 * @param cell
 * @param graph_refresh
 */
function showCallActivityNodeAttributeWin(cell,graph_refresh){
    callActivityNodeAttributePanel(cell,graph_refresh);
}

/**
 * 基本节点属性
 * @param cell
 * @returns {string|*}
 */
function createCallActivityNodeAttributeForm(cell){
    callActivityNodeAttributeForm =
        "<div class=\"m-portlet\" id='mportletId' style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+

					//被呼叫流程
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
						"<label class=\"col-form-label\" title='即被调用的流程'>被呼叫流程</label>"+
						"</div>"+
						"<div class=\"col-md-3\">"+
						// "<input class=\"form-control\" type=\"text\" onchange='setInputValue(this,\"CallActivity\")'  id=\"calledElement\" name=\"calledElement\" placeholder=\"请输入被呼叫流程\">"+
                        "<div class=\"input-group\"><input type=\"text\"  class=\"form-control m-input\" maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入被呼叫流程\" onchange='setInputValue(this,\"CallActivity\")' id='calledElement' name='calledElement' placeholder=\"请输入\" aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetCallActivity()\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" onclick='initCallActivity()'><i class=\"flaticon-search\"></i></span></div></div>"+

                        "</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return callActivityNodeAttributeForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initCallActivityData(cell){
    /**取值**/
    var calledElement = cell.calledElement;
    /**赋值**/
    $('#calledElement').val(calledElement);
}

/**
 * 创建表单
 * @param cell
 * @param graph_refresh
 */
function callActivityNodeAttributePanel(cell,graph_refresh) {
    callActivityNodeAttributeForm = createCallActivityNodeAttributeForm(cell);
    callActivityNodeAttributeInputParmGrid = createCallActivityNodeAttributeInputParmGrid(cell);
    callActivityNodeAttributeOutParmGrid = createCallActivityNodeAttributeOutParmGrid(cell);
    nodeNormalForm = createNodeNormalForm(cell, 1,"CallActivity");
    multiInstanceLoopCharacteristicForm = createMultiInstance(cell,"CallActivity");
    event_grid = creatEventGrid(cell, 1);
    //Tab Index
    var Tab =
        "<div class='col-md-1'>" +
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">" +
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>" +

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本属性</a>" +

				"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">监听事件</a>" +

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">会&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;签</a>" +

        		"<a href=\"#v-pills-messages5\" data-toggle=\"pill\" class=\"\">输入输出</a>" +

				// "<a href='javascript:setCallActivityValue()' class='svBtn'>保存配置</a>" +
			"</div>" +
        "</div>" +

        "<div class='col-md-11'>" +
			"<div class=\"tab-content tab-content-default\">" +
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">" + nodeNormalForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">" + callActivityNodeAttributeForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">" + event_grid + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">" + multiInstanceLoopCharacteristicForm + "</div>" +
        		"<div class=\"tab-pane fade\" id=\"v-pills-messages5\">" + callActivityNodeAttributeInputParmGrid +callActivityNodeAttributeOutParmGrid+ "</div>" +
			"</div>" +
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>" + Tab + "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();

    //基本属性
    initCallActivityData(cell);

    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell, 1);

    //共用taskGrid属性事件
    initevent_grid(cell, 1);

    //初始化会签数据
    initMultiInstanceData(cell);

    //输入配置
    initCallActivityNodeAttributeInput_grid(cell);

    //输出配置
    initCallActivityNodeAttributeOut_grid(cell);

    nodeScroll();
}

/**
 * 设置内容
 */
function setCallActivityValue(){
    var calledElement = $('#calledElement').val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本属性
        // if(null != calledElement && '' != calledElement){
            JehcClickCell.calledElement = calledElement;
        // }
        //4配置会签
        multi_instance_setvalue(JehcClickCell);

        //5输入输出
		if(callActivityNodeAttributeInput_setvalue(JehcClickCell) == false){
			return;
		}
		if(callActivityNodeAttributeOut_setvalue(JehcClickCell) == false){
           return;
		}
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}

////////////////////////////////////////////////////////输入参数开始//////////////////////////////////////////////////////
var callActivityNodeAttributeInputParmGrid;
/**
 * 创建输入参数Grid
 * @param cell
 */
function createCallActivityNodeAttributeInputParmGrid(cell){
    callActivityNodeAttributeInputParmGrid=
        "<div class=\"m-portlet\" style='height:170px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<div class=\"m-portlet__head-title\">"+
						"<h3 class=\"m-portlet__head-text\">输入参数</h3>"+
                        "&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-light-primary mr-3 m-btn m-btn--custom m-btn--icon\" onclick=\"addCallActivityNodeAttributeInputRow()\" title='新一行'><i class=\"la la-plus\"></i>新一行</button>"+
                        "&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-icon btn-light-success mr-2 m-btn m-btn--custom m-btn--icon\" onclick='callActivityNodeAttributeInput_setvalue()' title='保存'><i class=\"la la-save\"></i>保存</button>"+
				    "</div>"+
                "</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"node_CallActivityNodeAttributeInput\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='node_CallActivityNodeAttributeInput_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: "+itemBackGround+"'>"+
						"<div class=\"col-md-4\">源</div>"+
						"<div class=\"col-md-4\">源表达式</div>"+
						"<div class=\"col-md-3\">目标</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return callActivityNodeAttributeInputParmGrid;
}

/**
 * 新一行
 * @param source
 * @param sourceExpression
 * @param target
 */
function addCallActivityNodeAttributeInputRow(source,sourceExpression,target){
    if(source === undefined){
        source = "";
    }
    if(sourceExpression === undefined){
        sourceExpression = "";
    }
    if(target === undefined){
        target = "";
    }
    var uuid = guid();
    validatorDestroy('node_CallActivityNodeAttributeInput');
    var rows =
        "<div class=\"form-group m-form__group row\" id='node_callActivityNodeAttributeInputParmGrid_rows_"+uuid+"'>"+
			//源
			"<div class=\"col-md-4\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入' id='source"+uuid+"' name='callActivityInputOut[][source]' value='"+source+"' placeholder='请输入'>"+
			"</div>"+


			//源表达式
			"<div class=\"col-md-4\">"+
				"<input class=\"form-control\" type=\"text\" id='sourceExpression"+uuid+"' name=\"callActivityInputOut[][sourceExpression]\" value='"+sourceExpression+"' placeholder=\"请输入源表达式\">"+
			"</div>"+

			//目标
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\" id='target"+uuid+"' name=\"callActivityInputOut[][target]\" value='"+target+"' placeholder=\"请输入\">"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delCallActivityNodeAttributeInputRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#node_CallActivityNodeAttributeInput_grid").append(rows);
    reValidator('node_CallActivityNodeAttributeInput');
}

/**
 * 删除
 * @param rowID
 */
function delCallActivityNodeAttributeInputRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('node_CallActivityNodeAttributeInput');
        $("#node_callActivityNodeAttributeInputParmGrid_rows_"+rowID).remove();
        reValidator('node_CallActivityNodeAttributeInput');
    });
}

/**
 * 初始化init event grid
 * @param cell
 * @param flag
 */
function initCallActivityNodeAttributeInput_grid(cell,flag){
    //表单配置
    $('#node_CallActivityNodeAttributeInput').bootstrapValidator({
        message:'此值不是有效的'
    });
    initCallActivityNodeAttributeInput_data(cell,flag)
}

/**
 *初始化输入参数数据
 * @param cell
 * @param flag
 */
function initCallActivityNodeAttributeInput_data(cell,flag){
    var callActivity_input_value = cell.callActivity_input_value;
    if(null != callActivity_input_value && "" != callActivity_input_value){
        callActivity_input_value = eval('(' + callActivity_input_value + ')');
        /*已废弃
        var sourceList = $.makeArray(callActivity_input_value["source"]);
        var sourceExpressionList = $.makeArray(callActivity_input_value["sourceExpression"]);
        var targetList = $.makeArray(callActivity_input_value["target"]);
        for(var i = 0; i < sourceList.length; i++){
            var source = sourceList[i];
            var sourceExpression = sourceExpressionList[i];
            var target = targetList[i];
            addCallActivityNodeAttributeInputRow(source,sourceExpression,target)
        }*/
        callActivity_input_value = callActivity_input_value.callActivityInputOut;
        if(callActivity_input_value != undefined){
            for(var i = 0; i < callActivity_input_value.length; i++){
                var source = callActivity_input_value[i].source;
                var sourceExpression = callActivity_input_value[i].sourceExpression;
                var target = callActivity_input_value[i].target;
                addCallActivityNodeAttributeInputRow(source,sourceExpression,target);
            }
        }
    }
}

/**
 * 点击确定按钮设置mxgraph中cell属性
 * @param cell
 * @returns {boolean}
 */
function callActivityNodeAttributeInput_setvalue(cell){
    var bootform = $('#node_CallActivityNodeAttributeInput');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        toastrBoot (2,"存在不合法的字段!");
        return false;
    }
    /*已废弃
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var callActivity_input_value = JSON.stringify($("#node_CallActivityNodeAttributeInput").serializeObject());*/
    var callActivity_input_value = JSON.stringify($("#node_CallActivityNodeAttributeInput").serializeJSON());
    if(null != callActivity_input_value && "" != callActivity_input_value){
        JehcClickCell.callActivity_input_value = callActivity_input_value;
    }else{
        JehcClickCell.callActivity_input_value = "";
    }
    return true;
}
////////////////////////////////////////////////////////输入参数结束//////////////////////////////////////////////////////

////////////////////////////////////////////////////////输出参数开始//////////////////////////////////////////////////////
var callActivityNodeAttributeOutParmGrid = null;
/**
 * 创建输出参数Grid
 * @param cell
 */
function createCallActivityNodeAttributeOutParmGrid(cell){
    callActivityNodeAttributeOutParmGrid=
        "<div class=\"m-portlet\" style='height:150px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<div class=\"m-portlet__head-title\">"+
						"<h3 class=\"m-portlet__head-text\">输出参数</h3>"+
                        "&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-light-primary mr-3 m-btn m-btn--custom m-btn--icon\" onclick=\"addCallActivityNodeAttributeOutRow()\" title='新一行'><i class=\"la la-plus\"></i>新一行</button>"+
                        "&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-icon btn-light-success mr-2 m-btn m-btn--custom m-btn--icon\" title='保存' onclick='callActivityNodeAttributeOut_setvalue()'><i class=\"la la-save\"></i>保存</button>"+
				    "</div>"+
                "</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"node_CallActivityNodeAttributeOut\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='node_CallActivityNodeAttributeOut_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: "+itemBackGround+"'>"+
						"<div class=\"col-md-4\">源</div>"+
						"<div class=\"col-md-4\">源表达式</div>"+
						"<div class=\"col-md-3\">目标</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return callActivityNodeAttributeOutParmGrid;
}

/**
 * 新一行
 * @param source
 * @param sourceExpression
 * @param target
 */
function addCallActivityNodeAttributeOutRow(source,sourceExpression,target){
    if(source === undefined){
        source = "";
    }
    if(sourceExpression === undefined){
        sourceExpression = "";
    }
    if(target === undefined){
        target = "";
    }
    var uuid = guid();
    validatorDestroy('node_CallActivityNodeAttributeOut');
    var rows =
        "<div class=\"form-group m-form__group row\" id='node_callActivityNodeAttributeOutParmGrid_rows_"+uuid+"'>"+
			//源
			"<div class=\"col-md-4\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入' id='source"+uuid+"' name='callActivityInputOut[][source]' value='"+source+"' placeholder='请输入'>"+
			"</div>"+


			//源表达式
			"<div class=\"col-md-4\">"+
				"<input class=\"form-control\" type=\"text\" id='sourceExpression"+uuid+"' name=\"callActivityInputOut[][sourceExpression]\" value='"+sourceExpression+"' placeholder=\"请输入源表达式\">"+
			"</div>"+

			//目标
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\" id='target"+uuid+"' name=\"callActivityInputOut[][target]\" value='"+target+"' placeholder=\"请输入运行方式\">"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delCallActivityNodeAttributeOutRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#node_CallActivityNodeAttributeOut_grid").append(rows);
    reValidator('node_CallActivityNodeAttributeOut');
}


/**
 * 删除
 * @param rowID
 */
function delCallActivityNodeAttributeOutRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('node_CallActivityNodeAttributeOut');
        $("#node_callActivityNodeAttributeOutParmGrid_rows_"+rowID).remove();
        reValidator('node_CallActivityNodeAttributeOut');
    });
}

/**
 * 初始化init event grid
 * @param cell
 * @param flag
 */
function initCallActivityNodeAttributeOut_grid(cell,flag){
    //表单配置
    $('#node_CallActivityNodeAttributeOut').bootstrapValidator({
        message:'此值不是有效的'
    });
    initCallActivityNodeAttributeOut_data(cell,flag);
}

/**
 *初始化输入参数数据
 * @param cell
 * @param flag
 */
function initCallActivityNodeAttributeOut_data(cell,flag){
    var callActivity_out_value = cell.callActivity_out_value;
    if(null != callActivity_out_value && "" != callActivity_out_value){
        callActivity_out_value = eval('(' + callActivity_out_value + ')');
        /*
       已废弃
       var sourceList = $.makeArray(callActivity_out_value["source"]);
       var sourceExpressionList = $.makeArray(callActivity_out_value["sourceExpression"]);
       var targetList = $.makeArray(callActivity_out_value["target"]);
       for(var i = 0; i < sourceList.length; i++){
           var source = sourceList[i];
           var sourceExpression = sourceExpressionList[i];
           var target = targetList[i];
           addCallActivityNodeAttributeOutRow(source,sourceExpression,target)
       }*/
        callActivity_out_value = callActivity_out_value.callActivityInputOut;
        if(callActivity_out_value != undefined){
            for(var i = 0; i < callActivity_out_value.length; i++) {
                var source = callActivity_out_value[i].source;
                var sourceExpression = callActivity_out_value[i].sourceExpression;
                var target = callActivity_out_value[i].target;
                addCallActivityNodeAttributeOutRow(source, sourceExpression, target);
            }
        }
    }
}

/**
 * 点击确定按钮设置mxgraph中cell属性
 * @param cell
 * @returns {boolean}
 */
function callActivityNodeAttributeOut_setvalue(cell){
    var bootform = $('#node_CallActivityNodeAttributeOut');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        toastrBoot (2,"存在不合法的字段!");
        return false;
    }
    /* 已废弃
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var callActivity_out_value = JSON.stringify($("#node_CallActivityNodeAttributeOut").serializeObject());
    */
    var callActivity_out_value = JSON.stringify($("#node_CallActivityNodeAttributeOut").serializeJSON());
    if(null != callActivity_out_value && "" != callActivity_out_value){
        JehcClickCell.callActivity_out_value = callActivity_out_value;
    }else{
        JehcClickCell.callActivity_out_value = "";
    }
    return true;
}
////////////////////////////////////////////////////////输出参数结束//////////////////////////////////////////////////////