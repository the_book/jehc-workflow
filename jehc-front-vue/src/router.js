import Vue from "vue";
import Router from "vue-router";
import { getToken } from "@/core/util/tokenUtil";
import { reloadRouter,doRouterTree,getDynamicsRouters, setLocalItem,removeLocalItem,getLocalItem} from "@/core/util/jehcUtil.js";
import { async } from "q";
Vue.use(Router);
// 解决报错
const originalPush = Router.prototype.push
const originalReplace = Router.prototype.replace
// push
Router.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
// replace
Router.prototype.replace = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalReplace.call(this, location, onResolve, onReject)
  return originalReplace.call(this, location).catch(err => err)
}
const staticRouterMap = [
  {
    path: "/",
    redirect: "/dashboard",
    component: () => import("@/view/layout/layout"),
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        component: () => import("@/view/dashboard.vue")
      },
      {
        path: "/builder",
        name: "builder",
        component: () => import("@/view/builder.vue")
      }
    ]
  },
  {
    path: "/",
    component: () => import("@/view/layout/layout"),
    children: [     
      {//附件
        name: "/file/attachment",
        path: "/file/attachment",
        component: () => import("@/view/file/file-attachment/file-attachment-list.vue")
      }, 
      {//页面加载时长统计
        name: "/log/loading",
        path: "/log/loading",
        component: () => import("@/view/log/log-loadinfo/log-loadinfo-list.vue")
      },
      {//服务停止与启动
        name: "/log/startStop",
        path: "/log/startStop",
        component: () => import("@/view/log/log-start-stop/log-start-stop-list.vue")
      }, 
      {//操作日志
        name: "/log/operate",
        path: "/log/operate",
        component: () => import("@/view/log/log-operate/log-operate-list.vue")
      }, 
      {//异常日志
        name: "/log/error",
        path: "/log/error",
        component: () => import("@/view/log/log-error/log-error-list.vue")
      }, 
      {//变更日志
        name: "/log/modify",
        path: "/log/modify",
        component: () => import("@/view/log/log-modify-record/log-modify-record-list.vue")
      }, 
      {//登录日志
        name: "/log/login",
        path: "/log/login",
        component: () => import("@/view/log/log-login/log-login-list.vue")
      }, 



      {//公司信息
        name: "/sys/company",
        path: "/sys/company",
        component: () => import("@/view/sys/xt-company/xt-company.vue")
      },
      {//部门管理
        name: "/sys/departinfo",
        path: "/sys/departinfo",
        component: () => import("@/view/sys/xt-departinfo/xt-departinfo-list.vue")
      },
      {//岗位管理
        name: "/sys/post",
        path: "/sys/post",
        component: () => import("@/view/sys/xt-post/xt-post-list.vue")
      },
      {//用户管理
        path: "/sys/userinfo",
        name: "/sys/userinfo",
        component: () => import("@/view/sys/xt-userinfo/xt-userinfo-list.vue"),
      },
      {//组织机构
        path: "/sys/org",
        name: "/sys/org",
        component: () => import("@/view/sys/xt-org/xt-org-list.vue"),
      },

      {//系统消息
        path: "/sys/message",
        name: "/sys/message",
        component: () => import("@/view/sys/xt-message/xt-message-list.vue"),
      },
      {//平台消息
        path: "/sys/platform",
        name: "/sys/platform",
        component: () => import("@/view/sys/xt-platform/xt-platform-list.vue"),
      },
      {//公告
        path: "/sys/notify",
        name: "/sys/notify",
        component: () => import("@/view/sys/xt-notify/xt-notify-list.vue"),
      },
      {//通知
        path: "/sys/notice",
        name: "/sys/notice",
        component: () => import("@/view/sys/xt-notice/xt-notice-list.vue"),
      },
      {//知识库
        path: "/sys/knowledge",
        name: "/sys/knowledge",
        component: () => import("@/view/sys/xt-knowledge/xt-knowledge-list.vue"),
      },
      {//计量单位
        path: " /sys/dev/config/unit",
        name: "/sys/dev/config/unit",
        component: () => import("@/view/sys/xt-unit/xt-unit-list.vue"),
      },
      {//常量
        path: " /sys/dev/config/constant",
        name: "/sys/dev/config/constant",
        component: () => import("@/view/sys/xt-constant/xt-constant-list.vue"),          
      },
      {//路径
        path: " /sys/dev/config/path",
        name: "/sys/dev/config/path",
        component: () => import("@/view/sys/xt-path/xt-path-list.vue"),          
      },
      {//数据字典
        path: " /sys/dev/config/dictionary",
        name: "/sys/dev/config/dictionary",
        component: () => import("@/view/sys/xt-data-dictionary/xt-data-dictionary-list.vue"),          
      },
      {//第三方短信
        path: " /sys/dev/config/sms",
        name: "/sys/dev/config/sms",
        component: () => import("@/view/sys/xt-sms/xt-sms-list.vue"),          
      },
      {//ip冻结
        path: " /sys/dev/config/ipFrozen",
        name: "/sys/dev/config/ipFrozen",          
        component: () => import("@/view/sys/xt-ip-frozen/xt-ip-frozen-list.vue"),          
      },
      {//行政区划
        path: " /sys/dev/area/region",
        name: "/sys/dev/area/region",          
        component: () => import("@/view/sys/xt-area-region/xt-area-region-list.vue"),          
      },
      {//版本管理
        path: "/sys/dev/version",
        name: "/sys/dev/version",          
        component: () => import("@/view/sys/xt-version/xt-version-list.vue"),          
      },
      {//单号
        path: "/sys/dev/number",
        name: "/sys/dev/number",          
        component: () => import("@/view/sys/xt-number/xt-number-list.vue"),          
      },
      {//条码二维码
        path: "/sys/dev/encoderqrcode",
        name: "/sys/dev/encoderqrcode",          
        component: () => import("@/view/sys/xt-encoderqrcode/xt-encoderqrcode-list.vue"),          
      },


      {//账户
        path: "/oauth/account",
        name: "/oauth/account",
        component: () => import("@/view/oauth/oauth-account/oauth-account-list.vue"),
      },
      {//在线用户
        path: "/oauth/online",
        name: "/oauth/online",
        component: () => import("@/view/oauth/oauth-online/oauth-online-list.vue"),
      },  
      {//角色权限
        path: "/oauth/role",
        name: "/oauth/role",
        component: () => import("@/view/oauth/oauth-role/oauth-role-list.vue"),
      },  
      {//模块卸载
        path: "/oauth/module/uninstall",
        name: "/oauth/module/uninstall",
        component: () => import("@/view/oauth/oauth-module-uninstall/oauth-module-uninstall.vue"),
      },
      {//账户类型
        path: "/oauth/accountType",
        name: "/oauth/accountType",
        component: () => import("@/view/oauth/oauth-account-type/oauth-account-type-list.vue"),
      },
      {//平台密钥
        path: "/oauth/keyinfo",
        name: "/oauth/keyinfo",
        component: () => import("@/view/oauth/oauth-key-info/oauth-key-info-list.vue"),
      },
      {//配置非权限API
        path: "/oauth/function-common",
        name: "/oauth/function-common",
        component: () => import("@/view/oauth/oauth-function-common/oauth-function-common-list.vue"),
      },
      {//资源模块
        path: "/oauth/modules",
        name: "/oauth/modules",
        component: () => import("@/view/oauth/oauth-sys-modules/oauth-sys-modules-list.vue"),
      },
      {//菜单资源
        path: "/oauth/resources",
        name: "/oauth/resources",
        component: () => import("@/view/oauth/oauth-resources/oauth-resources-list.vue"),
      },
      {//功能资源
        path: "/oauth/function-info",
        name: "/oauth/function-info",
        component: () => import("@/view/oauth/oauth-function-info/oauth-function-info-list.vue"),
      },
      {//授权中心子系统
        path: "/oauth/mode",
        name: "/oauth/mode",
        component: () => import("@/view/oauth/oauth-sys-mode/oauth-sys-mode-list.vue"),
      },

      {//分配管理员
        path: "/oauth/admin",
        name: "/oauth/admin",
        component: () => import("@/view/oauth/oauth-admin/oauth-admin-list.vue"),
      },

      {//调度日志
        path: "/job/log",
        name: "/job/log",
        component: () => import("@/view/job/job-log/job-log-list.vue"),
      },
      {//配置任务
        path: "/job/config",
        name: "/job/config",
        component: () => import("@/view/job/job-config/job-config-list.vue"),
      },
      {//执行任务
        path: "/job/execute",
        name: "/job/execute",
        component: () => import("@/view/job/job-set/job-set-list.vue"),
      },

      //运管平台
      {//Redis配置
        path: " /omp/redisConfig",
        name: " /omp/redisConfig",
        component: () => import("@/view/omp/rds/omp-redis-config/omp-redis-config-list.vue"),
      },

      {//Redis监控
        path: " /omp/redisMain",
        name: " /omp/redisMain",
        component: () => import("@/view/omp/rds/omp-redis-main/omp-redis-main-list.vue"),
      },

      {//Redis监控日志
        path: " /omp/redisInfo",
        name: " /omp/redisInfo",
        component: () => import("@/view/omp/rds/omp-redis-info/omp-redis-info-list.vue"),
      },

      {//内存运行日志
        path: " /omp/mem",
        name: " /omp/mem",
        component: () => import("@/view/omp/scms/scms-monitor-mem/scms-monitor-mem-list.vue"),
      },
      {//CPU运行日志
        path: " /omp/cpu",
        name: " /omp/cpu",
        component: () => import("@/view/omp/scms/scms-monitor-cpu/scms-monitor-cpu-list.vue"),
      },
      {//物理机信息
        path: " /omp/monitor",
        name: " /omp/monitor",
        component: () => import("@/view/omp/scms/scms-monitor/scms-monitor-list.vue"),
      },
      {//授权中心耗时追踪
        path: "/omp/awstats/authLog",
        name: "/omp/awstats/authLog",
        component: () => import("@/view/omp/awstats/auth-log/auth-log-list.vue"),
      },
      {//路由信息
        path: "/omp/route/dynamic/route",
        name: "/omp/route/dynamic/route",
        component: () => import("@/view/omp/route/dynamic-route/dynamic-route-list.vue"),
      },
       {//路由模块
        path: "/omp/route/dynamic/route/modules",
        name: "/omp/route/dynamic/route/modules",
        component: () => import("@/view/omp/route/dynamic-route-modules/dynamic-route-modules-list.vue"),
      },


    ]
  },
  {
    path: "/",
    component: () => import("@/view/login"),
    children: [
      {
        name: "login",
        path: "/login",
        component: () => import("@/view/login.vue")
      },
      {
        name: "register",
        path: "/register",
        component: () => import("@/view/sys/register/register.vue")
      }
    ]
  },
  {
    path: "*",
    redirect: "/404"
  },
  {
    path: "/500",
    name: "500",
    component: () => import("@/view/sys/error/500.vue"),
  },
  {
    // the 404 route, when none of the above matches
    path: "/404",
    name: "404",
    component: () => import("@/view/sys/error/404.vue")
  }
];


const createRouter = () =>
  new Router({
      mode: 'hash',
      scrollBehavior: () => ({ y: 0 }),
      routes: [
        {
          path: "/",
          redirect: "/dashboard",
          component: () => import("@/view/layout/layout"),
          children: [
            {
              path: "/dashboard",
              name: "dashboard",
              component: () => import("@/view/dashboard.vue")
            },
            {
              path: "/builder",
              name: "builder",
              component: () => import("@/view/builder.vue")
            }
          ]
        },
        {
          path: "/",
          component: () => import("@/view/login"),
          children: [
            {
              name: "login",
              path: "/login",
              component: () => import("@/view/login.vue")
            },
            {
              name: "register",
              path: "/register",
              component: () => import("@/view/sys/register/register.vue")
            }
          ]
        },
        
        {
          path: "/500",
          name: "500",
          component: () => import("@/view/sys/error/500.vue"),
        },
        // {
        //   path: "/404",
        //   name: "404",
        //   component: () => import("@/view/sys/error/404.vue")
        // },
        // {
        //   path: "*",
        //   redirect: "/404"
        // },
      ]
  })
 
const router = createRouter()

export default router;

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
const JEHC_ROUTER_NEED_LOAD = "JEHC_ROUTER_NEED_LOAD";
let flag = 0;
router.beforeEach((to,from,next) => {  
  let isReq = true;
  let isLogin = false; 
  let isSetLoad = false;
  if (!getToken() && to.path.indexOf('/login')==-1) {   // 检测登录Token权限  
    flag = 0; 
    next({ path: "/login" })
  }else if(to.path === '/login'){
    isReq = false;//无需重新到后台请求
    isLogin = true;
    next()//表示不会再次调用router.beforeEach()进行循环      
    return  //需要进行return 否则出现死循环    
  }else{ 
    if(flag === 0 || getLocalItem(JEHC_ROUTER_NEED_LOAD) === "1"){          
      resetRouter(); 
      removeLocalItem(JEHC_ROUTER_NEED_LOAD);
      getDynamicsRouters(isReq,isSetLoad).then(res=>{      
        router.options.routes = res     
        if(null != res && res.length>0){         
          router.addRoutes(res) //动态添加路由 
          flag = 1;          
        }
        router.replace(to)  //重点代码 解决刷新页面 路由空白      
      });        
    }else{
      next()//表示不会再次调用router.beforeEach()进行循环     
    }  
  }
  setTimeout(() => {  // 每次更改路线时滚动页面至顶部
    window.scrollTo(0, 0);
  }, 100);
});

router.afterEach(() => {

});

// /**
//  * 
//  */
// export function initRouter(isReq){
//   console.log("initRouter",initRouter);  
//   resetRouter(); 
//   getDynamicsRouters(isReq).then(res=>{    
//     router.options.routes = res     
//     if(null != res && res.length>0){         
//       router.addRoutes(res) //动态添加路由 
//     }
//   });  
// }