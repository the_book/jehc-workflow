// import mx from 'mxgraph';
// const mxgraph = mx({
//   mxImageBasePath: './src/images',
//   mxBasePath: './src'
// });
// // decode bug https://github.com/jgraph/mxgraph/issues/49
// window.mxGraph = mxgraph.mxGraph;
// window.mxGraphModel = mxgraph.mxGraphModel;
// window.mxEditor = mxgraph.mxEditor;
// window.mxGeometry = mxgraph.mxGeometry;
// window.mxDefaultKeyHandler = mxgraph.mxDefaultKeyHandler;
// window.mxDefaultPopupMenu = mxgraph.mxDefaultPopupMenu;
// window.mxStylesheet = mxgraph.mxStylesheet;
// window.mxDefaultToolbar = mxgraph.mxDefaultToolbar;
// window.mxCell = mxgraph.mxCell
// window.mxConstants = mxgraph.mxConstants
// window.mxEvent = mxgraph.mxEvent
// window.mxGraph = mxgraph.mxGraph
// window.mxUtils = mxgraph.mxUtils
// window.mxCodec = mxgraph.mxCodec
// window.mxGeometry = mxgraph.mxGeometry
// window.mxEditor = mxgraph.mxEditor
// window.mxGraphHandler = mxgraph.mxGraphHandler
// window.mxRectangleShape = mxgraph.mxRectangleShape
// window.mxCellTracker = mxgraph.mxCellTracker
// window.mxClient = mxgraph.mxClient
// window.mxPerimeter = mxgraph.mxPerimeter
// window.mxEventObject = mxgraph.mxEventObject
// window.activeXObject = mxgraph.activeXObject
// export default mxgraph;


import mx from 'mxgraph';
const mxgraph = mx({
  mxImageBasePath: './src/images',
  mxBasePath: './src'
});
// decode bug https://github.com/jgraph/mxgraph/issues/49



window.mxGraph = mxgraph.mxGraph //图
window.mxGraphModel = mxgraph.mxGraphModel // 模型
window.mxEditor = mxgraph.mxEditor // 编辑器
window.mxGeometry = mxgraph.mxGeometry //几何
window.mxDefaultKeyHandler = mxgraph.mxDefaultKeyHandler // 默认键盘事件
window.mxDefaultPopupMenu = mxgraph.mxDefaultPopupMenu // 默认弹出菜单
window.mxStylesheet = mxgraph.mxStylesheet // 样式表
window.mxDefaultToolbar = mxgraph.mxDefaultToolbar // 默认工具栏
window.mxToolbar = mxgraph.mxToolbar // 工具栏
window.mxUndoManager = mxgraph.mxUndoManager //  撤销管理器
window.mxImage = mxgraph.mxImage //图片
window.mxImageShape = mxgraph.mxImageShape //图片
window.mxCell = mxgraph.mxCell // cell /图形
window.mxUtils = mxgraph.mxUtils // 实用程序
window.mxEvent = mxgraph.mxEvent // 事件
window.mxHierarchicalLayout = mxgraph.mxHierarchicalLayout // 分层布局
window.mxStencil = mxgraph.mxStencil // 模板
window.mxConstants = mxgraph.mxConstants // 常量
window.mxStencilRegistry = mxgraph.mxStencilRegistry // 模具注册表
window.mxRubberband = mxgraph.mxRubberband //画布可框选，框选设置
window.mxClipboard = mxgraph.mxClipboard  //剪切板
window.mxCodec = mxgraph.mxCodec // 编解码器
window.mxKeyHandler = mxgraph.mxKeyHandler // 键盘 键盘/事件
window.mxClient = mxgraph.mxClient  // 客户端
window.mxResources = mxgraph.mxResources // 资源
window.mxXmlRequest = mxgraph.mxXmlRequest //图像引入
window.mxXmlCanvas2D = mxgraph.mxXmlCanvas2D //2D画布
window.mxImageExport = mxgraph.mxImageExport  // 图像导出
window.mxGraphView = mxgraph.mxGraphView   // 图形视图
window.mxRectangle = mxgraph.mxRectangle // 矩形
window.mxRectangleShape = mxgraph.mxRectangleShape // 矩形形状
window.mxPoint = mxgraph.mxPoint // 点
window.mxFastOrganicLayout = mxgraph.mxFastOrganicLayout  // 快速有机布局
window.mxVertexHandler = mxgraph.mxVertexHandler  // 顶点处理程序
window.mxCellTracker = mxgraph.mxCellTracker  // cell 跟踪器
window.mxPerimeter = mxgraph.mxPerimeter  // 周长
window.mxGraphHandler = mxgraph.mxGraphHandler   // 画布处理程序
window.mxEdgeHandler = mxgraph.mxEdgeHandler    // 边缘处理器
window.mxConnectionHandler = mxgraph.mxConnectionHandler     // 连接处理程序
window.mxConstraintHandler = mxgraph.mxConstraintHandler      // 约束处理程序
window.mxShape = mxgraph.mxShape       // 形状
window.mxConnectionConstraint = mxgraph.mxConnectionConstraint        // 连接约束
window.mxPolyline = mxgraph.mxPolyline         // 多段线
window.mxEllipse = mxgraph.mxEllipse         // 多段线
window.mxOutline = mxgraph.mxOutline          // 多段线
export default mxgraph;

