import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "@/store";
import apiUtil from "@/core/util/apiUtil";
import ElementUI from 'element-ui';

Vue.config.productionTip = false;
// import KTUtil from "@/assets/js/components/util.js";
// window.KTUtil = KTUtil;

// Global 3rd party plugins
import "popper.js";
import "tooltip.js";
import PerfectScrollbar from "perfect-scrollbar";
window.PerfectScrollbar = PerfectScrollbar;
import ClipboardJS from "clipboard";
window.ClipboardJS = ClipboardJS;

// Vue 3rd party plugins
import i18n from "@/core/plugins/vue-i18n";
import vuetify from "@/core/plugins/vuetify";
import "@/core/plugins/portal-vue";
import "@/core/plugins/bootstrap-vue";
import "@/core/plugins/perfect-scrollbar";
import "@/core/plugins/highlight-js";
import "@/core/plugins/inline-svg";
import "@/core/plugins/apexcharts";
import "@/core/plugins/treeselect";
import "@/core/plugins/metronic";
import "@mdi/font/css/materialdesignicons.css";
import "@/core/plugins/formvalidation";
import {preventGlobalReClick } from "@/core/util/base"; // 引入方法
import VueQuillEditor from 'vue-quill-editor'
//引入样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css' 
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(VueQuillEditor);
Vue.use(ElementUI);

//实现quill-editor编辑器拖拽上传图片
import * as Quill from 'quill'
import { ImageDrop } from 'quill-image-drop-module'
Quill.register('modules/imageDrop', ImageDrop);

//实现quill-editor编辑器调整图片尺寸
import ImageResize from 'quill-image-resize-module'
Quill.register('modules/imageResize', ImageResize);
// API service init
apiUtil.init();

// import 'umy-ui/lib/theme-chalk/index.css';// 引入样式（禁止使用 否则样式被覆盖）
import { UTable, UTableColumn } from 'umy-ui';
Vue.component(UTable.name, UTable);
Vue.component(UTableColumn.name, UTableColumn);

//全局引入echarts
import * as echarts from 'echarts';
//需要挂载到Vue原型上
Vue.prototype.$echarts = echarts

//放置路由模块拦截
// router.beforeEach((to, from, next) => {
  // // Ensure we checked auth before each page load.
  // Promise.all([store.dispatch(VERIFY_AUTH)]).then(next);

  // // reset config to initial state
  // store.dispatch(RESET_LAYOUT_CONFIG);

  // // Scroll page to top on every route change
  // setTimeout(() => {
  //   window.scrollTo(0, 0);
  // }, 100);
// });

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount("#app");
