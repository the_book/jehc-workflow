//公共js
import Vue from 'vue'
// 防止按钮重复点击
const preventGlobalReClick = Vue.directive('preventReClick', {
  inserted: function (el, binding) {
      el.addEventListener('click', () => {
          if (!el.disabled) {
              el.disabled = true
              setTimeout(() => {
                  el.disabled = false
              }, binding.value || 5000)
          }
      })
  }
});
export { preventGlobalReClick }