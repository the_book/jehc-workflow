//返回
function goback(){
    tlocation(base_html_redirect+'/oauth/oauth-account-type/oauth-account-type-list.html');
}
$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});
//保存
function updateOauthAccountType(){
    submitBForm('defaultForm',oauthModules+'/oauthAccountType/update',base_html_redirect+'/oauth/oauth-account-type/oauth-account-type-list.html',null,"PUT");
}
$(document).ready(function(){
    var account_type_id = GetQueryString("account_type_id");
    //加载表单数据
    ajaxBRequestCallFn(oauthModules+'/oauthAccountType/get/'+account_type_id,{},function(result){
        $('#account_type_id').val(result.data.account_type_id);
        $('#title').val(result.data.title);
        $('#keyname').val(result.data.keyname);
        $('#content').val(result.data.content);
        $('#status').val(result.data.status);
        initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname",result.data.sys_mode_id);
        initComboData("account_type_id",oauthModules+"/oauthAccountType/listAll","account_type_id","title",result.data.account_type_id);
    });
});