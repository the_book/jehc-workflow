
// function beforeClick(treeId, treeNode) {
//     if (treeNode.level == 0 ) {
//         var zTree = $.fn.zTree.getZTreeObj("tree");
//         zTree.expandNode(treeNode);
//         return false;
//     }
//     return true;
// }


//加载数据完成事件
// function onAsyncSuccess(event, treeId, treeNode, msg){
//     // var length = $('#keyword').val().length;
//     // if(length > 0){
//     // 	var zTree = $.fn.zTree.getZTreeObj(treeId);
//     //     var nodeList = zTree.getNodesByParamFuzzy("name", $('#keyword').val());
//     //     //将找到的nodelist节点更新至Ztree内
//     //     $.fn.zTree.init($("#"+treeId), setting, nodeList);
//     // }
//     // closeWating(null,dialogWating);
// }

//单击事件
function onClick(event, treeId, treeNode, msg){
}

// /**
//  * 搜索树，显示并展示
//  * @param treeId
//  * @param text文本框的id
//  */
// function filter(){
//     InitztData();
// }

//删除
function delXtFunctioninfo(id){
    // var zTree = $.fn.zTree.getZTreeObj("tree"),
    //     nodes = zTree.getSelectedNodes();
    // if (nodes.length != 1) {
    //     toastrBoot(4,"必须选择一条记录进行删除");
    //     return;
    // }
    // if(nodes[0].tempObject != 'Function'){
    //     toastrBoot(4,"选择的记录必须为功能才能删除");
    //     return;
    // }
    // var params = {xt_functioninfo_id:nodes[0].id, _method:'DELETE'};
    var params = {function_info_id:id, _method:'DELETE'};
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        ajaxBRequestCallFn(oauthModules+'/oauthFunctionInfo/delete',params,function(result){
            try {
                if(typeof(result.success) != "undefined"){
                    if(result.success){
                        window.parent.toastrBoot(3,result.message);
                    }else{
                        window.parent.toastrBoot(4,result.message);
                    }
                    initTreeTable();
                }
            } catch (e) {

            }
        },null,"DELETE");
    })
}



/**
 * 下拉框变化筛选菜单树
 * @param thiz
 */
function doSysModeEvent(thiz) {
    var setting = {
        view: {
            // showLine: true,
            // showIcon: true,
            selectedMulti: false,
            dblClickExpand: true
        },
        data:{
            //必须使用data
            simpleData:{
                enable:true,
                idKey:"id",//id编号命名 默认
                pIdKey:"pId",//父id编号命名 默认
                rootPId:0 //用于修正根节点父节点数据，即 pIdKey 指定的属性值
            }
        },
        edit:{
            enable:false
        },
        callback:{
            // beforeClick: beforeClick,
            onClick:onClick//,//单击事件
            // onAsyncSuccess:onAsyncSuccess//加载数据完成事件
        }
    };

    var zTreeNodes;
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    var parm = {"sysmode_id":thiz.value};
    ajaxBRequestCallFn(oauthModules+'/oauthResources/bZTree',parm,function(result){
        zTreeNodes = eval("(" + result.data + ")");
        if(undefined == result.data || null == result.data || result.data == "" || result.data == "[]"){
            treeObj = $.fn.zTree.init($("#menu"), setting,[]);
            $(".ztree").append("暂无数据");
        }else{
            treeObj = $.fn.zTree.init($("#menu"), setting,zTreeNodes);
        }

        // treeObj.expandAll(true);

        // var t = $("#menu");
        // t.hover(function () {
        //     if (!t.hasClass("showIcon")) {
        //         t.addClass("showIcon");
        //     }
        // }, function() {
        //     t.removeClass("showIcon");
        // });
        closeWating(null,dialogWating);
    },null,"GET");
}

/////////////////////模块选择器开始///////////////////
function xtMenuinfoSelect(flag){
    $('#flag').val(flag);
    $('#xtMenuinfoBody').height(300);
    var $modal_dialog = $("#xtMenuinfoSelectDialog");
    $modal_dialog.css({'width':'500px'});
    $('#xtMenuinfoSelectModal').modal({"backdrop":"static"});
    treeObj = $.fn.zTree.init($("#menu"), null,[]);
    $(".ztree").append("暂无数据");
    initComboData("sysmode_id_",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname");

}

//单击事件
function onClick(event, treeId, treeNode, msg){

}

function doXtMenuinfoSelect(){
    var zTree = $.fn.zTree.getZTreeObj("menu"),
        nodes = zTree.getSelectedNodes();
    if (nodes.length != 1) {
        toastrBoot(4,"请选择一条隶属模块信息");
        return;
    }
    msgTishCallFnBoot("确定要选择【<font color=red>"+nodes[0].name+"</font>】？",function(){
        if($('#flag').val() == 1){
            $("#addXtFunctioninfoForm").find('#resources_title').val(nodes[0].name);
            $("#addXtFunctioninfoForm").find('#menu_id_').val(nodes[0].id);
        }else if($('#flag').val() == 2){
            $("#updateXtFunctioninfoForm").find('#resources_title').val(nodes[0].name);
            $("#updateXtFunctioninfoForm").find('#menu_id_').val(nodes[0].id);
        }
        $('#xtMenuinfoSelectModal').modal('hide');
    })
}
/////////////////////模块选择器结束///////////////////

$(function () {
    initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname");
    initTreeTable();
})

function doThisEvent(thiz){
    initTreeTable();
}

function initTreeTable(){
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    $.ajax({
        url:oauthModules+'/oauthFunctionInfo/treeList?sysmode_id='+$('#sysmode_id').val()+'&resources_module_id='+$("#resources_module_id").val(),
        type:"GET",
        dataType:"json",
        success:function(data) {
            var $table = $("#table");
            // console.log(data);
            data = eval("(" + data.data + ")");
            // console.log(data);
            $table.bootstrapTable('destroy').bootstrapTable({
                data:data,
                striped:true,
                class:'table table-hover table-bordered',
                sidePagination:"client",//表示服务端请求
                pagination:false,
                treeView:true,
                treeId:"id",
                treeField:"name",
                height: $(window).height() - 200 ,
                sortable:false,//是否启用排序
                columns:[
                    /*
                    {
                        field: 'ck',
                        checkbox:true
                    },
                    */
                    {
                        field:'name',
                        title:'名称'
                    },
                    {
                        field:'tempObject',
                        title:'性质',
                        formatter:'typeFormatter'
                    },
                    {
                        field:'integerappend',
                        title:'数据权限',
                        formatter:'authorFormatter'
                    },
                    {
                        field:'integerappend',
                        title:'拦截类型',
                        formatter:'authorTypeFormatter'
                    },
                    {
                        field:'buessid',
                        title:'操作',
                        formatter:'btnFormatter'
                    }
                ],
                onLoadSuccess:function(data){

                }
            });
            closeWating(null,dialogWating);
            if( $("#expandTreeState").val() == 1){
                expandTree();
            }
            if( $("#expandTreeState").val() == 0){
                collapseTree();
            }
        }
    });
}

/**
 *
 * @param value
 * @param row
 * @param index
 */
function typeFormatter(value, row, index){
    var tempObject = row.tempObject;
    if(tempObject == 'Sources'){
        return "<span class='label label-lg font-weight-bold label-light-primary label-inline'>资源</span>";
    }
    if(tempObject == 'Function'){
        return "<span class='label label-lg font-weight-bold label-light-info label-inline'>功能</span>";
    }
}

/**
 * 数据权限
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
function authorFormatter(value, row, index){
    var integerappend = row.integerappend;
    if(integerappend != null && integerappend != ""){
        var val = integerappend.split(",");
        if(val[0] == 0){
            return "<font color='red'>是</font>";
        }else if(val[0] == 1){
            return "否";
        }
    }
}

/**
 * 拦截类型
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
function authorTypeFormatter(value, row, index){
    var val = value.split(",");
    if(val[1] == 0){
        return "<span class='label label-lg font-weight-bold label-light-success label-inline'>无需拦截</span>";
    }else if(val[1] == 1){
        return "<span class='label label-lg font-weight-bold label-light-danger label-inline'>必须拦截</span>"
    }
}

/**
 * 格式化按钮
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
function btnFormatter(value, row, index){
    var tempObject = row.tempObject;
    var name = row.name;
    var fileName = "功能-"+row.content+"-"+value;
    var content = row.content;
    if(tempObject == 'Sources'){
        // return '<a href=javascript:addXtMenuinfo("'+value+'","'+content+'") class="btn btn-secondary m-btn m-btn--custom m-btn--icon" title="配置其菜单资源下的功能">添加功能</a>';
        return '';
    }
    if(tempObject == 'Function'){
        var btn = '<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="导出功能" href=javascript:exportOauthFunctionInfo("'+value+'","'+fileName+'")><i class="flaticon-download"></i></a>';
        return btn + '<a href=javascript:updateXtFunctioninfo("'+value+'") title="编 辑"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" ><i class="la la-pencil"></i></a><a href=javascript:delXtFunctioninfo("'+value+'")   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="删 除"><i class="la la-remove"></i></a>';
    }
}

function expandTree(){
    $("#expandTreeState").val("1");
    $('#table').bootstrapTable("expandAllTree")
}
function collapseTree(){
    $("#expandTreeState").val("0");
    $('#table').bootstrapTable("collapseAllTree")
}


/**
 * 导出功能
 * @param function_info_id
 * @param fileName
 */
function exportOauthFunctionInfo(function_info_id,fileName) {
    msgTishCallFnBoot("确定要导出该功能？",function(){
        var url = oauthModules+'/oauthFunctionInfo/export?function_info_id='+function_info_id; // 请求接口
        var method = "GET"; // 请求方式 GET或POST
        fileName = fileName+".json"; // 下载后的文件名称
        var params = "name='test" // 后台接口需要的参数
        downloadFileCallFn(url, method, fileName,params);
    });
}


/**
 * 导入功能
 */
var oauthFunctionInfoFileUploadResultArray = [];
function importOauthFunctionInfo() {
    $('#oauthFunctionInfoFileModal').modal({backdrop: 'static', keyboard: false});
    var importOauthFunctionInfoModalCount = 0 ;
    $('#oauthFunctionInfoFileModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++importOauthFunctionInfoModalCount == 1) {
            oauthFunctionInfoFileUploadResultArray.push('0');
            //使用bootstrap-fileinput渲染
            $('#oauthFunctionInfoFile').fileinput({
                language:'zh',//设置语言
                uploadUrl:oauthModules+'/oauthFunctionInfo/import',//上传的地址
                type: 'POST',
                contentType: "application/json;charset=utf-8",
                enctype: 'multipart/form-data',
                allowedFileExtensions:['json','txt'],//接收的文件后缀
                theme:'fa',//主题设置
                dropZoneTitle:'可以将文件拖放到这里',
                autoReplace: true,//布尔值，当上传文件达到maxFileCount限制并且一些新的文件被选择后，是否自动替换预览中的文件。如果maxFileCount值有效，那么这个参数会起作用。默认为false
                overwriteInitial: true,//布尔值，是否要覆盖初始预览内容和标题设置。默认为true，因此当新文件上传或文件被清除时任何initialPreview内容集将被覆盖。将其设置为false将有助于始终显示从数据库中保存的图像或文件，尤其是在使用多文件上传功能时尤其有用。
                uploadAsync:false,//默认异步上传
                showPreview:true,//是否显示预览
                // showUpload: true,//是否显示上传按钮
                showRemove: true,//显示移除按钮
                showCancel:true,//是否显示文件上传取消按钮。默认为true。只有在AJAX上传过程中，才会启用和显示
                showCaption: true,//是否显示文件标题，默认为true
                browseClass: "btn btn-light-primary font-weight-bold mr-2", //文件选择器/浏览按钮的CSS类。默认为btn btn-primary
                dropZoneEnabled: true,//是否显示拖拽区域
                validateInitialCount: true,
                maxFileSize: 0,//最大上传文件数限制，单位为kb，如果为0表示不限制文件大小
                minFileCount: 1, //每次上传允许的最少文件数。如果设置为0，则表示文件数是可选的。默认为0
                maxFileCount: 0, //每次上传允许的最大文件数。如果设置为0，则表示允许的文件数是无限制的。默认为0
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",//当检测到用于预览的不可读文件类型时，将在每个预览文件缩略图中显示的图标。默认为<i class="glyphicon glyphicon-file"></i>
                previewFileIconSettings: {
                    'docx': '<i ass="fa fa-file-word-o text-primary"></i>',
                    'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                    'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                    'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                    'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                    'pdf': '<i class="fa fa-file-archive-o text-muted"></i>',
                    'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                },
                uploadExtraData: function () {
                    return {
                        name: 'test'
                    }; //上传时额外附加参数
                },
                // msgSizeTooLarge: "文件 '{name}' (<b>{size} KB</b>) 超过了允许大小 <b>{maxSize} KB</b>.",
                msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",//字符串，当文件数超过设置的最大计数时显示的消息 maxFileCount。默认为：选择上传的文件数（{n}）超出了允许的最大限制{m}。请重试您的上传！
                // elErrorContainer:'#kartik-file-errors',
                ajaxSettings:{
                    headers:{
                        "token":getToken()
                    }
                }
            }).on('change',function () {
                // 清除掉上次上传的图片
                $(".uploadPreview").find(".file-preview-frame:first").remove();
                $(".uploadPreview").find(".kv-zoom-cache:first").remove();
            }).on('fileuploaded',function (event,data,previewId,index) {//异步上传成功处理
                console.log('单个上传成功，并清空上传控件内容',data,previewId,index);
                for(var i = 0; i < oauthFunctionInfoFileUploadResultArray.length; i++){
                    if(i == 0){
                        //清除文件输入 此方法清除所有未上传文件的预览，清除ajax文件堆栈，还清除本机文件输入
                        $('#oauthFunctionInfoFile').fileinput('clear');
                        $('#oauthFunctionInfoFile').fileinput('clear').fileinput('disable');
                        window.parent.toastrBoot(3,data.response.message);
                        //关闭文件上传的模态对话框
                        $('#oauthFunctionInfoFileModal').modal('hide');
                        //重新刷新bootstrap-table数据
                        search('datatables')
                        oauthFunctionInfoFileUploadResultArray.splice(0,oauthFunctionInfoFileUploadResultArray.length);
                        i--;
                        break;
                    }
                }
            }).on('fileerror',function (event,data,msg) { //异步上传失败处理
                console.log('单个上传失败，并清空上传控件内容',data,msg);
                window.parent.toastrBoot(4,"上传文件失败！");
            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
                console.log('批量上传成功，并清空上传控件内容',data,previewId,index);
                //清除文件输入 此方法清除所有未上传文件的预览，清除ajax文件堆栈，还清除本机文件输入
                $('#oauthFunctionInfoFile').fileinput('clear');
                $('#oauthFunctionInfoFile').fileinput('clear').fileinput('disable');
                window.parent.toastrBoot(3,data.response.message);
                //关闭文件上传的模态对话框
                $('#oauthFunctionInfoFileModal').modal('hide');
                //重新刷新bootstrap-table数据
                search('datatables')
            }).on('filebatchuploaderror', function(event, data, msg) {
                //批量上传失败，并清空上传控件内容
                console.log('批量上传失败，并清空上传控件内容',data,msg);
            }).on("filebatchselected", function (event, data, previewId, index) {
                console.log('选择文件',data,previewId,index);
            })/*.on('filepreajax', function(event, previewId, index) {
                var container = $("#divId");
                var oauthFunctionInfoDiv = container.find('.kv-upload-progress');
                oauthFunctionInfoDiv.hide();
                $('#oauthFunctionInfoFile').fileinput('enable');
                return false;
            })*/;
        }
    });
}

/**
 * 关闭窗体
 */
function closeOauthFunctionInfoFileWin() {
    $("#oauthFunctionInfoFile").fileinput('reset'); //重置上传控件（清空已文件）
    $('#oauthFunctionInfoFile').fileinput('clear');
    //关闭上传窗口
    $('#oauthFunctionInfoFileModal').modal('hide');
}