//返回r
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-sys-mode/oauth-sys-mode-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateOauthSysMode(){
	submitBForm('defaultForm',oauthModules+'/oauthSysMode/update',base_html_redirect+'/oauth/oauth-sys-mode/oauth-sys-mode-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var sys_mode_id = GetQueryString("sys_mode_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthSysMode/get/'+sys_mode_id,{},function(result){
		$('#sys_mode_id').val(result.data.sys_mode_id);
		$('#sysmode').val(result.data.sysmode);
		$('#sysname').val(result.data.sysname);
		$('#sys_mode_status').val(result.data.sys_mode_status);
        $('#sys_mode_icon').val(result.data.sys_mode_icon);
        $('#sys_mode_url').val(result.data.sys_mode_url);
        $('#sort').val(result.data.sort);
        initComboData("key_info_id",oauthModules+"/oauthKeyInfo/listAll","key_info_id","title",result.data.key_info_id);
	});
});
