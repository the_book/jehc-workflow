function updateXtMenuinfo(value){
    $('#updateXtMenuinfoForm')[0].reset();
    $('#updateXtMenuinfoForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    $.ajax({
        type:"GET",
        url:oauthModules+'/oauthResources/get/'+value,
        success: function(result){
            result = result.data;
            $("#updateXtMenuinfoForm").find("#resources_id").val(result.resources_id);
            $("#updateXtMenuinfoForm").find("#resources_title").val(result.resources_title);
            $("#updateXtMenuinfoForm").find("#resources_parentid_").val(result.resources_parentid);
            $("#updateXtMenuinfoForm").find("#resources_parentidTitle_").val(result.resources_parentidTitle_);
            $("#updateXtMenuinfoForm").find("#resources_leaf").val(result.resources_leaf);
            $("#updateXtMenuinfoForm").find("#resources_sys").val(result.resources_sys);
            $("#updateXtMenuinfoForm").find("#resources_iconCls").val(result.resources_iconCls);
            $("#updateXtMenuinfoForm").find("#resources_url").val(result.resources_url);
            $("#updateXtMenuinfoForm").find("#resources_sort").val(result.resources_sort);
            $("#updateXtMenuinfoForm").find("#resources_status").val(result.resources_status);
            $("#updateXtMenuinfoForm").find("#resources_module_idTemp").val(result.resources_module_id);

            $("#updateXtMenuinfoForm").find("#component_router").val(result.component_router);
            $("#updateXtMenuinfoForm").find("#component_icon").val(result.component_icon);
            $("#updateXtMenuinfoForm").find("#component_router_to").val(result.component_router_to);
            $("#updateXtMenuinfoForm").find("#component_jump_type").val(result.component_jump_type);
            $("#updateXtMenuinfoForm").find("#component_hide_menu").val(result.component_hide_menu);
            $("#updateXtMenuinfoForm").find("#component_open_new_page").val(result.component_open_new_page);
            $("#updateXtMenuinfoForm").find("#component_open_style").val(result.component_open_style);

            InitMenuModuleListSetV('resources_module_id_','resources_module_idTemp');
            $('#updateXtMenuinfoModal').modal({"backdrop":"static"});
        }
    });
}

function doUpdateXtMenuinfo(){
    submitBFormCallFn('updateXtMenuinfoForm',oauthModules+'/oauthResources/update',function(result){
        try {
            if(result.success === undefined){
                window.parent.toastrBoot(4,result.message);
                return;
            }
            window.parent.toastrBoot(3,result.message);
            search('datatables');
            $('#updateXtMenuinfoModal').modal('hide');
        } catch (e) {

        }
    },null,"PUT");
}