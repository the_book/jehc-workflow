var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,oauthModules+'/oauthRole/list?del_flag=0',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"role_id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"role_id",
                width:"50px"
            },
            {
                data:'role_name',
                width:"120px"
            },
            {
                data:'sysname',
                width:"120px"
            },
            {
                data:'role_type',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "平台权限";
                    }
                    if(data == 1){
                        return "业务权限";
                    }
                }
            },
            {
                data:"createBy",
                width:"120px",
                render:function(data, type, row, meta) {
                    if(null == data || data == ''){
                        return "<font color='#6495ed'>\</font>";
                    }
                    return data;
                }
            },
            {
                data:"create_time",
                width:"150px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"modifiedBy",
                width:"120px",
                render:function(data, type, row, meta) {
                    if(null == data || data == ''){
                        return "<font color='#6495ed'>\</font>";
                    }
                    return data;
                }
            },
            {
                data:"update_time",
                width:"150px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"role_id",
                render:function(data, type, row, meta) {
                    var role_id = row.role_id;
                    var role_name = row.role_name;
                    return '<span class="dropdown">' +
                                '<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">' +
                                    '<i class="la la-ellipsis-h"></i>' +
                                '</a>' +
                                '<div class="dropdown-menu dropdown-menu-right">' +
                                    '<button class="dropdown-item" onclick=addOauthResources("'+role_id+'","'+role_name+'")><i class="la la-edit"></i>分配资源</button>' +
                                    '<button class="dropdown-item" onclick=addXtUserinfo("'+role_id+'","'+role_name+'")><i class="fa fa-user-plus"></i>分配账户</a>' +
                                '</div>' +
                            '</span>' +
                            '<button onclick=toOauthRoleDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情">' +
                                '<i class="la la-eye""></i>' +
                            '</button>'

                    // var btn = '<button class="btn btn-default" onclick=toOauthRoleDetail("'+data+'")><i class="fa fa-gears"></i>详情</button>';
                    // btn = btn+'<button class="btn btn-default" onclick=addOauthResources("'+role_id+'","'+role_name+'")><i class="fa fa-edit"></i>分配资源</button>';
                    // btn = btn+'<button class="btn btn-default" onclick=addXtUserinfo("'+role_id+'","'+role_name+'")><i class="fa fa-user-plus"></i>分配账户</button>';


                    // return '<div class="dropdown">' +
                    //     '<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                    //         '<i class="flaticon-more"></i>' +
                    //     '</button>' +
                    //     '<div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton5">' +
                    //         '<button class="dropdown-item" onclick=toOauthRoleDetail("'+data+'")><i class="fa fa-gears"></i>详情</button>' +
                    //     '<div class="dropdown-divider"></div>' +
                    //         '<button class="dropdown-item" onclick=addOauthResources("'+role_id+'","'+role_name+'")><i class="fa fa-edit"></i>分配资源</button>' +
                    //         '<button class="dropdown-item" onclick=addXtUserinfo("'+role_id+'","'+role_name+'")><i class="fa fa-user-plus"></i>分配账户</button>' +
                    //     '</div>' +
                    //     '</div>'


                    // var role_id = row.role_id;
                    // var role_name = row.role_name;
                    // var btn = '<button class="btn btn-default" onclick=toOauthRoleDetail("'+data+'")><i class="fa fa-gears"></i>详情</button>';
                    // btn = btn+'<button class="btn btn-default" onclick=addOauthResources("'+role_id+'","'+role_name+'")><i class="fa fa-edit"></i>分配资源</button>';
                    // btn = btn+'<button class="btn btn-default" onclick=addXtUserinfo("'+role_id+'","'+role_name+'")><i class="fa fa-user-plus"></i>分配账户</button>';
                    // return btn;
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
});
//新增
function toOauthRoleAdd(){
    tlocation(base_html_redirect+'/oauth/oauth-role/oauth-role-add.html');
}
//修改
function toOauthRoleUpdate(){
    if($(".checkchild:checked").length != 1){
        toastrBoot(4,"选择数据非法");
        return;
    }
    var id = $(".checkchild:checked").val();
    tlocation(base_html_redirect+'/oauth/oauth-role/oauth-role-update.html?role_id='+id);
}
//详情
function toOauthRoleDetail(id){
    tlocation(base_html_redirect+'/oauth/oauth-role/oauth-role-detail.html?role_id='+id);
}
//删除
function delOauthRole(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要禁用的数据");
        return;
    }
    msgTishCallFnBoot("确定要禁用所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {xt_role_id:id,_method:'DELETE'};
        ajaxBReq(oauthModules+'/oauthRole/delete',params,['datatables'],null,"DELETE");
    })
}

function initListDeleted(){
    var RoleModalCount = 0 ;
    $('#RoleBody').height(reGetBodyHeight()-218);
    $('#RoleModal').modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#RoleModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++RoleModalCount == 1){
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,oauthModules+'/oauthRole/list?del_flag=1',null);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'200px',
                //列表表头字段
                colums:[
                    {
                        sClass:"text-center",
                        width:"20px",
                        data:"role_id",
                        render:function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildRole " value="' + data + '" /><span></span></label>';
                        },
                        bSortable:false
                    },
                    {
                        data:"role_id",
                        width:"40px"
                    },
                    {
                        data:'role_name'
                    },
                    {
                        data:'sysname'
                    },
                    {
                        data:"createBy",
                        render:function(data, type, row, meta) {
                            if(null == data || data == ''){
                                return "<font color='#6495ed'>\</font>";
                            }
                            return data;
                        }
                    },
                    {
                        data:"create_time",
                        width:"150px",
                        render:function(data, type, row, meta) {
                            return dateformat(data);
                        }
                    },
                    {
                        data:"modifiedBy",
                        render:function(data, type, row, meta) {
                            if(null == data || data == ''){
                                return "<font color='#6495ed'>\</font>";
                            }
                            return data;
                        }
                    },
                    {
                        data:"update_time",
                        width:"150px",
                        render:function(data, type, row, meta) {
                            return dateformat(data);
                        }
                    }
                ]
            });
            grid=$('#RoleDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallRole','checkchildRole');
            //实现单击行选中
            clickrowselected('RoleDatatables');

        }
    });
}


/**恢复操作**/
function recoverOauthRole(){
    if(returncheckedLength('checkchildRole') <= 0){
        toastrBoot(4,"请选择要恢复的角色");
        return;
    }
    msgTishCallFnBoot("确定要恢复所选择的角色？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {role_id:id};
        ajaxBRequestCallFn(oauthModules+'/oauthRole/recover',params,function(result){
            if(typeof(result.success) != "undefined"){
                window.parent.toastrBoot(3,result.message);
                $('#RoleModal').modal('hide');
                search('datatables');
            }
        },null,"PUT");
    })
}

$(document).ready(function(){
    initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname",null,"-1");
});

function doChangeRole(){
    search('datatables');
}