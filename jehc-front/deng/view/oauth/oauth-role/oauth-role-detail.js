//返回r
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-role/oauth-role-list.html');
}
$(document).ready(function(){
	var role_id = GetQueryString("role_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthRole/get/'+role_id,{},function(result){
		$('#role_id').val(result.data.role_id);
		$('#role_name').val(result.data.role_name);
        $('#r_code').val(result.data.r_code);
		$('#role_desc').val(result.data.role_desc);
		$('#role_type').val(result.data.role_type);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
		$('#sysmode_id').val(result.data.sysmode_id);
        initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname",result.data.sysmode_id);
	});
});
