//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-btn/lc-btn-list.html');
}
$(document).ready(function(){
	var btn_id = GetQueryString("btn_id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcBtn/get/'+btn_id,{},function(result){
        $('#btn_id').val(result.data.btn_id);
		$('#_label').val(result.data._label);
		$('#element_id').val(result.data.element_id);
		$('#element_name').val(result.data.element_name);
		$('#tips').val(result.data.tips);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
        $('#element_class').val(result.data.element_class);
        $('#icons').val(result.data.icons);
        $('#b_action').val(result.data.b_action);
        $('#b_script').val(result.data.b_script);
        $('#attr').val(result.data.attr);
        $('#sort_').val(result.data.sort_);
        $('#behavior').val(result.data.behavior);
	});
});
