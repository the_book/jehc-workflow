//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-btn/lc-btn-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateLcBtn(){
	submitBForm('defaultForm',workflowModules+'/lcBtn/update',base_html_redirect+'/workflow/lc-btn/lc-btn-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var btn_id = GetQueryString("btn_id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcBtn/get/'+btn_id,{},function(result){
        $('#btn_id').val(result.data.btn_id);
		$('#_label').val(result.data._label);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
		$('#create_id').val(result.data.create_id);
		$('#update_id').val(result.data.update_id);
		$('#element_id').val(result.data.element_id);
		$('#element_name').val(result.data.element_name);
		$('#tips').val(result.data.tips);
        $('#element_class').val(result.data.element_class);
        $('#icons').val(result.data.icons);
        $('#b_action').val(result.data.b_action);
        $('#b_script').val(result.data.b_script);
        $('#attr').val(result.data.attr);
        $('#sort_').val(result.data.sort_);
        $('#behavior').val(result.data.behavior);
	});
});
