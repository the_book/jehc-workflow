var chart = null;
$(function(){
    chart = echarts.init(document.getElementById('main'));
    chart.setOption({
        title : {
            text: '流程任务统计',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} {b} : {c} ({d}%)"
        },
        legend:{
            orient: 'vertical',
            left: 'left',
            data: []
        },
        series : [
            {
                name: '运行任务',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {"name":"订单审批流程","value":"11"},
                    {"name":"会签流程","value":"3"},
                    {"name":"网关流程","value":"4"},
                    {"name":"请假流程","value":"8"},
                    {"name":"测试流程","value":"50"}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    });
    initChartData();
});

$('#searchForm').bootstrapValidator({
    message:'此值不是有效的'
});
function initChartData(){
    var dialogWating = showWating("正在检索中...");
    var bootform =  $('#searchForm');
    $.ajax({
        url:workflowModules+'/lcReport/task',
        type:"POST",//PUT DELETE POST
        contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        data:JSON.stringify(bootform.serializeJSON()),//新版本写法
        success:function(result){
            closeWating(null,dialogWating);
            if(complateAuth(result)){
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            var option =  chart.getOption();
                            // var list =[
                            //     {"name":"订单审批流程","value":"51"},
                            //     {"name":"会签流程","value":"53"},
                            //     {"name":"网关流程","value":"52"},
                            //     {"name":"请假流程","value":"56"},
                            //     {"name":"测试流程","value":"50"}
                            // ];
                            option.series[0].data = result.data;
                            chart.setOption(option);
                        }else{
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            }
        },
        error:function(){
            closeWating(null,dialogWating);
        }
    })
}


