var grid;
$(document).ready(function() {
    var lc_process_id = GetQueryString("lc_process_id");
    $("#lc_process_id").val(lc_process_id);
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcDeploymentHis/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:reGetBodyHeight()*0.55+'px',
        sPaginationType:'simple_numbers',
        fixedHeader:true,//表头固定
        fixedColumns: {
            "leftColumns": 3
        },
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"id",
                width:"50px"
            },
            {
                data:"lc_deployment_his_id",
                width:"100px"
            },
            {
                data:"id",
                render:function(data, type, row, meta) {
                    var lc_process_id = row.lc_process_id;
                    var lc_deployment_his_id = row.lc_deployment_his_id;
                    var lc_process_title = row.lc_process_title;
                    var version = row.version;
                    // return '<button onclick=toDeploymentHisDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="版本详情">' +
                    //         '<i class="fa flaticon-alert-1"></i>' +
                    //     '</button>'+
                    //     '<button onclick=showDeploymentHisImage("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="流程图">' +
                    //         '<i class="la la-image"></i>' +
                    //     '</button>'+
                    //     '<button onclick=startProcessInstanceByKey("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="发起实例并增加业务Key">' +
                    //         '<i class="fa flaticon-interface-9"></i>' +
                    //     '</button>'+
                    //     '<button onclick=startProcessInstance("'+lc_deployment_his_id+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="发起实例">' +
                    //         '<i class="la la-caret-right"></i>' +
                    //     '</button>';/*+
						// 	'<button onclick=showLcDynamicFormHis("'+data+'","'+lc_process_id+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="表单详情">' +
						// 		'<i class="fa flaticon-map"></i>' +
						// 	'</button>'+
                    //         '<button onclick=initLcNodeAttribute("'+lc_deployment_his_id+'","'+lc_process_title+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="配置节点">' +
                    //             '<i class="m-menu__link-icon  icon-settings"></i></button>';*/
                    var btn = '<div class="btn-group ml-2">'+
                            '<button type="button" class="btn btn-light-primary font-weight-bold"><i class="fa fa-magic fa-lg"></i><span>操作</button>'+
                            '<button type="button" class="btn btn-light-primary font-weight-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                            '<span class="sr-only"></span>'+
                            '</button>'+
                            '<div class="dropdown-menu" style="">'+
                                '<a class="dropdown-item" href=javascript:toDeploymentHisDetail("'+data+'")>版本详情</a>'+
                                '<a class="dropdown-item" href=javascript:showDeploymentHisImage("'+data+'")>流程图</a>'+
                            '<div class="dropdown-divider"></div>'+
                                '<a class="dropdown-item" title="发起实例并增加业务Key" href="javascript:startProcessInstanceByKey('+lc_deployment_his_id+')">发起业务实例（流程自带）</a>'+
                                '<a class="dropdown-item" href=javascript:startProcessInstance("'+lc_deployment_his_id+'")>发起实例（流程自带）</a>'+
                                '<a class="dropdown-item" href=javascript:getModuleKey("'+lc_process_id+'","'+version+'")>根据自定义配置发起实例</a>'+
                            '</div>'+
                        '</div>';
                    return btn;
                }
            },
            {
                data:'lc_deployment_his_name'/*,
                render:function(data, type, row, meta) {
                    return "<font color='#6495ed'>"+data+"</font>";
                }*/
            },
            {
                data:'lc_deployment_his_time',
                render:function(data, type, row, meta) {
                    return dateformat(data);
                    /*return "<font color='#deb887'>"+dateformat(data)+"</font>";*/
                }
            },
            {
                data:'version',
                render:function(data, type, row, meta) {
                    return "<span class='label label-light-primary mr-2'>"+data+"</span>"
                }
            },
            {
                data:'lc_deployment_his_tenantId'
            },
            {
                data:'lc_deployment_his_status',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='label label-lg font-weight-bold label-light-success label-inline'>发布中</span>"
                    }
                    if(data == 1){
                        return "<span class='label label-lg font-weight-bold label-light-danger label-inline'>暂停</span>"
                    }
                    return "<span class='label label-lg font-weight-bold label-light-danger label-inline'>缺省</span>"
                }
            },
            {
                data:'lc_process_flag',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='label label-lg font-weight-bold label-light-primary label-inline'>通过平台设计器设计</span>"
                    }
                    if(data == 1){
                        return "<span class='label label-lg font-weight-bold label-light-info label-inline'>通过上传部署</span>"
                    }
                    return "<span class='label label-lg font-weight-boldlabel-light-success label-inline'>缺省</span>"
                }
            },
           /* {
                data:"name",
                width:"150px"
            },
            {
                data:"groupName",
                width:"150px"
            },*/
            {
                data:"createBy",
                render:function(data, type, row, meta) {
                    if(null == data || data == ''){
                        return "<font color='#6495ed'>\</font>";
                    }
                    return data;
                }
            },

            {
                data:"create_time",
                width:"150px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
});

//关闭流程版本或开启流程版本
function delLcDeploymentHis(flag){
    if(flag ==  0){
        if(returncheckedLength('checkchild') <= 0){
            toastrBoot(4,"请选择要发布的数据");
            return;
        }
        msgTishCallFnBoot("确定要发布所选择的数据？",function(){
            var id = returncheckIds('checkId').join(",");
            var params = {id:id,flag:flag};
            ajaxBReq(workflowModules+'/lcDeploymentHis/delete',params,['datatables'],null,"DELETE");
        });
    }else{
        if(returncheckedLength('checkchild') <= 0){
            toastrBoot(4,"请选择要暂停的数据");
            return;
        }
        msgTishCallFnBoot("确定要暂停所选择的数据？",function(){
            var id = returncheckIds('checkId').join(",");
            var params = {id:id,flag:flag};
            ajaxBReq(workflowModules+'/lcDeploymentHis/delete',params,['datatables'],null,"DELETE");
        });
    }

}

/**
 * 流程发布版本记录详情
 * @param lc_process_id
 * @param lc_process_title
 */
function toDeploymentHisDetail(id){
    $('#lcDeploymentHisDetailBody').height(reGetBodyHeight()*0.5);
    $('#lcDeploymentHisDetailModal').modal({backdrop:'static',keyboard:false});
    $('#lcDeploymentHisDetailModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcDeploymentHisDetailModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.5+'px'});
        //加载表单数据
        ajaxBRequestCallFn(workflowModules+'/lcDeploymentHis/get/'+id,{},function(result){
            $('#lc_deployment_his_id').val(result.data.lc_deployment_his_id);
            $('#lc_deployment_his_name').val(result.data.lc_deployment_his_name);
            $('#lc_deployment_his_time').val(dateformat(result.data.lc_deployment_his_time));
            $('#lc_deployment_his_tenantId').val(result.data.lc_deployment_his_tenantId);
            $('#lc_deployment_his_status').html(result.data.lc_deployment_his_status==0?"<font color='#008b8b'>【运行中】</font>":"<font color='#8b0000'>【关闭】</font>");
            $('#uid').val(result.data.uid);
            $('#uk').val(result.data.uk);
            var lc_process_flag = result.data.lc_process_flag;
            if(lc_process_flag == 0){
                $('#lc_process_flag').val("通过平台设计器设计");
            }
            if(lc_process_flag == 1){
                $('#lc_process_flag').val("通过上传部署");
            }
            $('#name').val(result.data.name);
            $('#groupName').val(result.data.groupName);
            $('#w').val(result.data.w);
            $('#h').val(result.data.h);
        });
    });
}

function showLcDynamicFormHis(lc_process_id,lc_process_title){
    $('#lcDeploymentHisPanelBody').height(reGetBodyHeight()*0.7);
    $('#lcDeploymentHisModalLabel').html("当前实例下表单---<font color=red>"+lc_process_title+"</font>");
    $('#lcDeploymentHisModal').modal({backdrop:'static',keyboard:false});
    $("#lcDeploymentHisIframe",document.body).attr("src",base_html_redirect+'/workflow/lc-deployment-his/lc-deployment-his-list.html?lc_process_id='+lc_process_id)
    $('#lcDeploymentHisModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcDeploymentHisModalDialog");
        $modal_dialog.css({'width':reGetBodyWidth()*0.55+'px'});
    });
}

/**
 * 发起实例
 * @param lc_deployment_his_id
 */
function startProcessInstance(lc_deployment_his_id){
    msgTishCallFnBoot("确定要发起实例？",function(){
        var params = {deploymentId:lc_deployment_his_id};
        ajaxBReq(workflowModules+'/lcProcess/startProcessInstance',params,['datatables'],null,"POST");
    });
}

/**
 * 流程图
 * @param id
 */
function showDeploymentHisImage(id) {
    var ImageModalCount = 0 ;
    $('#lcDeploymentHisImagePanelBody').height(reGetBodyHeight()*0.5);
    $('#lcDeploymentHisImageModal').modal({backdrop:'static',keyboard:false});
    $('#lcDeploymentHisImageModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcDeploymentHisImageModalDialog");
        $modal_dialog.css({'width':reGetBodyWidth()*0.55+'px'});
        if(++ImageModalCount == 1) {
            //加载表单数据
            ajaxBRequestCallFn(workflowModules+'/lcDeploymentHis/image/base64/'+id,{},function(result){
                $('#imagePath').attr("src", result.data);
            });
        }
    });
}


function closeWin() {
    search('datatables');
}

/**
 * 根据业务Key发起流程实例
 * @param lc_deployment_his_id
 */
function startProcessInstanceByKey(lc_deployment_his_id) {
    $('#businessKeyPanelBody').height(reGetBodyHeight()*0.5);
    $('#businessKeyModal').modal({backdrop:'static',keyboard:false});
    $('#businessKeyModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#businessKeyModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.5+'px'});
        $('#businessKeyForm')[0].reset();
        $("#lc_deployment_his_id_").val(lc_deployment_his_id);
    });
}

/**
 *
 */
function doStartProcessInstanceByKey() {
    msgTishCallFnBoot("确定要发起实例？",function(){
        var params = {deploymentId:$("#lc_deployment_his_id_").val(),businessKey:$("#businessKey").val()};
        ajaxBRequestCallFn(workflowModules+'/lcProcess/startProcessInstance',params,function (result) {
            if(typeof(result.success) != "undefined"){
                window.parent.toastrBoot(3,result.message);
                $('#businessKeyModal').modal('hide');
            }
        },null,"POST");
    });
}