//发起流程实例
var lcAttributeEntities = [];

/**
 *
 * @param id
 */
function getModuleKey(lc_process_id,version) {
    ajaxBRequestCallFn(workflowModules+'/lcProcess/get/'+lc_process_id,{},function(result){
        showDynamicActiviImpl(result.data.moduleKey,version)
    });
}
/**
 *
 * @param taskId
 */
function showDynamicActiviImpl(moduleKey,version) {
    $('#dynamicActiviImplForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    $('#dynamicActiviImplPanelBody').height(reGetBodyHeight()*0.5);
    $('#dynamicActiviImplModal').modal({backdrop:'static',keyboard:false});
    var dynamicActiviImplCount = 0 ;
    $('#dynamicActiviImplModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#dynamicActiviImplModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.6+'px'});
        $('#dynamicActiviImplForm')[0].reset();
        if(++dynamicActiviImplCount == 1){
            $("#moduleKey").val(moduleKey);
            $("#version").val(version);
            /*$("#comment").val("");
            $("#opinion").val("");*/
            $("#mutilValue").val("");
            $("#behavior").val("");
            /*$("#lcJumpRules").val("");
            $("#nodeBtnId").val("");*/
            resetUsersBl();
            // $("#activityIdDIV").hide();
            // $("#mutilValueDIV").hide();
            ajaxBRequestCallFn(workflowModules+'/lcProcess/attribute/'+moduleKey+'/'+version,{},function(result){
                result = result.data;
                /*$("#opinion").val(result.opinion);*/
                lcAttributeEntities = result.lcAttributeEntities;
                if(undefined != result){
                    $("#mutilValueDIV").show();
                    changeType();
                    if(null != lcAttributeEntities){
                        filterBtns(lcAttributeEntities);
                    }
                }
            });
        }
    });
}

/**
 * 筛选按钮
 * @param lcAttributeEntities
 */
function filterBtns(lcAttributeEntities) {
    // $("#btnModules").html("");
    // $("#nodeBtnId").html("");
    // var str = "<option value=''>请选择</option>";
    // $("#nodeBtnId").append(str);
    // if(undefined != lcAttributeEntities && null != lcAttributeEntities && '' != lcAttributeEntities){
    //     $.each(lcAttributeEntities, function(i, item){
    //         var str = "<option value=" + item.nodeBtnId + ">" + item._label + "</option>";
    //         $("#nodeBtnId").append(str);
    //     });
    // }

    var complated = $("#complated").val();
    if(undefined != lcAttributeEntities && null != lcAttributeEntities && '' != lcAttributeEntities){
        $.each(lcAttributeEntities, function(i, item){
        });
    }
}

/**
 * 统一处理任务
 * @param action
 * @param nodeBtnId
 */
function doTaskAction() {
    /*var opinion = $("#opinion").val();
    var nodeBtnId =  $("#nodeBtnId").val();
    var comment = $("#comment").val();
    if(null == nodeBtnId || "" == nodeBtnId){
        toastrBoot(4,"请选择审批类型");
        return;
    }
    if(opinion == 20 && (comment == null ||  comment == "")){
        toastrBoot(4,"请输入审批意见");
        return;
    }*/
    submitBFormCallFn('dynamicActiviImplForm',workflowModules+"/lcProcess/module/start",function(result){
        try{
            window.parent.toastrBoot(3,result.message);
            $('#dynamicActiviImplModal').modal('hide');
            search('datatables');
        }catch(e){
            window.parent.toastrBoot(4,result.message);
        }
    });
}

/**
 * 联动节点属性
 * @param thiz
 */
// function changeAttribute(thiz) {
//     var nodeBtnId = thiz.value;
//     if(null != nodeBtnId && '' != nodeBtnId){
//         $("#activityIdDIV").show();
//         $("#mutilValueDIV").show();
//     }else{
//         $("#activityIdDIV").hide();
//         $("#mutilValueDIV").hide();
//     }
//     var attributeArray =[];
//     if(null != lcAttributeEntities){
//         $.each(lcAttributeEntities, function(i, item){
//             var btnId = item.nodeBtnId;
//             if(nodeBtnId == btnId){
//                 attributeArray.push(item);
//             }
//         });
//     }
//     if(null != attributeArray){//设置下拉框目标节点
//         $("#lcJumpRules").html("");
//         // var str = "<option value=''>请选择</option>";
//         var str = "";
//         $.each(attributeArray, function(i, item){
//             if(null != item.activityId && '' != item.activityId){
//                 $("#activityIdDIV").show();
//                 str += "<option value=" + item.activityId + ">" + item.activityName + "</option>";
//                 $("#lcJumpRules").append(str);
//             }else{
//                 $("#activityIdDIV").hide();
//             }
//             $("#behavior").val(item.behavior);
//         });
//     }
//     var targetId= $("#lcJumpRules").val();
//     if(null != targetId && '' != targetId){
//         $.each(attributeArray, function(i, item){
//             if(null != item.activityId && '' != item.activityId && targetId == item.activityId && item.end == true){
//                 $("#mutilValueDIV").hide();
//                 resetUsersBl();//清空已选办理人
//             }
//         });
//     }
//     var behavior = $("#behavior").val();
//     if(behavior == 20 || behavior == 40 || behavior == 50 || behavior == 70 || behavior == 80 || behavior == 100 || behavior == 110 || behavior == 120 || behavior == 130){
//         /**20驳回40撤回50终止流程70转办80委派90加签100催办110设置归属人120挂起流程130激活流程无需处理人**/
//         $("#mutilValueDIV").hide();
//         resetUsersBl();//清空已选办理人
//     }
//     if(behavior == 90){//当前节点加签则办理人名称更为加签人
//         $("#mutilValueLabel").html("当前节点加签人")
//     }else{
//         $("#mutilValueLabel").html("下个节点处理人")
//     }
// }

/**
 *
 * @param thiz
 */
function changeType() {
    var complated = $("#complated").val();

    if(complated == 10){
        $("#mutilValueLabel").html("下个节点处理人")
    }
    if(complated == 20){
        $("#mutilValueLabel").html("当前节点处理人")
    }
}
/**
 * 办理任务用户
 * @param uuid
 * @param mutil
 */
function initUsersBl(){
    $('#usersBlPanelBody').height(reGetBodyHeight()-216);
    $('#usersBlModal').modal({backdrop:'static',keyboard:false});
    $('#usersBlModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#usersBlModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        $('#searchFormusersBl')[0].reset();
        var opt = {
            searchformId:'searchFormusersBl'
        };
        var options = DataTablesPaging.pagingOptions({
            ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/list',opt);},//渲染数据
            //在第一位置追加序列号
            fnRowCallback:function(nRow, aData, iDisplayIndex){
                jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                return nRow;
            },
            order:[],//取消默认排序查询,否则复选框一列会出现小箭头
            tableHeight:'150px',
            //列表表头字段
            colums:[
                {
                    sClass:"text-center",
                    width:"50px",
                    data:"xt_userinfo_id",
                    render:function (data, type, full, meta) {
                        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildusersBl" value="' + data + '" /><span></span></label>';
                    },
                    bSortable:false
                },
                {
                    data:"xt_userinfo_id",
                    width:"50px"
                },
                {
                    data:'xt_userinfo_name'
                },
                {
                    data:'xt_userinfo_realName'
                },
                {
                    data:'xt_userinfo_phone'
                },
                {
                    data:'xt_userinfo_origo'
                },
                {
                    data:'xt_userinfo_birthday'
                },
                {
                    data:'xt_userinfo_email'
                }
            ]
        });
        grid=$('#usersBlDatatables').dataTable(options);
        //实现全选反选
        docheckboxall('checkallusersBl','checkchildusersBl');
        //实现单击行选中
        clickrowselected('usersDatatablesBl');
        //实现单击操作开始
        // var table = $('#usersBlDatatables').DataTable();
        // $('#usersBlDatatables tbody').on('click', 'tr', function () {
        //     var data = table.row( this ).data();
        //     console.info(data.xt_userinfo_realName);
        // });
        //实现单击操作结束
    });
}


/**
 * 办理任务用户选择器
 */
function doSelectUsersBl() {
    var behavior = $("#behavior").val();
    var mutil = 10;
    if(behavior == 90){//当前节点加签则办理人名称更为加签人
        mutil = 20;
    }else{
        //说明是下个节点办理人，需要判断下个节点是否会签节点
        var lcJumpRules = $("#lcJumpRules").val();
        if(null != lcAttributeEntities){
            for(var i in lcAttributeEntities){
                var activityId = lcAttributeEntities[i].activityId;
                if(activityId == lcJumpRules){
                    mutil =lcAttributeEntities[i].mutil;
                    console.log("mutil",mutil);
                    break;
                }
            }
        }
    }
    if(returncheckedLength('checkchildusersBl') <= 0){
        toastrBoot(4,"请选择节点办理人");
        return;
    }
    var id = returncheckIds('checkchildusersBl').join(",");

    if(mutil != 20){//非会签节点
        if(returncheckedLength('checkchildusersBl') > 1){
            toastrBoot(4,"非会签节点不能选择多个办理人");
            return;
        }
    }
    var params = {xt_userinfo_id:id};
    //加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtUserinfo/users",params,function(result){
        var arr = result.data;
        var xt_userinfo_realName = null;
        if(undefined != result.data && null != result.data){
            for (var i = 0; i < arr.length; i++) {
                //名称维护
                if (null == xt_userinfo_realName) {
                    xt_userinfo_realName =arr[i].xt_userinfo_realName;
                } else {
                    xt_userinfo_realName = xt_userinfo_realName + "," + arr[i].xt_userinfo_realName;
                }
            }
        }
        if(null != xt_userinfo_realName){
            msgTishCallFnBoot('确定要选择:<br>'+ xt_userinfo_realName + '？',function(){
                $('#mutilValue').val(id);
                $('#mutilValueText').val(xt_userinfo_realName);
                $('#usersBlModal').modal('hide');
            });
        }else{
            $('#usersBlModal').modal('hide');
        }

    },null,"post");
}

function resetUsersBl() {
    $('#mutilValue').val("");
    $('#mutilValueText').val("");
}

