var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcForm/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
		tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"lc_form_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:'lc_form_id'
			},
			{
				data:'form_key',
                render:function(data, type, row, meta) {
                    return "<font style='color: #3699ff;'>"+data+"</font>";
                }
			},
            {
                data:'title'
            },
			{
				data:'productName'
			},
			{
				data:'groupName'
			},
			{
				data:'createBy'
			},
			{
				data:'modifiedBy'
			},
			{
				data:'create_time',
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>"+data+"</span>"
                }
			},
			{
				data:'update_time'
			},
			{
				data:"lc_form_id",
				width:"150px",
				render:function(data, type, row, meta) {
					var title =  row.title;
                    var btn = '<button onclick=showLcFormField("'+data+'","'+title+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="表单字段">' +
                    	'<i class="la la-bars"></i>' +
                    '</button>';
					return btn+"<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' onclick=\"javascript:toLcFormDetail('"+ data +"')\"><i class='la la-eye'></i></button>";
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});
//新增
function toLcFormAdd(){
	tlocation(base_html_redirect+'/workflow/lc-form/lc-form-add.html');
}
//修改
function toLcFormUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/workflow/lc-form/lc-form-update.html?lc_form_id='+id);
}
//详情
function toLcFormDetail(id){
	tlocation(base_html_redirect+'/workflow/lc-form/lc-form-detail.html?lc_form_id='+id);
}
//删除
function delLcForm(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {lc_form_id:id,_method:'DELETE'};
		ajaxBReq(workflowModules+'/lcForm/delete',params,['datatables'],null,"DELETE");
	})
}


/**
 * 表单字段
 * @param lc_form_id 表单id
 * @param title 标题
 */
function showLcFormField(lc_form_id,title) {
    $('#lcFormFieldPanelBody').height(reGetBodyHeight()-128);
    $('#lcFormFieldModalLabel').html("表单字段【<font color=red>"+title+"</font>】");
    $('#lcFormFieldModal').modal({backdrop:'static',keyboard:false});
    $('#lcFormFieldModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcFormFieldModalDialog");
        $("#lcFormFieldIframe",document.body).attr("src",base_html_redirect+'/workflow/lc-form-field/lc-form-field-list.html?lc_form_id='+lc_form_id);
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
    });
}