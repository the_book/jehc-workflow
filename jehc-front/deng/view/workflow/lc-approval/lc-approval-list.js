var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcApproval/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"lcApprovalId",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
            {
                data:"lcApprovalId",
                width:"50px"
            },
            {
                data: 'behavior',
                render: function (data, type, full, meta) {
                    return formatBehavior(data)
                }
            },
            {
                data:'comment'
            },
            {
                data:'create_time'
            },
            {
                data: 'createBy'
            }
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
	
});
/**删除操作**/
function delLcApproval(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {lcApprovalId:id};
		ajaxBReq(workflowModules+'/lcApproval/delete',params,['datatables']);
	})
}


/**
 * 格式化行为
 * @param data
 */
function formatBehavior(data) {
    if(data == 0){
        return "提交";
    }
    if(data == 10){
        return "通过";
    }
    if(data == 20){
        return "驳回";
    }
    if(data == 30){
        return "弃权";
    }
    if(data == 40){
        return "撤回";
    }
    if(data == 50){
        return "终止流程";
    }
    if(data == 60){
        return "执行跳转";
    }
    if(data == 70){
        return "转办";
    }
    if(data == 80){
        return "委派";
    }
    if(data == 90){
        return "加签";
    }
    if(data == 100){
        return "催办";
    }
    if(data == 110){
        return "设置归属人";
    }
    return "缺省";
}