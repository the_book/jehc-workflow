//返回
function goback(){
    tlocation(base_html_redirect+'/workflow/lc-status/lc-status-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateLcStatus(){
    submitBForm('defaultForm',workflowModules+'/lcStatus/update',base_html_redirect+'/workflow/lc-status/lc-status-list.html',null,"PUT");
}



$(document).ready(function(){
    datetimeInit();
    var lc_status_id = GetQueryString("lc_status_id");
    //加载表单数据
    ajaxBRequestCallFn(workflowModules+'/lcStatus/get/'+lc_status_id,{},function(result){
        $('#lc_status_id').val(result.data.lc_status_id);
        $('#lc_status_name').val(result.data.lc_status_name);
        $('#lc_status_remark').val(result.data.lc_status_remark);
        $('#lc_status_constant').val(result.data.lc_status_constant);
    });
});
