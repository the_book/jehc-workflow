//返回
function goback(){
    tlocation(base_html_redirect+'/workflow/lc-status/lc-status-list.html');
}



$(document).ready(function(){
    datetimeInit();
    var lc_status_id = GetQueryString("lc_status_id");
    //加载表单数据
    ajaxBRequestCallFn(workflowModules+'/lcStatus/get/'+lc_status_id,{},function(result){
        $('#lc_status_id').val(result.data.lc_status_id);
        $('#lc_status_name').val(result.data.lc_status_name);
        $('#lc_status_remark').val(result.data.lc_status_remark);
        $('#lc_status_constant').val(result.data.lc_status_constant);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
    });
});