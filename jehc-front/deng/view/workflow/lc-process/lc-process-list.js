var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcProcess/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight2,
        sPaginationType:'simple_numbers',
        fixedHeader:true,//表头固定
        fixedColumns: {
            "leftColumns": 4,
            "rightColumns": 1
        },
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"lc_process_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"lc_process_id"
			},
            {
                data:"lc_process_id",
                render:function(data, type, row, meta) {
                    var lc_process_flag = row.lc_process_flag;
                    var lc_process_title = row.lc_process_title;
                    var xt_attachment = row.xt_attachment;
                    var btn = "";
                    if(lc_process_flag == 0) {
                        // btn = '<div class="btn-group ml-2">' +
                        //         '<a href="#" class="btn btn-light-primary font-weight-bold" data-toggle="dropdown" aria-expanded="true">' +
                        //             '<i class="fa flaticon-truck"></i>选项' +
                        //         '</a>' +
                        //         '<button type="button" class="btn btn-light-primary font-weight-bold dropdown-toggle dropdown-toggle-split mr-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        //             '<span class="sr-only"></span>'+
                        //         '</button>'+
                        //         '<div class="dropdown-menu">' +
                        //             '<button class="dropdown-item" onclick=addLcDesign("' + data + '","' + lc_process_title + '")><i class="fa flaticon-logout"></i>设计流程</button>' +
                        //             '<button class="dropdown-item" onclick=downFileBpmn("' + data + '")><i class="fa flaticon-download"></i>下载bpmn文件</a>' +
                        //             '<button class="dropdown-item" onclick=downFileImg("' + data + '")><i class="fa flaticon-attachment"></i>下载img文件</a>' +
                        //             '<button class="dropdown-item" onclick=showProcessImage("' + data + '")><i class="la la-file-image-o"></i>流程图</a>' +
                        //             '<button class="dropdown-item" onclick=toLcProcessDetail("' + data + '")><i class="fa flaticon-list"></i>详情</a>' +
                        //         '</div>' +
                        //     '</div>';
                        btn = '<button class="btn btn-link" onclick=addLcDesign("' + data + '","' + lc_process_title + '")>设计流程</button>' +
                            // '<button class="btn btn-link" onclick=downFileBpmn("' + data + '")>下载bpm</a>' +
                            // '<button class="btn btn-link" onclick=downFileImg("' + data + '")>下载img</a>' +
                            '<button class="btn btn-link" onclick=showProcessImage("' + data + '")>监 控</a>' +
                            '<button class="btn btn-link" title="导出流程" onclick=exportProcess("' + data + '","'+lc_process_title+'")>导 出</button>'+
                            '<button class="btn btn-link" onclick=toLcProcessDetail("' + data + '")>详 情</a>'+
                            '<button onclick=showResources("'+data+'") class="btn btn-link" title="流程源码">' +
                            '源 码</button>'
                    }else{
                        // btn = '<div class="btn-group ml-2">' +
                        //         '<a href="#" class="btn btn-light-primary font-weight-bold" data-toggle="dropdown" aria-expanded="true">' +
                        //             '<i class="fa flaticon-truck"></i>选项' +
                        //         '</a>' +
                        //         '<button type="button" class="btn btn-light-primary font-weight-bold dropdown-toggle dropdown-toggle-split mr-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        //             '<span class="sr-only"></span>'+
                        //         '</button>'+
                        //         '<div class="dropdown-menu dropdown-menu-right">' +
                        //             '<button class="dropdown-item" onclick=downFile("' + xt_attachment + '")><i class="fa flaticon-attachment"></i>下载附件</a>' +
                        //             '<button class="dropdown-item" onclick=toLcProcessDetail("' + data + '")><i class="fa flaticon-list"></i>详情</a>' +
                        //         '</div>' +
                        //     '</div>';
                        btn = '<button class="btn btn-link" onclick=downFile("' + xt_attachment + '")>下载附件</a>' +

                            '<button class="btn btn-link" onclick=toLcProcessDetail("' + data + '")>详 情</a>';
                    }
                    return btn+
                        // '<button onclick=toLcProcessDetail("'+data+'") class="btn btn-light-primary font-weight-bold mr-1" title="详 情">' +
                        // '<i class="fa flaticon-list"></i>详 情' +
                        // '</button>'+
                        '</button>'+
                        '<button onclick=copyProcess("'+data+'") class="btn btn-link" title="复制一个模型">' +
                        '复 制' +
                        '</button>'+
                        '<button onclick=showLcDeploymentHis("'+data+'","'+lc_process_title+'") class="btn btn-link" title="发布版本记录">' +
                        '版 本</button>'+
                        '<button onclick=createDeployment("'+data+'") class="btn btn-link" title="发布流程">' +
                        '发 布</button>';
                }
            },
            {
                data:'lc_process_title'
            },
			{
				data:'lc_process_status',
                render:function(data, type, row, meta) {
                    if(data == 1){
                        return "<span class='label label-lg font-weight-bold label-light-success label-inline'>发布中</span>"
                    }
                    if(data == 0){
                        return "<span class='label label-lg font-weight-bold label-light-info label-inline'>待发布</span>"
                    }
                    if(data == 2){
                        return "<span class='label label-lg font-weight-bold label-light-danger label-inline'>已关闭</span>"
                    }
                    return "<span class='label label-lg font-weight-bold label-light-info label-inline'>缺省</span>"
                }
			},
            {
                data:'del_flag',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='label label-lg font-weight-bold label-light-success label-inline'>使用中</span>"
                    }
                    if(data == 1){
                        return "<span class='label label-lg font-weight-bold label-light-danger label-inline'>已废弃</span>"
                    }
                    return "<span class='label label-lg font-weight-bold label-light-info label-inline'>缺省</span>"
                }
            },
			{
				data:'lc_process_flag',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='label label-lg font-weight-bold label-light-primary label-inline'>通过平台设计器设计</span>"
                    }
                    if(data == 1){
                        return "<span class='label label-lg font-weight-bold label-light-info label-inline'>通过上传部署</span>"
                    }
                    return "<span class='label label-lg font-weight-boldlabel-light-success label-inline'>缺省</span>"
                }
			},
            {
                data:'version',
                render:function(data, type, row, meta) {
                    return "<span class='label label-light-warning mr-2'>"+data+"</span>"
                }
            },
            {
                data:"name"
            },
            {
                data:"groupName"
            },
            {
                data:'w',
                render:function(data, type, row, meta) {
                    return "<span class='label label-light-info mr-2'>"+data+"</span>"
                }
            },
            {
                data:'h',
                render:function(data, type, row, meta) {
                    return "<span class='label label-light-info mr-2'>"+data+"</span>"
                }
            },
            {
                data:"createBy",
                render:function(data, type, row, meta) {
                    return data;
                }
            },
            {
                data:"create_time",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"modifiedBy",
                render:function(data, type, row, meta) {
                    return data;
                }
            },
            {
                data:"update_time",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"moduleKey"
            }
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
	//产品线下拉框数据
    initProductList("lc_product_id");
    // $("#datatables").colResizable();
});
//新增
function toLcProcessAdd(){
	tlocation(base_html_redirect+'/workflow/lc-process/lc-process-add.html');
}
//修改
function toLcProcessUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	var table = $('#datatables').dataTable();
	var List = getTableContent(table);
	for(var i = 0; i < List.length; i++){
		var obj = List[i];
		if(obj.lc_process_id == id && obj.lc_process_flag == 0){
			toastrBoot(4,"通过平台设计器设计的数据不能在编辑中修改 只能在设计器中修改");
			return;
		}
	}
    tlocation(base_html_redirect+'/workflow/lc-process/lc-process-update.html?lc_process_id='+id);
}
//详情
function toLcProcessDetail(id){
    tlocation(base_html_redirect+'/workflow/lc-process/lc-process-detail.html?lc_process_id='+id);
}
//删除
function delLcProcess(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要废弃的数据");
		return;
	}
	msgTishCallFnBoot("确定要废弃所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {lc_process_id:id};
		ajaxBReq(workflowModules+'/lcProcess/delete',params,['datatables'],null,"DELETE");
	});
}

/**
 *
 * @param lc_process_id
 * @param lc_process_title
 */
function addLcDesign(lc_process_id,lc_process_title){
    $('#lcDesignPanelBody').height(reGetBodyHeight()-128);
	$('#lcDesignModalLabel').html("在线设计---<font color=red>"+lc_process_title+"</font>");
	$('#lcDesignModal').modal({backdrop:'static',keyboard:false});
	$('#lcDesignModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        $("#lcDesignIframe",document.body).attr("src",base_html_redirect+'/workflow/lc-design/lc-design2.0.html?lc_process_id='+lc_process_id);
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcDesignModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
    });
}

/**
 *
 */
function addDesign(){
    $('#lcDesignPanelBody').height(reGetBodyHeight()-128);
    // $('#lcDesignModalLabel').html("在线设计---<font color=red>"+lc_process_title+"</font>");
    $('#lcDesignModal').modal({backdrop:'static',keyboard:false});
    $('#lcDesignModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        $("#lcDesignIframe",document.body).attr("src",base_html_redirect+'/workflow/lc-design/lc-design2.0.html');
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcDesignModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
    });
}

/**
 * 关闭窗体
 */
function closeLcProcessWin(){
	search('datatables');
}

/**
 * 版本
 * @param lc_process_id
 * @param lc_process_title
 */
function showLcDeploymentHis(lc_process_id,lc_process_title){
	$('#lcDeploymentHisPanelBody').height(reGetBodyHeight()-216);
	$('#lcDeploymentHisModalLabel').html("流程发布版本【<font color=red>"+lc_process_title+"</font>】");
	$('#lcDeploymentHisModal').modal({backdrop:'static',keyboard:false});
	$('#lcDeploymentHisModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcDeploymentHisModalDialog");
        $("#lcDeploymentHisIframe",document.body).attr("src",base_html_redirect+'/workflow/lc-deployment-his/lc-deployment-his-list.html?lc_process_id='+lc_process_id);
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
    });
}

/**
 * 下载图片
 * @param lc_process_id
 */
function downFileImg(lc_process_id){
	downOrExportB(workflowModules+'/lcProcess/downFileImg?lc_process_id='+lc_process_id);
}


/**
 * 下载附件
 * @param xt_attachment
 */
function downFile(xt_attachment){
	downOrExportB(workflowModules+'/xtCommon/downFile?xt_attachment_id='+xt_attachment);
}

/**
 * 下载BPMN文件
 * @param lc_process_id
 */
function downFileBpmn(lc_process_id){
	downOrExportB(workflowModules+'/lcProcess/downFileBpmn?lc_process_id='+lc_process_id);
}


/**
 * 发布流程
 * @param lc_process_id
 */
function createDeployment(lc_process_id){
	msgTishCallFnBoot("确定要发布该流程？",function(){
		var params = {lc_process_id:lc_process_id};
		ajaxBReq(workflowModules+'/lcProcess/createDeployment',params,['datatables'],null,"PUT");
	});
}


/**
 * 初始化产品线
 * @param id
 */
function initProductList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"POST",
        url:workflowModules+"/lcProduct/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.lc_product_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
        }
    });
}

/**
 *
 * @param id
 * @param value_id
 * @param lc_product_id
 */
function initGroupListSetV(id,value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    if(null != $("#"+value_id).val() && '' != $("#"+value_id).val()){
        $.ajax({
            type:"GET",
            url:workflowModules+"/lcGroup/find/"+$("#"+value_id).val(),
            success: function(result){
                result = result.data;
                //从服务器获取数据进行绑定
                $.each(result, function(i, item){
                    str += "<option value=" + item.lc_group_id + ">" + item.name + "</option>";
                })
                $("#"+id).append(str);
            }
        });
    }else{
        $("#"+id).append(str);
    }
}

/**
 * 流程图
 * @param id
 */
function showProcessImage(id) {
    var ImageModalCount = 0 ;
    $('#lcProcessImagePanelBody').height(reGetBodyHeight()-128);
    $('#lcProcessImageModal').modal({backdrop:'static',keyboard:false});
    $('#lcProcessImageModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcProcessImageModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        // $modal_dialog.css({'width':reGetBodyWidth()*0.55+'px'});
        if(++ImageModalCount == 1) {
            //加载表单数据
            ajaxBRequestCallFn(workflowModules+'/lcProcess/image/base64/'+id,{},function(result){
                $('#imagePath').attr("src", result.data);
            });
        }
    });
}

/**
 * 拷贝流程
 * @param id
 */
function copyProcess(id){
    $('#defaultCopyProcessForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    var copyProcessModalCount = 0 ;
    $('#copyProcessPanelBody').height(reGetBodyHeight()-218);
    $('#copyProcessModal').modal({backdrop:'static',keyboard:false});
    $('#copyProcessModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // $('#copyProcessModalDialog').modal('show').css({ width: reGetBodyWidth()*0.55+'px'});
        var $modal_dialog = $("#copyProcessModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++copyProcessModalCount == 1) {
            //加载表单数据
            ajaxBRequestCallFn(workflowModules+'/lcProcess/get/'+id,{},function(result){
                $('#processId').val(result.data.lc_process_id);
                $('#lc_process_title').val(result.data.lc_process_title+"（复制）");
                $('#moduleKey').val(result.data.moduleKey+"-");
                $('#lc_product_idTemp').val(result.data.lc_product_id);
                $('#lc_group_idTemp').val(result.data.lc_group_id);
                //产品线下拉框数据
                initProductList("lc_product_id_");
                initGroupListSetV("lc_group_id_","lc_product_idTemp","lc_group_idTemp")
            });
        }
    });
}

/**
 * 复制
 */
function doCopy() {
    submitBFormCallFn('defaultCopyProcessForm',workflowModules+'/lcProcess/copy',function(result){
        try {
            if(typeof(result.success) != "undefined"){
                if(result.success){
                    window.parent.toastrBoot(3,result.message);
                    closeLcProcessWin();
                    $('#copyProcessModal').modal('hide');
                }else{
                    window.parent.toastrBoot(4,result.message);
                }
            }
        } catch (e) {
            window.parent.toastrBoot(4,"复制异常！");
        }
    },null,"POST");
}

/**
 * 导出流程
 * @param lc_process_id
 * @param lc_process_title
 */
function exportProcess(lc_process_id,lc_process_title) {
    msgTishCallFnBoot("确定要导出该流程？",function(){
        var url = workflowModules+'/lcProcess/export?lc_process_id='+lc_process_id; // 请求接口
        var method = "GET"; // 请求方式 GET或POST
        var fileName = lc_process_title+".json"; // 下载后的文件名称
        var params = "name='test" // 后台接口需要的参数
        downloadFileCallFn(url, method, fileName,params);
    });
}


/*
/!**
 * 初始化参数
 * @type {{uploadUrl: string, type: string, contentType: string, allowedFileExtensions: string[], enctype: string, layoutTemplates: {actionUpload: string, close: string}, showUploadedThumbs: boolean, autoReplace: boolean, overwriteInitial: boolean, showUpload: boolean, showCaption: boolean, showRemove: boolean, showPreview: boolean, showCancel: boolean, showClose: boolean, initialPreviewShowDelete: boolean, showBrowse: boolean, browseOnZoneClick: boolean, minFileCount: number, uploadAsync: boolean, maxFileSize: number, maxFileCount: number, validateInitialCount: boolean, msgFilesTooMany: string, uploadExtraData: initFileInputOptions.uploadExtraData}}
 *!/
var initFileInputOptions = {
    theme: "fa",
    language: 'zh', //设置语言
    uploadUrl: workflowModules+'/lcProcess/import',
    type: 'POST',
    contentType: "application/json;charset=utf-8",
    allowedFileExtensions: ['json','txt'],//接收的文件后缀
    enctype: 'multipart/form-data',
    layoutTemplates: {
        actionUpload: '',//去除上传预览缩略图中的上传按钮
        close: ''//去除右上角X 清空文件按钮
    },
    showUploadedThumbs: false,//布尔值，是否在预览窗口中持续显示已经上传的文件缩略图（用于ajax上传），直到按下删除/清除按钮。默认值为true。当设置为false时，下一批选择上传的文件将从预览中清除这些已经上传的文件缩略图。
    allowedFileExtensions: ['json','txt'],
    autoReplace: true,//布尔值，当上传文件达到maxFileCount限制并且一些新的文件被选择后，是否自动替换预览中的文件。如果maxFileCount值有效，那么这个参数会起作用。默认为false
    overwriteInitial: true,//布尔值，是否要覆盖初始预览内容和标题设置。默认为true，因此当新文件上传或文件被清除时任何initialPreview内容集将被覆盖。将其设置为false将有助于始终显示从数据库中保存的图像或文件，尤其是在使用多文件上传功能时尤其有用。
    showUpload: true, //布尔值，是否显示文件上传按钮。默认值为true。它默认为一个表单提交按钮，除非指定了uploadUrl属性。
    showCaption: false,//布尔值，是否显示文件名。默认值为true。
    showRemove: false, //布尔值，是否显示删除/清除按钮。默认值为true。
    showPreview: true, //布尔值，是否显示文件预览。默认值为true。
    showCancel: false,//布尔值，是否显示文件上传取消按钮。默认值为true。只有在AJAX上传过程中，才会启用和显示。
    showClose: false,//布尔值，是否显示预览界面的关闭图标。默认为true。只有当showPreview为true或者在预览模板中使用{close}标签时才会被解析。
    initialPreviewShowDelete: true,
    dropZoneEnabled: false,// 禁止点击预览区域进行文件上传操作
    showClose: true, //是否显示删除按钮
    autoReplace: true, //再次选择,覆盖之前图片内容
    showBrowse: false,//布尔值，是否显示文件浏览按钮。默认为true。
    browseOnZoneClick: false,//布尔值，是否在点击预览区域时触发文件浏览/选择。默认为false。
    minFileCount: 1,
    uploadAsync: false,//默认异步上传
    maxFileSize: 0,//单位为kb，如果为0表示不限制文件大小
    maxFileCount: 1,//表示允许同时上传的最大文件个数
    validateInitialCount: true,
    msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
    uploadExtraData:function (previewId, index) {
        var data = {
        };
        return data;
    }
}*/


/**
 * 导入流程
 */
var processFileUploadResultArray = [];
function importProcess() {
    $('#processFileModal').modal({backdrop: 'static', keyboard: false});
    var importProcessModalCount = 0 ;
    $('#processFileModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++importProcessModalCount == 1) {
            processFileUploadResultArray.push('0');
            //使用bootstrap-fileinput渲染
            $('#lcProcessFile').fileinput({
                language:'zh',//设置语言
                uploadUrl:workflowModules+'/lcProcess/import',//上传的地址
                type: 'POST',
                contentType: "application/json;charset=utf-8",
                enctype: 'multipart/form-data',
                allowedFileExtensions:['json','txt'],//接收的文件后缀
                theme:'fa',//主题设置
                dropZoneTitle:'可以将文件拖放到这里',
                autoReplace: true,//布尔值，当上传文件达到maxFileCount限制并且一些新的文件被选择后，是否自动替换预览中的文件。如果maxFileCount值有效，那么这个参数会起作用。默认为false
                overwriteInitial: true,//布尔值，是否要覆盖初始预览内容和标题设置。默认为true，因此当新文件上传或文件被清除时任何initialPreview内容集将被覆盖。将其设置为false将有助于始终显示从数据库中保存的图像或文件，尤其是在使用多文件上传功能时尤其有用。
                uploadAsync:true,//默认异步上传
                showPreview:true,//是否显示预览
                // showUpload: true,//是否显示上传按钮
                showRemove: true,//显示移除按钮
                showCancel:true,//是否显示文件上传取消按钮。默认为true。只有在AJAX上传过程中，才会启用和显示
                showCaption: true,//是否显示文件标题，默认为true
                browseClass: "btn btn-light-primary font-weight-bold mr-2", //文件选择器/浏览按钮的CSS类。默认为btn btn-primary
                dropZoneEnabled: true,//是否显示拖拽区域
                validateInitialCount: true,
                maxFileSize: 0,//最大上传文件数限制，单位为kb，如果为0表示不限制文件大小
                minFileCount: 1, //每次上传允许的最少文件数。如果设置为0，则表示文件数是可选的。默认为0
                maxFileCount: 1, //每次上传允许的最大文件数。如果设置为0，则表示允许的文件数是无限制的。默认为0
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",//当检测到用于预览的不可读文件类型时，将在每个预览文件缩略图中显示的图标。默认为<i class="glyphicon glyphicon-file"></i>
                previewFileIconSettings: {
                    'docx': '<i ass="fa fa-file-word-o text-primary"></i>',
                    'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                    'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                    'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                    'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                    'pdf': '<i class="fa fa-file-archive-o text-muted"></i>',
                    'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                },
                uploadExtraData: function () {
                    return {
                        name: 'test'
                    }; //上传时额外附加参数
                },
                // msgSizeTooLarge: "文件 '{name}' (<b>{size} KB</b>) 超过了允许大小 <b>{maxSize} KB</b>.",
                msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",//字符串，当文件数超过设置的最大计数时显示的消息 maxFileCount。默认为：选择上传的文件数（{n}）超出了允许的最大限制{m}。请重试您的上传！
                // elErrorContainer:'#kartik-file-errors',
                ajaxSettings:{
                    headers:{
                        "token":getToken()
                    }
                }
            }).on('change',function () {
                // 清除掉上次上传的图片
                $(".uploadPreview").find(".file-preview-frame:first").remove();
                $(".uploadPreview").find(".kv-zoom-cache:first").remove();
            }).on('fileuploaded',function (event,data,previewId,index) {//异步上传成功处理
                console.log('单个上传成功，并清空上传控件内容',data,previewId,index);
                for(var i = 0; i < processFileUploadResultArray.length; i++){
                    if(i == 0){
                        //清除文件输入 此方法清除所有未上传文件的预览，清除ajax文件堆栈，还清除本机文件输入
                        $('#lcProcessFile').fileinput('clear');
                        $('#lcProcessFile').fileinput('clear').fileinput('disable');
                        window.parent.toastrBoot(3,data.response.message);
                        //关闭文件上传的模态对话框
                        $('#processFileModal').modal('hide');
                        //重新刷新bootstrap-table数据
                        search('datatables')
                        processFileUploadResultArray.splice(0,processFileUploadResultArray.length);
                        i--;
                        break;
                    }
                }
            }).on('fileerror',function (event,data,msg) { //异步上传失败处理
                console.log('单个上传失败，并清空上传控件内容',data,msg);
                window.parent.toastrBoot(4,"上传文件失败！");
            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
                console.log('批量上传成功，并清空上传控件内容',data,previewId,index);
            }).on('filebatchuploaderror', function(event, data, msg) {
                //批量上传失败，并清空上传控件内容
                console.log('批量上传失败，并清空上传控件内容',data,msg);
            }).on("filebatchselected", function (event, data, previewId, index) {
                console.log('选择文件',data,previewId,index);
            })/*.on('filepreajax', function(event, previewId, index) {
                var container = $("#divId");
                var processDiv = container.find('.kv-upload-progress');
                processDiv.hide();
                $('#lcProcessFile').fileinput('enable');
                return false;
            })*/;
        }
    });

    /*$("#lcProcessFile").fileinput(initFileInputOptions).
    on("fileuploaded", function (event, data, previewId, index) {
        console.log("异步上传成功结果处理",data);
        // // 文件上传成功
        // const result = data.response
        // if (+result.code == +web_status.SUCCESS) {
        //     $('input[name=fileUrl]').val(result.url)
        //     $('input[name=host]').val(result.host)
        // } else {
        //     $.modal.alertError(result.msg);
        // }
    }).on("filesuccessremove", function (event, data, previewId, index) {

        console.log('------filesuccessremove 移除---',data,previewId,index);

    }).on("filebatchselected", function (event, data, previewId, index) {

        if (data.length == 0) {
            return;
        }
        console.log('------filebatchselected 立即上传---',data,index);
        $(this).fileinput("upload");
        // data.submit();
    }).on('filebatchpreupload', function(event, data) {

    }).on('fileuploadadd', function(event, data) {
        data.submit();
    }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
        //并清空上传控件内容
        $('#lcProcessFile').val('');
        $("#lcProcessFile").fileinput('reset'); //重置上传控件（清空已文件）
        for(var i = 0; i < processFileUploadResultArray.length; i++){
            if(i == 0){
                var obj = data.response;
                processFileUploadResultArray.splice(0,processFileUploadResultArray.length);
                i--;
                break;
            }
        }
        //上传成功调用移除按钮
        $(".fileinput-remove-button").click();
    }).on('filebatchuploaderror', function(event, data, msg) {
        //并清空上传控件内容
        $('#lcProcessFile').val('');
        $("#lcProcessFile").fileinput('reset'); //重置上传控件（清空已文件）
        window.parent.toastrBoot(4,"上传失败！");
    });*/
}

/**
 * 关闭窗体
 */
function closeProcessFileWin() {
    $("#processFile").fileinput('reset'); //重置上传控件（清空已文件）
    $('#lcProcessFile').fileinput('clear');
    //关闭上传窗口
    $('#processFileModal').modal('hide');
}

/**
 * 流程源码
 * @param id
 */
var myCodeMirror;
var currentBPMN = "";
var currentImg = "";
var currentMxgraph = "";
function showResources(id) {
    var lcReSourcesSelectModalCount = 0 ;
    $('#lcReSourcesBody').height(reGetBodyHeight()-218);
    $('#lcReSourcesModal').modal({backdrop: 'static', keyboard: false});
    $('#lcReSourcesModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcReSourcesModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++lcReSourcesSelectModalCount == 1){
            $("#sourcesID").val(0)
            if(null == myCodeMirror || undefined == myCodeMirror){
                myCodeMirror = CodeMirror.fromTextArea(document.getElementById('xmlRequestTextarea'),{
                    mode: "text/html",
                    // theme: 'monokai',
                    lineNumbers: true,
                    lineWrapping: true,      //代码折叠
                    styleActiveLine: true,   // 当前行背景高亮
                    indentUnit: 4,           // 缩进单位为4
                    matchBrackets: true,     //括号匹配
                    smartIndent: true,       // 自动缩进
                    autofocus: true,         // 自动焦点
                    autoCloseBrackets: true  // 自动补全括号
                });
            }
            myCodeMirror.setValue("");
            myCodeMirror.clearHistory();
            if(id != undefined){
                ajaxBRequestCallFn(workflowModules+'/lcProcess/get/'+id,{},function(result){
                    if(result.data.lc_process_bpmn != undefined){
                        myCodeMirror.setValue(result.data.lc_process_bpmn);
                        currentBPMN = result.data.lc_process_bpmn;
                    }
                    if(result.data.imgxml != undefined){
                        currentImg = result.data.imgxml;
                    }
                    if(result.data.lc_process_mxgraphxml != undefined){
                        currentMxgraph = result.data.lc_process_mxgraphxml;
                    }
                });
            }
        }
    });
}

/**
 * 动态设置改变源码显示
 */
function setResource() {
    myCodeMirror.setValue("");
    myCodeMirror.clearHistory();
    var sourcesID =  $("#sourcesID").val();
    if(sourcesID == 0){
        myCodeMirror.setValue(currentBPMN);
    }
    if(sourcesID == 1){
        myCodeMirror.setValue(currentMxgraph);
    }
    if(sourcesID == 2){
        myCodeMirror.setValue(currentImg);
    }
}

/**
 *
 */
function copyLcReSources() {
    var textA = document.createElement('textarea');// 创建textArea
    textA.id = 'xmlCode';
    textA.value = myCodeMirror.getValue();
    document.getElementById('copy').appendChild(textA);// 追加
    document.getElementById('xmlCode').select();// 选中复制内容
    try {
        window.document.execCommand('Copy');//不能复制隐藏域
        document.getElementById('copy').innerHTML = '';//移除
        toastrBoot(3,"复制成功！");
    } catch (error) {
        toastrBoot(4,"复制失败，浏览器不支持！");
    }
}

/**
 * 选择区域格式化
 */
function autoFormatSelection() {
    var range = getSelectedRange();
    myCodeMirror.autoFormatRange(range.from, range.to);
}

/**
 * 选择范围
 * @returns {{from: *, to: *}}
 */
function getSelectedRange() {
    return { from: myCodeMirror.getCursor(true), to: myCodeMirror.getCursor(false) };
}

/**
 * 注释
 * @param isComment
 */
function commentSelection(isComment) {
    var range = getSelectedRange();
    myCodeMirror.commentRange(isComment, range.from, range.to);
}

/**
 * 全部格式化
 */
function formatAll() {
    CodeMirror.commands["selectAll"](myCodeMirror);
    autoFormatSelection();
}