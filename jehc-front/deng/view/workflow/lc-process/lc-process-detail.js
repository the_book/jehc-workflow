//返回
function goback(){
    tlocation(base_html_redirect+'/workflow/lc-process/lc-process-list.html');
}

/**初始化附件右键菜单开始 参数4为1表示不拥有上传和删除功能 即明细页面使用**/
initBFileRight('xt_attachment','xt_attachment_pic',2);
/**初始化附件右键菜单结束**/

$(document).ready(function(){
    var lc_process_id = GetQueryString("lc_process_id");
    //加载表单数据
    ajaxBRequestCallFn(workflowModules+'/lcProcess/get/'+lc_process_id,{},function(result){
        $('#lc_process_id').val(result.data.lc_process_id);
        $('#lc_process_title').val(result.data.lc_process_title);
        $('#lc_process_img_path').val(result.data.lc_process_img_path);
        var lc_process_status = result.data.lc_process_status;
        $('#lc_process_status').val(lc_process_status);
        var lc_process_flag = result.data.lc_process_flag;
        $('#lc_process_flag').val(lc_process_flag);
        $('#xt_constant_id').val(result.data.xt_constant_id);
        $('#xt_attachment').val(result.data.xt_attachment);
        $('#lc_process_uid').val(result.data.lc_process_uid);
        $('#lc_process_uk').val(result.data.lc_process_uk);
        $('#w').val(result.data.w);
        $('#h').val(result.data.h);
        $('#lc_process_remark').val(result.data.lc_process_remark);
        $('#lc_process_bpmn').val(result.data.lc_process_bpmn);
        $('#lc_process_bpmn_path').val(result.data.lc_process_bpmn_path);
        $('#lc_process_mxgraphxml').val(result.data.lc_process_mxgraphxml);
        $('#imgxml').val(result.data.imgxml);
        $('#lc_process_remark').val(result.data.lc_process_remark);
        $('#lc_product_idTemp').val(result.data.lc_product_id);
        $('#lc_group_idTemp').val(result.data.lc_group_id);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
        $('#moduleKey').val(result.data.moduleKey);
        $('#candidate_group_type').val(result.data.candidate_group_type);
        $('#candidateStarterUsers').val(result.data.candidateStarterUsers);
        $('#candidateStarterGroups').val(result.data.candidateStarterGroups);
        var lc_process_mxgraph_style = result.data.lc_process_mxgraph_style;
        if(lc_process_mxgraph_style == 1){
            $('#lc_process_mxgraph_style').val("曲线");
        }else{
            $('#lc_process_mxgraph_style').val("直线");
        }
        $('#version').val(result.data.version);

        if(result.data.lc_process_flag == 1){
            $("#attFile").show();
            /**配置附件回显方法开始**/
            var params = {xt_attachment_id:$('#xt_attachment').val(),field_name:'xt_attachment'};
            ajaxBFilePathBackRequest(fileModules+'/attAchmentPathpp',params);
            /**配置附件回显方法结束**/
        }
        //产品线下拉框数据
        initProductList("lc_product_id");
        initGroupListSetV("lc_group_id","lc_product_idTemp","lc_group_idTemp")
    });
});


function initProductList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"POST",
        url:workflowModules+"/lcProduct/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.lc_product_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
            $('#lc_product_id').val($('#lc_product_idTemp').val());
        }
    });
}


/**
 *
 * @param id
 * @param value_id
 * @param lc_product_id
 */
function initGroupListSetV(id,value_id,s_value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    if(null != $("#"+value_id).val() && '' != $("#"+value_id).val()){
        $.ajax({
            type:"GET",
            url:workflowModules+"/lcGroup/find/"+$("#"+value_id).val(),
            success: function(result){
                result = result.data;
                //从服务器获取数据进行绑定
                $.each(result, function(i, item){
                    str += "<option value=" + item.lc_group_id + ">" + item.name + "</option>";
                })
                $("#"+id).append(str);
                if(undefined != $("#"+s_value_id).val()){

                    $('#'+id).val($("#"+s_value_id).val());
                }

            }
        });
    }else{
        $("#"+id).append(str);
    }

}