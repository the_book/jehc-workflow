//创建节点模板 暂时未使用
function insertEdgeTemplate(panel, graph, name, icon, style, width, height, value, parentNode){
		var cells = [new mxCell((value != null) ? value : '', new mxGeometry(0, 0, width, height), style)];
		cells[0].geometry.setTerminalPoint(new mxPoint(0, height), true);
		cells[0].geometry.setTerminalPoint(new mxPoint(width, 0), false);
		cells[0].edge = true;
		var funct = function(graph, evt, target){
			cells = graph.getImportableCells(cells);
			if(cells.length > 0){
				var validDropTarget = (target != null) ? graph.isValidDropTarget(target, cells, evt) : false;
				var select = null;
				if(target != null && !validDropTarget){
					target = null;
				}
				var pt = graph.getPointForEvent(evt);
				var scale = graph.view.scale;
				pt.x -= graph.snap(width / 2);
				pt.y -= graph.snap(height / 2);
				select = graph.importCells(cells, pt.x, pt.y, target);
				GraphEditor.edgeTemplate = select[0];
				graph.scrollCellToVisible(select[0]);
				graph.setSelectionCells(select);
			}
		};
		var node = panel.addTemplate(name, icon, parentNode, cells);
		var installDrag = function(expandedNode){
			if (node.ui.elNode != null){
				var dragPreview = document.createElement('div');
				dragPreview.style.border = 'dashed black 1px';
				dragPreview.style.width = width+'px';
				dragPreview.style.height = height+'px';
				mxUtils.makeDraggable(node.ui.elNode, graph, funct, dragPreview, -width / 2, -height / 2,graph.autoscroll, true);
			}
		};
		if(!node.parentNode.isExpanded()){
			panel.on('expandnode', installDrag);
		}else{
			installDrag(node.parentNode);
		}
		return node;
};
// 添加元素右上角的删除图标  
function addOverlays(graph, cell, addDeleteIcon){  
    var overlay = new mxCellOverlay(new mxImage('images/add.png', 24, 24), 'Add child');  
    overlay.cursor = 'hand';  
    overlay.align = mxConstants.ALIGN_CENTER;  
    overlay.addListener(mxEvent.CLICK, mxUtils.bind(this, function(sender, evt){  
        addChild(graph, cell);  
    }));  
    graph.addCellOverlay(cell, overlay);  
    if (addDeleteIcon){  
        overlay = new mxCellOverlay(new mxImage('images/close.png', 30, 30), 'Delete');  
        overlay.cursor = 'hand';  
        overlay.offset = new mxPoint(-4, 8);  
        overlay.align = mxConstants.ALIGN_RIGHT;  
        overlay.verticalAlign = mxConstants.ALIGN_TOP;  
        overlay.addListener(mxEvent.CLICK, mxUtils.bind(this, function(sender, evt){  
            deleteSubtree(graph, cell);  
        }));  
        graph.addCellOverlay(cell, overlay);  
    }  
}; 
// 添加子元素  
function addChild(graph, cell){  
    var model = graph.getModel();  
    var parent = graph.getDefaultParent();  
    var vertex;  
    model.beginUpdate();  
    try {  
        vertex = graph.insertVertex(parent, null, 'Double click to set name');  
        var geometry = model.getGeometry(vertex);  
        var size = graph.getPreferredSizeForCell(vertex);  
        geometry.width = size.width;  
        geometry.height = size.height;  
        var edge = graph.insertEdge(parent, null, '', cell, vertex);  
        edge.geometry.x = 1;  
        edge.geometry.y = 0;  
        edge.geometry.offset = new mxPoint(0, -20);  
    }finally{  
        model.endUpdate();  
    }  
    return vertex;  
};

////导入流程
function imp(graph,history){
    graph_refresh = graph;
    var processSelectModalCount = 0 ;
    $('#processBody').height(reGetBodyHeight()-228);
    $('#processSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#processSelectModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#processModalDialog");
        // $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.8+'px'});
        if(++processSelectModalCount == 1){
            $('#searchFormprocess')[0].reset();
            var opt = {
                searchformId:'searchFormprocess'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcProcess/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex+1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'220px',
                sPaginationType:'simple_numbers',
                //列表表头字段
                colums:[
                    {
                        data:"lc_process_id",
                        width:"50px"
                    },
                    {
                        data:'createBy',
                        width:"150px"
                    },
                    {
                        data:'lc_process_title',
                        width:"150px"
                    },
                    {
                        data:'lc_process_status',
                        width:"150px",
                        render:function(data, type, row, meta) {
                            if(data == 1){
                                return "<span class='label label-lg font-weight-bold label-light-success label-inline'>发布中</span>"
                            }
                            if(data == 0){
                                return "<span class='label label-lg font-weight-bold label-light-info label-inline'>待发布</span>"
                            }
                            if(data == 2){
                                return "<span class='label label-lg font-weight-bold label-light-danger label-inline'>已关闭</span>"
                            }
                            return "<span class='label label-lg font-weight-bold label-light-info label-inline'>缺省</span>"
                        }
                    },
                    {
                        data:'name',
                        width:"150px"
                    },
                    {
                        data:'groupName',
                        width:"150px"
                    },
                    {
                        data:"lc_process_id",
                        render:function(data, type, row, meta) {
                            var lc_process_flag = row.lc_process_flag;
                            var lc_process_title = row.lc_process_title;
                            var xt_attachment = row.xt_attachment;
                            var btn = '<button class="btn btn-light-primary font-weight-bold mr-2" onclick=doImpl("'+data+'")><i class="glyphicon glyphicon-eye-open"></i>导入流程</button>';
                            return btn;
                        }
                    }
                ]
            });
            grid=$('#processDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('processDatatables');
        }
    });
}

/**
 * 流程对象
 * @param id
 */
function doProcess(id){
    ajaxBRequestCallFn(workflowModules+'/lcProcess/get/'+id,null,function(result){
        result = result;
        if(result !== undefined && typeof(result.success) != "undefined" && result.success == true){
            var lc_process_title = result.data.lc_process_title;
            var lc_process_mxgraph_style = result.data.lc_process_mxgraph_style;
            var lc_process_mxgraphxml = result.data.lc_process_mxgraphxml;
            var lc_process_remark = result.data.lc_process_remark;
            var lc_process_id = result.data.lc_process_id;
            var lc_process_uid = result.data.lc_process_uid;
            var xt_constant_id = result.data.xt_constant_id;
            var lc_product_id =  result.data.lc_product_id;
            var lc_group_id =  result.data.lc_group_id;
            var moduleKey = result.data.moduleKey;
            var candidateStarterUsers = result.data.candidateStarterUsers;
            var candidateStarterGroups = result.data.candidateStarterGroups
            var candidate_group_type = result.data.candidate_group_type
            //////////////////////////////////开始导入XML文件///////////////////////////
            var graph = graph_refresh;
            graph.getModel().beginUpdate();
            try{
                /*
                 * 直接读取流程图的xml, 并展示流程图
                */
                if(lc_process_mxgraphxml != null && lc_process_mxgraphxml.length > 0){
                    var doc = mxUtils.parseXml(lc_process_mxgraphxml);
                    var dec = new mxCodec(doc);
                    dec.decode(doc.documentElement, graph.getModel());
                }
                validatePOOL(graph);
            }catch (e){
                console.log("---导入流程出现异常---",e);
            }finally{
                graph_refresh.getModel().endUpdate();
                linetostyle(lc_process_mxgraph_style,graph);
                graph_refresh.refresh();
                $('#processId').val(lc_process_uid);
                $('#processName').val(lc_process_title);
                $('#mxgraphxml').val(lc_process_mxgraphxml);
                $('#lc_process_mxgraph_style').val(lc_process_mxgraph_style);
                $('#lc_process_id').val(lc_process_id);
                $('#remark').val(lc_process_remark);
                $('#xt_constant_id').val(xt_constant_id);
                $('#lc_product_id').val(lc_product_id);
                $('#lc_group_id').val(lc_group_id);
                $("#moduleKey").val(moduleKey);
                $("#candidateStarterUsersTemp").val(candidateStarterUsers);
                $("#candidateStarterGroupsTemp").val(candidateStarterGroups);
                $("#candidate_group_typeTemp").val(candidate_group_type);
                initBaseFormData();
                $('#processSelectModal').modal('hide');
            }
            //////////////////////////////////结束导入XML文件///////////////////////////
        }
    });
}

/**
 * 执行导入
 * @param id
 */
function doImpl(id){
    msgTishCallFnBoot("确定导入该流程数据？",function(){
        doProcess(id);
    });
}

/**
 * 初始化处理人
 * 用户选择器
 * @param flag flag标识是1单个用户选择2支持多选----------
 * @param type type类型1在UserTask中使用2在“流程基本信息使用”3在泳道中使用
 */
var userFlag;
var userType;
function initassignee(flag,type){
    userFlag = flag;
    userType = type;
    var UserinfoSelectModalCount = 0 ;
    $('#UserinfoBody').height(reGetBodyHeight()-218);
    $('#UserinfoSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#UserinfoSelectModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#UserinfoModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++UserinfoSelectModalCount == 1){
            $('#searchFormUserinfo')[0].reset();
            var opt = {
                searchformId:'searchFormUserinfo'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        sClass:"text-center",
                        width:"20px",
                        data:"xt_userinfo_id",
                        render:function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUserinfo" value="' + data + '" /><span></span></label>';
                        },
                        bSortable:false
                    },
                    {
                        data:"xt_userinfo_id",
                        width:"20px"
                    },
                    {
                        data:'xt_userinfo_name'
                    },
                    {
                        data:'xt_userinfo_realName'
                    },
                    {
                        data:'xt_userinfo_phone'
                    },
                    {
                        data:'xt_userinfo_origo'
                    },
                    {
                        data:'xt_userinfo_birthday'
                    },
                    {
                        data:'xt_userinfo_email'
                    }
                ]
            });
            grid=$('#UserinfoDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallUserinfo','checkchildUserinfo');
            //实现单击行选中
            clickrowselected('UserinfoDatatables');
        }
    });
}

/**
 * 执行用户选择
 */
function doSelectUser(){
    if(returncheckedLength('checkchildUserinfo') <= 0){
        toastrBoot(4,"请选择员工");
        return;
    }
    //CAS 1. Usertask中候选人可选择
    if(userFlag == 2 && userType == 1) {
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        var params = {xt_userinfo_id:xt_userinfo_id};
        //加载表单数据
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/users",params,function(result){
            var arr = result.data;
            var xt_userinfo_realName = null;
            if(undefined != result.data && null != result.data){
                for (var i = 0; i < arr.length; i++) {
                    //名称维护
                    if (null == xt_userinfo_realName) {
                        xt_userinfo_realName =arr[i].xt_userinfo_realName;
                    } else {
                        xt_userinfo_realName = xt_userinfo_realName + "," + arr[i].xt_userinfo_realName;
                    }
                }
            }
            if(null != xt_userinfo_realName){
                msgTishCallFnBoot('确定要选择:<br>'+ xt_userinfo_realName + '？',function(){
                    //清空使用表达式
                    $('#useExpByCandidateUsers').val("1");
                    $('#useExpValueByCandidateUsers').val("");

                    $('#candidateUsers').val(xt_userinfo_id);
                    $('#candidateUsers_Text').val(xt_userinfo_realName);
                    setUserTaskValue();
                    $('#UserinfoSelectModal').modal('hide');
                });
            }else{
                $('#UserinfoSelectModal').modal('hide');
            }

        },null,"post");
    }

    //CAS 2.基础信息中发起人 单选
    //Usertask中处理人选择
    if(userFlag == 1 && userType == 1){
        if(returncheckedLength('checkchildUserinfo') != 1){
            toastrBoot(4,"员工只能选择一条记录！");
            return;
        }
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        //加载表单数据
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+xt_userinfo_id,{},function(result){
            var xt_userinfo_realName = result.data.xt_userinfo_realName;
            var str ="[<font color=red><br>用户姓名:" +result.data.xt_userinfo_realName + "<br>所属部门:" + result.data.xt_departinfo_name+"<br>所属岗位:"+result.data.xt_post_name+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){

                //清空使用表达式 并设置生效基于人员
                $('#useExpByAssignee').val("1");
                $('#useExpValueByAssignee').val("");

                $('#assignee').val(xt_userinfo_id);
                $('#assignee_text').val(xt_userinfo_realName);
                setUserTaskValue();
                $('#UserinfoSelectModal').modal('hide');
            });
        });
    }
    //“流程基本信息使用”中处理人选择
    if(userFlag == 1 && userType == 2){
        if(returncheckedLength('checkchildUserinfo') != 1){
            toastrBoot(4,"员工只能选择一条记录！");
            return;
        }
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        //加载表单数据
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+xt_userinfo_id,{},function(result){
            var xt_userinfo_realName = result.data.xt_userinfo_realName;
            var str ="[<font color=red><br>用户姓名:" +result.data.xt_userinfo_realName + "<br>所属部门:" + result.data.xt_departinfo_name+"<br>所属岗位:"+result.data.xt_post_name+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){
                $('#candidateStarterUsersTemp').val(xt_userinfo_id);
                $('#candidateStarterUsersTemp_Text').val(xt_userinfo_realName);
                initBaseFormData();
                $('#UserinfoSelectModal').modal('hide');
            });
        });
    }
    //泳道中使用 处理人选择
    if(userFlag == 1 && userType == 3){
        if(returncheckedLength('checkchildUserinfo') != 1){
            toastrBoot(4,"员工只能选择一条记录！");
            return;
        }
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        //加载表单数据
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+xt_userinfo_id,{},function(result){
            var xt_userinfo_realName = result.data.xt_userinfo_realName;
            var str ="[<font color=red><br>用户姓名:" +result.data.xt_userinfo_realName + "<br>所属部门:" + result.data.xt_departinfo_name+"<br>所属岗位:"+result.data.xt_post_name+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){
                $('#candidateStarterUsers_').val(xt_userinfo_id);
                $('#candidateStarterUsers_Text_').val(xt_userinfo_realName);
                setPoolValue();
                $('#UserinfoSelectModal').modal('hide');
            })
        });
    }
}

//加载数据完成事件
function onAsyncSuccess(event, treeId, treeNode, msg){
    closeWating(null,dialogWating);
}


//单击事件
function onClick(event, treeId, treeNode, msg){
    var id = treeNode.id;
    var tempObject = treeNode.tempObject;
    if(tempObject == 'DEPART'){

    }
    if(tempObject == 'POST'){

    }
}

var dialogWating;
/**
 * 初始数据
 * @param type 组类型：0按部门 1按岗位 2按角色
 * @param nodeType 节点类型：1任务，2基本流程中组，3泳道
 */
function initGroups(type,nodeType) {
    var url = "";
    if(type == 0){//按部门
        url = sysModules + "/xtOrg/depart/tree";
    }
    if(type == 1){//按岗位
        url = sysModules + "/xtOrg/post/tree";

    }if(type == 2){//按角色
        url = oauthModules + "/oauthRole/trees";
    }
    if ($('#orgTree').jstree()) {//判断是否已有实例
        $('#orgTree').jstree().destroy();//销毁实例
    };
    ajaxBRequestCallFn(url,{},function(result){
        if(undefined == result.data || null == result.data || "" == result.data){
            $("#orgTree").html("暂无数据！");
        }else{
            // data = result.data;
            data=eval("("+result.data+")");
            $('#orgTree').jstree({ "types" : {
                    //////////样式一///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file"
                    // }
                    ///////样式二///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder m--font-warning"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file  m--font-warning"
                    // }
                    ///////样式三///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder m--font-warning"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file  m--font-warning"
                    // }
                    ///////样式四///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder m--font-brand"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file  m--font-brand"
                    // }
                    // /////样式五///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder m--font-brand"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file  m--font-brand"
                    // }
                    // /////样式六///////////
                    "default" : {
                        "icon" : "fa fa-folder m--font-success"
                    },
                    "file" : {
                        "icon" : "fa fa-file  m--font-success"
                    }
                },
                "animation" : 10,
                "state" : { "key" : "demo2" },
                "plugins" : ["dnd", "state", "types",'checkbox' ,"search"],
                'core' : {
                    "themes" : {
                        "responsive": false,
                        "stripes":true,
                        "ellipsis": true //节点名过长时是否显示省略号。
                    },
                    'data' : data
                }
            }).on('select_node.jstree',function(e,data){

                /*选中之后触发*/
            }).on(' deselect_node.jstree',function(e,data){
                /*取消选中触发*/
            }).on("loaded.jstree", function (event, data) {
                data.instance.clear_state();/*此句用来清除之前选中的数据不可以去掉*/
                // data.instance.open_all(-1); // -1 打开所有节点
                data.instance.open_all(2); // -1 打开所有节点
                // $('#orgTree').jstree().open_all();/*初始化打开树，对于复选框。 不展开的话，如果下面有逻辑选择则会有问题*/
                // //选择指定节点
                // var tempIds = $("#ids").val();
                // var tempIdsArr = tempIds.split(",");
                // $.each(TempIdsArr,function(index,value){
                //     var id=value;
                //     $('#orgTree').jstree('select_node',id,true,true);/*选中id对应的节点*/
                // });
                initTreeSelect(nodeType);
            }).on("load_node.jstree",function(event,data){
                /*加载node时候触发*/
            });
        }
    });
}


/**
 * 初始化候选组（采用部门，岗位等）
 * @param flag flag标识是1单个组选择2支持多选
 * @param type type类型1在UserTask中使用2在“流程基本信息使用”3在泳道中使用
 * 标准格式 如：DPERT:10000,DEPART:10002
 */
var orgFlag;
var orgType;
function initcandidateGroups(flag,type){
    var candidate_group_type = $("#candidate_group_type").val();
    if(null == candidate_group_type || "" == candidate_group_type){
        toastrBoot(4,"请选择组类型！");
        return;
    }
    orgFlag = flag;
    orgType = type;
    var OrgSelectModalCount = 0 ;
    $('#OrgSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#OrgSelectModal').on("shown.bs.modal",function(){
        if(++OrgSelectModalCount == 1){
            initGroups(candidate_group_type,type);
        }
    });
}

/**
 * 处理组织机构选择
 */
function doOrgSelect(){
    var candidate_group_type = $("#candidate_group_type").val();
    var idType = "";
    if(candidate_group_type == 0){//按部门
        idType = "DEPART"
    }
    if(candidate_group_type == 1){//按岗位
        idType = "POST"

    }if(candidate_group_type == 2){//按角色
        idType = "ROLE"
    }
    if(candidate_group_type == 3){//按表达式
        idType = "BDS"
    }
    var treeNode = $('#orgTree').jstree(true).get_selected(true); //获取所有选中的节点对象
    var treeNode = $('#orgTree').jstree(true).get_checked(true); //获取所有 checkbox 选中的节点对象
    // var ids = $('#orgTree').jstree().get_checked();//获取所有 checkbox 选中的节点id
    if(undefined == treeNode || null == treeNode || "" == treeNode || null == candidate_group_type || "" == candidate_group_type){
        toastrBoot(4,"未选择节点！");
        return;
    }
    var text = null;
    var id = null;
    for(var i = 0; i < treeNode.length; i++) {
        //编号维护
        if(null == id){
            id= idType+":"+treeNode[i].id
        }else{
            id=id+","+idType+":"+treeNode[i].id;
        }
        //名称维护
        if(null == text){
            text=treeNode[i].text;
        }else{
            text=text+","+treeNode[i].text;
        }
    }
    msgTishCallFnBoot('确定保存所选的数据？',function(){
        //任务中使用
        if(orgType == 1){
            $('#candidateGroups').val(id);
            $('#candidateGroups_Text').val(text);

            //清空激活表达式
            $('#useExpByCandidateGroups').val("1");//表示使用表达式
            $('#useExpValueByCandidateGroups').val("");

            setUserTaskValue();
        }
        //主流程中使用
        if(orgType == 2){
            $('#candidateStarterGroups').val(id);
            $('#candidateStarterGroupsTemp').val(id);
            $('#candidateStarterGroups_Text').val(text);
            $('#candidateStarterGroupsTemp_Text').val(text);
            setBaseFormInfo();
        }
        //泳道池中使用
        if(orgType == 3){
            $('#candidateStarterGroups_').val(id);
            $('#candidateStarterGroups_Text_').val(text);
            setPoolValue();
        }
        $('#OrgSelectModal').modal('hide');
    })

}

//连线样式设置虚线
function connectEdge(editor){
	if (editor.defaultEdge != null){
		editor.defaultEdge.style = 'straightEdge';
	}
}

/**
 * 统一赋值操作
 * @param thiz
 * @param modes
 */
function setInputValue(thiz,modes){
    if(modes == "BaseInfo"){
        setBaseFormInfo();
    }
    if(modes == "UserTask"){
        setUserTaskValue();
    }
    if(modes == "BusinessRuleTask"){
        setBusinessRuleTaskValue();
    }
    if(modes == "CallActivity"){
        setCallActivityValue();
    }
    if(modes == "CancelBoundary"){
        setCancelBoundaryEventValue();
    }
    if(modes == "CancelEndEvent"){
        setCancelEndEventValue();
    }
    if(modes == "BompensationBoundaryEvent"){
        setBompensationBoundaryEventValue();
    }
    if(modes == "CompensationThrowingEvent"){
        setCompensationThrowingEventValue();
    }
    if(modes == "EndValue"){
        setEndValue();
    }
    if(modes == "ErrorBoundaryEvent"){
        setErrorBoundaryEventValue();
    }
    if(modes == "ErrorEndEvent"){
        setErrorEndEventValue();
    }
    if(modes == "ErrorStartEvent"){
        setErrorStartEventValue();
    }
    if(modes == "EventGateway"){
        setEventGatewayValue();
    }
    if(modes == "EventSubProcess"){
        setEventSubProcessValue();
    }
    if(modes == "ExclusiveGateway"){
        setExclusiveGatewayValue();
    }
    if(modes == "InclusiveGateway"){
        setInclusiveGatewayValue();
    }
    if(modes == "lane"){
        setlaneValue();
    }
    if(modes == "MailTask"){
        setMailTaskValue();
    }
    if(modes == "ManualTask"){
        setManualTaskValue();
    }
    if(modes == "MessageBoundaryEvent"){
        setMessageBoundaryEventValue();
    }
    if(modes == "MessageCatchingEvent"){
        setMessageCatchingEventValue();
    }
    if(modes == "MessageStartEvent"){
        setMessageStartEventValue();
    }
    if(modes == "NoneThrowing"){
        setNoneThrowingValue();
    }
    if(modes == "ParallelGateway"){
        setParallelGatewayValue();
    }
    if(modes == "Pool"){
        setPoolValue();
    }
    if(modes == "ReceiveTask"){
        setReceiveTaskValue();
    }
    if(modes == "ScriptTask"){
        setScriptTaskValue();
    }
    if(modes == "ServiceTask"){
        setServiceTaskValue();
    }
    if(modes == "SignalBoundaryEvent"){
        setSignalBoundaryEventValue();
    }
    if(modes == "SignalCatchingEvent"){
        setSignalCatchingEventValue()
    }
    if(modes == "SignalStartEvent"){
        setSignalStartEventValue()
    }
    if(modes == "SignalThrowingEvent"){
        setSignalThrowingEventValue()
    }
    if(modes == "Start"){
        setStartValue()
    }
    if(modes == "SubProcess"){
        setSubProcessValue()
    }
    if(modes == "TerminateEndEvent"){
        setTerminateEndEvent()
    }
    if(modes == "TimerBoundaryEvent"){
        setTimerBoundaryEventValue()
    }
    if(modes == "TimerCatchingEvent"){
        setTimerCatchingEventValue()
    }
    if(modes == "TimerStartEvent"){
        setTimerStartEventValue()
    }
    if(modes == "TransactionProcess"){
        setTransactionProcessValue()
    }
    if(modes == "Transition"){
        setTransitionValue()
    }

}

/**
*点击节点渲染处理人，处理组等
*type 1任务中 2泳道 3主页面
**/
function initACC(assignee,candidateUsers,candidateGroups,type){
    if(type == 1){
		//任务中处理人，候选人，处理组等节点操作
		if(null != assignee && "" != assignee){
		    if($('#useExpByAssignee').val() == 0){
		        //放行
            }else{
                //加载表单数据
                ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+assignee,{},function(result){
                    var xt_userinfo_realName = result.data.xt_userinfo_realName;
                    $('#assignee_text').val(xt_userinfo_realName);
                });
            }
		}
		if(null != candidateUsers && "" != candidateUsers){
		    if($('#useExpByCandidateUsers').val() == 0){//放行
            }else{
                var params = {xt_userinfo_id:candidateUsers};
                //加载表单数据
                ajaxBRequestCallFn(sysModules+"/xtUserinfo/users",params,function(result){
                    var arr = result.data;
                    var xt_userinfo_realName = null;
                    for (var i = 0; i < arr.length; i++) {
                        //名称维护
                        if (null == xt_userinfo_realName) {
                            xt_userinfo_realName =arr[i].xt_userinfo_realName
                        } else {
                            xt_userinfo_realName = xt_userinfo_realName + "," + arr[i].xt_userinfo_realName;
                        }
                    }
                    $('#candidateUsers_Text').val(xt_userinfo_realName);
                },null,"post");
            }
		}
		if(null != candidateGroups && "" != candidateGroups){
            var candidate_group_type = $("#candidate_group_type").val();
		    if($('#useExpByCandidateGroups').val() == 0){
		       //放行
            }else{
                $('#candidateGroups_Text').val('');
                var orgArray = candidateGroups.split(',');
                var xt_departinfo_id = "";
                var xt_post_id = "";
                var role_id = "";
                var candidateGroups_Text = null;
                for(var i = 0; i < orgArray.length; i++){
                    var org = orgArray[i];
                    var org_ = org.split(":");
                    if(candidate_group_type == 0){//按部门
                        if(null != xt_departinfo_id && '' != xt_departinfo_id){
                            xt_departinfo_id = xt_departinfo_id+","+org_[1];
                        }else{
                            xt_departinfo_id = org_[1];
                        }
                    }
                    if(candidate_group_type == 1){//按岗位
                        if(null != xt_post_id && '' != xt_post_id){
                            xt_post_id = xt_post_id + "," + org_[1];
                        }else{
                            xt_post_id = org_[1];
                        }

                    }if(candidate_group_type == 2){//按角色
                        if(null != role_id && '' != role_id){
                            role_id = role_id + "," + org_[1];
                        }else{
                            role_id = org_[1];
                        }
                    }
                }
                ///////////Task任务中部门开始///////////
                if(null != xt_departinfo_id && '' != xt_departinfo_id){//处理部门
                    ajaxBRequestCallFn(sysModules+"/xtDepartinfo/list/"+xt_departinfo_id,null,function(result){
                        var arr = result.data;
                        for (var i = 0; i < arr.length; i++) {
                            if(null != candidateGroups_Text){
                                candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_departinfo_name;
                            }else{
                                candidateGroups_Text = arr[i].xt_departinfo_name;
                            }
                        }
                        $('#candidateGroups_Text').val(candidateGroups_Text);
                    },null,"GET");
                }
                ///////////Task任务中部门结束///////////

                ///////////Task任务中岗位开始///////////
                if(null != xt_post_id && '' != xt_post_id){//处理岗位
                    ajaxBRequestCallFn(sysModules+"/xtPost/list/"+xt_post_id,null,function(result){
                        var arr = result.data;

                        for (var i = 0; i < arr.length; i++) {
                            if(null != candidateGroups_Text){
                                candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_post_name;
                            }else{
                                candidateGroups_Text = arr[i].xt_post_name;
                            }
                        }
                        $('#candidateGroups_Text').val(candidateGroups_Text);
                    },null,"GET");
                }
                ///////////Task任务中岗位结束///////////

                ///////////Task任务中角色开始///////////
                if(null != role_id && '' != role_id){//处理角色
                    ajaxBRequestCallFn(oauthModules+"/oauthRole/list/"+role_id,null,function(result){
                        var arr = result.data;
                        for (var i = 0; i < arr.length; i++) {
                            if(null != candidateGroups_Text){
                                candidateGroups_Text = candidateGroups_Text+","+arr[i].role_name;
                            }else{
                                candidateGroups_Text = arr[i].role_name;
                            }
                        }
                        $('#candidateGroups_Text').val(candidateGroups_Text);
                    },null,"GET");
                }
                ///////////Task任务中角色结束///////////
            }
		}
	}
	if(type == 2){
        //主流程发起人，发起人组等节点操作
        if(null != assignee && "" != assignee){
            ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+assignee,{},function(result){
                var xt_userinfo_realName = result.data.xt_userinfo_realName;
                $('#candidateStarterUsers_Text').val(xt_userinfo_realName);
            });
        }
        if(null != candidateGroups && "" != candidateGroups){
            var candidate_group_type = $("#candidate_group_type").val();
            $('#candidateStarterGroups_Text').val('');
            var orgArray = candidateGroups.split(',');
            var xt_departinfo_id = "";
            var xt_post_id = "";
            var role_id = "";
            var candidateGroups_Text = null;
            for(var i = 0; i < orgArray.length; i++){
                var org = orgArray[i];
                var org_ = org.split(":");
                if(candidate_group_type == 0){//按部门
                    if(null != xt_departinfo_id && '' != xt_departinfo_id){
                        xt_departinfo_id = xt_departinfo_id+","+org_[1];
                    }else{
                        xt_departinfo_id = org_[1];
                    }
                }
                if(candidate_group_type == 1){//按岗位
                    if(null != xt_post_id && '' != xt_post_id){
                        xt_post_id = xt_post_id + "," + org_[1];
                    }else{
                        xt_post_id = org_[1];
                    }
                }if(candidate_group_type == 2){//按角色
                    if(null != role_id && '' != role_id){
                        role_id = role_id + "," + org_[1];
                    }else{
                        role_id = org_[1];
                    }
                }
            }

            ///////////主流程部门开始///////////
            if(null != xt_departinfo_id && "" != xt_departinfo_id){//处理部门
                ajaxBRequestCallFn(sysModules+"/xtDepartinfo/list/"+xt_departinfo_id,null,function(result){
                    var arr = result.data;
                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_departinfo_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_departinfo_name;
                        }
                    }
                    $('#candidateStarterGroups_Text').val(candidateGroups_Text);
                },null,"GET");
            }
            ///////////主流程部门结束///////////


            ///////////主流程岗位开始///////////
            if(null != xt_post_id && "" != xt_post_id) {//处理岗位
                ajaxBRequestCallFn(sysModules + "/xtPost/list/" + xt_post_id, null, function (result) {
                    var arr = result.data;
                    for (var i = 0; i < arr.length; i++) {
                        if (null != candidateGroups_Text) {
                            candidateGroups_Text = candidateGroups_Text + "," + arr[i].xt_post_name;
                        } else {
                            candidateGroups_Text = arr[i].xt_post_name;
                        }
                    }
                    $('#candidateStarterGroups_Text').val(candidateGroups_Text);
                }, null, "GET");
            }
            ///////////主流程岗位结束///////////

            ///////////主流程角色开始///////////
            if(null != role_id && '' != role_id){//处理角色
                ajaxBRequestCallFn(oauthModules+"/oauthRole/list/"+role_id,null,function(result){
                    var arr = result.data;
                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].role_name;
                        }else{
                            candidateGroups_Text = arr[i].role_name;
                        }
                    }
                    $('#candidateStarterGroups_Text').val(candidateGroups_Text);
                },null,"GET");
            }
            ///////////主流程中角色结束///////////
        }
	}
	if(type == 3){
		//泳道池中流程发起人，发起人组等节点操作
		if(null != assignee && "" != assignee){
            ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+assignee,{},function(result){
                var xt_userinfo_realName = result.data.xt_userinfo_realName;
                $('#candidateStarterUsers_Text_').val(xt_userinfo_realName);
            });
		}
		if(null != candidateGroups && "" != candidateGroups){
            var candidate_group_type = $("#candidate_group_type").val();
            $('#candidateStarterGroups_Text_').val('');
            var orgArray = candidateGroups.split(',');
            var xt_departinfo_id = "";
            var xt_post_id = "";
            var role_id = "";
            var candidateGroups_Text = null;
            for(var i = 0; i < orgArray.length; i++){
                var org = orgArray[i];
                var org_ = org.split(":");
                if(candidate_group_type == 0){//按部门
                    if(null != xt_departinfo_id && '' != xt_departinfo_id){
                        xt_departinfo_id = xt_departinfo_id+","+org_[1];
                    }else{
                        xt_departinfo_id = org_[1];
                    }
                }
                if(candidate_group_type == 1){//按岗位
                    if(null != xt_post_id && '' != xt_post_id){
                        xt_post_id = xt_post_id + "," + org_[1];
                    }else{
                        xt_post_id = org_[1];
                    }
                }if(candidate_group_type == 2){//按角色
                    if(null != role_id && '' != role_id){
                        role_id = role_id + "," + org_[1];
                    }else{
                        role_id = org_[1];
                    }
                }
            }

            ///////////泳道中部门开始///////////
            if(null != xt_departinfo_id && "" != xt_departinfo_id){//处理部门
                ajaxBRequestCallFn(sysModules+"/xtDepartinfo/list/"+xt_departinfo_id,null,function(result){
                    var arr = result.data;
                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_departinfo_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_departinfo_name;
                        }
                    }
                    $('#candidateStarterGroups_Text_').val(candidateGroups_Text);
                },null,"GET");
            }
            ///////////泳道中部门结束///////////

            ///////////泳道中岗位开始///////////
            if(null != xt_post_id && "" != xt_post_id) {//处理岗位
                ajaxBRequestCallFn(sysModules + "/xtPost/list/" + xt_post_id, null, function (result) {
                    var arr = result.data;
                    for (var i = 0; i < arr.length; i++) {
                        if (null != candidateGroups_Text) {
                            candidateGroups_Text = candidateGroups_Text + "," + arr[i].xt_post_name;
                        } else {
                            candidateGroups_Text = arr[i].xt_post_name;
                        }
                    }
                    $('#candidateStarterGroups_Text_').val(candidateGroups_Text);
                }, null, "GET");
                ///////////泳道中岗位结束///////////
            }
            ///////////泳道中角色开始///////////
            if(null != role_id && '' != role_id){//处理角色
                ajaxBRequestCallFn(oauthModules+"/oauthRole/list/"+role_id,null,function(result){
                    var arr = result.data;
                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].role_name;
                        }else{
                            candidateGroups_Text = arr[i].role_name;
                        }
                    }
                    $('#candidateStarterGroups_Text_').val(candidateGroups_Text);
                },null,"GET");
            }
            ///////////泳道中角色结束///////////
		}
	}
}

/**
 * 导入流程
 * @param graph
 * @param history
 */
function importP(graph,history){
    imp(graph,history)
}

var graph_;
function saveP(graph){
    reValidate(graph);//验证连线
    ////////////////////V1.0版本实现///////////////////////
    //获取mxgraph拓扑图数据
    // var enc = new mxCodec(mxUtils.createXmlDocument());
    // var node = enc.encode(graph.getModel());
    // var mxgraphxml = mxUtils.getPrettyXml(node);
    // mxgraphxml = mxgraphxml.replace(/\"/g,"'");
    // //mxgraphxml = encodeURIComponent(mxgraphxml);
    // var xmlDoc = mxUtils.createXmlDocument();
    // var root = xmlDoc.createElement('output');
    // xmlDoc.appendChild(root);
    // var xmlCanvas = new mxXmlCanvas2D(root);
    // var imgExport = new mxImageExport();
    // imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);
    // var bounds = graph.getGraphBounds();
    // V1.0非编辑器使用方法（获取宽度，高度）
    // var w = Math.round(bounds.x + bounds.width + 4);
    // var h = Math.round(bounds.y + bounds.height + 4);
    // var imgxml = mxUtils.getXml(root);
    // //imgxml = "<output>"+imgxml+"</output>";
    // //imgxml = encodeURIComponent(imgxml);

    ////////////////////V2.0版本实现///////////////////////
    var enc = new mxCodec(mxUtils.createXmlDocument());
    var node = enc.encode(graph.getModel());
    var mxgraphxml = mxUtils.getPrettyXml(node);
    mxgraphxml = mxgraphxml.replace(/\"/g,"'");

    // var s = Math.max(0, parseFloat(zoomInput.value) || 100) / 100;//当前缩放值
    // var b = Math.max(0, parseInt(borderInput.value));//边框宽度

    var s = 100 / 100;//当前缩放值
    var b = 0;//边框宽度
    var bg = graph.background;//背景颜色

    var bounds = graph.getGraphBounds();

    // New image export
    var xmlDoc = mxUtils.createXmlDocument();

    var root = xmlDoc.createElement('output');
    xmlDoc.appendChild(root);
    // Renders graph. Offset will be multiplied with state's scale when painting state.
    var xmlCanvas = new mxXmlCanvas2D(root);
    xmlCanvas.translate(Math.floor((b / s - bounds.x) / graph.view.scale),
        Math.floor((b / s - bounds.y) / graph.view.scale));
    xmlCanvas.scale(s / graph.view.scale);
    /*
    var cells =graph.getModel().cells;
    for(var i in cells){
        if(undefined !=  cells[i].geometry){
            cells[i].geometry.nodeID = cells[i].nodeID;
            cells[i].geometry.nodeType = cells[i].node_type;
        }
    }*/
    var imgExport = new mxImageExport();
    imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);
    //V2.0在编辑器中获取画板宽度 高度
    // Puts request data together
    var w = Math.ceil(bounds.width * s / graph.view.scale + 2 * b);
    var h = Math.ceil(bounds.height * s / graph.view.scale + 2 * b);
    /*
    更加紧凑型 暂时不用
    var scale = graph.view.scale;
    var w = Math.ceil(bounds.width / scale);
    var h = Math.ceil(bounds.height / scale);
    */

    imgxml = mxUtils.getXml(root);
    saveProcess(mxgraphxml,w,h,imgxml,graph);
}

/**
 * 导出图片
 * @param graph
 * @param editor
 */
function exportP(graph,editor){
    if(null == $("#lc_process_id").val() || '' == $("#lc_process_id").val()){
        toastrBoot(4,"流程未生成，不能导出！");
        return;
    }
    msgTishCallFnBoot("确定要导出？",function(){
        downOrExportB(workflowModules+'/lcProcess/export?lc_process_id='+$("#lc_process_id").val());
        // var scale = graph.view.scale;
        // var bounds = graph.getGraphBounds();
        //
        // // New image export
        // var xmlDoc = mxUtils.createXmlDocument();
        // var root = xmlDoc.createElement('output');
        // xmlDoc.appendChild(root);
        //
        // // Renders graph. Offset will be multiplied with state's scale when painting state.
        // var xmlCanvas = new mxXmlCanvas2D(root);
        // xmlCanvas.translate(Math.floor(1 / scale - bounds.x), Math.floor(1 / scale - bounds.y));
        // xmlCanvas.scale(scale);
        //
        // var imgExport = new mxImageExport();
        // imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);
        //
        // // Puts request data together
        // var w = Math.ceil(bounds.width * scale + 2);
        // var h = Math.ceil(bounds.height * scale + 2);
        // var xml = mxUtils.getXml(root);
        //
        // // Requests image if request is valid
        // if (w > 0 && h > 0)
        // {
        //     var name = 'export.png';
        //     var format = 'png';
        //     var bg = '&bg=#FFFFFF';
        //
        //     new mxXmlRequest(editor.urlImage, 'filename=' + name + '&format=' + format +
        //         bg + '&w=' + w + '&h=' + h + '&xml=' + encodeURIComponent(xml)).
        //     simulate(document, '_blank');
        // }
    });

}

/**
 *
 */
function closeJehcLcWin(){
    $('#jehcLcModal').modal('hide');
}

/**
 *
 */
function initScroll(){
    // $("#sidebarContainer").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
    // $("#diagramContainer").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false}); // First scrollable DIV
}

/**
 *
 */
function nodeScroll(){
    // $("#mportletId").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
    // $("#mportletId1").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
    // $("#TabCol").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
}

/**
 *
 */
function serviceNodeAttributeFieldGridScroll(){
    // $("#serviceNodeAttributeFieldGrid").niceScroll({cursorborder:"",cursorcolor:"#e0e0e0",boxzoom:false});
}

/**
 * 初始化连线风格
 * @param flag
 * @param graph
 */
function linetostyle(flag,graph){
    console.log("linetostyle",flag);
    var line_style = basePath+'/deng/view/workflow/lc-design2.0/grapheditor/resources/default-style.xml';
    if(flag == 0){
        //如果为1直线
        line_style = basePath+'/deng/view/workflow/lc-design2.0/grapheditor/resources/default-style.xml';
    }else if(flag == 1){
        //如果为1曲线
        line_style = basePath+'/deng/view/workflow/lc-design2.0/grapheditor/resources/bight-style.xml';
    }
    var history = new mxUndoManager();
    //载入默认样式
    var node = mxUtils.load(line_style).getDocumentElement();
    var dec = new mxCodec(node.ownerDocument);
    dec.decode(node, graph.getStylesheet());
    var edgeStyle = graph.getStylesheet().getDefaultEdgeStyle();
    //edgeStyle[mxConstants.STYLE_EDGE] = mxEdgeStyle.TopToBottom;
    edgeStyle['gradientColor'] = '#c0c0c0';
    edgeStyle['strokeColor'] = '#c0c0c0'; //更改连线默认样式此处为颜色
    edgeStyle['dashed'] = '1'; //虚线
    edgeStyle['strokeWidth'] = 0.1;
    edgeStyle['fontSize'] = '8';
    edgeStyle['fontColor'] = '#000';
    edgeStyle['arrowWidth'] = 0.1;
    graph.alternateEdgeStyle = 'elbow=vertical';
    graph.refresh();
    $('#lc_process_mxgraph_style').val(flag);
}

/**
 * 载入XML流程图
 * @param graph
 * @param history
 */
function loadXml(graph,history){
    graph.getModel().beginUpdate();
    try{
        /*
         * 直接读取流程图的xml, 并展示流程图
        */
        var xml = '<root><mxCell id="0"/><mxCell id="1" parent="0"/><mxCell id="2" value="开始" style="roundImage;image=../view/workflow/lc-design/archive/grapheditor/images/activities/48/start.png" vertex="1" node_type="start" parent="1"><mxGeometry x="260" y="80" width="50" height="50" as="geometry"/></mxCell><mxCell id="3" value="状态" style="roundImage;image=../view/pc/lc-view/lc-design/archive/grapheditor/images/activities/48/state.png" vertex="1" node_type="state" parent="1"><mxGeometry x="260" y="290" width="50" height="50" as="geometry"/></mxCell><mxCell id="4" value="开始到状态" edge="1" parent="1" source="2" target="3"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="5" value="结束" style="roundImage;image=../view/pc/lc-view/lc-design/archive/grapheditor/images/activities/48/end.png" vertex="1" node_type="end" parent="1"><mxGeometry x="260" y="460" width="50" height="50" as="geometry"/></mxCell><mxCell id="6" value="状态到结束" edge="1" parent="1" source="3" target="5"><mxGeometry relative="1" as="geometry"/></mxCell></root>';
        xml = "<?xml version='1.0' encoding='utf-8'?><mxGraphModel>"+xml+"</mxGraphModel>";
        if(xml != null && xml.length > 0){
            var doc = mxUtils.parseXml(xml);
            var dec = new mxCodec(doc);
            dec.decode(doc.documentElement, graph.getModel());
        }
    }finally{
        graph.getModel().endUpdate();
        graph.refresh();
    }
}

$(document).ready(function() {
    $('#LcBaseForm').bootstrapValidator({
        message: '此值不是有效的'
    });
});

/**
 * 保存流程
 * @param mxgraphxml
 * @param w
 * @param h
 * @param imgxml
 * @param graph
 */
function saveProcess(mxgraphxml,w,h,imgxml,graph){
    if(null == $('#processName').val() || "" == $('#processName').val()){
        window.parent.toastrBoot(4,"请输入流程名称");
        return;
    }
    if(null == $('#moduleKey').val() || "" == $('#moduleKey').val()){
        window.parent.toastrBoot(4,"请输入模块Key");
        return;
    }
    msgTishCallFnBoot("确定保存该流程？",function(){
        $('#mxgraphxml').val(mxgraphxml);
        $('#imgxml').val(imgxml);
        $('#w').val(w);
        $('#h').val(h);
        submitBFormCallFn('LcBaseForm',workflowModules+'/lcProcess/create',function(result){
            try {
                var lc_process_id = result.data.lc_process_id;
                var lc_process_uid = result.data.lc_process_uid;
                if(lc_process_id != 0){
                    window.parent.toastrBoot(3,"保存流程成功");
                    $('#processId').val(lc_process_uid);
                    $('#lc_process_id').val(lc_process_id);
                    // $('#processName').val( result.data.lc_process_title);
                    // $('#mxgraphxml').val( result.data.lc_process_mxgraphxml);
                    // $('#lc_process_mxgraph_style').val( result.data.lc_process_mxgraph_style);
                    // $('#candidateStarterUsers').val( result.data.candidateStarterUsers);
                    // $('#remark').val( result.data.lc_process_remark);
                    // $('#xt_constant_id').val( result.data.xt_constant_id);
                }else{
                    window.parent.toastrBoot(4,"保存流程失败");
                }
            } catch (e) {

            }
        },null,null,formSetting());
    })
}


/**
 * 将字符串转换XML
 * @param xmlobj
 * @returns {*}
 */
function printString(xmlobj){
    var xmlDom;
    //IE
    if(document.all){
        xmlDom=new ActiveXObject("Microsoft.XMLDOM");
        xmlDom.loadXML(xmlobj);
    }
    //非IE
    else {
        xmlDom = new DOMParser().parseFromString(xmlobj, "text/xml");
    }
    return xmlDom;
}

/**
 *
 * @param graph
 */
function initProcess(graph){
    graph_refresh = graph;
    var lc_process_id = GetQueryString("lc_process_id");
    if(null != lc_process_id && '' != lc_process_id && undefined != lc_process_id){
        setTimeout(function(){
            doProcess(lc_process_id);
            $("#isSZ").val("0");
            doE()
        },100)
    }
}


/**
 *
 * 1.获取方法
     //始终从选择的mxcells返回数组的第一个细胞
     graph.getSelectionCell()
     //获取所有被选择的元素
     graph.getSelectionCells()
     //返回所选择所有cells的所有信息
     graph.getSelectionModel()
 2.设置方法
     //设置选中节点cell
     graph.setSelectionCell(v1);
 * @param graph
 */
function reValidate(graph) {
    var arrEdge = resultAllEdge(graph);
    for(var i in arrEdge){
        graph.getModel().beginUpdate();
        try
        {
            if(undefined === arrEdge[i].nodeID || null == arrEdge[i].nodeID || "" == arrEdge[i].nodeID){
                arrEdge[i].nodeID = "sequenceFlow"+guid();
            }
        }
        finally
        {
            graph.getModel().endUpdate();
        }
    }
}

/**
 *
 * @param cells
 */
function doCopyCells(graph,cells) {
    for(var i in cells){
        graph.getModel().beginUpdate();
        try
        {
            if(cells[i].edge){//连线
                cells[i].nodeID = "sequenceFlow"+guid();
            }else{
                if(undefined != cells[i].node_type){
                    cells[i].nodeID = cells[i].node_type + guid();
                    var arrVertex = getVertexByCell(cells[i]);
                    if(undefined != arrVertex && null != arrVertex && "" != arrVertex){
                        doChildren(graph,arrVertex)
                    }
                }
            }
        }
        finally
        {
            graph.getModel().endUpdate();
        }
    }
}

/**
 * 处理所有子节点
 * @param arrVertex
 */
function doChildren(graph,arrVertex) {
    console.log("---arrVertex----",arrVertex)
    for(var i in arrVertex){
        graph.getModel().beginUpdate();
        try {
            arrVertex[i].nodeID = arrVertex[i].node_type + guid();
        }
        finally
        {
            graph.getModel().endUpdate();
        }
    }
}



/**
 * 展开/收缩
 */
function expandOrg() {
    if($("#expandOrg").val() ==  0){
        $('#orgTree').jstree('open_all')//展开全部
        $("#expandOrg").val(1);
    }else{
        $('#orgTree').jstree('close_all');//收缩全部
        $("#expandOrg").val(0);
    }
}

/**
 * 全选/反选
 */
function checkAllOrg() {
    if($("#checkedOrg").val() ==  0){
        $("#checkedOrg").val(1)
        $('#orgTree').jstree().check_all();//选中所有节点
    }else{
        $('#orgTree').jstree().uncheck_all();//取消选中所有节点
        $("#checkedOrg").val(0)
    }
}


/**
 * 初始化组
 * @param nodeType 节点类型：1在UserTask中使用2在“流程基本信息使用”3在泳道中使用
 */
function initTreeSelect(nodeType) {
    console.log(nodeType);
    var candidateGroups = null;
    if(nodeType == 1){//任务
        candidateGroups =  $("#candidateGroups").val();
    }

    if(nodeType == 2){//主流程
        candidateGroups =  $("#candidateStarterGroupsTemp").val();
    }

    if(nodeType == 3){//泳道
        candidateGroups =  $("#candidateStarterGroups_").val();
    }



    var ids = null;
    if(null != candidateGroups && "" != candidateGroups){
        var orgArray = candidateGroups.split(',');
        for(var i = 0; i < orgArray.length; i++) {
            var org = orgArray[i];
            var org_ = org.split(":");
            if (null != ids && '' != ids) {
                ids = ids + "," + org_[1];
            } else {
                ids = org_[1];
            }
        }
    }

    if (null != ids && '' != ids) {
        // $('#orgTree').jstree().open_all();/*初始化打开树，对于复选框。 不展开的话，如果下面有逻辑选择则会有问题*/
        //选择指定节点
        var tempIds = ids;
        var tempIdsArr = tempIds.split(",");
        $.each(tempIdsArr,function(index,value){
            var id=value;
            $('#orgTree').jstree('select_node',id,true,true);/*选中id对应的节点*/
        });
    }
}

/**
 *
 * @param thiz
 * @param nodeType
 */
function canditateGroupChange(thiz,userType) {
    if(userType == 3){//主流程
        $("#candidateStarterGroups").val("");
        $("#candidateStarterGroups_Text").val("");
        $("#candidateStarterGroupsTemp").val("");
        $("#candidateStarterGroupsTemp_Text").val("");
        setInputValue(null,"BaseInfo")
    }
    if(userType == 2){//泳道
        $("#candidateStarterGroups_").val("");
        $("#candidateStarterGroups_Text_").val("");
        setInputValue(null,"Pool")
    }
    if(userType == 1){//任务
        $("#candidateGroups").val("");
        $("#candidateGroups_Text").val("");
        setInputValue(null,"UserTask")
    }
}

/**
 * 显示源码
 */
var myCodeMirror;
function showLcResources(){
    var attrib = getResources();
    var lcReSourcesSelectModalCount = 0 ;
    $('#lcReSourcesBody').height(reGetBodyHeight()-218);
    $('#lcReSourcesModal').modal({backdrop: 'static', keyboard: false});
    $('#lcReSourcesModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcReSourcesModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++lcReSourcesSelectModalCount == 1){
            // // 销毁
            if(null == myCodeMirror || undefined == myCodeMirror){
                myCodeMirror = CodeMirror.fromTextArea(document.getElementById('xmlRequestTextarea'),{
                    mode: "text/html",
                    lineNumbers: true
                });
            }
            myCodeMirror.setValue("");
            myCodeMirror.clearHistory();
            if(attrib.mxgraphxml!= undefined){
                myCodeMirror.setValue(attrib.mxgraphxml);
            }
            // $("#xmlRequestTextarea").val(attrib.mxgraphxml)
        }
    });
}

//获取 Codemirror 的值
//该方法得到的结果是经过转义的数据editor.getValue();
//得到的结果是未经过转义的数据editor.toTextArea();editor.getTextArea().value;

/**
 *
 */
function getResources() {
    var attrib = {};
    var graph = graph_;
    ////////////////////V2.0版本实现///////////////////////
    var enc = new mxCodec(mxUtils.createXmlDocument());
    var node = enc.encode(graph.getModel());
    var mxgraphxml = mxUtils.getPrettyXml(node);
    mxgraphxml = mxgraphxml.replace(/\"/g,"'");

    // var s = Math.max(0, parseFloat(zoomInput.value) || 100) / 100;//当前缩放值
    // var b = Math.max(0, parseInt(borderInput.value));//边框宽度

    var s = 100 / 100;//当前缩放值
    var b = 0;//边框宽度
    var bg = graph.background;//背景颜色

    var bounds = graph.getGraphBounds();

    // New image export
    var xmlDoc = mxUtils.createXmlDocument();

    var root = xmlDoc.createElement('output');
    xmlDoc.appendChild(root);
    // Renders graph. Offset will be multiplied with state's scale when painting state.
    var xmlCanvas = new mxXmlCanvas2D(root);
    xmlCanvas.translate(Math.floor((b / s - bounds.x) / graph.view.scale),
        Math.floor((b / s - bounds.y) / graph.view.scale));
    xmlCanvas.scale(s / graph.view.scale);
    /*
    var cells =graph.getModel().cells;
    for(var i in cells){
        if(undefined !=  cells[i].geometry){
            cells[i].geometry.nodeID = cells[i].nodeID;
            cells[i].geometry.nodeType = cells[i].node_type;
        }
    }*/
    var imgExport = new mxImageExport();
    imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);
    //V2.0在编辑器中获取画板宽度 高度
    // Puts request data together
    var w = Math.ceil(bounds.width * s / graph.view.scale + 2 * b);
    var h = Math.ceil(bounds.height * s / graph.view.scale + 2 * b);
    /*
    更加紧凑型 暂时不用
    var scale = graph.view.scale;
    var w = Math.ceil(bounds.width / scale);
    var h = Math.ceil(bounds.height / scale);
    */

    imgxml = mxUtils.getXml(root);
    attrib.imgxml = imgxml;
    attrib.w = w;
    attrib.h = h;
    attrib.mxgraphxml = mxgraphxml;
    return attrib;
}

/***
 * 创建自定义右键按钮
 * @param menu
 */
function createRightMenu(menu){
    menu.addItem('源码', null, function(){
        showLcResources();
    });
    // var submenu1 = menu.addItem('菜单...', null, null);
    // menu.addItem('菜单一', null, function(){
    //
    // }, submenu1);
    // menu.addItem('菜单二', null, function(){
    //
    // }, submenu1);
    // menu.addItem('菜单三', null, function(){
    //
    // }, submenu1);
}

/**
 *
 */
function copyLcReSources() {
    var textA = document.createElement('textarea');// 创建textArea
    textA.id = 'xmlCode';
    textA.value = myCodeMirror.getValue();
    document.getElementById('copy').appendChild(textA);// 追加
    document.getElementById('xmlCode').select();// 选中复制内容
    try {
        window.document.execCommand('Copy');//不能复制隐藏域
        document.getElementById('copy').innerHTML = '';//移除
        toastrBoot(3,"复制成功！");
    } catch (error) {
        toastrBoot(4,"复制失败，浏览器不支持！");
    }
}

/**
 * 选择区域格式化
 */
function autoFormatSelection() {
    var range = getSelectedRange();
    myCodeMirror.autoFormatRange(range.from, range.to);
}

/**
 * 选择范围
 * @returns {{from: *, to: *}}
 */
function getSelectedRange() {
    return { from: myCodeMirror.getCursor(true), to: myCodeMirror.getCursor(false) };
}

/**
 * 注释
 * @param isComment
 */
function commentSelection(isComment) {
    var range = getSelectedRange();
    myCodeMirror.commentRange(isComment, range.from, range.to);
}

/**
 * 全部格式化
 */
function formatAll() {
    CodeMirror.commands["selectAll"](myCodeMirror);
    autoFormatSelection();
}