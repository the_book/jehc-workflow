//连线节点[transition]
var transitionNodeAttributeForm;

/**
 * 连线
 * @param cell
 * @param graph_refresh
 * @param flag
 */
function showTransitionNodeAttributeWin(cell,graph_refresh,flag) {
    transitionNodeAttributePanel(cell, flag, graph_refresh);
}

/**
 * 连线属性
 * @param cell
 * @returns {string|*}
 */
function createTransitionNodeAttributeFormForm(cell){
    transitionNodeAttributeForm =
        "<div class=\"m-portlet\" id='mportletId' style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//标签宽度
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >标签宽度</label>"+
						"</div>"+
						"<div class=\"col-md-1\">"+
							"<input class=\"form-control m-input\" onchange='setInputValue(this,\"Transition\")' type=\"number\" maxlength='3' id=\"labelWidth\" name=\"labelWidth\" placeholder=\"请输入\">"+
						"</div>"+
					"</div>"+

					//表达式
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >表&nbsp;&nbsp;达&nbsp;式</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\"   onchange='setInputValue(this,\"Transition\")' maxlength=\"50\" id=\"skipexpression\" name=\"skipexpression\" placeholder=\"请输入表达式\">"+
						"</div>"+
					"</div>"+

					//条件
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >条&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;件</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<textarea class=\"form-control\" onchange='setInputValue(this,\"Transition\")' rows='3' maxlength=\"50\" id=\"condition\" name=\"condition\" placeholder=\"请输入条件\"></textarea>"+
						"</div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return transitionNodeAttributeForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initTransitionData(cell){
    var labelWidth = cell.labelWidth;
    var skipexpression = cell.skipexpression;
    var condition = cell.condition;
    $("#labelWidth").val(labelWidth);
    $("#skipexpression").val(skipexpression);
    $("#condition").val(condition);
}


/**
 * 设置内容
 */
function setTransitionValue(flag){
    var labelWidth = $("#labelWidth").val();
    var skipexpression = $("#skipexpression").val();
    var condition = $("#condition").val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,2)== false){
            return;
        }
        if(flag != 2){
            //2事件配置
            if(event_setvalue(JehcClickCell)== false){
                return;
            }
        }
        //3基本属性
        // if(null != labelWidth && "" != labelWidth){
            JehcClickCell.labelWidth = labelWidth;
        // }
        // if(null != skipexpression && "" != skipexpression){
            JehcClickCell.skipexpression = skipexpression;
        // }
        // if(null != condition && "" != condition){
            JehcClickCell.condition = condition;
        // }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}

/**
 * 连线属性
 * @param cell
 * @param flag
 * @param graph_refresh
 */
function transitionNodeAttributePanel(cell,flag,graph_refresh){
	if(flag == 2){
        transitionNodeAttributeForm = createTransitionNodeAttributeFormForm(cell,"Transition");
        nodeNormalForm = createNodeNormalForm(cell,2,"Transition");
        //Tab Index
        var Tab =
            "<div class='col-md-1'  id='TabCol'>"+
				"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本属性</a>"+

				// "<a href='javascript:setTransitionValue()' class='svBtn'>保存配置</a>"+
				"</div>"+
            "</div>"+

            "<div class='col-md-11'>"+
				"<div class=\"tab-content tab-content-default\">"+
					"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
					"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+transitionNodeAttributeForm+"</div>"+
				"</div>"+
            "</div>";
        var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>"+ Tab+ "</div></div>"
        $("#geSetContainer").empty();
        $("#geSetContainer").append(formInfo);
        //基本属性
        initTransitionData(cell)
        //一般属性 参数1表示非开始2其他
        initNodeNormalForm(cell,2);
	}else{
        transitionNodeAttributeForm = createTransitionNodeAttributeFormForm(cell,"Transition");
        nodeNormalForm = createNodeNormalForm(cell,2,"Transition");
        event_grid = creatEventGrid(cell,3);
        //Tab Index
        var Tab =
            "<div class='col-md-1'  id='TabCol'>"+
				"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
					"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

					"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">条件配置</a>"+

					"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">事件配置</a>"+

					// "<a href='javascript:setTransitionValue(3)' class='svBtn'>保存配置</a>"+
				"</div>"+
            "</div>"+

            "<div class='col-md-11'>"+
				"<div class=\"tab-content tab-content-default\">"+
					"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
					"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+transitionNodeAttributeForm+"</div>"+
					"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
				"</div>"+
            "</div>";
        var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>"+ Tab+ "</div></div>"
        $("#geSetContainer").empty();
        $("#geSetContainer").append(cExpansion);//收缩/展开
        $("#geSetContainer").append(formInfo);
        initSZ();
        //基本属性
        initTransitionData(cell);
        //一般属性 参数1表示非开始2其他
        initNodeNormalForm(cell,2);
        //共用taskGrid属性事件
        initevent_grid(cell,3);
        nodeScroll();
	}
}