var messageBoundaryEventForm;

/**
 * 消息边界
 * @param cell
 * @param graph_refresh
 * @private
 */
function messageBoundaryEventWin_(cell,graph_refresh){
	messageBoundaryEventPanel(cell,graph_refresh);
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function messageBoundaryEventPanel(cell,graph_refresh){
    nodeNormalForm = createNodeNormalForm(cell,2,"MessageBoundaryEvent");
    event_grid = creatEventGrid(cell,1);
    messageBoundaryEventForm = createMessageBoundaryEvent(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">基本属性</a>"+

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">监听事件</a>"+

				// "<a href='javascript:setMessageBoundaryEventValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+messageBoundaryEventForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,2);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
    //基本属性
    initMessageBoundaryEventData(cell);
    nodeScroll();
}

/**
 *
 * @returns {string|*}
 */
function createMessageBoundaryEvent(cell){
    var signalMessageBoundaryOption =
        "<option value='false'>false</option>" +
        "<option value='true'>true</option>";

    messageBoundaryEventForm =
        "<div class=\"m-portlet\" style='height:350px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//结束活动
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >结束活动</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<select class=\"form-control\" id='cancelActivity'  onchange='setInputValue(this,\"MessageBoundaryEvent\")' name=\"cancelActivity\">" +
								signalMessageBoundaryOption+
							"</select>"+
						"</div>"+
					"</div>"+

					//消息依附
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >消息依附</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\"  onchange='setInputValue(this,\"MessageBoundaryEvent\")' id=\"messageRef\" name=\"messageRef\" placeholder=\"请输入消息依附\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return messageBoundaryEventForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initMessageBoundaryEventData(cell){
    var cancelActivity = cell.cancelActivity;
    var messageRef = cell.messageRef;
    $('#cancelActivity').val(cancelActivity);
    $("#messageRef").val(messageRef);
}

/**
 * 设置内容
 */
function setMessageBoundaryEventValue(){
    var cancelActivity = $('#cancelActivity').val();
    var messageRef = $('#messageRef').val();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,2)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本属性
        // if(null != cancelActivity && "" != cancelActivity){
            JehcClickCell.cancelActivity = cancelActivity;
        // }
        //3基本属性
        // if(null != messageRef && "" != messageRef){
            JehcClickCell.messageRef = messageRef;
        // }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}