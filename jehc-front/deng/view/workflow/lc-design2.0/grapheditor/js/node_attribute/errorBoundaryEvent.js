var errorBoundaryEventNodeAttributeForm;
/**
 * 错误边界节点
 * @param cell
 * @param graph_refresh
 */
function showErrorBoundaryEventNodeAttributeWin(cell,graph_refresh){
    errorBoundaryEventNodeAttributePanel(cell,graph_refresh);
}

/**
 * @param cell
 * @returns {string|*}
 */
function createErrorBoundaryEventNodeAttributeForm(cell){
    errorBoundaryEventNodeAttributeForm =
        "<div class=\"m-portlet\" style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//错误编码
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >错误编码</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\"  onchange='setInputValue(this,\"ErrorBoundaryEvent\")' maxlength=\"50\"  data-bv-notempty data-bv-notempty-message=\"请输入错误编码\" id=\"errorcode\" name=\"errorcode\" placeholder=\"请输入错误编码\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return errorBoundaryEventNodeAttributeForm;
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function errorBoundaryEventNodeAttributePanel(cell,graph_refresh){
    nodeNormalForm = createNodeNormalForm(cell,2,"ErrorBoundaryEvent");
    event_grid = creatEventGrid(cell,1);
    errorBoundaryEventNodeAttributeForm = createErrorBoundaryEventNodeAttributeForm(cell);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>"+

				"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">基本属性</a>"+

        		"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">监听事件</a>"+

				// "<a href='javascript:setErrorBoundaryEventValue()' class='svBtn'>保存配置</a>"+
			"</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
			"<div class=\"tab-content tab-content-default\">"+
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+nodeNormalForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">"+errorBoundaryEventNodeAttributeForm+"</div>"+
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+event_grid+"</div>"+
			"</div>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();

    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell,2);
    //共用taskGrid属性事件
    initevent_grid(cell,1);
	//基本数据
    initErrorBoundaryEventData(cell);
    nodeScroll();
}

/**
 *
 * @param cell
 * @constructor
 */
function initErrorBoundaryEventData(cell){
    var errorcode = cell.errorcode;
    $("#errorcode").val(errorcode);
}

/**
 * 设置内容
 */
function setErrorBoundaryEventValue(){
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        var errorcode = $('#errorcode').val();
        // if(null != errorcode && '' !== errorcode){
            JehcClickCell.errorcode = errorcode;
        // }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}