var serviceTaskNodeAttributeForm;

/**
 * 服务任务节点
 * @param cell
 * @param graph_refresh
 */
function showServiceTaskNodeAttributeWin(cell,graph_refresh){
    serviceTaskNodeAttributePanel(cell,graph_refresh);
}

var serviceNodeAttributeFieldGrid;

/**
 *
 */
function displayField(){
	var val = $("#taskType").val();
    if(val == 'javaclass'){
        $('#classNameDiv').show();
        $('#expressionDiv').hide();
        $('#expression').val('');
        $('#delegateExpressionDiv').hide();
        $('#delegateExpression').val('');
    }
    if(val == 'delegateexpress'){
        $('#classNameDiv').hide();
        $('#className').val('');
        $('#expressionDiv').hide();
        $('#expression').val('');
        $('#delegateExpressionDiv').show();
    }
    if(val == 'express'){
        $('#classNameDiv').hide();
        $('#className').val('');
        $('#expressionDiv').show();
        $('#delegateExpressionDiv').hide();
        $('#delegateExpression').val('');
    }
}
/**
 *
 * @param cell
 * @returns {string|*}
 */
function createServiceTaskNodeAttributeForm(cell){
    serviceTaskNodeAttributeForm =
        "<div class=\"m-portlet\" id='mportletId'  style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
                    //自定义监听器
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >选择器</label>"+
                        "</div>"+
                        "<div class=\"col-md-4\">"+
                            "<input type='hidden' id='serviceTaskCustomId' name='serviceTaskCustomId'>"+
                            "<div class=\"input-group\"><input type=\"text\" id=\"serviceTaskCustomText\" name=\"serviceTaskCustomText\"  onchange='setInputValue(this,\"ServiceTask\")' readonly class=\"form-control m-input\" placeholder=\"请选择\" aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetServiceTask()\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" title='基于监听器' onclick='initServiceTaskByListener()'><i class=\"flaticon-search-1\"></i>选择监听器</span></div></div>"+
                        "</div>"+
                        "<div class=\"col-md-7\">"+
                            "<div class='alert m-alert m-alert--default' role='alert'>注意：<code>如果采用自定义则配置动态从自定义控件中选取</code> .</div>"+
                        "</div>"+
                    "</div>"+
					//类型
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >类型</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<select class=\"form-control\"  onchange='setInputValue(this,\"ServiceTask\")' maxlength=\"50\" id=\"taskType\" name=\"taskType\">"+
								"<option value='javaclass'>javaclass</option>" +
								"<option value='delegateexpress'>delegateexpress</option>" +
								"<option value='express'>express</option>" +
							"</select>"+
						"</div>"+
					"</div>"+

					//执行类
					"<div class=\"form-group row\" id='classNameDiv'>"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >类</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" onchange='setInputValue(this,\"ServiceTask\")'  data-bv-notempty data-bv-notempty-message=\"请输入执行类\" id=\"className\" name=\"className\" placeholder=\"请输入执行类\">"+
						"</div>"+
					"</div>"+

					//表达式
					"<div class=\"form-group row\" id='expressionDiv' style='display:none;'>"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >表&nbsp;达&nbsp;&nbsp;式</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" onchange='setInputValue(this,\"ServiceTask\")' data-bv-notempty data-bv-notempty-message=\"请输入表达式\" id=\"expression\" name=\"expression\" placeholder=\"请输入表达式\">"+
						"</div>"+
					"</div>"+

					//委托类型表达式
					"<div class=\"form-group row\" style='display:none;' id='delegateExpressionDiv'>"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >委托类型表达式</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" data-bv-notempty data-bv-notempty-message=\"请输入委托类型表达式\" onchange='setInputValue(this,\"ServiceTask\")' id=\"delegateExpression\" name=\"delegateExpression\" placeholder=\"请输入委托类型表达式\">"+
						"</div>"+
					"</div>"+

					//结果变量
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >结果变量</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" onchange='setInputValue(this,\"ServiceTask\")' data-bv-notempty data-bv-notempty-message=\"请输入结果变量\" id=\"resultVariable\" name=\"resultVariable\" placeholder=\"请输入结果变量\">"+
						"</div>"+
					"</div>"+

					//跳过表达式
					"<div class=\"form-group row\" id='skipExpressionDiv'>"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >跳过表达式</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" onchange='setInputValue(this,\"ServiceTask\")' data-bv-notempty data-bv-notempty-message=\"请输入跳过表达式\" id=\"skipExpression\" name=\"skipExpression\" placeholder=\"请输入跳过表达式\">"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return serviceTaskNodeAttributeForm;
}

/**
 * 初始化基本节点数据
 * @param cell
 */
function initServiceData(cell){
    //自定义
    var serviceTaskCustomId = cell.serviceTaskCustomId;
    var serviceTaskCustomText = cell.serviceTaskCustomText;
    if(undefined != serviceTaskCustomId && null != serviceTaskCustomId && "" != serviceTaskCustomId){//设为只读项
        $('#className').attr("readonly",true);
        $('#taskType').attr("disabled",true);
        $('#expression').attr("readonly",true);
        $('#delegateExpression').attr("readonly",true);
    }else{//去除input元素的readonly属性
        $('#className').attr("readonly",false);
        $('#taskType').attr("disabled",false);
        $('#expression').attr("readonly",false);
        $('#delegateExpression').attr("readonly",false);
    }
    /**取值**/
    var taskType = cell.taskType;
    var className = cell.className;
    var expression = cell.expression;
    var delegateExpression = cell.delegateExpression;
    var resultVariable = cell.resultVariable;
    var skipExpression = cell.skipExpression;
    /**赋值**/
    $('#serviceTaskCustomId').val(serviceTaskCustomId);
    $('#serviceTaskCustomText').val(serviceTaskCustomText);
    $('#className').val(className);
    $('#taskType').val(taskType);
    $('#expression').val(expression);
    $('#delegateExpression').val(delegateExpression);
    $('#resultVariable').val(resultVariable);
    $('#skipExpression').val(skipExpression);
    if(taskType == 'javaclass'){
        $('#classNameDiv').show();
        $('#expressionDiv').hide();
        $('#expression').val('');
        $('#delegateExpressionDiv').hide();
        $('#delegateExpression').val('');
    }else if(taskType == 'delegateexpress'){
        $('#classNameDiv').hide();
        $('#className').val('');
        $('#expressionDiv').hide();
        $('#expression').val('');
        $('#delegateExpressionDiv').show();
    }else if(taskType == 'express'){
        $('#classNameDiv').hide();
        $('#className').val('');
        $('#expressionDiv').show();
        $('#delegateExpressionDiv').hide();
        $('#delegateExpression').val('');
    }else{
        $('#classNameDiv').show();
        $('#taskType').val('javaclass');
        $('#expressionDiv').hide();
        $('#expression').val('');
        $('#delegateExpressionDiv').hide();
        $('#delegateExpression').val('');
    }
}


/**
 * 设置内容
 */
function setServiceTaskValue(){
    displayField();
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3配置会签
        multi_instance_setvalue(JehcClickCell);
        //4基本属性
        var serviceTaskCustomId = $('#serviceTaskCustomId').val();//非工作流属性
        var serviceTaskCustomText = $('#serviceTaskCustomText').val();//非工作流属性
        var className = $('#className').val();
        var taskType = $('#taskType').val();
        var expression = $('#expression').val();
        var delegateExpression = $('#delegateExpression').val();
        var resultVariable = $('#resultVariable').val();
        var skipExpression = $('#skipExpression').val();

        JehcClickCell.serviceTaskCustomId = serviceTaskCustomId;
        JehcClickCell.serviceTaskCustomText = serviceTaskCustomText;

        // if(null != className && '' != className){
            JehcClickCell.className = className;
        // }
        // if(null != taskType && '' != taskType){
            JehcClickCell.taskType = taskType;
        // }
        // if(null != expression && '' != expression){
            JehcClickCell.expression = expression;
        // }
        // if(null != delegateExpression && '' != delegateExpression){
            JehcClickCell.delegateExpression = delegateExpression;
        // }
        // if(null != resultVariable && '' != resultVariable){
            JehcClickCell.resultVariable = resultVariable;
        // }
        // if(null != skipExpression && '' != skipExpression){
            JehcClickCell.skipExpression = skipExpression;
        // }

        //基本属性中字段描述
        if(service_field_setvalue(JehcClickCell)== false){
            return;
        }

        //自定义
        if(undefined != serviceTaskCustomId && null != serviceTaskCustomId && "" != serviceTaskCustomId){//设为只读项
            $('#className').attr("readonly",true);
            $('#taskType').attr("disabled",true);
            $('#expression').attr("readonly",true);
            $('#delegateExpression').attr("readonly",true);
        }else{//去除input元素的readonly属性
            $('#className').attr("readonly",false);
            $('#taskType').attr("disabled",false);
            $('#expression').attr("readonly",false);
            $('#delegateExpression').attr("readonly",false);
        }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function serviceTaskNodeAttributePanel(cell,graph_refresh){
    serviceTaskNodeAttributeForm = createServiceTaskNodeAttributeForm(cell);
    nodeNormalForm = createNodeNormalForm(cell, 1,"ServiceTask");
    multiInstanceLoopCharacteristicForm = createMultiInstance(cell,"ServiceTask");
    event_grid = creatEventGrid(cell,1);
    serviceNodeAttributeFieldGrid = createServiceFieldGrid();
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>" +
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">" +
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>" +

				"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">基本属性</a>" +

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">会&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;签</a>" +

				"<a href=\"#v-pills-messages5\" data-toggle=\"pill\" class=\"\">监听事件</a>" +

        		"<a href=\"#v-pills-messages6\" data-toggle=\"pill\" class=\"\">字&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;段</a>" +

				// "<a href='javascript:setServiceTaskValue()' class='svBtn'>保存配置</a>" +
			"</div>" +
        "</div>" +

        "<div class='col-md-11'>" +
			"<div class=\"tab-content tab-content-default\">" +
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">" + nodeNormalForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">" + serviceTaskNodeAttributeForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">" + multiInstanceLoopCharacteristicForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages5\">" + event_grid + "</div>" +
        		"<div class=\"tab-pane fade\" id=\"v-pills-messages6\">" + serviceNodeAttributeFieldGrid + "</div>" +
			"</div>" +
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>" + Tab + "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();

    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell, 1);
    //基本属性
    initServiceData(cell);
    //初始化会签数据
    initMultiInstanceData(cell);
    //共用taskGrid属性事件
    initevent_grid(cell, 1);
    //服务字段配置
    initservice_field_grid(cell);
    nodeScroll();
    serviceNodeAttributeFieldGridScroll();
}

/**
 *服务任务字段配置
 */
function createServiceFieldGrid(){
    serviceNodeAttributeFieldGrid=
        "<div class=\"m-portlet\" id='serviceNodeAttributeFieldGrid' style='height:350px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<button type=\"button\" class=\"btn btn-info m-btn m-btn--custom m-btn--icon\" onclick=\"addServiceFieldRow()\">新一行</button>"+
					"&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-secondary m-btn m-btn--custom m-btn--icon\" value='保存' onclick='service_field_setvalue()'>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"serviceFieldForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='service_field_form_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f2f3f5;'>"+
						"<div class=\"col-md-3\">字段名称</div>"+
						"<div class=\"col-md-3\">字段值</div>"+
						"<div class=\"col-md-3\">表达式</div>"+
						"<div class=\"col-md-1\">操作</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return serviceNodeAttributeFieldGrid;
}

/**
 * 新一行
 * @param field
 * @param string
 * @param fieldexpression
 */
function addServiceFieldRow(field,string,fieldexpression){
    if(field === undefined){
        field = "";
    }
    if(string === undefined){
        string = "";
    }
    if(fieldexpression === undefined){
        fieldexpression = "";
    }

    var uuid = guid();
    validatorDestroy('serviceFieldForm');
    var rows =
        "<div class=\"form-group m-form__group row\" id='service_field_form_rows_"+uuid+"'>"+
			//名称
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入' id='field"+uuid+"' name='field' value='"+field+"' placeholder='请输入'>"+
			"</div>"+

			//字段值
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message=\"请输入字段值\" id='string"+uuid+"' name='string' value='"+string+"' placeholder='请输入字段值'>"+
			"</div>"+

			//表达式
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\" id='fieldexpression"+uuid+"' name=\"fieldexpression\" value='"+fieldexpression+"' placeholder=\"请输入表达式\">"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn m-btn--square  btn-secondary\" onclick='delServiceFieldFormRow(\""+uuid+"\")'>删除</button>"+
			"</div>"+
        "</div>";
    $("#service_field_form_grid").append(rows);
    reValidator('serviceFieldForm');
}

/**
 * 删除
 * @param rowID
 */
function delServiceFieldFormRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('serviceFieldForm');
        $("#service_field_form_rows_"+rowID).remove();
        reValidator('serviceFieldForm');
    });
}


/**
 *
 * @param cell
 */
function service_field_setvalue(){
    var bootform = $('#serviceFieldForm');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return false;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var serviceNodeAttributeField_value = JSON.stringify($("#serviceFieldForm").serializeObject());
    if(null != serviceNodeAttributeField_value && "" != serviceNodeAttributeField_value){
        JehcClickCell.serviceNodeAttributeField_value = serviceNodeAttributeField_value;
    }else{
        JehcClickCell.serviceNodeAttributeField_value = "";
    }
    return true;
}

/**
 * 初始化init service field grid
 * @param cell
 */
function initservice_field_grid(cell){
    //表单配置
    $('#serviceFieldForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    initservice_field_data(cell)
}

/**
 *
 * @param cell
 */
function initservice_field_data(cell){
    var serviceNodeAttributeField_value = cell.serviceNodeAttributeField_value;
    if(null != serviceNodeAttributeField_value && "" != serviceNodeAttributeField_value){
        serviceNodeAttributeField_value = eval('(' + serviceNodeAttributeField_value + ')');
        var fieldList = $.makeArray(serviceNodeAttributeField_value["field"]);
        var stringList = $.makeArray(serviceNodeAttributeField_value["string"]);
        var fieldexpressionList = $.makeArray(serviceNodeAttributeField_value["fieldexpression"]);

        for(var i = 0; i < fieldList.length; i++){
            var field = fieldList[i];
            var string = stringList[i];
            var fieldexpression = fieldexpressionList[i];
            addServiceFieldRow(field,string,fieldexpression)
        }
    }
}

/**
 * 初始化服务任务监听器
 */
function initServiceTaskByListener() {
    var listenerSelectModalCount = 0 ;
    $('#listenerBody').height(reGetBodyHeight()-218);
    $('#listenerSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#listenerSelectModal').on("shown.bs.modal",function(){
        if(++listenerSelectModalCount == 1){
            var $modal_dialog = $("#listenerModalDialog");
            $modal_dialog.css({'margin': 0 + 'px auto'});
            $modal_dialog.css({'width':reGetBodyWidth()+'px'});
            $('#searchFormlistener')[0].reset();
            var opt = {
                searchformId:'searchFormlistener'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcListener/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex+1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:'lc_listener_id'
                    },
                    {
                        data:'categories',
                        render:function(data, type, row, meta) {
                            if(data == 100){
                                return "任务监听"
                            }

                            if(data == 200){
                                return "执行监听"
                            }
                            return "缺省"
                        }
                    },
                    {
                        data:'lc_listener_name'
                    },
                    {
                        data:'type',
                        render:function(data, type, row, meta) {
                            if(data == 'javaclass'){
                                return "Java类"
                            }

                            if(data == 'express'){
                                return "表达式"
                            }

                            if(data == 'delegateExpression'){
                                return "委托表达式"
                            }

                            if(data == 'ScriptExecutionListener'){
                                return "脚本执行监听器"
                            }

                            if(data == 'ScriptTaskListener'){
                                return "脚本任务监听器"
                            }
                            return "缺省"
                        }
                    },
                    {
                        data:'event'
                    },
                    {
                        data:'del_flag',
                        render:function(data, type, row, meta) {
                            if(data == 0){
                                return "<font style='color:#0a6aa1 '>生效</font>"
                            }

                            if(data == 1){
                                return "禁用"
                            }
                            return "缺省"
                        }
                    },
                    {
                        data:'productName'
                    },
                    {
                        data:'groupName'
                    },
                    {
                        data:"lc_listener_id",
                        render:function(data, type, row, meta) {
                            var lc_listener_id = row.lc_listener_id;
                            var btn = '<button class="btn btn-light-primary font-weight-bold mr-2" onclick=doSelectServiceTaskListener("'+lc_listener_id+'")><span><span>确 认</span></span></button>';
                            return btn;
                        }
                    }
                ]
            });
            $('#listenerDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('listenerDatatables');
        }
    });
}

/**
 * 选择监听器配置项
 * @param data
 */
function doSelectServiceTaskListener(lc_listener_id) {
    msgTishCallFnBoot("确定选择该配置项？",function(){
        $.ajax({
            type:"GET",
            url:workflowModules+"/lcListener/get/"+lc_listener_id,
            success: function(result){
                if(complateAuth(result)){
                    try {
                        result = result.data;
                        var lc_listener_id = result.lc_listener_id;
                        var lc_listener_name = result.lc_listener_name;
                        var categories  =  result.categories;//分类：100任务监听 200执行监听 300基于连线执行监听
                        var event = result.event;//事件类型：create，assignment，complete，all | start，end | start，end，take
                        var listener_val = result.listener_val;//事件值
                        var type = result.type;//监听类型：javaclass，express，delegateExpression，ScriptExecutionListener，ScriptTaskListener
                        var javaclass_express = listener_val;//充当表达式或类路径或委托表达式
                        var event_type = type; //充当类型

                        if(type.toLowerCase() != 'javaclass' && type.toLowerCase() != 'delegateexpress' && type.toLowerCase() != 'express'){
                            toastrBoot(4,"类型未能匹配，无法设置！");
                            return;
                        }
                        $('#serviceTaskCustomId').val(lc_listener_id);
                        $('#serviceTaskCustomText').val(lc_listener_name);
                        $('#className').val(javaclass_express);
                        $('#taskType').val(type.toLowerCase());
                        $('#expression').val(javaclass_express);
                        $('#delegateExpression').val(javaclass_express);
                        if(type.toLowerCase() == 'javaclass'){
                            $('#classNameDiv').show();
                            $('#expressionDiv').hide();
                            $('#expression').val('');
                            $('#delegateExpressionDiv').hide();
                            $('#delegateExpression').val('');
                        }else if(type.toLowerCase() == 'delegateexpress'){
                            $('#classNameDiv').hide();
                            $('#className').val('');
                            $('#expressionDiv').hide();
                            $('#expression').val('');
                            $('#delegateExpressionDiv').show();
                        }else if(type.toLowerCase() == 'express'){
                            $('#classNameDiv').hide();
                            $('#className').val('');
                            $('#expressionDiv').show();
                            $('#delegateExpressionDiv').hide();
                            $('#delegateExpression').val('');
                        }else{
                            $('#classNameDiv').show();
                            $('#taskType').val('javaclass');
                            $('#expressionDiv').hide();
                            $('#expression').val('');
                            $('#delegateExpressionDiv').hide();
                            $('#delegateExpression').val('');
                        }
                        setInputValue(null,"ServiceTask");//调用设置参数
                    } catch (e) {
                        console.log("查询监听器异常："+e);
                    }
                }
                $('#listenerSelectModal').modal('hide');
            }
        });
    });
}

/**
 * 重置ServiceTask自定义选项
 */
function resetServiceTask() {
    var serviceTaskCustomId = $('#serviceTaskCustomId').val();
    if(undefined != serviceTaskCustomId && null != serviceTaskCustomId && "" != serviceTaskCustomId) {//设为只读项
        $('#serviceTaskCustomId').val("");//非工作流属性
        $('#serviceTaskCustomText').val("");//非工作流属性
        $('#className').val("");
        $('#taskType').val("javaclass");
        $('#expression').val("");
        $('#delegateExpression').val("");
        setInputValue(null,"ServiceTask");//调用设置参数
    }
}