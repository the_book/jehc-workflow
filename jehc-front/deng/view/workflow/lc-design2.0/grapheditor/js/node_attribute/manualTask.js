/**
 * 手动任务
 * @param cell
 * @param graph_refresh
 */
function showManualTaskNodeAttributeWin(cell,graph_refresh){
    manualTaskNodeAttributePanel(cell,graph_refresh);
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function manualTaskNodeAttributePanel(cell,graph_refresh){
    nodeNormalForm = createNodeNormalForm(cell, 1,"ManualTask");
    multiInstanceLoopCharacteristicForm = createMultiInstance(cell,"ManualTask");
    event_grid = creatEventGrid(cell,1);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>" +
			"<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">" +
				"<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">一般配置</a>" +

				"<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">会&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;签</a>" +

				"<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">监听事件</a>" +

				// "<a href='javascript:setManualTaskValue()' class='svBtn'>保存配置</a>" +
			"</div>" +
		"</div>" +

		"<div class='col-md-11'>" +
			"<div class=\"tab-content tab-content-default\">" +
				"<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">" + nodeNormalForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages3\">" + multiInstanceLoopCharacteristicForm + "</div>" +
				"<div class=\"tab-pane fade\" id=\"v-pills-messages4\">" + event_grid + "</div>" +
			"</div>" +
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>" + Tab + "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();

    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell, 1);
    //初始化会签数据
    initMultiInstanceData(cell);
    //共用taskGrid属性事件
    initevent_grid(cell, 1);
    nodeScroll();
}

/**
 * 设置内容
 */
function setManualTaskValue(){
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //4配置会签
        multi_instance_setvalue(JehcClickCell);
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}
	