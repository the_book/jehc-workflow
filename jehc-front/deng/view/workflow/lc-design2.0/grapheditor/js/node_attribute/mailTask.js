var mailTaskNodeAttributeForm;
/**
 * 邮件任务节点
 * @param cell
 * @param graph_refresh
 */
function showMailTaskNodeAttributeWin(cell,graph_refresh){
    mailTaskNodeAttributePanel(cell,graph_refresh);
}

/**
 *
 * @param cell
 * @returns {string|*}
 */
function createMailTaskNodeAttributeForm(cell){
    mailTaskNodeAttributeForm =
        "<div class=\"m-portlet\" id='mportletId' style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//接收者
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >接&nbsp;收&nbsp;&nbsp;者</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  onchange='setInputValue(this,\"MailTask\")' data-bv-notempty data-bv-notempty-message=\"请选择接收者\" id=\"to\" name=\"to\" placeholder=\"请选择接收者\">"+
						"</div>"+
					"</div>"+

					//发送者
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >发&nbsp;送&nbsp;&nbsp;者</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\"  onchange='setInputValue(this,\"MailTask\")' maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请选择发送者\" id=\"from\" name=\"from\" placeholder=\"请选择发送者\">"+
						"</div>"+
					"</div>"+

					//主题
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >主&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;题</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\"  onchange='setInputValue(this,\"MailTask\")' maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入主题\" id=\"subject\" name=\"subject\" placeholder=\"请输入主题\">"+
						"</div>"+
					"</div>"+

					//抄送
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >抄&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;送</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\"  onchange='setInputValue(this,\"MailTask\")' maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入抄送\" id=\"cc\" name=\"cc\" placeholder=\"请输入抄送\">"+
						"</div>"+
					"</div>"+

					//秘送
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >秘&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;送</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\"  onchange='setInputValue(this,\"MailTask\")' maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入秘送\" id=\"bcc\" name=\"bcc\" placeholder=\"请输入秘送\">"+
						"</div>"+
					"</div>"+

					//字符集
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >字&nbsp;符&nbsp;&nbsp;集</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\"  onchange='setInputValue(this,\"MailTask\")' maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入字符集\" id=\"charset\" name=\"charset\" placeholder=\"请输入字符集\">"+
						"</div>"+
					"</div>"+

					//HTML框
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >HTML&nbsp;框</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\"  onchange='setInputValue(this,\"MailTask\")' maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入HTML框\" id=\"html\" name=\"html\" placeholder=\"请输入HTML框\">"+
						"</div>"+
					"</div>"+

					//备注
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注</label>"+
						"</div>"+
						"<div class=\"col-md-6\">"+
							"<textarea class=\"form-control\"  onchange='setInputValue(this,\"MailTask\")' maxlength=\"50\" data-bv-notempty data-bv-notempty-message=\"请输入备注\" id=\"nohtml\" name=\"nohtml\" placeholder=\"请输入备注\" />"+
						"</div>"+
					"</div>"+

				"</div>"+
			"</form>"+
        "</div>";
    return mailTaskNodeAttributeForm;
}

/**
 * 邮件任务
 * @param cell
 */
function initMailTaskData(cell){
    var to = cell.to;
    var from = cell.from;
    var subject = cell.subject;
    var cc = cell.cc;
    var bcc = cell.bcc;
    var charset = cell.charset;
    var html = cell.html;
    var nohtml = cell.nohtml;
    if(null != to && '' != to){
        $('#to').val(to);
    }
    if(null != from && '' != from){
        $('#from').val(from);
    }
    if(null != subject && '' != subject){
        $('#subject').val(subject);
    }
    if(null != cc && '' != cc){
        $('#cc').val(cc);
    }
    if(null != bcc && '' != bcc){
        $('#bcc').val(bcc);
    }
    if(null != charset && '' != charset){
        $('#charset').val(charset);
    }
    if(null != html && '' != html){
        $('#html').val(html);
    }
    if(null != nohtml && '' != nohtml){
        $('#nohtml').val(nohtml);
    }
}

/**
 *
 * @param cell
 * @param graph_refresh
 */
function mailTaskNodeAttributePanel(cell,graph_refresh) {
    mailTaskNodeAttributeForm = createMailTaskNodeAttributeForm(cell);
    nodeNormalForm = createNodeNormalForm(cell, 1,"MailTask");
    multiInstanceLoopCharacteristicForm = createMultiInstance(cell,"MailTask");
    event_grid = creatEventGrid(cell,1);
    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>" +
            "<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">" +
                "<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"\">一般配置</a>" +

                "<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"active show\">基本属性</a>" +

                "<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">会&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;签</a>" +

                "<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">监听事件</a>" +

                // "<a href='javascript:setMailTaskValue()' class='svBtn'>保存配置</a>" +
            "</div>" +
        "</div>" +

        "<div class='col-md-11'>" +
            "<div class=\"tab-content tab-content-default\">" +
                "<div class=\"tab-pane fade\" id=\"v-pills-home2\">" + nodeNormalForm + "</div>" +
                "<div class=\"tab-pane fade active show\" id=\"v-pills-profile2\">" + mailTaskNodeAttributeForm + "</div>" +
                "<div class=\"tab-pane fade\" id=\"v-pills-messages3\">" + multiInstanceLoopCharacteristicForm + "</div>" +
                "<div class=\"tab-pane fade\" id=\"v-pills-messages4\">" + event_grid + "</div>" +
            "</div>" +
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>" + Tab + "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();

    //基本属性
    initMailTaskData(cell);
    //一般属性 参数1表示非开始2其他
    initNodeNormalForm(cell, 1);
    //初始化会签数据
    initMultiInstanceData(cell);
    //共用taskGrid属性事件
    initevent_grid(cell, 1);
    nodeScroll();
}


/**
 * 设置内容
 */
function setMailTaskValue(){
    var graph = new mxGraph();
    graph.getModel().beginUpdate();
    try
    {
        //1通用基本属性并具有赋值功能
        if(node_normal_setvalue(JehcClickCell,1)== false){
            return;
        }
        //2事件配置
        if(event_setvalue(JehcClickCell)== false){
            return;
        }
        //3基本属性
        var to = $('#to').val();
        var from = $('#from').val();
        var subject = $('#subject').val();
        var cc = $('#cc').val();
        var bcc = $('#bcc').val();
        var charset = $('#charset').val();
        var html = $('#html').val();
        var nohtml = $('#nohtml').val();
        // if(null != to && '' != to){
            JehcClickCell.to = to;
        // }
        // if(null != from && '' != from){
            JehcClickCell.from = from;
        // }
        // if(null != subject && '' != subject){
            JehcClickCell.subject = subject;
        // }
        // if(null != cc&& '' != cc){
            JehcClickCell.cc = cc;
        // }
        // if(null != bcc && '' != bcc){
            JehcClickCell.bcc = bcc;
        // }
        // if(null != charset && '' != charset){
            JehcClickCell.charset = charset;
        // }
        // if(null != html && '' != html){
            JehcClickCell.html = html;
        // }
        // if(null != nohtml && '' != nohtml){
            JehcClickCell.nohtml = nohtml;
        // }
        //4配置会签
        multi_instance_setvalue(JehcClickCell);
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}