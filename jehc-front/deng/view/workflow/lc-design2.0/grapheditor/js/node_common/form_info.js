function appendFormInfo(){
    message_grid = createMessageGrid();
    signal_grid = createSignalGrid();
    datamainProperties_grid = createDatamainPropertiesGrid();
    event_main_grid = createEventMainGrid(0);
    var cExpansion = "<div id='SSZK' style='position:absolute;width:50px;right: 0px;height: 20px;line-height: 20px;'><a href='javascript:doE()' class='flaticon2-up' style='text-decoration:none;color:#6c7293' id='sszkHref'>收缩</a></div>";
    var baseContent =
        "<div class=\"m-portlet\" id='mportletId' style='height:150px;overflow: auto;'>"+
            "<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
                "<div class=\"m-portlet__body\">"+
                    //流程名称
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >流程名称</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" type=\"text\" maxlength=\"50\" onchange='setInputValue(this,\"BaseInfo\")'  data-bv-notempty data-bv-notempty-message=\"请输入流程名称\" id=\"processNameTemp\" name=\"processNameTemp\" placeholder=\"请输入流程名称\">"+
                        "</div>"+
                    "</div>"+

                    //命名空间
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >命名空间</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" type=\"text\" maxlength=\"50\" value=\"http://www.activiti.org/test\"  onchange='setInputValue(this,\"BaseInfo\")' data-bv-notempty data-bv-notempty-message=\"请输入命名空间\" id=\"mainNameSpaceTemp\" name=\"mainNameSpaceTemp\" placeholder=\"请输入命名空间\">"+
                        "</div>"+
                    "</div>"+

                    //发起人
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >发&nbsp;&nbsp;起&nbsp;&nbsp;人</label>"+
                        "</div>"+
                        "<div class=\"col-md-3\">"+
                            "<input class=\"form-control\" id=\"candidateStarterUsers\" type='hidden' name=\"candidateStarterUsers\"></input>"+
                            "<div class=\"input-group\"><input type=\"text\" readonly class=\"form-control m-input\"  onchange='setInputValue(this,\"BaseInfo\")' id='candidateStarterUsers_Text' placeholder=\"请选择\" id='' aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetAssignee(1,1)\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span  style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" onclick='initassignee(1,2)'><i class=\"flaticon-user-ok\"></i></span></div></div>"+
                        "</div>"+
                    "</div>"+
                    //组类型
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >组&nbsp;&nbsp;类&nbsp;型</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<select id='candidate_group_type' onchange=\"canditateGroupChange(this,3)\"  class='form-control' name='candidate_group_type'><option value=''>请选择</option><option value='0'>部门</option><option value='1'>岗位</option><option value='2'>角色</option></select>"+
                        "</div>"+
                        "<div class=\"col-md-4\">"+
                            "<div class='alert m-alert m-alert--default' role='alert'>注意：<code>组类型仅限于部门，岗位，角色</code> .</div>"+
                        "</div>"+
                    "</div>"+
                    //发起人组
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >发起人组</label>"+
                        "</div>"+
                        "<div class=\"col-md-4\">"+
                            "<input class=\"form-control\" type='hidden' id=\"candidateStarterGroups\" name=\"candidateStarterGroups\"></input>"+
                            "<div class=\"input-group\"><input type=\"text\" id='candidateStarterGroups_Text'  onchange='setInputValue(this,\"BaseInfo\")' name='candidateStarterGroups_Text'  readonly class=\"form-control m-input\" placeholder=\"请选择\" aria-describedby=\"basic-addon2\"><div class=\"input-group-append\"><span style=\"cursor:pointer;\" class=\"input-group-text\" id=\"basic-addon2\" onclick=\"resetAssignee(1,2)\"><i class=\"fa fa-times\"></i></span></div><div class=\"input-group-append\"><span style='cursor:pointer;' class=\"input-group-text\" id=\"basic-addon2\" onclick='initcandidateGroups(2,2)'><i class=\"fa flaticon-users-1\"></i></span></div></div>"+
                        "</div>"+
                    "</div>"+

                    //备注
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注</label>"+
                        "</div>"+
                        "<div class=\"col-md-6\">"+
                            "<textarea class=\"form-control\" maxlength=\"500\" id=\"remarkTemp\"  onchange='setInputValue(this,\"BaseInfo\")' name=\"remarkTemp\" placeholder=\"请输入备注\"></textarea>"+
                        "</div>"+
                    "</div>"+

                    //产品线
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >产&nbsp;品&nbsp;线</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<select class=\"form-control\" id=\"lc_product_idTemp\"  onchange='setProductValue()' name=\"lc_product_idTemp\"><option value=''>请选择</option></select>"+
                        "</div>"+
                    "</div>"+

                    //产品组
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >产&nbsp;品&nbsp;组</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<select class=\"form-control\" id=\"lc_group_idTemp\"  onchange='setGroupValue()' name=\"lc_group_idTemp\"><option value=''>请选择</option></select>"+
                        "</div>"+
                    "</div>"+

                    //流程名称
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"col-form-label\" >模块Key</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" type=\"text\" maxlength=\"50\" onchange='setInputValue(this,\"BaseInfo\")'  data-bv-notempty data-bv-notempty-message=\"请输入\" id=\"moduleKeyTemp\" name=\"moduleKeyTemp\" placeholder=\"请输入\">"+
                        "</div>"+
                    "</div>"+
                "</div>"+
            "</form>"+
        "</div>";

    //Tab Index
    var Tab =
        "<div class='col-md-1' id='TabCol'>"+
        "<div class=\"nav flex-column nav-tabs nav-tabs-vertical mb-4 mb-xl-0\">"+
        "<a href=\"#v-pills-home2\" data-toggle=\"pill\" class=\"active show\">基础信息</a>"+

        // "<a href=\"#v-pills-profile2\" data-toggle=\"pill\" class=\"\">监听事件</a>"+

        "<a href=\"#v-pills-messages3\" data-toggle=\"pill\" class=\"\">数据属性</a>"+

        "<a href=\"#v-pills-messages4\" data-toggle=\"pill\" class=\"\">信号定义</a>"+

        "<a href=\"#v-pills-messages5\" data-toggle=\"pill\" class=\"\">消&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;息</a>"+

        // "<a href='javascript:setBaseFormInfo()' class='svBtn'>保存配置</a>"+

        "</div>"+
        "</div>"+

        "<div class='col-md-11'>"+
        "<div class=\"tab-content tab-content-default\">"+
        "<div class=\"tab-pane fade active show\" id=\"v-pills-home2\">"+baseContent+"</div>"+
        // "<div class=\"tab-pane fade\" id=\"v-pills-profile2\">"+event_main_grid+"</div>"+
        "<div class=\"tab-pane fade\" id=\"v-pills-messages3\">"+datamainProperties_grid+"</div>"+
        "<div class=\"tab-pane fade\" id=\"v-pills-messages4\">"+signal_grid+"</div>"+
        "<div class=\"tab-pane fade\" id=\"v-pills-messages5\">"+message_grid+"</div>"+
        "</div>"+
        "</div>";

    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;'>"+ Tab+ "</div></div>"
    $("#geSetContainer").empty();
    $("#geSetContainer").append(cExpansion);//收缩/展开
    $("#geSetContainer").append(formInfo);
    initSZ();
    initBaseFormData();
    nodeScroll();


}

/**
 * 设置Form基本信息
 */
function setBaseFormInfo(){
    var processNameTemp = $("#processNameTemp").val();
    var processIdTemp = $("#processIdTemp").val();
    var mainNameSpaceTemp = $("#mainNameSpaceTemp").val();
    var candidateStarterUsersTemp = $("#candidateStarterUsers").val();
    var candidateStarterGroupsTemp = $("#candidateStarterGroups").val();
    var remarkTemp = $("#remarkTemp").val();
    var moduleKeyTemp = $("#moduleKeyTemp").val();
    $("#processName").val(processNameTemp);
    $("#processId").val(processIdTemp);
    $("#mainNameSpace").val(mainNameSpaceTemp);
    $("#candidateStarterUsersTemp").val(candidateStarterUsersTemp);
    $("#candidateStarterGroupsTemp").val(candidateStarterGroupsTemp);
    $("#remark").val(remarkTemp);
    $("#moduleKey").val(moduleKeyTemp);
    $('#candidate_group_typeTemp').val($("#candidate_group_type").val());
    // //验证事件
    // if(event_main_setvalue() == false){
    //     return;
    // }

    //验证数据属性
    if(datamainProperties_setvalue() == false){
        return;
    }

    //验证信号
    if(signal_setvalue() == false){
        return;
    }

    //验证消息
    if(message_setvalue() == false){
        return;
    }
}

/**
 * 初始化基础Form数据
 */
function initBaseFormData(){
    var processName = $("#processName").val();
    var processId = $("#processId").val();
    var mainNameSpace = $("#mainNameSpace").val();
    var candidateStarterUsersTemp = $("#candidateStarterUsersTemp").val();
    var candidateStarterGroupsTemp = $("#candidateStarterGroupsTemp").val();
    var candidate_group_typeTemp = $('#candidate_group_typeTemp').val();
    var remark = $("#remark").val();
    var lc_product_id = $("#lc_product_id").val();
    var lc_group_id = $("#lc_group_id").val();
    var moduleKey = $("#moduleKey").val();
    $("#processNameTemp").val(processName);
    $("#processIdTemp").val(processId);
    $("#moduleKeyTemp").val(moduleKey);
    if(null == mainNameSpace || "" == mainNameSpace){
        $("#mainNameSpaceTemp").val("http://www.activiti.org/test");
    }else{
        $("#mainNameSpaceTemp").val(mainNameSpace);
    }
    $("#candidateStarterUsers").val(candidateStarterUsersTemp);
    $("#candidateStarterGroups").val(candidateStarterGroupsTemp);
    $("#remarkTemp").val(remark);
    $("#lc_product_idTemp").val(lc_product_id);
    $("#lc_group_idTemp").val(lc_group_id);
    $("#candidate_group_type").val(candidate_group_typeTemp);
    // //初始化事件
    // initeventmain_grid();

    //初始化数据属性
    initDatamainProperties_grid();

    //初始化消息
    initmessage_grid();

    //初始化信号
    initSignal_grid();

    initACC($('#candidateStarterUsersTemp').val(),null,$('#candidateStarterGroupsTemp').val(),2);

    setTimeout(function(){
        initProductListSetV('lc_product_idTemp','lc_product_id');
    },100);
}



function doE(){
    var isSZ = $("#isSZ").val();
    if(isSZ == 0){
        //执行收缩
        // $("#geSetContainer").css("height","20px");
        // $(".geDiagramContainer").css("bottom","20px");
        setContainerHeight = 30;
        $("#isSZ").val("1");
        $("#sszkHref").text('');//展开
        // $("#sszkHref").attr("class","fa fa-angle-down");
        $("#sszkHref").attr("class","la la-plus");
        $(".card-body").hide();
    }else{
        //执行展开
        // $("#geSetContainer").css("height","220px");
        // $(".geDiagramContainer").css("bottom","220px");
        setContainerHeight = 220;
        $("#isSZ").val("0");
        $("#sszkHref").text('');//收缩
        // $("#sszkHref").attr("class","fa fa-angle-up");
        $("#sszkHref").attr("class","la la-minus");
        $(".card-body").show();
    }
    ThizViewPort.editorUi.refresh();
}

function initSZ(){
    var isSZ = $("#isSZ").val();
    if(isSZ == 0){
        $("#sszkHref").text('');//收缩
        // $("#sszkHref").attr("class","fa fa-angle-up");
        $("#sszkHref").attr("class","la la-minus");
        $(".card-body").show();
    }else{
        $("#sszkHref").text('');//展开
        // $("#sszkHref").attr("class","fa fa-angle-down");
        $("#sszkHref").attr("class","la la-plus");
        $(".card-body").hide();
    }
}


function setProductValue() {
    var lc_product_idTemp = $("#lc_product_idTemp").val();
    $("#lc_product_id").val(lc_product_idTemp);
    $("#lc_group_id").val("");
    initProductListSetV('lc_product_idTemp','lc_product_id');
}

function setGroupValue() {
    var lc_group_idTemp = $("#lc_group_idTemp").val();
    $("#lc_group_id").val(lc_group_idTemp);
}

function initProductListSetV(id,value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"POST",
        url:workflowModules+"/lcProduct/find",
        success: function(result){
            if(complateAuth(result)){
                result = result.data;
                if(undefined != result){
                    //从服务器获取数据进行绑定
                    $.each(result, function(i, item){
                        str += "<option value=" + item.lc_product_id + ">" + item.name + "</option>";
                    })
                    $("#"+id).append(str);
                    try {
                        if(null != value_id && '' != value_id){
                            $('#'+id).val($("#"+value_id).val());
                        }
                        //设置产品组
                        initGroupListSetV("lc_group_idTemp","lc_group_id");
                    } catch (e) {
                        console.log("初始化产品线异常信息："+e);
                    }
                }
            }
        }
    });
}


/**
 *
 * @param id
 * @param value_id
 * @param lc_product_id
 */
function initGroupListSetV(id,value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    if(null != $("#lc_product_idTemp").val() && '' != $("#lc_product_idTemp").val()){
        $.ajax({
            type:"GET",
            url:workflowModules+"/lcGroup/find/"+$("#lc_product_idTemp").val(),
            success: function(result){
                if(complateAuth(result)){
                    result = result.data;
                    //从服务器获取数据进行绑定
                    $.each(result, function(i, item){
                        str += "<option value=" + item.lc_group_id + ">" + item.name + "</option>";
                    })
                    $("#"+id).append(str);
                    try {
                        if(null != value_id && '' != value_id){
                            $('#'+id).val($("#"+value_id).val());
                        }
                    } catch (e) {
                        console.log("初始化产品组异常信息："+e);
                    }
                }
            }
        });
    }else{
        $("#"+id).append(str);
    }
}

/**
 *
 * @param type (1主流程 2任务 3永道)
 * @param userType（1 assignee 2 candidateUsers 3 candidateGroups）
 */
function resetAssignee(type,userType){
    if(type == 1){
        if(userType == 1){
            $("#candidateStarterUsers").val("");
            $("#candidateStarterUsersTemp").val("");
            $("#candidateStarterUsersTemp_Text").val("");
            $("#candidateStarterUsers_Text").val("");
        }
        if(userType == 2){
            $("#candidateStarterGroups").val("");
            $("#candidateStarterGroups_Text").val("");
            $("#candidateStarterGroupsTemp").val("");
            $("#candidateStarterGroupsTemp_Text").val("");
        }
        setBaseFormInfo()
    }
    if(type == 2){
        if(userType == 1){
            $("#assignee").val("");
            $("#assignee_text").val("");

            //清空使用表达式
            $('#useExpByAssignee').val("");
            $('#useExpValueByAssignee').val("");
        }
        if(userType == 2){
            $("#candidateUsers").val("");
            $("#candidateUsers_Text").val("");

            //清空使用表达式
            $('#useExpByCandidateUsers').val("");
            $('#useExpValueByCandidateUsers').val("");
        }
        if(userType == 3){
            $("#candidateGroups").val("");
            $("#candidateGroups_Text").val("");

            //清空激活表达式
            $('#useExpByCandidateGroups').val("");//表示使用表达式
            $('#useExpValueByCandidateGroups').val("");
        }
        setUserTaskValue();
    }
    if(type == 3){
        if(userType == 1){
            $("#candidateStarterUsers_").val("");
            $("#candidateStarterUsers_Text_").val("");
        }
        if(userType == 2){
            $("#candidateStarterGroups_").val("");
            $("#candidateStarterGroups_Text_").val("");
        }
        setPoolValue();
    }
}

function doExp() {
    var isSZ = $("#isSZ").val();
    if(isSZ == 0){
        toastrBoot(3,"属性表单已打开，可以进行配置了");
        // //执行收缩
        // // $("#geSetContainer").css("height","20px");
        // // $(".geDiagramContainer").css("bottom","20px");
        // setContainerHeight = 30;
        // $("#isSZ").val("1");
        // $("#sszkHref").text('');//展开
        // // $("#sszkHref").attr("class","fa fa-angle-down");
        // $("#sszkHref").attr("class","la la-plus");
        // $(".card-body").hide();
    }else{
        //执行展开
        // $("#geSetContainer").css("height","220px");
        // $(".geDiagramContainer").css("bottom","220px");
        setContainerHeight = 220;
        $("#isSZ").val("0");
        $("#sszkHref").text('');//收缩
        // $("#sszkHref").attr("class","fa fa-angle-up");
        $("#sszkHref").attr("class","la la-minus");
        $(".card-body").show();
        toastrBoot(3,"属性表单已打开，可以进行配置了");
    }
    ThizViewPort.editorUi.refresh();
}

/**
 * 重置调用子流程
 */
function resetCallActivity(){
    $("#calledElement").val("");
}

/**
 * 选择流程
 */
function initCallActivity() {
    var  calledElement = $("#calledElement").val();
    var processSelectModalCount = 0 ;
    $('#processSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#processSelectModal').on("shown.bs.modal",function(){
        if(++processSelectModalCount == 1){
            $('#searchFormprocess')[0].reset();
            var opt = {
                searchformId:'searchFormprocess'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcProcess/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex+1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"lc_process_id",
                        width:"150px"
                    },
                    {
                        data:'createBy',
                        width:"150px"
                    },
                    {
                        data:'lc_process_title',
                        width:"150px",
                        render:function(data, type, row, meta) {
                            var lc_process_uk = row.lc_process_uk;
                            if(null == calledElement || calledElement == ""){
                                return data;
                            }
                            if(calledElement == lc_process_uk){
                                console.log(calledElement,lc_process_uk);
                                return "<font style='color: #22b9ff;font-size:20px;'>"+data+"</font>"
                            }
                            return data;
                        }
                    },
                    {
                        data:'lc_process_status',
                        width:"150px",
                        render:function(data, type, row, meta) {
                            if(data == 0){
                                return "待发布";
                            }
                            if(data == 1){
                                return "发布中";
                            }
                            if(data == 2){
                                return "已关闭";
                            }
                        }
                    },
                    {
                        data:'name',
                        width:"150px"
                    },
                    {
                        data:'groupName',
                        width:"150px"
                    },
                    {
                        data:"lc_process_id",
                        render:function(data, type, row, meta) {
                            var lc_process_flag = row.lc_process_flag;
                            var lc_process_title = row.lc_process_title;
                            var lc_process_uk = row.lc_process_uk;
                            var btn = '<button class="btn btn-light-primary font-weight-bold mr-2" onclick=doSelectCallActivity("'+lc_process_uk+'")><span><span>确 认</span></span></button>';
                            return btn;
                        }
                    }
                ]
            });
            grid=$('#processDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('processDatatables');
        }
    });
}

/**
 *
 * @param data
 */
function doSelectCallActivity(data) {
    msgTishCallFnBoot("确定选择该流程项？",function(){
        $("#calledElement").val(data);
        setCallActivityValue();
        $('#processSelectModal').modal('hide');
    });
}


/**
 *
 * @param type 节点类型 2表示UserTask
 * @param elementType 元素类型
 */
function initasksigneeByExp(nodeType,elementType) {
    var expSelectModalCount = 0 ;
    $('#expBody').height(reGetBodyHeight()-218);
    $('#expSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#expSelectModal').on("shown.bs.modal",function(){
        if(++expSelectModalCount == 1){
            var $modal_dialog = $("#expModalDialog");
            $modal_dialog.css({'margin': 0 + 'px auto'});
            $modal_dialog.css({'width':reGetBodyWidth()+'px'});
            $('#searchFormexp')[0].reset();
            var opt = {
                searchformId:'searchFormexp'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcExp/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex+1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"lc_exp_id",
                        width:"150px"
                    },
                    {
                        data:'lc_exp_name',
                        width:"150px"
                    },
                    {
                        data:'productName',
                        width:"150px"
                    },
                    {
                        data:'groupName',
                        width:"150px"
                    },
                    {
                        data:"lc_exp_id",
                        render:function(data, type, row, meta) {
                            var lc_exp_name = row.lc_exp_name;
                            var lc_exp_val = row.lc_exp_val;
                            var groupName = row.groupName;
                            var btn = '<button class="btn btn-light-primary font-weight-bold mr-2" onclick=doSelectExp("'+data+'","'+nodeType+'","'+elementType+'","'+lc_exp_val+'")><span><span>确 认</span></span></button>';
                            return btn;
                        }
                    }
                ]
            });
            grid=$('#expDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('expDatatables');
        }
    });
}


/**
 * 表达式选择器
 * @param lc_exp_id
 * @param type
 * @param elementType
 * @param lc_exp_val
 */
function doSelectExp(lc_exp_id,type,elementType,lc_exp_val) {
    msgTishCallFnBoot("确定选择该表达式项？",function(){
        if(type == 1){//任务节点
            if(elementType == 1){ //处理人 表达式
                $('#useExpByAssignee').val("0");//表示使用表达式
                $('#useExpValueByAssignee').val(lc_exp_id);
                $('#assignee').val(lc_exp_val);
                $('#assignee_text').val(lc_exp_val);
            }

            if(elementType == 2){ //候选人 表达式
                $('#useExpByCandidateUsers').val("0");//表示使用表达式
                $('#useExpValueByCandidateUsers').val(lc_exp_id);
                $('#candidateUsers').val(lc_exp_val);
                $('#candidateUsers_Text').val(lc_exp_val);
            }

            if(elementType == 3){ //候选群组 表达式
                $('#useExpByCandidateGroups').val("0");//表示使用表达式
                $('#useExpValueByCandidateGroups').val(lc_exp_id);
                $('#candidateGroups').val(lc_exp_val);
                $('#candidateGroups_Text').val(lc_exp_val);
            }
            setUserTaskValue();
        }
        $('#expSelectModal').modal('hide');
    });
}

//日期选择器渲染
function datetimeFormatInit(){
    $(".form_datetime").datetimepicker({
        format: 'yyyy-mm-ddThh:ii:ss',
        forceParse: 0, //设置为0，时间不会跳转1899，会显示当前时间。
        todayBtn:  1,
        clearBtn: 1,
        minView: 0,
        minuteStep:5,
        autoclose: 1,
        todayHighlight: 1,
        showMeridian:1,
        pickerPosition:"bottom-left",
        language:'zh-CN'//中文，需要引用zh-CN.js包
    });
    // setInterval(function(){$(".clear").removeAttr("style");},50);
}

/**
 * 时间选择器（用于时间边界事件，定时器启动事件）
 * @param type
 */
function showTimeSelect(type) {
    $("#defaultFormTimeSelect")[0].reset();
    var timeSelectModalCount = 0 ;
    $("#timeSelectType").val(type);
    // $('#lcTimeSelectBody').height(reGetBodyHeight()-218);
    $('#lcTimeSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#lcTimeSelectModal').on("shown.bs.modal",function(){
        if(++timeSelectModalCount == 1){
            // var $modal_dialog = $("#lcTimeSelectModalDialog");
            // $modal_dialog.css({'margin': 0 + 'px auto'});
            // $modal_dialog.css({'width':reGetBodyWidth()+'px'});
            showDayDiv();
        }
    });
}

/**
 *这个时间段指明了多久之后触发定时器事件，timeDuration元素被定义成timerEventDefinition元素的一个子元素（在bpmn的xml文件中），这个时间区间可以被写成ISO 8601的两种格式
 1.PnYnMnDTnHnMnS      //P表示日期的开始（年月日），T表示时间的开始（时分秒）,即表示：固定n年n月n天n小时n分钟n秒钟后
 2. PnW                              //表示n周后

 //P10DT5M：表示每10天5分钟为一个周期
 <timerEventDefinition>
 <timeDuration>P10DT5M</timeDuration>
 </timerEventDefinition>


 //PM10M：每10分钟为一个周期
 <timerEventDefinition>
 <timeDuration>PM10M</timeDuration>
 </timerEventDefinition>
 */
function doLcTimeSelect() {
    //PnYnMnDTnHnMnS
    //P表示日期的开始（年月日），T表示时间的开始（时分秒）,即表示：固定n年n月n天n小时n分钟n秒钟
    var timeSelectType = $("#timeSelectType").val();
    var timeType =  $("#timeType").val();
    var timeYear = $("#timeYear").val();//年
    var timeM = $("#timeM").val();//月
    var timeDay = $("#timeDay").val();//日
    var timeH = $("#timeH").val();//时
    var timeMi = $("#timeMi").val();//分
    var timeS = $("#timeS").val();//秒
    var timeW =  $("#timeW").val();//周

    var retryTimes = $("#retryTimes").val();//重复次数
    var timeRetryFlag = $('#timeRetry').is(':checked');//是否可重复
    var timeSelectValue = "P";
    var timeSelectText = "";
    var timeSelectFull = "P";
    var timeRetry = "";
    var timeRetryText = "";
    if(timeType == 0){
        if(undefined != timeYear && null != timeYear && '' != timeYear && 0 != timeYear){
            timeSelectValue = timeSelectValue + timeYear+"Y";
            timeSelectText = timeSelectText + timeYear+"年";
        }
        if(undefined != timeM && null != timeM && '' != timeM && 0 != timeM){
            timeSelectValue = timeSelectValue + timeM+"M";
            timeSelectText = timeSelectText + timeM+"月";
        }
        if(undefined != timeDay && null != timeDay && '' != timeDay && 0 != timeDay){
            timeSelectValue = timeSelectValue + timeDay+"D";
            timeSelectText = timeSelectText + timeDay+"天";
        }
        var timeSFMSelectValue = "T";
        if(undefined != timeH && null != timeH && '' != timeH && 0 != timeH){
            timeSFMSelectValue = timeSFMSelectValue + timeH+"H";
            timeSelectText = timeSelectText + timeH+"时";
        }
        if(undefined != timeMi && null != timeMi && '' != timeMi && 0 != timeMi){
            timeSFMSelectValue = timeSFMSelectValue + timeMi+"M";
            timeSelectText = timeSelectText + timeMi+"分";
        }
        if(undefined != timeS && null != timeS && '' != timeS && 0 != timeS){
            timeSFMSelectValue = timeSFMSelectValue + timeS+"S";
            timeSelectText = timeSelectText + timeMi+"秒";
        }
        if(timeSFMSelectValue != "T"){
            timeSelectValue = timeSelectValue+timeSFMSelectValue;
        }
        timeSelectFull = timeSelectFull + timeYear+"Y" + timeM + "M"+ timeDay+"D" + timeH+"H"+ timeMi+"M" + timeS+"S";
    }else if(timeType == 1){
        if(undefined != timeW && null != timeW && '' != timeW && 0 != timeW){
            timeSelectValue  = timeSelectValue +timeW+ "W";
            timeSelectText = timeSelectText + timeW+"周";
            timeSelectFull = timeSelectFull +timeW+ "W";
        }
    }

    // console.log("--timeSelectValue-----",timeSelectValue);
    if(timeSelectValue == "P"||timeSelectValue == "PT"){
        toastrBoot(4,"请设置日期");
        return
    }

    if(timeSelectType == 1 || timeSelectType == 2 || timeSelectType == 3){//持续时间
        $("#timeDuration").val(timeSelectValue);
        $("#timeDurationText").val(timeSelectText);
        $("#timeSelectFull").val(timeSelectFull);
    }else if(timeSelectType == 11 || timeSelectType == 22 || timeSelectType == 33){//重复日期
        if(timeRetryFlag){//可重复
            if(undefined != retryTimes && "" != retryTimes && null != retryTimes){
                timeRetry = "R" + retryTimes+"/";
                timeRetryText = "每隔"+timeSelectText+"重复"+ retryTimes + "次";
                $("#timeCycleText").val(timeRetryText);
            }else{
                $("#timeCycleText").val(timeSelectText);
            }
        }else {
            $("#timeCycleText").val(timeSelectText);
        }
        $("#timeCycle").val(timeRetry+timeSelectValue);
        $("#timeCycleFull").val(timeRetry+timeSelectFull);
    }else{
        //缺省
    }

    if(timeSelectType == 1 || timeSelectType == 11){
        setInputValue(null,"TimerStartEvent");//定时器启动事件
    }else if(timeSelectType == 2 || timeSelectType == 22){
        setInputValue(null,"TimerBoundaryEvent");//边界事件
    }else if(timeSelectType == 3 || timeSelectType == 33){
        setInputValue(null,"TimerCatchingEvent");//定时捕捉事件
    }else{
        //缺省
    }
    $('#lcTimeSelectModal').modal('hide');
}

/**
 * 清空时间选择器
 * @param type
 */
function resetTimeSelect(type) {
    if(type == 1 || type == 2 || type == 3){
        $("#timeDuration").val("");
        $("#timeDurationText").val("");
        $("#timeSelectFull").val("");
    }

    if(type == 11 || type == 22 || type == 33){
        $("#timeCycle").val("");
        $("#timeCycleText").val("");
        $("#timeCycleFull").val("");
    }

    if(type == 1 || type == 11){
        setInputValue(null,"TimerStartEvent");//定时器启动事件
    }else if(type == 2 || type == 22){
        setInputValue(null,"TimerBoundaryEvent");//边界事件
    }else if(type == 3 || type == 33){
        setInputValue(null,"TimerCatchingEvent");//定时捕捉事件
    }else {

    }
}

/**
 * 动态设置日期控件
 */
function showDayDiv() {
    var timeType = $("#timeType").val();
    var timeSelectType = $("#timeSelectType").val();
    if(0 == timeType){
        $("#nyt").show();//显示
        $("#sfm").show();//显示
        $("#week").hide();//隐藏 周
    }else if(1 == timeType){
        $("#week").show();//显示
        $("#nyt").hide(); //隐藏 年月天
        $("#sfm").hide(); //隐藏 时分秒
    }

    if(timeSelectType == 11 || timeSelectType == 22 || timeSelectType == 33){//重复日期
        $("#timeRetryDiv").show();//显示
        if($('#timeRetry').is(':checked')) {
            $("#retryTimesDiv").show();//展示
        }else{
            $("#retryTimesDiv").hide();//隐藏
            $("#retryTimes").val(1);//重置值

        }
    }else{
        $("#timeRetryDiv").hide();//隐藏
        $("#retryTimesDiv").hide();//隐藏
    }
}

/**
 *
 * @param str
 */
function checkValue(str,id){
    var re=/^(?:[1-9]?\d|100)$/;
    if(re.test(str)){
        $("#"+id).val(str);
    }else{
        toastrBoot(4,"只能输入正整数！");
        $("#"+id).val('1');
    }
}

/**
 *
 */
function checkBoxChange() {
    if($('#timeRetry').is(':checked')) {
        $("#retryTimesDiv").show();//展示
    }else{
        $("#retryTimesDiv").hide();//隐藏
        $("#retryTimes").val(1);//重置值
    }
}

/**
 * 时间表达式选择器
 * @param nodeType 节点类型
 * @param elementType 元素类型
 */
function showTimeExp(nodeType,elementType) {
    var expSelectModalCount = 0 ;
    $('#expBody').height(reGetBodyHeight()-218);
    $('#expSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#expSelectModal').on("shown.bs.modal",function(){
        if(++expSelectModalCount == 1){
            var $modal_dialog = $("#expModalDialog");
            $modal_dialog.css({'margin': 0 + 'px auto'});
            $modal_dialog.css({'width':reGetBodyWidth()+'px'});
            $('#searchFormexp')[0].reset();
            var opt = {
                searchformId:'searchFormexp'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcExp/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex+1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"lc_exp_id",
                        width:"150px"
                    },
                    {
                        data:'lc_exp_name',
                        width:"150px"
                    },
                    {
                        data:'productName',
                        width:"150px"
                    },
                    {
                        data:'groupName',
                        width:"150px"
                    },
                    {
                        data:"lc_exp_id",
                        render:function(data, type, row, meta) {
                            var btn = '<button class="btn btn-light-primary font-weight-bold mr-2" onclick=doTimeSelectExp("'+data+'","'+nodeType+'","'+elementType+'")><span><span>确 认</span></span></button>';
                            return btn;
                        }
                    }
                ]
            });
            $('#expDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('expDatatables');
        }
    });
}

/**
 *
 * @param lc_exp_id
 * @param type
 * @param elementType
 * @param lc_exp_val
 */
function doTimeSelectExp(lc_exp_id,nodeType,elementType) {
    msgTishCallFnBoot("确定选择该表达式项？",function(){
        if(nodeType == 11){//定时事件
            $.ajax({
                type:"GET",
                url:workflowModules+"/lcExp/get/"+lc_exp_id,
                success: function(result){
                    if(complateAuth(result)){
                        result = result.data;
                        $('#timeCycleId').val(result.lc_exp_id);
                        $("#timeCycle").val(result.lc_exp_val);
                        $("#timeCycleText").val(result.lc_exp_name);
                        $("#timeCycleFull").val(result.lc_exp_val);
                        if(elementType == 11){ //定时器启动事件 表达式
                            setInputValue(null,"TimerStartEvent");//定时器启动事件
                        }

                        if(elementType == 22){ //时间边界节点 表达式
                            setInputValue(null,"TimerBoundaryEvent");//边界事件
                        }

                        if(elementType == 33){ //定时捕捉事件 表达式
                            setInputValue(null,"TimerCatchingEvent");//定时捕捉事件
                        }
                    }
                }
            });
        }
        $('#expSelectModal').modal('hide');
    });
}