//一般配置
var nodeNormalForm;
//创建Boot风格"一般配置" form
function createNodeNormalForm(cell,flag,mode){
	if(flag == 1){
		nodeNormalForm =
			"<div class=\"m-portlet\" style='height:150px;overflow: auto;'>"+
            	"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" method=\"post\">"+
					"<div class=\"m-portlet__body\">"+
					//名称
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;称</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" onchange='setInputValue(this,\""+mode+"\")' id=\"nodeName_\" name=\"nodeName_\" placeholder=\"请输入名称\">"+
						"</div>"+
					"</div>"+

					//编号
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" title='编号全局唯一'>编&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" readonly maxlength=\"50\" onchange='setInputValue(this,\""+mode+"\")' id=\"nodeID\" name=\"nodeID\" placeholder=\"请输入编号\">"+
						"</div>"+
					"</div>"+

					//是否异步
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >是否异步</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<select class=\"form-control\" id=\"asynchronous\" name=\"asynchronous\" onchange='setInputValue(this,\""+mode+"\")'><option value=''>请选择</option><option value='0'>是</option></select>"+
						"</div>"+
					"</div>"+

					//是否专属
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >是否专属</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<select class=\"form-control\" id=\"exclusive\" name=\"exclusive\" onchange='setInputValue(this,\""+mode+"\")'><option value=''>请选择</option><option value='0'>是</option></select>"+
						"</div>"+
					"</div>"+


					//补偿边界事件
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" title='补偿边界事件'>补偿边界</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<select class=\"form-control\" id=\"isForCompensation\" name=\"isForCompensation\" onchange='setInputValue(this,\""+mode+"\")'><option value=''>请选择</option><option value='1'>是</option></select>"+
						"</div>"+
					"</div>"+

					//备注
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" >备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<textarea class=\"form-control\" maxlength=\"500\" id=\"documentation\" name=\"documentation\" placeholder=\"请输入备注\" onchange='setInputValue(this,\""+mode+"\")'></textarea>"+
						"</div>"+
					"</div>"+

					"</div>"+
				"</form>"+
			"</div>";
    }else if(flag == 2){
        nodeNormalForm =
            "<div class=\"m-portlet\" style='height:150px;overflow: auto;'>"+
            	"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" method=\"post\">"+
					"<div class=\"m-portlet__body\">"+
						//名称
						"<div class=\"form-group row\">"+
							"<div class=\"col-md-1\">"+
								"<label class=\"col-form-label\" >名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;称</label>"+
							"</div>"+
							"<div class=\"col-md-4\">"+
								"<input class=\"form-control\" type=\"text\" maxlength=\"50\" id=\"nodeName_\" onchange='setInputValue(this,\""+mode+"\")' name=\"nodeName_\" placeholder=\"请输入名称\">"+
							"</div>"+
						"</div>"+

						//编号
						"<div class=\"form-group row\">"+
							"<div class=\"col-md-1\">"+
								"<label class=\"col-form-label\" >编&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号</label>"+
							"</div>"+
							"<div class=\"col-md-4\">"+
								"<input class=\"form-control\" type=\"text\" readonly maxlength=\"50\" id=\"nodeID\" name=\"nodeID\" onchange='setInputValue(this,\""+mode+"\")' placeholder=\"请输入编号\">"+
							"</div>"+
						"</div>"+

					"</div>"+
				"</form>"+
            "</div>";
	}
    return nodeNormalForm;
}
function initNodeNormalForm(cell,flag){
	/**
	  *一般配置信息
	**/
	if(flag == 1){
	 	var asynchronous = cell.asynchronous;
	 	var exclusive = cell.exclusive;
	 	var isForCompensation = cell.isForCompensation;
	    if(asynchronous == 0){
            $("#asynchronous").val(0);
	    }else{
            $("#asynchronous").val("");
	    }
	    if(exclusive == 0){
            $("#exclusive").val(0);
	    }else{
            $("#exclusive").val("");
	    }
	    if(isForCompensation == 1){
            $("#isForCompensation").val(1);
	    }else{
            $("#isForCompensation").val("");
	    }
	}else if(flag == 2){

	}
	 /**取值**/
	var nodeName = cell.value;
 	var nodeID = cell.nodeID;
 	var documentation = cell.documentation;
 	/**赋值**/
 	$("#nodeName_").val(nodeName);
    $("#nodeID").val(nodeID);
    $("#documentation").val(documentation);
}

/**赋值功能**/
function node_normal_setvalue(cell,flag){
    var nodeName_ = $("#nodeName_").val();
	var nodeID = $("#nodeID").val();
	/**赋值**/
 	if(null != nodeName_ && '' != nodeName_){
        cell.value = nodeName_;
    }else{
        window.parent.toastrBoot(4,'请输入名称!');
    	return false;
    }
    if(null != nodeID && '' != nodeID){
    	cell.nodeID = nodeID;
    }else{
        window.parent.toastrBoot(4,'请输入编号!');
    	return false;
    }
	if(flag == 1){
        var documentation = $("#documentation").val();
		/**取值**/
	 	var asynchronous = $("#asynchronous").val();
	 	var exclusive = $("#exclusive").val();
	 	var isForCompensation = $("#isForCompensation").val();
	 	/**赋值**/
	    if(asynchronous != null && asynchronous != ""){
	    	cell.asynchronous = $("#asynchronous").val();
	    }else{
	    	cell.asynchronous = 1;
	    }
        if(exclusive != null && exclusive != ""){
	    	cell.exclusive = $("#exclusive").val();
	    }else{
	    	cell.exclusive = 1;
	    }
	    // if(null != documentation && '' != documentation){
	    	cell.documentation = documentation;
	    // }
        if(null != isForCompensation && '' != isForCompensation){
	    	cell.isForCompensation = 1;
	    }else{
	    	cell.isForCompensation = 2;
	    }
	}else if(flag == 2){

	}
}