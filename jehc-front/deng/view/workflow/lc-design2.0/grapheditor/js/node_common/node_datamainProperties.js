//////////////数据属性事件子流程 子流程 事物流程 流程信息等地方使用///////////
//数据属性表格
var datamainProperties_grid;

/**
 * 创建数据属性数据源Grid
 */
function createDatamainPropertiesGrid(){
    datamainProperties_grid =
        "<div class=\"m-portlet\" style='height:150px;overflow: auto;'>"+
			"<div class=\"m-portlet__head\">"+
				"<div class=\"m-portlet__head-caption\">" +
					"<button class=\"btn btn-light-primary mr-3 m-btn--custom m-btn--icon\" onclick=\"addDatamainPropertiesRow()\" title='新一行'><i class=\"la la-plus\"></i>新一行</button>"+
					"&nbsp;&nbsp;<button class=\"btn btn-icon btn-light-success mr-2 m-btn--custom m-btn--icon\" title='保存' onclick='datamainProperties_setvalue()'><i class=\"la la-save\"></i>保存</button>"+
				"</div>"+
			"</div>"+

			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"node_datamainProperties_form\" method=\"post\">"+
				"<div class=\"m-portlet__body\" id='datamainProperties_grid'>"+
					//Title
					"<div class=\"form-group m-form__group row\" style='background: #f2f3f5;'>"+
						"<div class=\"col-md-3\">编&nbsp;号</div>"+
						"<div class=\"col-md-3\">名 称</div>"+
        				"<div class=\"col-md-2\">类 型</div>"+
						"<div class=\"col-md-3\">值</div>"+
						"<div class=\"col-md-1\"></div>"+
					"</div>"+
				"</div>"+
			"</form>"+
        "</div>";
    return datamainProperties_grid;
}


/**
 * 新一行
 * @param ID
 * @param name
 * @param type
 * @param value
 */
function addDatamainPropertiesRow(ID,name,type,value){
    if(ID === undefined){
        ID = "";
    }
    if(name === undefined){
        name = "";
    }
    if(type === undefined){
        type = "";
    }
    if(value === undefined){
        value = "";
    }

    var uuid = guid();
    validatorDestroy('node_datamainProperties_form');
    data:[["string",""],["","boolean"],["","datetime"],["","double"],["","int"],["","long"]]
    var typeOption =  "<option value=''>请选择</option>" +
        "<option value='string'>string</option>" +
        "<option value='boolean'>boolean</option>"+
        "<option value='datetime'>datetime</option>"+
        "<option value='double'>double</option>"+
        "<option value='int'>int</option>"+
        "<option value='long'>long</option>";

    var rows =
        "<div class=\"form-group m-form__group row\" id='node_datamainProperties_rows_"+uuid+"'>"+
			//编号
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入编号' id='ID"+uuid+"' name='dataMainProperties[][ID]' value='"+ID+"' placeholder='请输入编号'>"+
			"</div>"+

			//名称
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\" id='name"+uuid+"' data-bv-notempty data-bv-notempty-message='请输入名称' name=\"dataMainProperties[][name]\" value='"+name+"' placeholder=\"请输入名称\">"+
			"</div>"+

			//类型
			"<div class=\"col-md-2\">"+
				"<select class=\"form-control\" id='type"+uuid+"' name=\"dataMainProperties[][type]\">" +
					typeOption+
				"</select>"+
			"</div>"+

			//值
			"<div class=\"col-md-3\">"+
				"<input class=\"form-control\" type=\"text\" id='value"+uuid+"' data-bv-notempty data-bv-notempty-message='请输入名称' name=\"dataMainProperties[][value]\" value='"+value+"' placeholder=\"请输入名称\">"+
			"</div>"+

			//操作
			"<div class=\"col-md-1\">"+
				"<button type=\"button\" class=\"btn btn-light-danger mr-3\" onclick='delDatamainPropertiesRow(\""+uuid+"\")' title='删除'><i class=\"la la-times\"></i></button>"+
			"</div>"+
        "</div>";
    $("#datamainProperties_grid").append(rows);
    $("#type"+uuid).val(type);
    reValidator('node_datamainProperties_form');
}

/**
 * 删除
 * @param rowID
 */
function delDatamainPropertiesRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('node_datamainProperties_form');
        $("#node_datamainProperties_rows_"+rowID).remove();
        reValidator('node_datamainProperties_form');
    });
}

/**
 * 初始化表单及数据
 */
function initDatamainProperties_grid(){
    //表单配置
    $('#node_datamainProperties_form').bootstrapValidator({
        message:'此值不是有效的'
    });
    initDatamainProperties_data();
}


/**
 * 初始化列数据
 */
function initDatamainProperties_data(){
    var datamainProperties_node_value = $("#datamainProperties_node_value").val();
    if(null != datamainProperties_node_value && "" != datamainProperties_node_value){
        datamainProperties_node_value = eval('(' + datamainProperties_node_value + ')');
        datamainProperties_node_value = datamainProperties_node_value.dataMainProperties;
        if(datamainProperties_node_value != undefined){
            for(var i = 0; i < datamainProperties_node_value.length; i++){
                var name = datamainProperties_node_value[i].name;
                var ID = datamainProperties_node_value[i].ID;
                var type = datamainProperties_node_value[i].type;
                var value = datamainProperties_node_value[i].value;
                addDatamainPropertiesRow(ID,name,type,value);
            }
        }
        /*已废弃
        var IDList = $.makeArray(datamainProperties_node_value["ID"]);
        var nameList = $.makeArray(datamainProperties_node_value["name"]);
        var typeList = $.makeArray(datamainProperties_node_value["type"]);
        var valueList = $.makeArray(datamainProperties_node_value["value"]);
        for(var i = 0; i < nameList.length; i++){
            var name = nameList[i];
            var ID = IDList[i];
            var type = typeList[i];
            var value = valueList[i];
            addDatamainPropertiesRow(ID,name,type,value);
        }
        */
    }
}

/**
 * 点击确定按钮
 * @param cell
 */
function datamainProperties_setvalue(){
    var bootform = $('#node_datamainProperties_form');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return;
    }
    /* 已废弃
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    $("#datamainProperties_node_value").val(JSON.stringify($("#node_datamainProperties_form").serializeObject()));
    */
    $("#datamainProperties_node_value").val(JSON.stringify($("#node_event").serializeJSON()));
}