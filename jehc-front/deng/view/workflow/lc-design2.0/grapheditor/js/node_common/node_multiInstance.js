/**
  *配置会签
**/
var multiInstanceLoopCharacteristicForm;

/**
 * 创建会签表单
 * @param cell
 */
function createMultiInstance(cell,mode){
    var isSequentialOption =
        "<option value=''>不启动</option>"+
        "<option value='true'>多实例顺序执行</option>"+
        "<option value='false'>多实例并行执行</option>";

    multiInstanceLoopCharacteristicForm =
        "<div class=\"m-portlet\" style='height:150px;overflow: auto;'>"+
			"<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
				"<div class=\"m-portlet__body\">"+
					//执行顺序
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" title='必选项，可选值有true、false。用于设置多实例的执行顺序。True：多实例顺序执行，false：多实例并行'>执行顺序</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<select class=\"form-control\" onchange='setInputValue(this,\""+mode+"\")' id='isSequential' name=\"isSequential\">" +
        						isSequentialOption+
							"</select>"+
						"</div>"+
					"</div>"+

					//循环基数
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" title='可选项。可以直接填整数，表示会签的人数。'>循环基数</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<input class=\"form-control\" type=\"text\" onchange='setInputValue(this,\""+mode+"\")' maxlength=\"50\"  id=\"loopCardinality\" name=\"loopCardinality\" placeholder=\"请输入循环基数\">"+
						"</div>"+
					"</div>"+

					//集合
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" title='可选项。会签人数的集合，通常为list。循环基数二选一' >集&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;合</label>"+
						"</div>"+
						"<div class=\"col-md-3\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\"  onchange='setInputValue(this,\""+mode+"\")' id=\"collection\" name=\"collection\" placeholder=\"请输入集合\">"+
						"</div>"+
						"<div class=\"col-md-8\"><div class='alert m-alert m-alert--default' role='alert'><code>说明：设置集合时候，该集合为EL表达式如${aplyList}，表示为选举审批人编号集合</code> .</div></div>"+
					"</div>"+
					//元素变量
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" title='选择集合时必选，即collection集合每次遍历的元素'>元素变量</label>"+
						"</div>"+
						"<div class=\"col-md-2\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" onchange='setInputValue(this,\""+mode+"\")' id=\"elementVariable\" name=\"elementVariable\" placeholder=\"请输入元素变量\">"+
						"</div>"+
						"<div class=\"col-md-9\"><div class='alert m-alert m-alert--default' role='alert'><code>说明：设置变量时候，该变量与任务节点审批人，候选人，组等采用的选举表达式值一致</code> .</div></div>"+
					"</div>"+

					//完成条件
					"<div class=\"form-group row\">"+
						"<div class=\"col-md-1\">"+
							"<label class=\"col-form-label\" title='可选。Activiti会签有个特性，如设置一个人完成后会签结束，那么其他人的代办任务都会消失。'>完成条件</label>"+
						"</div>"+
						"<div class=\"col-md-4\">"+
							"<input class=\"form-control\" type=\"text\" maxlength=\"50\" onchange='setInputValue(this,\""+mode+"\")' id=\"completionCondition\" name=\"completionCondition\" placeholder=\"请输入完成条件\">"+
						"</div>"+
					"</div>"+

       			 "</div>"+
			"</form>"+
        "</div>";
    return multiInstanceLoopCharacteristicForm;
}

/**
 * 初始化数据
 * @param cell
 */
function initMultiInstanceData(cell){
    /**取值**/
	var isSequential = cell.isSequential;
 	var loopCardinality = cell.loopCardinality;
 	var collection = cell.collection;
 	var elementVariable = cell.elementVariable;
 	var completionCondition = cell.completionCondition;
 	/**赋值**/
    $('#isSequential').val(isSequential);
    $('#loopCardinality').val(loopCardinality);
    $('#collection').val(collection);
    $('#elementVariable').val(elementVariable);
    $('#completionCondition').val(completionCondition);
}

/**
 * 赋值
 * @param cell
 */
function multi_instance_setvalue(cell){
	var isSequential = $('#isSequential').val();
	var loopCardinality = $('#loopCardinality').val();
	var collection = $('#collection').val();
	var elementVariable = $('#elementVariable').val();
	var completionCondition = $('#completionCondition').val();
 	// if(null != isSequential && '' != isSequential){
    	cell.isSequential = isSequential;
    // }
    // if(null != loopCardinality && '' != loopCardinality){
    	cell.loopCardinality = loopCardinality;
    // }
    // if(null != collection && '' != collection){
    	cell.collection = collection;
    // }
    // if(null != elementVariable && '' != elementVariable){
    	cell.elementVariable = elementVariable;
    // }
    // if(null != completionCondition && '' != completionCondition){
    	cell.completionCondition = completionCondition;
    // }
}