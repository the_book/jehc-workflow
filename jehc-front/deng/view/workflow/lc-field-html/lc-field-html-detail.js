//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-field-html/lc-field-html-list.html');
}
$(document).ready(function(){
	var lc_field_html_id = GetQueryString("lc_field_html_id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcFieldHtml/get/'+lc_field_html_id,{},function(result){
		$('#name').val(result.data.name);
		$('#code').val(result.data.code);
		$('#type').val(result.data.type);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);

	});
});
