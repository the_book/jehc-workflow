//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-field-html/lc-field-html-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateLcFieldHtml(){
	submitBForm('defaultForm',workflowModules+'/lcFieldHtml/update',base_html_redirect+'/workflow/lc-field-html/lc-field-html-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var lc_field_html_id = GetQueryString("lc_field_html_id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcFieldHtml/get/'+lc_field_html_id,{},function(result){
		$('#lc_field_html_id').val(result.data.lc_field_html_id);
        $('#name').val(result.data.name);
		$('#code').val(result.data.code);
		$('#type').val(result.data.type);
	});
});
