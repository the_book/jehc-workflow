/**
 * 运行时流程变量
 * @param instId
 */
function initActRuIdentitylink(executionId,instId) {
    $("#procInstId2").val(instId);
    $("#executionId2").val(executionId);
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchFormActRuIdentitylink'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/actRuIdentitylink/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(0)', nRow).html(iDisplayIndex +1);
            // var executionId_ = aData.executionId;
            // if (executionId_ == executionId) {
            //     //给行添加背景色
            //     $(nRow).css("background", "#e1f0ff");
            //     //给行内的字添加颜色
            //     // $(nRow).css("color", "#fff");
            //     $(nRow).css("font-size", 18);
            //     $(nRow).css("font-weight", 500);
            // }
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        /*tableWidth:reGetBodyWidth()*0.65+'px',*/
        sPaginationType:'simple_numbers',
        //列表表头字段
        colums:[
            {
                data:"id",
                width:"50px"
            },
            {
                data:'rev',
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--info'>"+data+"</span>";
                }
            },
            {
                data:'groupId'
            },
            {
                data:"type"
            },
            {
                data:"procDefId"
            },
            {
                data:"procInstId"
            },
            {
                data:"taskId"
            },
            {
                data:"userId"
            },
            {
                data:"id",
                render:function(data, type, row, meta) {
                    var procInstId = row.procInstId;
                    var btn = "<button class='btn btn-link' onclick=javascript:delActRuIdentitylink('"+ data +"','"+procInstId+"')>删 除</button>";
                    return btn;
                }
            }
        ]
    });
    $('#datatablesActRuIdentitylink').dataTable(options);
    //实现单击行选中
    clickrowselected('datatablesActRuIdentitylink');
}

/**
 * 删除
 * @param id
 * @param procInstId
 */
function delActRuIdentitylink(id,procInstId) {
    msgTishCallFnBoot("确定要删除该运行时人员？",function(){
        var params = {id:id,procInstId:procInstId,_method:'DELETE'};
        ajaxBReq(workflowModules+'/actRuIdentitylink/delete',params,['datatablesActRuIdentitylink'],null,"DELETE");
    })
}