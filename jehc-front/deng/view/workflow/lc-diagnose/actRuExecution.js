/**
 * 运行实例详情
 * @param id
 */
function initActRuExecution(id,procInstId){
    var actRuExecutionModalCount = 0 ;
    $('#actRuExecutionPanelBody').height(reGetBodyHeight()-218);
    $('#actRuExecutionModal').modal({backdrop:'static',keyboard:false});
    $('#actRuExecutionModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++actRuExecutionModalCount == 1) {
            //加载表单数据
            ajaxBRequestCallFn(workflowModules+'/actRuExecution/get/'+id,{},function(result){
                $('#id').val(result.data.id);
                $('#rev').val(result.data.rev);
                $('#procInstId').val(result.data.procInstId);
                $('#parentId').val(result.data.parentId);
                $('#procDefId').val(result.data.procDefId);
                $('#businessKey').val(result.data.businessKey);
                $('#superExec').val(result.data.superExec);
                $('#actId').val(result.data.actId);
                $('#isConcurrent').val(result.data.isConcurrent);
                $('#isScope').val(result.data.isScope);
                $('#isEventScope').val(result.data.isEventScope);
                $('#suspensionState').val(result.data.suspensionState);
                $('#cachedEntState').val(result.data.cachedEntState);
                $('#tenantId').val(result.data.tenantId);
                $('#name').val(result.data.name);
            });
            initActRuVariable(id,procInstId);
            initActRuIdentitylink(id,procInstId);
            initActRuTask(id,procInstId);
        }
        var $modal_dialog = $("#actRuExecutionModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.9+'px'});
    });
}