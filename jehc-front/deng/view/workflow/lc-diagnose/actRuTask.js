/**
 * 运行时任务
 * @param instId
 */
function initActRuTask(executionId,instId) {
    $("#procInstId3").val(instId);
    $("#executionId3").val(executionId);
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchFormActRuTask'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/actRuTask/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(0)', nRow).html(iDisplayIndex +1);
            // var executionId_ = aData.executionId;
            // if (executionId_ == executionId) {
            //     //给行添加背景色
            //     $(nRow).css("background", "#e1f0ff");
            //     //给行内的字添加颜色
            //     // $(nRow).css("color", "#fff");
            //     $(nRow).css("font-size", 18);
            //     $(nRow).css("font-weight", 500);
            // }
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        tableWidth:reGetBodyWidth()*0.65+'px',
        /*tableWidth:reGetBodyWidth()*0.65+'px',*/
        sPaginationType:'simple_numbers',
        //表头固定
        fixedHeader:true,
        fixedColumns: {
            "leftColumns": 3,
            "rightColumns": 1
        },
        //列表表头字段
        colums:[
            {
                data:"id",
                width:"50px"
            },
            {
                data:'rev',
                width:"50px",
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--info'>"+data+"</span>";
                }
            },
            {
                data:"id",
                width:"150px"
            },
            {
                data:'executionId',
                width:"150px"
            },
            {
                data:"procDefId",
                width:"50px"
            },
            {
                data:"procInstId",
                width:"50px"
            },
            {
                data:"name",
                width:"50px"
            },
            {
                data:"parentTaskId",
                width:"50px"
            },
            {
                data:"description",
                width:"50px"
            },
            {
                data:"taskDefKey",
                width:"50px"
            },
            {
                data:"owner",
                width:"50px"
            },
            {
                data:"assignee",
                width:"50px"
            },
            {
                data:"delegation",
                width:"50px"
            },
            {
                data:"priority",
                width:"50px",
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--brand'>"+data+"</span>"
                }
            },
            {
                data:"createTime",
                width:"50px",
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'>"+dateformat(data)+"</span>";
                }
            },
            {
                data:"dueDate",
                width:"50px"
            },
            {
                data:"category",
                width:"50px"
            },
            {
                data:"tenantId",
                width:"50px"
            },
            {
                data:"formKey",
                width:"50px"
            },
            {
                data:"suspensionState",
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 2){
                        return "<span class='m-badge m-badge--info'>挂起</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--success'>激活</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
            },
            {
                data:"id",
                width:"50px",
                render:function(data, type, row, meta) {
                    var procInstId = row.procInstId;
                    var btn = "<button class='btn btn-link' onclick=javascript:delActRuTask('"+ data +"','"+procInstId+"')>删 除</button>";
                    return btn;
                }
            }
        ]
    });
    var dataTable = $('#datatablesActRuTask').dataTable(options);
    //实现单击行选中
    clickrowselected('datatablesActRuTask');

    $('#datatablesActRuTask').width($(".scrolltable").width());
    // new $.fn.dataTable.FixedColumns(dataTable,{
    //     "iLeftColumns":2,
    //     "iRightColumns":1,
    //     "drawCallback": function(){
    //         //重绘Icheck 这里是我封装的初始化方法
    //         iCheckInitFunction();
    //         //重新设置全选事件 这里是我全选/反选的注册事件
    //         TableiCheck(".DTFC_Cloned thead tr th input.i-checks", ".DTFC_Cloned tbody tr td input.i-checks");
    //     }
    // });
}

/**
 * 删除
 * @param id
 * @param procInstId
 */
function delActRuTask(id,procInstId) {
    msgTishCallFnBoot("确定要删除该任务？",function(){
        var params = {id:id,procInstId:procInstId,_method:'DELETE'};
        ajaxBReq(workflowModules+'/actRuTask/delete',params,['datatablesActRuTask'],null,"DELETE");
    })
}