var grid;
$(document).ready(function() {
    var currentListCount = 0;
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/actRuExecution/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(0)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        // "createdRow": function( row, data, dataIndex ) {
        //     alert();
        //     if(dataIndex==0){
        //         $(row).children().eq(0).attr("rowspan",currentListCount);
        //         $(row).children().eq(1).attr("rowspan",currentListCount);
        //         $(row).children().eq(2).attr("rowspan",currentListCount);
        //         $(row).children().eq(3).attr("rowspan",currentListCount);
        //     }else{
        //         $(row).children().eq(0).hide()
        //         $(row).children().eq(1).hide()
        //         $(row).children().eq(2).hide()
        //         $(row).children().eq(3).hide()
        //     }
        // },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:reGetBodyHeight()*0.55+'px',
        //表头固定
        fixedHeader:true,
        fixedColumns: {
            "leftColumns": 2,
            "rightColumns": 1
        },
        columsdefs:[{
            "targets": [2,3,4], //第3,4，5列
            "orderable" : false,
            "data":null,
            "createdCell": function (td, cellData, rowData, row, col) {
                var rowspan = 1;
                if(col == 2){
                    rowspan = rowData.parentIdRowSpan;
                }
                if(col == 3){
                    rowspan = rowData.procInstIdRowSpan;
                }
                // if(col ==4){
                //     rowspan = rowData.procDefIdRowSpan;
                // }
                if (rowspan > 1) {
                    $(td).attr('rowspan', rowspan)
                }
                if (rowspan == 0) {
                    $(td).remove();
                }
            }
        }],
        //列表表头字段
        colums:[
            {
                data:"id",
                width:"50px"
            },
            {
                data:'id'
            },
            {
                data:'parentId'
            },
            {
                data:'procInstId'/*,
                render:function(data, type, row, meta) {
                    return "<button onclick='initActRuVariable("+data+")' title='运行时变量' class='btn btn-secondary m-btn m-btn--custom m-btn--label-info m-btn--bolder'>"+data+"</button>"
                }*/
            },
            {
                data:'procDefId'
            },
            {
                data:'suspensionState',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 2){
                        return "<span class='label label-lg font-weight-bold label-light-info label-inline'>挂起</span>"
                    }
                    if(data == 1){
                        return "<span class='label label-lg font-weight-bold label-light-success label-inline'>激活</span>"
                    }
                    return "<span class='label label-lg font-weight-bold label-light-info label-inline'>缺省</span>"
                }
            },
            {
                data:'businessKey'
            },
            {
                data:'isConcurrent',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--info'>否</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--brand '>是</span>"
                    }
                    return "<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>缺省</span>"
                }
            },
            {
                data:'actId'
            },
            {
                data:'isActive',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--danger'>否</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--info'>是</span>"
                    }
                    return "<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>缺省</span>"
                }
            },
            {
                data:'cachedEntState',
                width:"50px",
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--warning'>"+data+"</span>"
                }
            },
            {
                data:"id",
                render:function(data, type, row, meta) {
                    var parentId = row.parentId;
                    var procInstId = row.procInstId;
                    var isActive = row.isActive;

                    var btn = "<button class='btn btn-link' onclick=javascript:initActRuExecution('"+ data +"','"+procInstId+"')>详 情</button>";
                        btn += "<button class='btn btn-link' title='删除运行实例及其相关' onclick=javascript:deleteActRuExecution('"+ data +"','"+procInstId+"')>删 除</button>";
                    // if(1 == isActive){
                    //     btn += "<button class='btn btn-link' onclick=javascript:initActRuExecution('"+ data +"','"+procInstId+"')>运行任务</button>";
                    // }
                    return btn;
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
});

$('#TabCol a').click(function (e) {
    e.preventDefault()
    $(this).tab('show');
    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
})

/**
 * 删除运行实例及其相关
 * @param id
 * @param procInstId
 */
function deleteActRuExecution(id,procInstId) {
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var params = {id:id,procInstId:procInstId,_method:'DELETE'};
        ajaxBReq(workflowModules+'/actRuExecution/delete',params,['datatables'],null,"DELETE");
    })
}