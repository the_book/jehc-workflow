var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcTaskHis/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(0)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        fixedHeader:true,//表头固定
        fixedColumns: {
            "leftColumns": 2
        },
        //列表表头字段
        colums:[
            {
                data:"taskId",
                width:"50px"
            },
            {
                data:"taskId",
                render:function(data, type, row, meta) {
                    var processInstanceId = row.processInstanceId;
                    var taskId = row.taskId;
                    var btn = '';
                    btn = btn+ '<div class="btn-group">' +
                        '<a href="#" class="btn label-light-success font-weight-bold" data-toggle="dropdown" aria-expanded="true">' +
                        '<i class="la la-bars"></i>操作' +
                        '</a>' +
                        '<button type="button" class="btn btn-light-success font-weight-bold dropdown-toggle dropdown-toggle-split mr-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<span class="sr-only"></span>'+
                        '</button>'+
                        '<div class="dropdown-menu dropdown-menu-right">' +
                        '<button class="dropdown-item" onclick=showLcProcessInstance("' + processInstanceId + '")><i class="la la-image"></i>流程实例图</button>' +
                        '<button class="dropdown-item" onclick=initLcApprovalWin("' + processInstanceId + '")><i class="fa flaticon-graphic-1"></i>审批记录</a>' +
                        '<button class="dropdown-item" onclick=toLcTaskHisDetail("' + taskId + '")><i class="la la-eye"></i>详情</a>' +
                        '</div>' +
                        '</div>';
                    // btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="查看流程实例图" onclick=showLcProcessInstance('+processInstanceId+')><i class="la la-image"></i></button>';
                    // btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="查看流程实例审批记录" onclick=initLcApprovalWin('+processInstanceId+')><i class="fa flaticon-graphic-1"></i></button>';
                    // btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情" onclick=toLcTaskHisDetail('+taskId+')><i class="la la-eye"></i></button>';
                    btn = btn +'<button class="btn btn-light-primary font-weight-bold" title="撤回" onclick=callBack('+taskId+')><i class="fa flaticon-reply"></i>撤回</button>';
                    return btn;
                }
            },
            {
                data:'name'
            },
            {
                data:'processTitle'
            },
            {
                data:'productName'
            },
            {
                data:'groupName'
            },
            {
                data:'version',
                render:function(data, type, row, meta) {
                    return "<span class='label label-light-warning mr-2'>"+data+"</span>"
                }
            },
            {
                data:'owner'
            },
            {
                data:'assignee'
            },
            {
                data:'tenantId'
            },
            {
                data:'processInstanceId',
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>"+data+"</span>"
                }
            },
            {
                data:'createTime',
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:'endTime',
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:'durationInMillis',
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>"+formateDhms(data)+"</span>"
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现单击行选中
    clickrowselected('datatables');

    //产品线下拉框数据
    initProductList("lc_product_id");
});



function showLcProcessInstance(id){
    $('#lcImagePanelBody').height(reGetBodyHeight()-128);
    $('#lcImageModalLabel').html("实例监控图");
    $('#lcImageModal').modal({backdrop:'static',keyboard:false});
    $("#lcImageIframe",document.body).attr("src",base_html_redirect+"/workflow/lc-image/image.html?id="+id);
    $('#lcImageModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcImageModalDialog");
        /**
         //获取可视窗口的高度
         var clientHeight = (document.body.clientHeight < document.documentElement.clientHeight) ? document.body.clientHeight: document.documentElement.clientHeight;
         //得到dialog的高度
         var dialogHeight = $modal_dialog.height();
         //计算出距离顶部的高度
         var m_top = (clientHeight - dialogHeight)/2;
         console.log("clientHeight : " + clientHeight);
         console.log("dialogHeight : " + dialogHeight);
         console.log("m_top : " + m_top);
         $modal_dialog.css({'margin': m_top + 'px auto'});
         **/
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
    });
}

/**
 *
 */
function closeLcImageWin(){
    search('datatables');
}

/**
 *
 * @param id
 */
function initLcApprovalWin(id){
    var approvalCount = 0 ;
    $('#lcHisLogPanelBody').height(reGetBodyHeight()-128);
    $('#lcHisLogModal').modal({backdrop:'static',keyboard:false});
    $('#lcHisLogModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcHisLogModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.6+'px'});
        if(++approvalCount == 1) {
            $('#searchFormApproval')[0].reset();
            $('#instanceId').val(id);
            var opt = {
                searchformId: 'searchFormApproval'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax: function (data, callback, settings) {
                    datatablesCallBack(data, callback, settings, workflowModules + '/lcApproval/list', opt);
                },//渲染数据
                //在第一位置追加序列号
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex + 1);
                    return nRow;
                },
                order: [],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight: '300px',
                //列表表头字段
                colums: [
                    {
                        sClass: "text-center",
                        width: "50px",
                        data: "lcApprovalId",
                        render: function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildApproval" value="' + data + '" /><span></span></label>';
                        },
                        bSortable: false
                    },
                    {
                        data: "lcApprovalId",
                        width: "50px"
                    },
                    {
                        data: 'behavior',
                        render: function (data, type, full, meta) {
                            return formatBehavior(data)
                        }
                    },
                    {
                        data: 'comment'
                    },
                    {
                        data: 'create_time'
                    },
                    {
                        data: 'createBy'
                    }
                ]
            });
            grid = $('#approvalDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallApproval', 'checkchildApproval');
            //实现单击行选中
            // clickrowselected('approvalDatatables');
        }
    });
}


function formateDhms(timestamp) {

    if (timestamp) {
        timestamp = timestamp/1000;
        var result = '';
        if (timestamp >= 86400) {
            var days = Math.floor(timestamp / 86400);
            timestamp = timestamp % 86400;
            result = days  + ' 天';
            if (timestamp > 0) {
                result += ' ';
            }
        }
        if (timestamp >= 3600) {
            var hours = Math.floor(timestamp / 3600);
            timestamp = timestamp % 3600;
            result += hours + ' 小时';
            if (timestamp > 0) {
                result += ' ';
            }
        }
        if (timestamp >= 60) {
            var minutes = Math.floor(timestamp / 60);
            timestamp = timestamp % 60;
            result += minutes + ' 分钟';
            if (timestamp > 0) {
                result += ' ';
            }
        }
        result += parseInt(timestamp) + ' 秒';
        return result;
    }
    return "";
}

/**
 *
 * @param id
 */
function initProductList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"POST",
        url:workflowModules+"/lcProduct/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.lc_product_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
        }
    });
}

/**
 *
 * @param id
 * @param value_id
 * @param lc_product_id
 */
function initGroupListSetV(id,value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    if(null != $("#"+value_id).val() && '' != $("#"+value_id).val()){
        $.ajax({
            type:"GET",
            url:workflowModules+"/lcGroup/find/"+$("#"+value_id).val(),
            success: function(result){
                result = result.data;
                //从服务器获取数据进行绑定
                $.each(result, function(i, item){
                    str += "<option value=" + item.lc_group_id + ">" + item.name + "</option>";
                })
                $("#"+id).append(str);
            }
        });
    }else{
        $("#"+id).append(str);
    }
}

/**
 * 任务详情
 * @param lc_process_id
 * @param lc_process_title
 */
function toLcTaskHisDetail(id){
    $('#lcTaskDetailBody').height(reGetBodyHeight()*0.5);
    $('#lcTaskDetailModal').modal({backdrop:'static',keyboard:false});
    $('#lcTaskDetailModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcTaskDetailModalDialog");
        $modal_dialog.css({'width':reGetBodyWidth()*0.5+'px'});
        //加载表单数据
        ajaxBRequestCallFn(workflowModules+'/lcTaskHis/get/'+id,{},function(result){
            $('#processTitle').val(result.data.processTitle);
            $('#productName').val(result.data.productName);
            $('#groupName').val(result.data.groupName);
            $('#version').val(result.data.version);
            // $('#').html(result.data.lc_deployment_his_status==0?"<font color='#008b8b'>【运行中】</font>":"<font color='#8b0000'>【关闭】</font>");
            $('#name').val(result.data.name);
            $('#assignee').val(result.data.assignee);
            $('#taskId').val(result.data.taskId);
            $('#owner').val(result.data.owner);
            $('#processDefinitionId').val(result.data.processDefinitionId);
            $('#processInstanceId').val(result.data.processInstanceId);
            $('#taskDefinitionKey').val(result.data.taskDefinitionKey);
            $('#tenantId').val(result.data.tenantId);
            $('#startTime').val(dateformat(result.data.startTime));
            $('#endTime').val(dateformat(result.data.endTime));
            $('#durationInMillis').val(formateDhms(result.data.durationInMillis));
        });
    });
}

var userType;
/**
 *
 * @param type
 */
function initAssignee(type){
    userType = type;
    var UserinfoSelectModalCount = 0 ;
    $('#UserinfoBody').height(reGetBodyHeight()-128);
    $('#UserinfoSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#UserinfoSelectModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#UserinfoModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++UserinfoSelectModalCount == 1){
            $('#searchFormUserinfo')[0].reset();
            var opt = {
                searchformId:'searchFormUserinfo'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        sClass:"text-center",
                        width:"20px",
                        data:"xt_userinfo_id",
                        render:function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUserinfo" value="' + data + '" /><span></span></label>';
                        },
                        bSortable:false
                    },
                    {
                        data:"xt_userinfo_id",
                        width:"20px"
                    },
                    {
                        data:'xt_userinfo_name'
                    },
                    {
                        data:'xt_userinfo_realName'
                    },
                    {
                        data:'xt_userinfo_phone'
                    },
                    {
                        data:'xt_userinfo_origo'
                    },
                    {
                        data:'xt_userinfo_birthday'
                    },
                    {
                        data:'xt_userinfo_email'
                    }
                ]
            });
            grid=$('#UserinfoDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallUserinfo','checkchildUserinfo');
            //实现单击行选中
            clickrowselected('UserinfoDatatables');
        }
    });
}


function doSelectUser(){
    if(returncheckedLength('checkchildUserinfo') <= 0){
        toastrBoot(4,"请选择员工");
        return;
    }
    //CAS 1. 发起人
    if(userType == 1) {
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        var params = {xt_userinfo_id:xt_userinfo_id};
        //加载表单数据
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/list",params,function(result){
            var arr = result.data;
            var xt_userinfo_realName;
            for (var i = 0; i < arr.length; i++) {
                //名称维护
                if (null == xt_userinfo_realName) {
                    xt_userinfo_realName =arr[i].xt_userinfo_realName;
                } else {
                    xt_userinfo_realName = xt_userinfo_realName + "," + arr[i].xt_userinfo_realName;
                }
            }
            msgTishCallFnBoot('确定要选择:<br>'+ xt_userinfo_realName + '？',function(){
                $('#owner_').val(xt_userinfo_id);
                $('#owner_Text').val(xt_userinfo_realName);
                $('#UserinfoSelectModal').modal('hide');
            });
        },null,"post");
    }

    //CAS 2.经办人单选
    if(userType == 2){
        if(returncheckedLength('checkchildUserinfo') != 1){
            toastrBoot(4,"员工只能选择一条记录！");
            return;
        }
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        //加载表单数据
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+xt_userinfo_id,{},function(result){
            var xt_userinfo_realName = result.data.xt_userinfo_realName;
            var str ="[<font color=red><br>用户姓名:" +result.data.xt_userinfo_realName + "<br>所属部门:" + result.data.xt_departinfo_name+"<br>所属岗位:"+result.data.xt_post_name+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){
                $('#assignee_').val(xt_userinfo_id);
                $('#assignee_Text').val(xt_userinfo_realName);
                $('#UserinfoSelectModal').modal('hide');
            });
        });
    }
}

/**
 * 加载数据完成事件
 * @param event
 * @param treeId
 * @param treeNode
 * @param msg
 */
function onAsyncSuccess(event, treeId, treeNode, msg){
    closeWating(null,dialogWating);
}


/**
 * 单击事件
 * @param event
 * @param treeId
 * @param treeNode
 * @param msg
 */
function onClick(event, treeId, treeNode, msg){
    var id = treeNode.id;
    var tempObject = treeNode.tempObject;
    if(tempObject == 'DEPART'){

    }
    if(tempObject == 'POST'){

    }
}

/**
 * 初始数据
 */
var dialogWating;
function InitztData() {
    var setting = {
        view:{
            selectedMulti:false,
            showLine:false
        },
        check:{
            enable:false
        },
        /*async:{
            enable:true,//设置 zTree是否开启异步加载模式  加载全部信息
            url:sysModules+"/xtOrg/list",//Ajax获取数据的 URL地址
            type:"GET",//PUT DELETE POST
            otherParam:{
                'expanded':function(){return 'true'}
            } //异步参数
        },*/
        data:{
            //必须使用data
            simpleData:{
                enable:true,
                idKey:"id",//id编号命名 默认
                pIdKey:"pId",//父id编号命名 默认
                rootPId:0 //用于修正根节点父节点数据，即 pIdKey 指定的属性值
            }
        },
        edit:{
            enable:false
        },
        callback:{
            onClick:onClick,//单击事件
            onAsyncSuccess:onAsyncSuccess//加载数据完成事件
        }
    };
    dialogWating = showWating({msg:'正在拼命的加载中...'});
    ajaxBRequestCallFn(sysModules+"/xtOrg/list",{},function(result){
        zTreeNodes = eval("(" + result.data + ")");
        treeObj = $.fn.zTree.init($("#tree"), setting,zTreeNodes);
        // treeObj.expandAll(true);
        closeWating(null,dialogWating);
    });
}


/**
 * 初始化候选组（采用部门，岗位等）
 * @param flag flag标识是1单个组选择2支持多选
 * @param type type类型1在UserTask中使用2在“流程基本信息使用”3在泳道中使用
 *
 * 标准格式"    ['部门编号','0'],['岗位编号','1']   "
 */
var orgFlag;
var orgType;
function initcandidateGroups(flag,type){
    orgFlag = flag;
    orgType = type;
    console.log("initcandidateGroups",flag,type);
    var OrgSelectModalCount = 0 ;
    $('#OrgSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#OrgSelectModal').on("shown.bs.modal",function(){
        if(++OrgSelectModalCount == 1){
            InitztData();
        }
    });
}

/**
 *
 */
function doOrgSelect(){
    var id;
    var text;
    var zTree = $.fn.zTree.getZTreeObj("tree"),
        nodes = zTree.getSelectedNodes();
    for(var i = 0; i < nodes.length; i++){
        //编号维护
        if(null == id){
            if(nodes[i].tempObject == 'DEPART'){
                id="["+nodes[i].id+",0]";
            }
            if(nodes[i].tempObject == 'POST'){
                id="["+nodes[i].id+",1]";
            }
        }else{
            if(nodes[i].tempObject == 'DEPART'){
                id=id+","+"["+nodes[i].id+",0]";
            }
            if(nodes[i].tempObject == 'POST'){
                id=id+","+"["+nodes[i].id+",1]";
            }
        }
        //名称维护
        if(null == text){
            text=nodes[i].name;
        }else{
            text=text+","+nodes[i].name;
        }
    }

    msgTishCallFnBoot('确定保存所选的数据？',function(){
        //任务中使用
        if(orgType == 1){
            $('#candidateGroups').val(id);
            $('#candidateGroups_Text').val(text);
        }
        //主流程中使用
        if(orgType == 2){
            $('#candidateStarterGroupsTemp').val(id);
            $('#candidateStarterGroupsTemp_Text').val(text);
        }
        //泳道池中使用
        if(orgType == 3){
            $('#candidateStarterGroups_').val(id);
            $('#candidateStarterGroups_Text_').val(text);
        }
        $('#OrgSelectModal').modal('hide');
    })
}

/**
 *点击节点渲染处理人，处理组等
 *type 1任务中 2泳道 3主页面
 **/
function initACC(assignee,candidateUsers,candidateGroups,type){
    console.log("type",type,"assignee",assignee,"candidateUsers",candidateUsers,"candidateGroups",candidateGroups)
    if(type == 1){
        //任务中处理人，候选人，处理组等节点操作
        if(null != assignee){
            //加载表单数据
            ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+assignee,{},function(result){
                result = eval("(" + result + ")");
                var xt_userinfo_realName = result.data.xt_userinfo_realName;
                $('#assignee_text').val(xt_userinfo_realName);
            });
        }
        if(null != candidateUsers){
            var params = {xt_userinfo_id:candidateUsers};
            //加载表单数据
            ajaxBRequestCallFn(sysModules+"/xtUserinfo/list",params,function(result){
                result = eval("(" + result + ")");
                var arr = result.items;
                var xt_userinfo_realName;
                for (var i = 0; i < arr.length; i++) {
                    //名称维护
                    if (null == xt_userinfo_realName) {
                        xt_userinfo_realName =arr[i].xt_userinfo_realName
                    } else {
                        xt_userinfo_realName = xt_userinfo_realName + "," + arr[i].xt_userinfo_realName;
                    }
                }
                $('#candidateUsers_Text').val(xt_userinfo_realName);
            });
        }
        if(null != candidateGroups){
            $('#candidateGroups_Text').val('');
            var orgArray = candidateGroups.split('],[');
            var xt_departinfo_id;
            var xt_post_id;
            for(var i = 0; i < orgArray.length; i++){
                var org = orgArray[i];
                org = org.replace("[",'');
                org = org.replace("]",'');
                var org_ = org.split(",");
                //部门
                if(org_[1] == 0){
                    if(null != xt_departinfo_id && '' != xt_departinfo_id){
                        xt_departinfo_id = xt_departinfo_id+","+org_[0];
                    }else{
                        xt_departinfo_id = org_[0];
                    }
                }
                //岗位
                if(org_[1] == 1){
                    if(null != xt_post_id && '' != xt_post_id){
                        xt_post_id = xt_post_id + "," + org_[0];
                    }else{
                        xt_post_id = org_[0];
                    }
                }
            }
            if(null != xt_departinfo_id){
                //处理部门
                var params = {xt_departinfo_id:xt_departinfo_id};
                //加载表单数据
                ajaxBRequestCallFn(sysModules+"/xtDepartinfo/list",params,function(result){
                    result = eval("(" + result + ")");
                    var arr = result.items;
                    var candidateGroups_Text;

                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_departinfo_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_departinfo_name;
                        }
                    }
                    var candidateGroups_Text_Temp = $('#candidateGroups_Text').val();
                    if(null != candidateGroups_Text_Temp && '' != candidateGroups_Text_Temp){
                        if(null != candidateGroups_Text){
                            $('#candidateGroups_Text').val(candidateGroups_Text_Temp+","+candidateGroups_Text);
                        }
                    }else{
                        $('#candidateGroups_Text').val(candidateGroups_Text);
                    }
                });
            }
            if(null != xt_post_id){
                //处理岗位
                var params = {xt_post_id:xt_post_id};
                //加载表单数据
                ajaxBRequestCallFn(sysModules+"/xtPost/list",params,function(result){
                    result = eval("(" + result + ")");
                    var arr = result.items;
                    var candidateGroups_Text;

                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_post_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_post_name;
                        }
                    }
                    var candidateGroups_Text_Temp = $('#candidateGroups_Text_Temp').val();
                    if(null != candidateGroups_Text_Temp && '' != candidateGroups_Text_Temp){
                        if(null != candidateGroups_Text){
                            $('#candidateGroups_Text').val(candidateGroups_Text_Temp+","+candidateGroups_Text);
                        }
                    }else{
                        $('#candidateGroups_Text').val(candidateGroups_Text);
                    }
                });
            }
        }
    }
    if(type == 2){
    }
    if(type == 3){
        //泳道池中流程发起人，发起人组等节点操作
        if(null != assignee){
            //加载表单数据
            ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+assignee,{},function(result){
                var xt_userinfo_realName = result.data.xt_userinfo_realName;
                $('#candidateStarterUsers_Text_').val(xt_userinfo_realName);
            });
        }
        if(null != candidateGroups){
            $('#candidateStarterGroups_Text_').val('');
            var orgArray = candidateGroups.split('],[');
            var xt_departinfo_id;
            var xt_post_id;
            for(var i = 0; i < orgArray.length; i++){
                var org = orgArray[i];
                org = org.replace("[",'');
                org = org.replace("]",'');
                var org_ = org.split(",");
                //部门
                if(org_[1] == 0){
                    if(null != xt_departinfo_id && '' != xt_departinfo_id){
                        xt_departinfo_id = xt_departinfo_id+","+org_[0];
                    }else{
                        xt_departinfo_id = org_[0];
                    }
                }
                //岗位
                if(org_[1] == 1){
                    if(null != xt_post_id && '' != xt_post_id){
                        xt_post_id = xt_post_id + "," + org_[0];
                    }else{
                        xt_post_id = org_[0];
                    }
                }
            }
            if(null != xt_departinfo_id){
                //处理部门
                var params = {xt_departinfo_id:xt_departinfo_id};
                //加载表单数据
                ajaxBRequestCallFn(sysModules+"/xtDepartinfo/list",params,function(result){
                    result = eval("(" + result + ")");
                    var arr = result.items;
                    var candidateGroups_Text;

                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_departinfo_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_departinfo_name;
                        }
                    }
                    var candidateGroups_Text_Temp = $('#candidateStarterGroups_Text_').val();
                    if(null != candidateGroups_Text_Temp && '' != candidateGroups_Text_Temp){
                        if(null != candidateGroups_Text){
                            $('#candidateStarterGroups_Text_').val(candidateGroups_Text_Temp+","+candidateGroups_Text);
                        }
                    }else{
                        $('#candidateStarterGroups_Text_').val(candidateGroups_Text);
                    }
                });
            }
            if(null != xt_post_id){
                //处理岗位
                var params = {xt_post_id:xt_post_id};
                //加载表单数据
                ajaxBRequestCallFn(sysModules+"/xtPost/list",params,function(result){
                    result = eval("(" + result + ")");
                    var arr = result.items;
                    var candidateGroups_Text;

                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_post_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_post_name;
                        }
                    }
                    var candidateGroups_Text_Temp = $('#candidateStarterGroups_Text_').val();
                    if(null != candidateGroups_Text_Temp && '' != candidateGroups_Text_Temp){
                        if(null != candidateGroups_Text){
                            $('#candidateStarterGroups_Text_').val(candidateGroups_Text_Temp+","+candidateGroups_Text);
                        }
                    }else{
                        $('#candidateStarterGroups_Text_').val(candidateGroups_Text);
                    }
                });
            }
        }
    }
}

/**
 * 清空查询条件
 * @param formid
 */
function resetAll(formid){
    if(null == formid){
        $('#searchForm')[0].reset();
    }else{
        $('#'+formid)[0].reset();
    }
    $('#assignee_').val("");
    $('#owner_').val("");
}

/**
 *  撤回
 * @param taskId
 */
function callBack(taskId) {
    msgTishCallFnBoot("确定撤回流程？",function(){
        var params = {taskId:taskId};
        ajaxBRequestCallFn(workflowModules+'/lcTask/callBack',params,function(result){
            toastrBoot(3,result.message);
            search('datatables');
        },null,"POST");
    })
}

/**
 * 格式化行为
 * @param data
 */
function formatBehavior(data) {
    if(data == 0){
        return "提交";
    }
    if(data == 10){
        return "通过";
    }
    if(data == 20){
        return "驳回";
    }
    if(data == 30){
        return "弃权";
    }
    if(data == 40){
        return "撤回";
    }
    if(data == 50){
        return "终止流程";
    }
    if(data == 60){
        return "执行跳转";
    }
    if(data == 70){
        return "转办";
    }
    if(data == 80){
        return "委派";
    }
    if(data == 90){
        return "加签";
    }
    if(data == 100){
        return "催办";
    }
    if(data == 110){
        return "设置归属人";
    }
    if(data == 120){
        return "挂起";
    }
    if(data == 130){
        return "激活";
    }
    return "缺省";
}