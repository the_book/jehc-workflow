var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcApply/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"lcApplyId",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"lcApplyId",
				width:"50px"
			},
			{
				data:'title'
			},
			{
				data:'modelKey'
			},
			{
				data:'create_time',
				render:function(data, type, row, meta) {
					return dateformat(data); 
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
	
	//实现单击操作开始
	var table = $('#datatables').DataTable();
    $('#datatables tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        showLcAppyDetail(data);
    });
    //实现单击操作结束

});
/**删除操作**/
function delLcApply(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {lc_apply_id:lc_apply_id};
		ajaxBReq(workflowModules+'/lcApply/delete',params,['datatables']);
	})
}
var grid_;
function showLcAppyDetail(data){
	var procInstId = data.procInstId;
	var lcApplyId = data.lcApplyId;
    var imagePathUrl = workflowModules+'/lcProcess/view/base64/image/instance/'+procInstId;
    ajaxBRequestCallFn(imagePathUrl,{},function(result){
        if(undefined == result.data || null == result.data || '' == result.data){
            return;
        }
        $('#imagePath').attr("src", result.data.image);
    });

	initLcApprovalWin(procInstId);
	$('#lcApplyDetailPanelBody').height(reGetBodyHeight()-216);
	$('#lcApplyDetailModal').modal({backdrop:'static',keyboard:false});
	// $("#lcFormIframe",document.body).attr("src",url)
	$('#lcApplyDetailModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){  
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcApplyDetailModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        //解决tab选项卡打开后datatables
        $.fn.dataTable
            .tables( { visible: true, api: true } )
            .columns.adjust();
    });
	$('#title').val(data.title);
    $('#modelKey').val(data.modelKey);
	$('#create_time').val(dateformat(data.create_time));
	$('#remark').val(data.remark);
}

function initLcApprovalWin(procInstId){
	$('#searchFormApproval')[0].reset();
	$('#instanceId').val(procInstId);
	var opt = {
			searchformId:'searchFormApproval'
		};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcApproval/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(0)', nRow).html(iDisplayIndex +1);
				return nRow;
		},
		tableHeight:'200px',
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        //表头固定
        fixedHeader:true,
        fixedColumns: {
            "leftColumns": 2,
            "rightColumns": 1
        },
		//列表表头字段
		colums:[
            {
                data:"lcApprovalId",
                width:"50px"
            },
            {
                data:'behavior',
                width:"100px",
                render: function (data, type, full, meta) {
                    return formatBehavior(data)
                }
            },
            {
                data:'comment',
                width:"150px"
            },
            {
                data:'create_time',
                width:"150px"
            },
            {
                data:'createBy',
                width:"50px"
            }
		]
	});
	grid_=$('#approvalDatatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkallApproval','checkchildApproval');
	//实现单击行选中
	clickrowselected('approvalDatatables');
}

/**
 * 格式化行为
 * @param data
 */
function formatBehavior(data) {
    if(data == 0){
        return "提交";
    }
    if(data == 10){
        return "通过";
    }
    if(data == 20){
        return "驳回";
    }
    if(data == 30){
        return "弃权";
    }
    if(data == 40){
        return "撤回";
    }
    if(data == 50){
        return "终止流程";
    }
    if(data == 60){
        return "执行跳转";
    }
    if(data == 70){
        return "转办";
    }
    if(data == 80){
        return "委派";
    }
    if(data == 90){
        return "加签";
    }
    if(data == 100){
        return "催办";
    }
    if(data == 110){
        return "设置归属人";
    }
    if(data == 120){
        return "挂起";
    }
    if(data == 130){
        return "激活";
    }
    return "缺省";
}

$('#myTab a').click(function (e) {
    e.preventDefault()
    $(this).tab('show');
    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
})

// $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
//     $.fn.dataTable
//         .tables( { visible: true, api: true } )
//         .columns.adjust();
// })