//返回
function goback(){
    tlocation(base_html_redirect+'/workflow/lc-group/lc-group-list.html');
}



//保存
function updateLcGroup(){
    submitBForm('defaultForm',workflowModules+'/lcGroup/update',base_html_redirect+'/workflow/lc-group/lc-group-list.html',null,"PUT");
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

$(document).ready(function(){
    datetimeInit();
    var lc_group_id = GetQueryString("lc_group_id");
    //加载表单数据
    ajaxBRequestCallFn(workflowModules+'/lcGroup/get/'+lc_group_id,{},function(result){
        $('#lc_product_id').val(result.data.lc_product_id);
        $('#name').val(result.data.name);
        $('#lc_group_id').val(result.data.lc_group_id);
        initProductListSetV("lc_product_id",result.data.lc_product_id);
    });
});


function initProductListSetV(id,value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"POST",
        url:workflowModules+"/lcProduct/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.lc_product_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
            try {
                if(null != value_id && '' != value_id){
                    $('#'+id).val(value_id);
                }
            } catch (e) {
                console.log("异常信息："+e);
            }
        }
    });
}