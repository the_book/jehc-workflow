
//返回
function goback(){
    tlocation(base_html_redirect+'/workflow/lc-group/lc-group-list.html');
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

//初始化日期选择器
$(document).ready(function(){
    datetimeInit();
    initProductList('lc_product_id');
});

/**
 * 保存
 */
function addLcGroup() {
    submitBForm('defaultForm',workflowModules+'/lcGroup/add',base_html_redirect+'/workflow/lc-group/lc-group-list.html');

}




function initProductList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"POST",
        url:workflowModules+"/lcProduct/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.lc_product_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
        }
    });
}
