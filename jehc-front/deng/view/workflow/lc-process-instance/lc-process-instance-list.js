var grid;
$(document).ready(function() {
    var lc_process_id = GetQueryString("lc_process_id");
    $("#lc_process_id").val(lc_process_id);
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcDeploymentHis/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(0)', nRow).html(iDisplayIndex +1);
            jQuery('td:eq(1)', nRow).html("<span class='row-details fa fa-caret-right' data_id='"+aData.lc_deployment_his_id+"' id='"+aData.id+"'></span> ");
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:reGetBodyHeight()*0.55+'px',
        /*fixedHeader:true,//表头固定
        fixedColumns: {
            "leftColumns": 3
        },*/
        //列表表头字段
        colums:[
            {
                data:"id",
                width:"50px"
            },
            {
                data:"id",
                width:"50px"
            },
            {
                data:"lc_deployment_his_id",
                width:"100px"
            },
            {
                data:'lc_deployment_his_name'/*,
                render:function(data, type, row, meta) {
                    return "<font color='#6495ed'>"+data+"</font>";
                }*/
            },
            {
                data:'lc_deployment_his_time',
                render:function(data, type, row, meta) {
                    return dateformat(data);
                    /*return "<font color='#deb887'>"+dateformat(data)+"</font>";*/
                }
            },
            {
                data:'version',
                render:function(data, type, row, meta) {
                    return "<span class='label label-light-warning mr-2'>"+data+"</span>"
                }
            },
            {
                data:'lc_deployment_his_tenantId'
            },
            {
                data:'lc_deployment_his_status',
                render:function(data, type, row, meta) {
                    if(data == 1){
                        return "<span class='label label-lg font-weight-bold label-light-danger label-inline'>已关闭</span>"
                    }
                    if(data == 0){
                        return "<span class='label label-lg font-weight-bold label-light-success label-inline'>发布中</span>"
                    }
                    return "<span class='label label-lg font-weight-bold label-light-info label-inline'>缺省</span>"
                }
            },
            {
                data:'lc_process_flag',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='label label-lg font-weight-bold label-light-primary label-inline'>通过平台设计器设计</span>"
                    }
                    if(data == 1){
                        return "<span class='label label-lg font-weight-bold label-light-info label-inline'>通过上传部署</span>"
                    }
                    return "<span class='label label-lg font-weight-boldlabel-light-success label-inline'>缺省</span>"
                }
            },
            {
                data:"name",
                width:"150px"
            },
            {
                data:"groupName",
                width:"150px"
            },
            {
                data:"id",
                render:function(data, type, row, meta) {
                    var lc_deployment_his_id = row.lc_deployment_his_id;
                    return '<button onclick=toLcDeploymentDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="版本详情">' +
                        '<i class="la la-eye"></i>' +
                        '</button>';
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);

    $('.table').on('click', ' tbody td .row-details', function () {
        var nTr = $(this).parents('tr')[0];
        try{
            if ( grid.fnIsOpen(nTr) )//判断是否已打开
            {
                /* This row is already open - close it */
                $(this).addClass("fa-caret-right").removeClass("fa-caret-down");
                grid.fnClose( nTr );
            }else{
                /* Open this row */
                $(this).addClass("fa-caret-down").removeClass("fa-caret-right");
                // 调用方法显示详细信息 data_id为自定义属性 存放配置ID
                fnFormatDetails(nTr,$(this).attr("data_id"));
            }
        }catch (e){
            /* Open this row */
            $(this).addClass("fa-caret-down").removeClass("fa-caret-right");
            // 调用方法显示详细信息 data_id为自定义属性 存放配置ID
            fnFormatDetails(nTr,$(this).attr("data_id"));
        }
    });
});

/**
 *
 * @param nTr
 * @param pdataId
 */
function fnFormatDetails(nTr,pdataId){
    //根据配置Id 异步查询数据
    var sOut = '<div style=\'text-align: center;border:1px; \'><div style=\'width: 70%;margin: auto;\'><form class="m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed " method="POST" id="searchForm-'+pdataId+'">'
    sOut += '<div class="form-group m-form__group row">';
    sOut += '<label class="col-form-label">运行状态:</label>';
    sOut += '<div class="col-lg-2">';
    sOut += '<select class="form-control" name="instanceType" onchange=init("'+pdataId+'")><option value="1">运行中</option><option value="">已完成</option></select>';
    sOut += '<input type="hidden" class="form-control" name="deployment_id" value="'+pdataId+'">';
    sOut += '</div>';
    /*sOut += '<label class="col-form-label">活动节点:</label>';
    sOut += '<div class="col-lg-3">';
    sOut += '<select class="form-control" name="nodeType" onchange=init("'+pdataId+'")><option value="1">只显示任务</option><option value="">全部</option></select>';
    sOut += '</div>';*/
    sOut += '<div class="col-lg-1">';
    sOut += '<a href="javascript:search(\'datatables-'+pdataId+'\')" title="刷新" class="btn btn-secondary m-btn m-btn--custom m-btn--label-warning m-btn--bolder m-btn--uppercase" ><span><i class="fa fa-spin fa-refresh m-r-5"></i><span>刷新</span></span></a>';
    sOut += '</div>';
    sOut += '</div>';
    sOut += '</form>';
    sOut += "<table class='table table-bordered table-striped table-hover' style='width: 70%;white-space: nowrap; ' id='datatables-"+pdataId+"'><thead>";
    sOut+='<tr style="background:#e1f0ff;">';
    sOut+='<th style="background: #e1f0ff"><div class="div_left">序号</div></th>';
    sOut+='<th style="background: #e1f0ff"><div class="div_left">流程名称</div></th>';
    sOut+='<th style="background: #e1f0ff"><div class="div_left">业务编号</div></th>';
    sOut+='<th style="background: #e1f0ff"><div class="div_left">流程实例</div></th>';
    /*
        sOut+='<th style="background: #e1f0ff"><div class="div_left">当前任务</div></th>';
    */
    sOut+='<th style="background: #e1f0ff"><div class="div_left">开始时间</div></th>';
    sOut+='<th style="background: #e1f0ff"><div class="div_left">结束时间</div></th>';
    sOut+='<th style="background: #e1f0ff"><div class="div_left">耗时</div></th>';
    sOut+='<th style="background: #e1f0ff"><div class="div_left">状态</div></th>';
    sOut+='<th style="background: #e1f0ff"><div class="div_left">操作</div></th>';
    sOut+='</tr>'
    sOut+='</thead></table></div></div>';
    grid.fnOpen( nTr,sOut, 'details' );
    init(pdataId);
}

function init(pdataId) {
    var subGgrid;
    var opt = {
        searchformId:'searchForm-'+pdataId
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcInstance/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            //在第一位置追加序列号
            jQuery('td:eq(0)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:'220px',
        //表头固定
        /*fixedHeader:true,
        fixedColumns: {
            "leftColumns": 2,
            "rightColumns": 1
        },*/
        //列表表头字段
        colums:[
            {
                data:"id",
                width:"50px"
            },
            {
                data:"processDefinitionName",
                width:"100px"
            },
            {
                data:"businessKey",
                width:"100px"
            },
            {
                data:"processInstanceId",
                width:"100px"
            },
            /*{
                data:"taskName",
                width:"100px"
            },*/
            {
                data:"startTime",
                width:"100px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"endTime",
                width:"100px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            },
            {
                data:"durationInMillis",
                width:"100px",
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>"+formateDhms(data)+"</span>"
                }
            },
            {
                data:"suspended",
                width:"100px",
                render:function(data, type, row, meta) {
                    if(data == false){
                        return "<span class='label label-lg font-weight-bold label-light-success label-inline'>激活</span>"
                    }else{
                        return "<span class='label label-lg font-weight-bold label-light-danger label-inline'>挂起</span>"
                    }
                }
            },
            {
                data:"id",
                width:"100px",
                render:function(data, type, row, meta) {
                    var processDefinitionName = row.processDefinitionName;
                    var end = row.end;
                    var btn = '<button onclick=showLcProcessInstance("'+processDefinitionName+'","'+data+'","'+pdataId+'") class="btn btn-light-danger font-weight-bold mr-2" title="流程监控">' +
                        '<i class="la la-image"></i>监 控' +
                        '</button>';
                    btn = btn +'<button class="btn btn-light-primary font-weight-bold mr-2" title="审批记录" onclick=initLcApprovalWin("'+processDefinitionName+'","'+data+'","'+pdataId+'")><i class="fa flaticon-graphic-1"></i>审批记录</button>';
                    btn = btn +'<button class="btn btn-light-dark font-weight-bold mr-2" title="历史" onclick=initLcAcitiviWin("'+processDefinitionName+'","'+data+'","'+pdataId+'")><i class="la la-eye"></i>历史</button>';
                    if(!end){
                        /*btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="跳转" onclick=initLcTaskJumpWin('+data+')><i class="fa fa-history fa-lg"></i></button>';
                        btn += '<button onclick=suspend("'+processDefinitionName+'","'+data+'","'+pdataId+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="挂起">' +
                            '<i class="la la-power-off"></i>' +
                            '</button>';
                        btn = btn+'<button onclick=activate("'+processDefinitionName+'","'+data+'","'+pdataId+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="激活">' +
                            '<i class="la la-caret-right"></i>' +
                            '</button>';*/
                        btn = btn+ '<button onclick=showLcProcessInstanceBase64("'+processDefinitionName+'","'+data+'","'+pdataId+'") class="btn btn-light-warning font-weight-bold mr-2" title="活动图">' +
                            '<i class="la la-image"></i>流程图' +
                            '</button>';
                        btn = btn+ '<div class="btn-group">' +
                                    '<a href="#" class="btn btn-light-success font-weight-bold" data-toggle="dropdown" aria-expanded="true">' +
                                    '<i class="la la-bars"></i>操作' +
                                    '</a>' +
                                    '<button type="button" class="btn btn-light-success font-weight-bold dropdown-toggle dropdown-toggle-split mr-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                    '<span class="sr-only"></span>'+
                                    '</button>'+
                                    '<div class="dropdown-menu">' +
                                        '<button class="dropdown-item" onclick=initLcTaskJumpWin("'+processDefinitionName+'","'+data+'","'+pdataId+'")><i class="la la-gavel"></i>跳转</a>' +
                                        '<button class="dropdown-item" onclick=suspend("'+processDefinitionName+'","'+data+'","'+pdataId+'")><i class="la la-rotate-right"></i>挂起</a>' +
                                        '<button class="dropdown-item" onclick=activate("'+processDefinitionName+'","'+data+'","'+pdataId+'")><i class="la la-caret-right"></i>激活</a>' +
                                        '<button class="dropdown-item" onclick=endProcess("'+processDefinitionName+'","'+data+'","'+pdataId+'")><i class="la la-power-off"></i>终止流程</a>' +
                                    '</div>' +
                                '</div>';
                        return btn;
                    }else{
                        return btn;
                    }
                }
            }
        ]
    });
    $('#datatables-'+pdataId).dataTable(options);
}

/**
 * 激活
 * @param name
 * @param id
 * @param pdataId
 */
function activate(name,id,pdataId) {
    msgTishCallFnBoot("确定要激活该流程实例："+name+"？",function(){
        var params = {_method:'GET'};
        ajaxBReq(workflowModules+'/lcInstance/activate/'+id,params,['datatables-'+pdataId],null,"GET");
    })
}

/**
 * 挂起
 * @param name
 * @param id
 * @param pdataId
 */
function suspend(name,id,pdataId) {
    msgTishCallFnBoot("确定要挂起该流程实例："+name+"？",function(){
        var params = {_method:'GET'};
        ajaxBReq(workflowModules+'/lcInstance/suspend/'+id,params,['datatables-'+pdataId],null,"GET");
    })
}

/**
 *
 * @param processDefinitionName
 * @param id
 * @param pdataId
 */
function showLcProcessInstance(processDefinitionName,id,pdataId){
    $('#lcImagePanelBody').height(reGetBodyHeight()-128);
    $('#lcImageModalLabel').html("实例监控图");
    $('#lcImageModal').modal({backdrop:'static',keyboard:false});
    $("#lcImageIframe",document.body).attr("src",base_html_redirect+"/workflow/lc-image/image.html?id="+id);
    $('#lcImageModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcImageModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
    });
}

/**
 *
 */
function closeLcImageWin(){
}

/**
 *
 * @param timestamp
 * @returns {string}
 */
function formateDhms(timestamp) {

    if (timestamp) {
        timestamp = timestamp/1000;
        var result = '';
        if (timestamp >= 86400) {
            var days = Math.floor(timestamp / 86400);
            timestamp = timestamp % 86400;
            result = days  + ' 天';
            if (timestamp > 0) {
                result += ' ';
            }
        }
        if (timestamp >= 3600) {
            var hours = Math.floor(timestamp / 3600);
            timestamp = timestamp % 3600;
            result += hours + ' 小时';
            if (timestamp > 0) {
                result += ' ';
            }
        }
        if (timestamp >= 60) {
            var minutes = Math.floor(timestamp / 60);
            timestamp = timestamp % 60;
            result += minutes + ' 分钟';
            if (timestamp > 0) {
                result += ' ';
            }
        }
        result += parseInt(timestamp) + ' 秒';
        return result;
    }
    return "";

}

/**
 *
 * @param name
 * @param id
 * @param pdataId
 */
function initLcApprovalWin(name,id,pdataId){
    var approvalCount = 0 ;
    $('#lcHisLogPanelBody').height(reGetBodyHeight()-128);
    $('#lcHisLogModal').modal({backdrop:'static',keyboard:false});
    $('#lcHisLogModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcHisLogModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.6+'px'});
        if(++approvalCount == 1) {
            $('#searchFormApproval')[0].reset();
            $('#instanceId').val(id);
            var opt = {
                searchformId: 'searchFormApproval'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax: function (data, callback, settings) {
                    datatablesCallBack(data, callback, settings, workflowModules + '/lcApproval/list', opt);
                },//渲染数据
                //在第一位置追加序列号
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex + 1);
                    return nRow;
                },
                order: [],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight: '300px',
                //列表表头字段
                colums: [
                    {
                        sClass: "text-center",
                        width: "50px",
                        data: "lcApprovalId",
                        render: function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildApproval" value="' + data + '" /><span></span></label>';
                        },
                        bSortable: false
                    },
                    {
                        data: "lcApprovalId",
                        width: "50px"
                    },
                    {
                        data: 'behavior',
                        render: function (data, type, full, meta) {
                            return formatBehavior(data)
                        }
                    },
                    {
                        data: 'comment'
                    },
                    {
                        data: 'create_time'
                    },
                    {
                        data: 'createBy'
                    }
                ]
            });
            grid = $('#approvalDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallApproval', 'checkchildApproval');
            //实现单击行选中
            // clickrowselected('approvalDatatables');
        }
    });
}

/**
 *
 * @param processDefinitionName
 * @param id
 * @param pdataId
 */
function initLcAcitiviWin(processDefinitionName,id,pdataId) {
    $("#instanceId").val(id);
    // $('#lcActivitiPanelBody').height(reGetBodyHeight()*0.6);
    $('#lcActivitiModal').modal({backdrop:'static',keyboard:false});
    $('#lcActivitiModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcActivitiModalDialog");
        $modal_dialog.css({'width':reGetBodyWidth()*0.5+'px'});
        $('#searchFormLcActiviti')[0].reset();
        $('#instanceId_').val(id);
        var opt = {
            searchformId:'searchFormLcActiviti'
        };
        var options = DataTablesPaging.pagingOptions({
            ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcInstance/list/activity',opt);},//渲染数据
            //在第一位置追加序列号
            fnRowCallback:function(nRow, aData, iDisplayIndex){
                jQuery('td:eq(0)', nRow).html(iDisplayIndex +1);
                return nRow;
            },
            order:[],//取消默认排序查询,否则复选框一列会出现小箭头
            tableHeight:'150px',
            //列表表头字段
            colums:[
                {
                    data:"id",
                    width:"50px"
                },
                {
                    data:'activityName'
                },
                {
                    data:'startTime',
                    render:function (data, type, full, meta) {
                        return dateformat(data)
                    }
                },
                {
                    data:'endTime',
                    render:function (data, type, full, meta) {
                        return dateformat(data)
                    }
                },
                {
                    data:'durationInMillis',
                    render:function(data, type, row, meta) {
                        return "<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>"+formateDhms(data)+"</span>"
                    }
                },
                {
                    data:'assignee'
                },
                {
                    data:'activityType'
                }
            ]
        });
        $('#lcActivitiDatatables').dataTable(options);
        //实现单击行选中
        clickrowselected('lcActivitiDatatables');
    });
}

/**
 * 跳转节点
 * @param id
 */
function initLcTaskJumpWin(processDefinitionName,id,pdataId) {
    console.log(processDefinitionName,id,pdataId);
    var SourcesModalCount = 0 ;
    $('#lcTaskJumpPanelBody').height(100);
    $('#lcTaskJumpModal').modal({backdrop:'static',keyboard:false});
    $('#lcTaskJumpModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++SourcesModalCount == 1) {
            // 是弹出框居中。。。
            var $modal_dialog = $("#lcTaskJumpModalDialog");
            $modal_dialog.css({'width':'500px'});
            $('#lcTaskJumpForm')[0].reset();
            $('#processInstanceId').val(id);
            $('#pdataId').val(pdataId);
            initTaskList("activityId",id);
        }
    });
}

/**
 *
 * @param activityId
 * @param id
 */
function initTaskList(activityId,id){
    $("#"+activityId).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:workflowModules+"/lcTask/userTask/list/"+id,
        success: function(result){
            //从服务器获取数据进行绑定
            result = result.data;
            $.each(result, function(i, item){
                str += "<option value=" + item.taskId + ">" + item.name + "</option>";
            })
            $("#"+activityId).append(str);
        }
    });
}

/**
 * 执行任意节点跳转
 */
function excuteCurrentTaskJumpCmd() {
    msgTishCallFnBoot("确定执行跳转？",function(){
        var params = {processInstanceId:$("#processInstanceId").val(),activityId:$('#activityId').val()};
        ajaxBRequestCallFn(workflowModules+'/lcTask/excuteCurrentTaskJumpCmd',params,function(){
            toastrBoot(3,"执行跳转节点成功");
            $('#lcTaskJumpModal').modal('hide');
            search('datatables-'+$('#pdataId').val());
        },null,"POST");
    })
}

/**
 * 终止流程
 * @param taskId
 */
function endProcess(processDefinitionName,id,pdataId) {
    msgTishCallFnBoot("确定终止流程？",function(){
        var params = {processInstanceId:id};
        ajaxBRequestCallFn(workflowModules+'/lcTask/termination',params,function(){
            toastrBoot(3,"执行终止流程成功");
            search('datatables-'+pdataId);
        },null,"POST");
    })
}

/**
 * 部署详情
 * @param id
 */
function toLcDeploymentDetail(id){
    $('#lcDeploymentDetailBody').height(reGetBodyHeight()*0.5);
    $('#lcDeploymentDetailModal').modal({backdrop:'static',keyboard:false});
    $('#lcDeploymentDetailModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcDeploymentDetailModalDialog");
        $modal_dialog.css({'width':reGetBodyWidth()*0.5+'px'});
        //加载表单数据
        ajaxBRequestCallFn(workflowModules+'/lcDeploymentHis/get/'+id,{},function(result){
            $('#lc_deployment_his_id').val(result.data.lc_deployment_his_id);
            $('#lc_deployment_his_name').val(result.data.lc_deployment_his_name);
            $('#lc_deployment_his_time').val(dateformat(result.data.lc_deployment_his_time));
            $('#lc_deployment_his_tenantId').val(result.data.lc_deployment_his_tenantId);
            $('#lc_deployment_his_status').html(result.data.lc_deployment_his_status==0?"<font color='#008b8b'>【运行中】</font>":"<font color='#8b0000'>【关闭】</font>");
            $('#uid').val(result.data.uid);
            $('#uk').val(result.data.uk);
            var lc_process_flag = result.data.lc_process_flag;
            if(lc_process_flag == 0){
                $('#lc_process_flag').val("通过平台设计器设计");
            }
            if(lc_process_flag == 1){
                $('#lc_process_flag').val("通过上传部署");
            }
            $('#name').val(result.data.name);
            $('#groupName').val(result.data.groupName);
            $('#w').val(result.data.w);
            $('#h').val(result.data.h);
        });
    });
}

/**
 *
 * @param processDefinitionName
 * @param processInstanceId
 * @param pdataId
 */
function showLcProcessInstanceBase64(processDefinitionName,processInstanceId,pdataId) {
    $('#lcProcessInstanceBase64PanelBody').height(reGetBodyHeight()-128);
    $('#lcProcessInstanceBase64Modal').modal({backdrop:'static',keyboard:false});
    var base64Count = 0 ;
    $('#lcProcessInstanceBase64Modal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcProcessInstanceBase64ModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        //加载表单数据
        if(++base64Count == 1){
            ajaxBRequestCallFn(workflowModules+'/lcProcess/view/base64/image/instance/'+processInstanceId,{},function(result){
                $('#imagePath').attr("src", result.data.image);
            });
        }
    });
}

/**
 * 格式化行为
 * @param data
 */
function formatBehavior(data) {
    if(data == 0){
        return "提交";
    }
    if(data == 10){
        return "通过";
    }
    if(data == 20){
        return "驳回";
    }
    if(data == 30){
        return "弃权";
    }
    if(data == 40){
        return "撤回";
    }
    if(data == 50){
        return "终止流程";
    }
    if(data == 60){
        return "执行跳转";
    }
    if(data == 70){
        return "转办";
    }
    if(data == 80){
        return "委派";
    }
    if(data == 90){
        return "加签";
    }
    if(data == 100){
        return "催办";
    }
    if(data == 110){
        return "设置归属人";
    }
    if(data == 120){
        return "挂起";
    }
    if(data == 130){
        return "激活";
    }
    return "缺省";
}
/**
 * 催办
 * @param taskId
 */
function urge(taskId) {
    msgTishCallFnBoot("确定催办该任务？",function(){
        var params = {taskId:taskId,behavior:100};
        ajaxBRequestCallFn(workflowModules+'/lcTask/approve',params,function(){
            toastrBoot(3,"催办任务成功");
            search('datatables');
        },null,"POST");
    })
}