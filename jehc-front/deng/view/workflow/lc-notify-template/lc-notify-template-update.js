//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-notify-template/lc-notify-template-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateLcNotifyTemplate(){
	submitBForm('defaultForm',workflowModules+'/lcNotifyTemplate/update',base_html_redirect+'/workflow/lc-notify-template/lc-notify-template-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcNotifyTemplate/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#title').val(result.data.title);
		$('#template_key').val(result.data.template_key);
		$('#type').val(result.data.type);
        $('#content').val(result.data.content);
	});
});
