//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-notify-template/lc-notify-template-list.html');
}
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcNotifyTemplate/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#title').val(result.data.title);
        $('#create_id').val(result.data.createBy);
        $('#content').val(result.data.content);
        $('#update_id').val(result.data.modifiedBy);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
		$('#template_key').val(result.data.template_key);
		$('#type').val(result.data.type);

	});
});
