
/**
 * 初始化节点属性
 * @param _hid 流程发布记录id
 * @param lc_process_title 流程名称
 * @param nodeId 节点id
 * @param nodeName 节点名称
 * @param lc_process_id 流程id
 * @param mounts_id 挂载编号
 * @param mutil 是否会签节点
 */
function initNodeAttribute(hid_,lc_process_title,nodeId,nodeName,lc_process_id,mounts_id,mutil) {
    $('[data-toggle="popover"]').popover();
    $('#lcNodeAttrform').bootstrapValidator({
        message:'此值不是有效的'
    });
    var nodeAttributeSelectModalCount = 0 ;
    $('#nodeAttributePanelBody').height(reGetBodyHeight()-218);
    $('#nodeAttributeModalLabel').html("流程节点：["+lc_process_title+"]-[<font color=red>"+nodeName+"</font>]");
    $('#nodeAttributeModal').modal({backdrop: 'static', keyboard: false});
    $('#nodeAttributeModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#nodeAttributeModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++nodeAttributeSelectModalCount == 1){
            $('#TabCol a:first').tab('show');//初始化显示哪个tab
            $('#TabCol a').click(function (e) {
                e.preventDefault();//阻止a链接的跳转行为
                $(this).tab('show');//显示当前选中的链接及关联的content
            })

            $('#lcNodeAttrform')[0].reset();
            initNotice();
            initNotifyTemplateIds();//初始化通知模板组件

            $("#jumpRuleTable").empty();
            resetJumprule();
            $("#tbody-uuid").empty();
            $("#node_form_field").empty();
            $("#lc_process_id").val(lc_process_id);

            $("#nodeId").val(nodeId);
            $("#nodeIdTemp").val(nodeId);

            if(null == hid_ || "" == hid_ || 'null' == hid_){
                $("#hid_").val("");
            }else{
                $("#hid_").val(hid_);
            }

            if(null == mounts_id || "" == mounts_id || 'null' == mounts_id){
                $("#mounts_id").val("");
            }else{
                $("#mounts_id").val(mounts_id);
            }
            $("#lc_node_attribute_id").val("");
            var param = {hid_:$("#hid_").val(),nodeId:nodeId,mounts_id:$("#mounts_id").val()};
            ajaxBRequestCallFn(workflowModules+'/lcNodeAttribute/attribute',param,function(result){
                if(null != result.data){
                    $('#lc_node_attribute_id').val(result.data.lc_node_attribute_id);
                    $('#attributeKey').val(result.data.attributeKey);
                    $('#nodeAttributeRemark').val(result.data.remark);
                    $('#notice_type').val(result.data.notice_type);
                    $('#pcFormUri').val(result.data.pcFormUri);
                    $('#mobileFormUri').val(result.data.mobileFormUri);
                    $('#initiator').val(result.data.initiator);
                    $('#attr').val(result.data.attr);
                    $('#orderNumber').val(result.data.orderNumber);
                    $('#urgent').val(result.data.urgent);
                    $('#notify_template_ids').val(result.data.notify_template_ids);

                    $('#opinion').val(result.data.opinion);
                    $('#allowRecall').val(result.data.allowRecall);
                    $('#recall').val(result.data.recall);
                    $('#rejectType').val(result.data.rejectType);
                    $('#rejectRule').val(result.data.rejectRule);
                    $('#supportFreeJump').val(result.data.supportFreeJump);
                    $('#rejectExecutor').val(result.data.rejectExecutor);
                    $('#rejectTerminationOtherExecution').val(result.data.rejectTerminationOtherExecution);
                    $('#mutil').val(result.data.mutil);
                    $('#mutilKey').val(result.data.mutilKey);
                    $('#mounts_id').val(result.data.mounts_id);
                    initMutil(result);//会签
                    initNotice();
                    setNotifyTemplateIds();//给通知模板组件赋值
                    if(undefined != result.data.lcJumpRules && null != result.data.lcJumpRules && '' != result.data.lcJumpRules){
                        var lcJumpRules = result.data.lcJumpRules;
                        $.each(lcJumpRules, function(i, item){
                            createJumpRuleTable(item.target_node_id,item.node_name,item.jump_rules_id,item.based_on_condition,item.conditions_);
                        })
                    }

                    initBtn(result);//初始化按钮

                    if(undefined != result.data.nodeFormField && null != result.data.nodeFormField && '' != result.data.nodeFormField){
                        var nodeFormField = result.data.nodeFormField;
                        $.each(nodeFormField, function(i, item){
                            addFormFieldRow(item.field_label,item.field_id,item.field_name,item.field_read,item.field_write,item.filed_hide,item.filed_required)
                        })
                    }

                    if(undefined != result.data.lcNodeCandidate && null != result.data.lcNodeCandidate && '' != result.data.lcNodeCandidate){
                        var lcNodeCandidate = result.data.lcNodeCandidate;
                        //处理人
                        $('#assignee').val(lcNodeCandidate[0].assignee_id);
                        $('#assignee_type').val(lcNodeCandidate[0].assignee_type);
                        $('#remark0').val(lcNodeCandidate[0].remark);
                        //候选人
                        $('#candidateUsers').val(lcNodeCandidate[0].candidate_id);
                        $('#remark1').val(lcNodeCandidate[0].remark1);
                        //指定组
                        $('#candidateGroups').val(lcNodeCandidate[0].candidate_group_id);
                        $('#candidate_group_type').val(lcNodeCandidate[0].candidate_group_type);
                        $('#remark2').val(lcNodeCandidate[0].remark2);

                        //初始化办理人
                        initACC(lcNodeCandidate[0].assignee_id,lcNodeCandidate[0].candidate_id,lcNodeCandidate[0].candidate_group_id,1);
                    }
                }else{
                    $('#nodeAttributeModalLabel').html("流程节点：["+lc_process_title+"]-[<font color=red>"+nodeName+"</font>]-<font color=#3699ff>暂无规则</font>");
                }
                initMutilHtml(mutil);//会签是否展示
            });
            initNodeImage();//初始化流程图
        }
    });
}

/**
 * 保存按钮
 */
function saveLcNodeAttribute() {
    saveNotice();
    saveNotifyTemplateIds();
    // var bootform =  $('#lcNodeAttrform');
    // console.log(JSON.stringify(bootform.serializeJSON()))
    submitBFormCallFn('lcNodeAttrform',workflowModules+'/lcNodeAttribute/save',function(result){
        try {
            if(typeof(result.success) != "undefined"){
                if(result.success){
                    window.parent.toastrBoot(3,result.message);
                    $('#nodeAttributeModal').modal('hide');
                }else{
                    window.parent.toastrBoot(4,result.message);
                }
            }
            var treeNode = $('#processTree').jstree(true).get_selected(true); //获取所有选中的节点对象
            treeNode = treeNode[0];
            var domId = treeNode.id;
            var type = treeNode.original.tempObject;
            var text = treeNode.text;
            if(type == "C"){
                initNodeDatables(domId,text,2);
            }
            if(type == "P"){
                initNodeDatables(domId,"",1);
            }
        } catch (e) {

        }
    },null,"POST");
}


function initNotice(){
    $("#notice_types").select2({
        width: "100%", //设置下拉框的宽度
        placeholder: "请选择", // 空值提示内容，选项值为 null
        tags:true,
        /*allowClear:!0,*/
        createTag:function (decorated, params) {
            return null;
        }
    });
    var notice_type = $("#notice_type").val();
    if(null != notice_type && "" != notice_type){
        var notice_types = notice_type.split(",");//注意：arr为select的id值组成的数组
        $('#notice_types').val(notice_types).trigger('change');
    }
}


function saveNotice() {
    var notice_types = $('#notice_types').val();
    var ids = null;
    if(null != notice_types){
        for(var i = 0; i < notice_types.length; i++){
            if(ids == null){
                ids = notice_types[i];
            }else{
                ids =ids+ ","+notice_types[i];
            }
        }
    }
    $('#notice_type').val(ids);
}



function initProductList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"POST",
        url:workflowModules+"/lcProduct/find",
        success: function(result){
            result = result.data;
            if(undefined != result){
                //从服务器获取数据进行绑定
                $.each(result, function(i, item){
                    str += "<option value=" + item.lc_product_id + ">" + item.name + "</option>";
                })
                $("#"+id).append(str);
            }
        }
    });
}


/**
 *
 * @param id
 * @param value_id
 * @param lc_product_id
 */
function initGroupListSetV(id,value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    if(null != $("#"+value_id).val() && '' != $("#"+value_id).val()){
        $.ajax({
            type:"GET",
            url:workflowModules+"/lcGroup/find/"+$("#"+value_id).val(),
            success: function(result){
                result = result.data;
                if(undefined != result){
                    //从服务器获取数据进行绑定
                    $.each(result, function(i, item){
                        str += "<option value=" + item.lc_group_id + ">" + item.name + "</option>";
                    })
                    $("#"+id).append(str);
                }
            }
        });
    }else{
        $("#"+id).append(str);
    }
}


/**
 * 初始化主流程信息
 * @param id
 */
function initProcess(id){
    var jcProcessModalCount = 0 ;
    $('#jcProcessPanelBody').height(reGetBodyHeight()*0.6);
    $('#jcProcessModal').modal({backdrop:'static',keyboard:false});
    $('#jcProcessModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++jcProcessModalCount == 1) {
            //加载表单数据
            ajaxBRequestCallFn(workflowModules+'/lcProcess/get/'+id,{},function(result){
                $('#titleTemp').val(result.data.lc_process_title);
                $('#moduleKey').val(result.data.moduleKey);
                $('#lc_product_idTemp').val(result.data.lc_product_id);
                $('#lc_group_idTemp').val(result.data.lc_group_id);
                $('#lc_product_id_').val(result.data.lc_product_id);
                $('#lc_group_id_').val(result.data.lc_group_id);
                //产品线下拉框数据
                initProductList("lc_product_id_");
                initGroupListSetV("lc_group_id_","lc_product_idTemp","lc_group_idTemp")
            });
        }
    });
}

/**
 * 初始化节点流程图
 */
function initNodeImage() {
    var type;
    var hid_ = $("#hid_").val();
    var mounts_id = $("#mounts_id").val();
    var id = "";
    if(null == hid_ || "" == hid_ || 'null' == hid_){

    }else{
        type = 2;
        id = hid_;
    }
    if(null == mounts_id || "" == mounts_id || 'null' == mounts_id){

    }else{
        type = 1;
        id = mounts_id;
    }

    if(type == 1){
        ajaxBRequestCallFn(workflowModules+'/lcProcess/image/attr/'+id,[{activityId:$("#nodeId").val()}],function(result){
            $('#nodeImage').attr("src", result.data);
        },null,"POST");
    }
    if(type == 2){
        ajaxBRequestCallFn(workflowModules+'/lcDeploymentHis/image/attr/'+id,[{activityId:$("#nodeId").val()}],function(result){
            $('#nodeImage').attr("src", result.data);
        },null,"POST");
    }
}


/**
 * 初始化通知模板组件
 * @param notify_template_ids
 */
function initNotifyTemplateIds() {
    initComboDataCallFn("notify_template_idTemp",oauthModules+"/lcNotifyTemplate/find","id","title",null,function (data) {
        $("#notify_template_idTemp").select2({
            width: "100%", //设置下拉框的宽度
            placeholder: "请选择", // 空值提示内容，选项值为 null
            tags:true,
            /*allowClear:!0,*/
            createTag:function (decorated, params) {
                return null;
            }
        });
    });
}

/**
 *
 */
function setNotifyTemplateIds() {
    var notify_template_ids = $('#notify_template_ids').val();
    if(undefined != notify_template_ids && null != notify_template_ids && '' != notify_template_ids){
        //赋值
        var notify_template_idTemp = notify_template_ids.split(",");//注意：arr为select的id值组成的数组
        $('#notify_template_idTemp').val(notify_template_idTemp).trigger('change');
    }
}

/**
 *
 */
function saveNotifyTemplateIds() {
    var notify_template_idTemp = $('#notify_template_idTemp').val();
    var ids = null;
    if(undefined != notify_template_idTemp && null != notify_template_idTemp && '' != notify_template_idTemp){
        for(var i = 0; i < notify_template_idTemp.length; i++){
            if(ids ==  null){
                ids = notify_template_idTemp[i];
            }else{
                ids = ids+","+notify_template_idTemp[i];
            }
        }
    }
    console.log("2",notify_template_idTemp);
    $('#notify_template_ids').val(ids);
}