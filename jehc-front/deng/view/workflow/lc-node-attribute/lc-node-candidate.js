/**
 * 初始化处理人
 * 用户选择器
 * @param flag flag标识是1单个用户选择2支持多选----------
 * @param type type类型1在UserTask中使用2在“流程基本信息使用”3在泳道中使用
 */
var userFlag;
var userType;
function initassignee(flag,type){
    userFlag = flag;
    userType = type;
    var UserinfoSelectModalCount = 0 ;
    $('#UserinfoSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#UserinfoBody').height(reGetBodyHeight()-218);
    $('#UserinfoSelectModal').on("shown.bs.modal",function(){
        if(++UserinfoSelectModalCount == 1){
            $('#searchFormUserinfo')[0].reset();
            var opt = {
                searchformId:'searchFormUserinfo'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        sClass:"text-center",
                        width:"20px",
                        data:"xt_userinfo_id",
                        render:function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUserinfo" value="' + data + '" /><span></span></label>';
                        },
                        bSortable:false
                    },
                    {
                        data:"xt_userinfo_id",
                        width:"20px"
                    },
                    {
                        data:'xt_userinfo_name',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_realName',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_phone',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_origo',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_birthday',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_email',
                        width:"150px"
                    }
                ]
            });
            grid=$('#UserinfoDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallUserinfo','checkchildUserinfo');
            //实现单击行选中
            clickrowselected('UserinfoDatatables');
        }
    });
    var $modal_dialog = $("#UserinfoModalDialog");
    $modal_dialog.css({'margin': 0 + 'px auto'});
    $modal_dialog.css({'width':reGetBodyWidth()+'px'});
}

/**
 * 选择用户
 */
function doSelectUser(){
    if(returncheckedLength('checkchildUserinfo') <= 0){
        toastrBoot(4,"请选择员工");
        return;
    }
    var nTrs = grid.fnGetNodes();//fnGetNodes获取表格所有行，nTrs[i]表示第i行tr
    //CAS 1. Usertask中候选人可选择
    if(userFlag == 2 && userType == 1) {
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        var xt_userinfo_realName = "";
        for(var i = 0; i < nTrs.length; i++){
            var userName = grid.fnGetData(nTrs[i]).xt_userinfo_realName;
            var userid = grid.fnGetData(nTrs[i]).xt_userinfo_id;
            if(xt_userinfo_id.indexOf(userid)>= 0){
                //名称维护
                if (null == xt_userinfo_realName || '' == xt_userinfo_realName) {
                    xt_userinfo_realName =userName;
                } else {
                    xt_userinfo_realName = xt_userinfo_realName + "," + userName;
                }
            }
        }
        if(null != xt_userinfo_realName && '' != xt_userinfo_realName){
            msgTishCallFnBoot('确定要选择:<br>'+ xt_userinfo_realName + '？',function(){
                $('#candidateUsers').val(xt_userinfo_id);
                $('#candidateUsers_Text').val(xt_userinfo_realName);
                $('#UserinfoSelectModal').modal('hide');
            });
        }else{
            $('#UserinfoSelectModal').modal('hide');
        }

        /*var params = {xt_userinfo_id:xt_userinfo_id};
        //加载表单数据
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/users",params,function(result){
            var arr = result.data;
            var xt_userinfo_realName = "";
            if(undefined != result.data && null != result.data){
                for (var i = 0; i < arr.length; i++) {
                    //名称维护
                    if (null == xt_userinfo_realName || '' == xt_userinfo_realName) {
                        xt_userinfo_realName =arr[i].xt_userinfo_realName;
                    } else {
                        xt_userinfo_realName = xt_userinfo_realName + "," + arr[i].xt_userinfo_realName;
                    }
                }
            }
            if(null != xt_userinfo_realName && '' != xt_userinfo_realName){
                msgTishCallFnBoot('确定要选择:<br>'+ xt_userinfo_realName + '？',function(){
                    $('#candidateUsers').val(xt_userinfo_id);
                    $('#candidateUsers_Text').val(xt_userinfo_realName);
                    $('#UserinfoSelectModal').modal('hide');
                });
            }else{
                $('#UserinfoSelectModal').modal('hide');
            }

        },null,"post");*/
    }

    //CAS 2.基础信息中发起人 单选
    //Usertask中处理人选择
    if(userFlag == 1 && userType == 1){
        if(returncheckedLength('checkchildUserinfo') != 1){
            toastrBoot(4,"员工只能选择一条记录！");
            return;
        }
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        var xt_userinfo_realName = "";
        var depName = "";
        var postName = "";
        for(var i = 0; i < nTrs.length; i++){
            var userName = grid.fnGetData(nTrs[i]).xt_userinfo_realName;
            var userid = grid.fnGetData(nTrs[i]).xt_userinfo_id;
            depName = grid.fnGetData(nTrs[i]).xt_departinfo_name;
            postName = grid.fnGetData(nTrs[i]).xt_post_name;
            if(xt_userinfo_id == userid){
                xt_userinfo_realName = userName;//名称维护
                break;
            }
        }
        if(null != xt_userinfo_realName && '' != xt_userinfo_realName){
            var str ="[<font color=red><br>用户姓名:" +xt_userinfo_realName + "<br>所属部门:" + depName+"<br>所属岗位:"+postName+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){
                $('#assignee').val(xt_userinfo_id);
                $('#assignee_text').val(xt_userinfo_realName);
                $('#UserinfoSelectModal').modal('hide');
            });
        }else{
            $('#UserinfoSelectModal').modal('hide');
        }
        /*
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+xt_userinfo_id,{},function(result){
            var xt_userinfo_realName = result.data.xt_userinfo_realName;
            var str ="[<font color=red><br>用户姓名:" +result.data.xt_userinfo_realName + "<br>所属部门:" + result.data.xt_departinfo_name+"<br>所属岗位:"+result.data.xt_post_name+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){
                $('#assignee').val(xt_userinfo_id);
                $('#assignee_text').val(xt_userinfo_realName);
                $('#UserinfoSelectModal').modal('hide');
            });
        });*/
    }
    //“流程基本信息使用”中处理人选择
    if(userFlag == 1 && userType == 2){
        if(returncheckedLength('checkchildUserinfo') != 1){
            toastrBoot(4,"员工只能选择一条记录！");
            return;
        }
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        var xt_userinfo_realName = "";
        var depName = "";
        var postName = "";
        for(var i = 0; i < nTrs.length; i++){
            var userName = grid.fnGetData(nTrs[i]).xt_userinfo_realName;
            var userid = grid.fnGetData(nTrs[i]).xt_userinfo_id;
            depName = grid.fnGetData(nTrs[i]).xt_departinfo_name;
            postName = grid.fnGetData(nTrs[i]).xt_post_name;
            if(xt_userinfo_id == userid){
                xt_userinfo_realName = userName;//名称维护
                break;
            }
        }
        if(null != xt_userinfo_realName && '' != xt_userinfo_realName){
            var str ="[<font color=red><br>用户姓名:" +xt_userinfo_realName + "<br>所属部门:" + depName+"<br>所属岗位:"+postName+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){
                $('#candidateStarterUsersTemp').val(xt_userinfo_id);
                $('#candidateStarterUsersTemp_Text').val(xt_userinfo_realName);
                $('#UserinfoSelectModal').modal('hide');
            });
        }else{
            $('#UserinfoSelectModal').modal('hide');
        }
        /*
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+xt_userinfo_id,{},function(result){
            var xt_userinfo_realName = result.data.xt_userinfo_realName;
            var str ="[<font color=red><br>用户姓名:" +result.data.xt_userinfo_realName + "<br>所属部门:" + result.data.xt_departinfo_name+"<br>所属岗位:"+result.data.xt_post_name+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){
                $('#candidateStarterUsersTemp').val(xt_userinfo_id);
                $('#candidateStarterUsersTemp_Text').val(xt_userinfo_realName);
                $('#UserinfoSelectModal').modal('hide');
            });
        });*/
    }
    //泳道中使用 处理人选择
    if(userFlag == 1 && userType == 3){
        if(returncheckedLength('checkchildUserinfo') != 1){
            toastrBoot(4,"员工只能选择一条记录！");
            return;
        }
        var xt_userinfo_id = returncheckIds('checkId').join(",");
        var xt_userinfo_realName = "";
        var depName = "";
        var postName = "";
        for(var i = 0; i < nTrs.length; i++){
            var userName = grid.fnGetData(nTrs[i]).xt_userinfo_realName;
            var userid = grid.fnGetData(nTrs[i]).xt_userinfo_id;
            depName = grid.fnGetData(nTrs[i]).xt_departinfo_name;
            postName = grid.fnGetData(nTrs[i]).xt_post_name;
            if(xt_userinfo_id == userid){
                xt_userinfo_realName = userName;//名称维护
                break;
            }
        }
        if(null != xt_userinfo_realName && '' != xt_userinfo_realName){
            var str ="[<font color=red><br>用户姓名:" +xt_userinfo_realName + "<br>所属部门:" + depName+"<br>所属岗位:"+postName+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){
                $('#candidateStarterUsers_').val(xt_userinfo_id);
                $('#candidateStarterUsers_Text_').val(xt_userinfo_realName);
                $('#UserinfoSelectModal').modal('hide');
            });
        }else{
            $('#UserinfoSelectModal').modal('hide');
        }
        /*
        ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+xt_userinfo_id,{},function(result){
            var xt_userinfo_realName = result.data.xt_userinfo_realName;
            var str ="[<font color=red><br>用户姓名:" +result.data.xt_userinfo_realName + "<br>所属部门:" + result.data.xt_departinfo_name+"<br>所属岗位:"+result.data.xt_post_name+"<br></font>]";
            msgTishCallFnBoot('确定要选择:<br>'+ str + '？',function(){
                $('#candidateStarterUsers_').val(xt_userinfo_id);
                $('#candidateStarterUsers_Text_').val(xt_userinfo_realName);
                $('#UserinfoSelectModal').modal('hide');
            })
        });*/
    }
}

//单击事件
function onClick(event, treeId, treeNode, msg){
    var id = treeNode.id;
    var tempObject = treeNode.tempObject;
    if(tempObject == 'DEPART'){

    }
    if(tempObject == 'POST'){

    }
}

//初始数据
var dialogWating;
function initGroups(type) {
    var url = "";
    if(type == 0){//按部门
        url = sysModules + "/xtOrg/depart/tree";
    }
    if(type == 1){//按岗位
        url = sysModules + "/xtOrg/post/tree";
    }if(type == 2){//按角色
        url = oauthModules + "/oauthRole/trees";
    }
    if ($('#orgTree').jstree()) {//判断是否已有实例
        $('#orgTree').jstree().destroy();//销毁实例
    };
    ajaxBRequestCallFn(url,{},function(result){
        if(undefined == result.data || null == result.data || "" == result.data){
            $("#orgTree").html("暂无数据！");
        }else{
            // data = result.data;
            data=eval("("+result.data+")");
            $('#orgTree').jstree({ "types" : {
                //////////样式一///////////
                // "default" : {
                //     "icon" : "fa fa-folder"
                // },
                // "file" : {
                //     "icon" : "fa fa-file"
                // }
                ///////样式二///////////
                // "default" : {
                //     "icon" : "fa fa-folder m--font-warning"
                // },
                // "file" : {
                //     "icon" : "fa fa-file  m--font-warning"
                // }
                ///////样式三///////////
                // "default" : {
                //     "icon" : "fa fa-folder m--font-warning"
                // },
                // "file" : {
                //     "icon" : "fa fa-file  m--font-warning"
                // }
                ///////样式四///////////
                // "default" : {
                //     "icon" : "fa fa-folder m--font-brand"
                // },
                // "file" : {
                //     "icon" : "fa fa-file  m--font-brand"
                // }
                // /////样式五///////////
                // "default" : {
                //     "icon" : "fa fa-folder m--font-brand"
                // },
                // "file" : {
                //     "icon" : "fa fa-file  m--font-brand"
                // }
                // /////样式六///////////
                "default" : {
                    "icon" : "fa fa-folder m--font-success"
                },
                "file" : {
                    "icon" : "fa fa-file  m--font-success"
                }
            },
            "animation" : 10,
            "state" : { "key" : "demo2" },
            "checkbox": {
                // "keep_selected_style": false,//是否默认选中
                "three_state": false,//父子级别级联选择
                // "tie_selection": false,
                "whole_node": false //是必须配置的,这样的话点击节点名字的话就不会触发checkbox
            },
            "plugins" : ["dnd", "state", "types",'checkbox' ,"search"],
                'core' : {
                    "themes" : {
                        "responsive": false,
                        "stripes":true,
                        "ellipsis": true //节点名过长时是否显示省略号。
                    },
                    'data' : data
                }
            }).on('select_node.jstree',function(e,data){

                /*选中之后触发*/
            }).on(' deselect_node.jstree',function(e,data){
                /*取消选中触发*/
            }).on("loaded.jstree", function (event, data) {
                data.instance.clear_state();/*此句用来清除之前选中的数据不可以去掉*/
                // data.instance.open_all(-1); // -1 打开所有节点
                data.instance.open_all(2); // -1 打开所有节点
                // $('#orgTree').jstree().open_all();/*初始化打开树，对于复选框。 不展开的话，如果下面有逻辑选择则会有问题*/
                // //选择指定节点
                // var tempIds = $("#ids").val();
                // var tempIdsArr = tempIds.split(",");
                // $.each(TempIdsArr,function(index,value){
                //     var id=value;
                //     $('#orgTree').jstree('select_node',id,true,true);/*选中id对应的节点*/
                // });
                initTreeSelect();
            }).on("load_node.jstree",function(event,data){
                /*加载node时候触发*/
            });
        }
    });
}


/**
 * 初始化候选组（采用部门，岗位等）
 * @param flag flag标识是1单个组选择2支持多选
 * @param type type类型1在UserTask中使用2在“流程基本信息使用”3在泳道中使用
 * 标准格式 如：DPERT:10000,DEPART:10002
 */
var orgFlag;
var orgType;
function initcandidateGroups(flag,type){
    var candidate_group_type = $("#candidate_group_type").val();
    if(null == candidate_group_type || "" == candidate_group_type){
        toastrBoot(4,"请选择组类型！");
        return;

    }
    orgFlag = flag;
    orgType = type;
    var OrgSelectModalCount = 0 ;
    $('#OrgSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#OrgSelectModal').on("shown.bs.modal",function(){
        if(++OrgSelectModalCount == 1){
            initGroups(candidate_group_type);
        }
    });
}
// $('#OrgSelectModal').on('hide.bs.modal', function() {
//     //模态框关闭时，关闭遮罩
//     $('#jehcCoverModal').css('display','none');
// });

/**
 *
 */
function doOrgSelect(){
    var candidate_group_type = $("#candidate_group_type").val();
    var idType = "";
    if(candidate_group_type == 0){//按部门
        idType = "DEPART"
    }
    if(candidate_group_type == 1){//按岗位
        idType = "POST"

    }
    if(candidate_group_type == 2){//按角色
        idType = "ROLE"
    }

    if(candidate_group_type == 3){//按表达式
        idType = "BDS"
    }

    var treeNode = $('#orgTree').jstree(true).get_selected(true); //获取所有选中的节点对象
    var treeNode = $('#orgTree').jstree(true).get_checked(true); //获取所有 checkbox 选中的节点对象
    // var ids = $('#orgTree').jstree().get_checked();//获取所有 checkbox 选中的节点id
    if(undefined == treeNode || null == treeNode || "" == treeNode || null == candidate_group_type || "" == candidate_group_type){
        toastrBoot(4,"未选择节点！");
        return;
    }
    var text = null;
    var id = null;
    for(var i = 0; i < treeNode.length; i++) {
        //编号维护
        if(null == id){
            id= idType+":"+treeNode[i].id
        }else{
            id=id+","+idType+":"+treeNode[i].id;
        }
        //名称维护
        if(null == text){
            text=treeNode[i].text;
        }else{
            text=text+","+treeNode[i].text;
        }
    }
    msgTishCallFnBoot('确定保存所选的数据？',function(){
        //任务中使用
        if(orgType == 1){
            $('#candidateGroups').val(id);
            $('#candidateGroups_Text').val(text);
        }
        //主流程中使用
        if(orgType == 2){
            $('#candidateStarterGroupsTemp').val(id);
            $('#candidateStarterGroupsTemp_Text').val(text);
        }
        //泳道池中使用
        if(orgType == 3){
            $('#candidateStarterGroups_').val(id);
            $('#candidateStarterGroups_Text_').val(text);
        }
        $('#OrgSelectModal').modal('hide');
    })
}


/**
 *点击节点渲染处理人，处理组等
 *type 1任务中 2泳道 3主页面
 **/
function initACC(assignee,candidateUsers,candidateGroups,type){
    if(type == 1){
        //任务中处理人，候选人，处理组等节点操作
        if(null != assignee && "" != assignee){
            ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+assignee,{},function(result){
                var xt_userinfo_realName = result.data.xt_userinfo_realName;
                $('#assignee_text').val(xt_userinfo_realName);
            });
        }
        if(null != candidateUsers && "" != candidateUsers){
            var params = {xt_userinfo_id:candidateUsers};
            ajaxBRequestCallFn(sysModules+"/xtUserinfo/users",params,function(result){
                var arr = result.data;
                var xt_userinfo_realName = "";
                for (var i = 0; i < arr.length; i++) {
                    //名称维护
                    if (null == xt_userinfo_realName || "" == xt_userinfo_realName) {
                        xt_userinfo_realName =arr[i].xt_userinfo_realName
                    } else {
                        xt_userinfo_realName = xt_userinfo_realName + "," + arr[i].xt_userinfo_realName;
                    }
                }
                $('#candidateUsers_Text').val(xt_userinfo_realName);
            },null,"post");
        }
        if(null != candidateGroups && "" != candidateGroups){
            var candidate_group_type = $("#candidate_group_type").val();
            $('#candidateGroups_Text').val('');
            var orgArray = candidateGroups.split(',');
            var xt_departinfo_id = "";
            var xt_post_id = "";
            var role_id ="";
            for(var i = 0; i < orgArray.length; i++){
                var org = orgArray[i];
                var org_ = org.split(":");
                if(candidate_group_type == 0){//按部门
                    if(null != xt_departinfo_id && '' != xt_departinfo_id){
                        xt_departinfo_id = xt_departinfo_id+","+org_[1];
                    }else{
                        xt_departinfo_id = org_[1];
                    }
                }
                if(candidate_group_type == 1){//按岗位
                    if(null != xt_post_id && '' != xt_post_id){
                        xt_post_id = xt_post_id + "," + org_[1];
                    }else{
                        xt_post_id = org_[1];
                    }

                }if(candidate_group_type == 2){//按角色
                    if(null != role_id && '' != role_id){
                        role_id = role_id + "," + org_[1];
                    }else{
                        role_id = org_[1];
                    }
                }
            }
            if(null != xt_departinfo_id && '' != xt_departinfo_id){//处理部门
                ajaxBRequestCallFn(sysModules+"/xtDepartinfo/list/"+xt_departinfo_id,null,function(result){
                    var arr = result.data;
                    var candidateGroups_Text = "";
                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text && "" != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_departinfo_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_departinfo_name;
                        }
                    }
                    var candidateGroups_Text_Temp = $('#candidateGroups_Text').val();
                    if(null != candidateGroups_Text_Temp && '' != candidateGroups_Text_Temp){
                        if(null != candidateGroups_Text){
                            $('#candidateGroups_Text').val(candidateGroups_Text_Temp+","+candidateGroups_Text);
                        }
                    }else{
                        $('#candidateGroups_Text').val(candidateGroups_Text);
                    }
                },null,"GET");
            }
            if(null != xt_post_id && '' != xt_post_id){//处理岗位
                ajaxBRequestCallFn(sysModules+"/xtPost/list/"+xt_post_id,null,function(result){
                    var arr = result.data;
                    var candidateGroups_Text = "";
                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text && "" != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_post_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_post_name;
                        }
                    }
                    var candidateGroups_Text_Temp = $('#candidateGroups_Text_Temp').val();
                    if(null != candidateGroups_Text_Temp && '' != candidateGroups_Text_Temp){
                        if(null != candidateGroups_Text){
                            $('#candidateGroups_Text').val(candidateGroups_Text_Temp+","+candidateGroups_Text);
                        }
                    }else{
                        $('#candidateGroups_Text').val(candidateGroups_Text);
                    }
                },null,"GET");
            }
            if(null != role_id && '' != role_id){//处理角色
                ajaxBRequestCallFn(oauthModules+"/oauthRole/list/"+role_id,null,function(result){
                    var arr = result.data;
                    var candidateGroups_Text = "";
                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text && "" != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].role_name;
                        }else{
                            candidateGroups_Text = arr[i].role_name;
                        }
                    }
                    var candidateGroups_Text_Temp = $('#candidateGroups_Text_Temp').val();
                    if(null != candidateGroups_Text_Temp && '' != candidateGroups_Text_Temp){
                        if(null != candidateGroups_Text){
                            $('#candidateGroups_Text').val(candidateGroups_Text_Temp+","+candidateGroups_Text);
                        }
                    }else{
                        $('#candidateGroups_Text').val(candidateGroups_Text);
                    }
                },null,"GET");
            }
        }
    }
    if(type == 2){

    }
    if(type == 3){
        //泳道池中流程发起人，发起人组等节点操作
        if(null != assignee && "" != assignee){
            ajaxBRequestCallFn(sysModules+"/xtUserinfo/get/"+assignee,{},function(result){
                var xt_userinfo_realName = result.data.xt_userinfo_realName;
                $('#candidateStarterUsers_Text_').val(xt_userinfo_realName);
            });
        }
        if(null != candidateGroups && "" != candidateGroups){
            $('#candidateStarterGroups_Text_').val('');
            var orgArray = candidateGroups.split('],[');
            var xt_departinfo_id = "";
            var xt_post_id = "";
            for(var i = 0; i < orgArray.length; i++){
                var org = orgArray[i];
                org = org.replace("[",'');
                org = org.replace("]",'');
                var org_ = org.split(",");
                //部门
                if(org_[1] == 0){
                    if(null != xt_departinfo_id && '' != xt_departinfo_id){
                        xt_departinfo_id = xt_departinfo_id+","+org_[0];
                    }else{
                        xt_departinfo_id = org_[0];
                    }
                }
                //岗位
                if(org_[1] == 1){
                    if(null != xt_post_id && '' != xt_post_id){
                        xt_post_id = xt_post_id + "," + org_[0];
                    }else{
                        xt_post_id = org_[0];
                    }
                }
            }
            if(null != xt_departinfo_id && "" != xt_departinfo_id){
                //处理部门
                ajaxBRequestCallFn(sysModules+"/xtDepartinfo/list/"+xt_departinfo_id,null,function(result){
                    var arr = result.data;
                    var candidateGroups_Text = "";
                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text && "" != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_departinfo_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_departinfo_name;
                        }
                    }
                    var candidateGroups_Text_Temp = $('#candidateStarterGroups_Text_').val();
                    if(null != candidateGroups_Text_Temp && '' != candidateGroups_Text_Temp){
                        if(null != candidateGroups_Text){
                            $('#candidateStarterGroups_Text_').val(candidateGroups_Text_Temp+","+candidateGroups_Text);
                        }
                    }else{
                        $('#candidateStarterGroups_Text_').val(candidateGroups_Text);
                    }
                },null,"post");
            }
            if(null != xt_post_id && "" != xt_post_id){
                //处理岗位
                ajaxBRequestCallFn(sysModules+"/xtPost/list/"+xt_post_id,null,function(result){
                    var arr = result.data;
                    var candidateGroups_Text = "";
                    for (var i = 0; i < arr.length; i++) {
                        if(null != candidateGroups_Text && '' != candidateGroups_Text){
                            candidateGroups_Text = candidateGroups_Text+","+arr[i].xt_post_name;
                        }else{
                            candidateGroups_Text = arr[i].xt_post_name;
                        }
                    }
                    var candidateGroups_Text_Temp = $('#candidateStarterGroups_Text_').val();
                    if(null != candidateGroups_Text_Temp && '' != candidateGroups_Text_Temp){
                        if(null != candidateGroups_Text){
                            $('#candidateStarterGroups_Text_').val(candidateGroups_Text_Temp+","+candidateGroups_Text);
                        }
                    }else{
                        $('#candidateStarterGroups_Text_').val(candidateGroups_Text);
                    }
                },null,"post");
            }
        }
    }
}

/**
 *
 * @param type (1主流程 2任务 3永道)
 * @param userType（1 assignee 2 candidateUsers 3 candidateGroups）
 */
function resetAssignee(type,userType){
    if(type == 1){
        if(userType == 1){
            $("#candidateStarterUsers").val("");
            $("#candidateStarterUsersTemp").val("");
            $("#candidateStarterUsersTemp_Text").val("");
            $("#candidateStarterUsers_Text").val("");
        }
        if(userType == 2){
            $("#candidateStarterGroups").val("");
            $("#candidateStarterGroups_Text").val("");
            $("#candidateStarterGroupsTemp").val("");
            $("#candidateStarterGroupsTemp_Text").val("");
        }
    }
    if(type == 2){
        if(userType == 1){
            $("#assignee").val("");
            $("#assignee_text").val("");
        }
        if(userType == 2){
            $("#candidateUsers").val("");
            $("#candidateUsers_Text").val("");
        }
        if(userType == 3){
            $("#candidateGroups").val("");
            $("#candidateGroups_Text").val("");
        }
    }
    if(type == 3){
        if(userType == 1){
            $("#candidateStarterUsers_").val("");
            $("#candidateStarterUsers_Text_").val("");
        }
        if(userType == 2){
            $("#candidateStarterGroups_").val("");
            $("#candidateStarterGroups_Text_").val("");
        }
    }
}

/**
 * 展开/收缩
 */
function expandOrg() {
    if($("#expandOrg").val() ==  0){
        $('#orgTree').jstree('open_all')//展开全部
        $("#expandOrg").val(1);
    }else{
        $('#orgTree').jstree('close_all');//收缩全部
        $("#expandOrg").val(0);
    }
}

/**
 * 全选/反选
 */
function checkAllOrg() {
    if($("#checkedOrg").val() ==  0){
        $("#checkedOrg").val(1)
        $('#orgTree').jstree().check_all();//选中所有节点
    }else{
        $('#orgTree').jstree().uncheck_all();//取消选中所有节点
        $("#checkedOrg").val(0)
    }
}

/**
 * 组类型切换
 * @param thiz
 */
function canditateGroupChange(thiz) {
    var candidate_group_type = thiz.value;
    resetAssignee(2,3);

}

/**
 *
 * @returns {*}
 */
function initTreeSelect() {
    var ids = null;
    var candidateGroups =  $("#candidateGroups").val();
    if(null != candidateGroups && "" != candidateGroups){
        var orgArray = candidateGroups.split(',');
        for(var i = 0; i < orgArray.length; i++) {
            var org = orgArray[i];
            var org_ = org.split(":");
            if (null != ids && '' != ids) {
                ids = ids + "," + org_[1];
            } else {
                ids = org_[1];
            }
        }
    }

    if (null != ids && '' != ids) {
        // $('#orgTree').jstree().open_all();/*初始化打开树，对于复选框。 不展开的话，如果下面有逻辑选择则会有问题*/
        //选择指定节点
        var tempIds = ids;
        var tempIdsArr = tempIds.split(",");
        $.each(tempIdsArr,function(index,value){
            var id=value;
            $('#orgTree').jstree('select_node',id,true,true);/*选中id对应的节点*/
        });
    }
}