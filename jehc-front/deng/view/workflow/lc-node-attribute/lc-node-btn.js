
///////////////按钮筛选器////////////////////
function initBtn(result) {
    var lcNodeBtns = result.data.lcNodeBtns;
    $("#tbody-uuid").html("");
    if(undefined != lcNodeBtns && null != lcNodeBtns && '' != lcNodeBtns){
        $.each(lcNodeBtns, function(i, item){
            var lcJumpRules = item.lcJumpRules;
            creatBtnTable(item.btn_id,item._label,item.node_btn_id,item.btnAction,lcJumpRules,item.btn_label);
        })
    }
}


/**
 *
 * @param node_btn_id
 * @param _label
 * @param btn_id
 * @param btnAction
 */
function creatBtnTable(btn_id,_label,node_btn_id,btnAction,lcJumpRules,btnLabel){
    createNodeBtn(btn_id,_label,node_btn_id,btnAction,lcJumpRules,btnLabel);
}

/**
 * 删除按钮包括关联的所有节点
 * @param id
 */
function delBtnRow(id) {
    $("#btnHtmlRow-uuid"+id).remove();
    // resetBtn();
}

/**
 * 当前按钮关联的所有节点
 * @param id
 */
function delBtnRuleRow(id) {
    $("#btnJumpRule-uuid"+id).html("");//删除
}


/**
 * 按钮选择器
 * @param thiz
 * @param id
 */
function showBtnWin(thiz,id){
    var btnSelectModalCount = 0 ;
    $('#btnModal').modal({backdrop: 'static', keyboard: false});
    $('#btnModal').on("shown.bs.modal",function(){
        if(++btnSelectModalCount == 1){
            $('#searchFormbtn')[0].reset();
            var opt = {
                searchformId:'searchFormbtn'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcBtn/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex+1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"btn_id",
                        width:"50px"
                    },
                    {
                        data:'_label',
                        width:"150px"
                    },
                    {
                        data:"btn_id",
                        render:function(data, type, row, meta) {
                            var btn_id = row.btn_id;
                            var _label = row._label;
                            var b_action = row.b_action;
                            var b_script  = row.b_script;
                            var element_id = row.element_id;
                            var element_name =  row.element_name;
                            var tips = row.tips;
                            var btn = '<button class="btn btn-light-primary font-weight-bold mr-2" onclick=creatBtnTable("'+btn_id+'","'+_label+'","","'+b_action+'")><span><span>确 认</span></span></button>';
                            return btn;
                        }
                    }
                ]
            });
            grid=$('#btnDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('btnDatatables');
        }
    });
}


/**
 *
 */
function resetBtn() {
    $("#clickBtnId").val("");
    $("#btnAction").val("");
}

/**
 *
 */
function setBtn() {
    var uuid = $("#clickBtnId").val();
    if(undefined != uuid){
        $("#btnAction"+uuid).val($("#btnAction").val());
    }
}

function clickBtn(id) {
    $("#clickBtnId").val(id);
    $("#btnAction").val($("#btnAction"+id).val());
}

/**
 *
 * @param flag
 */
function editBtn(flag) {
    if(flag == 1){
        $("#btnAction").prop("disabled", "disabled");
    }else{
        $("#btnAction").prop("disabled", "");
    }
}


function selectRule(uuid) {
    var url = "";
    var mounts_id = $("#mounts_id").val();
    if((null == mounts_id || '' == mounts_id) && null != $("#hid_").val()){//说明非挂靠
        url = workflowModules+'/lcDeploymentHis/userTasks/'+$("#hid_").val();
    }else{
        url = workflowModules+'/lcProcess/userTasks/'+mounts_id;
    }
    var jumpRuleSelectModalCount = 0 ;
    $('#jumpRuleModal').modal({backdrop: 'static', keyboard: false});
    $('#jumpRuleModal').on("shown.bs.modal",function(){
        if(++jumpRuleSelectModalCount == 1){
            $('#searchFormjumpRule')[0].reset();
            var opt = {
                searchformId:'searchFormjumpRule'
            };
            var options = DataTablesList.listOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,url,opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex+1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"nodeId",
                        width:"50px"
                    },
                    {
                        data:'nodeName',
                        width:"150px"
                    },
                    {
                        data:"nodeId",
                        render:function(data, type, row, meta) {
                            var nodeId = row.nodeId;
                            var nodeName = row.nodeName;
                            var btn = '<button class="btn btn-light-primary font-weight-bold mr-2" onclick=createBtnJumpRuleTable("'+nodeId+'","'+nodeName+'","","","","'+uuid+'")><span><span>确 认</span></span></button>';
                            return btn;
                        }
                    }
                ]
            });
            grid=$('#jumpRuleDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('jumpRuleDatatables');
        }
    });
}

/**
 *
 * @param target_node_id
 * @param nodeName
 * @param jump_rules_id
 * @param based_on_condition
 * @param conditions_
 * @param parentUUIDd
 */
function createBtnJumpRuleTable(target_node_id,nodeName,jump_rules_id,based_on_condition,conditions_,parentUUID) {
    var uuid = guid();
    if(target_node_id === undefined){
        target_node_id = "";
    }
    if(nodeName === undefined){
        nodeName = "";
    }
    if(jump_rules_id === undefined){
        jump_rules_id = "";
    }
    if(based_on_condition === undefined){
        based_on_condition = "0";
    }
    if(conditions_ === undefined){
        conditions_ = "";
    }

    var backgound="#c9f7f5";//背景样式
    if($("#nodeId").val() == target_node_id){
        backgound = "#f64e60";
    }
    var btnHtml =
        '<div class="form-group m-form__group row" id="btnRule-uuid'+uuid+'">'+
            '<input class="form-control" type="hidden" id="target_node_id'+uuid+'" value="'+target_node_id+'" name="lcNodeBtns[][lcJumpRules][][target_node_id]">'+
            '<input class="form-control" type="hidden" id="jump_rules_id'+uuid+'" value="'+jump_rules_id+'" name="lcNodeBtns[][lcJumpRules][][jump_rules_id]">'+
            '<input class="form-control" type="hidden" id="nodeName'+uuid+'" value="'+nodeName+'" name="lcNodeBtns[][lcJumpRules][][node_name]">'+
            '<label class="col-md-3 col-form-label">'+nodeName+'</label>'+
            '<div class="col-md-3">'+
                '<input class="form-control" id="conditions_'+uuid+'" value="'+conditions_+'" name="lcNodeBtns[][lcJumpRules][][conditions_]" type="text" placeholder="请输入流转条件">'+
            '</div>'+
            '<div class="col-md-5">'+
                '<input class="form-control" type="text" style="background: '+backgound+'" readonly value="'+target_node_id+'">'+
            '</div>'+
            '<div class="col-md-1">'+
                '<a class="btn btn-light-primary font-weight-bold mr-2" href=javascript:delNodeBtnRule(\"'+uuid+'\")>'+
                '<span><i class="fa fa-times"></i><span>删 除</span></span>'+
                '</a>'+
            '</div>'+
        '</div>';
    $("#btnJumpRule-uuid"+parentUUID).append(btnHtml);//采用父级uuid
    $('#jumpRuleModal').modal('hide');
}

/**
 * 删除单个关联节点
 */
function delNodeBtnRule(uuid) {
    $("#btnRule-uuid"+uuid).remove();
}


function createNodeBtn(btn_id,_label,node_btn_id,btnAction,lcJumpRules,btnLabel) {
    var uuid = guid();
    if(btn_id === undefined){
        btn_id = "";
    }
    if(_label === undefined){
        _label = "";
    }
    if(node_btn_id === undefined){
        node_btn_id = "";
    }
    if(btnAction === undefined){
        btnAction = "";
    }
    if(btnLabel === undefined){
        btnLabel = "";
    }
    if(btnLabel == null){
        btnLabel = "";
    }
    var btnHtml =
        '<tr id="btnHtmlRow-uuid'+uuid+'">'+
            '<input class="form-control" type="hidden" id="node_btn_id'+uuid+'" value="'+node_btn_id+'" name="lcNodeBtns[][node_btn_id]">'+
            '<input class="form-control" type="hidden" id="btn_id'+uuid+'" value="'+btn_id+'" name="lcNodeBtns[][btn_id]">'+
            '<td style="width: 120px;"><div class="btn-group ml-2"><a class="btn btn-light-success font-weight-bold" id="_label'+uuid+'" style="border-bottom-left-radius: 6px;border-top-left-radius: 6px;">'+_label+'</a><button type="button" class="btn btn-light-success font-weight-bold dropdown-toggle dropdown-toggle-split" style="border-bottom-right-radius: 6px;border-top-right-radius: 6px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only"></span></button><div class="dropdown-menu" style="border-bottom-right-radius: 6px;border-top-right-radius: 6px;"><a class="dropdown-item" href=javascript:delBtnRow("'+uuid+'")><i class="la la-times"></i>删除按钮</a><a class="dropdown-item" href=javascript:delBtnRuleRow("'+uuid+'")><i class="la la-times"></i>删除关联节点</a><button type="button" class="dropdown-item" onclick=selectRule(\"'+uuid+'\") title="请选择关联节点"><i class="la la-pagelines"></i>关联节点</button></div></div></td>'+
            '<td style="width: 220px;"><input class="form-control" id="btnAction'+uuid+'" value="'+btnAction+'" type="text" name="lcNodeBtns[][btn_action]" placeholder="请输入扩展动作"><input class="form-control" id="btnLabel'+uuid+'" value="'+btnLabel+'" type="text" name="lcNodeBtns[][btn_label]" placeholder="请输入扩展按钮名称"></td>'+
            '<td id="btnJumpRule-uuid'+uuid+'">'+
            '</td>'+
        '</tr>';
    $("#tbody-uuid").append(btnHtml);
    if(undefined != lcJumpRules && null != lcJumpRules && '' != lcJumpRules){
        for(var i in lcJumpRules){
            createBtnJumpRuleTable(lcJumpRules[i].target_node_id,lcJumpRules[i].node_name,lcJumpRules[i].jump_rules_id,lcJumpRules[i].based_on_condition,lcJumpRules[i].conditions_,uuid);
        }
    }
    $('#btnModal').modal('hide');
}