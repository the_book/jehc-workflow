/**
 *
 * @param result
 */
function initMutil(result) {
    var policy = result.data.policy;
    var votingStrategy = result.data.votingStrategy;
    var allowSign = result.data.allowSign;
    var needAllVote = result.data.needAllVote;
    var percentage = result.data.percentage;
    var numberVotes = result.data.numberVotes;

    //决策方式
    if(undefined != policy && null != policy && "" != policy){
        // $("input[name=policy][value="+policy+"]").attr("checked",true);
        $("input[name=policy][value="+policy+"]").prop("checked",true);
    }

    //策略
    if(undefined != votingStrategy && null != votingStrategy && "" != votingStrategy){
        $("input[name=votingStrategy][value="+votingStrategy+"]").prop("checked",true);

        if(votingStrategy == 10){//百分比
            $('#percentage').val(percentage);
            $('#percentageDIV').show();
            $('#numberVotes').val("");
            $('#numberVotesDIV').hide();
        }

        if(votingStrategy == 20){//投票数
            $('#percentage').val("");
            $('#percentageDIV').hide();
            $('#numberVotes').val(numberVotes);
            $('#numberVotesDIV').show();
        }
    }else{
        $('#percentage').val(percentage);
        $('#percentageDIV').show();
        $('#numberVotes').val("");
        $('#numberVotesDIV').hide();
    }

    //是否需要所有人员投票
    if(undefined != needAllVote && null != needAllVote && "" != needAllVote){
        $("input[name=needAllVote][value="+needAllVote+"]").prop("checked",true);
    }
    $('#allowSign').val(allowSign);
    $('#numberVotes').val(numberVotes);

    if(undefined != allowSign && null != allowSign && "" != allowSign && 10 == allowSign){
        $("[name='allowSignCheckBox']").prop("checked",true);
    }else{
        $("[name='allowSignCheckBox']").prop("checked",false);
    }
}

/**
 *
 */
function clickVotingStrategy() {
    var votingStrategy = $("input[name='votingStrategy']:checked").val();
    if(votingStrategy == 10){
        if(null == $('#percentage').val() || "" == $('#percentage').val()){//默认51%
            $('#percentage').val("51");
        }
        $('#percentageDIV').show();
        $('#numberVotes').val("");
        $('#numberVotesDIV').hide();
    }else{
        if(null == $('#numberVotes').val() || "" == $('#numberVotes').val()){//默认1
            $('#numberVotes').val("1");
        }
        $('#percentage').val("");
        $('#percentageDIV').hide();
        $('#numberVotesDIV').show();
    }
}

function clickAllowSign() {
    if($('#allowSignCheckBox').prop('checked')){
        $('#allowSign').val(10);
    }else{
        $('#allowSign').val("");
    }
}

function initMutilHtml(mutil) {
    console.log("mutil",mutil)
    if(mutil == 20){//如果会签节点 则展示会签配置
        $('#mutilTab').removeAttr("class");
    }else{
        $('#mutilTab').attr("class","disabled");
    }
}