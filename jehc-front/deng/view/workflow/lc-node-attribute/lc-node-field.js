var formFieldDatatablesGrid;
/**
 * 初始化表单字段权限窗体
 */
function initFormFieldWin() {
    var formFieldSelectModalCount = 0 ;
    $('#formFieldBody').height(reGetBodyHeight()-218);
    $('#formFieldSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#formFieldSelectModal').on("shown.bs.modal",function(){
        var $modal_dialog = $("#formFieldModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++formFieldSelectModalCount == 1){
            $('#searchFormUserinfo')[0].reset();
            var opt = {
                searchformId:'searchFormformField'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/lcFormField/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'220px',
                //列表表头字段
                colums:[
                    {
                        sClass:"text-center",
                        width:"20px",
                        data:"lc_form_field_id",
                        render:function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildformField" value="' + data + '" /><span></span></label>';
                        },
                        bSortable:false
                    },
                    {
                        data:"lc_form_field_id",
                        width:"20px"
                    },
                    {
                        data:'field_label',
                        width:"50px",
                    },
                    {
                        data:'field_read',
                        width:"50px",
                        render:function(data, type, row, meta) {
                            if(data == 0){
                                return "是";
                            }
                            if(data == 1){
                                return "否";
                            }
                            return "<font style='color: #3699ff;'>缺省</font>";
                        }
                    },
                    {
                        data:'field_write',
                        width:"50px",
                        render:function(data, type, row, meta) {
                            if(data == 0){
                                return "是";
                            }
                            if(data == 1){
                                return "否";
                            }
                            return "<font style='color: #3699ff;'>缺省</font>";
                        }
                    },
                    {
                        data:'filed_hide',
                        width:"50px",
                        render:function(data, type, row, meta) {
                            if(data == 0){
                                return "是";
                            }
                            if(data == 1){
                                return "否";
                            }
                            return "<font style='color: #3699ff;'>缺省</font>";
                        }
                    },
                    {
                        data:'filed_required',
                        width:"50px",
                        render:function(data, type, row, meta) {
                            if(data == 0){
                                return "是";
                            }
                            if(data == 1){
                                return "否";
                            }
                            return "<font style='color: #3699ff;'>缺省</font>";
                        }
                    }
                ]
            });
            formFieldDatatablesGrid=$('#formFieldDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallformField','checkchildformField');
            //实现单击行选中
            clickrowselected('formFieldDatatables');
        }
    });
}
/**
 * 新一行
 * @param field_label 标签
 * @param field_id 字段id
 * @param field_name 字段名称
 * @param field_read 只读
 * @param field_write 可写
 * @param filed_hide 隐藏
 * @param filed_required 必填
 */
function addFormFieldRow(field_label,field_id,field_name,field_read,field_write,filed_hide,filed_required){
    //表单配置
    // $('#lcNodeAttrform').bootstrapValidator({
    //     message:'此值不是有效的'
    // });
    if(field_label === undefined){
        field_label = "";
    }
    if(field_id === undefined){
        field_id = "";
    }
    if(field_name === undefined){
        field_name = "";
    }
    if(field_read === undefined){
        field_read = "";
    }
    if(field_write === undefined){
        field_write = "";
    }
    if(filed_hide === undefined){
        filed_hide = "";
    }
    if(filed_required === undefined){
        filed_required = "";
    }
    var uuid = guid();
    // validatorDestroy('lcNodeAttrform');
    var rows =
        "<div class=\"form-group m-form__group row\" id='node_form_field_rows_"+uuid+"'>"+
            //标签
            "<div class=\"col-md-2\">"+
                "<input class='form-control' type='hidden' id='field_name"+uuid+"' name='nodeFormField[][field_name]' value='"+field_name+"'>"+
                "<input class='form-control' type='hidden' id='field_id"+uuid+"' name='nodeFormField[][field_id]' value='"+field_id+"'>"+
                "<input class=\"form-control\" type=\"text\"  data-bv-notempty data-bv-notempty-message='请输入' id='field_label"+uuid+"' name='nodeFormField[][field_label]' value='"+field_label+"' placeholder='请输入'>"+
            "</div>"+
            //只读
            "<div class=\"col-md-2\">"+
                "<select class=\"form-control\" id='field_read"+uuid+"' name=\"nodeFormField[][field_read]\">" +
                    "<option value=''>请选择</option>" +
                    "<option value='0'>是</option>" +
                    "<option value='1'>否</option>" +
                "</select>"+
            "</div>"+

            //可写
            "<div class=\"col-md-2\">"+
                "<select class=\"form-control\" id='field_write"+uuid+"' name=\"nodeFormField[][field_write]\">" +
                    "<option value=''>请选择</option>" +
                    "<option value='0'>是</option>" +
                    "<option value='1'>否</option>" +
                "</select>"+
            "</div>"+

            //隐藏
            "<div class=\"col-md-2\">"+
                "<select class=\"form-control\" id='filed_hide"+uuid+"' name=\"nodeFormField[][filed_hide]\">" +
                    "<option value=''>请选择</option>" +
                    "<option value='0'>是</option>" +
                    "<option value='1'>否</option>" +
                "</select>"+
            "</div>"+

            //必填
            "<div class=\"col-md-2\">"+
                "<select class=\"form-control\" id='filed_required"+uuid+"' name=\"nodeFormField[][filed_required]\">" +
                    "<option value=''>请选择</option>" +
                    "<option value='0'>是</option>" +
                    "<option value='1'>否</option>" +
                "</select>"+
            "</div>"+
            //操作
            "<div class=\"col-md-2\">"+
            "   <button type=\"button\" class=\"btn btn-light-danger mr-3\" onclick='delNodeFormFieldRow(\""+uuid+"\")' title='删除'><i class=\"la la-times\"></i></button>"+
            "</div>"+
        "</div>";
    $("#node_form_field").append(rows);
    $("#field_read"+uuid).val(field_read);
    $("#field_write"+uuid).val(field_write);
    $("#filed_hide"+uuid).val(filed_hide);
    $("#filed_required"+uuid).val(filed_required);
    reValidator('lcNodeAttrform');
}

/**
 * 删除表单配置
 * @param rowID
 */
function delNodeFormFieldRow(rowID){
    msgTishCallFnBoot("确认删除？",function(){
        validatorDestroy('lcNodeAttrform');
        $("#node_form_field_rows_"+rowID).remove();
        reValidator('lcNodeAttrform');
    });
}

/**
 * 保存表单配置
 * @param cell
 */
function event_form_setvalue(){
    var bootform = $('#eventForm');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    if(!boostrapValidator.isValid()){
        window.parent.toastrBoot(4,"存在不合法的字段!");
        return false;
    }
    var form_node_value = JSON.stringify($("#eventForm").serializeJSON());
    if(null != form_node_value && "" != form_node_value){
        JehcClickCell.form_node_value = form_node_value;
    }else{
        JehcClickCell.form_node_value = "";
    }
    return true;
}

/**
 *
 */
function doSelectFormfield() {
    msgTishCallFnBoot("确定要选择？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {lc_form_field_id:id};
        ajaxBRequestCallFn(workflowModules+'/lcFormField/find',params,function(result){
            result = result.data;
            if(undefined != result){
                $.each(result, function(i, item){
                    addFormFieldRow(item.field_label,item.field_id,item.field_name,item.field_read,item.field_write,item.filed_hide,item.filed_required);
                })
            }
            $('#formFieldSelectModal').modal('hide');
        });
    })

}