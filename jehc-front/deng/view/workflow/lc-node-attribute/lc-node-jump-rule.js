

/**
 * 展示
 * @param thiz
 * @param id
 */
function showTargetNodeWin(thiz,id) {
    var url = "";
    var mounts_id = $("#mounts_id").val();
    if((null == mounts_id || '' == mounts_id) && null != $("#hid_").val()){//说明非挂靠
        url = workflowModules+'/lcDeploymentHis/userTasks/'+$("#hid_").val();
    }else{
        url = workflowModules+'/lcProcess/userTasks/'+mounts_id;
    }
    var jumpRuleSelectModalCount = 0 ;
    $('#jumpRuleModal').modal({backdrop: 'static', keyboard: false});
    $('#jumpRuleModal').on("shown.bs.modal",function(){
        if(++jumpRuleSelectModalCount == 1){
            $('#searchFormjumpRule')[0].reset();
            var opt = {
                searchformId:'searchFormjumpRule'
            };
            var options = DataTablesList.listOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,url,opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(0)', nRow).html(iDisplayIndex+1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"nodeId",
                        width:"50px"
                    },
                    {
                        data:'nodeName',
                        width:"150px"
                    },
                    {
                        data:"nodeId",
                        render:function(data, type, row, meta) {
                            var nodeId = row.nodeId;
                            var nodeName = row.nodeName;
                            var btn = '<button class="btn btn-light-primary font-weight-bold mr-2" onclick=createJumpRuleTable("'+nodeId+'","'+nodeName+'")><span><span>确 认</span></span></button>';
                            return btn;
                        }
                    }
                ]
            });
            grid=$('#jumpRuleDatatables').dataTable(options);
            //实现单击行选中
            clickrowselected('jumpRuleDatatables');
        }
    });
}

/**
 *
 * @param nodeId
 * @param nodeName
 */
function createJumpRuleTable(target_node_id,nodeName,jump_rules_id,based_on_condition,conditions_) {
    var uuid = guid();
    if(target_node_id === undefined){
        target_node_id = "";
    }
    if(nodeName === undefined){
        nodeName = "";
    }
    if(jump_rules_id === undefined){
        jump_rules_id = "";
    }
    if(based_on_condition === undefined){
        based_on_condition = "0";
    }
    if(conditions_ === undefined){
        conditions_ = "";
    }
    var rows = "<div id='node_jumprule_rows_"+uuid+"' onclick=clickJumprule('"+uuid+"')>"+
                "<input class=\"form-control\" type='hidden' id='conditions_"+uuid+"' value='"+conditions_+"' name='lcJumpRules[][conditions_]'>"+
                "<input class=\"form-control\" type='hidden' id='nodeName"+uuid+"' value='"+nodeName+"' name='lcJumpRules[][node_name]'>"+
                "<input class='form-control' type='hidden' id='based_on_condition"+uuid+"' value='"+based_on_condition+"' name='lcJumpRules[][based_on_condition]'>"+
                "<input class=\"form-control\" type=\"hidden\" id='jump_rules_id"+uuid+"' value='"+jump_rules_id+"' name='lcJumpRules[][jump_rules_id]'>"+
                "<input class=\"form-control\" type=\"hidden\" id='target_node_id"+uuid+"' value='"+target_node_id+"' name='lcJumpRules[][target_node_id]'>"+
                "<div class=\"btn-group ml-2\"><a class=\"btn btn-light-success font-weight-bold\" style='border-bottom-left-radius: 6px;border-top-left-radius: 6px;' id='nodeName"+uuid+"'>"+nodeName+"</a><button type=\"button\" class=\"btn btn-light-success font-weight-bold dropdown-toggle dropdown-toggle-split\" style='border-bottom-right-radius: 6px;border-top-right-radius: 6px;' data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"sr-only\"></span></button><div class=\"dropdown-menu\" style=\"\"><a class=\"dropdown-item\" href=javascript:delJumpRuleRow('"+uuid+"')><i class='la la-times'></i>删 除</a><a class=\"dropdown-item\" href=javascript:editJump('"+uuid+"')><i class='flaticon-edit-1'></i>编辑</a><button type=\"button\" class=\"dropdown-item\" onclick=\"editJump(1)\" title='禁止使用编辑框'><i class=\"fa flaticon-interface-11\"></i>禁止编辑</button></div></div>"+
            "</div>";
    $("#jumpRuleTable").append(rows);
    $('#jumpRuleModal').modal('hide');
    clickJumprule(uuid)
}

function delJumpRuleRow(id) {
    $("#node_jumprule_rows_"+id).remove();
    resetJumprule();
}

function resetJumprule(){
    $("#jumpruleClickId").val("");
    $("#based_on_condition").val("");
    $("#conditions_").val("");
    $("[name='based_on_condition_']").prop("checked",false);
}
/**
 *
 * @param id
 */
function clickJumprule(id) {
    $("#jumpruleClickId").val(id);
    $("#conditions_").val($("#conditions_"+id).val());
    var based_on_condition = $("#based_on_condition"+id).val();
    if(based_on_condition == "1") {
        $("[name='based_on_condition_']").val("")
        $("[name='based_on_condition_']").prop("checked", true);
    }else{
        $("[name='based_on_condition_']").prop("checked",false);
    }
}


/**
 *
 */
function setJumpRule() {
    var uuid = $("#jumpruleClickId").val();
    if(undefined != uuid){
        $("#conditions_"+uuid).val($("#conditions_").val());
        if($('#based_on_condition_').is(':checked')) {
            $("#based_on_condition"+uuid).val("1");
        }else{
            $("#based_on_condition"+uuid).val("0");
        }
    }
}

/**
 *
 * @param id
 */
function editJump(flag) {
    if(flag == 1){
        $("[name='based_on_condition_']").prop("disabled", "disabled");
        $("#conditions_").prop("disabled", "disabled");
    }else{
        $("[name='based_on_condition_']").prop("disabled", "");
        $("#conditions_").prop("disabled", "");
    }

}