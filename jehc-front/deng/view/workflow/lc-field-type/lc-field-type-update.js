//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-field-type/lc-field-type-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateLcFieldType(){
	submitBForm('defaultForm',workflowModules+'/lcFieldType/update',base_html_redirect+'/workflow/lc-field-type/lc-field-type-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var type_id = GetQueryString("type_id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcFieldType/get/'+type_id,{},function(result){
		$('#type_id').val(result.data.type_id);
        $('#field_type').val(result.data.field_type);
		$('#name').val(result.data.name);
		$('#remark').val(result.data.remark);

	});
});
