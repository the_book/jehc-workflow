//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-field-type/lc-field-type-list.html');
}
$(document).ready(function(){
	var type_id = GetQueryString("type_id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcFieldType/get/'+type_id,{},function(result){
		$('#field_type').val(result.data.field_type);
		$('#name').val(result.data.name);
        $('#remark').val(result.data.remark);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);

	});
});
