//返回
function goback(){
    tlocation(base_html_redirect+'/workflow/lc-form-field/lc-form-field-list.html?lc_form_id='+$("#lc_form_id").val());
	// tlocation(base_html_redirect+'/workflow/lc-form-field/lc-form-field-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateLcFormField(){
	submitBForm('defaultForm',workflowModules+'/lcFormField/update',base_html_redirect+'/workflow/lc-form-field/lc-form-field-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var lc_form_field_id = GetQueryString("lc_form_field_id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcFormField/get/'+lc_form_field_id,{},function(result){
		$('#lc_form_field_id').val(result.data.lc_form_field_id);
		$('#field_label').val(result.data.field_label);
		$('#field_id').val(result.data.field_id);
		$('#field_name').val(result.data.field_name);
		$('#field_read').val(result.data.field_read);
		$('#field_write').val(result.data.field_write);
		$('#filed_hide').val(result.data.filed_hide);
		$('#filed_required').val(result.data.filed_required);
		$('#create_id').val(result.data.create_id);
		$('#update_id').val(result.data.update_id);
		$('#del_flag').val(result.data.del_flag);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
		$('#field_maxlength').val(result.data.field_maxlength);
		$('#field_type_id').val(result.data.field_type_id);
		$('#field_html_id').val(result.data.field_html_id);
		$('#sort_').val(result.data.sort_);
		$('#participation').val(result.data.participation);
		$('#tips').val(result.data.tips);
		$('#lc_form_id').val(result.data.lc_form_id);
        $('#field_type_idTemp').val(result.data.field_type_id);
        $('#field_html_idTemp').val(result.data.field_html_id);
        initFieldTypeList("field_type_id");
        initFieldHtmlList("field_html_id");
	});
});



/**
 *
 * @param id
 */
function initFieldTypeList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:workflowModules+"/lcFieldType/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.type_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
            $('#field_type_id').val($('#field_type_idTemp').val());
        }
    });
}


/**
 *
 * @param id
 */
function initFieldHtmlList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:workflowModules+"/lcFieldHtml/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.lc_field_html_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
            $('#field_html_id').val($('#field_html_idTemp').val());
        }
    });
}