//返回
function goback(){
    tlocation(base_html_redirect+'/workflow/lc-form-field/lc-form-field-list.html?lc_form_id='+$("#lc_form_id").val());
	// tlocation(base_html_redirect+'/workflow/lc-form-field/lc-form-field-list.html');
}
$(document).ready(function(){
	var lc_form_field_id = GetQueryString("lc_form_field_id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcFormField/get/'+lc_form_field_id,{},function(result){
		$('#lc_form_field_id').val(result.data.lc_form_field_id);
		$('#field_label').val(result.data.field_label);
		$('#field_id').val(result.data.field_id);
		$('#field_name').val(result.data.field_name);
		$('#field_read').val(result.data.field_read);
		$('#field_write').val(result.data.field_write);
		$('#filed_hide').val(result.data.filed_hide);
		$('#filed_required').val(result.data.filed_required);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
		$('#field_maxlength').val(result.data.field_maxlength);
		$('#field_type_id').val(result.data.field_type_id);
		$('#field_html_id').val(result.data.field_html_id);
		$('#sort_').val(result.data.sort_);
		$('#participation').val(result.data.participation);
		$('#tips').val(result.data.tips);
		$('#lc_form_id').val(result.data.lc_form_id);

        $('#field_type_id').val(result.data.typeName);
        $('#field_html_id').val(result.data.htmlName);
	});
});
