//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-form-field/lc-form-field-list.html?lc_form_id='+$("#lc_form_id").val());
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function addLcFormField(){
	submitBForm('defaultForm',workflowModules+'/lcFormField/add',base_html_redirect+'/workflow/lc-form-field/lc-form-field-list.html?lc_form_id='+$("#lc_form_id").val());
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
    var lc_form_id = GetQueryString("lc_form_id");
    $("#lc_form_id").val(lc_form_id);
    initFieldTypeList("field_type_id");
    initFieldHtmlList("field_html_id");
});


/**
 *
 * @param id
 */
function initFieldTypeList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:workflowModules+"/lcFieldType/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.type_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
        }
    });
}


/**
 *
 * @param id
 */
function initFieldHtmlList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:workflowModules+"/lcFieldHtml/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.lc_field_html_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
        }
    });
}
