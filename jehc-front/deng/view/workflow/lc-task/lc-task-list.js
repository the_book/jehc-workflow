var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcTask/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(0)', nRow).html(iDisplayIndex +1);
            $('td:eq(1)', nRow).attr('style', 'text-align:left;')
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        /*fixedHeader:true,//表头固定
        fixedColumns: {
            "leftColumns": 2,
            "rightColumns": 1
        },*/
        //列表表头字段
        colums:[
            {
                data:"taskId",
                width:"20px"
            },
            {
                data:"taskId",
                width:"50px",
                render:function(data, type, row, meta) {
                    var processInstanceId = row.processInstanceId;
                    var taskId = row.taskId;
                    var btn = '';
                    // btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情" onclick=toLcTaskHisDetail('+taskId+')><i class="la la-eye"></i></button>';
                    // btn = btn + '<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="查看流程实例图" onclick=showLcProcessInstance('+processInstanceId+')><i class="la la-image"></i></button>';
                    // btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="查看流程实例审批记录" onclick=initLcApprovalWin('+processInstanceId+')><i class="fa flaticon-graphic-1"></i></button>';
                    // btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="设置经办人" onclick=setAssignee('+taskId+')><i class="fa flaticon-avatar"></i></button>';
                    // btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="设置发起人" onclick=setOwner('+taskId+')><i class="fa fa-user-plus"></i></button>';
                    // btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="添加组成员" onclick=addGroupUser('+taskId+')><i class="fa flaticon-users"></i></button>';
                    // btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="移除组成员" onclick=deleteGroupUser('+taskId+')><i class="fa flaticon-cancel"></i></button>';

                    btn = btn+ '<div class="btn-group">' +
                        '<a href="#" class="btn btn-light-danger font-weight-bold" data-toggle="dropdown" aria-expanded="true">' +
                        '<i class="fa flaticon-bell"></i>监控' +
                        '</a>' +
                        '<button type="button" class="btn btn-light-danger font-weight-bold dropdown-toggle dropdown-toggle-split mr-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<span class="sr-only"></span>'+
                        '</button>'+
                        '<div class="dropdown-menu dropdown-menu-right">' +
                        '<button class="dropdown-item" onclick=showLcProcessInstance("' + processInstanceId + '")><i class="la la-image"></i>流程监控</button>' +
                        '<button class="dropdown-item" onclick=showLcProcessInstanceBase64("' + taskId + '")><i class="la la-file-image-o"></i>活动图</button>' +
                        '<button class="dropdown-item" onclick=initLcApprovalWin("' + processInstanceId + '")><i class="fa flaticon-graphic-1"></i>审批记录</a>' +
                        '<button class="dropdown-item" onclick=toLcTaskHisDetail("' + taskId + '")><i class="la la-eye"></i>详情</a>' +
                        '</div>' +
                        '</div>';
                    btn = btn+ '<div class="btn-group ml-2 mr-2">' +
                        '<a href="#" class="btn btn-light-success font-weight-bold" data-toggle="dropdown" aria-expanded="true">' +
                        '<i class="fa flaticon-edit-1"></i>操作审批人' +
                        '</a>' +
                        '<button type="button" class="btn btn-light-success font-weight-bold dropdown-toggle dropdown-toggle-split mr-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<span class="sr-only"></span>'+
                        '</button>'+
                        '<div class="dropdown-menu dropdown-menu-right">' +
                        '<button class="dropdown-item" onclick=setAssignee("' + taskId + '")><i class="fa flaticon-avatar"></i>转办</button>' +
                        '<button class="dropdown-item" onclick=delegateTask("' + taskId + '")><i class="fa flaticon-businesswoman"></i>委派</button>' +
                        '<button class="dropdown-item" onclick=setOwner("' + taskId + '")><i class="fa fa-user-plus"></i>设置任务所属人</a>' +
                        '<button class="dropdown-item" onclick=addGroupUser("' + taskId + '")><i class="fa flaticon-users"></i>添加组成员</a>' +
                        '<button class="dropdown-item" onclick=deleteGroupUser("' + taskId + '")><i class="fa flaticon-cancel"></i>移除组成员</a>' +
                        '</div>' +
                        '</div>';
                    btn = btn+ '<div class="btn-group ml-2 mr-2">' +
                        '<a href="#" class="btn btn-light-warning font-weight-bold" data-toggle="dropdown" aria-expanded="true">' +
                        '<i class="fa flaticon-edit-1"></i>操作' +
                        '</a>' +
                        '<button type="button" class="btn btn-light-warning font-weight-bold dropdown-toggle dropdown-toggle-split mr-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<span class="sr-only"></span>'+
                        '</button>'+
                        '<div class="dropdown-menu dropdown-menu-right">' +
                        '<button class="dropdown-item " title="强制终止流程" onclick=endProcess('+taskId+')><i class="fa flaticon-delete"></i>强行终止</button>'+
                        '<button class="dropdown-item " title="跳转" onclick=initLcTaskJumpWin('+processInstanceId+','+taskId+')><i class="fa fa-history fa-lg"></i>跳 转</button>' +

                        '<button class="dropdown-item " title="驳回" onclick=initLcTaskBackActiviImplWin('+taskId+')><i class="fa flaticon-information"></i>驳 回</button>'+
                        '<button class="dropdown-item " title="选择下一节点提交" onclick=initNextNodeWin('+taskId+')><i class="fa flaticon-clock"></i>提 交</button>'+
                        '</div>' +
                        '</div>';
                    btn = btn+ '<button class="btn btn-light-dark font-weight-bold mr-2" title="办理任务" onclick=showDynamicActiviImpl('+taskId+')><i class="fa flaticon-interface-9"></i>办 理</button>';
                    btn = btn+ '<button class="btn btn-light-primary font-weight-bold" title="催办任务" onclick=urge('+taskId+')><i class="fa flaticon-alert-2"></i>催 办</button>';
                    return btn;
                }
            },
            {
                data:'name',
                width:"50px"
            },
            {
                data:'owner',
                width:"50px"
            },
            {
                data:'assignee',
                width:"50px"
            },
            {
                data:'description',
                width:"50px"
            },
            {
                data:'processTitle'
            },
            {
                data:'version',
                width:"150px",
                render:function(data, type, row, meta) {
                    return "<span class='label label-light-warning mr-2'>"+data+"</span>"
                }
            },{
                data:'businessKey'
            },
            {
                data:'processInstanceId',
                width:"50px",
                render:function(data, type, row, meta) {
                    return "<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>"+data+"</span>"
                }
            },
            {
                data:'taskId',
                width:"50px"
            },
            {
                data:'taskDefinitionKey',
                width:"50px"
            },
            {
                data:'tenantId',
                width:"50px"
            },
            {
                data:'suspended',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == true){
                        return "<span class='label label-lg font-weight-bold label-light-info label-inline'>挂起</span>"
                    }
                    if(data == false){
                        return "<span class='label label-lg font-weight-bold label-light-success label-inline'>激活</span>"
                    }
                    return "<span class='label label-lg font-weight-bold label-light-info label-inline'>缺省</span>"
                }
            },
            {
                data:'createTime',
                width:"50px",
                render:function(data, type, row, meta) {
                    return "<span class='label label-lg font-weight-bold label-light-primary label-inline'>"+dateformat(data)+"</span>";
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现单击行选中
    clickrowselected('datatables');
});


/**
 * 转办
 * @param taskId
 */
function setAssignee(taskId){
    initassignee(taskId,0);
}

/**
 * 归属人
 * @param taskId
 */
function setOwner(taskId){
    initassignee(taskId,1);
}

/**
 * 组成员
 * @param taskId
 */
function addGroupUser(taskId){
    initassignee(taskId,2);
}

/**
 * 删除组成员
 * @param taskId
 */
function deleteGroupUser(taskId){
    initassignee(taskId,3);
}

/**
 * 委派（加签）
 * @param taskId
 */
function delegateTask(taskId) {
    initassignee(taskId,4);
}

function initassignee(taskId,type){
    var text = "";
    $('#lcTaskType').val(type);
    $('#taskId').val(taskId);
    if(type == 0){
        text="转办";
    }
    if(type == 1){
        text="设置归属人";
    }
    if(type == 2){
        text="添加组成员";
    }
    if(type == 3){
        text="移除组成员";
    }
    if(type == 4){
        text="委派";
    }
    $('#lcAssigneePanelBody').height(reGetBodyHeight()-218);
    $('#lcAssigneeModalLabel').html(text);
    $('#lcAssigneeModal').modal({backdrop:'static',keyboard:false});
    $('#lcAssigneeModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcAssigneeModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        $('#searchFormUserinfo')[0].reset();
        var opt = {
            searchformId:'searchFormUserinfo'
        };
        var options = DataTablesPaging.pagingOptions({
            ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/list',opt);},//渲染数据
            //在第一位置追加序列号
            fnRowCallback:function(nRow, aData, iDisplayIndex){
                jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                return nRow;
            },
            order:[],//取消默认排序查询,否则复选框一列会出现小箭头
            tableHeight:'150px',
            //列表表头字段
            colums:[
                {
                    sClass:"text-center",
                    width:"50px",
                    data:"xt_userinfo_id",
                    render:function (data, type, full, meta) {
                        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUserinfo" value="' + data + '" /><span></span></label>';
                    },
                    bSortable:false
                },
                {
                    data:"xt_userinfo_id",
                    width:"50px"
                },
                {
                    data:'xt_userinfo_name'
                },
                {
                    data:'xt_userinfo_realName'
                },
                {
                    data:'xt_userinfo_phone'
                },
                {
                    data:'xt_userinfo_origo'
                },
                {
                    data:'xt_userinfo_birthday'
                },
                {
                    data:'xt_userinfo_email'
                }
            ]
        });
        grid=$('#userinfoDatatables').dataTable(options);
        //实现全选反选
        docheckboxall('checkallUserinfo','checkchildUserinfo');
        //实现单击行选中
        clickrowselected('UserinfoDatatables');
        //实现单击操作开始
        var table = $('#userinfoDatatables').DataTable();
        $('#userinfoDatatables tbody').on('click', 'tr', function () {
            var data = table.row( this ).data();
            console.info(data.xt_userinfo_realName);
        });
        //实现单击操作结束
    });
}

function doUser(){
    if($(".checkchildUserinfo:checked").length != 1){
        toastrBoot(4,"请选择要处理的人！");
        return;
    }
    msgTishCallFnBoot("确定要操作该项？",function(){
        var xt_userinfo_id =$(".checkchildUserinfo:checked").val();
        var taskId = $('#taskId').val();
        var lcTaskType = $('#lcTaskType').val();
        var params = {taskId:taskId,mutilValue:xt_userinfo_id};
        if(lcTaskType == 0){
            ajaxBReq(workflowModules+'/lcTask/transfer',params,['datatables'],null,"POST");
        }
        if(lcTaskType == 1){
            ajaxBReq(workflowModules+'/lcTask/setOwner',params,['datatables'],null,"POST");
        }
        if(lcTaskType == 2){
            ajaxBReq(workflowModules+'/lcTask/addGroupUser',params,['datatables'],null,"POST");
        }
        if(lcTaskType == 3){
            ajaxBReq(workflowModules+'/lcTask/deleteGroupUser',params,['datatables'],null,"POST");
        }
        if(lcTaskType == 4){
            ajaxBReq(workflowModules+'/lcTask/delegateTask',params,['datatables'],null,"POST");
        }
        $('#lcAssigneeModal').modal('hide');
        search('datatables');
    });
}

/**
 * 完成任务
 * @param taskId
 */
function completeTask(taskId){
    msgTishCallFnBoot("确定要完成该任务？",function(){
        var params = {taskId:taskId};
        ajaxBReq(workflowModules+'/lcTask/completeTask',params,['datatables'],null,"POST");
    });
}

/**
 * 流程图
 * @param id
 */
function showLcProcessInstance(id){
    $('#lcImagePanelBody').height(reGetBodyHeight()-128);
    $('#lcImageModalLabel').html("实例监控图");
    $('#lcImageModal').modal({backdrop:'static',keyboard:false});
    $("#lcImageIframe",document.body).attr("src",base_html_redirect+"/workflow/lc-image/image.html?id="+id);
    $('#lcImageModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcImageModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
    });
}

/**
 * 关闭图片窗体
 */
function closeLcImageWin(){
    search('datatables');
}

/**
 * 初始化流程审批人窗体
 * @param id
 */
function initLcApprovalWin(id){
    var approvalCount = 0 ;
    $('#lcHisLogPanelBody').height(reGetBodyHeight()-128);
    $('#lcHisLogModal').modal({backdrop:'static',keyboard:false});
    $('#lcHisLogModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。  
        var $modal_dialog = $("#lcHisLogModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.6+'px'});
        if(++approvalCount == 1) {
            $('#searchFormApproval')[0].reset();
            $('#instanceId').val(id);
            var opt = {
                searchformId: 'searchFormApproval'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax: function (data, callback, settings) {
                    datatablesCallBack(data, callback, settings, workflowModules + '/lcApproval/list', opt);
                },//渲染数据
                //在第一位置追加序列号
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex + 1);
                    return nRow;
                },
                order: [],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight: '300px',
                //列表表头字段
                colums: [
                    {
                        sClass: "text-center",
                        width: "50px",
                        data: "lcApprovalId",
                        render: function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildApproval" value="' + data + '" /><span></span></label>';
                        },
                        bSortable: false
                    },
                    {
                        data: "lcApprovalId",
                        width: "50px"
                    },
                    {
                        data: 'behavior',
                        render: function (data, type, full, meta) {
                            return formatBehavior(data)
                        }
                    },
                    {
                        data: 'comment'
                    },
                    {
                        data: 'create_time'
                    },
                    {
                        data: 'createBy'
                    }
                ]
            });
            grid = $('#approvalDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallApproval', 'checkchildApproval');
            //实现单击行选中
            // clickrowselected('approvalDatatables');
        }
    });

}

/**
 * 跳转节点
 * @param id
 */
function initLcTaskJumpWin(id,taskId) {
    $('#lcTaskJumpForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    var SourcesModalCount = 0 ;
    $('#lcTaskJumpPanelBody').height(150);
    $('#lcTaskJumpModal').modal({backdrop:'static',keyboard:false});
    $('#lcTaskJumpModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++SourcesModalCount == 1) {
            // 是弹出框居中。。。
            var $modal_dialog = $("#lcTaskJumpModalDialog");
            $modal_dialog.css({'width':'600px'});
            $('#lcTaskJumpForm')[0].reset();
            $('#processInstanceId').val(id);
            $('#taskIdJump').val(taskId);
            initTaskList("activityId",id);
        }
    });
}

/**
 * 初始化任务节点
 * @param activityId
 * @param id
 */
function initTaskList(activityId,id){
    $("#"+activityId).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:workflowModules+"/lcTask/userTask/list/"+id,
        success: function(result){
            //从服务器获取数据进行绑定
            result = result.data;
            $.each(result, function(i, item){
                str += "<option value=" + item.id + ">" + item.name + "</option>";
            })
            $("#"+activityId).append(str);
        }
    });
}

/**
 * 执行任意节点跳转
 */
function executeJump() {
    msgTishCallFnBoot("确定执行跳转？",function(){
        submitBFormCallFn('lcTaskJumpForm',workflowModules+'/lcTask/executeJump',function(result){
            try{
                toastrBoot(3,"执行跳转节点成功");
                $('#lcTaskJumpModal').modal('hide');
                search('datatables');
            }catch(e){
                window.parent.toastrBoot(4,result.message);
            }
        });
    })
}

/**
 * 终止流程
 * @param taskId
 */
function endProcess(taskId) {
    msgTishCallFnBoot("确定终止流程？",function(){
        var params = {taskId:taskId};
        ajaxBRequestCallFn(workflowModules+'/lcTask/termination',params,function(){
            toastrBoot(3,"执行终止流程成功");
            search('datatables');
        },null,"POST");
    })
}

/**
 *  驳回
 * @param taskId
 */
function reject() {
    msgTishCallFnBoot("确定驳回流程？",function(){
        submitBFormCallFn('lcTaskBackActiviImplForm',workflowModules+'/lcTask/reject',function(result){
            try{
                toastrBoot(3,result.message);
                search('datatables');
                $('#lcTaskBackActiviImplModal').modal('hide');
            }catch(e){
                window.parent.toastrBoot(4,result.message);
            }
        });
    })
}

/**
 * 格式化时间
 * @param timestamp
 * @returns {string}
 */
function formateDhms(timestamp) {
    if (timestamp) {
        timestamp = timestamp/1000;
        var result = '';
        if (timestamp >= 86400) {
            var days = Math.floor(timestamp / 86400);
            timestamp = timestamp % 86400;
            result = days  + ' 天';
            if (timestamp > 0) {
                result += ' ';
            }
        }
        if (timestamp >= 3600) {
            var hours = Math.floor(timestamp / 3600);
            timestamp = timestamp % 3600;
            result += hours + ' 小时';
            if (timestamp > 0) {
                result += ' ';
            }
        }
        if (timestamp >= 60) {
            var minutes = Math.floor(timestamp / 60);
            timestamp = timestamp % 60;
            result += minutes + ' 分钟';
            if (timestamp > 0) {
                result += ' ';
            }
        }
        result += parseInt(timestamp) + ' 秒';
        return result;
    }
    return "";
}

/**
 * 根据任务id查询图片
 * @param taskId
 */
function showLcProcessInstanceBase64(taskId) {
    $('#lcProcessInstanceBase64PanelBody').height(reGetBodyHeight()-128);
    $('#lcProcessInstanceBase64Modal').modal({backdrop:'static',keyboard:false});
    var base64Count = 0 ;
    $('#lcProcessInstanceBase64Modal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#lcProcessInstanceBase64ModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        //加载表单数据
        if(++base64Count == 1){
            ajaxBRequestCallFn(workflowModules+'/lcProcess/view/base64/image/'+taskId,{},function(result){
                $('#imagePath').attr("src", result.data.image);
            });
        }
    });
}

/**
 *
 */
function doSelectApprover() {
    if(returncheckedLength('checkchildApprover') <= 0){
        toastrBoot(4,"请选择人员");
        return;
    }
    var searchType = $('#searchType').val()
    var id = returncheckIds('checkchildApprover').join(",");

    if(searchType == 0 || searchType == 1){
        if(returncheckedLength('checkchildApprover') > 1){
            toastrBoot(4,"所属人或经办人类型不能选择多个");
            return;
        }
    }
    var params = {xt_userinfo_id:id};
    //加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtUserinfo/users",params,function(result){
        var arr = result.data;
        var xt_userinfo_realName = null;
        if(undefined != result.data && null != result.data){
            for (var i = 0; i < arr.length; i++) {
                //名称维护
                if (null == xt_userinfo_realName) {
                    xt_userinfo_realName =arr[i].xt_userinfo_realName;
                } else {
                    xt_userinfo_realName = xt_userinfo_realName + "," + arr[i].xt_userinfo_realName;
                }
            }
        }
        if(null != xt_userinfo_realName){
            msgTishCallFnBoot('确定要选择:<br>'+ xt_userinfo_realName + '？',function(){
                if(searchType == 0){//任务所属人
                    $('#ownerSearch').val(id);
                    $('#ownerText').val(xt_userinfo_realName);
                }
                if(searchType == 1){//经办人
                    $('#transactorsSearch').val(id);
                    $('#transactorsText').val(xt_userinfo_realName);
                }
                if(searchType == 2){//候选人
                    $('#candidatesSearch').val(id);
                    $('#candidatesText').val(xt_userinfo_realName);
                }
                $('#ApproverModal').modal('hide');
            });
        }else{
            $('#ApproverModal').modal('hide');
        }

    },null,"post");
}

/**
 *
 * @param type
 */
function initApprover(type){
    $('#ApproverPanelBody').height(reGetBodyHeight()-218);
    $('#ApproverModal').modal({backdrop:'static',keyboard:false});
    $('#ApproverModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#ApproverModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        $('#searchFormApprover')[0].reset();
        $('#searchType').val(type);
        var opt = {
            searchformId:'searchFormApprover'
        };
        var options = DataTablesPaging.pagingOptions({
            ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/list',opt);},//渲染数据
            //在第一位置追加序列号
            fnRowCallback:function(nRow, aData, iDisplayIndex){
                jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                return nRow;
            },
            order:[],//取消默认排序查询,否则复选框一列会出现小箭头
            tableHeight:'150px',
            //列表表头字段
            colums:[
                {
                    sClass:"text-center",
                    width:"50px",
                    data:"xt_userinfo_id",
                    render:function (data, type, full, meta) {
                        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildApprover" value="' + data + '" /><span></span></label>';
                    },
                    bSortable:false
                },
                {
                    data:"xt_userinfo_id",
                    width:"50px"
                },
                {
                    data:'xt_userinfo_name'
                },
                {
                    data:'xt_userinfo_realName'
                },
                {
                    data:'xt_userinfo_phone'
                },
                {
                    data:'xt_userinfo_origo'
                },
                {
                    data:'xt_userinfo_birthday'
                },
                {
                    data:'xt_userinfo_email'
                }
            ]
        });
        grid=$('#ApproverDatatables').dataTable(options);
        //实现全选反选
        docheckboxall('checkallApprover','checkchildApprover');
        //实现单击行选中
        clickrowselected('ApproverDatatables');
        //实现单击操作开始
        var table = $('#ApproverDatatables').DataTable();
        $('#ApproverDatatables tbody').on('click', 'tr', function () {
            var data = table.row( this ).data();
            console.info(data.xt_userinfo_realName);
        });
        //实现单击操作结束
    });
}

/**
 *
 * @param type
 */
function resetApprover(type){
    clearAll();
}

/**
 *
 * @param thiz
 */
function selectOpt() {
    var val = $("#taskType").val();
    if(val == 0){
        $("#ownerLabel").show();
        $("#ownerDiv").show();

        $("#transactorsLabel").hide();
        $("#transactorsDiv").hide();

        $("#candidatesLabel").hide();
        $("#candidatesDiv").hide();

        $("#groupLabel").hide();
        $("#groupDiv").hide();
    }else if(val == 1){
        $("#ownerLabel").hide();
        $("#ownerDiv").hide();

        $("#transactorsLabel").show();
        $("#transactorsDiv").show();

        $("#candidatesLabel").hide();
        $("#candidatesDiv").hide();

        $("#groupLabel").hide();
        $("#groupDiv").hide();
    }else if(val == 2){
        $("#ownerLabel").hide();
        $("#ownerDiv").hide();

        $("#transactorsLabel").hide();
        $("#transactorsDiv").hide();

        $("#candidatesLabel").show();
        $("#candidatesDiv").show();

        $("#groupLabel").hide();
        $("#groupDiv").hide();
    }else if(val == 3){
        $("#ownerLabel").hide();
        $("#ownerDiv").hide();

        $("#transactorsLabel").hide();
        $("#transactorsDiv").hide();

        $("#candidatesLabel").hide();
        $("#candidatesDiv").hide();

        $("#groupLabel").show();
        $("#groupDiv").show();
    }else{
        $("#ownerLabel").hide();
        $("#ownerDiv").hide();

        $("#transactorsLabel").hide();
        $("#transactorsDiv").hide();

        $("#candidatesLabel").hide();
        $("#candidatesDiv").hide();

        $("#groupLabel").hide();
        $("#groupDiv").hide();
    }
    clearAll();
}

function clearAll() {
    $("#ownerSearch").val("");
    $("#ownerText").val("");

    $("#transactorsSearch").val("");
    $("#transactorsText").val("");

    $("#candidatesSearch").val("");
    $("#candidatesText").val("");

    $("#groupSearch").val("");
    $("#groupText").val("");
}

function resetForm() {
    resetAll();
    clearAll()
}

/**
 *
 * @param taskId
 */
function initOrgWin(type) {
    $('#orgPanelBody').height(reGetBodyHeight()-218);
    $('#orgModal').modal({backdrop:'static',keyboard:false});
    var orgCount = 0 ;
    $('#orgModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#orgModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        //加载表单数据
        if(++orgCount == 1){

        }
    });
}


//初始数据
var dialogWating;
function initGroups(type) {
    var url = "";
    if(type == 0){//按部门
        url = sysModules + "/xtOrg/depart/tree";
    }
    if(type == 1){//按岗位
        url = sysModules + "/xtOrg/post/tree";
    }if(type == 2){//按角色
        url = oauthModules + "/oauthRole/trees";
    }
    if ($('#orgTree').jstree()) {//判断是否已有实例
        $('#orgTree').jstree().destroy();//销毁实例
    };
    ajaxBRequestCallFn(url,{},function(result){
        if(undefined == result.data || null == result.data || "" == result.data){
            $("#orgTree").html("暂无数据！");
        }else{
            // data = result.data;
            data=eval("("+result.data+")");
            $('#orgTree').jstree({ "types" : {
                    //////////样式一///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file"
                    // }
                    ///////样式二///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder m--font-warning"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file  m--font-warning"
                    // }
                    ///////样式三///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder m--font-warning"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file  m--font-warning"
                    // }
                    ///////样式四///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder m--font-brand"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file  m--font-brand"
                    // }
                    // /////样式五///////////
                    // "default" : {
                    //     "icon" : "fa fa-folder m--font-brand"
                    // },
                    // "file" : {
                    //     "icon" : "fa fa-file  m--font-brand"
                    // }
                    // /////样式六///////////
                    "default" : {
                        "icon" : "fa fa-folder m--font-success"
                    },
                    "file" : {
                        "icon" : "fa fa-file  m--font-success"
                    }
                },
                "animation" : 10,
                "state" : { "key" : "demo2" },
                "plugins" : ["dnd", "state", "types",'checkbox' ,"search"],
                'core' : {
                    "themes" : {
                        "responsive": false,
                        "stripes":true,
                        "ellipsis": true //节点名过长时是否显示省略号。
                    },
                    'data' : data
                }
            }).on('select_node.jstree',function(e,data){

                /*选中之后触发*/
            }).on(' deselect_node.jstree',function(e,data){
                /*取消选中触发*/
            }).on("loaded.jstree", function (event, data) {
                data.instance.clear_state();/*此句用来清除之前选中的数据不可以去掉*/
                // data.instance.open_all(-1); // -1 打开所有节点
                data.instance.open_all(2); // -1 打开所有节点
                // $('#orgTree').jstree().open_all();/*初始化打开树，对于复选框。 不展开的话，如果下面有逻辑选择则会有问题*/
                // //选择指定节点
                // var tempIds = $("#ids").val();
                // var tempIdsArr = tempIds.split(",");
                // $.each(TempIdsArr,function(index,value){
                //     var id=value;
                //     $('#orgTree').jstree('select_node',id,true,true);/*选中id对应的节点*/
                // });
            }).on("load_node.jstree",function(event,data){
                /*加载node时候触发*/
            });
        }
    });
}

/**
 *
 */
function doOrgSelect(){
    var candidate_group_type = $("#candidate_group_type").val();
    var idType = "";
    // if(candidate_group_type == 0){//按部门
    //     idType = "DEPART"
    // }
    // if(candidate_group_type == 1){//按岗位
    //     idType = "POST"
    //
    // }
    // if(candidate_group_type == 2){//按角色
    //     idType = "ROLE"
    // }
    //
    // if(candidate_group_type == 3){//按表达式
    //     idType = "BDS"
    // }

    var treeNode = $('#orgTree').jstree(true).get_selected(true); //获取所有选中的节点对象
    var treeNode = $('#orgTree').jstree(true).get_checked(true); //获取所有 checkbox 选中的节点对象
    // var ids = $('#orgTree').jstree().get_checked();//获取所有 checkbox 选中的节点id
    if(undefined == treeNode || null == treeNode || "" == treeNode || null == candidate_group_type || "" == candidate_group_type){
        toastrBoot(4,"未选择节点！");
        return;
    }
    var text = null;
    var id = null;
    for(var i = 0; i < treeNode.length; i++) {
        //编号维护
        if(null == id){
            id= idType+":"+treeNode[i].id
        }else{
            id=id+","+idType+":"+treeNode[i].id;
        }
        //名称维护
        if(null == text){
            text=treeNode[i].text;
        }else{
            text=text+","+treeNode[i].text;
        }

    }
    msgTishCallFnBoot('确定保存所选的数据？',function(){
        $('#groupSearch').val(id);
        $('#groupText').val(text);
        $('#orgModal').modal('hide');
    })
}



/**
 * 展开/收缩
 */
function expandOrg() {
    if($("#expandOrg").val() ==  0){
        $('#orgTree').jstree('open_all')//展开全部
        $("#expandOrg").val(1);
    }else{
        $('#orgTree').jstree('close_all');//收缩全部
        $("#expandOrg").val(0);
    }
}

/**
 * 全选/反选
 */
function checkAllOrg() {
    if($("#checkedOrg").val() ==  0){
        $("#checkedOrg").val(1)
        $('#orgTree').jstree().check_all();//选中所有节点
    }else{
        $('#orgTree').jstree().uncheck_all();//取消选中所有节点
        $("#checkedOrg").val(0)
    }
}

/**
 * 组类型切换
 * @param thiz
 */
function canditateGroupChange(thiz) {
    initGroups(thiz.value)
}

/**
 * 格式化行为
 * @param data
 */
function formatBehavior(data) {
    if(data == 0){
        return "提交";
    }
    if(data == 10){
        return "通过";
    }
    if(data == 20){
        return "驳回";
    }
    if(data == 30){
        return "弃权";
    }
    if(data == 40){
        return "撤回";
    }
    if(data == 50){
        return "终止流程";
    }
    if(data == 60){
        return "执行跳转";
    }
    if(data == 70){
        return "转办";
    }
    if(data == 80){
        return "委派";
    }
    if(data == 90){
        return "加签";
    }
    if(data == 100){
        return "催办";
    }
    if(data == 110){
        return "设置归属人";
    }
    if(data == 120){
        return "挂起";
    }
    if(data == 130){
        return "激活";
    }
    return "缺省";
}

/**
 * 催办
 * @param taskId
 */
function urge(taskId) {
    msgTishCallFnBoot("确定催办该任务？",function(){
        var params = {taskId:taskId,behavior:100};
        ajaxBRequestCallFn(workflowModules+'/lcTask/approve',params,function(){
            toastrBoot(3,"催办任务成功");
            search('datatables');
        },null,"POST");
    })
}