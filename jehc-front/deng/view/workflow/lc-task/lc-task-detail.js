
/**
 * 任务详情
 * @param lc_process_id
 * @param lc_process_title
 */
function toLcTaskHisDetail(id){
    $('#lcTaskDetailBody').height(reGetBodyHeight()*0.5);
    $('#lcTaskDetailModal').modal({backdrop:'static',keyboard:false});
    $('#lcTaskDetailModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#lcTaskDetailModalDialog");
        $modal_dialog.css({'width':reGetBodyWidth()*0.5+'px'});
        //加载表单数据
        ajaxBRequestCallFn(workflowModules+'/lcTaskHis/get/'+id,{},function(result){
            $('#processTitle').val(result.data.processTitle);
            $('#productName').val(result.data.productName);
            $('#groupName').val(result.data.groupName);
            $('#version').val(result.data.version);
            // $('#').html(result.data.lc_deployment_his_status==0?"<font color='#008b8b'>【运行中】</font>":"<font color='#8b0000'>【关闭】</font>");
            $('#name').val(result.data.name);
            $('#assignee').val(result.data.assignee);
            $('#id').val(result.data.taskId);
            $('#owner').val(result.data.owner);
            $('#processDefinitionId').val(result.data.processDefinitionId);
            $('#processInstanceId').val(result.data.processInstanceId);
            $('#taskDefinitionKey').val(result.data.taskDefinitionKey);
            $('#tenantId').val(result.data.tenantId);
            $('#startTime').val(dateformat(result.data.startTime));
            $('#endTime').val(dateformat(result.data.endTime));
            $('#durationInMillis').val(formateDhms(result.data.durationInMillis));
        });
    });
}
