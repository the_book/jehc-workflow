
/**
 * 可驳回节点（按流程图方式查询）
 * @param id
 */
function initLcTaskBackActiviImplWin(id) {
    $('#lcTaskBackActiviImplForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    var SourcesModalCount = 0 ;
    $('#lcTaskBackActiviImplPanelBody').height(150);
    $('#lcTaskBackActiviImplModal').modal({backdrop:'static',keyboard:false});
    $('#lcTaskBackActiviImplModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++SourcesModalCount == 1) {
            // 是弹出框居中。。。
            var $modal_dialog = $("#lcTaskBackActiviImplModalDialog");
            $modal_dialog.css({'width':'600px'});
            $('#lcTaskBackActiviImplForm')[0].reset();
            $('#taskId_').val(id);
            initLcTaskBackActiviImplList("activityId_",id);
        }
    });
}

/**
 * 可驳回节点（按流程图方式查询）
 * @param activityId
 * @param id
 */
function initLcTaskBackActiviImplList(activityId,id){
    $("#"+activityId).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:workflowModules+"/lcTask/canBackActivity/list/"+id,
        success: function(result){
            //从服务器获取数据进行绑定
            result = result.data;
            $.each(result, function(i, item){
                str += "<option value=" + item.id + ">" + item.name + "</option>";
            })
            $("#"+activityId).append(str);
        }
    });
}

/**
 * 下一个节点（根据流程图方向查找）,用于非自定义按钮 方式
 * @param taskId
 */
function initNextNodeWin(taskId) {
    $('#defaultNextNodeForm').bootstrapValidator({
        message:'此值不是有效的'
    });
    $('#nextNodePanelBody').height(reGetBodyHeight()-128);
    $('#nextNodeModal').modal({backdrop:'static',keyboard:false});
    var nextNodeCount = 0 ;
    $('#nextNodeModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        var $modal_dialog = $("#nextNodeModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        $("#currentTaskId").val(taskId);
        $("#nextNodeList").html("");
        //加载表单数据
        if(++nextNodeCount == 1){
            ajaxBRequestCallFn(workflowModules+'/lcTask/nextNode/'+taskId,{},function(result){
                console.log(result);
                var data = result.data;
                $.each(data, function(i, item){
                    createNextNodeTable(item);
                })
            });
        }
    });
}

/**
 *
 * @param nodeId
 * @param nodeName
 */
function createNextNodeTable(item) {
    var uuid = guid();
    var name = item.name;
    var activityId = item.taskId;
    var mutil = item.mutil;
    var mutilValue = item.mutilValue;
    var nodeType = item.nodeType;
    var mutilText = "未知";

    if(name === undefined){
        name = "";
    }
    if(activityId === undefined){
        activityId = "";
    }
    if(mutil === undefined){
        mutil = "";
    }else{
        if(mutil == 10){
            mutilText = "否";
        }
        if(mutil == 20){
            mutilText = "是";
        }
    }
    if(mutilValue === undefined){
        mutilValue = "";
    }
    if(nodeType === undefined){
        nodeType = "";
    }
    var rows = "<div class=\"form-group m-form__group row\" >" +
        "<input class=\"form-control\" type='hidden' id='mutilValue"+uuid+"' value='"+mutilValue+"' name='lcReceiveParams[][mutilValue]'>"+
        "<input class=\"form-control\" type='hidden' id='activityId"+uuid+"' value='"+activityId+"' name='lcReceiveParams[][activityId]'>"+
        "<input class=\"form-control\" type='hidden' id='mutil"+uuid+"' value='"+mutil+"' name='lcReceiveParams[][mutil]'>"+
        "<div class=\"col-md-2\"><label class=\"col-form-label\" id='name"+uuid+"' name='lcReceiveParams[][name]'>"+name+"</label></div>"+
        "<div class=\"col-md-1\"><label class=\"col-form-label\" id='nodeType"+uuid+"' name='lcReceiveParams[][nodeType]'>"+nodeType+"</label></div>"+
        "<div class=\"col-md-1\"><label class=\"col-form-label\" id='mutilText"+uuid+"' name='lcReceiveParams[][mutilText]'>"+mutilText+"</label></div>"+
        "<div class=\"col-md-3\">" +
        "<div class=\"input-group\">" +
        "<input type=\"text\" readonly class=\"form-control m-input\" name='lcReceiveParams[][assigneeText]' id='assigneeText"+uuid+"' placeholder=\"请选择\" aria-describedby=\"basic-addon2\">" +
        "<div class=\"input-group-append\">" +
        "<span style=\"cursor:pointer;\" class=\"input-group-text\" onclick=resetUsers('"+uuid+"','"+mutil+"','"+name+"')>" +
        "<i class=\"fa fa-times\"></i>" +
        "</span>" +
        "</div>" +
        "<div class=\"input-group-append\">" +
        "<span  style='cursor:pointer;' class=\"input-group-text\" onclick=initUsers('"+uuid+"','"+mutil+"','"+name+"')>" +
        "<i class=\"flaticon-user-ok\"></i>" +
        "</span>" +
        "</div>" +
        "</div>" +
        "</div>"+
        "</div>";
    $("#nextNodeList").append(rows);
}

/**
 * 重置审批人
 * @param uuid
 * @param mutil
 * @param text
 */
function resetUsers(uuid,mutil,text) {
    $('#mutilValue'+uuid).val("");
    $('#assigneeText'+uuid).val("");
}
/**
 *
 * @param uuid
 * @param mutil
 */
function initUsers(uuid,mutil,text){
    $('#usersPanelBody').height(reGetBodyHeight()-218);
    $('#usersModalLabel').html(text);
    $('#usersModal').modal({backdrop:'static',keyboard:false});
    $('#usersModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#usersModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        $('#searchFormusers')[0].reset();
        $("#usersUuid").val(uuid);
        $("#userMutil").val(mutil);
        var opt = {
            searchformId:'searchFormusers'
        };
        var options = DataTablesPaging.pagingOptions({
            ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/list',opt);},//渲染数据
            //在第一位置追加序列号
            fnRowCallback:function(nRow, aData, iDisplayIndex){
                jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                return nRow;
            },
            order:[],//取消默认排序查询,否则复选框一列会出现小箭头
            tableHeight:'150px',
            //列表表头字段
            colums:[
                {
                    sClass:"text-center",
                    width:"50px",
                    data:"xt_userinfo_id",
                    render:function (data, type, full, meta) {
                        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildusers" value="' + data + '" /><span></span></label>';
                    },
                    bSortable:false
                },
                {
                    data:"xt_userinfo_id",
                    width:"50px"
                },
                {
                    data:'xt_userinfo_name'
                },
                {
                    data:'xt_userinfo_realName'
                },
                {
                    data:'xt_userinfo_phone'
                },
                {
                    data:'xt_userinfo_origo'
                },
                {
                    data:'xt_userinfo_birthday'
                },
                {
                    data:'xt_userinfo_email'
                }
            ]
        });
        grid=$('#usersDatatables').dataTable(options);
        //实现全选反选
        docheckboxall('checkallusers','checkchildusers');
        //实现单击行选中
        clickrowselected('usersDatatables');
        //实现单击操作开始
        // var table = $('#usersDatatables').DataTable();
        // $('#usersDatatables tbody').on('click', 'tr', function () {
        //     var data = table.row( this ).data();
        //     console.info(data.xt_userinfo_realName);
        // });
        //实现单击操作结束
    });
}

/**
 *
 */
function doSelectUsers() {
    if(returncheckedLength('checkchildusers') <= 0){
        toastrBoot(4,"请选择节点审批人");
        return;
    }
    var uuid = $("#usersUuid").val();
    var mutil = $("#userMutil").val();
    var id = returncheckIds('checkchildusers').join(",");

    if(mutil == 10){
        if(returncheckedLength('checkchildusers') > 1){
            toastrBoot(4,"非会签节点不能选择多条记录");
            return;
        }
    }
    if(mutil == 20){

    }
    var params = {xt_userinfo_id:id};
    //加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtUserinfo/users",params,function(result){
        var arr = result.data;
        var xt_userinfo_realName = null;
        if(undefined != result.data && null != result.data){
            for (var i = 0; i < arr.length; i++) {
                //名称维护
                if (null == xt_userinfo_realName) {
                    xt_userinfo_realName =arr[i].xt_userinfo_realName;
                } else {
                    xt_userinfo_realName = xt_userinfo_realName + "," + arr[i].xt_userinfo_realName;
                }
            }
        }
        if(null != xt_userinfo_realName){
            msgTishCallFnBoot('确定要选择:<br>'+ xt_userinfo_realName + '？',function(){
                $('#mutilValue'+uuid).val(id);
                $('#assigneeText'+uuid).val(xt_userinfo_realName);
                $('#usersModal').modal('hide');
            });
        }else{
            $('#usersModal').modal('hide');
        }

    },null,"post");
}

/**
 * 完成任务
 */
function complate() {
    submitBFormCallFn('defaultNextNodeForm',workflowModules+'/lcTask/completeTask',function(result){
        try {
            if(typeof(result.success) != "undefined"){
                if(result.success){
                    window.parent.toastrBoot(3,result.message);
                    search('datatables');
                    $('#nextNodeModal').modal('hide');
                }else{
                    window.parent.toastrBoot(4,result.message);
                }
            }
        } catch (e) {

        }
    },null,"POST");
}