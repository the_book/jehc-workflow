//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-listener/lc-listener-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function addLcListener(){
	submitBForm('defaultForm',workflowModules+'/lcListener/add',base_html_redirect+'/workflow/lc-listener/lc-listener-list.html');
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
    //产品线下拉框数据
    initProductList("lc_product_id");
});


function initProductList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"POST",
        url:workflowModules+"/lcProduct/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.lc_product_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
        }
    });
}


/**
 *
 * @param id
 * @param value_id
 * @param lc_product_id
 */
function initGroupListSetV(id,value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    if(null != $("#"+value_id).val() && '' != $("#"+value_id).val()){
        $.ajax({
            type:"GET",
            url:workflowModules+"/lcGroup/find/"+$("#"+value_id).val(),
            success: function(result){
                result = result.data;
                //从服务器获取数据进行绑定
                $.each(result, function(i, item){
                    str += "<option value=" + item.lc_group_id + ">" + item.name + "</option>";
                })
                $("#"+id).append(str);
            }
        });
    }else{
        $("#"+id).append(str);
    }

}


/**
 *
 * @param thiz
 */
function doChangeEvent(thiz) {
    var val = thiz.value;
    var str = "<option value=''>请选择</option>";
    $("#event").html("");
    if(val == 100){
        str += "<option value='create'>create</option>";
        str += "<option value='assignment'>assignment</option>";
        str += "<option value='complete'>complete</option>";
        str += "<option value='all'>all</option>";
    }

    if(val == 200){
        str += "<option value='start'>start</option>";
        str += "<option value='end'>end</option>";
    }

    if(val == 300){
        str += "<option value='start'>start</option>";
        str += "<option value='end'>end</option>";
        str += "<option value='take'>take</option>";
    }
    $("#event").append(str);
}