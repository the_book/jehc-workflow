//返回
function goback(){
	tlocation(base_html_redirect+'/workflow/lc-listener/lc-listener-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateLcListener(){
	submitBForm('defaultForm',workflowModules+'/lcListener/update',base_html_redirect+'/workflow/lc-listener/lc-listener-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var lc_listener_id = GetQueryString("lc_listener_id");
	//加载表单数据
	ajaxBRequestCallFn(workflowModules+'/lcListener/get/'+lc_listener_id,{},function(result){
		$('#lc_listener_id').val(result.data.lc_listener_id);
		$('#lc_listener_name').val(result.data.lc_listener_name);
        $('#categories').val(result.data.categories);
		$('#type').val(result.data.type);
		$('#event').val(result.data.event);
		$('#listener_val').val(result.data.listener_val);
		$('#lc_product_id').val(result.data.lc_product_id);
		$('#lc_group_id').val(result.data.lc_group_id);
        $('#lc_product_idTemp').val(result.data.lc_product_id);
        $('#lc_group_idTemp').val(result.data.lc_group_id);
        $('#remark').val(result.data.remark);
        //产品线下拉框数据
        initProductList("lc_product_id");
        initGroupListSetV("lc_group_id","lc_product_idTemp","lc_group_idTemp");
        doChangeEvent("event");
        $('#event').val(result.data.event);
	});
});



function initProductList(id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"POST",
        url:workflowModules+"/lcProduct/find",
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.lc_product_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
            $('#lc_product_id').val($('#lc_product_idTemp').val());
        }
    });
}


/**
 *
 * @param id
 * @param value_id
 * @param lc_product_id
 */
function initGroupListSetV(id,value_id,s_value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    if(null != $("#"+value_id).val() && '' != $("#"+value_id).val()){
        $.ajax({
            type:"GET",
            url:workflowModules+"/lcGroup/find/"+$("#"+value_id).val(),
            success: function(result){
                result = result.data;
                //从服务器获取数据进行绑定
                $.each(result, function(i, item){
                    str += "<option value=" + item.lc_group_id + ">" + item.name + "</option>";
                })
                $("#"+id).append(str);
                if(undefined != $("#"+s_value_id).val()){

                    $('#'+id).val($("#"+s_value_id).val());
                }

            }
        });
    }else{
        $("#"+id).append(str);
    }

}


/**
 *
 * @param thiz
 */
function doChangeEvent(id) {
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    var val = $("#categories").val();
    if(val == 100){
        str += "<option value='create'>create</option>";
        str += "<option value='assignment'>assignment</option>";
        str += "<option value='complete'>complete</option>";
        str += "<option value='all'>all</option>";
    }

    if(val == 200){
        str += "<option value='start'>start</option>";
        str += "<option value='end'>end</option>";
    }

    if(val == 300){
        str += "<option value='start'>start</option>";
        str += "<option value='end'>end</option>";
        str += "<option value='take'>take</option>";
    }
    $("#"+id).append(str);
}