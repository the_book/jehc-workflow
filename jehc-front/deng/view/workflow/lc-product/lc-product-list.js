var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,workflowModules+'/lcProduct/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"lc_product_id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"lc_product_id",
                width:"50px"
            },
            {
                data:'name',
                width:"150px"
            },
            {
                data:'l_key',
                width:"150px"
            },
            {
                data:'md5',
                width:"150px"
            },
            {
                data:'create_time',
                width:"50px"
            },
            {
                data:'update_time',
                width:"50px"
            },
            {
                data:'createBy',
                width:"50px"
            },
            {
                data:'modifiedBy',
                width:"50px"
            },
            {
                data:"lc_product_id",
                render:function(data, type, row, meta) {
                    return "<a href=\"javascript:toLcProductDetail('"+ data +"')\" class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill'><i class='la la-eye'></i></a>";
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
});
//新增
function toLcProductAdd(){
    tlocation(base_html_redirect+'/workflow/lc-product/lc-product-add.html');
}
//修改
function toLcProductUpdate(){
    if($(".checkchild:checked").length != 1){
        toastrBoot(4,"选择数据非法");
        return;
    }
    var id = $(".checkchild:checked").val();
    tlocation(base_html_redirect+'/workflow/lc-product/lc-product-update.html?lc_product_id='+id);
}
//详情
function toLcProductDetail(id){
    tlocation(base_html_redirect+'/workflow/lc-product/lc-product-detail.html?lc_product_id='+id);
}
//删除
function delLcProduct(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要删除的数据");
        return;
    }
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {lc_status_id:id};
        ajaxBReq(workflowModules+'/lcProduct/delete',params,['datatables']);
    })
}
