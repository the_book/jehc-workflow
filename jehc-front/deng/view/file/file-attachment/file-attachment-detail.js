//返回
function goback(){
	tlocation(base_html_redirect+'/file/file-attachment/file-attachment-list.html');
}


$(document).ready(function(){
	var xt_attachment_id = GetQueryString("xt_attachment_id");
	//加载表单数据
    ajaxBRequestCallFn(fileModules+"/xtAttachment/get/"+xt_attachment_id,{},function(result){
        $("#xt_attachmentName").val(result.data.xt_attachmentName);
        $("#xt_attachmentTitle").val(result.data.xt_attachmentTitle);
        $("#xt_attachmentType").val(result.data.xt_attachmentType);
        $("#xt_attachmentSize").val(result.data.xt_attachmentSize);
        $("#xt_attachmentPath").val(result.data.xt_attachmentPath);
        if(result.data.xt_attachmentIsDelete == 0){
        	$("#xt_attachmentIsDelete").append("正常");
        }else{
        	$("#xt_attachmentIsDelete").append("已删除");
        }
        $("#xt_path_absolutek").val(result.data.xt_path_absolutek);
        $("#xt_path_relativek").val(result.data.xt_path_relativek);
        $("#xt_path_urlk").val(result.data.xt_path_urlk);
        $("#xt_attachmentCtime").val(result.data.xt_attachmentCtime);
        $("#xt_userinfo_id").val(result.data.xt_userinfo_realName);
    });
});
