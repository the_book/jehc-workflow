//返回
function goback(){
	tlocation(base_html_redirect+'/log/log-operate/log-operate-list.html');
}

/**
 *
 */
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
    ajaxBRequestCallFn(logModules+"/logOperate/get/"+id,{},function(result){
        $("#class_name").val(result.data.class_name);
        $("#method").val(result.data.method);
        $("#param").val(result.data.param);
        $("#result").val(result.data.result);
        $("#method").val(result.data.method);
        $("#uri").val(result.data.uri);
        $("#max_memory").val(result.data.max_memory);
        $("#total_memory").val(result.data.total_memory);
        $("#free_memory").val(result.data.free_memory);
        $("#use_memory").val(result.data.use_memory);
        $("#total_time").val(result.data.total_time);
        $("#begin_time").val(result.data.begin_time);
        $("#end_time").val(result.data.end_time);
        $("#createBy").val(result.data.createBy);
        $("#batch").val(result.data.batch);
        var type = result.data.type;
        if(type == 0){
            $("#type").val("业务");
        }else if(type == 1){
            $("#type").val("参数拦截");
        }
    });
});
