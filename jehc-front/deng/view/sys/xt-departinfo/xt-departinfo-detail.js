//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-departinfo/xt-departinfo-list.html');
}

$(document).ready(function(){
	datetimeInit();
	var xt_departinfo_id = GetQueryString("xt_departinfo_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtDepartinfo/get/"+xt_departinfo_id,{},function(result){
        $("#xt_departinfo_id").val(result.data.xt_departinfo_id);
        $("#xt_departinfo_parentId").val(result.data.xt_departinfo_parentId);
        $("#xt_departinfo_name").val(result.data.xt_departinfo_name);
        $("#d_code").val(result.data.d_code);
        $("#xt_departinfo_connectTelNo").val(result.data.xt_departinfo_connectTelNo);
        $("#xt_departinfo_mobileTelNo").val(result.data.xt_departinfo_mobileTelNo);
        $("#xt_departinfo_faxes").val(result.data.xt_departinfo_faxes);
        $("#xt_departinfo_desc").val(result.data.xt_departinfo_desc);
        $("#xt_departinfo_time").val(result.data.xt_departinfo_time);
        $("#xt_departinfo_type").val(result.data.xt_departinfo_type);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);

        if($('#xt_departinfo_parentId').val() == null || $('#xt_departinfo_parentId').val() == "" || $('#xt_departinfo_parentId').val() == 0){
            $('#xt_departinfo_parentName').val("无");
            return;
        }
        $.ajax({
            type:"GET",
            url:sysModules+"/xtDepartinfo/get/"+$('#xt_departinfo_parentId').val(),
            success: function(result){
                result = result.data;
                if(result != null && result != ''){
                    $('#xt_departinfo_parentName').val(result.xt_departinfo_name);
                }else{
                    $('#xt_departinfo_parentName').val("无");
                    $('#xt_departinfo_parentId').val("0");
                }
            }
        });
    });
});
