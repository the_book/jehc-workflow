var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtPlatform/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
		tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"xt_platform_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"xt_platform_id",
				width:"50px"
			},
			{
				data:'title'
			},
			{
				data:'status',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return '<span class="label label-lg font-weight-bold label-light-info label-inline">正常</span>';
                    }
                    if(data == 1){
                        return '<span class="label label-lg font-weight-bold label-light-warning label-inline">关闭</span>';
                    }
                    return '<span class="label label-lg font-weight-bold label-light-warning label-inline">缺省</span>';
                }
			},
			{
				data:'createBy'
			},
			{
				data:'modifiedBy'
			},
			{
				data:'create_time'
			},
			{
				data:'update_time'
			},
			{
				data:"xt_platform_id",
				width:"150px",
				render:function(data, type, row, meta) {
					return "<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' onclick=\"javascript:toXtPlatformDetail('"+ data +"')\"><i class='la la-eye'></i></button>";
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});
//新增
function toXtPlatformAdd(){
	tlocation(base_html_redirect+'/sys/xt-platform/xt-platform-add.html');
}
//修改
function toXtPlatformUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/sys/xt-platform/xt-platform-update.html?xt_platform_id='+id);
}
//详情
function toXtPlatformDetail(id){
	tlocation(base_html_redirect+'/sys/xt-platform/xt-platform-detail.html?xt_platform_id='+id);
}
//删除
function delXtPlatform(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {xt_platform_id:id,_method:'DELETE'};
		ajaxBReq(sysModules+'/xtPlatform/delete',params,['datatables'],null,"DELETE");
	})
}
