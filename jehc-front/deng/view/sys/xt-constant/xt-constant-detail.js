//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-constant/xt-constant-list.html');
}


$(document).ready(function(){
	var xt_constant_id = GetQueryString("xt_constant_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtConstant/get/"+xt_constant_id,{},function(result){
        $("#xt_constant_id").val(result.data.xt_constant_id);
        $("#name").val(result.data.name);
        $("#ckey").val(result.data.ckey);
        $("#value").val(result.data.value);
        $("#type").val(result.data.type);
        $("#url").val(result.data.url);
        $("#remark").val(result.data.remark);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
    });
});