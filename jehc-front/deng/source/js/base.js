
//////////////服务端API相关API///////////////
//集成基础地址API（总控API）
var baseApi = "http://localhost:8082";

//授权中心API
var oauthModules = baseApi;

//监控中心API
var monitorModules = baseApi;

//运维中心API
var opModules = baseApi;

//平台中心API
var sysModules = baseApi;

//工作流中心API
var workflowModules = baseApi;

//文件中心API
var fileModules = baseApi;

//日志中心API
var logModules = baseApi;

//即时通讯中心API
var imModules = baseApi;

//即时通讯中心WebSocket
var imWebSocketModules = baseApi;

//报表中心API
var reportModules = baseApi;

//调度中心API
var jobModules = baseApi;

//病历云API
var medicalModules = baseApi;

//运管平台API
var ompModules = baseApi;

//////////////HTML相关基础路径///////////////
var basePath = "http://localhost:8089";

var loginPath = "http://localhost:8089/login.html";

var base_html_redirect = "http://localhost:8089/view";
